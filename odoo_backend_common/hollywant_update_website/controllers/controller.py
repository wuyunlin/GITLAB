# -*- coding: utf-8 -*-
import json

import logging

from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import ensure_db
import odoo.tools.config as config
from datetime import datetime
import pytz


_logger = logging.getLogger(__name__)


class update(http.Controller):
    @http.route([
        '/update_index',
    ], type='http', auth="public", website=True)
    def update_index(self, redirect=None, **kw):
        ensure_db()
        request.params['login_success'] = False
        values = request.params.copy()

        try:
            values['databases'] = http.db_list()
        except:
            values['databases'] = None
        old_uid = request.uid

        update_account = config.get('updating_account', False)
        if not update_account:
            return

        update_password = config.get('updating_password', False)
        if not update_password:
            return

        uid = request.session.authenticate(
            request.session.db, update_account, update_password)
        if uid is not False:
            request.params['login_success'] = True
        request.uid = old_uid
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }

        return request.render(
            "hollywant_update_website.hollywant_update",
            qcontext=context
        )

    @http.route(['/app_api/update_website_info'], type='json', auth='none')
    def update_website_info(self, *args, **kwargs):
        request.cr.execute('''
         SELECT *
         FROM update_website as W
         WHERE W.update_info_valid=true
         ORDER BY W."id"
         DESC
         ''')
        rows_dict = request.cr.dictfetchone()

        result_dict = {'code': -1}
        if rows_dict is not None:
            result_dict['update_note'] = rows_dict['update_note']
            result_dict['update_date_start'] = self.localizeStrTime(
                rows_dict['update_date_start'],
                "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S"
            )
            result_dict['update_date_end'] = self.localizeStrTime(
                rows_dict['update_date_end'],
                "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            result_dict['update_date'] = (self.localizeStrTime(
                rows_dict['update_date_start'],
                "%Y-%m-%d %H:%M:%S", u"%m %d %H:%M") + u'-' +
                self.localizeStrTime(
                    rows_dict['update_date_end'],
                    "%Y-%m-%d %H:%M:%S", "%m %d %H:%M")
            ).replace(' ', '')
        return result_dict

    def localizeStrTime(self, utcTimeStr, fromFormat, toFormat):
        utc = pytz.timezone('UTC')
        context_tz = pytz.timezone(u'Asia/Shanghai')
        utc_time = utc.localize(
            datetime.strptime(utcTimeStr, fromFormat), is_dst=False)
        localized_time = utc_time.astimezone(context_tz)
        localized_time_str = localized_time.strftime(toFormat)
        return localized_time_str
