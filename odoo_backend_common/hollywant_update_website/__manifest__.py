# -*- coding: utf-8 -*-
{
    'name': "HollyWant Update Website",

    'summary': """
    """,

    'description': """
        Hollywant Update Website
    """,

    'author': "Siyuan",
    'website': "http://http://www.want-want.com/",

    'category': 'HollyWant',
    'version': '0.1',
    'sequence': 1,
    'depends': [
        'base'
    ],
    'qweb': [
        'static/xml/update.xml',
    ],
    'data': [
        'views/update_website_view.xml'
    ],
}
