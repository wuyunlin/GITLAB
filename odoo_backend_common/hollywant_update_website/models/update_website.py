# -*- coding: utf-8 -*-
from openerp import fields, models


class UpdateWebsite(models.Model):
    _name = 'update.website'
    _order = 'id desc'

    name = fields.Char(
        string="名称"
    )

    version = fields.Char(
        string="版本号",
        required=True
    )

    update_note = fields.Text(
        string="升级信息",
    )

    update_date_start = fields.Datetime(
        string="升级开始时间",
    )

    update_date_end = fields.Datetime(
        string="升级结束时间",
    )

    update_info_valid = fields.Boolean("是否有效")
