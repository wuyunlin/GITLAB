/**
 * Created by zhouhao on 16-12-1.
 */
odoo.define('hollywant_update_website.hollywant_update_temp', function (require) {
    "use strict";
    var core = require('web.core');
    var Widget = require('web.Widget');
    var ajax = require('web.ajax');
    var updateTime = Widget.extend(
        {
            template: "update",
            init: function (parent) {
                var self = this;
                this._super(parent);
            },
            start: function () {
                var self = this;
                self.updateTime();
            },

            updateTime: function () {
                var self =this;
                self.rpc("/app_api/update_website_info", {}).then(function (rm) {
                   $('#update_content').html(rm.update_note);
                    $('#begin_time').html(
                        rm.update_date_start.split(' ')[0].split('-')[1]
                        + "."
                        + rm.update_date_start.split(' ')[0].split('-')[2]
                        + " "
                        +rm.update_date_start.split(' ')[1].split(':')[0]
                        + ":"
                        +rm.update_date_start.split(' ')[1].split(':')[1]
                    );
                    $('#end_time').html(
                        rm.update_date_end.split(' ')[0].split('-')[1]
                        + "."
                        +rm.update_date_end.split(' ')[0].split('-')[2]
                        + " "
                        +rm.update_date_end.split(' ')[1].split(':')[0]
                        + ":"
                        +rm.update_date_end.split(' ')[1].split(':')[1]
                    );
                });
            },
        }
    );

    core.action_registry.add('hollywant_update_website.hollywant_update_temp', updateTime);
    updateTime.is_ready = init();
    return updateTime;
});