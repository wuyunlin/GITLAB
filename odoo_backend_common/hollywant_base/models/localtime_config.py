# -*- coding: utf-8 -*-

import pytz
from datetime import datetime
from odoo.tools import ustr
from odoo.fields import Datetime
def convert_to_export(self, value, record):
    """ convert `value` from the cache to a valid value for export. The
        parameter `env` is given for managing translations.
    """
    #将存储在数据库中的UTC时区的datetime字段转换成用户本机时区
    current_timezone = record.env.context.get('tz', False)
    if current_timezone == False:
        current_timezone = u'Asia/Shanghai'
    current_tz = pytz.timezone(current_timezone)
    utc_tz = pytz.timezone('UTC')
    if value:
        value_datetime = datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
        utc_tz_datetime = utc_tz.localize(value_datetime, is_dst=None)
        value = utc_tz_datetime.astimezone(current_tz)
        localized_time_str = value.strftime("%Y-%m-%d %H:%M:%S")
    else:
        localized_time_str=False
    if record.env.context.get('export_raw_data'):
        return localized_time_str
    return bool(localized_time_str) and ustr(localized_time_str)

Datetime.convert_to_export = convert_to_export
