#coding=utf-8

from odoo import api, fields, models, _
from odoo.tools import partition
from itertools import chain, repeat
from collections import defaultdict
from itertools import chain, repeat
from lxml import etree
from lxml.builder import E

import sys
#
# Functions for manipulating boolean and selection pseudo-fields
#
def name_boolean_group(id):
    return 'in_group_' + str(id)

def name_selection_groups(ids):
    return 'sel_groups_' + '_'.join(map(str, ids))

def is_boolean_group(name):
    return name.startswith('in_group_')

def is_selection_groups(name):
    return name.startswith('sel_groups_')

def is_reified_group(name):
    return is_boolean_group(name) or is_selection_groups(name)

def get_boolean_group(name):
    return int(name[9:])

def get_selection_groups(name):
    return map(int, name[11:].split('_'))

def parse_m2m(commands):
    "return a list of ids corresponding to a many2many value"
    ids = []
    for command in commands:
        if isinstance(command, (tuple, list)):
            if command[0] in (1, 4):
                ids.append(command[1])
            elif command[0] == 5:
                ids = []
            elif command[0] == 6:
                ids = list(command[2])
        else:
            ids.append(command)
    return ids

class RoleConfig(models.Model):
    _name = 'role.config'

    name = fields.Char(u'角色')
    groups_ids = fields.Many2many('res.groups','role_config_group_rel','role_id','group_id',string=u'组')
    company_id = fields.Many2one('res.company', u'所属公司', index=True, default= lambda self: self.env.user.company_id.id)

    @api.model
    def create(self, values):
        values = self._remove_reified_groups(values)
        user = super(RoleConfig, self).create(values)
        return user

    @api.multi
    def write(self, values):
        values = self._remove_reified_groups(values)
        res = super(RoleConfig, self).write(values)
        return res

    def _remove_reified_groups(self, values):
        """ return `values` without reified group fields """
        add, rem = [], []
        values1 = {}

        for key, val in values.iteritems():
            if is_boolean_group(key):
                (add if val else rem).append(get_boolean_group(key))
            elif is_selection_groups(key):
                rem += get_selection_groups(key)
                if val:
                    add.append(val)
            else:
                values1[key] = val

        if 'groups_ids' not in values and (add or rem):
            # remove group ids in `rem` and add group ids in `add`
            values1['groups_ids'] = zip(repeat(3), rem) + zip(repeat(4), add)

        return values1

    @api.model
    def default_get(self, fields):
        group_fields, fields = partition(is_reified_group, fields)
        fields1 = (fields + ['groups_ids']) if group_fields else fields
        values = super(RoleConfig, self).default_get(fields1)
        self._add_reified_groups(group_fields, values)
        return values

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        # determine whether reified groups fields are required, and which ones
        fields1 = self.fields_get().keys()
        group_fields, other_fields = partition(is_reified_group, fields1)

        # read regular fields (other_fields); add 'groups_id' if necessary
        drop_groups_id = False
        if group_fields and fields:
            if 'groups_ids' not in other_fields:
                other_fields.append('groups_ids')
                drop_groups_id = True
        else:
            other_fields = fields

        res = super(RoleConfig, self).read(other_fields, load=load)

        # post-process result to add reified group fields
        if group_fields:
            for values in res:
                self._add_reified_groups(group_fields, values)
                if drop_groups_id:
                    values.pop('groups_ids', None)
        return res

    def _add_reified_groups(self, fields, values):
        """ add the given reified group fields into `values` """
        gids = set(parse_m2m(values.get('groups_ids') or []))
        for f in fields:
            if is_boolean_group(f):
                values[f] = get_boolean_group(f) in gids
            elif is_selection_groups(f):
                selected = [gid for gid in get_selection_groups(f) if gid in gids]
                values[f] = selected and selected[-1] or False

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(RoleConfig, self).fields_get(allfields, attributes=attributes)
        # add reified groups fields
        for app, kind, gs in self.env['res.groups'].sudo().get_groups_by_application():
            # boolean group fields
            for g in gs:
                res[name_boolean_group(g.id)] = {
                    'type': 'boolean',
                    'string': g.name,
                    'help': '',
                    'exportable': False,
                    'selectable': False,
                }
        return res


class resUser(models.Model):
    _inherit = 'res.users'
    user_role= fields.Many2one('role.config',string='用户角色')

    def _default_groups(self):
        default_user = self.env.ref('base.default_user', raise_if_not_found=False)
        if default_user:
            default_user = default_user.sudo()
        return (default_user or self.env['res.users']).groups_id

    @api.model
    def create(self, values):
        if values.get('user_role'):
            groups_ids = self.env['role.config'].browse(values.get('user_role')).groups_ids
            if groups_ids:
                groups_ids = groups_ids.ids
                values['groups_id'] = [(6,0,groups_ids)]
        user = super(resUser, self).create(values)
        return user


    @api.multi
    def write(self,values):
        if values.get('user_role'):
            groups_ids = self.env['role.config'].browse(values.get('user_role')).groups_ids
            if groups_ids:
                groups_ids = groups_ids.ids
                values['groups_id'] = [(6,0,groups_ids)]
        res = super(resUser, self).write(values)
        return res


    @api.multi
    def res_group_user(self,role_config_id,user_id):
        cr = self.env.cr
        sql = """select group_id from role_config_group_rel where role_id = %d  """ % role_config_id
        cr.execute(sql)
        res_group_ids = cr.fetchall()
        sql = """insert into res_groups_users_rel (gid,uid) VALUES """
        group_value_str = ''
        for index, res_group_id in enumerate(res_group_ids):
            if index == 0:
                group_value_str = group_value_str + '(%d,%d)' % (res_group_id[0], user_id)
            else:
                group_value_str = group_value_str + ',' + '(%d,%d)' % (res_group_id[0], user_id)
        sql = sql + group_value_str
        cr.execute(sql)
        return

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(resUser, self).fields_get(allfields, attributes=attributes)
        # add reified groups fields
        for app, kind, gs in self.env['res.groups'].sudo().get_groups_by_application():
            #所有都展示为selection类型
            for g in gs:
                res[name_boolean_group(g.id)] = {
                    'type': 'boolean',
                    'string': g.name,
                    'help': '',
                    'exportable': False,
                    'selectable': False,
                }
        return res


class GroupsViewRole(models.Model):
    _inherit = 'res.groups'

    @api.model
    def _update_user_groups_view(self):
        """ Modify the view with xmlid ``base.user_groups_view``, which inherits
            the user form view, and introduces the reified group fields.
        """
        if self._context.get('install_mode'):
            # use installation/admin language for translatable names in the view
            user_context = self.env['res.users'].context_get()
            self = self.with_context(**user_context)

        # We have to try-catch this, because at first init the view does not
        # exist but we are already creating some basic groups.
        view = self.env.ref('base.user_groups_view', raise_if_not_found=False)
        view_config = self.env.ref('group_security.group_role_config_form', raise_if_not_found=False)
        if view and view.exists() and view._name == 'ir.ui.view':
            group_no_one = view.env.ref('base.group_no_one')
            xml1, xml2 = [], []
            xml1.append(E.separator(string=_('Application'), colspan="2"))
            for app, kind, gs in self.get_groups_by_application():
                # hide groups in categories 'Hidden' and 'Extra' (except for group_no_one)
                attrs = {}
                if app.xml_id in (
                'base.module_category_hidden', 'base.module_category_extra', 'base.module_category_usability'):
                    attrs['groups'] = 'base.group_no_one'

                if 1:
                    #修改源码，把所有都展示为选择框类型
                    # application separator with boolean fields
                    app_name = app.name or _('Other')
                    xml2.append(E.separator(string=app_name, colspan="4", **attrs))
                    for g in gs:
                        field_name = name_boolean_group(g.id)
                        if g == group_no_one:
                            # make the group_no_one invisible in the form view
                            xml2.append(E.field(name=field_name, invisible="1", **attrs))
                        else:
                            xml2.append(E.field(name=field_name, **attrs))

            xml2.append({'class': "o_label_nowrap"})
            xml = E.field(E.group(*(xml1), col="2"), E.group(*(xml2), col="4"), name="groups_id", position="replace")
            xml.addprevious(etree.Comment("GENERATED AUTOMATICALLY BY GROUPS"))
            xml_content = etree.tostring(xml, pretty_print=True, xml_declaration=True, encoding="utf-8")
            view.with_context(lang=None).write({'arch': xml_content})
            if view_config and view_config.exists() and view_config._name == 'ir.ui.view':
                xml_config = E.field(E.group(*(xml1), col="2"), E.group(*(xml2), col="4"), name="groups_ids",
                              position="replace")
                xml_config.addprevious(etree.Comment("GENERATED AUTOMATICALLY BY GROUPS"))
                xml_content2 = etree.tostring(xml_config, pretty_print=True, xml_declaration=True, encoding="utf-8")
                view_config.with_context(lang=None).write({'arch': xml_content2})

