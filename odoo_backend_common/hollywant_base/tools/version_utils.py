# -*- coding: utf-8 -*-

import logging

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')

#用于版本的转化 例如：0.7.0（目前公司都是 三位的版本号）,版本号支持到3位，版本间隔支持到 2位
#-1 代表版本格式不对
def versionToInt(version):
    version_list  = version.split('.')
    assert len(version_list) == 3, u'版本号不正确'
    version_int = 0
    version_int= int (version_list[0]) * 1000000 + int (version_list[1]) * 1000 +int (version_list[2])
    return version_int
#字符串版本比较version1 和version2,用操作符比较：operational 支持5类 > < >= <= ==
def versionCompare(version1,version2, operational='>='):
    opertional_liset= ('>','>=','<','<=','==')
    assert operational in opertional_liset, u'操作符不正确'
    version1_num = versionToInt(version1)
    version2_num = versionToInt(version2)
    if operational == '>':
        return version1_num > version2_num
    elif operational == '>=':
        return version1_num >= version2_num
    elif operational == '<':
        return version1_num < version2_num
    elif operational == '<=':
        return version1_num <= version2_num
    else:
        return version1_num == version2_num