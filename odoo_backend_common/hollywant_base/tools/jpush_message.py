# -*- coding: utf-8 -*-

import json
import logging
import traceback
import urllib2

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')

class jpush_message(object):
    def __init__(self,jpush_url,jpush_key):
        self.jpush_url = jpush_url
        self.jpush_key =jpush_key
    #提供接口，内部组装数据tags(并集关系），tags_and(交集关系） 数组，extras：数据字典，键值对
    def send_jpush_mesage(self,content,title=None,tags=None, flag=True, tags_and=None,extras=None):
        if self.jpush_url==None or self.jpush_key==None:
            print u'Jpush url或者key没有传值'
            return {
                    "result":False,
                    "message":'Jpush url或者key没有传值'
                    }
        if content ==None or len(content)== 0:
            print u'消息内容不能为空'
            return False 
        if title == None:
            title='猴犀利消息'
        json_data={
                "platform": "all",
                "audience":"all",
                "notification": {
                    "android": {
                        "alert": content,
                        "title": title,
                    },
                    "ios": {
                        "alert": content,
                        "title":title,
                        "sound": "default",
                    }
                },
                "options": {
                    "time_to_live": 60,
                    "apns_production": flag
            },
        }
        if tags !=None and isinstance(tags,list) ==True and len(tags)>0:
            json_data["audience"]= {
                    "tag": tags
                    
                }
        if tags_and !=None and isinstance(tags_and,list) ==True and len(tags_and)>0:
            if json_data["audience"] =="all":
                json_data["audience"]= {
                    "tag_and": tags_and
                }
            else:
                json_data["audience"]["tag_and"]= tags_and
        if extras !=None and isinstance(extras,dict) ==True:
            json_data["notification"]["android"]["extras"]=extras
            json_data["notification"]["ios"]["extras"]=extras
        return self.send_message(json_data)
    #外部组装json数据
    def send_message(self,json_data):
        if self.jpush_url==None or self.jpush_key==None:
            return {
                    "result":False,
                    "message":'Jpush url或者key没有传值'
                    }
        send_headers ={
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": "Basic "+self.jpush_key
                       }
        print json_data
        try:
            jsondata=json.dumps(json_data)
            req = urllib2.Request(url=self.jpush_url,data=jsondata,headers=send_headers)
            r = urllib2.urlopen(req)
            print r.read()
            r.close()
            return {
                    "result":True,
                    }
        except:
            print traceback.print_exc()
            return {
                    "result":True,
                    "message":'tag 值没有被注册'
                    }
