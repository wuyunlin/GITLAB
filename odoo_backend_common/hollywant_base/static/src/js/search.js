odoo.define('hollywant_base.search', function (require) {
    "use strict";
    var SearchView = require('web.SearchView');
    SearchView.include({
        init: function () {
            var self = this;
            self._super.apply(self, arguments);
            self.visible_filters = (localStorage.visible_search_menu !== 'false');
        }
    }); 
});
