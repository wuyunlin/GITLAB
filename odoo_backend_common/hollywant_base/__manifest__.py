# -*- coding: utf-8 -*-
{
    'name': "HollyWant Base",

    'summary': """
    """,

    'description': """
        Hollywant Base
    """,

    'author': "Siyuan",
    'website': "http://http://www.want-want.com/",

    'category': 'HollyWant',
    'version': '0.1',
    'sequence': 1,
    'depends': [
        'base'
    ],

    'data': [
        'security/group_security_security.xml',
        'security/ir.model.access.csv',
        'views/app_update_view.xml',
        'views/role_config_view.xml',
        #'views/user2_view.xml',
        'views/search_view.xml',
    ],
}
