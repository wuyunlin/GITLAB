# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################
{
    "name": "Product Variant Extra Price",
    "category": 'Uncategorized',
    "summary": """
        This module allows you to manually apply additional extra prices for Product's variants.""",
    "description": """
====================
**Help and Support**
====================
.. |icon_features| image:: variant_price_extra/static/src/img/icon-features.png
.. |icon_support| image:: variant_price_extra/static/src/img/icon-support.png
.. |icon_help| image:: variant_price_extra/static/src/img/icon-help.png

|icon_help| `Help <http://webkul.uvdesk.com/en/customer/create-ticket/>`_ |icon_support| `Support <http://webkul.uvdesk.com/en/customer/create-ticket/>`_ |icon_features| `Request new Feature(s) <http://webkul.uvdesk.com/en/customer/create-ticket/>`_
    """,
    "sequence": 1,
    "author": "Webkul Software Pvt. Ltd.",
'website': 'https://store.webkul.com/Odoo-Product-Variant-Extra-Price.html',
    "version": '1.0',
    "depends": ['product'],
    "data": ['views/product_inherit_view.xml'],
    'images': ['static/description/Banner.png'],
    "installable": True,
    "application": True,
    "auto_install": False,
    "price": 20,
    "currency": 'EUR',
"pre_init_hook": 'pre_init_check',
'live_test_url':'http://odoodemo.webkul.com/?module=variant_price_extra&version=10.0',
}
