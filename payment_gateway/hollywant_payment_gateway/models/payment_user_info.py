# -*- coding: utf-8 -*-
from openerp import api, fields, models
from odoo.addons.hollywant_payment_gateway.common.backend_common import *
from openerp.tools.translate import _


class PaymentUserInfo(models.Model):
    _name = 'payment.user.info'

    name = fields.Char(
        string='Name',
        required=True
    )

    user_id = fields.Many2one(
        comodel_name='res.users',
        string='User',
        required=True,
        default=lambda self: self.env.user.id
    )

    hollywant_payment_id = fields.Char(
        string='Hollywant Payment ID',
        required=True
    )

    hollywant_secret_key = fields.Char(
        string='Hollywant Secret Key',
        required=True
    )

    # 支付宝信息配置
    alipay_seller_id = fields.Char(
        string='Alipay Seller ID',
    )

    alipay_app_id = fields.Char(
        string='Alipay App ID',
    )

    alipay_private_rsa = fields.Text(
        string='Alipay Private RSA'
    )
    alipay_public_key = fields.Text(
        string='Alipay Public Key'
    )
    # 微信信息配置
    weixinpay_seller_id = fields.Char(
        string='Weixinpay Seller ID',
    )

    weixinpay_app_id = fields.Char(
        string='Weixinpay App ID',
    )

    weixinpay_private_key = fields.Text(
        string='Weixinpay Private Key'
    )
    # 旺币信息配置
    wangbipay_seller_id = fields.Char(
        string='Wangbipay Seller ID',
    )

    wangbipay_app_id = fields.Char(
        string='Wangbipay App ID',
    )

    is_alipay = fields.Boolean(string='AliPay')

    is_weixinpay = fields.Boolean(string='WeixinPay')

    is_wangbipay = fields.Boolean(string='WangbiPay')

    def _remove_spaces(self, val):
        return val.strip()

    def _remove_fields_spaces(self, val):
        if val.get('hollywant_payment_id', False):
            val['hollywant_payment_id'] = \
                self._remove_spaces(val['hollywant_payment_id'])

        if val.get('alipay_seller_id', False):
            val['alipay_seller_id'] = \
                self._remove_spaces(val['alipay_seller_id'])

        if val.get('alipay_app_id', False):
            val['alipay_app_id'] = \
                self._remove_spaces(val['alipay_app_id'])

        if val.get('alipay_private_rsa', False):
            val['alipay_private_rsa'] = \
                self._remove_spaces(val['alipay_private_rsa'])

        if val.get('alipay_public_key', False):
            val['alipay_public_key'] = \
                self._remove_spaces(val['alipay_public_key'])

        if val.get('weixinpay_seller_id', False):
            val['weixinpay_seller_id'] = \
                self._remove_spaces(val['weixinpay_seller_id'])

        if val.get('weixinpay_app_id', False):
            val['weixinpay_app_id'] = \
                self._remove_spaces(val['weixinpay_app_id'])

        if val.get('weixinpay_private_key', False):
            val['weixinpay_private_key'] = \
                self._remove_spaces(val['weixinpay_private_key'])

        if val.get('wangbipay_seller_id', False):
            val['wangbipay_seller_id'] = \
                self._remove_spaces(val['wangbipay_seller_id'])

        if val.get('wangbipay_app_id', False):
            val['wangbipay_app_id'] = \
                self._remove_spaces(val['wangbipay_app_id'])

        return val

    @api.constrains('hollywant_payment_id')
    def _check_payment_id(self):
        payment_user = self.env['payment.user.info'].search(
            [('hollywant_payment_id', '=', self.hollywant_payment_id)]
        )
        if len(payment_user) > 1:
            raise ValueError(_('Payment ID is exist!'))

    @api.onchange('name')
    def onchange_name(self):
        if self.name:
            if not self.hollywant_secret_key:
                self.hollywant_secret_key = get_md5_string(self.name)

    @api.multi
    def read(self, fields=None, load='_classic_read'):
        res = super(PaymentUserInfo, self).read(fields=fields, load=load)
        user_id = self.env.user.id

        if user_id == 1:
            return res

        params = self._context.get('params', False)

        if params:
            action = params.get('action', False)

            if action:
                action_id = self.env.ref(
                    'hollywant_payment_gateway.action_payment_user_info')

                if action == action_id.id:
                    values = []
                    for rec in res:
                        rec_user_id = rec['user_id']

                        if type(rec_user_id) != int:
                            rec_user_id = rec['user_id'][0]
                        if user_id == rec_user_id:
                            values.append(rec)

                    return values
        return res

    @api.multi
    def write(self, vals):
        return super(PaymentUserInfo, self).write(
            self._remove_fields_spaces(vals)
        )

    @api.model
    def create(self, vals):
        return super(PaymentUserInfo, self).create(
            self._remove_fields_spaces(vals)
        )
