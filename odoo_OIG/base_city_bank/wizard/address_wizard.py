# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
import logging
from odoo.exceptions import ValidationError
from odoo.exceptions import ValidationError
from odoo.exceptions import UserError, RedirectWarning, ValidationError

_logger = logging.getLogger(__name__)

class address_wizard(models.TransientModel):
    _name = "res.address.wizard"
    _description = u"地址搜索"

    country_id = fields.Many2one('res.state.city', string=u'国', default=lambda self: self.env['ir.model.data'].xmlid_to_object('base_city_bank.res_city_1'))
    state_id = fields.Many2one('res.state.city', string=u'省' )
    city_id = fields.Many2one('res.state.city', string=u'市' )
    region_id = fields.Many2one('res.state.city', string=u'区县' )
    logistics_days = fields.Integer(u'物流货运天数')
    city_fields = fields.Char(u'地址字段', default=lambda self: self._context.get('city_fields', False))
    
    @api.multi
    def set_address(self):
        if (len(self.state_id)) !=1:
            raise ValidationError(u"省不能为空!")
        elif (len(self.city_id)) !=1:
            raise ValidationError(u"市不能为空!")
        elif (len(self.region_id)) !=1:
            raise ValidationError(u"区县不能为空!")


        if self.city_id.parent_id.id != self.state_id.id:
            raise ValidationError(u"该省下面没有此市!")
        if self.region_id.parent_id.id != self.city_id.id:
            raise ValidationError(u"该市下面没有此区县!")

        obj = self._context.get('active_model', False)
        obj_id = self._context.get('active_id', False)
        res_partner_object = self.env['res.partner'].search([('id', '=', obj_id)])
        res_partner_object.write({'logistics_days':self.logistics_days})
        ff = self.city_fields.split(',')
        ff = [x.strip() for x in ff]

        field_city_country = len(ff) > 0 and ff[0]
        field_city_state = len(ff) > 1 and ff[1]
        field_city_city = len(ff) > 2 and ff[2]
        field_city_region = len(ff) > 3 and ff[3]
        city_state = self.state_id and self.state_id.name
        city_city = self.city_id and self.city_id.name
        city_region = self.region_id and self.region_id.name
        city_country = self.env['ir.model.data'].xmlid_to_object('base.cn').id
        city_data = { }
        if field_city_country and city_country:
            city_data[field_city_country] = city_country
        if field_city_state and city_state:
            city_state = self.env['res.country.state'].search([('name', 'ilike', city_state), ('country_id', '=', city_country)])
            if city_state:
                city_data[field_city_state] = city_state[0].id
        if field_city_city and city_city and city_city != u'市辖区' and city_city != u'县':
            city_data[field_city_city] = city_city
        if field_city_region and city_region:
            city_data[field_city_region] = city_region
        self.env[obj].browse(obj_id).write(city_data)
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def onchange_region_id(self,region_id):
        res = {}
        if region_id:
            res_state_city = self.env['res.state.city'].search([('id', '=', int(region_id))])
            res['logistics_days'] = res_state_city.logistics_days
        return {'value': res}

