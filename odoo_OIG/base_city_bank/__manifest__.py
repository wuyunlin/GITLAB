# -*- coding: utf-8 -*-

{
    'name': '中国省市区及银行网点数据',
    'version': '1.0',
    'category': 'Hidden',
    'description': """
中国省市区及银行网点数据
=========================================

1) 增加省市区县模型res_state_city
2) Partner表单上地址格式改成 国家 - 省 - 市 - 区县 - 街道
3) Partner表单上增加地址搜索按钮。表单增加地址搜索按钮的写法（context指定表单上国家、省、市、区县四个字段的字段名）：<button name="%(action_address_city_wizard)d" type="action" string="地址搜索" class="btn btn-sm oe_edit_only fa fa-external-link btn btn-link " context="{'city_fields': 'country_id,state_id,city,street'}"/>
4) 导入中国省市区县数据，方便地址搜索
5) 导入中国各大银行的开户行名称地址数据，方便搜索

    """,
    'depends': ['sales_team'],
    'website': 'https://www.zhiyunerp.com/',
    'data': [
        # 'res_city_view.xml',
        'inherit_res_partner.xml',
        'res.state.city.csv',
    ],
}
