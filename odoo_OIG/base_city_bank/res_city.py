# -*- coding: utf-8 -*-

import datetime
import logging
import string
import re

from odoo import api, fields, models, _
from odoo.tools.misc import ustr

_logger = logging.getLogger(__name__)

class CountryState(models.Model):
    _description = u"省市区县"
    _name = 'res.state.city'
    _order = 'code'

    name = fields.Char(string=u'名称', required=True )
    code = fields.Char(string=u'代码', required=True )
    parent_id = fields.Many2one('res.state.city', string=u'上级' )
    name_en = fields.Char(string=u'名称拼音')
    name_short = fields.Char(string=u'简拼')
    logistics_days = fields.Integer(u'物流货运天数')
    father_code = fields.Char(u'上面的code')


class ResPartner(models.Model):
    _inherit = 'res.partner.bank'

    acc_name = fields.Char(u'户名', copy=False)
    
    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id:
            self.acc_name = self.partner_id.name 

