
# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError


class inherit_res_partner(models.Model):
    _inherit = 'res.partner'

    my_state_id = fields.Many2one('res.state.city',u'省')
    my_city_id = fields.Many2one('res.state.city',u'市')
    my_region_id = fields.Many2one('res.state.city',u'区县')

    @api.multi
    def onchange_my_state_id(self,my_state_id):
        res = {}
        if my_state_id:
            china_object = self.env['res.state.city'].search([('parent_id', '=', False)])
            res_state_city = self.env['res.state.city'].search([('id', '=', int(my_state_id))])
            if res_state_city.parent_id.id != china_object.id:
                raise ValidationError("请选择中国下面的省份!!")
    @api.multi
    def onchange_my_region_id(self,my_state_id,my_city_id):
        res = {}
        if my_state_id:
            china_object = self.env['res.state.city'].search([('parent_id', '=', False)])
            res_state_city = self.env['res.state.city'].search([('id', '=', int(my_state_id))])
            if res_state_city.parent_id.id != china_object.id:
                raise ValidationError("请选择中国下面的省份!!")


    @api.multi
    def onchange_my_region_id(self,my_region_id):
        res = {}
        if my_region_id:
            res_state_city = self.env['res.state.city'].search([('id', '=', int(my_region_id))])
            res['logistics_days'] = res_state_city.logistics_days
        return {'value': res}








