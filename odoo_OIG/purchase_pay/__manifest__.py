﻿{
    'name': '采购付款申请单',
    'version': '1.0',
    "category": "Generic Modules/Test",
    'description': """
     """,
    'author': 'OIG',
    'depends': ['oig_base'],
    'data': [
        'views/purchase_pay_views.xml',
        'wizard/purchase_pay_wizard_view.xml',
        'security/ir.model.access.csv',
        ],
    'installable': True,
    'auto_install': False,
    'application': True,
}