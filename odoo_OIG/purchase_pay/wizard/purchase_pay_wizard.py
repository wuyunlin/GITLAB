# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
import datetime
# 批量采购审批
class purchase_pay_wizard(models.TransientModel):

    _name = "purchase.pay.wizard"
    _description = "Purchase Pay"

    # 采购员工审批
    @api.multi
    def purchase_approval(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for purchase_pay in self.env['purchase.pay'].browse(active_ids):
            type = {}
            type['type'] = "staff"
            purchase_pay.purchase_pay_audit(type)
        return {'type': 'ir.actions.act_window_close'}

# 批量采购经理审批
class Manager_Batch_Confirm(models.TransientModel):

    _name = "manager.batch.confirm"
    _description = "Manager approval"

    @api.multi
    def manager_batch_approval(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for purchase_pay in self.env['purchase.pay'].browse(active_ids):
            type = {}
            type['type'] = "manager"
            purchase_pay.purchase_pay_audit(type)
        return {'type': 'ir.actions.act_window_close'}

# 批量财务审批
class Finance_Batch_Confirm(models.TransientModel):

    _name = "finance.approval.confirm"
    _description = "Finance approval"

    @api.multi
    def finance_approval(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for purchase_pay in self.env['purchase.pay'].browse(active_ids):
            type = {}
            type['type'] = "finance"
            purchase_pay.purchase_pay_audit(type)
        return {'type': 'ir.actions.act_window_close'}

# 批量财务批量付款
class Finance_Pay(models.TransientModel):

    _name = "finance.pay"
    _description = "Financial Pay"

    @api.multi
    def finance_pay(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for purchase_pay in self.env['purchase.pay'].browse(active_ids):
            purchase_pay.pay_money()
        return {'type': 'ir.actions.act_window_close'}