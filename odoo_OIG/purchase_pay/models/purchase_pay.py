# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
import datetime
class purchase_pay(models.Model):
    _name = 'purchase.pay'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = u'采购付款申请单'

    # 创建方式（手动创建、自动创建）
    #####延期的账单 晚上跑批不删除
    flag = fields.Selection([
        ('manual', u'手动'),
        ('auto', u'自动'),
        ('delay', u'延期'),
    ], string=u'创建方式', track_visibility='onchange')
    # 多个采购订单中间以“，”分割开
    purchase_order = fields.Text(u'采购订单', compute='_compute_purchase_order', store=True)
    audit_state = fields.Selection([
        ('first', u'待采购审批'),
        ('second', u'待采购经理审批'),
        ('third', u'待财务审批'),
        ('fourth', u'审核完成'),
        ('paid', u'已支付'),
    ], string=u'审核状态', default='first', track_visibility='onchange')
    pay_account = fields.Char(u'付款账户')
    account_name = fields.Char(u'户名')
    bank_information = fields.Char(u'开户行信息')
    special_flag = fields.Char(u'特批标识')
    special_reason = fields.Char(u'需要特批的缘由')
    account_invoice_state = fields.Selection([
        ('prepayments', u'预付款申请单'),
        ('payment_requisition', u'付款申请单'),
        ('refund_requisition', u'退款申请单'),
    ], string=u'付款申请单类型')
    supplier_no = fields.Char(u'付款申请单号')
    cost = fields.Float(u'运费', digit=(16, 2))
    pay_type = fields.Char(u'付款方式')
    pay_number = fields.Char(u'外部订单号')
    cost_remark = fields.Char(u'运费备注',track_visibility='onchange')
    website = fields.Char(u'账户网址')
    type = fields.Selection([
        ('in_refund', u'供应商退款'),
        ('in_invoice', u'付款申请单'),

    ], string=u'采购付款申请单类型', default='in_invoice')

    state = fields.Selection([
        ('draft', u'未支付'),
        ('paid', u'已支付'),
    ], string=u'采购付款申请单状态', default='draft')

    date_due = fields.Date(string=u'预计付款日期', readonly=True, index=True, copy=False)

    partner_id = fields.Many2one('res.partner', string=u'供应商')

    invoice_line_ids = fields.One2many('account.invoice.line', 'purchase_pay_id', string=u'付款申请单明细')

    plan_date = fields.Date(u'计划付款日期')

    all_money = fields.Float(u'货款金额', compute='_compute_all_money')

    # 计算 运费和产品总费用之和
    cost_product_money = fields.Float(u'总金额', compute='_compute_all_money')

    account_move_id = fields.Many2one('account.move', u'凭证号')

    responser = fields.Char(u'负责人')

    purchase_time = fields.Char(u'采购审核时间', track_visibility='onchange')

    finance_time = fields.Char(u'财务付款时间', track_visibility='onchange')

    difference = fields.Float(u'差额', track_visibility='onchange')

    # 计算产品对应的总金额
    @api.multi
    def _compute_all_money(self):
        for purchase_pays in self:
            all_money = 0
            for account_invoice_line in purchase_pays.invoice_line_ids:
                all_money += account_invoice_line.quantity
            purchase_pays.all_money = all_money
            purchase_pays.cost_product_money = all_money + purchase_pays.cost

    # 计算关联采购付款申请单中的采购订单
    @api.multi
    @api.depends('invoice_line_ids')
    def _compute_purchase_order(self):
        for each in self:
            if len(each.ids) > 0:
                sql = """select string_agg(b.name,';') from account_invoice a
                        inner join purchase_order b on b.id = a.purchase_order_id where a.purchase_pay_id = %s"""%(each.id)
                self._cr.execute(sql)
                fet = self._cr.fetchone()
                each.purchase_order = fet and fet[0] or ''

    # 批量退款功能
    def refund_money(self):
        account_invoice_instance = self.env['account.invoice'].search([('purchase_pay_id', '=', self.id)])
        for invoice in account_invoice_instance:
            new_invoice = invoice.copy(default={
                'type': 'in_refund',
                'audit_state': 'first',
                'state': 'draft',
                'supplier_no': 'New',
                'account_invoice_state': 'refund_requisition',
                'date_invoice': datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
            })
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.pay',
            'type': 'ir.actions.act_window',
            'res_id': new_invoice.purchase_pay_id[0].id,
            'context': self.env.context,
        }

    # 审批功能（采购审批、采购经理审批、财务审批）
    def purchase_pay_audit(self, type):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        if self.account_invoice_state == "refund_requisition":
            if not self.pay_account:
                raise UserError("退款申请单中付款账户不能为空！")

        result = self.env['account.invoice'].search([('purchase_pay_id', '=', self.id)])
        # 采购审批
        # flag 标识所有的付款申请单中有没有某个付款申请单审核为待采购审批，有的话为1
        flag = 0
        # 特批审批原因（只有在需要采购经理特批时才有）
        reason = ''
        # 员工审批
        if type.get("type") == "staff":
            for account_invoice in result:
                if account_invoice.amount_total:
                    # 执行采购审批
                    if self.audit_state == 'first' and account_invoice.audit_state in('second', 'third', 'fourth'):
                        account_invoice.write({'audit_state': 'first'})
                    account_invoice.purchase_audit()
                    # 采购审核完成，如果单子需要特批的话，则判断采购审批后的特批理由是否为空，
                    # 这是为了控制付款申请单中显示的特批缘由只是保存处理的最后一个要特批的采购订单的特批缘由而导致的特批缘由不全的问题
                    if account_invoice.audit_state == 'second':
                        if reason:
                            if len(account_invoice.special_reason) > len(reason):
                                reason = account_invoice.special_reason
                            else:
                                reason = reason
                        else:
                            reason = account_invoice.special_reason
                        flag = 1
                else:
                    account_invoice_line_parameter = self.env['account.invoice.line'].search([('purchase_pay_id', '=', self.id)])
                    if account_invoice_line_parameter:
                        raise UserError("付款单订单明细不能为空，请补充金额或删掉付款单明细！")
                    else:
                        if not self.cost:
                            raise UserError("邮费不能为空,请补充运费货删掉付款单明细！")
                        else:
                            pass
            # 如果flag 为1，代表其中有待采购经理审批的，则整个采购付款申请单都为待采购经理审批。否则为待财务审批
            account_invoice_result = self.env['account.invoice'].search([('purchase_pay_id', '=', self.id)])
            if account_invoice_result:
                if flag == 1:
                    self.change_audit_state(account_invoice_result, 'second', reason)
                else:
                    self.change_audit_state(account_invoice_result, 'third', reason)
                self.write({'purchase_time': now})
            else:
                raise UserError("付款单订单明细不能为空，请补充金额或删掉付款单！")

        # 经理审批
        elif type.get("type") == "manager":
            for account_invoice in result:
                account_invoice.purchase_manager_audit()
                if account_invoice.audit_state == 'third':
                    flag = 1
                # 如果flag 为1，代表其中有待采购经理审批的，则整个采购付款申请单都为待采购经理审批。否则为待财务审批
            if flag == 1:
                self.change_audit_state(result, 'third', reason)
            else:
                self.change_audit_state(result, 'fourth', reason)
            self.write({'purchase_time': now})
        # 财务审批
        elif type.get("type") == "finance":
            for account_invoice in result:
                account_invoice.finance_audit()
            self.change_audit_state(result, 'fourth', reason)
            self.write({'finance_time': now})

    # 处理审批后，状态的修改
    def change_audit_state(self, result, audit_state, reason):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        temp = 0
        self.write({'special_reason': reason})
        for account_invoice_result in result:
            if account_invoice_result.audit_state != audit_state:
                temp = 1
                break
        if temp == 0:
            self.write({'audit_state': audit_state})
            if audit_state == "paid":
                self.write({'state': "paid"})
        else:
            self.write({'audit_state': audit_state})
            for account_invoice in result:
                account_invoice.write({'audit_state': audit_state})
    # 付款功能
    def pay_money(self):
        result = ''
        # 当前时间
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        account_invoice_result = self.env['account.invoice'].search([('purchase_pay_id', '=', self.id)])
        try:
            for account_invoice in account_invoice_result:
                account_invoice.verification()
                # 如果采购订单中所有明细的已支付金额等于小计，则将付款申请单中的外部订单号写进采购订单中的外部订单号字段中
                ###### Begin
                if self.partner_id[0].is_buy == True:
                    if account_invoice.purchase_order_id and account_invoice.purchase_order_id[0].order_line:
                        temp = 0
                        for order_line in account_invoice.purchase_order_id[0].order_line:
                            if order_line.qty_paid != order_line.price_subtotal:
                                temp = 1

                    if temp == 0:
                        if self.pay_number:
                            self.env['purchase.order'].search([('id', '=', account_invoice.purchase_order_id[0].id)]).write({'partner_ref': self.pay_number})
                ###### End

        except Exception as err:
            result = result + err.name
        if result:
            raise UserError(result)
        else:
            self.write({'audit_state': 'paid'})
            self.write({'state': 'paid'})

        # 计算出运费和产品总金额之和
        all_money = 0
        for account_invoice_line in self.invoice_line_ids:
            all_money += account_invoice_line.quantity
       
        total = self.cost+all_money

        if self.difference:
            total = all_money - self.difference
        else:
            pass

        account_move_lines = []
        # 如果运费不为空，则创建借方明细
        pay_param = self.env['ir.config_parameter'].sudo().get_param('pay')
        if self.cost:
            account_account = self.env['account.account'].search([('code', '=', self.env['ir.config_parameter'].sudo().get_param('freight'))])
            if self.account_invoice_state == 'refund_requisition':
                if account_account:
                    account_invoice_lineA = {'account_id': account_account.id, 'partner_id': self.partner_id.id, 'debit': 0, 'credit': self.cost, 'name': pay_param + self.partner_id.name+"(运费)"+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_lineA))
            else:
                if account_account:
                    account_invoice_lineA = {'account_id': account_account.id, 'partner_id': self.partner_id.id, 'debit': self.cost, 'credit': 0, 'name': pay_param + self.partner_id.name+"(运费)"+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_lineA))
        # 处理差额问题
        if self.difference:
            account_account = self.env['account.account'].search([('code', '=', self.env['ir.config_parameter'].sudo().get_param('difference'))])
            if self.account_invoice_state == 'refund_requisition':
                if account_account:
                    account_invoice_difference = {'account_id': account_account.id, 'partner_id': self.partner_id.id, 'debit': 0, 'credit': -self.difference, 'name': pay_param + self.partner_id.name+"(差额)"+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_difference))
            else:
                if account_account:
                    account_invoice_difference = {'account_id': account_account.id, 'partner_id': self.partner_id.id, 'debit': -self.difference, 'credit': 0, 'name': pay_param + self.partner_id.name+"(差额)"+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_difference))

        # 如果产品总金额不为空，则创建借方明细
        if all_money:
            account_account = self.env['account.account'].search([('code', '=', self.env['ir.config_parameter'].sudo().get_param('bank_savings'))])
            # 退款申请单
            if self.account_invoice_state == 'refund_requisition':
                if account_account:
                    account_invoice_lineB = {'account_id': account_account.id, 'partner_id': self.partner_id.id, 'debit': 0, 'credit': all_money, 'name': pay_param + self.partner_id.name+"(货款)"+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_lineB))
            else:  # 预付款申请单
                if account_account:
                    account_invoice_lineB = {'account_id': account_account.id, 'partner_id': self.partner_id.id, 'debit': all_money, 'credit': 0, 'name': pay_param + self.partner_id.name+"(货款)"+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_lineB))

        # 如果运费和产品总金额不为空，则创建贷方明细
        if total:
            account_account = self.env['account.account'].search([('code', '=', self.env['ir.config_parameter'].sudo().get_param('accounts_payable'))])
            # 退款申请单
            if self.account_invoice_state == 'refund_requisition':
                if account_account:
                    account_invoice_line_credit = {'account_id': account_account.id, 'debit': total, 'credit': 0, 'name': pay_param + self.partner_id.name+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_line_credit))
            else:  # 预付款申请单
                if account_account:
                    account_invoice_line_credit = {'account_id': account_account.id, 'debit': 0, 'credit': total, 'name': pay_param + self.partner_id.name+":" + self.supplier_no}
                    account_move_lines.append((0, 0, account_invoice_line_credit))

        nowDate = datetime.datetime.now() .strftime("%Y-%m-%d")
        if account_move_lines:
            account_move_result = self.env['account.move'].create({'journal_id': int(self.env['ir.config_parameter'].sudo().get_param('journal_id')), 'date': nowDate, 'ref': self.supplier_no, 'line_ids': account_move_lines})
        # 将生成的凭证写到采购订单上去
        self.write({'account_move_id': account_move_result.id})
        # 将财务付款的时间记录下来
        self.write({'finance_time': nowDate})

    # 回退
    def go_back(self):
        result = ''
        account_invoice_result = self.env['account.invoice'].search([('purchase_pay_id', '=', self.id)])
        try:
            for account_invoice in account_invoice_result:
                if account_invoice.audit_state in ("second", "third", "fourth"):
                    account_invoice.write({'audit_state': "first"})
                    account_invoice.write({'special_reason': ""})
                else:
                    raise UserError("当前状态不能进行回退操作！")
        except Exception as err:
            result = result + err.name
        if result:
            raise UserError(result)
        else:
            self.write({'audit_state': 'first'})
            self.write({'special_reason': ""})
            self.write({'purchase_time': ""})
            self.write({'finance_time': ""})

    # 重写创建方法
    @api.model
    def create(self, vals):
        if not vals.get('supplier_no', False) or vals.get('supplier_no') == 'New':
            vals['supplier_no'] = self.env['ir.sequence'].next_by_code('purchase.pay') or '/'
        return super(purchase_pay, self).create(vals)

    # 重写删除方法
    @api.multi
    def unlink(self):
        for purchase_pay_param in self:
            if purchase_pay_param.audit_state != "first":
                raise UserError("当前状态不能进行删除操作，只有待采购审核才能删除！")
            account_invoice = self.env['account.invoice'].search([('purchase_pay_id', '=', purchase_pay_param.id)])
            # 删除大付款申请单的同时删除源付款申请单
            for invoice in account_invoice:
                invoice.unlink()
        return super(purchase_pay, self).unlink()

    # @api.multi
    # def write(self, vals):
    #     res = super(purchase_pay, self).write(vals)
    #     account_invoice = self.env['account.invoice'].search([('purchase_pay_id', '=', self.id)])
    #     for account_param in account_invoice:
    #         account_param.write()
    #
    # def write(self, vals):
    #     res = super(oig_receiver, self).write(vals)
    #     if vals.get('code') or vals.get('name') or vals.get('stock_location_id'):
    #         self.issued_receiver()
    #     return res

class purchase_pay_line(models.Model):
    _inherit = 'account.invoice.line'

    @api.multi
    @api.depends('quantity')
    def _compute_proportion(self):
        for index, each in enumerate(self):
            each.proportion = '%s%%'%(round(100*float(each.quantity)/(each.purchase_line_id.product_qty*each.purchase_line_id.price_unit),2) ) if each.purchase_line_id.product_qty*each.purchase_line_id.price_unit else '/'

    @api.onchange('quantity')
    def _onchange_quantity(self):
        if self.quantity:
            self.proportion='%s%%'%round(100*self.quantity/self.price_subtotal_money,2)
        else:
            self.proportion = '/'

    purchase_pay_id = fields.Many2one('purchase.pay', u'采购付款申请单')

    proportion = fields.Char(compute='_compute_proportion', store=True, string="比例")

    price_subtotal_money = fields.Monetary(related='purchase_line_id.price_subtotal', string="总金额")

    date_due = fields.Date(string=u'预计付款日期', related="purchase_pay_id.date_due")

    line_state = fields.Selection([
        ('first', u'待采购审批'),
        ('second', u'待采购经理审批'),
        ('third', u'待财务审批'),
        ('fourth', u'审核完成'),
        ('paid', u'已支付'),
    ], string=u'审核状态', related='purchase_pay_id.audit_state')
