# -*- coding: utf-8 -*-

{
    'name': '中国会计科目表 -- 智云ERP提供',
    'version': '1.0',
    'category': 'Localization',
    'author': 'OSCG',
    'website': 'https://www.zhiyunerp.com',
    'description': """
    中国会计科目表
    """,
    'depends': ['l10n_cn'],
    'data': [
        'data/l10n_cn_oscg_chart_data.xml',
        #'data/account_oscg_template_data.yml',
    ],
    'license': 'GPL-3',
}
