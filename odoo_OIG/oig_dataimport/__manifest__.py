# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Data import OIG',
    'version': '1.0',
    'category': 'Sales',
    'summary': 'import',
    'description': """
    """,
    'depends': ['product'],
    'data': [
        'views/product_view.xml'

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
