# -*- coding: utf-8 -*-
from odoo import api, fields, models
import logging
import base64
import datetime
_logger = logging.getLogger(__name__)

import sys
import math
reload(sys)
sys.setdefaultencoding('utf8')

class export_data_to_excel(models.TransientModel):
    _name = "export.data.to.excel"
    _description = u"excel导出数据参数配置"
    name = fields.Char(string='Action Usage')
    command = fields.Char(string='Action Usage')
    model = fields.Char(string='Action Usage')
    field = fields.Char(string='Action Usage')
    file_name = fields.Char(string='Action Usage')


class product_template(models.Model):
    _inherit = 'product.template'

    @api.model
    def create(self, vals):
        # vals['name']='供应商王先生'
        res = super(product_template, self).create(vals)
        return res

    def _write_pt(self, pt_id, vals):
        pt_obj=self.env['product.template']
        pt_instance=pt_obj.browse(pt_id)
        return True
##############################################导入产品 开始(_mycopy 暂没用到)##############################################################
    def _mycopy(self, pt_instance, vals,limit):
        cr = self.env.cr
        attribute_id=pt_instance.attribute_line_ids[0].attribute_id.id
        value_id=pt_instance.attribute_line_ids[0].attribute_id.value_ids[0].id

        sql="""select  c_fcode,c_fchinesename  from product_import_view  where c_fcode not in
              (select default_code from product_template where active='t' and default_code is not null )  group by c_fcode,c_fchinesename limit %s"""%(limit)
        cr.execute(sql)
        fet=cr.fetchall() or []
        x=1
        if not fet:
            return False
        for each in fet:
            _logger.info('====line================================%s============='%x)
            x+=1
            c_fcode, c_fchinesename=each
            sql = """select  m_fid, c_fid, m_fchinesename, c_fchinesename, m_fcode, c_fcode, m_faddtime, c_faddtime from product_import_view where m_fcode ='%s'""" % ( m_fcode)
            cr.execute(sql)
            dictfet=cr.dictfetchall()
            value_ids=[]
            for line in dictfet:
                value_ids.append([0, 0, {'attribute_id': attribute_id, 'name':'%s:%s'%(line['c_fchinesename'],line['c_fcode'])   }])
            attribute_line_ids=[ [0, False, {  'product_tmpl_id':pt_instance.id, u'attribute_id': attribute_id, u'value_ids':value_ids     }          ]     ]
            vals.update({'attribute_line_ids':attribute_line_ids,'name':m_fchinesename,'default_code':m_fcode,'my_default_code':m_fcode})
            ###############图片#############################################################
            sql = """select fpicture from createcodepicture where fpicture is not null and fcodetype='母代码' and finterid=%s limit 1"""%(dictfet[0]['m_fid'])
            self.env.cr.execute(sql)
            fet = self.env.cr.fetchone()
            vals['image']=False
            if fet:
                _logger.info('====image================================%s=============' % m_fcode)
                vals.update({'image':base64.b64encode(fet[0])})

            new_id=pt_instance.copy(default=vals)
            sql="""with res as (
                    select a.id, d.c_fcode from product_product a
                    inner join product_attribute_value_product_product_rel b on b.product_product_id=a.id
                    inner join product_attribute_value c on c.id=b.product_attribute_value_id
                    inner join product_import_view d on d.c_fchinesename||':'||d.c_fcode =c.name
                    where a.product_tmpl_id=%s
                    )
                    update product_product set default_code=res.c_fcode from res where res.id=product_product.id;
                    delete from ir_translation where name='product.template,name';
                    """%(new_id.id)
            cr.execute(sql)
            if new_id:
                sql = """insert into product_template_import(product_template_id,code) values (%s,'%s') """ % (new_id.id,m_fcode)
                cr.execute(sql)
                #-------更新子代码图片
                sql="""select c.id,a.fcode,b.fpicture from CreateCodeChildCode a
                        inner join createcodepicture b on  a.FID=b.FInterID and b.FCodeType='子代码'
                        inner join product_product c on c.default_code=a.fcode
                        where c.product_tmpl_id=%s and b.fpicture is not null"""%(new_id.id)
                cr.execute(sql)
                fet=cr.fetchall()
                for line in fet:
                    self.env['product.product'].browse(line[0]).write({'z_image':base64.b64encode(line[2])})
        return True

    ################################################# 导入产品 开始######################################################
    def _copy_pt(self, pt_id, vals,limit):
        _logger.info('======================Start create product_template template:%s========================='%(pt_id))
        cr = self.env.cr
        # sql = """select c_fid, c_fcode,c_fchinesename  from product_import_view  where c_fcode not in
        #       (select default_code from product_template where active='t' and default_code is not null ) group by c_fid, c_fcode,c_fchinesename limit %s  """ % limit
        sql = """select c_fid, c_fcode,c_fchinesename from product_import_view_copy where flag = False limit %s  """ % limit
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            product_ins = self.env['product.template'].browse(pt_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                data={'name':line['c_fchinesename'],'default_code':line['c_fcode']}
                # sql = """select fpicture from createcodepicture where fpicture is not null and fcodetype='子代码' and finterid=%s limit 1""" %(line['c_fid'])
                # cr.execute(sql)
                # fet = cr.fetchone()
                # if fet is not None:
                #     data.update({'image': base64.b64encode(fet[0])})
                # -------更新子代码图片
                sql = """select a.fcode,b.fpicture from CreateCodeChildCode a
                        inner join createcodepicture b on  a.FID=b.FInterID and b.FCodeType='子代码'
                        where a.fcode='%s' and b.fpicture is not null limit 1""" % (line['c_fcode'])
                cr.execute(sql)
                fet = cr.fetchall()
                for line_img in fet:
                    data.update({'image': base64.b64encode(line_img[1])})
                new_product = product_ins.copy(default=data)

                # 导入完成的产品，更改标识状态
                sql = """update product_import_view_copy set flag = TRUE where c_fid = %s """ %(line['c_fid'])
                cr.execute(sql)

            _logger.info('======================end create product_template template:%s=========================' % (pt_id))
        else:
            _logger.info('#######################所有产品导入完成，不需要继续导入了#####################################')
            pass

        return True

    @api.model
    def _import_pt(self,*args):
        #
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_pt(args[0],args[1],args[2])
        else:
            self._write_pt(args[0], args[1])

##############################################产品导入 结束####################################################################

##############################################供应商导入####################################################################
    def _copy_supplier(self, partner_id, vals,limit):
        _logger.info('======================Start create res_partner template:%s========================='%(partner_id))
        cr=self.env.cr
        sql="""select a.fname as name,coalesce(a.fnumber,'') as ref ,coalesce(a.fcontact,'') as comment,coalesce(a.fphone,'') as phone,coalesce(a.ffax,'') as fax,coalesce(a.femail,'') as email from supplier_view a where fname not in (select name from res_partner where active='t' and supplier='t') limit %s  """%limit
        cr.execute(sql)
        dicfet=cr.dictfetchall()
        if dicfet:
            x=1
            partner_ins=self.env['res.partner'].browse(partner_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x+=1
                new_partner=partner_ins.copy(default=line)
                line.update({'supplier':'t','type_flag': 'supplier'})
                new_partner.write(line)
            _logger.info('======================END create res_partner template:%s=========================' % (partner_id))
        else:
            _logger.info('#######################所有供应商导入完成，不需要继续导入了#####################################')
            pass
    @api.model
    def _import_supplier(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_supplier(args[0],args[1],args[2])
        else:
            pass
    ##############################################供应商导入####################################################################

    ######################################################用户导入################################################################
    def _copy_users(self, users_id, vals, limit):
        _logger.info('======================Start create res_users template:%s=========================' % (users_id))
        cr = self.env.cr
        sql = """select a.fname as name,a.fname as login from t_user a where fname not in (select login from res_users where active='t') limit %s  """ % limit
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            users_ins = self.env['res.users'].browse(users_id)
            new_users=0
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                new_users= users_ins.copy(default=line)
                new_users.write(line)
                new_users.partner_id.write({'type_flag':'emp'})
            _logger.info('======================END create res_users template:%s=========================' % (new_users))
        else:
            _logger.info('#######################所有用户导入完成，不需要继续导入了#####################################')
            pass

    @api.model
    def _import_users(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_users(args[0], args[1], args[2])
        else:
            pass
###############################################用户导入结束##################################################################

    ##############################################部门导入####################################################################
    def _copy_department(self, department_id, vals, limit):
        _logger.info('======================Start create department template:%s=========================' % (department_id))
        cr = self.env.cr
        sql = """select fname as name,coalesce(fnumber,'') as ref from t_department
                where fname not in (select name from res_partner where active='t' and type_flag='department') limit %s  """ % (limit)
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            department_id_ins = self.env['res.partner'].browse(department_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                new_department = department_id_ins.copy(default=line)
                line.update({'type_flag': 'department'})
                new_department.write(line)
            _logger.info('======================END create department template:%s=========================' % (department_id))
        else:
            _logger.info('#######################所有部门导入完成，不需要继续导入了#####################################')
            pass
    @api.model
    def _import_department(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_department(args[0], {}, args[2])
        else:
            pass

##############################################部门导入 结束####################################################################

##############################################仓库导入####################################################################
    def _copy_warehouse(self, warehouse_id, vals,limit,type):
        _logger.info('======================Start create stock_warehouse template:%s========================='%(warehouse_id))
        cr=self.env.cr
        if type == 0:
            # 发货仓
            sql ="""select A .NAME,A .code,A .ftype,case when (A .ftranstype = '空运') then 'air' when (A.ftranstype = '海运') then 'sea' else null end as delivery from warehouse_view  a where ftype = 'shipping_warehouse' AND a .name NOT IN (SELECT NAME FROM stock_location WHERE active = 't') limit %s  """%limit
        elif type == 1:
            # 移动仓
            sql = """SELECT A .NAME,A .code,A .ftype,c.ID as stock_location_id_shipping,case when (A .ftranstype = '空运') then 'air' when (A.ftranstype = '海运') then 'sea' else null end as delivery FROM warehouse_view A INNER JOIN warehouse_view b ON a.fdstockid = b.fwarehouseid LEFT JOIN stock_location c ON b.code = c.name WHERE A .ftype = 'move_warehouse' AND A .name NOT IN (SELECT NAME FROM stock_location WHERE active = 't') limit %s  """ % limit
        elif type == 2:
            # 中转仓
            sql = """SELECT A . NAME,A .code,A .ftype,a.name as transfer_warehouse,d. ID AS stock_location_id_shipping, e.ID AS stock_location_id_move,case when (A .ftranstype = '空运') then 'air' when (A.ftranstype = '海运') then 'sea' else null end as delivery FROM warehouse_view A INNER JOIN warehouse_view b ON A .fdstockid = b.fwarehouseid INNER JOIN warehouse_view C ON C .fdstockid = A .fdstockid AND C .ftype = 'move_warehouse' AND C .ftranstype = A .ftranstype LEFT JOIN stock_location d ON b.code = d. NAME LEFT JOIN stock_location e ON C .code = e. NAME WHERE A .ftype = 'transfer_warehouse' AND REPLACE(A.NAME, CHR(10), '') NOT IN (SELECT NAME FROM stock_location WHERE active = 't') limit %s  """ % limit

        cr.execute(sql)
        dicfet=cr.dictfetchall()
        if dicfet:
            x=1
            # warehouse_obj=self.env['stock.location']
            warehouse_ins=self.env['stock.location'].browse(warehouse_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x+=1
                new_warehouse=warehouse_ins.copy(default=line)
                # new_warehouse = warehouse_obj.create(line)
                new_warehouse.write(line)
            _logger.info('======================END create stock_warehouse template:%s=========================' % (warehouse_id))
        else:
            _logger.info('####################### %s 类型仓库导入完成，不需要继续导入了#####################################'%(type))
            pass
    @api.model
    def _import_warehouse(self,*args):
        if not(args and len(args)==4 and args[1] in ('create','write')):
            return True
        pt,action,limit,type=args
        if action=='create':
            self._copy_warehouse(args[0],{},args[2],args[3])
        else:
            pass

    ##############################################仓库导入  结束####################################################################

    ##############################################收货人样品类型导入####################################################################
    def _copy_sample_type(self, sample_type_id, vals,limit):
        _logger.info('======================Start create sample_type template:%s========================='%(sample_type_id))
        cr=self.env.cr
        sql="""SELECT a.NAME ||'样品类型' AS name,b.id as default_location_src_id FROM warehouse_view a INNER JOIN stock_location b ON b.name = a.name WHERE a.ftype = 'transfer_warehouse' and a.name ||'样品类型' not in (select name from stock_picking_type where name is not null) limit %s  """%(limit)
        cr.execute(sql)
        dicfet=cr.dictfetchall()
        if dicfet:
            x=1
            stock_picking_type_id_ins=self.env['stock.picking.type'].browse(sample_type_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x+=1
                new_stock_picking_type=stock_picking_type_id_ins.copy(default=line)
                new_stock_picking_type.write(line)
            _logger.info('======================END create sample_type template:%s=========================' % (sample_type_id))
        else:
            _logger.info('####################### 收货人样品类型导入完成，不需要继续导入了#####################################' )
            pass
    @api.model
    def _import_sample_type(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_sample_type(args[0], {}, args[2])
        else:
            pass

     ############################################# 收货人样品类型导入 结束##############################################################

    ##############################################收货人收货类型导入####################################################################
    def _copy_receive_type(self, department_id, vals, limit):
        _logger.info('======================Start create stock_picking_type template:%s=========================' % (
            department_id))
        cr = self.env.cr
        sql = """SELECT A .NAME || '收货类型' AS NAME, b. ID AS default_location_dest_id FROM warehouse_view A INNER JOIN stock_location b ON b. NAME = A . NAME WHERE A .ftype = 'transfer_warehouse' AND A . NAME || '收货类型' NOT IN ( SELECT NAME FROM stock_picking_type WHERE name is not null) limit %s""" % (limit)
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            stock_picking_type_id_ins = self.env['stock.picking.type'].browse(department_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                new_stock_picking_type = stock_picking_type_id_ins.copy(default=line)
                new_stock_picking_type.write(line)
            _logger.info('======================END create stock_picking_type template:%s=========================' % (department_id))
        else:
            _logger.info('####################### 收货人收货类型导入完成，不需要继续导入了#####################################')
            pass
    @api.model
    def _import_receive_type(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_receive_type(args[0], {}, args[2])
        else:
            pass

############################################# 收货人收货类型导入 结束##############################################################

    ##############################################收货人导入####################################################################
    def _copy_receiver(self, receiver_id, vals, limit):
        _logger.info('======================Start create receiver template:%s=========================' % (receiver_id))
        cr = self.env.cr
        sql = """SELECT A .NAME AS NAME, b. ID AS stock_location_id, C . ID AS stock_picking_type_id2, d. ID AS stock_picking_type_id FROM warehouse_view A INNER JOIN stock_location b ON b. NAME = A . NAME INNER JOIN stock_picking_type C ON C . NAME = A . NAME || '样品类型' INNER JOIN stock_picking_type d ON d. NAME = A . NAME || '收货类型' WHERE A .ftype = 'transfer_warehouse' AND A . NAME NOT IN (SELECT NAME FROM oig_receiver WHERE NAME IS NOT NULL) limit %s""" % (limit)
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            receiver_id_ins = self.env['oig.receiver'].browse(receiver_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                new_receiver = receiver_id_ins.copy(default=line)
                new_receiver.write(line)
            _logger.info('======================END create receiver template:%s=========================' % (receiver_id))
        else:
            _logger.info('####################### 收货人导入完成，不需要继续导入了#####################################')
            pass
    @api.model
    def _import_receiver(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_receiver(args[0], {}, args[2])
        else:
            pass
############################################# 收货人导入 结束##############################################################

    ##############################################快递公司导入####################################################################
    def _copy_express(self, express_id, vals, limit):
        _logger.info( '======================Start create express_id template:%s=========================' % (express_id))
        cr = self.env.cr
        sql = """select fname as name,fname as code from t_submessage where ftypeid=10014 and fname not in (select name from oig_carrier where name is not null) limit %s  """ % ( limit)
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            carrier_id_ins = self.env['oig.carrier'].browse(express_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                new_carrier = carrier_id_ins.copy(default=line)
                # line.update({'express_fg':'t'})
                new_carrier.write(line)
            _logger.info('======================END create express_id template:%s=========================' % (express_id))
        else:
            _logger.info('####################### 快递公司导入完成，不需要继续导入了#####################################')
            pass

################################快递公司
    @api.model
    def _import_express(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_express(args[0], {}, args[2])
        else:
            pass

    ##############################################快递公司导入 结束####################################################################


##############################################职员导入####################################################################
    def _copy_emp(self, emp_id, vals,limit):
        _logger.info('======================Start create emp_id template:%s========================='%(emp_id))
        cr=self.env.cr
        sql="""select fname as name,coalesce(fnumber,'') as ref from t_emp
                where fname not in (select name from res_partner where active='t' and type_flag='emp') limit %s  """%(limit)
        cr.execute(sql)
        dicfet=cr.dictfetchall()
        if dicfet:
            x=1
            emp_id_ins=self.env['res.partner'].browse(emp_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x+=1
                new_emp_id=emp_id_ins.copy(default=line)
                line.update({'type_flag':'emp'})
                new_emp_id.write(line)
            _logger.info('======================END create emp_id template:%s=========================' % (emp_id))
        else:
            _logger.info('#######################所有职员导入完成，不需要继续导入了#####################################')
            pass
    @api.model
    def _import_emp(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_emp(args[0],{},args[2])
        else:
            pass

    ##############################################职员导入 结束####################################################################

##############################################科目导入####################################################################
    def _copy_account(self, account_id, vals,limit):
        _logger.info('======================Start create account template:%s=========================' % (account_id))
        cr = self.env.cr
        sql = """SELECT a AS code,b AS NAME,c AS has_child FROM account_km
                    WHERE FLAG = FALSE order by a limit %s  """ % (limit)
        cr.execute(sql)
        dicfet = cr.dictfetchall()
        if dicfet:
            x = 1
            account_id_ins = self.env['account.account'].browse(account_id)
            for line in dicfet:
                _logger.info('======================line=================%s========' % (x))
                x += 1
                ### 判断是否已存在此科目，若存在则更新，否则新增
                account_account = self.env['account.account'].search([('code', '=', line['code'])])
                if account_account:
                    account_account.write({'name': line['name'],'has_child':line['has_child']})
                else:
                    new_account_id = account_id_ins.copy(default=line)
                ### 打标记
                sql = """UPDATE account_km SET FLAG = TRUE where a = '%s'""" % (line['code'])
                cr.execute(sql)
            _logger.info('======================END create account template:%s=========================' % (account_id))
        else:
            _logger.info('#######################所有科目导入完成，不需要继续导入了#####################################')
            pass



        ###########################第二次模板#####################################
        # _logger.info('======================Start create account template:%s========================='%(account_id))
        # cr=self.env.cr
        # sql="""SELECT d1 AS code,e1 AS NAME,g1 AS user_type,h1 AS balance_direction,
        #         case when (i1 = '是') then TRUE when (i1 = '是') then FALSE END as adjustment_fg
        #         FROM account_data1 where flag = FALSE limit %s  """%(limit)
        # cr.execute(sql)
        # dicfet=cr.dictfetchall()
        # if dicfet:
        #     x=1
        #     account_id_ins=self.env['account.account'].browse(account_id)
        #     for line in dicfet:
        #         _logger.info('======================line=================%s========' % (x))
        #         x+=1
        #         ### 查询 account_account_type 表中对应类型的id
        #         user_type_id = self.env['account.account.type'].search([('name','=',line['user_type'])]).id
        #         line['user_type_id'] = user_type_id
        #         ### 判断是否已存在此科目，若存在则更新，否则新增
        #         account_account = self.env['account.account'].search([('code', '=', line['code'])])
        #         if account_account:
        #             account_account.write({'name':line['name'],'user_type_id':line['user_type_id'],'balance_direction':line['balance_direction'],'adjustment_fg':line['adjustment_fg']})
        #         else:
        #             new_account_id=account_id_ins.copy(default=line)
        #         ### 打标记
        #         sql = """UPDATE account_data1 SET FLAG = TRUE where d1 = '%s'""" %(line['code'])
        #         cr.execute(sql)
        #     _logger.info('======================END create account template:%s=========================' % (account_id))
        # else:
        #     _logger.info('#######################所有科目导入完成，不需要继续导入了#####################################')
        #     pass



        ########################第一次模板，导入t_account表的代码###############
        # _logger.info('======================Start create account template:%s=========================' % (account_id))
        # cr = self.env.cr
        # sql = """select fname as name,fnumber as code from account_view where fnumber not in (select code from account_account) limit %s  """ % (
        # limit)
        # cr.execute(sql)
        # dicfet = cr.dictfetchall()
        # if dicfet:
        #     x = 1
        #     account_id_ins = self.env['account.account'].browse(account_id)
        #     for line in dicfet:
        #         _logger.info('======================line=================%s========' % (x))
        #         x += 1
        #         new_account_id = account_id_ins.copy(default=line)
        #         # new_account_id.write(line)
        #     _logger.info('======================END create account template:%s=========================' % (account_id))
        # else:
        #     _logger.info('#######################所有科目导入完成，不需要继续导入了#####################################')
        #     pass
    @api.model
    def _import_account(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_account(args[0],{},args[2])
        else:
             pass

    def handle_arrive(self):
        sql="""            --------批量插入:到货单插入脚本：
            INSERT INTO public.goods_arriving(
            create_date, origin_id, package_count, write_uid, actual_weight,
            create_uid, acourier_company, date_over, state, freight_person,
            arriving_area, full_attribute, real_package_count, write_date,
            freight_cost, remark, name, arrive_date, acourier_number, financial_change_fg)
            with res as (select row_number() over(PARTITION BY purchase_order_id order by purchase_order_line_id ),* from po_handle where flag='f' and stock_qty<>0)
            select a.date_order as create_date,a.name as origin_id,b.count as package_count,1 as write_uid,1 as actual_weight,
            1 as create_uid,1953 as acourier_company,null as date_over,'check_financial' as state,'other' as freight_person,'期初' as arriving_area ,
            'intact' as full_attribute,b.count as real_package_count,now() as  write_date , 0 as freight_cost ,'期初' as remark ,'期初' as name,
            now() as arrive_date , '期初快递单号' asacourier_number,False as financial_change_fg   from purchase_order a
            inner join res b on b.purchase_order_id=a.id and b.flag='f' and b.row_number=1;

            --------批量插入:到货明细插入
            INSERT INTO public.goods_arriving_real_line(
             financial_fg, create_date, po_id, qty, create_uid, over_reason,
            plan_qty, out_qty, financial_number, now_in_qty, pack_qty, wrap_criteria,
            state, order_id, has_in_count, transit_location, write_date,
            big_package_id, write_uid, qa_qty, product_id, has_in_bad_count,
            inventory_qty)
            select False as financial_fg, now() as create_date,a.purchase_order_id as po_id,a.stock_qty as qty,1 as create_uid,
            null as over_reason,a.stock_qty as plan_qty,0 as out_qty,null as financial_number,0 as now_in_qty,0 as pack_qty,
            null as wrap_criteria,'check_financial' as state,b.id as order_id,0 as has_in_count, d.transit_location as transit_location,
            now() as write_date,null as big_package_id,1 as write_uid,0 as qa_qty,c.product_id as product_id,0 as has_in_bad_count,
            0 as has_in_bad_count
            from po_handle a
            inner join goods_arriving b on b.origin_id=a.fbillno and b.name='期初'
            inner join purchase_order_line c on c.id=a.purchase_order_line_id
            inner join purchase_order d on d.id=a.purchase_order_id
            where a.flag='f' and  a.stock_qty<>0;
            ----批量插入:到货明细包裹表插入脚本
            INSERT INTO public.goods_arriving_real_line_package(
                        create_uid, area_id, create_date, weight, box_count, has_in_package_count,
                        has_in_package_id, box_type, write_uid, volume, width, length,
                        has_in_package_qty, real_line_id, write_date, box_qty, height,
                        location_id)
            select 1 as create_uid,1 as area_id,now() as create_date,1 as weight,1 as box_count,0 as has_in_package_count,
            null as has_in_package_id,1 as box_type,1 as write_uid,1 as volume,1 as width,1 as length,
            0 as has_in_package_qty,d.id as real_line_id,now() as write_date,a.stock_qty as box_qty,1 as height,null as location_id
            from po_handle a
            inner join goods_arriving b on b.origin_id=a.fbillno and b.name='期初'
            inner join purchase_order_line c on c.id=a.purchase_order_line_id
            inner join goods_arriving_real_line d on d.product_id=c.product_id and d.order_id=b.id
            where a.flag='f' and  a.stock_qty<>0;

            ----批量插入:采购明细插入
            INSERT INTO public.goods_arriving_plan_line(
                         financial_fg, create_date, po_id, qty, bad_qty, create_uid,
                        state, financial_number, real_line_id, order_id, old_qty, write_date,
                        write_uid, po_line_id, product_id)
            select False as financial_fg,now() as create_date,a.purchase_order_id as po_id, a.stock_qty as qty,0 as bad_qty,1 as create_uid, 'check_financial' as state,
            null as financial_number,d.id as real_line_id,b.id as order_id,c.product_qty as old_qty,now() as write_date,1 as write_uid,
            c.id as po_line_id,c.product_id as product_id
            from po_handle a
            inner join goods_arriving b on b.origin_id=a.fbillno and b.name='期初'
            inner join purchase_order_line c on c.id=a.purchase_order_line_id
            inner join goods_arriving_real_line d on d.product_id=c.product_id and d.order_id=b.id
            where a.flag='f' and a.stock_qty<>0;
           ----------更改标记
           update po_handle set Flag=True where Flag=False;"""
        self.env.cr.execute(sql)
        ##############到货单金融确认
        sql="""select id from goods_arriving where name='期初'and state='check_financial'"""
        self.env.cr.execute(sql)
        fet=self.env.cr.fetchall()
        if fet:
            for need_id in fet:
                #try:
                self.env['goods.arriving'].browse(need_id[0]).button_12()
                ###########查询po
                sql="""select id from purchase_order where name='%s'"""%(self.env['goods.arriving'].browse(need_id[0]).origin_id)
                self.env.cr.execute(sql)
                fet=self.env.cr.fetchone()
                if fet:
                    purchase_obj=self.env['purchase.order'].browse(fet[0])
                    picking_ids=purchase_obj.picking_ids
                    for obj in picking_ids:
                        if obj.location_id.id<>8 or obj.state<>'done':
                            continue
                        return_obj=self.with_context(active_id=obj.id,active_ids=[obj.id]).env['stock.return.picking']
                        self.env['stock.picking'].browse(return_obj.create({}).create_returns()['res_id']).do_transfer()
                #except Exception, e:
                    # sql="""insert into goods_bad(goods_arriving_id,error) values (%s,'%s')"""%(need_id[0],'')
                    # self.env.cr.execute(sql)
        sql=""" update goods_arriving set name=name||origin_id where name='期初' and origin_id is not null"""
        self.env.cr.execute(sql)
        return True
    def handle_platform(self, purchase_instance):
        sql="""with res as (
                select a.id as purchase_platform_id,b.id as purchase_order_line_id,b.product_qty,a.total,sum(a.total) over(PARTITION BY b.id), cast(a.total as float)/sum(a.total) over(PARTITION BY b.id) as rate,
                b.product_qty- sum(a.total) over(PARTITION BY b.id) as cy  from purchase_platform a
                inner join purchase_order_line b on b.id=a.po_line_number
                inner join purchase_order c on c.id=b.order_id
                where c.id=%s ),
                res2 as (
                select row_number() over(PARTITION BY purchase_order_line_id order by purchase_platform_id desc), res.*,round(rate*cy) as add_qty, round(rate*cy)+total as new_qty,sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) as newsum,
                res.product_qty-sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) newcy from res  where rate<>1 )
                update purchase_platform set total=res2.new_qty from res2 where res2.purchase_platform_id=purchase_platform.id;
                with res as (
                select a.id as purchase_platform_id,b.id as purchase_order_line_id,b.product_qty,a.total,sum(a.total) over(PARTITION BY b.id), cast(a.total as float)/sum(a.total) over(PARTITION BY b.id) as rate,
                b.product_qty- sum(a.total) over(PARTITION BY b.id) as cy  from purchase_platform a
                inner join purchase_order_line b on b.id=a.po_line_number
                inner join purchase_order c on c.id=b.order_id
                where c.id=%s ),
                res2 as (
                select row_number() over(PARTITION BY purchase_order_line_id order by purchase_platform_id desc), res.*,round(rate*cy) as add_qty, round(rate*cy)+total as new_qty,sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) as newsum,
                res.product_qty-sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) newcy from res  where rate<>1 )
                --select * from res2
                update purchase_platform set total=res2.new_qty+res2.newcy from res2 where res2.purchase_platform_id=purchase_platform.id and res2.row_number=1;"""%(purchase_instance.id,purchase_instance.id)
        cr=self.env.cr
        cr.execute(sql)
        return True



    def handle_stock(self, purchase_instance, stock_list):
        #####create table po_handle(fbillno varchar,purchase_order_id int,purchase_order_line_id int,stock_qty int,account_qty int,count int,flag boolean)  ;
        if not stock_list:
            return   False
        sql=''
        cr=self.env.cr
        count=0
        for line in stock_list:
            if line[1]<>0:
                count+=1
        for line in stock_list:
            sql+="""insert into po_handle(fbillno,purchase_order_id,purchase_order_line_id,stock_qty,account_qty,count,flag)
                    select a.name as fbillno,a.id as purchase_order_id,b.id as purchase_order_line_id,%s as stock_qty,%s as account_qty,%s as count,False as flag from purchase_order a
                    inner join purchase_order_line b on b.order_id=a.id and b.product_id=%s
                    where a.id=%s;
                    """%(line[1],line[2],count,line[0],purchase_instance.id)
        if sql:
            cr.execute(sql)
        return True

    def handle_account(self, purchase_instance, account_list):
        ##########调用手工创建付款单的方法
        if  purchase_instance.invoice_ids:
            invoice_instance = purchase_instance.invoice_ids[0]
        else:
            invoice_instance=purchase_instance.create_account_invoice()
        ####找到生成的付款单
        if not purchase_instance.invoice_ids:
            return False
        if not invoice_instance:
            invoice_instance=purchase_instance.invoice_ids[0]
        ##########修改金额
        # price_subtotal,price_subtotal_signed,payment_amount,quantity,bili
        # price_unit
        cr = self.env.cr
        for line in account_list:
            sql = """select a.id, a.price_unit,b.product_qty from account_invoice_line a
                    inner join purchase_order_line b on b.id=a.purchase_line_id  where a.invoice_id=%s and a.product_id=%s limit 1""" % (invoice_instance.id,line[0])
            cr.execute(sql)
            fet = cr.fetchone()
            if not fet:
                continue
            account_invoice_line_id, price_unit, qty = fet
            account_qty = line[2]
            amount = round(price_unit * account_qty, 2)
            rate = str(round(float(account_qty) / qty, 4) * 100) + '%'
            dic = {'price_subtotal': amount, 'price_subtotal_signed': amount, 'payment_amount': amount,
                   'quantity': account_qty, 'bili': rate}
            if account_qty:
                self.env['account.invoice.line'].browse(account_invoice_line_id).write(dic)
            else:
                self.env['account.invoice.line'].browse(account_invoice_line_id).unlink()
        if len(invoice_instance.invoice_line_ids)==0:
            invoice_instance.unlink()
            return True
        #########直接更新付款单状态为待验证状态
        invoice_instance.write({'state':'proforma2'})
        invoice_instance.action_invoice_open()
        #########直接更新付款单状态为
        invoice_instance.write({'state': 'paid'})
        account_move_instance=invoice_instance.move_id
        ###########红冲凭证
        reverse_moves_ids=account_move_instance.reverse_moves(account_move_instance.date, account_move_instance.journal_id or False)
        ###########红冲凭证与源凭证做关联
        if reverse_moves_ids:
            reverse_id=reverse_moves_ids[0]
            self.env['account.move'].browse(reverse_id).write({'ref':'期初红冲：%s'%(account_move_instance.name)})
            account_move_instance.write({'ref':'期初：%s'%(purchase_instance.name)})
            #########数据库做记录
            sql="""insert into account_move_log(account_move_id,account_move_id_reverse) values (%s, %s)"""%(account_move_instance.id,reverse_id)
            self.env.cr.execute(sql)

        ###########核销
        sql="""select id from account_move_line where move_id in (%s,%s) and account_id in (select id from account_account where code='2202')"""%(account_move_instance.id,reverse_id)
        cr.execute(sql)
        fet=cr.fetchall()
        if fet:
            move_ids=[x[0] for x  in fet]
            move_lines = self.env['account.move.line'].browse(move_ids)
            currency = False
            for aml in move_lines:
                if not currency and aml.currency_id.id:
                    currency = aml.currency_id.id
                elif aml.currency_id:
                    if aml.currency_id.id == currency:
                        continue
                    #raise UserError(_('Operation not allowed. You can only reconcile entries that share the same secondary currency or that don\'t have one. Edit your journal items or make another selection before proceeding any further.'))
            #Don't consider entrires that are already reconciled
            move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
            #Because we are making a full reconcilition in batch, we need to consider use cases as defined in the test test_manual_reconcile_wizard_opw678153
            #So we force the reconciliation in company currency only at first
            move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_excluded', manual_full_reconcile_currency=currency).reconcile()

            #then in second pass the amounts in secondary currency, only if some lines are still not fully reconciled
            move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
            if move_lines_filtered:
                move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_only', manual_full_reconcile_currency=currency).reconcile()
            move_lines.compute_full_after_batch_reconcile()
        ###########################################
        return True

    ##############################################采购单导入####################################################################
    def _copy_purchase(self, purchase_id, vals, limit):
        _logger.info( '======================Start create purchase template:%s=========================' % (purchase_id))
        cr = self.env.cr
        # 检查记录表是否存在
        sql = """select 1 from information_schema.columns where table_name = 'po_ok'"""
        cr.execute(sql)
        # 没有则新建表
        if not cr.fetchone():
            sql = """create table po_ok(id int,purchase_order_id int); """
            cr.execute(sql)
        ############################基础数据视图######################################
        # sql = """select distinct fbillno from po_view where fbillno not in (select purchase_interid from purchase_order where purchase_interid is not null)  limit %s  """ % (limit)
        sql = """select distinct fbillno from po_view limit %s  """ % (limit)
        cr.execute(sql)
        dicfet1 = cr.fetchall() or []
        x = 1
        purchase_ins = self.env['purchase.order'].browse(purchase_id)
        for line in dicfet1:
            _logger.info('======================line=================%s========' % (x))
            x += 1
            sql="""select a.* from po_view  a where a.fbillno='%s'"""%(line)
            cr.execute(sql)
            dicfet = cr.dictfetchall() or []
            if not dicfet:
                continue
            dic=dicfet[0].copy()
            dic['order_line']=[]
            #------获取收货人 - -----------
            sql="""select b.FTransType, b.FName from  PurchaseRequisitions as a
            inner join MultiDestinationWarehouse as b on  a.FWareHouse = b.FName
            inner join MultiDestinationWarehouse as c on c.FWarehouseId = b.FDStockId
            where FPOORD = '%s' limit  1"""%(line[0])
            cr.execute(sql)
            fet=cr.fetchone()
            if not fet:
                sql = """select min(id) from oig_receiver"""
                cr.execute(sql)
                transit_location=cr.fetchone()[0]
            else:
                sql="""select id from oig_receiver where name='%s'"""%(fet[1],)
                cr.execute(sql)
                fet=cr.fetchone()
                if fet:
                    transit_location=fet[0]
                else:
                    sql = """select min(id) from oig_receiver"""
                    cr.execute(sql)
                    transit_location = cr.fetchone()[0]
            dic['oig_receiver_id']=transit_location
            dic['name'] = line[0]+'%s' %('_01')
            dic['pay_way'] = 'all'
            dic['notes']='K3 PO:'+str(line[0])
            # 生成采购单主表
            new_account_id = purchase_ins.copy(default=dic)
            # 判断是否有半完成采购单，若有则生成第二张采购单主表
            num_flag = 0
            for each3 in dicfet:
                if each3['入库数量'] > 0:
                    num_flag += 1
            if num_flag > 0:
                dic['name'] = line[0]+'%s' %('_02')
                dic['purchase_interid'] = line[0]+'%s' %('_02')
                new_account_id2 = purchase_ins.copy(default=dic)
            # 生成采购订单明细
            for each in dicfet:
                line_list = []
                line_list2 = []
                each['lock']=str(each['fmrpclosed'])
                ##########################部分入库的单据 采购数量填总数
                product_qty=each['product_qty']
                account_qty=each['开票数量']
                stock_qty = each['入库数量']
                unstock_qty = product_qty - stock_qty
                ############################## 判断采购订单为半完成状态 还是未完成######################
                if stock_qty == 0:
                    each.update({'product_qty':product_qty,'product_uom':1,'date_planned':each['date_planned'],'purchase_date_planned':each['date_planned'],'purchase_interid':each['purchase_interid']})
                    ################将开票数量 到货数量 写在备注里
                    remark='期初采购单：开票数量（%s）到货数量(%s)'%(account_qty,stock_qty)
                    each.update({'name':remark})
                    ##########################################追加平台信息表#####################################
                    sql="""--------------平台分配
                        SELECT SUM (A .quantity) AS qty,SUM (A .quantity) AS qty_origin,C . ID AS sys_platform_id FROM ar_sub_group_receipt_0602 AS A
                        INNER JOIN multidestinationwarehouse d ON a.warehousein = d.fwarehouseid
                        INNER JOIN (SELECT subGroupId,platforms,warehouseid,ID FROM ar_sub_group) AS b ON A .subGroupId = b.subGroupId and b.warehouseid = d.fdstockid
                        INNER JOIN sys_platform C ON C .pid = b. ID WHERE stage = '采购阶段'
                        AND billno = '%s' AND sku = '%s' GROUP BY b.platforms,C . ID"""%(line[0],each['product_code'])
                    cr.execute(sql)
                    platform_fet=cr.dictfetchall()
                    platform_line1=[]
                    for platform_line in platform_fet:
                        platform_line1.append((0,0,platform_line))
                    if not platform_line1:
                        sql="""select id from sys_platform where name='ebay+US+53623'"""
                        cr.execute(sql)
                        ebay=cr.fetchone()[0]
                        platform_line1=[(0,0,{'qty':product_qty,'qty_origin':product_qty,'sys_platform_id':ebay})]
                    each.update({'platform_line':platform_line1})
                    # each['platform_line'] = platform_line1
                    #########################################检查供应商交货期 没有的话就给该产品添加#############
                    sql="""select id from product_supplierinfo where name=%s and product_id=%s"""%(each['partner_id'],each['product_id'])
                    cr.execute(sql)
                    fet=cr.fetchone()
                    if not fet:
                        sql="""SELECT b.fdelivery AS delivery_days, c.id as product_tmpl_id ,c.name as product_name FROM CreateCodeChildCode A
                                INNER JOIN CreateCodeDefaultField b ON b.fcodetype = '子代码' INNER JOIN product_template c on c.default_code = a.fcode
                                AND A .FID = b.FInterID WHERE A .fcode = '%s' LIMIT 1; """%(each['product_code'])
                        cr.execute(sql)
                        delivery_days=cr.dictfetchone()
                        delay=delivery_days['delivery_days'] or 0
                        self.env['product.supplierinfo'].create({'product_id':delivery_days['product_tmpl_id'],'name':each['partner_id'],'delay':delay,'product_code':each['product_code'],'product_name':delivery_days['product_name']})
                    line_list.append( (0,0,each) )
                    new_account_id.write({'order_line':line_list})
                    # new_account_id.order_line.write({'platform_line':platform_line1})
                else:
                    #######################将半完成状态的采购订单拆分为：已完成和未完成的两张单########################

                    ######已完成部分
                    each2 = each.copy()
                    each2.update({'product_qty': stock_qty, 'product_uom': 1, 'date_planned':each['date_planned'],'purchase_date_planned':each['date_planned'],'purchase_interid':each['purchase_interid']})
                    ################将开票数量 到货数量 写在备注里
                    remark2 = '期初采购单：开票数量（%s）到货数量(%s)' % (account_qty, stock_qty)
                    each2.update({'name': remark2})
                    line_list2.append((0, 0, each2))
                    new_account_id2.write({'order_line': line_list2})
                    new_account_id2.update({'state':'done'})

                    ######未完成部分
                    each.update({'product_qty': unstock_qty, 'product_uom': 1, 'date_planned':each['date_planned'],'purchase_date_planned':each['date_planned'],'purchase_interid':each['purchase_interid']})
                    ################将开票数量 到货数量 写在备注里
                    remark = '期初采购单：开票数量（%s）到货数量(%s),已完成部分为采购单（%s）' % (0, 0,new_account_id2.name)
                    each.update({'name': remark})
                    #         #########################################追加平台信息表#####################################
                    sql = """--------------平台分配
                            SELECT SUM (A .quantity) AS qty,SUM (A .quantity) AS qty_origin,C . ID AS sys_platform_id FROM ar_sub_group_receipt_0602 AS A
                            INNER JOIN multidestinationwarehouse d ON a.warehousein = d.fwarehouseid
                            INNER JOIN (SELECT subGroupId,platforms,warehouseid,ID FROM ar_sub_group) AS b ON A .subGroupId = b.subGroupId and b.warehouseid = d.fdstockid
                            INNER JOIN sys_platform C ON C .pid = b. ID WHERE stage = '采购阶段'
                            AND billno = '%s' AND sku = '%s' GROUP BY b.platforms,C . ID""" %(line[0], each['product_code'])
                    cr.execute(sql)
                    platform_fet = cr.dictfetchall()
                    platform_line1 = []
                    # ###计算小组库存总数
                    # total_qty = 0.0
                    # for platform_line in platform_fet:
                    #     total_qty += platform_line['qty']
                    # ####按原比例进行对现在的数量进行分组
                    # num = unstock_qty
                    # for platform_line in platform_fet:
                    #     min_qty = min(math.ceil((platform_line['qty']/total_qty)*unstock_qty),num)
                    #     num = num - math.ceil((platform_line['qty'] / total_qty)*unstock_qty)
                    #     # 将重新计算出的小组数量分配给各小组
                    #     platform_line['qty'] = min_qty
                    #     platform_line['qty_origin'] = min_qty
                    #     platform_line1.append((0, 0, platform_line))
                    #     if num <= 0:
                    #         break
                    for platform_line in platform_fet:
                        platform_line1.append((0,0,platform_line))
                    ###若没有查询到小组比例，默认设定一个小组
                    if not platform_line1:
                        sql = """select id from sys_platform where name='ebay+US+53623'"""
                        cr.execute(sql)
                        ebay = cr.fetchone()[0]
                        platform_line1 = [(0, 0, {'qty': unstock_qty, 'qty_origin': unstock_qty, 'sys_platform_id': ebay})]
                    each['platform_line'] = platform_line1
                    #########################################检查供应商交货期 没有的话就给该产品添加#############
                    sql = """select id from product_supplierinfo where name=%s and product_id=%s""" % (
                    each['partner_id'], each['product_id'])
                    cr.execute(sql)
                    fet = cr.fetchone()
                    if not fet:
                        sql = """SELECT b.fdelivery AS delivery_days, c.id as product_tmpl_id ,c.name as product_name FROM CreateCodeChildCode A
                                                    INNER JOIN CreateCodeDefaultField b ON b.fcodetype = '子代码' INNER JOIN product_template c on c.default_code = a.fcode
                                                    AND A .FID = b.FInterID WHERE A .fcode = '%s' LIMIT 1; """ % (
                        each['product_code'])
                        cr.execute(sql)
                        delivery_days = cr.dictfetchone()
                        delay = delivery_days['delivery_days'] or 0
                        self.env['product.supplierinfo'].create(
                            {'product_id': delivery_days['product_tmpl_id'], 'name': each['partner_id'], 'delay': delay,
                             'product_code': each['product_code'], 'product_name': delivery_days['product_name']})
                    # each['platform_line']=platform_line1
                    line_list.append((0, 0, each))
                    new_account_id.write({'order_line': line_list})
                    #new_account_id.order_line.write({'platform_line': platform_line1})


        #     #######处理平台数据 使得总数量正确
        #     self.handle_platform(new_account_id)
            ###########采购订单确认
            # new_account_id.button_confirm()
        #     ############封装方法处理财务 库存问题
        #     self.handle_stock(new_account_id, stock_waitting_list)
        #     self.handle_account(new_account_id,stock_waitting_list)
            sql="""insert into po_ok(id,purchase_order_id) values (%s,%s)"""%(int(each['finterid']),new_account_id.id)
            cr.execute(sql)
        # #批量生成到货单并确认
        # self.handle_arrive()
        _logger.info( '======================END create purchase_id template:%s=========================' % (purchase_id))
        return True

    @api.model
    def _import_purchase(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_purchase(args[0], {}, args[2])
        else:
            pass
############################execute#############################################
    @api.multi
    def action_execute(self):
        sql=self.description_sale
        ACTION_DICT= {
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'base.module.upgrade',
                        'target': 'new',
                        'type': 'ir.actions.act_window',

                    }
        from odoo import api, fields, models, modules, tools, _
        return dict(ACTION_DICT, name=_('Apply Schedule Upgrade'))
        if sql:
            self.env.cr.execute(sql)
            import uuid
            from odoo.addons.web.controllers.main import ExportData2Excel as _Export
            _name = str(uuid.uuid1())
            _command = """select name from res_partner limit 10"""
            _field = [u'物料编码']
            _file_name =  u'销售预测单_'
            _Export.do(self, self.env.cr, 1, _name, _file_name, _command, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}

        else:
            pass
#######################################0527  库存相关数据同步##########################################################

    ########封装 盘点的创建方法
    def create_inventory(self, vals_inv_create,vals_inv_write):
        pack_obj=self.env['stock.inventory']
        ######源生字段
                # name 盘点单号 location_id:仓库主库位 date:盘点时间 filter 类型 package_id 包裹ID
        #vals = {'name': 'ABCD', 'location_id':15,'date':'2017-05-27','filter':'pack','package_id':7}
        ######新加字段
                #inventory_type:盘点类型
        # vals_inv_create.update({'inventory_type':'begin'})
        # vals_line=[]
        # for line in range(1):
        #     #######源生字段
        #     each={'product_id': 51058, 'location_id':15,'package_id':7,'product_qty':15}
        #     vals_line.append((0, 0, each))
        #####创建盘点单
        new_obj=pack_obj.create(vals_inv_create)
        #####确认盘点单
        new_obj.prepare_inventory()
        #####写盘点单明细
        new_obj.write(vals_inv_write)
        # 获取实际数量
        dict = {}
        for line in vals_inv_write['line_ids']:
            product_id = line[2]['product_id']
            product_qty = line[2]['product_qty']
            dict.update({product_id:product_qty})
        ##将实际数量改为理论数量+实际数量
        line_ids = new_obj.line_ids
        for line_id in line_ids:
            theoretical_qty = line_id.theoretical_qty
            real_qty = dict.get(line_id.product_id.id)
            qty = real_qty + theoretical_qty
            line_id.update({'product_qty': qty})

        #####完成盘点单
        new_obj.action_done()
        return new_obj.id
    ##########取得默认参数
    def get_parm(self):
        cr = self.env.cr
        ##############################默认参数获取
        sql = """select lot_stock_id from stock_warehouse where id=1"""
        cr.execute(sql)
        fet = cr.fetchall()
        # 默认中转仓库位
        template_location_id = fet[0][0]
        # 默认平台
        sql = """select id from res_partner where ref='ebay' limit 1"""
        cr.execute(sql)
        fet = cr.fetchall()
        #默认移动仓库位
        template_partner_id = fet[0][0]
        sql="""select lot_stock_id from stock_warehouse where name='移动仓'"""
        cr.execute(sql)
        fet=cr.fetchall()
        template_location_id2=fet[0][0]
        return (template_location_id,template_partner_id,template_location_id2)
    ########打标记
    def get_flag(self,keys):
        sql="""insert into stock_flag(k3_stock_id,ar_sub_group_warehouse_daily_closing_id,OutWarehouseDetail_id,ar_sub_group_receipt_id,new_id,type,remark)
            values(%s,%s,%s,%s,%s,'%s','%s')"""%(keys[0] or 0,keys[1] or 0,keys[2] or 0,keys[3] or 0,keys[4] or 0,keys[5],keys[6])
        self.env.cr.execute(sql)
        return True

    ########计算小组库存(中转仓,目的仓)
    def group_stock(self,new_id):
        cr = self.env.cr
        stock_inventory = self.env['stock.inventory'].browse(new_id)
        stock_moves = stock_inventory.move_ids
        if stock_moves:
            for stock_move in stock_moves:
                stock_quants = stock_move.quant_ids
                if stock_quants:
                    for stock_quant in stock_quants:
                        stock_quant_id = stock_quant.id
                        # 删除验证时自动生成的小组库存
                        sql = 'delete from stock_quant_platform where stock_quant_id = %s' % (stock_quant_id)
                        cr.execute(sql)
                        # 查询出对应的小组库存
                        sql = """SELECT a.quantity as qty,c.id as stock_location_id,d.id as product_id,f.id as sys_platform_id
                                                                FROM ar_sub_group_warehouse_daily_closing_0602 a
                                                                INNER JOIN multidestinationwarehouse b ON a.warehouseid = b.fwarehouseid
                                                                INNER JOIN stock_location c ON c.name = b.fname
                                                                INNER JOIN product_product d ON d.default_code = a.sku 
                                                                INNER JOIN ar_sub_group e ON e.subgroupid = a.subgroupid AND e.warehouseid = b.fdstockid
                                                                INNER JOIN sys_platform f ON f.pid = e.id
                                                                where a.quantity <> 0 and a.sku = '%s' and c.id = '%s'
                                                            """ % (stock_quant.product_id.default_code, stock_quant.location_id.id)
                        cr.execute(sql)
                        stock_quant_platforms = cr.dictfetchall()
                        if stock_quant_platforms:
                            for stock_quant_platform in stock_quant_platforms:
                                stock_quant_platform['stock_quant_id'] = stock_quant_id
                                # 创建小组库存数据
                                self.env['stock.quant.platform'].create(stock_quant_platform)
                        else:
                            sql = """select min(id) as sys_platform_id from sys_platform where warehouseid = '%s'""" % (stock_quant.location_id.origin_id)
                            cr.execute(sql)
                            stock_quant_platform = cr.dictfetchone()
                            if not stock_quant_platform['sys_platform_id']:
                                sql = 'select min(id) as sys_platform_id from sys_platform'
                                cr.execute(sql)
                                stock_quant_platform = cr.dictfetchone()

                            stock_quant_platform['stock_quant_id'] = stock_quant_id
                            stock_quant_platform['qty'] = stock_quant.qty
                            # 创建小组库存数据
                            self.env['stock.quant.platform'].create(stock_quant_platform)
        return True

    ########计算小组库存(移动仓)
    def group_stock_move(self,new_id):
        cr = self.env.cr
        stock_inventory = self.env['stock.inventory'].browse(new_id)
        stock_moves = stock_inventory.move_ids
        if stock_moves:
            for stock_move in stock_moves:
                stock_quants = stock_move.quant_ids
                if stock_quants:
                    for stock_quant in stock_quants:
                        stock_quant_id = stock_quant.id
                        # 删除验证时自动生成的小组库存
                        sql = 'delete from stock_quant_platform where stock_quant_id = %s' % (stock_quant_id)
                        cr.execute(sql)
                        # 查询出对应的小组库存
                        sql = """select quantity as qty,sys_platform_id from stock_move_view_copy where sku = '%s' and location_id = %s and date = '%s'""" % (
                        stock_quant.product_id.default_code, stock_quant.location_id.id, stock_inventory.date)
                        cr.execute(sql)
                        stock_quant_platforms = cr.dictfetchall()
                        if stock_quant_platforms:
                            for stock_quant_platform in stock_quant_platforms:
                                stock_quant_platform['stock_quant_id'] = stock_quant_id
                                # 创建小组库存数据
                                self.env['stock.quant.platform'].create(stock_quant_platform)
        return True


    ##########中转仓 数据来源 K3_stock_ar && ar_sub_group_warehouse_daily_closing_0602 && OutWarehouseDetail
    @api.model
    def _import_stock(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action !='create':
            return True
        cr=self.env.cr
       #####################中转仓#########################################
        k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,remark=(0,0,0,0,0,'zhongzhuan')
        #################### NO.1 取得K3中转仓库存
        sql = """select * from stock_transfer_view_copy where flag = false order by location_id, product_id limit %s""" % limit
        cr.execute(sql)
        k3_fet = cr.dictfetchall()
        if k3_fet:
            x = 1
            vals_inv_create = {}
            vals_line = []
            location_id = None
            for line in k3_fet:
                _logger.info('======================line======transfer===========%s========' % (x))
                x += 1
                k3_stock_id = line['id']
                if location_id is None:
                    location_id = line['location_id']
                    vals_inv_create = {'name': 'transfer', 'location_id': line['location_id'], 'date': '2018-04-12',
                                       'filter': 'partial'}
                if location_id == line['location_id']:
                    each = {'product_id': line['product_id'], 'location_id': line['location_id'],
                            'product_qty': line['quantity']}
                    vals_line.append((0, 0, each))
                else:
                    vals_inv_write = {'line_ids': vals_line, 'date': '2018-04-12'}
                    #########创建盘点单
                    new_id = self.create_inventory(vals_inv_create, vals_inv_write)
                    vals_inv_create = {}
                    vals_line = []
                    # 计算小组库存
                    self.group_stock(new_id)
                    # 若仓库ID变化，则重新生成调整单
                    location_id = line['location_id']
                    vals_inv_create = {'name': 'transfer', 'location_id': line['location_id'], 'date': '2018-04-12',
                                       'filter': 'partial'}
                    each = {'product_id': line['product_id'], 'location_id': line['location_id'],
                            'product_qty': line['quantity']}
                    vals_line.append((0, 0, each))

                # 将所需数据插入新建表中
                keys = (
                    k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                    ar_sub_group_receipt_id, new_id, 'inventory', remark)
                self.get_flag(keys)
                ##################打标记
                sql = """update stock_transfer_view_copy set flag=True where wh_code='%s' and prod_code='%s'""" % (
                line['wh_code'], line['prod_code'])
                cr.execute(sql)

            # 最后一种仓库生产盘点单，并计算小组库存
            vals_inv_write = {'line_ids': vals_line, 'date': '2018-04-12'}
            #########创建盘点单
            new_id2 = self.create_inventory(vals_inv_create, vals_inv_write)
            # 计算小组库存
            self.group_stock(new_id2)






















        #################################################################################################
         #################### NO.1 取得K3中转仓库存
        # sql = """SELECT A .ID,A .prod_code,A .wh_code,A .quantity,A .COST,b.ftranstype,b.fname,b.fdstockid,C.ID AS product_id, d.id AS location_id FROM K3_Stock_ar A INNER JOIN MultiDestinationWarehouse b ON b.fwarehouseid = CAST (A .wh_code AS INT)
        #                 INNER JOIN product_product C ON C .default_code = A .prod_code INNER JOIN stock_location d ON d. NAME = b.fname WHERE A .quantity <> 0 AND A . FLAG = 'f' AND b.ftype = '中转仓'
        #               limit %s""" % limit
        # cr.execute(sql)
        # k3_fet = cr.dictfetchall()
        # if k3_fet:
        #     x=1
        #     for line in k3_fet:
        #         _logger.info('======================line===transfer==============%s========' % (x))
        #         x += 1
        #         k3_stock_id = line['id']
        #         vals_inv_create = {'name': 'transfer', 'location_id': line['location_id'], 'date': '2018-03-21',
        #                            'filter': 'partial'}
        #         each = {'product_id': line['product_id'], 'location_id': line['location_id'],
        #                 'product_qty': line['quantity']}
        #         vals_line = [(0, 0, each)]
        #         vals_inv_write = {'line_ids': vals_line, 'date': '2018-03-21', }
        #         #########创建盘点单
        #         new_id = self.create_inventory(vals_inv_create, vals_inv_write)
        #         stock_inventory = self.env['stock.inventory'].browse(new_id)
        #         stock_moves = stock_inventory.move_ids
        #         if stock_moves:
        #             for stock_move in stock_moves:
        #                 stock_quants = stock_move.quant_ids
        #                 if stock_quants:
        #                     for stock_quant in stock_quants:
        #                         stock_quant_id = stock_quant.id
        #                         # 删除验证时自动生成的小组库存
        #                         sql = 'delete from stock_quant_platform where stock_quant_id = %s' % (stock_quant_id)
        #                         cr.execute(sql)
        #                         # 查询出对应的小组库存
        #                         sql = """SELECT a.quantity as qty,c.id as stock_location_id,d.id as product_id,f.id as sys_platform_id
        #                                 FROM ar_sub_group_warehouse_daily_closing_0602 a
        #                                 INNER JOIN multidestinationwarehouse b ON a.warehouseid = b.fwarehouseid
        #                                 INNER JOIN stock_location c ON c.name = b.fname
        #                                 INNER JOIN product_product d ON d.default_code = a.sku
        #                                 INNER JOIN ar_sub_group e ON e.subgroupid = a.subgroupid AND e.warehouseid = b.fdstockid
        #                                 INNER JOIN sys_platform f ON f.pid = e.id
        #                                 where a.quantity <> 0 and a.sku = '%s' and c.id = '%s'
        #                             """%(line['prod_code'],line['location_id'])
        #                         cr.execute(sql)
        #                         stock_quant_platforms = cr.dictfetchall()
        #                         if stock_quant_platforms:
        #                             for stock_quant_platform in stock_quant_platforms:
        #                                 stock_quant_platform['stock_quant_id'] = stock_quant_id
        #                                 # 创建小组库存数据
        #                                 self.env['stock.quant.platform'].create(stock_quant_platform)
        #                         else:
        #                             sql = 'select min(id) as sys_platform_id from sys_platform where warehouseid = %s' %(line['fdstockid'])
        #                             cr.execute(sql)
        #                             stock_quant_platform = cr.dictfetchone()
        #                             stock_quant_platform['stock_quant_id'] = stock_quant_id
        #                             stock_quant_platform['qty'] = line['quantity']
        #                             # 创建小组库存数据
        #                             self.env['stock.quant.platform'].create(stock_quant_platform)
        #         # 将所需数据添加至新建表中
        #         keys = (
        #             k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
        #             ar_sub_group_receipt_id,new_id, 'inventory', remark)
        #         self.get_flag(keys)
        #         ##################打标记
        #         sql = """update stock_dest_view_copy set flag=True where wh_code='%s'  and prod_code='%s'""" % (line['wh_code'], line['prod_code'])
        #         cr.execute(sql)
        else:
            _logger.info('#######################中转仓库存同步完成，不需要继续导入了#####################################')
            pass
        return True
   ###############目的仓 K3_stock &&  ar_sub_group_warehouse_daily_closing_0602
    @api.model
    def _import_stock_dest(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action !='create':
            return True
        cr=self.env.cr
        k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,remark = (0, 0, 0, 0, 0,'dest')
        ##############################默认参数获取
        # template_location_id, template_partner_id,template_location_id2 = self.get_parm()
       #####################目的仓#########################################
        #################### NO.1 取得K3目的仓库存
        sql="""select * from stock_dest_view_copy where flag = false order by location_id, product_id limit %s"""%limit
        cr.execute(sql)
        k3_fet=cr.dictfetchall()
        if k3_fet:
            x=1
            vals_inv_create = {}
            vals_line = []
            location_id = None
            for line in k3_fet:
                _logger.info('======================line======dest===========%s========' % (x))
                x += 1
                k3_stock_id=line['id']
                if location_id is None:
                    location_id = line['location_id']
                    vals_inv_create = {'name': 'dest', 'location_id': line['location_id'], 'date': '2018-04-12','filter': 'partial'}
                if location_id == line['location_id']:
                    each = {'product_id': line['product_id'], 'location_id':line['location_id'],'product_qty': line['quantity']}
                    vals_line.append((0, 0, each))
                else:
                    vals_inv_write = {'line_ids': vals_line, 'date': '2018-04-12'}
                    #########创建盘点单
                    new_id = self.create_inventory(vals_inv_create, vals_inv_write)
                    vals_inv_create = {}
                    vals_line = []
                    # 计算小组库存
                    self.group_stock(new_id)
                    # 若仓库ID变化，则重新生成调整单
                    location_id = line['location_id']
                    vals_inv_create = {'name': 'dest', 'location_id': line['location_id'], 'date': '2018-04-12','filter': 'partial'}
                    each = {'product_id': line['product_id'], 'location_id': line['location_id'],
                            'product_qty': line['quantity']}
                    vals_line.append((0, 0, each))

                # 将所需数据插入新建表中
                keys = (
                k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id,
                new_id, 'inventory',remark)
                self.get_flag(keys)
                ##################打标记
                sql="""update stock_dest_view_copy set flag=True where wh_code='%s' and prod_code='%s'"""%(line['wh_code'],line['prod_code'])
                cr.execute(sql)

            # 最后一种仓库生产盘点单，并计算小组库存
            vals_inv_write = {'line_ids': vals_line, 'date': '2018-04-12'}
            #########创建盘点单
            new_id2 = self.create_inventory(vals_inv_create, vals_inv_write)
            # 计算小组库存
            self.group_stock(new_id2)



        ############################################################################################################
                # #################### NO.1 取得K3目的仓库存
                # sql = """SELECT A .ID,A .prod_code,A .wh_code,A .quantity,A . COST,b.ftranstype,b.fname,b.fdstockid,C.ID AS product_id, d.id AS location_id FROM K3_Stock_ar A INNER JOIN MultiDestinationWarehouse b ON b.fwarehouseid = CAST (A .wh_code AS INT)
                #                 INNER JOIN product_product C ON C .default_code = A .prod_code INNER JOIN stock_location d ON d. NAME = b.fname WHERE A .quantity <> 0 AND A . FLAG = 'f' AND b.ftype = '发货仓'
                #               limit %s""" % limit
                # cr.execute(sql)
                # k3_fet = cr.dictfetchall()
                # if k3_fet:
                #     x = 1
                #     for line in k3_fet:
                #         _logger.info('======================line======dest===========%s========' % (x))
                #         x += 1
                #         k3_stock_id = line['id']
                #         vals_inv_create = {'name': 'dest', 'location_id': line['location_id'], 'date': '2018-03-21',
                #                            'filter': 'partial'}
                #         each = {'product_id': line['product_id'], 'location_id': line['location_id'],
                #                 'product_qty': line['quantity']}
                #         vals_line = [(0, 0, each)]
                #         vals_inv_write = {'line_ids': vals_line, 'date': '2018-03-21', }
                #         #########创建盘点单
                #         new_id = self.create_inventory(vals_inv_create, vals_inv_write)
                #         stock_inventory = self.env['stock.inventory'].browse(new_id)
                #         stock_moves = stock_inventory.move_ids
                #         if stock_moves:
                #             for stock_move in stock_moves:
                #                 stock_quants = stock_move.quant_ids
                #                 if stock_quants:
                #                     for stock_quant in stock_quants:
                #                         stock_quant_id = stock_quant.id
                #                         # 删除验证时自动生成的小组库存
                #                         sql = 'delete from stock_quant_platform where stock_quant_id = %s' % (
                #                         stock_quant_id)
                #                         cr.execute(sql)
                #                         # 查询出对应的小组库存
                #                         sql = """SELECT a.quantity as qty,c.id as stock_location_id,d.id as product_id,f.id as sys_platform_id
                #                                                         FROM ar_sub_group_warehouse_daily_closing_0602 a
                #                                                         INNER JOIN multidestinationwarehouse b ON a.warehouseid = b.fwarehouseid
                #                                                         INNER JOIN stock_location c ON c.name = b.fname
                #                                                         INNER JOIN product_product d ON d.default_code = a.sku
                #                                                         INNER JOIN ar_sub_group e ON e.subgroupid = a.subgroupid AND e.warehouseid = b.fdstockid
                #                                                         INNER JOIN sys_platform f ON f.pid = e.id
                #                                                         where a.quantity <> 0 and a.sku = '%s' and c.id = '%s'
                #                                                     """ % (line['prod_code'], line['location_id'])
                #                         cr.execute(sql)
                #                         stock_quant_platforms = cr.dictfetchall()
                #                         if stock_quant_platforms:
                #                             for stock_quant_platform in stock_quant_platforms:
                #                                 stock_quant_platform['stock_quant_id'] = stock_quant_id
                #                                 # 创建小组库存数据
                #                                 self.env['stock.quant.platform'].create(stock_quant_platform)
                #                         else:
                #                             sql = 'select min(id) as sys_platform_id from sys_platform where warehouseid = %s' % (
                #                             line['fdstockid'])
                #                             cr.execute(sql)
                #                             stock_quant_platform = cr.dictfetchone()
                #                             stock_quant_platform['stock_quant_id'] = stock_quant_id
                #                             stock_quant_platform['qty'] = line['quantity']
                #                             # 创建小组库存数据
                #                             self.env['stock.quant.platform'].create(stock_quant_platform)
                #         # 将所需数据插入新建表中
                #         keys = (
                #             k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                #             ar_sub_group_receipt_id,
                #             new_id, 'inventory', remark)
                #         self.get_flag(keys)
                #         ##################打标记
                #         sql = """update K3_Stock_ar set flag=True where wh_code='%s'   and prod_code='%s'""" % (
                #         line['wh_code'], line['prod_code'])
                #         cr.execute(sql)
        else:
            _logger.info('#######################目的仓库存同步完成，不需要继续导入了#####################################')
            pass
        return True
    #############移动仓 数据来自ar_sub_group_receipt_0602
    @api.model
    def _import_stock_move(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action !='create':
            return True
        cr=self.env.cr
        k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,remark= ( 0, 0, 0, 0, 0,'move')
        #####################移动仓#########################################
        #################### NO.1 取得K3移动仓库存
        sql = """select * from stock_move_view_copy01 where flag = false order by location_id,date,product_id limit %s""" % limit
        cr.execute(sql)
        k3_fet = cr.dictfetchall()
        if k3_fet:
            x=1
            vals_line = []
            vals_inv_write = {}
            location_id = None
            date = None
            for line in k3_fet:
                _logger.info('======================line===move==============%s========' % (x))
                x += 1
                if location_id is None and date is None:
                    location_id = line['location_id']
                    date = line['date']
                    vals_inv_create = {'name': 'move', 'location_id': line['location_id'], 'date': line['date'],'filter': 'partial'}
                if location_id == line['location_id'] and date == line['date']:
                    each = {'product_id': line['product_id'], 'location_id': line['location_id'],'product_qty': line['quantity']}
                    vals_line.append((0, 0, each))
                else:
                    vals_inv_write = {'line_ids': vals_line,  'date': date}
                    #########创建盘点单
                    new_id = self.create_inventory(vals_inv_create, vals_inv_write)
                    vals_inv_create = {}
                    vals_line = []
                    # 计算小组库存
                    self.group_stock_move(new_id)

                    # 若仓库ID变化，则重新生成调整单
                    location_id = line['location_id']
                    date = line['date']
                    vals_inv_create = {'name': 'move', 'location_id': line['location_id'], 'date': line['date'],'filter': 'partial'}
                    each = {'product_id': line['product_id'], 'location_id': line['location_id'],
                            'product_qty': line['quantity']}
                    vals_line.append((0, 0, each))

                ##################打标记
                sql = """update stock_move_view_copy01 set flag=True where sku ='%s' and location_id = %s and date = '%s'""" % (line['sku'],line['location_id'],line['date'])
                cr.execute(sql)
            # 最后一种仓库生产盘点单，并计算小组库存
            vals_inv_write = {'line_ids': vals_line, 'date': date}
            #########创建盘点单
            new_id2 = self.create_inventory(vals_inv_create, vals_inv_write)
            # 计算小组库存
            self.group_stock_move(new_id2)

        # #################### NO.1 取得K3移动仓库存
        # sql = """SELECT A . ID,G . ID AS product_id,A .sku,A .quantity,h.fname,b.platforms,f. ID AS sys_platform_id,
        #             A . DATE - INTERVAL '8 hours' AS DATE,C .ftranstype,e. ID AS location_id
        #             FROM ar_sub_group_receipt_0602 A
        #             INNER JOIN ar_sub_group b ON A .subGroupId = b.subGroupId
        #             AND A .warehouseIn = b.warehouseId ---中转仓库
        #             INNER JOIN MultiDestinationWarehouse C ON C .fwarehouseid = A .warehousesource --目的仓
        #             INNER JOIN MultiDestinationWarehouse d ON d.fwarehouseid = A .warehousein
        #             INNER JOIN multidestinationwarehouse h ON h.ftranstype = c.ftranstype and h.fdstockid = d.fwarehouseid and h.ftype = '移动仓'
        #             INNER JOIN stock_location e ON e. NAME = h.fname
        #             INNER JOIN sys_platform f ON f.pid = b. ID
        #             INNER JOIN product_product G ON G .default_code = A .sku
        #             WHERE A .stage = '移仓阶段' AND A . FLAG = FALSE limit %s""" % limit
        # cr.execute(sql)
        # k3_fet = cr.dictfetchall()
        # if k3_fet:
        #     x = 1
        #     for line in k3_fet:
        #         _logger.info('======================line===move==============%s========' % (x))
        #         x += 1
        #         k3_stock_id = line['id']
        #         vals_inv_create = {'name': 'move', 'location_id': line['location_id'], 'date': '2018-03-21',
        #                            'filter': 'partial'}
        #         each = {'product_id': line['product_id'], 'location_id': line['location_id'],
        #                 'product_qty': line['quantity']}
        #         vals_line = [(0, 0, each)]
        #         vals_inv_write = {'line_ids': vals_line, 'date': '2018-03-21', }
        #         #########创建盘点单
        #         new_id = self.create_inventory(vals_inv_create, vals_inv_write)
        #         stock_inventory = self.env['stock.inventory'].browse(new_id)
        #         stock_moves = stock_inventory.move_ids
        #         if stock_moves:
        #             for stock_move in stock_moves:
        #                 stock_quants = stock_move.quant_ids
        #                 if stock_quants:
        #                     for stock_quant in stock_quants:
        #                         stock_quant_id = stock_quant.id
        #                         # 删除验证时自动生成的小组库存
        #                         sql = 'delete from stock_quant_platform where stock_quant_id = %s' % (
        #                         stock_quant_id)
        #                         cr.execute(sql)
        #                         # 查询出对应的小组库存
        #                         stock_quant_platform = {'stock_quant_id': stock_quant_id,
        #                                                 'qty': line['quantity'],
        #                                                 'sys_platform_id': line['sys_platform_id']}
        #                         # 创建小组库存数据
        #                         self.env['stock.quant.platform'].create(stock_quant_platform)
        #         # 将所需数据插入新建表中
        #         keys = (
        #             k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
        #             ar_sub_group_receipt_id,
        #             new_id, 'inventory', remark)
        #         self.get_flag(keys)
        #         ##################打标记
        #         sql = """update ar_sub_group_receipt_0602 set flag=True where id=%s""" % (line['id'])
        #         cr.execute(sql)


        else:
            _logger.info('#######################移动仓库存同步完成，不需要继续导入了#####################################')
            pass
        return True

    # 调整my_data_copy表的数据，插入到account_data表
    @api.model
    def adjust_my_data_copy(self, *args):
        cr = self.env.cr
        sql = 'select DISTINCT a from my_data_copy order by a'
        cr.execute(sql)
        codes = cr.fetchall()
        for code in codes:
            # sql = """select * from my_data_copy where a = '%s' order by num"""%(code)
            sql = """select num,a,b,c,cast(j as numeric) as j,cast(k as numeric) as k from my_data_copy where a = '%s' order by num"""%(code)
            # sql = """select * from my_data_copy from a = '1001.01' order by num"""

            cr.execute(sql)
            accounts = cr.dictfetchall()
            if len(accounts) == 2:
                dic = {}
                for account in accounts:
                    if account['c'] == '综合本位币':
                        dic['num'] = account['num']
                        dic['account_code'] = account['a']
                        dic['account_name'] = account['b']
                        dic['standard_money'] = account['c']
                        dic['debit'] = account['j']
                        dic['credit'] = account['k']
                        dic['currency'] = False
                        dic['amount_currency'] = 0
                    elif account['c'] == '人民币' or '折合本位币':
                        continue
                self.insert_account_data(**dic)
            elif len(accounts) == 3:
                dic = {}
                for account in accounts:
                    if account['c'] == '综合本位币':
                        dic['num'] = account['num']
                        dic['account_code'] = account['a']
                        dic['account_name'] = account['b']
                        dic['standard_money'] = account['c']
                        dic['debit'] = account['j']
                        dic['credit'] = account['k']
                    elif account['c'] == '折合本位币':
                        continue
                    else:
                        dic['currency'] = account['c']
                        if account['j'] != 0 and account['k'] == 0:
                            dic['amount_currency'] = account['j']
                        elif account['k'] != 0 and account['j'] == 0:
                            dic['amount_currency'] = -account['k']
                self.insert_account_data(**dic)
            elif len(accounts) > 3:
                dic = {}
                for account in accounts:
                    if account['c'] == '人民币':
                        dic['num'] = account['num']
                        dic['account_code'] = account['a']
                        dic['account_name'] = account['b']
                        dic['standard_money'] = account['c']
                        dic['debit'] = account['j']
                        dic['credit'] = account['k']
                        dic['currency'] = False
                        dic['amount_currency'] = 0
                        self.insert_account_data(**dic)
                        dic.clear()
                    elif account['c'] == '综合本位币':
                        continue
                    elif account['c'] == '折合本位币':
                        dic['num'] = account['num']
                        dic['account_code'] = account['a']
                        dic['account_name'] = account['b']
                        dic['standard_money'] = account['c']
                        dic['debit'] = account['j']
                        dic['credit'] = account['k']
                        self.insert_account_data(**dic)
                        dic.clear()
                    else:
                        dic['currency'] = account['c']
                        if account['j'] != 0 and account['k'] == 0:
                            dic['amount_currency'] = account['j']
                        elif account['k'] != 0 and account['j'] == 0:
                            dic['amount_currency'] = -account['k']
        _logger.info('#######################调整财务数据完成#####################################')
        return True

    # 给account_data表插入数据
    def insert_account_data(self,**dic):
        sql = """insert into account_data(num,account_code,account_name,standard_money,debit,credit,currency,amount_currency) values(%s,'%s','%s','%s',%s,%s,'%s',%s)""" \
              % (dic['num'], dic['account_code'], dic['account_name'], dic['standard_money'], dic['debit'],
                 dic['credit'], dic['currency'], dic['amount_currency'])
        self.env.cr.execute(sql)
        return True

    # # 生成凭证(之前版本)
    # @api.model
    # def create_voucher(self):
    #     cr = self.env.cr
    #     sql = """select * from account_data order by num"""
    #     cr.execute(sql)
    #     lines = cr.dictfetchall()
    #     account_move_lines = []
    #     for line in lines:
    #         # 查找科目ID
    #         sql = """select id from account_account where code = '%s'""" %(line['account_code'])
    #         cr.execute(sql)
    #         account_id = cr.fetchone()
    #         # 查找币种ID
    #         sql = """select id from res_currency where name = '%s'""" %(line['currency_code'])
    #         cr.execute(sql)
    #         currency_id = cr.fetchone()
    #         if not currency_id:
    #             currency_id = False
    #             line['amount_currency'] = False
    #         account_move_line = {'account_id':account_id,'debit':line['debit'],'credit':line['credit'],'currency_id':currency_id,'amount_currency':line['amount_currency'],'name':'期初数据初始化'}
    #         account_move_lines.append((0,0,account_move_line))
    #     # 创建日记账分录
    #     account_move = self.env['account.move'].create(
    #         {'journal_id': 5, 'date': '2018-03-26', 'ref': '期初财务数据初始化', 'line_ids':account_move_lines})
    #     return True

    # # 生成期初凭证(新版本)
    # @api.model
    # def _first_create_voucher(self):
    #     cr = self.env.cr
    #     sql = """select * from account_data1 order by a1"""
    #     cr.execute(sql)
    #     lines = cr.dictfetchall()
    #     account_move_lines = []
    #     for line in lines:
    #         if line['c1'] == '辅':
    #             sql = """select * from account_data2 where b2 = '%s' order by a2""" %(line['d1'])
    #             cr.execute(sql)
    #             data_lines = cr.dictfetchall()
    #             for data_line in data_lines:
    #                 # 查找科目ID
    #                 sql = """select id from account_account where code = '%s'""" % (data_line['b2'])
    #                 cr.execute(sql)
    #                 account_id = cr.fetchone()
    #
    #                 sql = """select id from res_partner where name = '%s'""" % (data_line['c2'])
    #                 cr.execute(sql)
    #                 partner_id = cr.fetchone()
    #
    #                 if not account_id or partner_id:
    #                     account_move_line = {'account_id': account_id, 'debit': data_line['d2'], 'credit': data_line['e2'],
    #                                          'name': '期初数据初始化','partner_id': partner_id}
    #                 account_move_lines.append((0, 0, account_move_line))
    #         else:
    #             if line['b1'] == '首':
    #                 continue
    #
    #             # 查找科目ID
    #             sql = """select id from account_account where code = '%s'""" % (line['d1'])
    #             cr.execute(sql)
    #             account_id = cr.fetchone()
    #             if account_id:
    #                 account_move_line2 = {'account_id': account_id, 'debit': line['j1'], 'credit': line['k1'],
    #                                       'name': '期初数据初始化'}
    #             account_move_lines.append((0, 0, account_move_line2))
    #     # 创建日记账分录
    #     account_move = self.env['account.move'].create(
    #         {'journal_id': 5, 'date': '2018-04-28', 'ref': '期初财务数据初始化', 'line_ids': account_move_lines})
    #     return True
    #
    # # 生成发生额凭证(新版本)
    # @api.model
    # def _last_create_voucher(self):
    #     cr = self.env.cr
    #     sql = """select * from account_data1 order by a1"""
    #     cr.execute(sql)
    #     lines = cr.dictfetchall()
    #     account_move_lines = []
    #     for line in lines:
    #         if line['c1'] == '辅':
    #             sql = """select * from account_data2 where b2 = '%s' order by a2""" % (line['d1'])
    #             cr.execute(sql)
    #             data_lines = cr.dictfetchall()
    #             for data_line in data_lines:
    #                 # 查找科目ID
    #                 sql = """select id from account_account where code = '%s'""" % (data_line['b2'])
    #                 cr.execute(sql)
    #                 account_id = cr.fetchone()
    #
    #                 sql = """select id from res_partner where name = '%s'""" % (data_line['c2'])
    #                 cr.execute(sql)
    #                 partner_id = cr.fetchone()
    #
    #                 if not account_id or partner_id:
    #                     account_move_line = {'account_id': account_id, 'debit': data_line['f2'],'credit': data_line['g2'],
    #                                          'name': '发生额数据初始化', 'partner_id': partner_id}
    #                 account_move_lines.append((0, 0, account_move_line))
    #         else:
    #             if line['b1'] == '首':
    #                 continue
    #             # 查找科目ID
    #             sql = """select id from account_account where code = '%s'""" % (line['d1'])
    #             cr.execute(sql)
    #             account_id = cr.fetchone()
    #             if account_id:
    #                 account_move_line = {'account_id': account_id, 'debit': line['l1'], 'credit': line['m1'],
    #                                      'name': '发生额数据初始化'}
    #
    #             account_move_lines.append((0, 0, account_move_line))
    #     # 创建日记账分录
    #     account_move = self.env['account.move'].create(
    #         {'journal_id': 5, 'date': '2018-04-28', 'ref': '发生额财务数据初始化', 'line_ids': account_move_lines})
    #     return True

    # 生成凭证
    @api.model
    def _create_voucher(self):
        cr = self.env.cr
        sql = """select a.* from account_data a 
                  INNER JOIN account_km b on a.a = b.a and b.c = FALSE
                  where a.g is not null and a.h is not null order by a.a"""
        cr.execute(sql)
        lines = cr.dictfetchall()
        account_move_lines = []
        for line in lines:
            # 查找科目ID
            sql = """select id from account_account where code = '%s'""" % (line['a'])
            cr.execute(sql)
            account_id = cr.fetchone()
            # 查找辅助科目ID
            partner_id = False
            if line['c']:
                sql = """select id from res_partner where type_flag = 'supplier'and name = '%s'""" %(line['c'])
                cr.execute(sql)
                partner_id = cr.fetchone()
            # 查找币种ID
            sql = """select id from res_currency where name = '%s'""" % (line['currency_code'])
            cr.execute(sql)
            currency_id = cr.fetchone()
            if not currency_id:
                currency_id = False
                amount_currency = False

                account_move_line = {'account_id': account_id, 'debit': float(line['g']), 'credit': float(line['h']),'partner_id':partner_id,
                                     'currency_id': currency_id, 'amount_currency': amount_currency,'name': '期初数据初始化'}
            else:
                if float(line['g']) != 0 and float(line['h']) == 0:
                    account_move_line = {'account_id': account_id, 'debit': float(line['f']), 'credit': float(line['h']),'partner_id': partner_id,
                                         'currency_id': currency_id, 'amount_currency': float(line['g']),'name': '期初数据初始化'}
                elif float(line['g']) == 0 and float(line['h']) != 0:
                    account_move_line = {'account_id': account_id, 'debit': float(line['g']), 'credit': float(line['f']),
                                         'partner_id': partner_id,
                                         'currency_id': currency_id, 'amount_currency': -float(line['h']),
                                         'name': '期初数据初始化'}

            account_move_lines.append((0, 0, account_move_line))
        # 创建日记账分录
        account_move = self.env['account.move'].create(
            {'journal_id': 5, 'date': '2018-06-30', 'ref': '期初财务数据初始化', 'line_ids': account_move_lines})
        return True






