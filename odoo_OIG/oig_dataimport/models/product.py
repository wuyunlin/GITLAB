# -*- coding: utf-8 -*-
from odoo import api, fields, models
import logging
import base64
import datetime
_logger = logging.getLogger(__name__)

import sys
reload(sys)
sys.setdefaultencoding('utf8')

class export_data_to_excel(models.TransientModel):
    _name = "export.data.to.excel"
    _description = u"excel导出数据参数配置"
    name = fields.Char(string='Action Usage')
    command = fields.Char(string='Action Usage')
    model = fields.Char(string='Action Usage')
    field = fields.Char(string='Action Usage')
    file_name = fields.Char(string='Action Usage')


class product_template(models.Model):
    _inherit = 'product.template'

    @api.model
    def create(self, vals):
        # vals['name']='供应商王先生'
        res = super(product_template, self).create(vals)
        return res

    def _write_pt(self, pt_id, vals):
        pt_obj=self.env['product.template']
        pt_instance=pt_obj.browse(pt_id)
        return True

    def _mycopy(self, pt_instance, vals,limit):
        cr = self.env.cr
        attribute_id=pt_instance.attribute_line_ids[0].attribute_id.id
        value_id=pt_instance.attribute_line_ids[0].attribute_id.value_ids[0].id

        sql="""select  m_fcode,m_fchinesename  from product_import_view  where m_fcode not in
              (select my_default_code from product_template where active='t' and my_default_code is not null )  group by m_fcode,m_fchinesename limit %s"""%(limit)
        cr.execute(sql)
        fet=cr.fetchall() or []
        x=1
        if not fet:
            return False
        for each in fet:
            _logger.info('====line================================%s============='%x)
            x+=1
            m_fcode, m_fchinesename=each
            sql = """select  m_fid, c_fid, m_fchinesename, c_fchinesename, m_fcode, c_fcode, m_faddtime, c_faddtime from product_import_view where m_fcode ='%s'""" % ( m_fcode)
            cr.execute(sql)
            dictfet=cr.dictfetchall()
            value_ids=[]
            for line in dictfet:
                value_ids.append([0, 0, {'attribute_id': attribute_id, 'name':'%s:%s'%(line['c_fchinesename'],line['c_fcode'])   }])
            attribute_line_ids=[ [0, False, {  'product_tmpl_id':pt_instance.id, u'attribute_id': attribute_id, u'value_ids':value_ids     }          ]     ]
            vals.update({'attribute_line_ids':attribute_line_ids,'name':m_fchinesename,'default_code':m_fcode,'my_default_code':m_fcode})
            ###############图片#############################################################
            sql = """select fpicture from createcodepicture where fpicture is not null and fcodetype='母代码' and finterid=%s limit 1"""%(dictfet[0]['m_fid'])
            self.env.cr.execute(sql)
            fet = self.env.cr.fetchone()
            vals['image']=False
            if fet:
                _logger.info('====image================================%s=============' % m_fcode)
                vals.update({'image':base64.b64encode(fet[0])})

            new_id=pt_instance.copy(default=vals)
            sql="""with res as (
                    select a.id, d.c_fcode from product_product a
                    inner join product_attribute_value_product_product_rel b on b.product_product_id=a.id
                    inner join product_attribute_value c on c.id=b.product_attribute_value_id
                    inner join product_import_view d on d.c_fchinesename||':'||d.c_fcode =c.name
                    where a.product_tmpl_id=%s
                    )
                    update product_product set default_code=res.c_fcode from res where res.id=product_product.id;
                    delete from ir_translation where name='product.template,name';
                    """%(new_id.id)
            cr.execute(sql)
            if new_id:
                sql = """insert into product_template_import(product_template_id,code) values (%s,'%s') """ % (new_id.id,m_fcode)
                cr.execute(sql)
                #-------更新子代码图片
                sql="""select c.id,a.fcode,b.fpicture from CreateCodeChildCode a
                        inner join createcodepicture b on  a.FID=b.FInterID and b.FCodeType='子代码'
                        inner join product_product c on c.default_code=a.fcode
                        where c.product_tmpl_id=%s and b.fpicture is not null"""%(new_id.id)
                cr.execute(sql)
                fet=cr.fetchall()
                for line in fet:
                    self.env['product.product'].browse(line[0]).write({'z_image':base64.b64encode(line[2])})
        return True
    def _copy_pt(self, pt_id, vals,limit):
        _logger.info('======================Start create product_template template:%s========================='%(pt_id))
        cr=self.env.cr
        #检查记录表是否存在
        sql="""select 1 from information_schema.columns where table_name = 'product_template_import'"""
        cr.execute(sql)
        #没有则新建表
        if not cr.fetchone():
            sql="""create table product_template_import(product_template_id int,code varchar(1000),fid int,description text ); """
            cr.execute(sql)
        sql="""select 1 from information_schema.columns where table_name = 'product_product_import'"""
        cr.execute(sql)
        #没有则新建表
        if not cr.fetchone():
            sql="""create table product_product_import(product_product_id int,code varchar(1000),fid int,description text ); """
            cr.execute(sql)
        pt_obj=self.env['product.template']
        pt_instance=pt_obj.browse(pt_id)
        vals={}
        fet=self._mycopy(pt_instance,vals,limit)
        # if not fet:
        #     sql="""select  FInterID from createcodepicture where FCodeType='母代码' and is_ok ='f' and FPicture is not null limit 100"""
        #     cr.execute(sql)
        #     ids=cr.fetchall()
        #     if not ids:
        #         return True
        #     ids=tuple([x[0] for x in ids])
        #     import pymssql, base64
        #     conn = pymssql.connect(host="192.168.1.220", user="sa", password="xxxxxx!", database="xxxx")
        #     cur = conn.cursor()
        #     sql="""select   FInterID,FPicture  from MMS.dbo.CreateCodePicture where FPicture is not null and FCodeType='母代码' and  FInterID in %s""" % (str(ids))
        #     cur.execute(sql )
        #     fet = cur.fetchall() or []
        #     x=1
        #     for line in fet:
        #         x+=1
        #         sql = """select b.id
        #                 from CreateCodeMotherCode as a
        #                 inner join product_template b on b.default_code=a.fcode
        #                 where a.fid=%s""" % line[0]
        #         cr.execute(sql)
        #         template_fet = cr.fetchone()
        #         if template_fet:
        #             image = base64.b64encode(line[1])
        #             template_ins=self.env['product.template'].browse(template_fet[0])
        #             template_ins.write({'image': image})
        #             sql="""update createcodepicture set is_ok='t' where FInterID=%s """%(line[0])
        #             cr.execute(sql)
        #         else:
        #             pass
        _logger.info( '======================end create product_template template:%s=========================' % (pt_id))

        return True

    @api.model
    def _import_pt(self,*args):
        #
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_pt(args[0],args[1],args[2])
        else:
            self._write_pt(args[0], args[1])


##############################################供应商导入####################################################################
    def _copy_supplier(self, partner_id, vals,limit):
        _logger.info('======================Start create res_partner template:%s========================='%(partner_id))
        cr=self.env.cr
        sql="""select a.fname as name,coalesce(a.fnumber,'') as ref ,coalesce(a.fcontact,'') as comment,coalesce(a.fphone,'') as phone,coalesce(a.ffax,'') as fax,coalesce(a.femail,'') as email from supplier_view a where fname not in (select name from res_partner where active='t' and supplier='t') limit %s  """%limit
        cr.execute(sql)
        dicfet=cr.dictfetchall() or {}
        x=1
        partner_ins=self.env['res.partner'].browse(partner_id)
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x+=1
            new_partner=partner_ins.copy(default=line)
            line.update({'supplier':'t'})
            new_partner.write(line)
        _logger.info('======================END create res_partner template:%s=========================' % (partner_id))
    @api.model
    def _import_supplier(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_supplier(args[0],args[1],args[2])
        else:
            pass
            ##############################################供应商导入####################################################################

    def _copy_users(self, users_id, vals, limit):
        _logger.info(
            '======================Start create res_partner template:%s=========================' % (users_id))
        cr = self.env.cr
        sql = """select a.fname as name,a.fname as login from t_user a where fname not in (select login from res_users where active='t') limit %s  """ % limit
        cr.execute(sql)
        dicfet = cr.dictfetchall() or {}
        x = 1
        users_ins = self.env['res.users'].browse(users_id)
        new_users=0
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x += 1
            new_users= users_ins.copy(default=line)
            new_users.write(line)
            new_users.partner_id.write({'staff_fg':'t'})
        _logger.info('======================END create res_users template:%s=========================' % (new_users))

    @api.model
    def _import_users(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_users(args[0], args[1], args[2])
        else:
            pass
##############################################仓库导入####################################################################
    def _copy_warehouse(self, warehouse_id, vals,limit):
        _logger.info('======================Start create stock_warehouse template:%s========================='%(warehouse_id))
        cr=self.env.cr
        sql="""select * from warehouse_view limit %s  """%limit
        cr.execute(sql)
        dicfet=cr.dictfetchall() or {}
        x=1
        warehouse_obj=self.env['stock.warehouse']
        warehouse_ins=self.env['stock.warehouse'].browse(warehouse_id)
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x+=1
            #new_warehouse=warehouse_ins.copy(default=line)
            new_warehouse = warehouse_obj.create(line)
            #new_warehouse.write(line)
        _logger.info('======================END create stock_warehouse template:%s=========================' % (warehouse_id))
    @api.model
    def _import_warehouse(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_warehouse(args[0],{},args[2])
        else:
            pass


##############################################部门导入####################################################################
    def _copy_department(self, department_id, vals,limit):
        _logger.info('======================Start create department template:%s========================='%(department_id))
        cr=self.env.cr
        sql="""select fname as name,coalesce(fnumber,'') as ref from t_department
                where fname not in (select name from res_partner where active='t' and dep_fg='t') limit %s  """%(limit)
        cr.execute(sql)
        dicfet=cr.dictfetchall() or {}
        x=1
        department_id_ins=self.env['res.partner'].browse(department_id)
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x+=1
            new_department=department_id_ins.copy(default=line)
            line.update({'dep_fg':True})
            new_department.write(line)
        _logger.info('======================END create department template:%s=========================' % (department_id))

    ##############################################快递公司导入####################################################################
    def _copy_express(self, express_id, vals, limit):
        _logger.info( '======================Start create express_id template:%s=========================' % (express_id))
        cr = self.env.cr
        sql = """select FName as name,FName as ref
                from t_SubMessage
                where FTypeID=10014 and FName not in (select ref from res_partner where ref  is not null) limit %s  """ % ( limit)
        cr.execute(sql)
        dicfet = cr.dictfetchall() or {}
        x = 1
        department_id_ins = self.env['res.partner'].browse(express_id)
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x += 1
            new_department = department_id_ins.copy(default=line)
            line.update({'express_fg':'t'})
            new_department.write(line)
        _logger.info('======================END create express_id template:%s=========================' % (express_id))
        ##############################################快递公司导入####################################################################

    @api.model
    def _import_department(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_department(args[0],{},args[2])
        else:
            pass
################################快递公司
    @api.model
    def _import_express(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_express(args[0], {}, args[2])
        else:
            pass

##############################################职员导入####################################################################
    def _copy_emp(self, emp_id, vals,limit):
        _logger.info('======================Start create emp_id template:%s========================='%(emp_id))
        cr=self.env.cr
        sql="""select fname as name,coalesce(fnumber,'') as ref from t_emp
                where fname not in (select name from res_partner where active='t' and staff_fg='t') limit %s  """%(limit)
        cr.execute(sql)
        dicfet=cr.dictfetchall() or {}
        x=1
        emp_id_ins=self.env['res.partner'].browse(emp_id)
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x+=1
            new_emp_id=emp_id_ins.copy(default=line)
            line.update({'staff_fg':'t'})
            new_emp_id.write(line)
        _logger.info('======================END create emp_id template:%s=========================' % (emp_id))
    @api.model
    def _import_emp(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_emp(args[0],{},args[2])
        else:
            pass

##############################################科目导入####################################################################
    def _copy_account(self, account_id, vals,limit):
        _logger.info('======================Start create account template:%s========================='%(account_id))
        cr=self.env.cr
        sql="""select fname as name,fnumber as code from account_view where fnumber not in (select code from account_account) limit %s  """%(limit)
        cr.execute(sql)
        dicfet=cr.dictfetchall() or {}
        x=1
        account_id_ins=self.env['account.account'].browse(account_id)
        for line in dicfet:
            _logger.info('======================line=================%s========' % (x))
            x+=1
            new_account_id=account_id_ins.copy(default=line)
            #new_account_id.write(line)
        _logger.info('======================END create account template:%s=========================' % (account_id))
    @api.model
    def _import_account(self,*args):
        if not(args and len(args)==3 and args[1] in ('create','write')):
            return True
        pt,action,limit=args
        if action=='create':
            self._copy_account(args[0],{},args[2])
        else:
             pass

    def handle_arrive(self):
        sql="""            --------批量插入:到货单插入脚本：
            INSERT INTO public.goods_arriving(
            create_date, origin_id, package_count, write_uid, actual_weight,
            create_uid, acourier_company, date_over, state, freight_person,
            arriving_area, full_attribute, real_package_count, write_date,
            freight_cost, remark, name, arrive_date, acourier_number, financial_change_fg)
            with res as (select row_number() over(PARTITION BY purchase_order_id order by purchase_order_line_id ),* from po_handle where flag='f' and stock_qty<>0)
            select a.date_order as create_date,a.name as origin_id,b.count as package_count,1 as write_uid,1 as actual_weight,
            1 as create_uid,1953 as acourier_company,null as date_over,'check_financial' as state,'other' as freight_person,'期初' as arriving_area ,
            'intact' as full_attribute,b.count as real_package_count,now() as  write_date , 0 as freight_cost ,'期初' as remark ,'期初' as name,
            now() as arrive_date , '期初快递单号' asacourier_number,False as financial_change_fg   from purchase_order a
            inner join res b on b.purchase_order_id=a.id and b.flag='f' and b.row_number=1;

            --------批量插入:到货明细插入
            INSERT INTO public.goods_arriving_real_line(
             financial_fg, create_date, po_id, qty, create_uid, over_reason,
            plan_qty, out_qty, financial_number, now_in_qty, pack_qty, wrap_criteria,
            state, order_id, has_in_count, transit_location, write_date,
            big_package_id, write_uid, qa_qty, product_id, has_in_bad_count,
            inventory_qty)
            select False as financial_fg, now() as create_date,a.purchase_order_id as po_id,a.stock_qty as qty,1 as create_uid,
            null as over_reason,a.stock_qty as plan_qty,0 as out_qty,null as financial_number,0 as now_in_qty,0 as pack_qty,
            null as wrap_criteria,'check_financial' as state,b.id as order_id,0 as has_in_count, d.transit_location as transit_location,
            now() as write_date,null as big_package_id,1 as write_uid,0 as qa_qty,c.product_id as product_id,0 as has_in_bad_count,
            0 as has_in_bad_count
            from po_handle a
            inner join goods_arriving b on b.origin_id=a.fbillno and b.name='期初'
            inner join purchase_order_line c on c.id=a.purchase_order_line_id
            inner join purchase_order d on d.id=a.purchase_order_id
            where a.flag='f' and  a.stock_qty<>0;
            ----批量插入:到货明细包裹表插入脚本
            INSERT INTO public.goods_arriving_real_line_package(
                        create_uid, area_id, create_date, weight, box_count, has_in_package_count,
                        has_in_package_id, box_type, write_uid, volume, width, length,
                        has_in_package_qty, real_line_id, write_date, box_qty, height,
                        location_id)
            select 1 as create_uid,1 as area_id,now() as create_date,1 as weight,1 as box_count,0 as has_in_package_count,
            null as has_in_package_id,1 as box_type,1 as write_uid,1 as volume,1 as width,1 as length,
            0 as has_in_package_qty,d.id as real_line_id,now() as write_date,a.stock_qty as box_qty,1 as height,null as location_id
            from po_handle a
            inner join goods_arriving b on b.origin_id=a.fbillno and b.name='期初'
            inner join purchase_order_line c on c.id=a.purchase_order_line_id
            inner join goods_arriving_real_line d on d.product_id=c.product_id and d.order_id=b.id
            where a.flag='f' and  a.stock_qty<>0;

            ----批量插入:采购明细插入
            INSERT INTO public.goods_arriving_plan_line(
                         financial_fg, create_date, po_id, qty, bad_qty, create_uid,
                        state, financial_number, real_line_id, order_id, old_qty, write_date,
                        write_uid, po_line_id, product_id)
            select False as financial_fg,now() as create_date,a.purchase_order_id as po_id, a.stock_qty as qty,0 as bad_qty,1 as create_uid, 'check_financial' as state,
            null as financial_number,d.id as real_line_id,b.id as order_id,c.product_qty as old_qty,now() as write_date,1 as write_uid,
            c.id as po_line_id,c.product_id as product_id
            from po_handle a
            inner join goods_arriving b on b.origin_id=a.fbillno and b.name='期初'
            inner join purchase_order_line c on c.id=a.purchase_order_line_id
            inner join goods_arriving_real_line d on d.product_id=c.product_id and d.order_id=b.id
            where a.flag='f' and a.stock_qty<>0;
           ----------更改标记
           update po_handle set Flag=True where Flag=False;"""
        self.env.cr.execute(sql)
        ##############到货单金融确认
        sql="""select id from goods_arriving where name='期初'and state='check_financial'"""
        self.env.cr.execute(sql)
        fet=self.env.cr.fetchall()
        if fet:
            for need_id in fet:
                #try:
                self.env['goods.arriving'].browse(need_id[0]).button_12()
                ###########查询po
                sql="""select id from purchase_order where name='%s'"""%(self.env['goods.arriving'].browse(need_id[0]).origin_id)
                self.env.cr.execute(sql)
                fet=self.env.cr.fetchone()
                if fet:
                    purchase_obj=self.env['purchase.o' \
                                          'rder'].browse(fet[0])
                    picking_ids=purchase_obj.picking_ids
                    for obj in picking_ids:
                        if obj.location_id.id<>8 or obj.state<>'done':
                            continue
                        return_obj=self.with_context(active_id=obj.id,active_ids=[obj.id]).env['stock.return.picking']
                        self.env['stock.picking'].browse(return_obj.create({}).create_returns()['res_id']).do_transfer()
                #except Exception, e:
                    # sql="""insert into goods_bad(goods_arriving_id,error) values (%s,'%s')"""%(need_id[0],'')
                    # self.env.cr.execute(sql)
        sql=""" update goods_arriving set name=name||origin_id where name='期初' and origin_id is not null"""
        self.env.cr.execute(sql)
        return True
    def handle_platform(self, purchase_instance):
        sql="""with res as (
                select a.id as purchase_platform_id,b.id as purchase_order_line_id,b.product_qty,a.total,sum(a.total) over(PARTITION BY b.id), cast(a.total as float)/sum(a.total) over(PARTITION BY b.id) as rate,
                b.product_qty- sum(a.total) over(PARTITION BY b.id) as cy  from purchase_platform a
                inner join purchase_order_line b on b.id=a.po_line_number
                inner join purchase_order c on c.id=b.order_id
                where c.id=%s ),
                res2 as (
                select row_number() over(PARTITION BY purchase_order_line_id order by purchase_platform_id desc), res.*,round(rate*cy) as add_qty, round(rate*cy)+total as new_qty,sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) as newsum,
                res.product_qty-sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) newcy from res  where rate<>1 )
                update purchase_platform set total=res2.new_qty from res2 where res2.purchase_platform_id=purchase_platform.id;
                with res as (
                select a.id as purchase_platform_id,b.id as purchase_order_line_id,b.product_qty,a.total,sum(a.total) over(PARTITION BY b.id), cast(a.total as float)/sum(a.total) over(PARTITION BY b.id) as rate,
                b.product_qty- sum(a.total) over(PARTITION BY b.id) as cy  from purchase_platform a
                inner join purchase_order_line b on b.id=a.po_line_number
                inner join purchase_order c on c.id=b.order_id
                where c.id=%s ),
                res2 as (
                select row_number() over(PARTITION BY purchase_order_line_id order by purchase_platform_id desc), res.*,round(rate*cy) as add_qty, round(rate*cy)+total as new_qty,sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) as newsum,
                res.product_qty-sum(round(rate*cy)+total) over (PARTITION BY purchase_order_line_id) newcy from res  where rate<>1 )
                --select * from res2
                update purchase_platform set total=res2.new_qty+res2.newcy from res2 where res2.purchase_platform_id=purchase_platform.id and res2.row_number=1;"""%(purchase_instance.id,purchase_instance.id)
        cr=self.env.cr
        cr.execute(sql)
        return True



    def handle_stock(self, purchase_instance, stock_list):
        #####create table po_handle(fbillno varchar,purchase_order_id int,purchase_order_line_id int,stock_qty int,account_qty int,count int,flag boolean)  ;
        if not stock_list:
            return   False
        sql=''
        cr=self.env.cr
        count=0
        for line in stock_list:
            if line[1]<>0:
                count+=1
        for line in stock_list:
            sql+="""insert into po_handle(fbillno,purchase_order_id,purchase_order_line_id,stock_qty,account_qty,count,flag)
                    select a.name as fbillno,a.id as purchase_order_id,b.id as purchase_order_line_id,%s as stock_qty,%s as account_qty,%s as count,False as flag from purchase_order a
                    inner join purchase_order_line b on b.order_id=a.id and b.product_id=%s
                    where a.id=%s;
                    """%(line[1],line[2],count,line[0],purchase_instance.id)
        if sql:
            cr.execute(sql)
        return True

    def handle_account(self, purchase_instance, account_list):
        ##########调用手工创建付款单的方法
        if  purchase_instance.invoice_ids:
            invoice_instance = purchase_instance.invoice_ids[0]
        else:
            invoice_instance=purchase_instance.create_account_invoice()
        ####找到生成的付款单
        if not purchase_instance.invoice_ids:
            return False
        if not invoice_instance:
            invoice_instance=purchase_instance.invoice_ids[0]
        ##########修改金额
        # price_subtotal,price_subtotal_signed,payment_amount,quantity,bili
        # price_unit
        cr = self.env.cr
        for line in account_list:
            sql = """select a.id, a.price_unit,b.product_qty from account_invoice_line a
                    inner join purchase_order_line b on b.id=a.purchase_line_id  where a.invoice_id=%s and a.product_id=%s limit 1""" % (invoice_instance.id,line[0])
            cr.execute(sql)
            fet = cr.fetchone()
            if not fet:
                continue
            account_invoice_line_id, price_unit, qty = fet
            account_qty = line[2]
            amount = round(price_unit * account_qty, 2)
            rate = str(round(float(account_qty) / qty, 4) * 100) + '%'
            dic = {'price_subtotal': amount, 'price_subtotal_signed': amount, 'payment_amount': amount,
                   'quantity': account_qty, 'bili': rate}
            if account_qty:
                self.env['account.invoice.line'].browse(account_invoice_line_id).write(dic)
            else:
                self.env['account.invoice.line'].browse(account_invoice_line_id).unlink()
        if len(invoice_instance.invoice_line_ids)==0:
            invoice_instance.unlink()
            return True
        #########直接更新付款单状态为待验证状态
        invoice_instance.write({'state':'proforma2'})
        invoice_instance.action_invoice_open()
        #########直接更新付款单状态为
        invoice_instance.write({'state': 'paid'})
        account_move_instance=invoice_instance.move_id
        ###########红冲凭证
        reverse_moves_ids=account_move_instance.reverse_moves(account_move_instance.date, account_move_instance.journal_id or False)
        ###########红冲凭证与源凭证做关联
        if reverse_moves_ids:
            reverse_id=reverse_moves_ids[0]
            self.env['account.move'].browse(reverse_id).write({'ref':'期初红冲：%s'%(account_move_instance.name)})
            account_move_instance.write({'ref':'期初：%s'%(purchase_instance.name)})
            #########数据库做记录
            sql="""insert into account_move_log(account_move_id,account_move_id_reverse) values (%s, %s)"""%(account_move_instance.id,reverse_id)
            self.env.cr.execute(sql)

        ###########核销
        sql="""select id from account_move_line where move_id in (%s,%s) and account_id in (select id from account_account where code='2202')"""%(account_move_instance.id,reverse_id)
        cr.execute(sql)
        fet=cr.fetchall()
        if fet:
            move_ids=[x[0] for x  in fet]
            move_lines = self.env['account.move.line'].browse(move_ids)
            currency = False
            for aml in move_lines:
                if not currency and aml.currency_id.id:
                    currency = aml.currency_id.id
                elif aml.currency_id:
                    if aml.currency_id.id == currency:
                        continue
                    #raise UserError(_('Operation not allowed. You can only reconcile entries that share the same secondary currency or that don\'t have one. Edit your journal items or make another selection before proceeding any further.'))
            #Don't consider entrires that are already reconciled
            move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
            #Because we are making a full reconcilition in batch, we need to consider use cases as defined in the test test_manual_reconcile_wizard_opw678153
            #So we force the reconciliation in company currency only at first
            move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_excluded', manual_full_reconcile_currency=currency).reconcile()

            #then in second pass the amounts in secondary currency, only if some lines are still not fully reconciled
            move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
            if move_lines_filtered:
                move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_only', manual_full_reconcile_currency=currency).reconcile()
            move_lines.compute_full_after_batch_reconcile()
        ###########################################
        return True

    ##############################################采购单导入####################################################################
    def _copy_purchase(self, purchase_id, vals, limit):
        _logger.info( '======================Start create purchase template:%s=========================' % (purchase_id))
        cr = self.env.cr
        # 检查记录表是否存在
        sql = """select 1 from information_schema.columns where table_name = 'po_ok'"""
        cr.execute(sql)
        # 没有则新建表
        if not cr.fetchone():
            sql = """create table po_ok(id int,purchase_order_id int); """
            cr.execute(sql)
        ############################基础数据视图######################################
        sql = """select distinct fbillno from po_view  limit %s  """ % (limit)
        cr.execute(sql)
        dicfet1 = cr.fetchall() or []
        x = 1
        purchase_ins = self.env['purchase.order'].browse(purchase_id)
        for line in dicfet1:
            _logger.info('======================line=================%s========' % (x))
            x += 1
            sql="""select a.*,B.totalqty from po_view  a
                    left join po_stage_view b on b.sku=a.name and b.billno=a.fbillno  where a.fbillno='%s'"""%(line)
            cr.execute(sql)
            dicfet = cr.dictfetchall() or []
            if not dicfet:
                continue
            dic=dicfet[0].copy()
            dic.pop('name')
            dic['order_line']=[]
            #------海运方式 目的仓 - -----------
            sql="""select b.FTransType, b.FName from  PurchaseRequisitions as a
            inner join MultiDestinationWarehouse as b on  a.FWareHouse = b.FName
            inner join MultiDestinationWarehouse as c on c.FWarehouseId = b.FDStockId
            where FPOORD = '%s' limit  1"""%(line[0])
            cr.execute(sql)
            fet=cr.fetchone()
            if not fet:
                sql = """select min(id) from stock_attiribute where type='中转仓'"""
                cr.execute(sql)
                transit_location=cr.fetchone()[0]
            else:
                sql="""select id from stock_attiribute where name='%s' and type='中转仓' """%(fet[1],)
                cr.execute(sql)
                fet=cr.fetchone()
                if fet:
                    transit_location=fet[0]
                else:
                    sql = """select min(id) from stock_attiribute where type='中转仓'"""
                    cr.execute(sql)
                    transit_location = cr.fetchone()[0]
            dic['transit_location']=transit_location
            dic['notes']='K3 PO:'+str(line[0])
            new_account_id = purchase_ins.copy(default=dic)
            line_list=[]
            stock_waitting_list = []
            for each in dicfet:
                each['lock']=str(each['fmrpclosed'])
                ##########################部分入库的单据 采购数量填总数
                product_qty=each['product_qty']
                account_qty=each['开票数量']
                stock_qty = product_qty-(each['totalqty'] or 0)
                stock_one = (each['product_id'], stock_qty,account_qty)
                stock_waitting_list.append(stock_one)
                each.update({'product_qty':product_qty,'product_uom':1,'date_planned':each['date_order']})
                ################将开票数量 到货数量 写在备注里
                remark='期初采购单：开票数量（%s）到货数量(%s)'%(account_qty,stock_qty)
                each.update({'name':remark})
                #########################################追加平台信息表#####################################
                sql="""--------------平台分配
                         select b.platforms,sum(a.quantity) as total,c.id as platform
                        from ar_sub_group_receipt_0602 as a
                        inner join (
                          select distinct subGroupId,platforms
                          from ar_sub_group )as b on a.subGroupId=b.subGroupId
                        inner join res_partner c on c.ref=b.platforms
                        where stage='采购阶段'
                        and billno='%s' and sku='%s' GROUP by b.platforms,c.id"""%(line[0],each['name'])
                cr.execute(sql)
                platform_fet=cr.dictfetchall()
                purchase_platform_id=[]
                for platform_line in platform_fet:
                    purchase_platform_id.append((0,0,platform_line))
                if not purchase_platform_id:
                    sql="""select id from res_partner where ref='ebay'"""
                    cr.execute(sql)
                    ebay=cr.fetchone()[0]
                    purchase_platform_id=[(0,0,{'total':product_qty,'platform':ebay})]
                each.update({'purchase_platform_id':purchase_platform_id})
                #########################################检查供应商交货期 没有的话就给该产品添加#############
                sql="""select id from product_product_supplierinfo where partner_id=%s and product_id=%s"""%(each['partner_id'],each['product_id'])
                cr.execute(sql)
                fet=cr.fetchone()
                if not fet:
                    # sql="""insert into product_product_supplierinfo(product_id,partner_id,delivery_days)
                    # select %s as product_id,%s as partner_id,b.fdelivery as delivery_days from CreateCodeChildCode a
                    # inner join Create  CodeDefaultField b on b.fcodetype='子代码' and a.FID=b.FInterID
                    # where a.fcode='%s' limit 1 returning id; """%(each['product_id'],each['partner_id'],each['name'])
                    # cr.execute(sql)
                    sql="""select b.fdelivery as delivery_days from CreateCodeChildCode a
                            inner join CreateCodeDefaultField b on b.fcodetype='子代码' and a.FID=b.FInterID
                            where a.fcode='%s' limit 1; """%(each['name'])
                    cr.execute(sql)
                    delivery_days=cr.fetchone()
                    delivery_days=delivery_days and delivery_days[0] or 0
                    self.env['product.product.supplierinfo'].create({'product_id':each['product_id'],'partner_id':each['partner_id'],'delivery_days':delivery_days})
                line_list.append( (0,0,each) )
            new_account_id.write({'order_line':line_list})
            #######处理平台数据 使得总数量正确
            self.handle_platform(new_account_id)
            ###########采购订单确认
            new_account_id.button_confirm()
            ############封装方法处理财务 库存问题
            self.handle_stock(new_account_id, stock_waitting_list)
            self.handle_account(new_account_id,stock_waitting_list)
            sql="""insert into po_ok(id,purchase_order_id) values (%s,%s)"""%(int(each['finterid']),new_account_id.id)
            cr.execute(sql)
        #批量生成到货单并确认
        self.handle_arrive()
        _logger.info( '=============='
                      ''
                      '========END create purchase_id template:%s=========================' % (purchase_id))
        return True

    @api.model
    def _import_purchase(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action == 'create':
            self._copy_purchase(args[0], {}, args[2])
        else:
            pass
############################execute#############################################
    @api.multi
    def action_execute(self):
        sql=self.description_sale
        ACTION_DICT= {
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'base.module.upgrade',
                        'target': 'new',
                        'type': 'ir.actions.act_window',

                    }
        from odoo import api, fields, models, modules, tools, _
        return dict(ACTION_DICT, name=_('Apply Schedule Upgrade'))
        if sql:
            self.env.cr.execute(sql)
            import uuid
            from odoo.addons.web.controllers.main import ExportData2Excel as _Export
            _name = str(uuid.uuid1())
            _command = """select name from res_partner limit 10"""
            _field = [u'物料编码']
            _file_name =  u'销售预测单_'
            _Export.do(self, self.env.cr, 1, _name, _file_name, _command, _field)
            return {"type": "ir.actions.act_su", "url": "/ExportData2Excel?name=" + _name}

        else:
            pass
#######################################0527  库存相关数据同步##########################################################

    ########封装 包裹的创建方法
    def create_pack(self, vals):
        pack_obj=self.env['stock.quant.package']
        ######源生字段
                # packaging_id 包裹类型 location_id:仓库主库位
        #vals = {'packaging_id': 1, 'location_id':383}
        ######新加字段
                #length:长 width:宽 height:高 volume:体积 weight:重量 po_location_id:目的仓对应的主库位
                #financial_number:金融单号 financial_fg：金融属性 can_select_qty:sku 数量
        #vals.update({'length':1,'width':1,'height':1,'volume':1,'weight':1,'po_location_id':1,'financial_number':'12345','financial_fg':True})
        new_obj=pack_obj.create(vals)
        return new_obj.id

    ########封装 盘点的创建方法
    def create_inventory(self, vals_inv_create,vals_inv_write):
        pack_obj=self.env['stock.inventory']
        ######源生字段
                # name 盘点单号 location_id:仓库主库位 date:盘点时间 filter 类型 package_id 包裹ID
        #vals = {'name': 'ABCD', 'location_id':15,'date':'2017-05-27','filter':'pack','package_id':7}
        ######新加字段
                #inventory_type:盘点类型
        vals_inv_create.update({'inventory_type':'begin'})
        # vals_line=[]
        # for line in range(1):
        #     #######源生字段
        #     each={'product_id': 51058, 'location_id':15,'package_id':7,'product_qty':15}
        #     vals_line.append((0, 0, each))
        #####创建盘点单
        new_obj=pack_obj.create(vals_inv_create)
        #####确认盘点单
        new_obj.prepare_inventory()
        #####写盘点单明细
        new_obj.write(vals_inv_write)
        #####完成盘点单
        new_obj.action_done()
        return new_obj.id
    ##########取得默认参数
    def get_parm(self):
        cr = self.env.cr
        ##############################默认参数获取
        sql = """select lot_stock_id from stock_warehouse where id=1"""
        cr.execute(sql)
        fet = cr.fetchall()
        # 默认中转仓库位
        template_location_id = fet[0][0]
        # 默认平台
        sql = """select id from res_partner where ref='ebay' limit 1"""
        cr.execute(sql)
        fet = cr.fetchall()
        #默认移动仓库位
        template_partner_id = fet[0][0]
        sql="""select lot_stock_id from stock_warehouse where name='移动仓'"""
        cr.execute(sql)
        fet=cr.fetchall()
        template_location_id2=fet[0][0]
        return (template_location_id,template_partner_id,template_location_id2)
    ########打标记
    def get_flag(self,keys):
        sql="""insert into stock_flag(k3_stock_id,ar_sub_group_warehouse_daily_closing_id,OutWarehouseDetail_id,ar_sub_group_receipt_id,new_id,type,remark)
            values(%s,%s,%s,%s,%s,'%s','%s')"""%(keys[0] or 0,keys[1] or 0,keys[2] or 0,keys[3] or 0,keys[4] or 0,keys[5],keys[6])
        self.env.cr.execute(sql)
        return True
    ##########中转仓 数据来源 K3_stock_ar && ar_sub_group_warehouse_daily_closing_0602 && OutWarehouseDetail
    @api.model
    def _import_stock(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action !='create':
            return True
        cr=self.env.cr
        ###########取得默认参数
        template_location_id,template_partner_id,template_location_id2=self.get_parm()
       #####################中转仓#########################################
        k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,remark=(0,0,0,0,0,'zhongzhuan')

        #################### NO.1 取得K3中转仓库存
        sql="""select a.id,a.prod_code,c.id as product_id,a.wh_code,b.fname,d.lot_stock_id as po_location_id,a.quantity ,a.cost,b.ftranstype
             from K3_Stock_ar a
             inner join MultiDestinationWarehouse b on b.fwarehouseid=cast(a.wh_code as int)
             inner join MultiDestinationWarehouse b2 on b.fdstockid=b2.fwarehouseid
             inner join product_product c on c.default_code=a.prod_code
             inner join stock_warehouse d on d.name=b2.fname
             where a.quantity<>0 and a.flag='f' and b.ftype='中转仓'
              limit %s"""%limit
        cr.execute(sql)
        k3_fet=cr.dictfetchall()
        for line in k3_fet:
            k3_stock_id=line['id']
            transport=line['ftranstype']=='空运' and 'air' or 'sea'
            ##################NO.2 从Ar中获取平台比例
            sql="""select c.id as partner_id,b.platforms,sum(a.quantity) from ar_sub_group_warehouse_daily_closing_0602  a
                    inner join MultiDestinationWarehouse a2 on cast(a2.fwarehouseid as int)=a.warehouseid
                    inner join ar_sub_group b on b.subgroupid=a.subgroupid and b.warehouseid=a2.fdstockid
                    inner join res_partner c on c.ref=b.platforms --and c.platform_fg is True
                    where quantity<>0  and a.warehouseid='%s'        and a.sku='%s'
                    group by b.platforms,c.id"""%(line['wh_code'],line['prod_code'])
            cr.execute(sql)
            ar_fet=cr.dictfetchall()
            if not ar_fet:
                #########样品仓 坏品仓等
                volume = 1000
                financial_fg =  False
                vals_pack = {'packaging_id': 1, 'length': 10, 'width':10,
                             'height':10,
                             'volume': volume, 'weight': 10, 'po_location_id': line['po_location_id'],
                             'financial_number': '', 'financial_fg': financial_fg,'transport':transport}
                #########创建空包裹
                new_id =pack_id= self.create_pack(vals_pack)
                keys=(k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,'pack',remark)
                self.get_flag(keys)
                #######包裹打标记
                vals_inv_create = {'name': '没有Ar_sub', 'location_id': template_location_id, 'date': '2017-05-01',
                                   'filter': 'pack', 'package_id': pack_id}
                each = {'product_id': line['product_id'], 'location_id':template_location_id, 'package_id': pack_id,
                        'product_qty': line['quantity'], 'partner_id': template_partner_id }
                vals_line = [(0, 0, each)]
                vals_inv_write = {'line_ids': vals_line, 'date':  '2017-05-01',}
                #########创建盘点单
                new_id=self.create_inventory(vals_inv_create, vals_inv_write)
                keys=(k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,'inventory',remark)
                self.get_flag(keys)
                #######盘点单打标记
            else:
                for ar_line in ar_fet:
                    #ar_sub_group_warehouse_daily_closing_id=ar_line['id']
                    ###################从detail里按日期获取明细 逐条扣减数量
                    sum=ar_line['sum']
                    platforms=ar_line['platforms']
                    sql="""select a.id,fProOutWarehouseNo as fno,fqty,flength,fwidth,fhigh,fpositions,fparcelcode,b.id as packaging_id,fisfinancial,fcreatetime-interval'8 hours' as fcreatetime from
                            OutWarehouseDetail a
                            left join product_packaging b on b.name=a.fparcelcode
                            where (fStatus='拣货前' or fStatus='拣货中' or fStatus='拣货完成' or fStatus='出库确认')
                             and  fqty<>0 and fwarehouse='%s'  and fcode='%s' order by fcreatetime desc"""%(line['fname'],line['prod_code'])
                    cr.execute(sql)
                    detail_fet=cr.dictfetchall()
                    if detail_fet:
                        for detail_line in detail_fet:
                            outwarehousedetail_id=detail_line['id']
                            qty=min(sum,detail_line['fqty'])
                            sum=sum-detail_line['fqty']
                            volume=detail_line['flength']*detail_line['fwidth']*detail_line['fhigh']*0.01**3
                            financial_fg=detail_line['fisfinancial']!='非金融' and True or False
                            financial_number=detail_line['fisfinancial']!='非金融' and detail_line['fisfinancial'] or None
                            vals_pack = {'packaging_id': detail_line['packaging_id'],'length':detail_line['flength'],'width':detail_line['fwidth'],'height':detail_line['fhigh'],
                                         'volume':volume,'weight':1,'po_location_id':line['po_location_id'],'financial_number':financial_number,'financial_fg':financial_fg,'transport':transport}
                            ###########大包裹统一处理
                            # if detail_line['fno']:
                            #     sql="""select id from stock_quant_package where name='%s'"""%(detail_line['fno'])
                            #     cr.execute(sql)
                            #     father_id=cr.fetchone()
                            #     if not father_id:
                            #         father_id=self.create_pack(vals_pack)
                            #         # keys = (k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                            #         #         ar_sub_group_receipt_id, new_id, 'pack')
                            #         # self.get_flag(keys)
                            #     else:
                            #         father_id=father_id[0]
                            #     vals_pack.update({'parent_id':father_id})
                            #########创建空包裹
                            new_id=pack_id=self.create_pack(vals_pack)
                            keys = (k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                                    ar_sub_group_receipt_id, new_id, 'pack',remark)
                            self.get_flag(keys)
                            vals_inv_create= {'name': 'aaaa', 'location_id': template_location_id, 'date': detail_line['fcreatetime'], 'filter': 'pack', 'package_id': pack_id}
                            each = {'product_id': line['product_id'], 'location_id': template_location_id, 'package_id': pack_id, 'product_qty': qty,'partner_id':ar_line['partner_id']}
                            vals_line=[(0, 0, each)]
                            vals_inv_write = {'line_ids':vals_line,'date':detail_line['fcreatetime']}
                            #########创建盘点单
                            new_id=self.create_inventory(vals_inv_create,vals_inv_write)
                            keys = (k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                                    ar_sub_group_receipt_id, new_id, 'inventory',remark)
                            self.get_flag(keys)
                            if int(sum)<=0:
                                break
                    else:
                        #########样品仓 坏品仓等
                        volume = 1000
                        financial_fg = False
                        vals_pack = {'packaging_id': 1, 'length': 10, 'width': 10,
                                     'height': 10,
                                     'volume': volume, 'weight': 10, 'po_location_id': line['po_location_id'],
                                     'financial_number': '', 'financial_fg': financial_fg}
                        #########创建空包裹
                        new_id=pack_id = self.create_pack(vals_pack)
                        keys = (k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                                ar_sub_group_receipt_id, new_id, 'pack',remark)
                        self.get_flag(keys)

                        vals_inv_create = {'name': 'aaaa', 'location_id': template_location_id, 'date': '2017-05-01',
                                           'filter': 'pack', 'package_id': pack_id}

                        each = {'product_id': line['product_id'], 'location_id': template_location_id,
                                'package_id': pack_id,
                                'product_qty': line['quantity'], 'partner_id': template_partner_id}
                        vals_line = [(0, 0, each)]
                        vals_inv_write = {'line_ids': vals_line, 'date': '2017-05-01', }
                        #########创建盘点单
                        new_id = self.create_inventory(vals_inv_create, vals_inv_write)
                        keys = (k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id,
                                ar_sub_group_receipt_id, new_id, 'inventory', remark)
                        self.get_flag(keys)

            ##################打标记
            sql="""update K3_Stock_ar set flag=True where wh_code='%s'   and prod_code='%s'"""%(line['wh_code'],line['prod_code'])
            cr.execute(sql)
        return True
   ###############目的仓 K3_stock &&  ar_sub_group_warehouse_daily_closing_0602
    @api.model
    def _import_stock_dest(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action !='create':
            return True
        cr=self.env.cr
        k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,remark = (0, 0, 0, 0, 0,'dest')
        ##############################默认参数获取
        template_location_id, template_partner_id,template_location_id2 = self.get_parm()
       #####################中转仓#########################################
        #################### NO.1 取得K3中转仓库存
        sql="""select a.id,a.prod_code,c.id as product_id,a.wh_code,b.fname,d.lot_stock_id as location_id,a.quantity ,a.cost,b.ftranstype
             from K3_Stock_ar a
             inner join MultiDestinationWarehouse b on b.fwarehouseid=cast(a.wh_code as int)
             inner join product_product c on c.default_code=a.prod_code
             inner join stock_warehouse d on d.name=b.fname
             where a.quantity<>0 and a.flag='f' and b.ftype='发货仓'
              limit %s"""%limit
        cr.execute(sql)
        k3_fet=cr.dictfetchall()
        for line in k3_fet:
            k3_stock_id=line['id']
            ##################NO.2 从Ar中获取平台比例
            sql="""select c.id as partner_id,b.platforms,sum(a.quantity) from ar_sub_group_warehouse_daily_closing_0602  a
                    inner join MultiDestinationWarehouse a2 on cast(a2.fwarehouseid as int)=a.warehouseid
                    inner join ar_sub_group b on b.subgroupid=a.subgroupid and b.warehouseid=a2.fdstockid
                    inner join res_partner c on c.ref=b.platforms --and c.platform_fg is True
                    where quantity<>0  and a.warehouseid='%s'        and a.sku='%s'
                    group by b.platforms,c.id"""%(line['wh_code'],line['prod_code'])
            cr.execute(sql)
            ar_fet=cr.dictfetchall()
            if ar_fet:
                vals_inv_create = {'name': 'dest', 'location_id': line['location_id'], 'date': '2017-05-01',
                                   'filter': 'partial'}
                vals_line=[]
                for ar_line in ar_fet:
                    #ar_sub_group_warehouse_daily_closing_id = line['id']
                    each = {'product_id': line['product_id'], 'location_id': line['location_id'],
                            'product_qty': ar_line['sum'], 'partner_id': ar_line['partner_id']}
                    each_copy=each.copy()
                    vals_line.append((0,0,each_copy))
                vals_inv_write = {'line_ids': vals_line, 'date': '2017-05-01', }
                #########创建盘点单
                new_id=self.create_inventory(vals_inv_create, vals_inv_write)
                keys=(k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,'inventory',remark)
                self.get_flag(keys)
            else:
                #########样品仓 坏品仓等
                vals_inv_create = {'name': 'dest', 'location_id': line['location_id'], 'date': '2017-05-01',
                                   'filter': 'partial'}
                each = {'product_id': line['product_id'], 'location_id':line['location_id'],
                        'product_qty': line['quantity'], 'partner_id': template_partner_id }
                vals_line = [(0, 0, each)]
                vals_inv_write = {'line_ids': vals_line, 'date':  '2017-05-01',}
                #########创建盘点单
                new_id=self.create_inventory(vals_inv_create, vals_inv_write)
                keys = (
                k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id,
                new_id, 'inventory',remark)
                self.get_flag(keys)
            ##################打标记
            sql="""update K3_Stock_ar set flag=True where wh_code='%s'   and prod_code='%s'"""%(line['wh_code'],line['prod_code'])
            cr.execute(sql)
        return True
    #############移动仓 数据来自ar_sub_group_receipt_0602
    @api.model
    def _import_stock_move(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        if action !='create':
            return True
        cr=self.env.cr

        k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id, new_id,remark= ( 0, 0, 0, 0, 0,'move')
        ##############################默认参数获取
        template_location_id2, template_partner_id,template_location_id = self.get_parm()
       #####################移动仓#########################################
        #################### NO.1
        sql="""----1 从源数据得到 产品 目的仓 平台 入库日期 运输方式 缺少包裹信息，金融属性性质 （ar_sub_group_receipt ar_sub_group MultiDestinationWarehouse ）
                select a.id,g.id as product_id,a.sku,a.quantity,d.fname,b.platforms,a.date-interval'8 hours'as date,c.ftranstype,
                e.lot_stock_id as po_location_id,f.id as partner_id
                from ar_sub_group_receipt_0602 a
                inner join ar_sub_group  b on a.subGroupId=b.subGroupId and a.warehouseIn=b.warehouseId
                ---中转仓库
                inner join MultiDestinationWarehouse c on c.fwarehouseid=a.warehousesource
                --目的仓
                inner join MultiDestinationWarehouse d on d.fwarehouseid=a.warehousein
                inner join stock_warehouse e on e.name=d.fname
                inner join res_partner f on f.ref=b.platforms
                inner join product_product g on g.default_code=a.sku
                where  stage='移仓阶段'
                 and a.flag=False limit %s"""%limit
        cr.execute(sql)
        ar_fet=cr.dictfetchall()
        for line in ar_fet:
            transport=line['ftranstype']=='空运' and 'air' or 'sea'
            ar_sub_group_receipt_id=line['id']
            #########默认包裹
            volume = 1000
            financial_fg =  False
            vals_pack = {'packaging_id': 1, 'length': 10, 'width':10,
                         'height':10,
                         'volume': volume, 'weight': 10, 'po_location_id': line['po_location_id'],
                         'financial_number': '', 'financial_fg': financial_fg,'transport':transport}
            #########创建空包裹
            new_id=pack_id = self.create_pack(vals_pack)
            keys = (
            k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id,
            new_id, 'pack',remark)
            self.get_flag(keys)
            vals_inv_create = {'name': 'move', 'location_id': template_location_id, 'date': line['date'],
                               'filter': 'pack', 'package_id': pack_id}
            each = {'product_id': line['product_id'], 'location_id':template_location_id, 'package_id': pack_id,
                    'product_qty': line['quantity'], 'partner_id': line['partner_id'] }
            vals_line = [(0, 0, each)]
            vals_inv_write = {'line_ids': vals_line, 'date':  line['date'],}
            #########创建盘点单
            new_id=self.create_inventory(vals_inv_create, vals_inv_write)
            keys = (
            k3_stock_id, ar_sub_group_warehouse_daily_closing_id, outwarehousedetail_id, ar_sub_group_receipt_id,
            new_id, 'inventory',remark)
            self.get_flag(keys)
            # ##################打标记
            sql="""update ar_sub_group_receipt_0602 set flag=True where id=%s"""%(line['id'])
            cr.execute(sql)
        return True

    ##########给中转仓和移动仓的包裹加上大包裹
    @api.model
    def _import_bigpack(self, *args):
        if not (args and len(args) == 3 and args[1] in ('create', 'write')):
            return True
        pt, action, limit = args
        ###########中转仓
        sql = """  select b.po_location_id,b.product_id,b.transport from stock_flag a
                  inner join stock_quant_package b on b.id=a.new_id
                  inner join stock_quant c on c.package_id=b.id
                  where a.type='pack' and parent_id is null and c.location_id =(select lot_stock_id from stock_warehouse where id=1)
                  group by b.po_location_id,b.product_id,b.transport  """
        cr = self.env.cr
        pack_obj = self.env['stock.quant.package']
        cr.execute(sql)
        fet = cr.dictfetchall()
        x = 0
        for line_dic in fet:
            x += 1
            line_dic.update({'packaging_id': 1})
            # line_dic={'po_location_id':50,'product_id':18420,'transport':'sea','packaging_id':1}
            new_id = pack_obj.create(line_dic)
            sql = """  select b.id from stock_flag a
              inner join stock_quant_package b on b.id=a.new_id
              inner join stock_quant c on c.package_id=b.id and c.location_id =(select lot_stock_id from stock_warehouse where id=1)
              where a.type='pack' and parent_id is null
              and b.po_location_id=%(po_location_id)s and b.product_id=%(product_id)s and  b.transport='%(transport)s' """ % (
            line_dic)
            cr.execute(sql)
            fet2 = cr.dictfetchall()
            for y in fet2:
                pack_obj.browse(y['id']).write({'parent_id': new_id.id})

        ###########移动仓
        sql = """  select b.po_location_id,b.product_id,b.transport from stock_flag a
                  inner join stock_quant_package b on b.id=a.new_id
                  inner join stock_quant c on c.package_id=b.id
                  where a.type='pack' and parent_id is null and c.location_id =(select lot_stock_id from stock_warehouse where name='移动仓')
                  group by b.po_location_id,b.product_id,b.transport  """
        cr = self.env.cr
        pack_obj = self.env['stock.quant.package']
        cr.execute(sql)
        fet = cr.dictfetchall()
        x = 0
        for line_dic in fet:
            x += 1
            line_dic.update({'packaging_id': 1})
            # line_dic={'po_location_id':50,'product_id':18420,'transport':'sea','packaging_id':1}
            new_id = pack_obj.create(line_dic)
            sql = """  select b.id from stock_flag a
              inner join stock_quant_package b on b.id=a.new_id
              inner join stock_quant c on c.package_id=b.id and c.location_id =(select lot_stock_id from stock_warehouse where name='移动仓')
              where a.type='pack' and parent_id is null
              and b.po_location_id=%(po_location_id)s and b.product_id=%(product_id)s and  b.transport='%(transport)s' """ % (
            line_dic)
            cr.execute(sql)
            fet2 = cr.dictfetchall()
            for y in fet2:
                pack_obj.browse(y['id']).write({'parent_id': new_id.id})
        return True













