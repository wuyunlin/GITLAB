# -*- coding: utf-8 -*-

{
    'name': 'China - Accounting Reports',
    'version': '1.0',
    'category': 'Localization',
    'author': 'OSCG',
    'description': """
        Accounting reports for China
    """,
    'depends': [
        'l10n_cn', 'account_reports'
    ],
    'data': [
        'data/account_financial_html_report_data.xml',
        'account_move_print.xml',
        'account_move_report.xml',
    ],
    'installable': True,
    'auto_install': True,
    'website': 'https://www.zhiyunerp.com',
    'license': 'OEEL-1',
}
