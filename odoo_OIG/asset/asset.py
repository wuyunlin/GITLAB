﻿# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################

from odoo import api, fields, models
from odoo import tools
from odoo.exceptions import  ValidationError
import xlrd
import base64
STATE_COLOR_SELECTION = [
    ('0', 'Red'),
    ('1', 'Green'),
    ('2', 'Blue'),
    ('3', 'Yellow'),
    ('4', 'Magenta'),
    ('5', 'Cyan'),
    ('6', 'Black'),
    ('7', 'White'),
    ('8', 'Orange'),
    ('9', 'SkyBlue')
]
###########################sql导出excel 以后移动到公共模块
import odoo
from odoo import http
from cStringIO import StringIO
from xlsxwriter.workbook import Workbook
from odoo.http import content_disposition, dispatch_rpc, request, \
                      serialize_exception as _serialize_exception
import re
import datetime
class asset_state(models.Model):
    """ 
    Model for asset states.
    """
    _name = 'asset.state'
    _description = 'State of Asset'
    _order = "sequence"

    STATE_SCOPE_TEAM = [
        ('0', 'Finance'),
        ('1', 'Warehouse'),
        ('2', 'Manufacture'),
        ('3', 'Maintenance'),
        ('4', 'Accounting')
    ]

    name = fields.Char('State', size=64, required=True, translate=True)
    sequence = fields.Integer('Sequence', help="Used to order states.", default=3)
    state_color = fields.Selection(STATE_COLOR_SELECTION, 'State Color')
    team = fields.Selection(STATE_SCOPE_TEAM, 'Scope Team')

    def change_color(self):
        color = int(self.state_color) + 1
        if (color>9): color = 0
        return self.write({'state_color': str(color)})


class asset_category(models.Model):
    _description = u'资产类别'
    _name = 'asset.category'

    name = fields.Char('名称', required=True, translate=True)
    asset_ids = fields.Many2many('asset.asset', id1='category_id', id2='asset_id', string='Assets')
    ###新加字段 配置序列号ir.sequence
    ir_sequence_id = fields.Many2one('ir.sequence', u'序号规则', )


class asset_asset(models.Model):
    """
    Assets
    """
    _name = 'asset.asset'
    _order='asset_category_id'
    _description = u'资产'
    _inherit = ['mail.thread']

    def _read_group_state_ids(self, domain, read_group_order=None, access_rights_uid=None, team='3'):
        access_rights_uid = access_rights_uid or self.uid
        stage_obj = self.env['asset.state']
        order = stage_obj._order
        # lame hack to allow reverting search, should just work in the trivial case
        if read_group_order == 'stage_id desc':
            order = "%s desc" % order
        # write the domain
        # - ('id', 'in', 'ids'): add columns that should be present
        # - OR ('team','=',team): add default columns that belongs team
        search_domain = []
        search_domain += ['|', ('team','=',team)]
        #search_domain += [('id', 'in', ids)]
        stage_ids = stage_obj._search(search_domain, order=order, access_rights_uid=access_rights_uid)
        result = stage_obj.name_get(access_rights_uid, stage_ids)
        # restore order of the search
        result.sort(lambda x,y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))
        return result, {}    

    def _read_group_finance_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '0')

    def _read_group_warehouse_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '1')

    def _read_group_manufacture_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '2')

    def _read_group_maintenance_state_ids(self, domain, read_group_order=None, access_rights_uid=None):
        return self._read_group_state_ids(domain, read_group_order, access_rights_uid, '3')
    def _default_state(self):
        return self.env['asset.state'].search([],limit=1) and self.env['asset.state'].search([],limit=1).id

    CRITICALITY_SELECTION = [
        ('0', 'General'),
        ('1', 'Important'),
        ('2', 'Very important'),
        ('3', 'Critical')
    ]
    name = fields.Char(u'计算机名称', size=64)
    finance_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','0')])
    warehouse_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','1')])
    manufacture_state_id = fields.Many2one('asset.state', 'State', domain=[('team','=','2')])
    maintenance_state_id = fields.Many2one('asset.state', u'状态',default=_default_state )
    maintenance_state_color = fields.Selection(related='maintenance_state_id.state_color', selection=STATE_COLOR_SELECTION, string="Color", readonly=True)
    criticality = fields.Selection(CRITICALITY_SELECTION, 'Criticality')
    property_stock_asset = fields.Many2one(
        'stock.location', u"安装位置",
        company_dependent=True, domain=[('usage', 'like', 'asset')],
        help="This location will be used as the destination location for installed parts during asset life.")
    user_id = fields.Many2one('res.users', u'使用人', track_visibility='onchange')
    active = fields.Boolean(u'有效', default=True)
    asset_number = fields.Char(u'资产编号', size=64)
    model = fields.Char('Model', size=64)
    serial = fields.Char(u'SN号', size=64)
    vendor_id = fields.Many2one('res.partner', 'Vendor')
    manufacturer_id = fields.Many2one('res.partner', 'Manufacturer')
    start_date = fields.Date('Start Date')
    purchase_date = fields.Date(u'采购日期')
    warranty_start_date = fields.Date('Warranty Start')
    warranty_end_date = fields.Date('Warranty End')
    image = fields.Binary("Image")
    image_small = fields.Binary("Small-sized image")
    image_medium = fields.Binary("Medium-sized image")
    category_ids = fields.Many2many('asset.category', id1='asset_id', id2='category_id', string='Tags')
    asset_number = fields.Char(u'资产编号', size=64)

    #####新加字段
    user = fields.Char(u'使用人', size=64)
    wired_mac = fields.Char(u'有线MAC地址', size=64)
    wiredless_mac = fields.Char(u'无线MAC地址', size=64)
    ip0 = fields.Char(u'IP', size=64,default='192')
    ip1 = fields.Char(u'IP', size=64, default='160')
    ip2 = fields.Char(u'IP', size=64, default='0')
    ip3 = fields.Char(u'IP', size=64)
    ip = fields.Char(u'IP', size=64)
    ip = fields.Char(compute='_compute_ip', string='IP',store=True)
    asset_brand_id = fields.Many2one('asset.brand', u'品牌')
    asset_brand_id2 = fields.Many2one('asset.brand', u'品牌2')
    asset_model_id = fields.Many2one('asset.model', u'型号')
    asset_category_id = fields.Many2one('asset.category', u'资产类别')
    asset_location_id = fields.Many2one('asset.location', u'安装位置')
    note = fields.Text( u'备注')
    _sql_constraints = [
            ('asset_number_unique', 'unique (asset_number)', u"资产编号不能重复"),
    ]

    @api.one
    @api.depends('ip0', 'ip1', 'ip2','ip3')
    def _compute_ip(self):
        for source in self:
            source.ip = '%s.%s.%s.%s'%(source.ip0,source.ip1,source.ip2,source.ip3 or '')

    _group_by_full = {
        'finance_state_id': _read_group_finance_state_ids,
        'warehouse_state_id': _read_group_warehouse_state_ids,
        'manufacture_state_id': _read_group_manufacture_state_ids,
        'maintenance_state_id': _read_group_maintenance_state_ids,
    }

    # def _compute_enterprise(self):
    #     for each in self:
    #         each.enterprise=self.env.user.company_id.name
    # enterprise = fields.Char(compute='_compute_enterprise')

    @api.onchange('asset_brand_id')
    def onchange_asset_brand_id(self):
        res = {}
        # If no UoM or incorrect UoM put default one from product
        if self.asset_brand_id:
            res['domain'] = {'asset_brand_id2': [('id', '=', self.asset_brand_id.id)]}
        return res


    @api.model
    def create(self, vals):
        tools.image_resize_images(vals)

        ###取得序列号
        if ( not vals.get('asset_number') ) and vals.get('asset_category_id',0):
            category= self.env['asset.category'].browse(vals.get('asset_category_id',0))
            code=category.ir_sequence_id and category.ir_sequence_id.code
            if code:
                vals['asset_number'] = self.env['ir.sequence'].next_by_code(code) or ''
        return super(asset_asset, self).create(vals)

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})
        default.update({'asset_number':''}
           )
        return super(asset_asset, self).copy(default)

    @api.multi
    def action_duplicate(self):
        self.ensure_one()
        mass_mailing_copy = self.copy()
        if mass_mailing_copy:
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'asset.asset',
                'res_id': mass_mailing_copy.id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
                #'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},

            }
        return False







    @api.multi
    def write(self, vals):
        tools.image_resize_images(vals)
        return super(asset_asset, self).write(vals)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


####品牌
class asset_brand(models.Model):
    _description = u'品牌'
    _name = 'asset.brand'
    name = fields.Char(u'名称', required=True)
###型号
class asset_model(models.Model):
    _description = u'型号'
    _name = 'asset.model'
    name = fields.Char(u'名称', required=True)
###安装位置
class asset_model(models.Model):
    _description = u'安装位置'
    _name = 'asset.location'
    name = fields.Char(u'安装位置', required=True)

###导入临时表
class asset_import_wizard(models.TransientModel):
    _name = 'asset.import.wizard'
    _description = u'导入资产'
    flag = fields.Selection(string=u"添加方式", selection=[('0', u"按照资产类别批量添加"), ('1', u"从excel导入")])
    name = fields.Char(string=u'名字',default='资产批量导入')
    asset_category_id = fields.Many2one('asset.category', u'资产类别')
    times = fields.Integer(string=u'导入资产个数')
    file = fields.Binary(string=u'导入文件', filters='*.xls')
    @api.multi
    def import_asset(self):
        new_ids = []
        flag=self.flag
        ####按excel导入
        if flag=='1':
            try:
                excel = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
            except:
                raise ValidationError('请选择导入文件。')
            sh = excel.sheet_by_index(0)
            #按照excel内容创建资产  优先根据这里进行匹配 匹配不上去数据库查找
            template_dic={'计算机名称':['name','char',''],'安装位置':['asset_location_id','many2one','asset.location'],'资产类别':['asset_category_id','many2one','asset.category'],
                          '资产编号':['asset_number','char',''],'使用人':['user','char',''],'SN号':['serial','char',''],'采购日期':['purchase_date','date',''],
                          '有线MAC地址':['wired_mac','char',''],'无线MAC地址':['wiredless_mac','char',''],'品牌':['asset_brand_id','many2one','asset.brand'],
                          '型号':['asset_model_id','many2one','asset.model'],'IP':['ip','char',''],'状态':['maintenance_state_id','many2one','asset.state'],'备注':['note','char','']
                          }
            def get_now_dic(template_list):
                now_dic={}
                for index in range(len(template_list)):
                    field=template_list[index]
                    if template_dic.get(field):
                        now_dic[index]=template_dic.get( field )
                    else:
                        continue
                        #从数据库取 暂时不做 注意翻译问题
                        #sql="""select name,field_description from ir_model_fields where model='asset.asset'"""
                return now_dic
            def create_asset_excel(data,now_dic):
                values={}
                for index in range(len(data)):
                    field,type,related_model=now_dic[index]
                    value=data[index]
                    if type=='many2one':
                        related_id=self.env[related_model].search([('name','=',value)],limit=1)
                        if related_id:
                            values.update({field: related_id.id})
                    elif type=='date':
                        if value:
                            values.update({field:value})
                    else:
                        if value:
                            values.update({field:value})
                ######对IP字段做特殊处理
                if values.get('ip'):
                    ip=values.get('ip')
                    x=0
                    for i in ip.split('.'):
                        values.update({'ip%s'%x:i})
                        x=x+1
                asset_number=values.get('asset_number')
                if asset_number and self.env['asset.asset'].search([('asset_number','=',asset_number)]):
                    self.env['asset.asset'].search([('asset_number', '=', asset_number)]).write(values)
                    return self.env['asset.asset'].search([('asset_number','=',asset_number)]).id
                else:
                    return self.env['asset.asset'].create(values).id

            for rx in range(sh.nrows):
                if rx==0:
                    template_list = [str(sh.cell(rx, i).value or '') for i in range(13)]
                else:
                    data = [str(sh.cell(rx, i).value or '') for i in range(13)]
                    now_dic=get_now_dic(template_list)
                    new_ids.append(create_asset_excel(data,now_dic))
            #对Ip做处理

        #按资产类别导入
        else:
            if (not self.asset_category_id ) or not (self.times):
                raise ValidationError('请填写资产类别和导入资产的个数')
            #按照资产类别批量创建资产
            def create_asset():
                return self.env['asset.asset'].create({'asset_category_id':self.asset_category_id.id})
            for i in range(0,self.times):
                new_ids.append(create_asset().id)
        [action] = self.env.ref('asset.action_assets').read()
        action['domain'] = [('id', 'in', new_ids)]
        return action


















