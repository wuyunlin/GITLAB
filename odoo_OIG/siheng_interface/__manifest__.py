# -*- coding: utf-8 -*-
{
    'name': "SiHeng Interface",

    'summary': """
        """,

    'description': """
        四衡接口模块
    """,

    'author': "siheng",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # for the full list
    'category': 'SiHeng',
    'version': '0.1',
    'sequence': 1000,
    # any module necessary for this one to work correctly
    'depends': [
        'base',
    ],


}
