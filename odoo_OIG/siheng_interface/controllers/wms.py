# -*- coding: utf-8 -*-
from odoo import http,SUPERUSER_ID
import odoo
from datetime import datetime, timedelta
from odoo.http import request
from odoo.addons.web.controllers.main import DataSet
from common import image_url, utcStrTime, utctime_start_thisday, localizeStrTime, cn_strftime_zeropoint, utctime_start_lastdays, cn_current_year_and_date
from common import cn_yesterday_year_and_date
from common import utc_strftime_zeropoint
from common import check_params
from common import create_bill
from common import create_line_bill
from common import create_log_bill
from common import update_log_bill
import odoo.tools.config as config
from odoo.tools import float_compare
import logging
import json
import time
from common import utcStrTime,utc_strftime_zeropoint,utctime_start_lastdays,utc_strftime_zeropoint
import requests
from odoo.exceptions import ValidationError
_logger = logging.getLogger(__name__)

def predispatch(func):  # 分发前操作，目前主要是登录
    def _predispatch(*args, **kwargs):
        _logger.debug("Begin predispatch ...")

        if not request.session.uid:  # 如果session过期或者不存在，尝试登陆
            db = config.get('db_name',"TEST_0228")   # 直接读取odoo config 文件
            login = config.get('admin',"admin")
            password = config.get('admin',"admin")

            #_logger.error("public_user:" +login)
            dbs = odoo.service.db.list_dbs(False)
            if db not in dbs:
                return {
                    "title": "用户初始化",
                    "error": "数据库不存在！"
                }
            new_context = request._context.copy()
            new_context.update({'lang': 'zh_CN'})
            request._context = new_context
            request.session.authenticate(db, login, password)
        if not request.session.uid:
            return {
                "title": "用户初始化",
                "error": "用户名或密码错误，请重新登录！"
            }
        request.uid = request.session.uid
        if request.uid:
            request.env.uid = request.uid
        _logger.debug("End predispatch ...\n")
        return func(*args, **kwargs)
    return _predispatch

class vmcController(DataSet):
    @http.route(['/api/wms'], type='json', auth="none")
    def vmc_call_kw(self, *args, **kwargs):
        _logger.info("请求参数:" + str(kwargs))
        result = self.wms_version_control(self, *args, **kwargs)
        if result.get('returnFlag') == '0' and kwargs.get('except_id'):
            # 封装日志信息
            query_sql = """select id,error_message,create_date from interface_log where id = '%s'"""%(kwargs.get('except_id'))
            request.cr.execute(query_sql)
            query_result = request.cr.dictfetchone()
            params = {
                'request_parameter': json.dumps(kwargs),
                'record_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                'request_way': 'ask_inside',
                'record_state': result.get('returnFlag') == '0' and 'Unsolved' or 'ok',
                'error_message': query_result['create_date']+"的错误信息：" + query_result['error_message'] + ";+++++++"+datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"重新请求，错误信息：+++++++"+result.get('result').get('message'),
                'method': kwargs.get('method'),
                'except_id': kwargs.get('except_id')}
            # 更新日志记录
            interface_log_id = update_log_bill(**params)
        else:
            # 封装日志信息
            params = {
                'request_parameter': json.dumps(kwargs),
                'record_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                'request_way': 'ask_inside',
                'record_state': result.get('returnFlag') == '0' and 'Unsolved' or 'ok',
                'error_message':  result.get('returnFlag') == '0' and result.get('result').get('message') or 'ok',
                'method': kwargs.get('method')}
            # 生成日志记录
            interface_log_id = create_log_bill(**params)

        ### 若是采购入库回传接口，并且message等于‘未找到对应采购单，请检查！’，则将数据插入purchase_stockin_log表中
        if kwargs.get('method') == 'PurchaseStockInOrder' and result.get('result').get('message') == '未找到对应采购单，请检查！':
            for line in kwargs.get('data'):
                params = (kwargs.get('purchaser_no'),kwargs.get('origin'),kwargs.get('remark'),line.get('default_code'),
                          line.get('product_qty'),line.get('refuse_qty'),line.get('sample_qty'),line.get('line_remark'),
                          datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),'0')
                sql = """ insert into purchase_stockin_log (purchaser_no,origin,remark,default_code,product_qty,refuse_qty,
                                sample_qty,line_remark,create_time,state)
                                values ('%s','%s','%s','%s',%s,%s,%s,'%s','%s','%s')""" %(params)
                request.cr.execute(sql)

        _logger.info("执行完:" + kwargs['method'])
        _logger.info("返回参数:" + str(result))
        return result
    #版本控制
    def wms_version_control(self, *args, **kwargs):
        app_version = kwargs.get('app_version', False)
        _logger.error(app_version)
        if not app_version:
            res = eval("self." + kwargs['method'])(*args, **kwargs)
        else:
            res = eval("self." + kwargs['method'])(*args, **kwargs)
        return res

    @predispatch  # 分发前检查登录，如没有登录，使用配置账户登录
    def vmc_machine_init(self, *args, **kwargs):  # 售货机初始化
        _logger.debug("Begin vmc_machine_init ...")
        record={'Flag':123}
        _logger.debug("End vmc_machine_init ...\n")
        return record

    # {
    #      "jsonrpc":"2.0",
    #      "id":1,
    #      "params":{"method":"GetSupplierAccount","supplier_code":[],"start_date":"2017-01-01","end_date":"2018-01-01"}
    # }
    @predispatch
    def GetSupplierAccount(self, *args, **kwargs):  # 供应商结算信息
        _logger.debug("Begin GetSupplierAccount ...")
        _logger.info("供应商结算信息 接收参数###########" + str(kwargs))
        list = ['method', 'supplier_code', 'start_date', 'end_date']
        # 进行必填参数校验
        message = check_params(kwargs, list)
        if message:
            record = {"returnFlag": "0", "result": {"message": message}}
            return record
        # 查询供应商结算信息
        #
        # 设置供应商条件
        # for key in kwargs['supplier_code']:

        try:
            # 设置指定订单号查询条件
            po_para = 'and 1=1' if not kwargs.get('po_name') else """and  purchase_order.name='%s'""" % (kwargs.get('po_name'))
            # 批量查询传入的供应商编码，并进行utf-8解码
            kwargs['supplier_code'] = [each.encode('utf-8') for each in kwargs['supplier_code']]
            supplier_para = 'and 1=1' if not kwargs['supplier_code'] else """ and res_partner.ref in %s""" % (
                tuple(kwargs['supplier_code'] + ['-1']),)
            # 设置查询时间段条件
            # date_para="""and  purchase_order.date_order+interval'8 hour'<='%s' and purchase_order.date_order+interval'8 hour'<='%s'"""%(kwargs['start_date'],kwargs['end_date'])
            date_para = """and  purchase_order.date_order+interval'8 hour'>='%s' and purchase_order.date_order+interval'8 hour'<='%s'""" % (
                kwargs['start_date'], kwargs['end_date'])
            # 查询sql
            sql = """ SELECT
                            purchase_order . NAME AS po_no,
                            purchase_order .date_order + INTERVAL '8 hour' AS date_order,
                            res_partner . REF AS supplier_code,
                            product_template.default_code AS default_code,
                            product_template. NAME AS product_name,
                            purchase_order_line.product_qty AS po_qty,
                            purchase_order_line.qty_received AS stock_qty,
                            purchase_order_line.product_qty - purchase_order_line.qty_received AS unstock_qty,
                            purchase_order_line.price_subtotal AS po_amount,
                            purchase_order_line.qty_paid AS account_amount,
                            purchase_order_line.price_subtotal - purchase_order_line.qty_paid AS unaccount_amount
                        FROM
                            purchase_order
                        INNER JOIN purchase_order_line ON purchase_order_line.order_id = purchase_order . ID
                        INNER JOIN res_partner ON res_partner . ID = purchase_order .partner_id
                        INNER JOIN product_product ON product_product. ID = purchase_order_line.product_id
                        INNER JOIN product_template ON product_template. ID = product_product.product_tmpl_id
                        WHERE
                            purchase_order . STATE IN ('purchase', 'done')
                            %s %s %s
                    """ %(supplier_para, date_para, po_para)
            request.cr.execute(sql)
            result = request.cr.dictfetchall()
        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            _logger.info('sql:' + repr(sql))
            record = {"returnFlag": "0", "result": {"message": "供应商结算信息接口 请求异常 ############# "+(e.message or e.name)}}
            return record
        record = {"returnFlag": "1", "result": {"result": result}}
        #############################################################
        # record = {"returnFlag": "1", "result":{"po_no":"po001","start_date":"2017-01-01","end_date":"2018-01-01","supplier_code":"SC001","default_code": "P001",
        #                                        "product_name": "电脑","po_qty": "100","stock_qty": "90","unstock_qty": "10","account_amount": "900","unaccount_amount": "100"} }
        _logger.debug("End GetSupplierAccount ...\n")
        return record


    # {
    #     "jsonrpc": "2.0",
    #     "id": 1,
    #     "params": {"method": " PurchaseStockInOrder ", "purchaser_no": "PO00024", "origin": "IN001",
    #                "data": [{"default_code": "PROD_DEL02", "product_qty": 1, "refuse_qty": 1, "sample_qty": 1},
    #                         {"default_code": "PROD_DEL", "product_qty": 1, "refuse_qty": 1, "sample_qty": 1}]}
    # }
    @predispatch
    def PurchaseStockInOrder(self, *args, **kwargs):  #	采购入库单回传接口
        _logger.debug("Begin PurchaseStockInOrder ...")
        _logger.info("采购入库单回传接口 接收参数###########"+str(kwargs))
        list = ['method', 'purchaser_no', 'origin']
        message = check_params(kwargs, list)
        if message:
            record = {"returnFlag": "0", "result": {"message": message}}
            return record
        if kwargs.has_key('data'):
            list2 = ['default_code', 'product_qty', 'refuse_qty', 'sample_qty']
            dict = kwargs['data']
            if not dict:
                record = {"returnFlag": "0", "result": {"message": "参数中data字段为空，请检查！"}}
                return record
            for d in dict:
                message2 = check_params(d, list2)
                if message2:
                    record = {"returnFlag": "0", "result": {"message": message2}}
                    return record
        ############################################################
        # 校验入库数量不能为零或负数，抽样，拒收数量不能为负数
        qty_flag = 0
        for line in kwargs['data']:
            if line['product_qty'] <= 0 or line['sample_qty'] < 0 or line['refuse_qty'] < 0:
                qty_flag += 1
        if qty_flag > 0:
            record = {"returnFlag": "0", "result": {"message": "产品入库数量不能为零或负数，抽样、拒收数量不能为负数，请检查！"}}
            return record

        ##########################################################
        try:
            # 获取采购订单号
            purchase_no = kwargs['purchaser_no']
            # 获取WMS入库单单号（ASN单号）
            asnno = kwargs['origin']
            # 通过采购订单号查询订单对象
            pruchase_order = request.env['purchase.order'].search([('name', '=', purchase_no)])
            if not pruchase_order:
                record = {"returnFlag": "0", "result": {"message": "未找到对应采购单，请检查！"}}
                return record
            # 判断采购订单是否关闭
            if pruchase_order.open_state == 'close':
                record = {"returnFlag": "0", "result": {"message": "采购订单已关闭，不能执行入库操作！"}}
                return record
            # 判断此入库单是否重复入库
            stock_in_order = request.env['stock.picking'].search([('origin','=',purchase_no),('asnno','=',asnno)])
            if stock_in_order:
                record = {"returnFlag": "1", "result": {"message": "此采购单已入库，不能重复操作！"}}
                return record
            # 判断入库数量是否大于订单总数量-已入库数量
            for line in kwargs['data']:
                # 获取订单对应产品的订单明细
                purchase_order_line = pruchase_order.order_line.filtered(lambda x: x.product_id.default_code == line['default_code'])
                # 判断采购订单行是否关闭
                if purchase_order_line.open_state == 'close':
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": "采购订单行已关闭，不能执行入库操作！"}}
                    return record
                # 未入库数量
                un_receive_qty = purchase_order_line.product_qty - purchase_order_line.qty_received
                if line['product_qty'] > un_receive_qty:
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": "产品%s的入库数量不能大于未接收数量:%s"%(line['default_code'],un_receive_qty)}}
                    return record

            # 获取可用的采购入库单对象
            stock_picking = pruchase_order.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
            # 获取收货人的分拣类型
            stock_picking_type = pruchase_order.oig_receiver_id.stock_picking_type_id2
            # 源仓库ID
            src_warehouse_id = stock_picking_type.default_location_src_id.id
            # 目的仓库ID
            dest_warehouse_id = stock_picking_type.default_location_dest_id.id
            # 生成抽样单
            num = 0 # 判断是否有样品需要入库 若大于0，则表示需要生成抽样单
            for line in kwargs['data']:
                if line['sample_qty'] > 0:
                    num += 1
            if num > 0:  # 如果有产品抽样数量大于0 的，则生产抽样单，否则不生成
                params1 = {'purchase_no': purchase_no + '抽样单', 'stock_picking_type': stock_picking_type, 'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': dest_warehouse_id}
                picking_sample = create_bill(**params1)
            ##########line 操作
            for line in kwargs['data']:
                # 获取入库单中对应产品，并修改完成数量
                stock_picking.pack_operation_product_ids.filtered(lambda x: x.product_id.default_code == line['default_code']).write({'qty_done': line['product_qty']})
                #########拒收数量填入备注字段
                # 获取拒收数量并保存到订单明细remark字段中
                if line['refuse_qty'] > 0:
                    # 获取采购订单明细对象
                    order_line = pruchase_order.order_line.filtered(lambda x: x.product_id.default_code == line['default_code'])
                    # 获取采购订单明细的备注内容
                    remark = order_line.remark
                    if not remark:
                        remark = '%s %s%s%s' % ( datetime.now().strftime("%Y-%m-%d %H:%M:%S"), '拒收', line['refuse_qty'], '件;\n')
                    else:
                        remark = '%s%s %s%s%s' %(remark,datetime.now().strftime("%Y-%m-%d %H:%M:%S"), '拒收', line['refuse_qty'], '件;\n')
                    # 将拒收数量写入采购订单明细的备注中
                    order_line.write({'remark': remark})

                # 获取产品实例
                product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])[0]
                # 生成抽样单明细
                if num > 0:
                    if line['sample_qty'] > 0:
                        if line['product_qty'] >= line['sample_qty']:
                            params = {'purchase_no': purchase_no, 'picking_instance': picking_sample,
                                      'product_instance': product_instance, 'product_qty': line['sample_qty'],
                                      'src_warehouse_id': src_warehouse_id,'dest_warehouse_id': dest_warehouse_id }
                            move_id = create_line_bill(**params)
                        else:
                            _logger.info('抽样数量不能大于实际入库数量')
                            # raise ValidationError('抽样数量不能大于实际入库数量')
                            request._cr.rollback()
                            record = {"returnFlag": "0", "result": {"message": "抽样数量不能大于实际入库数量"}}
                            return record
            ############主表操作
            for pack in stock_picking.pack_operation_product_ids:
                if pack.qty_done > 0:
                    pack.product_qty = pack.qty_done
                else:
                    pack.unlink()
            stock_picking.do_transfer()
            # 将ASN单号写入入库单中
            stock_picking.write({'asnno':asnno})
            order_no = stock_picking.name
            ############样品入库
            if num > 0:
                picking_sample.action_confirm()
                picking_sample.action_assign()
                for pack in picking_sample.pack_operation_product_ids:
                     pack.qty_done=pack.product_qty
                picking_sample.do_transfer()
        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "采购入库单回传接口 请求异常 #######"+(e.message or e.name)}}
            return record

        record = {"returnFlag": "1", "result": {"order_no": order_no}}
        _logger.debug("End PurchaseStockInOrder ...\n")
        return record

    # {
    #     "jsonrpc": "2.0",
    #     "id": 1,
    #     "params": {"method": "MrpOrder", "mrp_no": "mo001", "default_code ": "OIG-012", " product_qty ": 10,
    #                "origin": "MO001",
    #                "data": [{"default_code": "OIG-123", "line_qty": 100, "scrapt_qty": 0},
    #                         {"default_code": "OIG-456", "line_qty": 100, "scrapt_qty": 0}]}
    # }
    @predispatch
    def MrpOrder(self, *args, **kwargs):  # 加工单处理结果回传接口
        _logger.debug("Begin MrpOrder ...")
        _logger.info("加工单处理结果回传接口 接收参数###########" + str(kwargs))
        list = ['method', 'default_code', 'product_qty', 'origin', 'oig_receiver']
        message = check_params(kwargs, list)
        if message:
            record = {"returnFlag": "0", "result": {"message": message}}
            return record
        if kwargs.has_key('data'):
            list2 = ['default_code', 'line_qty', 'scrapt_qty']
            dict = kwargs['data']
            if not dict:
                record = {"returnFlag": "0", "result": {"message": "参数中data字段为空，请检查！"}}
                return record
            for d in dict:
                message2 = check_params(d, list2)
                if message2:
                    record = {"returnFlag": "0", "result": {"message": message2}}
                    return record
        ############################################################
        # 校验加工数量不为负数
        if kwargs['product_qty'] <= 0:
            record = {"returnFlag": "0", "result": {"message": "加工产品数量不能为负数或零，请检查！"}}
            return record

        # 校验物料产品数量和报废数量不能为负数
        qty_flag = 0
        for line in kwargs['data']:
            if line['line_qty'] < 0 or line['scrapt_qty'] < 0:
                qty_flag += 1
        if qty_flag > 0:
            record = {"returnFlag": "0", "result": {"message": "物料产品数量、报废数量不能为负数，请检查！"}}
            return record
        #######################################################
        try:
            # 获取产品对象
            product_instance = request.env['product.product'].search([('default_code', '=', kwargs['default_code'])])
            # 获取产品模板对象
            product_tmp_instance = request.env['product.template'].search([('default_code', '=', kwargs['default_code'])])
            # 获取物料清单对象
            mrp_bom = request.env['mrp.bom'].search([('product_tmpl_id', '=', product_tmp_instance.id)])
            if not mrp_bom:
                record = {"returnFlag": "0", "result": {"message": '未找到对应的物料清单，请检查！'}}
                return record

            # 通过收货人查找中转仓
            stock_location_id = request.env['oig.receiver'].search([('name', '=', kwargs['oig_receiver'])]).stock_location_id.id
            # 创建制造订单
            man_order = request.env['mrp.production'].sudo().create({
                'product_id': product_instance.id,
                'product_uom_id': product_instance.uom_id.id,
                'product_qty': kwargs['product_qty'],
                'bom_id': mrp_bom.id,
                'location_src_id': stock_location_id,
                'location_dest_id': stock_location_id,
            })
            order_no = man_order.name
            # 设置生产产品数量
            produce_wizard = request.env['mrp.product.produce'].sudo().with_context({
                'active_id': man_order.id,
                'active_ids': [man_order.id],
            }).create({
                'product_qty': kwargs['product_qty'],
            })

            #检查可用
            man_order.action_assign()
            # 判断是否可用
            if man_order.availability != 'assigned':
                # 取消制造订单
                man_order.action_cancel()
                # TODO: 后面需要加日志备注
                request._cr.rollback()
                record = {"returnFlag": "0", "result": {"message": '此制造订单库存不够，不能操作！'}}
                return record
            # 生产
            produce_wizard.do_produce()
            # 调用标记为完成的按钮方法
            man_order.button_mark_done()

            ######将报废产品生成盘亏单
            # 判断是否有报废产品，若有则生成盘亏单
            num = 0
            for line in kwargs['data']:
                if line['scrapt_qty'] > 0:
                    num += 1
            if num > 0:
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '内部调拨')])
                inventory_warehouse_id = request.env['stock.location'].search([('name', '=', '盘点仓')]).id
                # 生成盘库单主单
                params1 = {'purchase_no': order_no , 'stock_picking_type': stock_picking_type,
                           'src_warehouse_id': stock_location_id, 'dest_warehouse_id': inventory_warehouse_id}
                picking_instance = create_bill(**params1)
                for line in kwargs['data']:
                    if line['scrapt_qty'] > 0:
                        # 获取产品实例
                        product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                        # 生成盘亏单明细
                        params = {'purchase_no': order_no, 'picking_instance': picking_instance,
                                      'product_instance': product_instance, 'product_qty': line['scrapt_qty'],
                                      'src_warehouse_id': stock_location_id, 'dest_warehouse_id': inventory_warehouse_id}
                        move_id = create_line_bill(**params)

                # 检查可用
                picking_instance.action_assign()
                # 若盘亏单状态不为可用状态，则取消此盘亏单以及取消制造订单
                if picking_instance.state != 'assigned':
                    # 取消盘亏单
                    picking_instance.action_cancel()
                    # 取消制造订单
                    man_order.action_cancel()
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": '此制造订单库存不够，不能操作！'}}
                    return record
                for pack in picking_instance.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty
                picking_instance.do_transfer()
        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "加工单处理结果回传接口 请求异常 #########"+(e.message or e.name)}}
            return record

        #######################################################
        record = {"returnFlag": "1", "result": {"order_no": order_no}}
        _logger.debug("End MrpOrder ...\n")
        return record


    # {
    #     "jsonrpc": "2.0",
    #     "id": 1,
    #     "params": {"method": "TransOrder", "type ": "01", "origin ": "TRAN001", "src_warehouse": "美国万邑通海运仓",
    #                "dest_warehouse": "美国万邑通空运仓",
    #                "data": [{"default_code": "OIG-123", "qty": 100},
    #                         {"default_code": "OIG-456", "qty": 100}]}
    # }
    @predispatch
    def TransOrder(self, *args, **kwargs):  # 调拨单，调整单，中转出库单等接口
        _logger.debug("Begin TransOrder ...")
        _logger.info("调拨单，调整单，中转出库单等接口 接收参数###########" + str(kwargs))
        list = ['method','type','origin','src_warehouse','dest_warehouse']
        message = check_params(kwargs,list)
        if message:
            record = {"returnFlag": "0", "result": {"message": message}}
            return record
        if kwargs.has_key('data'):
            list2 = ['default_code', 'qty']
            dict = kwargs['data']
            if not dict:
                record = {"returnFlag": "0", "result": {"message": "参数中data字段为空，请检查！"}}
                return record
            for d in dict:
                message2 = check_params(d, list2)
                if message2:
                    record = {"returnFlag": "0", "result": {"message": message2}}
                    return record
        ##########################################################
        try:
            type = kwargs['type']
            origin = kwargs['origin']
            src_warehouse = kwargs['src_warehouse']
            dest_warehouse = kwargs['dest_warehouse']

            # 对产品数量中的负数进行校验（调整单除外）
            if type != '02':
                qty_flag = 0
                for line in kwargs['data']:
                    if line['qty'] <= 0:
                        qty_flag += 1
                if qty_flag > 0:
                    record = {"returnFlag": "0", "result": {"message": '产品数量不能为负数或零，请检查！'}}
                    return record
            # 对中转出库单和退货单明细中的stockin_no字段进行必填校验
            if type == '03' or type == '08':
                for line in kwargs['data']:
                    if line.has_key('stockin_no'):
                        value = line.get('stockin_no')
                        if value == 'None' or value == '':
                            record = {"returnFlag": "0", "result": {"message": '参数中:%s字段为必填字段,请检查！' %('stockin_no')}}
                            return record
                    else:
                        record = {"returnFlag": "0", "result": {"message": '参数中:%s字段不存在,请检查！'%('stockin_no')}}
                        return record


            #############查询数据库中分拣类型，和仓库的id
            # 源仓库
            src_warehouse_id = request.env['stock.location'].search([('name', '=', src_warehouse)]).id
            if not src_warehouse_id:
                record = {"returnFlag": "0", "result": {"message": '源仓库：%s， 未查到，请检查！'%(src_warehouse)}}
                return record
            # 目的仓库
            dest_warehouse_id = request.env['stock.location'].search([('name', '=', dest_warehouse)]).id
            if not dest_warehouse_id:
                record = {"returnFlag": "0", "result": {"message": '目的仓库：%s， 未查到，请检查！'%(dest_warehouse)}}
                return record
            # 盘点仓
            inventory_warehouse_id = request.env['stock.location'].search([('name', '=', '盘点仓')]).id
            if not inventory_warehouse_id:
                record = {"returnFlag": "0", "result": {"message": '仓库：盘点仓， 未查到，请检查！'}}
                return record
            # 其他仓
            other_warehouse_id = request.env['stock.location'].search([('name', '=', '其他仓')]).id
            if not other_warehouse_id:
                record = {"returnFlag": "0", "result": {"message": '仓库：其他仓， 未查到，请检查！'}}
                return record
            # 采销仓
            purchase_sales_warehouse_id = request.env['stock.location'].search([('name', '=', '采销仓')]).id
            if not purchase_sales_warehouse_id:
                record = {"returnFlag": "0", "result": {"message": '仓库：采销仓， 未查到，请检查！'}}
                return record
            ##################

            if type == '01': # 调拨单
                # 分拣类型
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '调拨内部调拨')])
                if not stock_picking_type:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：调拨内部调拨， 未查到，请检查！'}}
                    return record
                # 生成调拨单
                params1 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type,
                           'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': dest_warehouse_id,'remark':kwargs.get('remark')}
                picking_instance = create_bill(**params1)
                for line in kwargs['data']:
                    # 获取产品实例
                    product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                    if not product_instance:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                        return record
                    # 生成调拨单明细
                    params = {'purchase_no': origin, 'picking_instance': picking_instance,
                              'product_instance': product_instance, 'product_qty': line['qty'],
                              'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': dest_warehouse_id,'remark':line.get('remark')}
                    move_id = create_line_bill(**params)

                order_no = picking_instance.name
                # 检查可用
                picking_instance.action_assign()
                # 若调拨单状态不为可用状态，则取消此调拨单
                if picking_instance.state != 'assigned':
                    # 取消调拨单
                    picking_instance.action_cancel()
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": '此调拨单库存不够，不能操作！'}}
                    return record
                for pack in picking_instance.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty
                picking_instance.do_transfer()
            elif type == '02': # 调整单
                # 盘盈分拣类型
                stock_picking_type1 = request.env['stock.picking.type'].search([('name', '=', '盘盈内部调拨')])
                if not stock_picking_type1:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：盘盈内部调拨， 未查到，请检查！'}}
                    return record
                # 盘亏分拣类型
                stock_picking_type2 = request.env['stock.picking.type'].search([('name', '=', '盘亏内部调拨')])
                if not stock_picking_type2:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：盘亏内部调拨， 未查到，请检查！'}}
                    return record
                ##### 判断盘盈盘亏产品
                data1 = []
                data2 = []
                for line in kwargs['data']:
                    if line['qty'] > 0:
                        # 盘盈产品
                        temp1 = {'default_code': line['default_code'], 'qty': line['qty'],'remark':line.get('remark')}
                        data1.append(temp1)
                    elif line['qty'] < 0:
                        # 盘亏产品
                        temp2 = { 'default_code': line['default_code'], 'qty': line['qty'],'remark':line.get('remark')}
                        data2.append(temp2)

                # 生成盘点单（盘亏）需要判断库存
                if len(data2):
                    params2 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type2,
                               'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': inventory_warehouse_id,'remark':kwargs.get('remark')}
                    picking_instance2 = create_bill(**params2)
                    for line in data2:
                        # 获取产品实例
                        product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                        if not product_instance:
                            request._cr.rollback()
                            record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                            return record
                        # 生成盘亏单明细
                        params_two = {'purchase_no': origin, 'picking_instance': picking_instance2,
                                  'product_instance': product_instance, 'product_qty': abs(line['qty']),
                                  'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': inventory_warehouse_id,'remark':line.get('remark')}
                        move_id = create_line_bill(**params_two)

                    order_no = picking_instance2.name
                    # 检查可用
                    picking_instance2.action_assign()
                    # 若盘亏单状态不为可用状态，则取消此盘亏单并且取消盘盈单
                    if picking_instance2.state != 'assigned':
                        # 取消盘亏单
                        picking_instance2.action_cancel()
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": '此调调整单库存不够，不能操作！'}}
                        return record
                    for pack in picking_instance2.pack_operation_product_ids:
                        pack.qty_done = pack.product_qty
                    picking_instance2.do_transfer()

                # 生成盘点单（盘盈）
                if len(data1):
                    params1 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type1,
                               'src_warehouse_id': inventory_warehouse_id, 'dest_warehouse_id': dest_warehouse_id, 'remark':kwargs.get('remark')}
                    picking_instance1 = create_bill(**params1)
                    for line in data1:
                        # 获取产品实例
                        product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                        if not product_instance:
                            request._cr.rollback()
                            record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                            return record
                        # 生成盘点单明细
                        params_one = {'purchase_no': origin, 'picking_instance': picking_instance1,
                                      'product_instance': product_instance, 'product_qty': line['qty'],
                                      'src_warehouse_id': inventory_warehouse_id,
                                      'dest_warehouse_id': dest_warehouse_id,'remark':line.get('remark')}
                        move_id = create_line_bill(**params_one)

                    # 判断是否有盘亏单生成，若有则把盘亏单号和赢亏单号一起返回
                    if len(data2) > 0:
                        order_no = order_no + ';' + picking_instance1.name
                    else:
                        order_no = picking_instance1.name
                    picking_instance1.action_assign()
                    for pack in picking_instance1.pack_operation_product_ids:
                        pack.qty_done = pack.product_qty
                    picking_instance1.do_transfer()

            elif type == '03':   # 中转出库单
                # 分拣类型
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '发运内部调拨')])
                if not stock_picking_type:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：发运内部调拨， 未查到，请检查！'}}
                    return record
                # 移动仓
                move_warehouse_id = request.env['stock.location'].search([('name', '=', src_warehouse)]).stock_location_id_move.id
                if not move_warehouse_id:
                    record = {"returnFlag": "0", "result": {"message": '移动仓库：%s对应的移动仓库， 未查到，请检查！' % (src_warehouse)}}
                    return record
                # 获取中转天数
                transfer_days = request.env['stock.location'].search([('name', '=', src_warehouse)]).transfer_days
                # 计算预计到货日期
                date = (datetime.now()+timedelta(days=transfer_days)).strftime("%Y-%m-%d")
                ############## 根据第三方入库单号进行分组
                data = {}
                for line in kwargs['data']:
                    stockin_no = line['stockin_no']
                    if data.has_key(stockin_no):
                        data[stockin_no].append(line)
                    else:
                        data[stockin_no] = [line]
                ############## 创建中转出库单并进行成本调整
                order_no = ''
                for key in data.keys():
                    # 创建一条补货组记录
                    procurement_group = request.env['procurement.group'].create({'name': key, 'move_type': 'direct'})
                    # 创建中转出库单主表
                    params1 = {'purchase_no': origin+':'+key, 'stock_picking_type': stock_picking_type,
                               'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': move_warehouse_id, 'date':date, 'remark':kwargs.get('remark')}
                    picking_instance = create_bill(**params1)
                    # 更改发运单名称
                    picking_instance.write({'name': origin+':'+key})

                    for line in data[key]:
                        # 获取产品实例
                        product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                        if not product_instance:
                            request._cr.rollback()
                            record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                            return record
                        # 生成中转出库单明细
                        params = {'purchase_no': origin, 'picking_instance': picking_instance,'product_instance': product_instance,
                                  'product_qty': line['qty'],'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': move_warehouse_id,
                                  'group_id':procurement_group.id, 'remark':line.get('remark')}
                        move_id = create_line_bill(**params)

                    order_no += picking_instance.name + ';'
                    # 检查可用
                    picking_instance.action_assign()
                    # 若中转出库单状态不为可用状态，则取消此中转出库单
                    if picking_instance.state != 'assigned':
                        # 取消中转出库单
                        picking_instance.action_cancel()
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": '此中转出库单库存不够，不能操作！'}}
                        return record
                    for pack in picking_instance.pack_operation_product_ids:
                        pack.qty_done = pack.product_qty
                    picking_instance.do_transfer()
                    # 通过发运单号查找对应的移仓入库单
                    stock_picking = request.env['stock.picking'].search([('origin', '=', picking_instance.name)])
                    #########输入预计到达日期
                    if stock_picking:
                        stock_picking.write({'expected_arrival_date':date,'theoretical_arrival_date':date})
                    # 生成成本调整单
                    land_obj = request.env['stock.landed.cost']
                    # 创建成本调整单
                    ########### 备注：将小数准确性中Product Price 改为3，增加Account 为3
                    result = land_obj.auto_cost_adjust(picking_instance)
                    if result['flag'] == 0:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": result['message']}}
                        return record
                    # 通过成本调整单拣货字段查找对应的成本调整单并将名称修改为第三方入库单号
                    stock_landed_cost = request.env['stock.landed.cost'].search([('picking_ids', '=',picking_instance.name )])
                    stock_landed_cost.write({'name':key})



            elif type == '04':   # 其他入库单
                # 分拣类型
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '其他入库内部调拨')])
                if not stock_picking_type:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：其他入库内部调拨， 未查到，请检查！'}}
                    return record
                # 生成其他入库单
                params1 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type,
                           'src_warehouse_id': other_warehouse_id, 'dest_warehouse_id': dest_warehouse_id,'remark':kwargs.get('remark')}
                picking_instance = create_bill(**params1)
                for line in kwargs['data']:
                    # 获取产品实例
                    product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                    if not product_instance:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                        return record
                    # 生成其他入库单明细
                    params = {'purchase_no': origin, 'picking_instance': picking_instance,
                              'product_instance': product_instance, 'product_qty': line['qty'],
                              'src_warehouse_id': other_warehouse_id, 'dest_warehouse_id': dest_warehouse_id, 'remark': line.get('remark')}
                    move_id = create_line_bill(**params)
                # 检查可用
                order_no = picking_instance.name
                picking_instance.action_assign()
                for pack in picking_instance.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty
                picking_instance.do_transfer()

            elif type == '05':   # 其他出库单
                # 分拣类型
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '其他出库内部调拨')])
                if not stock_picking_type:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：其他出库内部调拨， 未查到，请检查！'}}
                    return record
                # 生成其他出库单
                params1 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type,
                           'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': other_warehouse_id,'remark':kwargs.get('remark')}
                picking_instance = create_bill(**params1)
                for line in kwargs['data']:
                    #判断库存是否充足
                    # 获取产品实例
                    product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                    if not product_instance:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                        return record
                    # 生成出库单明细
                    params = {'purchase_no': origin, 'picking_instance': picking_instance,
                              'product_instance': product_instance, 'product_qty': line['qty'],
                              'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': other_warehouse_id, 'remark': line.get('remark')}
                    move_id = create_line_bill(**params)
                order_no = picking_instance.name
                picking_instance.action_assign()
                # 若其他出库单状态不为可用状态，则取消此其他出库单
                if picking_instance.state != 'assigned':
                    # 取消其他出库单
                    picking_instance.action_cancel()
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": '此其他出库单库存不够，不能操作！'}}
                    return record
                for pack in picking_instance.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty

                picking_instance.do_transfer()

            elif type == '06': # 采销单
                # 分拣类型
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '采销调拨内部调拨')])
                # 生成采销调拨单
                params1 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type,
                           'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': purchase_sales_warehouse_id}
                picking_instance = create_bill(**params1)
                for line in kwargs['data']:
                    # 判断库存是否充足
                    # 获取产品实例
                    product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                    if not product_instance:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                        return record
                    # 生成采销调拨单明细
                    params = {'purchase_no': origin, 'picking_instance': picking_instance,
                              'product_instance': product_instance, 'product_qty': line['qty'],
                              'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': purchase_sales_warehouse_id}
                    move_id = create_line_bill(**params)
                order_no = picking_instance.name
                # 检查可用
                picking_instance.action_assign()
                # 若采销单状态不为可用状态，则取消此采销单
                if picking_instance.state != 'assigned':
                    # 取消采销单
                    picking_instance.action_cancel()
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": '此采销单库存不够，不能操作！'}}
                    return record
                for pack in picking_instance.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty
                picking_instance.do_transfer()
                #########查找之前单号进行操作
                sales_orders = request.env['sales.orders'].search([('name', '=', origin)])
                if sales_orders:
                    for line in kwargs['data']:
                        # 获得采销明细对象
                        sales_orders_line = sales_orders.sales_orders_line.filtered(lambda x: x.product.default_code == line['default_code'])
                        if not sales_orders_line:
                            request._cr.rollback()
                            record = {"returnFlag": "0","result": {"message": "产品%s不存在，请检查！" %(line['default_code'])}}
                            return record
                        sales_orders_line.write({'real_number': line['qty']})
                    sales_orders.write({'state': 'return'})
                    remark = '快递单号：%s,邮费：%s,备注：%s' % (kwargs.get('track_number'), kwargs.get('postage'), kwargs.get('remark'))
                    sales_orders.write({'remark': remark})
                else:
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": '未找到对应的采销单，请检查！'}}
                    return record
            elif type == '07':  # 采退单
                # 查找对应的采退单对象
                recall_refund = request.env['recall.refund'].search([('name', '=', origin)])
                if recall_refund:
                    for line in kwargs['data']:
                        # 获得采退明细对象
                        recall_refund_line = recall_refund.order_line.filtered(lambda x: x.product == line['default_code'])
                        if not recall_refund_line:
                            request._cr.rollback()
                            record = {"returnFlag": "0","result": {"message": "产品%s不存在，请检查！" %(line['default_code'])}}
                            return record
                        recall_refund_line.write({'real_number': line['qty']})
                    order_no = origin
                    recall_refund.write({'state': 'return'})
                    remark = '快递单号：%s,邮费：%s,备注：%s' % (kwargs.get('track_number'),kwargs.get('postage'),kwargs.get('remark'))
                    recall_refund.write({'remark': remark})
                else:
                    request._cr.rollback()
                    record = {"returnFlag": "0", "result": {"message": '未找到对应的采退单，请检查！'}}
                    return record

            elif type == '08':   # 退货单
                ######## 根据第三方入库单号拆分数据
                data = {}
                for line in kwargs['data']:
                    stockin_no = line['stockin_no']
                    if data.has_key(stockin_no):
                        data[stockin_no].append(line)
                    else:
                        data[stockin_no] = [line]
                ######### 生成退货单
                order_no = ''
                for key in data.keys():
                    stock_picking = request.env['stock.picking'].search([('name', '=', '%s:%s' %(origin,key))])
                    if stock_picking:
                        dict = {'stock_picking_id':stock_picking.id,'stock_location_id':dest_warehouse_id,'return_form_line':[]}
                        return_form_line = []
                        for line in data[key]:
                            # 获取产品实例
                            product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                            if not product_instance:
                                request._cr.rollback()
                                record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                                return record
                            return_form_line.append((0,0,{'product':product_instance.id,'number':line['qty']}))
                        dict['return_form_line'] = return_form_line

                        return_form = request.env['return.form'].create(dict)
                        order_no += return_form.name + ';'
                    else:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": "未找到源中转出库单==%s:%s，请检查！"%(origin,key)}}
                        return record

            elif type == '09':  # 发货取消
                # 发货取消仓
                delivery_cancel_warehouse_id = request.env['stock.location'].search([('name', '=', '发货取消仓')]).id
                if not other_warehouse_id:
                    record = {"returnFlag": "0", "result": {"message": '仓库：发货取消仓， 未查到，请检查！'}}
                    return record
                # 分拣类型
                stock_picking_type = request.env['stock.picking.type'].search([('name', '=', '发货取消内部调拨')])
                if not stock_picking_type:
                    record = {"returnFlag": "0", "result": {"message": '分拣类型：发货取消内部调拨， 未查到，请检查！'}}
                    return record
                # 生成发货取消入库单
                params1 = {'purchase_no': origin, 'stock_picking_type': stock_picking_type,
                           'src_warehouse_id': delivery_cancel_warehouse_id, 'dest_warehouse_id': dest_warehouse_id,
                           'remark': kwargs.get('remark')}
                picking_instance = create_bill(**params1)
                for line in kwargs['data']:
                    # 获取产品实例
                    product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                    if not product_instance:
                        request._cr.rollback()
                        record = {"returnFlag": "0", "result": {"message": "产品%s不存在，请检查！" % (line['default_code'])}}
                        return record
                    # 生成发货取消入库单明细
                    params = {'purchase_no': origin, 'picking_instance': picking_instance,
                              'product_instance': product_instance, 'product_qty': line['qty'],
                              'src_warehouse_id': delivery_cancel_warehouse_id, 'dest_warehouse_id': dest_warehouse_id,
                              'remark': line.get('remark')}
                    move_id = create_line_bill(**params)
                # 检查可用
                order_no = picking_instance.name
                picking_instance.action_assign()
                for pack in picking_instance.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty
                picking_instance.do_transfer()


        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "调拨单，调整单，中转出库单等接口 请求异常 #########"+(e.message or e.name)}}
            return record
        ##########################################################
        record = {"returnFlag": "1", "result": {"order_no": order_no}}
        _logger.debug("End TransOrder ...\n")
        return record

    @predispatch
    def SellOutOrder(self, *args, **kwargs):  #	销售出库单回传接口
        _logger.debug("Begin SellOutOrder ...")
        _logger.info("销售出库单回传接口 接收参数###########" + str(kwargs))
        list = ['method', 'target_storehouse_name', 'out_date_treasury']
        message = check_params(kwargs, list)
        if message:
            record = {"returnFlag": "0", "result": {"message": message}}
            return record
        if kwargs.has_key('data'):
            list2 = ['default_code', 'code', 'number']
            dicts = kwargs['data']
            if not dicts:
                record = {"returnFlag": "0", "result": {"message": "参数中data字段为空，请检查！"}}
                return record
            for d in dicts:
                message2 = check_params(d, list2)
                if message2:
                    record = {"returnFlag": "0", "result": {"message": message2}}
                    return record
        ############################################################
        try:
            # 通过目的仓id号查询目的仓对象
            target_storehouse_instance = request.env['stock.location'].search([('name', '=', kwargs['target_storehouse_name'])])
            if target_storehouse_instance:
                target_storehouse = target_storehouse_instance.id
            else:
                record = {"returnFlag": "0", "result": {"message": "传入的目的仓名称不存在"}}
                return record
            # 获取销售出库单日期
            out_date_treasury = kwargs['out_date_treasury']

            vars = {'target_storehouse': target_storehouse,
                    'out_date_treasury': out_date_treasury,'state':'draft'}
            lines = []
            ##########销售出库单明细操作
            total_qty = 0
            for line in kwargs['data']:
                 # 计算销售产品的总数量
                 total_qty += line['number']
                 # 获取商品id
                 product_instance = request.env['product.product'].search([('default_code', '=', line['default_code'])])
                 if product_instance:
                     product = product_instance.id
                 else:
                     record = {"returnFlag": "0", "result": {"message": "传入的商品编码不存在"}}
                     return record
                 # 获取小组id
                 group_instance = request.env['sys.platform'].search([('code', '=', line['code'])])
                 if group_instance:
                     group = group_instance.id
                 else:
                     record = {"returnFlag": "0", "result": {"message": "传入的小组不存在"}}
                     return record
                 lines.append((0, 0, {'product': product,
                                      'number': line['number'],
                                      'group': group}))
            if lines:
                vars.update({'total_qty': total_qty})
                vars.update({'selling_out_line': lines})
                # 拿到创建的销售出库单，获取单号，例如：CK00001
                transfer_instance = request.env['selling.out'].create(vars)
            else:
                record = {"returnFlag": "0", "result": {"message": "没有传入出库单商品明细"}}
                return record

            # 查询产品库存数据
            selling_out_line2 = transfer_instance.query_stock_qty()
            # 将母单据状态更新为已完成状态
            #transfer_instance.update({'state':'done'})
            # 拆单
            #transfer_instance.split_selling_out()

        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "销售出库单回传接口 ########### 请求异常,异常信息: " + (e.message or e.name)}}
            return record

        record = {"returnFlag": "1", "result": {"name": transfer_instance.name}}
        _logger.debug("End SellOutOrder ...\n")
        return record

    @predispatch
    def GetAllSupplier(self, *args, **kwargs):  # 获取所有供应商名称
        _logger.debug("Begin GetAllSupplier ...")
        _logger.info("获取所有供应商名称接口 接收参数###########" + str(kwargs))
        try:
            # 查询sql
            sql = """ select DISTINCT name from res_partner where type_flag = 'supplier' and active='t' and supplier='t' """
            request.cr.execute(sql)
            # print sql
            result = request.cr.fetchall() or [()]
            result=[each[0] for each in result]


        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "获取供应商信息接口 请求异常 ############# "+(e.message or e.name)}}
            return record
        record = {"returnFlag": "1", "result": result}
        _logger.debug("End GetAllSupplier ...\n")
        return record

    @predispatch
    def GetAllReceiver(self, *args, **kwargs):  # 获取所有收货人名称
        _logger.debug("Begin GetAllReceive ...")
        _logger.info("获取所有收货人名称接口 接收参数###########" + str(kwargs))
        try:
            # 查询sql
            sql = """select DISTINCT name,code from oig_receiver """
            request.cr.execute(sql)
            result = request.cr.dictfetchall() or [()]
            # result = [each[0] for each in result]

        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "获取收货人信息接口 请求异常 ############# " + (e.message or e.name)}}
            return record
        record = {"returnFlag": "1", "result": result}
        _logger.debug("End GetAllReceive ...\n")
        return record

    @predispatch
    def GetAllTransferStock(self, *args, **kwargs):  # 获取所有中转仓名称
        _logger.debug("Begin GetAllTransferStock ...")
        _logger.info("获取所有中转仓名称接口 接收参数###########" + str(kwargs))
        try:
            # 查询sql
            sql = """select DISTINCT transfer_warehouse from public.stock_location where transfer_warehouse is not NULL"""
            request.cr.execute(sql)
            # print sql
            result = request.cr.fetchall() or [()]
            result = [each[0] for each in result]

        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "获取中转仓信息接口 请求异常 ############# " + (e.message or e.name)}}
            return record
        record = {"returnFlag": "1", "result": result}
        _logger.debug("End GetAllTransferStock ...\n")
        return record

    @predispatch
    def PurchaseOrderAuditStatus(self, *args, **kwargs):  # 判断采购订单是否审核通过
        _logger.debug("Begin GetAllTransferStock ...")
        _logger.info("判断采购订单是否审核通过接口 接收参数###########" + str(kwargs))
        try:
            # 查询sql
            sql = """select a.state from purchase_order a 
                        INNER JOIN purchase_order_line b on b.order_id = a.ID
                        where b.purchase_interid = '%s'""" %(kwargs['purchase_interid'])
            request.cr.execute(sql)
            state = request.cr.fetchone()
            if not state or state[0] == 'cancel':
                record = {"returnFlag": "0","result": {"message": "申请单号有误，未找到对应有效采购订单，请检查！"}}
                return record
            else:
                if state[0] in ('draft','sent','to approve'):
                    result = False
                elif state[0] in ('purchase','done'):
                    result = True
        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "判断采购订单是否审核通过接口 请求异常 ############# " + (e.message or e.name)}}
            return record
        record = {"returnFlag": "1", "result": result}
        _logger.debug("End GetAllTransferStock ...\n")
        return record


    @predispatch
    def GetReceiverInfo(self, *args, **kwargs):  # 获取收货人的M码
        _logger.debug("Begin  GetReceiverInfo...")
        _logger.info("获取收货人的M码 接收参数###########" + str(kwargs))
        product_receiverinfo = ''
        try:
            sql = """select pr.id,pt.default_code,pt.name,pr.receive_product_code,pr.receive_product_code2,r.id,pt.id from product_template pt 
                    left join product_receiverinfo pr
                    INNER JOIN oig_receiver r on r.id = pr.oig_receiver_id
                    on pt.id = pr.product_template_id WHERE pt.default_code = '%s' and r.name = '%s' 
            """%(kwargs.get("sku"), kwargs.get("receiver_name"))
            request.cr.execute(sql)
            result = request.cr.fetchall()
            if result:
                    product_receiverinfo = request.env['product.receiverinfo'].search([('id', '=', result[0][0])])
                    record = product_receiverinfo.write({'receive_product_code': kwargs.get("m_code"), 'receive_product_code2': kwargs.get("m_code")})
            else:
                oig_receiver = request.env['oig.receiver'].search([('name', '=', kwargs.get("receiver_name"))])
                product_template = request.env['product.template'].search([('default_code', '=', kwargs.get("sku"))])
                if oig_receiver and product_template:
                    dict={}
                    dict['oig_receiver_id'] = oig_receiver.id
                    dict['receive_product_code'] = kwargs.get("m_code")
                    dict['receive_product_code2'] = kwargs.get("m_code")
                    dict['product_template_id'] = product_template.id
                    record = request.env['product.receiverinfo'].create(dict)
                    product_receiverinfo = record
                else:
                    record = {"returnFlag": "0", "result": {"message": "没有查到相关产品和收货人信息！"}}
        except Exception as e:
            _logger.error(e)
            request._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "获取收货人M码接口 请求异常 ############# " + (e.message or e.name)}}
            return record

        if product_receiverinfo:
            record = {"returnFlag": "1", "result": {"message": "更新产品M码成功！"}}
            request._cr.commit()
            # 执行产品的下发操作
            product_receiverinfo.product_template_id.issued_product()
        else:
            record
        return record


