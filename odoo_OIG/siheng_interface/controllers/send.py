# -*- coding: utf-8 -*-
# from common import get_sign
# from common import get_urlencode_data
# from common import get_assembling_data
# from common import generate_data
# from common import parseData
# from common import create_log_bill
from datetime import datetime, timedelta
from odoo.http import request
from odoo import api, fields, models, tools
import requests
from common import update_log_bill
import logging
import json
import urllib
import urllib2
import hashlib
import base64

_logger = logging.getLogger(__name__)

class common_function(models.AbstractModel):
    _name = 'common.function'

    def create_line_bill(self,**kwargs):
        stock_move = {'name': kwargs.get('purchase_no'),
                      'picking_id': kwargs.get('picking_instance').id,
                      'product_id': kwargs.get('product_instance').id,
                      'product_uom_qty': kwargs.get('product_qty'),
                      'product_uom': kwargs.get('product_instance').uom_id.id,
                      'location_id': kwargs['src_warehouse_id'],
                      'location_dest_id': kwargs['dest_warehouse_id']}
        if kwargs.has_key('group_id'):
            stock_move['group_id'] = kwargs['group_id']
        if kwargs.has_key('remark'):
            stock_move['remark'] = kwargs['remark']
        if kwargs.has_key('price_unit'):
            stock_move['price_unit'] = kwargs['price_unit']
        move_id = self.env['stock.move'].create(stock_move)
        return move_id

    def create_bill(self,**kwargs):
        stock_picking = {'move_type': 'direct',
                         'picking_type_id': kwargs.get('stock_picking_type').id,
                         'origin': kwargs.get('purchase_no'),
                         'location_id': kwargs['src_warehouse_id'],
                         'location_dest_id': kwargs['dest_warehouse_id']}
        if kwargs.has_key('date'):
            stock_picking['expected_arrival_date'] = kwargs['date']
            stock_picking['theoretical_arrival_date'] = kwargs['date']
        if kwargs.has_key('remark'):
            stock_picking['remark'] = kwargs['remark']
        picking_instance = self.env['stock.picking'].create(stock_picking)
        return picking_instance


class sendController(models.AbstractModel):
    _name = 'send.controller'

    def dispatch(self, method, data):
        if self.env['ir.config_parameter'].sudo().get_param('request_flag') == '1':
            _logger.info(u'传输过来的数据：############# method='+str(method)+'########### data='+str(data))
            data = self.parseData(data) # 将传过来的数据处理
            sign = self.get_sign(data)  # 获取签名
            data2 = self.get_urlencode_data(data)   # 对data数据转码
            kwargs = self.get_assembling_data(data2, sign, method)  # 组装数据
            post_data = self.generate_data(kwargs)     # 装换数据格式
            _logger.info("请求参数:" + str(post_data))
            res = eval("self." + method)(post_data)
            _logger.info("返回参数:" + str(res))
            # 封装日志信息
            if res['Response'].get('return').get('returnFlag') == '0' and kwargs.get('except_id'):
                # 封装日志信息
                query_sql = """select id,error_message,create_date from interface_log where id = '%s'"""%(kwargs.get('except_id'))
                request.cr.execute(query_sql)
                query_result = request.cr.dictfetchone()
                params = {
                    'request_parameter': json.dumps(kwargs),
                    'record_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                    'request_way': 'ask_inside',
                    'record_state': res['Response'].get('return').get('returnFlag') == '0' and 'Unsolved' or 'ok',
                    'error_message': query_result['create_date']+"的错误信息：" + query_result['error_message'] + ";+++++++"+datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"重新请求，错误信息：+++++++"+res.get('Response').get('return').get('returnDesc'),
                    'method': kwargs.get('method'),
                    'except_id': kwargs.get('except_id')}
                # 更新日志记录
                interface_log_id = update_log_bill(**params)
                return res
            else:
                params = {
                    'request_parameter': data,
                    'record_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                    'record_state': res['Response'].get('return').get('returnFlag') == '0' and 'Unsolved' or 'ok',
                    'request_way': 'ask_out',
                    'error_message': res['Response'].get('return').get('returnFlag') == '0' and res.get('Response').get('return').get('returnDesc') or 'ok',
                    'method': method}
                interface_log_id = self.create_log_bill(**params)
                return res
        else:
            dict = {}
            return dict

    ########  WMS接口调用
    def putCustData(self,post_data):  # 客户资料
        url = self.env['ir.config_parameter'].sudo().get_param('wms_url')
        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        # headers = {'Content-Type': 'application/json; charset=utf-8'}
        # # data = json.dumps(kwargs)
        # response = requests.post(url, data=kwargs, headers=headers, verify=False)
        # # result = json.loads(response.content)
        # if requests.codes.ok == response.status_code or 201 == response.status_code:
        #     # content = json.loads(response.content)
        #     result = urllib.unquote(response.content)
        #     print result
        #     _logger.info(u'调用成功')
        # else:
        #     _logger.info(u'调用失败')

    def putSkuVeData(self,post_data):  # 与供应商产品资料代码对应关系接口（ERP调用）
        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        url = self.env['ir.config_parameter'].sudo().get_param('wms_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def putSkuCoData(self,post_data):  # 与中转仓产品资料代码对应关系接口(ERP调用)
        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        url = self.env['ir.config_parameter'].sudo().get_param('wms_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def BomListData(self,post_data):  # BOM信息下发接口【ERP调用】
        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        url = self.env['ir.config_parameter'].sudo().get_param('wms_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def queryINVData(self,post_data):  # 库存查询接口【ERP MMS 接口平台调用】
        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        url = self.env['ir.config_parameter'].sudo().get_param('wms_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def PackKitData(self,post_data):  # 包装、加工、发运下发接口【MMS ERP  接口平台调用】
        # url = 'http://192.168.1.53:8090/datahubWeb/FLUXWMSJSONAPI/'
        url = self.env['ir.config_parameter'].sudo().get_param('wms_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    #######  SRM接口调用
    def putPOData(self,post_data):  # 采购订单下发（ERP调用）
        # url = 'http://192.168.1.53:8480/data/api'
        url = self.env['ir.config_parameter'].sudo().get_param('srm_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def cancelPOData(self,post_data):  # 采购订单取消下发（ERP调用）
        # url = 'http://192.168.1.53:8480/data/api'
        url = self.env['ir.config_parameter'].sudo().get_param('srm_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def queryPOData(self,post_data):  # 采购状态明细查询（ERP调用）
        # url = 'http://192.168.1.53:8480/data/api'
        url = self.env['ir.config_parameter'].sudo().get_param('srm_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def ClosePOData(self,post_data):  # 采购订单关闭（ERP调用）
        # url = 'http://192.168.1.53:8480/data/api'
        url = self.env['ir.config_parameter'].sudo().get_param('srm_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result

    def closePOLine(self,post_data):  # 采购订单订单行关闭（ERP调用）
        # url = 'http://192.168.1.53:8480/data/api'
        url = self.env['ir.config_parameter'].sudo().get_param('srm_url')
        response = urllib2.urlopen(url, post_data)  # 提交，发送数据
        content = response.read()  # 获取提交后返回的信息
        result = urllib.unquote(content) # 对返回信息进行转码处理
        dict_result = json.loads(result)
        return dict_result


    #################################### 分装方法###############
    # 对页面传过来的字典数据进行处理
    def parseData(self,param):
        # 组装字典数据
        data = {}
        # data1 = {}
        # list = []
        # list.append(param)
        # data1['header'] = param
        data['xmldata'] = param
        # 将字典转为json
        new_data = json.dumps(data)
        return new_data

    # 生成验签字符串
    def get_sign(self,data):
        # 将密匙和data数据拼接成字符串
        appSecret = self.env['ir.config_parameter'].sudo().get_param('appSecret')
        str = appSecret + data + appSecret
        # 进行MD5加密
        m = hashlib.md5()
        m.update(str)
        strMd5 = m.hexdigest()
        # 进行base64签名并转为大写
        strBase64 = base64.b64encode(strMd5).upper()
        # 进行utf-8编码
        sign = urllib.quote(strBase64.encode('utf-8'))
        return sign

    # 组装数据
    def get_assembling_data(self,data, sign, method):
        kwargs = {}  # 定义一个要提交的数据数组(字典)
        kwargs['method'] = method
        kwargs['client_customerid'] = self.env['ir.config_parameter'].sudo().get_param('client_customerid')
        kwargs['client_db'] = self.env['ir.config_parameter'].sudo().get_param(method + '_client_db')
        kwargs['messageid'] = self.env['ir.config_parameter'].sudo().get_param(method + '_method')
        kwargs['apptoken'] = self.env['ir.config_parameter'].sudo().get_param('apptoken')
        kwargs['timestamp'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        kwargs['appkey'] = self.env['ir.config_parameter'].sudo().get_param('appkey')
        kwargs['sign'] = sign
        kwargs['data'] = data
        return kwargs

    # urlencoding 转码
    def get_urlencode_data(self,data):
        return urllib.quote(data)

    # 将字典格式转为数据传输所需格式
    def generate_data(self,kwargs):
        return urllib.urlencode(kwargs)

    # 生成日志记录表
    def create_log_bill(self, **kwargs):
        log_id = self.env['interface.log'].create({'request_parameter': kwargs.get('request_parameter'),
                                                      'record_time': kwargs.get('record_time'),
                                                      'record_state': kwargs.get('record_state'),
                                                      'request_way': kwargs.get('request_way'),
                                                      'error_message': kwargs.get('error_message'),
                                                      'method': kwargs.get('method'),
                                                      })
        return log_id













