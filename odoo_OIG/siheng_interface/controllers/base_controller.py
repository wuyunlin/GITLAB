# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import DataSet
from common import *
import logging

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')
class baseController(DataSet):
    @http.route(['/app_api/vmc/session'], type='json', auth="user")
    def vmc_session_call_kw(self,*args, **kwargs):
        _logger.error(u"请求参数:"+str(kwargs));
        result = eval("self."+kwargs['method'])(request,*args, **kwargs)
        _logger.error(u"执行完:"+kwargs['method']);
        return result

    @http.route(['/app_api/vmc/nosession'], type='json', auth="none")
    def vmc_nosession_call_kw(self,*args, **kwargs):
        _logger.error(u"请求参数:"+str(kwargs));
        result = eval("self."+kwargs['method'])(request,*args, **kwargs)
        _logger.error(u"执行完:"+kwargs['method']);
        return result
