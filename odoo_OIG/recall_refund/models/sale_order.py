# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
import datetime
from odoo.exceptions import UserError
class sales_orders(models.Model):
    # 创建表的名称
    _name = 'sales.orders'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    # 创建表描述
    _description = u'采销单'

    # 字段名称
    name = fields.Char(u'单号', required=True, default='New')
    # 供应商
    res_partner_id = fields.Many2one('res.partner', u'供应商', track_visibility='onchange')
    # 收货人
    oig_receiver_id = fields.Many2one('oig.receiver', u'收货人', track_visibility='onchange')
    # 订单明细
    sales_orders_line = fields.One2many('sales.orders.line', 'sales_orders_id', string='采销单明细', track_visibility='onchange')

    # 状态 create 生成采销  issued 下发采销 return 采销回传
    state = fields.Selection([
        ('create', u'生成采销'),
        ('issued', u'下发采销'),
        ('return', u'采销回传'),
        ('confirm', u'价格确认')
    ], readonly=True, required=True, string='状态', default='create')
    # 备注
    remark = fields.Char(u'备注')
    address = fields.Text(u'地址')
    returns_info = fields.Char(u'退货信息')

    # 下发采销单
    @api.multi
    def issued_sales_orders(self):
        if len(self.sales_orders_line) == 0:
            raise UserError("采销单明细行不能为空！")
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        dict = {}
        # 订单编号 暂时数据
        dict['OrderNo'] = self.display_name
        # 订单类型
        dict['OrderType'] = self.env['ir.config_parameter'].sudo().get_param('OrderType_CX')
        # 订单创建时间
        dict['OrderTime'] = now
        # 货主ID
        dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
        # ERP出库计划单号
        dict['SOReference1'] = self.display_name
        # 采退、采销是传供应商代码
        dict['ConsigneeID'] = self.res_partner_id.ref
        # 收货人名称
        # dict['ConsigneeName'] = self.res_partner_id.name
        dict['ConsigneeNames'] = self.oig_receiver_id.name
        # 仓库ID
        dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')
        # 退货信息
        dict['Notes '] = self.returns_info
        # 退货地址
        dict['C_Address1'] = self.address

        line_list = []
        for line in self.sales_orders_line:
            now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
            line_dict = {}
            # 行号 暂时数据
            line_dict['LineNo'] = line.id
            # 客户编号
            line_dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
            # 产品代码
            line_dict['SKU'] = line.product.default_code
            # 收货人信息
            line_dict['LotAtt04'] = self.oig_receiver_id.code
            # 包装或加工数量
            line_dict['QtyOrdered'] = line.number
            line_list.append(line_dict)
        dict['detailsItem'] = line_list

        if dict:
            xmldata = {'header': dict}
            if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                result = self.env['send.controller'].dispatch('PackKitData', xmldata)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    raise UserError('调用失败'+ result.get("Response").get("return").get("returnDesc"))
                else:
                    self.write({'state': "issued"})
            else:
                pass
        return True

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('sales.orders') or '/'
        return super(sales_orders, self).create(vals)

    # 删除功能
    @api.multi
    def unlink(self):
        for order in self:
            if order.state == 'issued' or order.state == 'return' or order.state == 'confirm':
                raise UserError("当前状态不能进行删除操作，只有【生成采销】状态才能删除！")
        return super(sales_orders, self).unlink()

    # 价格确认
    def confirm_sales_order(self):
        self.write({'state': "confirm"})
        return True

# 采销单明细
class sales_order_line(models.Model):

    _name = "sales.orders.line"

    _descrption = u"采销单明细";

    sales_orders_id = fields.Many2one('sales.orders', u'采销单')
    # 中转仓
    transfer_warehouse = fields.Char(u'中转仓')

    # 箱号
    case_number = fields.Char(u'箱号')

    # 箱长
    box_length = fields.Integer(u'箱长')

    # 箱宽
    box_width = fields.Integer(u'箱宽')

    # 箱高
    box_height = fields.Integer(u'箱高')

    # 满托
    full_care = fields.Char(u'满托')

    # ASN 单号
    asn = fields.Char(u'ASN单号')

    # 产品
    product = fields.Many2one('product.product', u'产品')

    # 数量
    number = fields.Integer(u'数量')

    # 价格
    price = fields.Float(u'价格')

    # 实际数量
    real_number = fields.Integer(u'实际数量')

    # 库位
    location = fields.Char(u'库区')

    line_state = fields.Selection([
        ('create', u'生成采销'),
        ('issued', u'下发采销'),
        ('return', u'采销回传'),
        ('confirm', u'价格确认')
    ], string='状态', related='sales_orders_id.state')

    # 采销单明细选中产品，带出最后一次入库的单价
    @api.multi
    def onchange_product_price(self, product):
        res = {}
        if product:
            sql = """select price_unit from purchase_order_line where product_id = %s order by id DESC limit 1"""%(product)
            self._cr.execute(sql)
            fet = self._cr.fetchone()
            if fet:
                res['price'] = fet[0]
        return {'value': res}