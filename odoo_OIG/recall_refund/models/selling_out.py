# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.addons.siheng_interface.controllers.send import sendController
import datetime
from odoo.addons.siheng_interface.controllers.common import create_bill
from odoo.addons.siheng_interface.controllers.common import create_line_bill
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)
import json


class selling_out(models.Model):
    # 创建表的名称
    _name = 'selling.out'
    # 创建表描述
    _description = u'销售出库单'
    # 字段名称
    name = fields.Char(u'单号', required=True, default='New')
    # 目的仓
    target_storehouse = fields.Many2one('stock.location', u'目的仓', required=True)
    # 日期
    out_date_treasury = fields.Date(u'销售出库单日期')
    # 出库单号
    sell_out_number = fields.Many2one('stock.picking', u'出库单号')
    # 出库状态
    state = fields.Selection([
        ('draft', u'未出库'),
        ('done', u'已出库'),
    ], string=u'出库状态')
    # 销售明细(小组)
    selling_out_line = fields.One2many('selling.out.line', 'selling_out_id', string='销售出库单明细(小组)')
     # 销售明细
    selling_out_line2 = fields.One2many('selling.out.line2', 'selling_out_id', string='销售出库单明细')
    # 销售缺货
    selling_stockout_line = fields.One2many('selling.stockout.line', 'selling_out_id', string='销售出库缺货明细')
    # 关联的日记账分录的单号
    account_move_id = fields.Many2one('account.move', u'日记账分录')
    # 总数量
    total_qty = fields.Integer(u'总数量')
    # 母单据ID
    parent_id = fields.Many2one('selling.out', u'母单据')
    # 已出库数量
    done_qty = fields.Integer(u'已出库数量', default = 0)
    # 是否缺货标识
    stockout_flag = fields.Boolean(u'是否缺货', default = False)

    split_flag = fields.Boolean(u'拆单失败', default=False)

    # xi
    def get_stockout_down(self):
        query = """select  b.name as 单号, d.name as 仓库,c.default_code as 产品,a.qty as 出库数量,
                a.stock_qty as 库存数量,a.qty-a.stock_qty as cy from selling_stockout_line a 
                inner join selling_out b on b.id=a.selling_out_id
                inner join product_product c on c.id=a.product
                inner join stock_location d on d.id=b.target_storehouse
                where b.id=%s """% (self.id)
        query = query.replace('=', '%3d').replace('+', '%2B')
        result = {'titles':['单号','仓库','产品','出库数量','库存数量','差额'], 'filename': '%s缺货'%(self.name) + '.xlsx', 'command': query}
        jsonstr = json.dumps(result)
        return {'type': 'ir.actions.act_url',
                'url': '/web/excel/download?para=%s' % jsonstr,
                'target': 'new'}

    # 处理子母单的问题
    def do_seling_out(self):
        selling_out_result = self.env['selling.out'].search([('state', '=', 'draft'), ('parent_id', '=', False)])
        for selling_out in selling_out_result:
            try:
                # 拆单功能
                selling_out.split_selling_out()
                selling_out._cr.commit()
            except Exception as e:
                selling_out._cr.rollback()
                selling_out.invalidate_cache()
                selling_out._cr.commit()
                continue


    # 确认销售出库单
    def confirm_order(self):
        if self.state == 'done':
            raise UserError('此销售出库单已确认，不能重复确认！')
        # 生成销售出库单。
        record = self.create_selling_out()
        if isinstance(record, dict):
            self._cr.rollback()
            return record

        ############ 调整小组存存
        # AdjustmentGroupProductNumber()
        #flag = self.AdjustmentGroupProductNumber()
        account_move_id = self.env['account.move'].search([('ref', '=', record.name)])
        self.write({'account_move_id': account_move_id.id})
        ###调整凭证
        # mess = self.AdjustmentVoucher(record, account_move_id)
        # if mess:
        #     self._cr.rollback()
        #     return mess
        # if not flag:
        #     self._cr.rollback()
        #     record = {"returnFlag": "0", "result": {"message": "小组库存调整失败"}}
        #     return record
        if self.parent_id:
            update_dict={'child_id':self.id,'parent_id':self.parent_id.id}
            # 更新子母单状态
            sql="""update selling_out_line2 set is_done=True where selling_out_id=%(parent_id)s and product in (
                    select product from selling_out_line2 where selling_out_id=%(child_id)s);
                    update selling_out set  state='done',done_qty=done_qty+(select sum(qty) from selling_out_line2 
                    where selling_out_id=%(child_id)s) where id in (%(child_id)s,%(parent_id)s);"""%(update_dict)
        else:
            update_dict = {'child_id': self.id}
            sql="""                   
                    update selling_out_line2 set is_done=True where selling_out_id=%(child_id)s;
                    update selling_out set state='done',done_qty=done_qty+(select sum(qty) from selling_out_line2 
                    where selling_out_id=%(child_id)s) where id=%(child_id)s;"""%(update_dict)
        self._cr.execute(sql)
        return True

    # 跑批确认销售出库单
    def auto_confirm_order(self, *args):
        sql = """select id from selling_out where state = 'draft' and stockout_flag = FALSE and parent_id is not null  order by name LIMIT %s """ %(args[0])
        self._cr.execute(sql)
        ids = self._cr.fetchall()
        for id in ids:
            selling_out_instance = self.env['selling.out'].browse(id)
            start_time = datetime.datetime.now()
            _logger.info("##########确认销售出库单：%s,开始##############" % selling_out_instance.name)
            try:
                selling_out_instance.confirm_order()
                pass_time = datetime.datetime.now() - start_time
                _logger.info("##########确认销售出库单：%s,结束######耗时：%s########" % (selling_out_instance.name,pass_time))
                self._cr.commit()
            except Exception as e:
                _logger.error(e)
                self._cr.rollback()
                selling_out_instance.update({'stockout_flag': True})
                pass_time = datetime.datetime.now() - start_time
                _logger.info("##########销售出库单：%s,库存不足######耗时：%s########" % (selling_out_instance.name,pass_time))
                self._cr.commit()
        return True

    def auto_split_order(self, *args):
        sql = """select id from selling_out where state = 'draft' and split_flag = FALSE and parent_id is  null LIMIT %s """ %(args[0])
        self._cr.execute(sql)
        ids = self._cr.fetchall()
        for id in ids:
            selling_out_instance = self.env['selling.out'].browse(id)
            start_time = datetime.datetime.now()
            _logger.info("##########拆单出库单：%s,开始##############" % selling_out_instance.name)
            try:
                selling_out_instance.split_selling_out()
                pass_time = datetime.datetime.now() - start_time
                _logger.info("##########拆单出库单：%s,结束######耗时：%s########" % (selling_out_instance.name,pass_time))
            except Exception as e:
                _logger.error(e)
                self._cr.rollback()
                selling_out_instance.update({'split_flag': True})
                pass_time = datetime.datetime.now() - start_time
                _logger.info("##########销售出库单：%s,库存不足######耗时：%s########" % (selling_out_instance.name,pass_time))
        return True


    # 查询产品库存
    def query_stock_qty(self):
        if not self.selling_out_line2:
            sql="""  with res as (
                    select product_id, location_id, sum(qty) as stock_qty from stock_quant c where  c.reservation_id is null 
                    group by product_id, location_id)
                    insert into selling_out_line2(selling_out_id,product,qty,stock_qty,is_done)
                      select b.selling_out_id, b.product, sum(b.number) as qty, coalesce(res.stock_qty, 0) as stock_qty
                        ,'f'as is_done  from selling_out a
                    inner join selling_out_line b on b.selling_out_id = a.id
                    left join res on res.product_id = b.product and res.location_id = a.target_storehouse
                    where a.id = %s group by b.selling_out_id, b.product,coalesce(res.stock_qty, 0);
                     insert into selling_stockout_line(selling_out_id,product,qty,stock_qty)
                     select selling_out_id,product,qty,stock_qty from selling_out_line2
                     where  selling_out_id=%s and qty>stock_qty;"""%(self.id,self.id)
        else:
            sql="""with res as (
                    select product_id, location_id, sum(qty) as stock_qty from stock_quant c where  c.reservation_id is null 
                    group by product_id, location_id),
                    res2 as (
                        select b.id, b.product, b.qty, coalesce(res.stock_qty, 0) as stock_qty  from selling_out a
                    inner join selling_out_line2 b on b.selling_out_id = a.id
                    left join res on res.product_id = b.product and res.location_id = a.target_storehouse
                    where a.id = %s and coalesce(b.is_done, 'f') = 'f')
                    update selling_out_line2 set stock_qty = res2.stock_qty from res2 where
                     res2.id = selling_out_line2.id;
                     DELETE from selling_stockout_line where selling_out_id=%s;
                     insert into selling_stockout_line(selling_out_id,product,qty,stock_qty)
                     select selling_out_id,product,qty,stock_qty from selling_out_line2
                     where  selling_out_id=%s and qty>stock_qty;"""%(self.id,self.id,self.id)
        self._cr.execute(sql)
        if not self.selling_stockout_line:
            self._cr.execute("""update selling_out set stockout_flag='f' where (id=%s or parent_id=%s) and stockout_flag='t' """%(self.id,self.id))
        else:
            self._cr.execute("""update selling_out set stockout_flag='f' where id=%s and stockout_flag='t' """ % ( self.id))
        return True
    # 对明细较多的单据进行拆单处理
    def split_selling_out(self):
        # 设置拆单后子单据的明细条数
        child_number = self.env['ir.config_parameter'].sudo().get_param('child_number')

        # 查询明细对象
        selling_out_line2s = self.selling_out_line2
        a_selling_out_line = []
        a_selling_out_line2 = []

        b_selling_out_line = []
        b_selling_out_line2 = []
        b_selling_stockout_line = []
        total_qty = 0
        total_qty2 = 0
        num = 1
        for line in selling_out_line2s:
            qty = line.qty
            stock_qty = line.stock_qty
            if qty > stock_qty:
                total_qty2 += qty
                # 查询明细并写入列表中
                b_selling_out_line2.append((0, 0, {'product': line.product.id,
                                                   'qty': line.qty,
                                                   'stock_qty': line.stock_qty}))
                # 查询小组明细并写入到列表中
                product_lines = self.selling_out_line.filtered(lambda x: x.product == line.product)
                for product_line in product_lines:
                    b_selling_out_line.append((0, 0, {'product': product_line.product.id,
                                                      'number': product_line.number,
                                                      'group': product_line.group.id}))
                # 查询销售缺货明细并写入列表中
                pro_lines = self.selling_stockout_line.filtered(lambda x: x.product == line.product)
                for pro_line in pro_lines:
                    b_selling_stockout_line.append((0, 0, {'product': pro_line.product.id,
                                                           'qty': pro_line.qty,
                                                           'stock_qty': pro_line.stock_qty}))

            else:
                total_qty += qty
                # 查询明细并写入列表中
                a_selling_out_line2.append((0, 0, {'product': line.product.id,
                                                   'qty': line.qty,
                                                   'stock_qty': line.stock_qty}))
                # 查询小组明细并写入到列表中
                product_lines = self.selling_out_line.filtered(lambda x: x.product == line.product)
                for product_line in product_lines:
                    a_selling_out_line.append((0, 0, {'product': product_line.product.id,
                                                      'number': product_line.number,
                                                      'group': product_line.group.id}))

            # 库存够的产品拆分为子单
            if len(a_selling_out_line2) == int(child_number):
                _logger.info("##########子单据：%s生成##############" %num)
                vars = {'target_storehouse': self.target_storehouse.id,
                        'out_date_treasury': self.out_date_treasury,
                        'state': 'draft', 'parent_id': self.id,'total_qty':total_qty}
                vars.update({'selling_out_line': a_selling_out_line, 'selling_out_line2': a_selling_out_line2})
                child_transfer_instance = self.env['selling.out'].create(vars)
                a_selling_out_line = []
                a_selling_out_line2 = []
                total_qty = 0
                _logger.info("##########子单据：%s生成结束##############" %num)
                num +=1
            # 库存不够的产品拆分为子单
            if len(b_selling_out_line2) == int(child_number):
                _logger.info("##########子单据：%s生成##############" % num)
                vars = {'target_storehouse': self.target_storehouse.id,
                        'out_date_treasury': self.out_date_treasury,
                        'state': 'draft', 'parent_id': self.id,'total_qty':total_qty2}
                vars.update({'selling_out_line': b_selling_out_line, 'selling_out_line2': b_selling_out_line2,
                             'selling_stockout_line': b_selling_stockout_line})
                child_transfer_instance2 = self.env['selling.out'].create(vars)
                b_selling_out_line = []
                b_selling_out_line2 = []
                b_selling_stockout_line = []
                total_qty2 = 0
                _logger.info("##########子单据：%s生成结束##############" % num)
                num += 1
        # 库存够的产品拆分为子单
        if len(a_selling_out_line2) > 0:
            _logger.info("##########子单据：%s生成##############" % num)
            vars2 = {'target_storehouse': self.target_storehouse.id,
                     'out_date_treasury': self.out_date_treasury,
                     'state': 'draft', 'parent_id': self.id,'total_qty':total_qty}
            vars2.update({'selling_out_line': a_selling_out_line, 'selling_out_line2': a_selling_out_line2})
            child_transfer_instance = self.env['selling.out'].create(vars2)
            _logger.info("##########子单据：%s生成结束##############" % num)
            num += 1
        # 库存不够的产品拆分为子单
        if len(b_selling_out_line2) > 0:
            _logger.info("##########子单据：%s生成##############" % num)
            vars2 = {'target_storehouse': self.target_storehouse.id,
                     'out_date_treasury': self.out_date_treasury,
                     'state': 'draft', 'parent_id': self.id,'total_qty':total_qty2}
            vars2.update({'selling_out_line': b_selling_out_line, 'selling_out_line2': b_selling_out_line2,
                          'selling_stockout_line': b_selling_stockout_line})
            child_transfer_instance2 = self.env['selling.out'].create(vars2)
            _logger.info("##########子单据：%s生成结束##############" % num)
            num += 1

        # 将母单据的状态更改为已出库状态
        self.update({'state':'done'})
        return True



    # 生成出库单
    def create_selling_out(self):
        # 汇总产品数量
        product = {}
        for line in self.selling_out_line:
            if not product.has_key(line.product):
                product[line.product] = line.number
            else:
                product[line.product] += line.number
        # 拣货类型
        stock_picking_type = self.env['stock.picking.type'].search([('name', '=', '销售出库内部调拨')])
        # 生成出库单
        params1 = {'purchase_no': self.name + '出库单', 'stock_picking_type': stock_picking_type,
                   'src_warehouse_id': self.target_storehouse.id, 'dest_warehouse_id': self.env.ref('stock.stock_location_customers').id}
        selling_out =self.env['common.function']. create_bill(**params1)

        # 生成出库单明细
        for key in product.keys():
            # product_instance = self.env['product.product'].search([('default_code', '=', key)])
            params = {'purchase_no': self.name, 'picking_instance': selling_out,
                      'product_instance': key, 'product_qty': product.get(key),
                      'src_warehouse_id': self.target_storehouse.id, 'dest_warehouse_id': self.env.ref('stock.stock_location_customers').id}
            move_id =self.env['common.function']. create_line_bill(**params)

        self.sell_out_number = selling_out
        # self.account_move_id = self.env['account.move'].search([('name', '=', selling_out.name)])
        # 检查可用
        selling_out.action_assign()
        # 若出库单状态不为可用状态，则取消此出库单
        if selling_out.state != 'assigned':
            raise UserError('此出库单库存不够，请查看销售缺货列表！')
            # # 取消出库单
            # selling_out.action_cancel()
            # record = {"returnFlag": "0", "result": {"message": '此出库单库存不够，不能操作！'}}
            # return record
        for pack in selling_out.pack_operation_product_ids:
            pack.qty_done = pack.product_qty
        selling_out.do_transfer()
        return selling_out

        # 商品库存调整
        # 1.获取实际传入erp的商品比例
    def getRealProductNumber(self):
        # 获取实际需要的商品比例
        # 汇总产品总数量和各个小组需要的数量
        product_dict = {}
        for line in self.selling_out_line:
            #####产品第一次出现
            if not product_dict.has_key(line.product.id):
                product_dict[line.product.id]={}
                product_dict[line.product.id][line.group.id]=line.number
            #####产品非第一次出现
            else:
                ####小组第一次出现
                if not product_dict[line.product.id].has_key(line.group.id):
                    product_dict[line.product.id][line.group.id] = line.number
                ####小组非第一次出现
                else:
                    product_dict[line.product.id][line.group.id] += line.number
        return product_dict
    # 获取系统中的商品比例
    def getNowNumber(self):
        stock_picking_instance=self.sell_out_number
        product_dict={}
        def check_unique_product(stock_picking_instance):
            sql="""select product_id from stock_move where picking_id=%s group by product_id having(count(*)>1)"""%(stock_picking_instance.id)
            self._cr.execute(sql)
            fet=self._cr.fetchall()
            if fet:
                flag=False
            else:
                flag=True
            return flag

        def get_platform_line(move_id):
            sql="""select d.sys_platform_id,sum(d.qty) as qty
                    from stock_move  a 
                    inner join stock_quant_move_rel b on b.move_id=a.id
                    inner join stock_quant c on c.id=b.quant_id 
                    inner join stock_quant_platform d on d.stock_quant_id=c.id
                    where a.id=%s group by d.sys_platform_id"""%(move_id)
            self._cr.execute(sql)
            fet=self._cr.fetchall()
            if not fet:
                #####user error
                raise UserError('没有符合的小组库存!')
            platform_line={}
            for line in fet:
                platform_line[line[0]]=line[1]
            return platform_line
        #####校验产品不重复
        if not check_unique_product(stock_picking_instance):
            raise UserError('产品不唯一!')
            ####usererror 产品不唯一
        for move in stock_picking_instance.move_lines:
            ###########获取小组明细
            product_dict[move.product_id.id]=get_platform_line(move.id)
        return product_dict

    def trans_func(self,product_id, location_id, platform_id_less, platform_id_more, qty):
        #########少的组 需要调拨给多的组
        ##########获取源位置 满足条件【产品 库位 小组】的小组明细 然后进行调拨
        sql = """select a.id,a.qty,b.id as stock_quant_id from stock_quant_platform a 
               inner join stock_quant b on b.id=a.stock_quant_id
               where b.product_id=%s and b.location_id=%s and a.sys_platform_id=%s order by b.in_date """ % (
            product_id, location_id, platform_id_less)
        self._cr.execute(sql)
        fet = self._cr.fetchall()

        def get_already_line(stock_quant_id,sys_platform_id):
            #######注意 需要给库存小组建立 stock_quant_id sys_platform_id唯一索引
            sql="""select id from stock_quant_platform where stock_quant_id=%s and sys_platform_id=%s limit 1"""%(stock_quant_id,sys_platform_id)
            self._cr.execute(sql)
            fet = self._cr.fetchone()
            if not fet:
                return False
            else:
                return fet[0]
        for each in fet:
            handle_qty = min(each[1], qty)
            #####剩余数量
            qty = qty - handle_qty
            #####查看是否有目的组的明细在同一个quant下 有的话就追加在已有的上面
            already_line = get_already_line(each[2],platform_id_more)
            if already_line:
                #######小组库存数量大于本次需要调拨的数量
                ####### 1 更新员小组库存数量-qty 目的组+qty
                if each[1] > handle_qty:
                    sql="""update stock_quant_platform set qty=qty-%s where id=%s;
                           update stock_quant_platform set qty=qty+%s where id=%s;"""%(handle_qty,each[0],handle_qty,already_line)
                    self._cr.execute(sql)
                #######1 删除源小组库存的记录，2 更新目的组+qty
                else:
                    sql = """delete from  stock_quant_platform where id=%s;
                             update stock_quant_platform set qty=qty+%s where id=%s;""" % (each[0],handle_qty, already_line)
                    self._cr.execute(sql)
            else:
                ####拆分
                if each[1] > handle_qty:
                    sql = """update stock_quant_platform set qty=qty-%s where id=%s;
                         insert into stock_quant_platform(stock_quant_id,sys_platform_id,qty,old_qty)
                         select stock_quant_id,%s,%s,%s from stock_quant_platform where id=%s;""" %(handle_qty,each[0],platform_id_more,handle_qty,handle_qty,each[0])
                    self._cr.execute(sql)
                    #######拆分
                else:
                    sql = """ update stock_quant_platform set sys_platform_id=%s where id=%s;""" % (platform_id_more,each[0])
                    self._cr.execute(sql)
            if not qty:
                break
        if qty > 0:
            #product_id, location_id, platform_id_less, platform_id_more, qty,self.id 【源单号】
            ##利用上述字段创建小组调拨记录 便于后期费用统计结算使用
            # 生成小组调拨单
            create_group_id = self.env['group.transfer'].create({'product_id': product_id,
                                                                 'location_id': location_id,
                                                                 'platform_id_less': platform_id_less,
                                                                 'platform_id_more': platform_id_more,
                                                                 'qty': qty,
                                                                 'selling_out_id': self.id,


                                                          })

    def dest_trans_func(self,product_id, location_id, platform_id_more, platform_id_less, qty):
        #########少的组 需要调拨给多的组
        ##########获取源位置 满足条件【产品 库位 小组】的小组明细 然后进行调拨

        stock_move_instance=self.sell_out_number.move_lines.filtered(lambda x: x.product_id.id == product_id)
        if not stock_move_instance:
            x=1

        stock_quant_ids=[x.id for x in stock_move_instance[0].quant_ids ]

        sql = """select a.id,a.qty,b.id as stock_quant_id from stock_quant_platform a 
               inner join stock_quant b on b.id=a.stock_quant_id
               where  a.sys_platform_id=%s  and b.id in %s   order by b.in_date """ % ( platform_id_less,str( tuple( stock_quant_ids+[0] )) )
        self._cr.execute(sql)
        fet = self._cr.fetchall()

        def get_already_line(stock_quant_id,sys_platform_id):
            #######注意 需要给库存小组建立 stock_quant_id sys_platform_id唯一索引
            sql="""select id from stock_quant_platform where stock_quant_id=%s and sys_platform_id=%s limit 1"""%(stock_quant_id,sys_platform_id)
            self._cr.execute(sql)
            fet = self._cr.fetchone()
            if not fet:
                return False
            else:
                return fet[0]
        for each in fet:
            handle_qty = min(each[1], qty)
            #####剩余数量
            qty = qty - handle_qty
            #####查看是否有目的组的明细在同一个quant下 有的话就追加在已有的上面
            already_line = get_already_line(each[2],platform_id_more)
            if already_line:
                #######小组库存数量大于本次需要调拨的数量
                ####### 1 更新员小组库存数量-qty 目的组+qty
                if each[1] > handle_qty:
                    sql="""update stock_quant_platform set qty=qty-%s where id=%s;
                           update stock_quant_platform set qty=qty+%s where id=%s;"""%(handle_qty,each[0],handle_qty,already_line)
                    self._cr.execute(sql)
                #######1 删除源小组库存的记录，2 更新目的组+qty
                else:
                    sql = """delete from  stock_quant_platform where id=%s;
                             update stock_quant_platform set qty=qty+%s where id=%s;""" % (each[0],handle_qty, already_line)
                    self._cr.execute(sql)
            else:
                ####拆分
                if each[1] > handle_qty:
                    sql = """update stock_quant_platform set qty=qty-%s where id=%s;
                         insert into stock_quant_platform(stock_quant_id,sys_platform_id,qty,old_qty)
                         select stock_quant_id,%s,%s,%s from stock_quant_platform where id=%s;""" %(handle_qty,each[0],platform_id_more,handle_qty,handle_qty,each[0])
                    self._cr.execute(sql)
                    #######拆分
                else:
                    sql = """ update stock_quant_platform set sys_platform_id=%s where id=%s;""" % (platform_id_more,each[0])
                    self._cr.execute(sql)

    ######调整小组库存
    def AdjustmentGroupProductNumber(self):
        product_real=self.getRealProductNumber()
        product_now=self.getNowNumber()
        #product_real={'19':{'1':10,'2':20,'3':30}}
        #product_now={'19':{'1':5,'2':15,'3':40}}
        for product_id, value in product_real.items():
            more_list,less_list={},{}
            for platform_id,qty in value.items():
                qty_now= product_now.get(product_id) and product_now.get(product_id).get(platform_id) or 0
                # if qty<qty_now:
                #     more_list[platform_id]= qty_now - qty
                ####该小组多出了数量【当前大于实际】,系统大于传进来的小组商品数量
                if qty<=qty_now:
                    continue
                else:####当前小于实际 该小组少出了数量
                    less_list[platform_id] = qty -qty_now

            if product_now.get(product_id):
                for platform_id, qty in product_now.get(product_id).items():
                    qty_now = value.get(platform_id) or 0
                    if qty <= qty_now:
                        continue
                    else:  ####当前小于实际 该小组少出了数量
                        more_list[platform_id] = qty - qty_now

            #####如果没有差异
            if not less_list:
                continue
            trans_list=[]
            more_list_copy=more_list.copy()
            for platform_id_less,qty_less in less_list.items():
                ######
                for platform_id_more,qty_add in more_list.items():
                    qty=min(qty_less,qty_add)
                    if not qty:
                        continue
                    trans_list.append((platform_id_less, platform_id_more, qty))
                    more_list[platform_id_more] = more_list[platform_id_more] - qty
                    qty_less=qty_less-qty
                    ####没有剩余数量了
                    if not qty_less:
                        break
            for each in trans_list:
                platform_id_less, platform_id_more, qty = each
                product_id=product_id
                location_id=self.sell_out_number.location_id.id
                location_dest_id=self.sell_out_number.location_dest_id.id

                #######分摊函数
                self.trans_func(product_id,location_id,platform_id_less, platform_id_more, qty)
                self.dest_trans_func(product_id,location_dest_id,platform_id_less, platform_id_more, qty)
        return True

        ################# 调整日记账分录的贷方和借方的金额
        ################# START##################################################

    def AdjustmentVoucher(self,record,account_move_id):
        sql = """select f.platforms,h.id,sum(e.qty * d.cost) as qty,e.qty as quantity,j.name,j.id as ptid from stock_picking a
                       inner join stock_move b on b.picking_id = a.id
                       inner join product_product i on i.id=b.product_id
                       inner  join product_template j on j.id=i.product_tmpl_id
                       inner join stock_quant_move_rel c on c.move_id = b.id
                       inner join stock_quant d on d.id = c.quant_id
                       inner join stock_quant_platform e on e.stock_quant_id = d.id
                       inner join sys_platform f on f.id=e.sys_platform_id
                       inner join res_partner h on h.ref=f.platforms
                       where a.id = %s group by f.platforms,h.id,quantity,j.id,j.name;""" % (record.id)
        self._cr.execute(sql)
        fet = self._cr.fetchall()
        if not fet:
            #####user error
            record = {"returnFlag": "0", "result": {"message": "计算成本出现异常，没有对应的小组库存！"}}
            return record
        account_lines = []
        for value in fet:
            product_template_instance = self.env['product.template'].browse(value[5])
            accounts_data = product_template_instance.get_product_accounts()
            account_lines.append((0, 0, {'account_id': accounts_data.get('stock_valuation').id,
                                         'partner_id': value[1],
                                         'credit': value[2],
                                         'move_id': account_move_id.id,
                                         'journal_id': accounts_data.get('stock_journal').id,
                                         'name': value[4]}))
            account_lines.append((0, 0, {'account_id': accounts_data.get('stock_output').id,
                                         'partner_id': value[1],
                                         'debit': value[2],
                                         'move_id': account_move_id.id,
                                         'journal_id': accounts_data.get('stock_journal').id,
                                         'name': value[4]}))

        if account_lines:
            self.env['account.move.line'].search([('move_id', '=', account_move_id.id)]).unlink()
            account_move_id.write({'line_ids': account_lines})
        else:
            self._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": "调整凭证失败！"}}
            return record

        ################# END##################################################



    @api.model
    def create(self, vals):
        if vals.get('parent_id'):
            vals['name'] = self.env['ir.sequence'].next_by_code('selling.out2') or '/'
        else :
            vals['name'] = self.env['ir.sequence'].next_by_code('selling.out') or '/'

        return super(selling_out, self).create(vals)











# 销售出库单明细（小组）
class selling_out_line(models.Model):
    _name = "selling.out.line"
    _descrption = u"销售出库单明细（小组）";

    selling_out_id = fields.Many2one('selling.out', u'销售出库单')

    # 产品
    product = fields.Many2one('product.product', u'产品')

    # 小组
    group = fields.Many2one('sys.platform', u'小組', required=True)

    # 数量
    number = fields.Integer(u'数量')




# 销售出库单明细
class selling_out_line2(models.Model):
    _name = "selling.out.line2"
    _descrption = u"销售出库单明细";

    selling_out_id = fields.Many2one('selling.out', u'销售出库单')

    # 产品
    product = fields.Many2one('product.product', u'产品')

    # 数量
    qty = fields.Integer(u'数量')

    # 库存数量
    stock_qty = fields.Integer(u'库存数量')
    is_done = fields.Boolean(u'是否出库', default=False)


# 销售缺货明细
class selling_stockout_line(models.Model):
    _name = "selling.stockout.line"
    _descrption = u"销售缺货明细";

    selling_out_id = fields.Many2one('selling.out', u'销售出库单')

    # 产品
    product = fields.Many2one('product.product', u'产品')

    # 数量
    qty = fields.Integer(u'数量')

    # 库存数量
    stock_qty = fields.Integer(u'库存数量')
