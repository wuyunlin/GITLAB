# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.addons.siheng_interface.controllers.send import sendController
from odoo.exceptions import UserError
import json
from datetime import datetime, timedelta
import requests
import urllib2
class group_transfer(models.Model):
    _name = 'group.transfer'
    # 创建表描述
    _description = u'销售出库单'
    # 字段名称
    name = fields.Char(u'调拨单号', required=True, default='New')
    # 产品
    product_id = fields.Many2one('product.product', u'产品')
    # 仓库位置
    location_id = fields.Many2one('stock.location', u'仓库位置')
    # 少的组
    platform_id_less = fields.Many2one('sys.platform', u'调拨组')
    # 多的组
    platform_id_more = fields.Many2one('sys.platform', u'接受调拨数量组')
    # 调拨数量
    qty = fields.Integer(u'调拨数量')
    # 源单号
    selling_out_id = fields.Many2one('selling.out', u'源单号')


    @api.model
    def create(self, vals):

        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('group.transfer') or '/'
        return super(group_transfer, self).create(vals)




