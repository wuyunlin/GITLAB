# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.addons.siheng_interface.controllers.send import sendController
import datetime
import logging
from odoo.exceptions import UserError
class recall_refund(models.Model):
    # 创建表的名称
    _name = 'recall.refund'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    # 创建表描述
    _description = u'采退单'

    # 字段名称
    name = fields.Char(u'单号', required=True, default='New')
    # 供应商
    res_partner_id = fields.Many2one('res.partner', u'供应商', track_visibility='onchange')
    # 收货人
    oig_receiver_id = fields.Many2one('oig.receiver', u'收货人', track_visibility='onchange')
    # 采购订单
    purchase_order_id = fields.Many2one('purchase.order', u'采购订单', track_visibility='onchange')
    # 订单明细
    order_line = fields.One2many('recall.refund.line', 'recall_refund_id', string='订单明细', track_visibility='onchange')
    # 状态 search 查询数据  create 生成采退 issued 下发采退 return 采退回传
    state = fields.Selection([
        ('search', u'查询数据'),
        ('create', u'生成采退'),
        ('issued', u'下发采退'),
        ('return', u'采退回传'),
    ], readonly=True, required=True, string='状态', default='search')
    remark = fields.Char(u'备注')
    parents_sku = fields.Char(u'父级SKU')
    child_sku = fields.Char(u'子SKU(多个;隔开)')
    address = fields.Text(u'地址')
    returns_info = fields.Char(u'退货信息')

    # 删除功能
    @api.multi
    def unlink(self):
        for order in self:
            if order.state == "issued" or order.state == "return":
                raise UserError("当前状态不能进行删除操作，只有【查询数据】以及【生成采退】状态才能删除！")
        return super(recall_refund, self).unlink()


    # 库存查询
    @api.multi
    def search_recall_refund(self):
        if len(self.order_line) > 0:
            self.order_line.unlink()
        dict = {}
        # 收货人
        dict['LotAtt04'] = self.oig_receiver_id.code
        # 库区
        dict['Zone'] = self.env['ir.config_parameter'].sudo().get_param('Returns_Zone')
        # 如果有SKU ，则按照SKU查询，没有则按照货主和仓库进行查询
        if self.child_sku:
            # SKU编码
            dict['SKU'] = self.child_sku
        else:
            # 货主ID
            dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
            # 仓库ID
            dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')

        xmldata = {'data': {'header': dict}}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
           try:
               result = self.env['send.controller'].dispatch('queryINVData', xmldata)
           except Exception as e:
               raise UserError("接口调用出错！")
           if result.get("Response").get("return").get("returnFlag") == "0":
               if result.get("Response").get("return").get("returnDesc") == 'data+not+found!':
                    raise UserError("没有查到数据！")
               else:
                   raise UserError(result.get("Response").get("return").get("returnDesc"))
           else:
               temp = 0
               if len(result.get("Response").get("items")) > 0:
                   for parameter in result.get("Response").get("items").get("item"):
                       temp = 1
                       dictLine = {}
                       dictLine['box_length'] = float(parameter.get("CaseLen"))
                       dictLine['box_width'] = float(parameter.get("CaseWid"))
                       dictLine['box_height'] = float(parameter.get("CaseHei"))
                       dictLine['product'] = parameter.get("SKU")
                       dictLine['number'] = float(parameter.get("Qty"))
                       dictLine['recall_refund_id'] = self.id
                       dictLine['case_number'] = parameter.get("WmsCaseno")
                       dictLine['asn'] = parameter.get("Lottatt07")
                       dictLine['location'] = parameter.get("Zone")
                       dictLine['stock_qty'] = float(parameter.get("Qty"))
                       self.order_line.create(dictLine)
               self.write({'state': 'search'})
               if temp == 0 and len(result.get("Response").get("items").get("item")) > 0:
                   raise UserError('查询有结果,但没有匹配的供应商！')
        else:
            pass
        return True

    # 下发采退单
    @api.multi
    def create_order(self):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        dict = {}
        # 订单编号 暂时数据
        dict['OrderNo'] = self.display_name
        # 订单类型
        dict['OrderType'] = self.env['ir.config_parameter'].sudo().get_param('OrderType_CT')
        # 订单创建时间
        dict['OrderTime'] = now
        # 货主ID
        dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
        # ERP出库计划单号
        dict['SOReference1'] = self.display_name
        # 采退传供应商代码
        dict['ConsigneeID'] = self.res_partner_id.ref
        # 收货人名称
        dict['ConsigneeName'] = self.res_partner_id.name
        # 仓库ID
        dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')
        # 退货信息
        dict['Notes '] = self.returns_info
        # 退货地址
        dict['C_Address1'] = self.address

        line_list = []
        for line in self.order_line:
            if line.number > line.stock_qty:
                raise UserError("下发采退数量不能多于库存数量！")
            line_dict = {}
            # 行号 暂时数据
            line_dict['LineNo'] = line.id
            # 客户编号
            line_dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
            # 产品代码
            line_dict['SKU'] = line.product
            # 收货人信息
            line_dict['LotAtt04'] = self.oig_receiver_id.code
            # 包装或加工数量
            line_dict['QtyOrdered'] = line.number
            line_list.append(line_dict)
        dict['detailsItem'] = line_list
        if dict:
            xmldata = {'header': [dict]}
            if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                if not dict['detailsItem']:
                    raise UserError('采退单明细为空，不能下发！')
                result = self.env['send.controller'].dispatch('PackKitData', xmldata)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    raise UserError('调用失败' + result.get("Response").get("return").get("returnDesc"))
                else:
                    self.write({'state': 'issued'})
            else:
                pass
        return True

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('recall.refund') or '/'
        return super(recall_refund, self).create(vals)

    # onChange()方法，通过采购订单带出供应商和收货人
    @api.multi
    def onchange_partner_receiver(self, purchase_order_id):
        res = {}
        if purchase_order_id:
            res_purchase_order = self.env['purchase.order'].search([('id', '=', int(purchase_order_id))])
            res['oig_receiver_id'] = res_purchase_order.oig_receiver_id
            res['res_partner_id'] = res_purchase_order.partner_id
        return {'value': res}

    @api.multi
    def get_child_sku(self):
        sql = """select string_agg(default_code,';') from product_template where mother_code = '%s'"""%(self.parents_sku)
        self._cr.execute(sql)
        result = self._cr.fetchone()
        if result:
            self.child_sku = result[0]



class recal_refund_line(models.Model):

    _name = "recall.refund.line"
    _descrption = u"采退单明细";

    recall_refund_id = fields.Many2one('recall.refund', u'采退单')
    # 中转仓
    transfer_warehouse = fields.Char(u'中转仓')

    # 箱号
    case_number = fields.Char(u'箱号')

    # 箱长
    box_length = fields.Integer(u'箱长')

    # 箱宽
    box_width = fields.Integer(u'箱宽')

    # 箱高
    box_height = fields.Integer(u'箱高')

    # 满托
    full_care = fields.Char(u'满托')

    # ASN 单号
    asn = fields.Char(u'ASN单号')

    # 产品
    product = fields.Char(u'产品')

    # 数量
    number = fields.Integer(u'数量')

    # 实际数量
    real_number = fields.Integer(u'实际数量')
    # 库位
    location = fields.Char(u'库区')
    # 库存数量
    stock_qty = fields.Integer(u'库存数量')
    # 价格
    price = fields.Float(u'价格')

    # 生成采退单
    @api.multi
    def oe_myservice_1(self):
        if len(self.ids) <= 0:
            raise UserError("请勾选采退订单行！")
        ret = list(set(self[0].recall_refund_id.order_line.ids) ^ set(self.ids))
        if ret:
            self.browse(ret).unlink()
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'recall.refund.line',
            'type': 'ir.actions.act_window',
            'res_id': self[0].recall_refund_id[0].id,
            'context': self[0].recall_refund_id[0].env.context,
        }