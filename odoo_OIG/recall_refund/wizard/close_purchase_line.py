# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
from odoo.addons.oig_purchase.models.account_invoice_support import SPECIAL_REASON
class close_purchase_line(models.TransientModel):
    _name = "create.purchase.line"
    _description = u"生成采退单"

    @api.model
    def default_get(self, fields):
        rec = super(close_purchase_line, self).default_get(fields)
        context = dict(self._context or {})
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')
        if not active_model or not active_ids:
            raise UserError(
                ("请至少选择一个采退单明细！"))
        invoice = self.env[active_model].browse(active_ids[0])
        return rec

    @api.multi
    def create_purchase_line(self):

        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        if len(active_ids) > 0:
            recall_refund_line_result = self.env['recall.refund.line'].search([('id', '=', active_ids[0])])

        if recall_refund_line_result:
            recall_refund_result = self.env['recall.refund.line'].search([('recall_refund_id', '=', recall_refund_line_result.recall_refund_id.id)])
            if recall_refund_result:
                ret = list(set(recall_refund_result.ids) ^ set(active_ids))
            if ret:
                for param in ret:
                    self.env['recall.refund.line'].search([('id', '=', param)]).unlink()
            return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'recall.refund',
                'type': 'ir.actions.act_window',
                'res_id': recall_refund_line_result.recall_refund_id.id,
                'context': self.env.context,
            }


