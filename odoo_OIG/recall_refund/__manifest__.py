# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Recall refund',
    'version': '1.2',
    'category': 'Purchases',
    'sequence': 60,
    'summary': 'Recall refund',
    'description': """
Manage goods requirement by Purchase Orders easily
==================================================
    """,
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['oig_base'],
    'data': [
        'views/recall_refund_views.xml',
        'views/sales_orders_views.xml',
        'views/selling_out_views.xml',
        'views/group_transfer_view.xml',
        'security/ir.model.access.csv',
        'wizard/close_purchase_line_view.xml',
    ],
    'qweb': ['static/src/xml/create_purchase_line.xml'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
