# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
from odoo.addons.oig_purchase.models.account_invoice_support import SPECIAL_REASON
class close_button_wizard(models.TransientModel):
    _name = "close.button.wizard"
    _description = u"订单行关闭"
    @api.model
    def default_get(self, fields):
        rec = super(close_button_wizard, self).default_get(fields)
        context = dict(self._context or {})
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')
        if not active_model or not active_ids:
            raise UserError(
                ("请至少选择一个付款明细"))
        invoice = self.env[active_model].browse(active_ids[0])
        return rec

    @api.multi
    def oe_myservice_1(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for purchase_pay in self.env['purchase.order.line'].browse(active_ids):
            purchase_pay.button_close()

        order_id = self.env['purchase.order.line'].browse(active_ids[0]).order_id.id
        purchase_order_line = self.env['purchase.order.line'].search([('order_id', '=', order_id)])

        # 判断该采购订单下所有明细是否都已经关闭，如果已经关闭，则关闭整张采购订单
        temp = 0
        for line in purchase_order_line:
            if line.open_state == "open":
                temp = 1
        if temp == 0:
            self.env['purchase.order'].search([('id', '=', order_id)]).write({'open_state': 'close'})

        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            'res_id': order_id,
            'context': self.env.context,
        }

    @api.multi
    def confirm(self):
        ########查询满足条件的付款单
        def get_invoice():
            for invoice in invoice_line_instance[0].purchase_id.invoice_ids:
                if invoice.type==invoice_line_instance[0].invoice_id.type  and invoice.date_invoice == date and invoice.state in ['draft'] and invoice.id<> invoice_line_instance[0].invoice_id.id:
                    return invoice.id
            return False

        def get_line(invoice_line,search_invoice):
            fit_line=invoice_line.search([('invoice_id','=',search_invoice),('purchase_line_id','=',invoice_line.purchase_line_id.id)])
            return fit_line and fit_line[0] or False


        ###取得勾选的付款明细ID集合
        active_ids = self._context.get('active_ids',[])
        if not active_ids:
            return True
        date=self.date
        #获取付款明细实例
        invoice_line_instance=self.env['account.invoice.line'].browse(active_ids)
        old_invoice = invoice_line_instance[0].invoice_id
        dict = invoice_line_instance[0].purchase_id.action_view_invoice()
        ###判断日期大于源账单日期
        if old_invoice.date_invoice>self.date:
            raise UserError(('账单日期只可以向后延期不可以向前延！'))
        #######如果调整日期等于当前付款单的日期
        if date == old_invoice.date_invoice:
            return dict
        #######如果付款单的状态不处于草稿状态 不能进行调整
        if old_invoice.state not in ['draft'] or old_invoice.audit_state not in ['first']:
            raise UserError(('当前付款申请单已经进入审批阶段，不能调整日期！'))
        #######如果所有明细行都被选中 那么直接修改主表日期即可
        all_selected = False
        if len(old_invoice.invoice_line_ids) == len(active_ids):
            all_selected=True
        #####获取新的付款单
        search_invoice=get_invoice()
        ####没有新的付款单
        if not search_invoice:
            ####如果旧的付款单被全部选择了，直接修改旧的付款单的日期
            if all_selected:
                default_line={'date_invoice': date, 'flag': 'delay'}
                if old_invoice.flag=='manual' and SPECIAL_REASON[0] not in (old_invoice.special_reason or ''):
                    default_line.update({'special_reason':'%s%s'%(old_invoice.special_reason and old_invoice.special_reason+'；' or '',SPECIAL_REASON[0] )})
                old_invoice.write(default_line)
            ###没有全部选中 copy一个付款单出来
            else:
                default_line={'flag': 'delay', 'date_invoice': date, 'invoice_line_ids': False}
                # if old_invoice.flag=='manual' and SPECIAL_REASON[0] not in (old_invoice.special_reason or ''):
                #     default_line.update({'special_reason':'%s%s'%(old_invoice.special_reason and old_invoice.special_reason+'；' or '',SPECIAL_REASON[0] )})
                new_invoice = old_invoice.copy(default=default_line)
                invoice_line_instance.write({'invoice_id': new_invoice.id})
        else:
            ######将付款明细移到找到的付款单下面
            ######判断是否当前产品已有付款明细行？ 1已有：合并数量并将当前付款明细删除 2 没有则将当前付款明细搬家
            for invoice_line in invoice_line_instance:
                fit_line=get_line(invoice_line,search_invoice)
                if fit_line:
                    quantity=invoice_line.quantity
                    invoice_line.unlink()
                    fit_line.write({'quantity':quantity+fit_line.quantity})
                else:
                    invoice_line.write({'invoice_id': search_invoice})

        #####如果所有明细被选中且找到了新的付款单 那么将旧的付款单删除
        if all_selected and search_invoice:
            old_invoice.unlink()
        #dict = invoice_line_instance[0].purchase_id.action_view_invoice()
        return dict





