# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.addons.siheng_interface.controllers.send import sendController
from odoo.exceptions import UserError
import json
from datetime import datetime, timedelta
import requests
import urllib2
class interface_log(models.Model):
    _name = 'interface.log'
    # 请求参数
    request_parameter = fields.Char(u'请求参数')
    # 记录时间
    record_time = fields.Datetime(u'记录时间')
    # 状态
    record_state = fields.Selection([
        ('resolved', u'已解决'),
        ('Unsolved', u'未解决'),
        ('ok', u'成功'),
    ], string=u'记录状态')
    # 返回的错误信息
    error_message = fields.Char(u'错误信息')
    # 下发状态
    issued_state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发'),
    ], string=u'下发状态')
    # 请求方法
    method = fields.Char(u'请求方法')
    # 处理的完成时间
    complet_time = fields.Datetime(u'处理完成时间')
    # 发送方式
    request_way = fields.Selection([
        ('ask_out', u'向外面发送请求'),
        ('ask_inside', u'向内发送请求'),
    ], string=u'请求方式')
    # 参数重新下发
    @api.multi
    def interface_log_issued(self):
        request_parameter = json.loads(self.request_parameter)
        method = self.method
        if "except_id" in self.request_parameter:
            pass
        else:
            request_parameter['except_id'] = self.id
        # origin = request_parameter['origin']
        # data = request_parameter['data']
        # method = request_parameter['method']
        way = self.request_way
        if way == 'ask_out':
                xmldata = request_parameter
                if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                    result = self.env['send.controller'].dispatch(method, xmldata)
                    if result.get("Response").get("return").get("returnFlag") == "1":
                        self.write({'record_state': "resolved", 'issued_state': "hasIssued",
                                    'complet_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')})
                    else:
                        raise UserError('请求失败,参数有误!')
        else:
            xmldata = {'jsonrpc': '2.0', 'id': 1, 'params': request_parameter}
            data = json.dumps(xmldata)
            headers = {'Content-Type': 'application/json; charset=utf-8'}
            base_url = self.env['ir.config_parameter'].get_param('web.base.url')
            log_url = self.env['ir.config_parameter'].get_param('log_url')
            url = base_url + log_url
            # url = 'http://localhost:8069/api/wms'
            response = requests.post(url, data=data, headers=headers, verify=False)
            # response = urllib2.urlopen('http://localhost:8069/api/wms', data)  # 提交，发送数据
            # content = response.read()  # 获取提交后返回的信息
            text = json.loads(response.text)
            #result = vmcController(DataSet).vmc_call_kw(self, xmldata)
            if text.get("result").get('returnFlag') == "1":
                self.write({'record_state': "resolved", 'issued_state': "hasIssued", 'complet_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')})
            else:
                raise UserError('请求失败,参数有误!')
        return True

    # 处理异常数据重新下发
    def do_exception_data(self, *args):
        now = datetime.now() .strftime("%Y-%m-%d")
        interface_log = self.env['interface.log'].search(
            [('create_date', '>=', now + ' 00:00:00'), ('create_date', '<', now + ' 23:59:59'),
             ('record_state', '=', 'Unsolved'), ('method', 'in', args)])
        for log in interface_log:
            log.interface_log_issued()