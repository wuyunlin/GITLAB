# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _

class  cn_province(models.Model):
    _name = 'cn.province'
    _description = u'省'
    name = fields.Char(u'名称')
    code = fields.Char(u'编号')
class  cn_city(models.Model):
    _name = 'cn.city'
    _description = u'市'
    name = fields.Char(u'名称')
    code = fields.Char(u'编号')
    cn_province_id = fields.Many2one('cn.province', u'省')
    trans_day = fields.Integer(u'物流天数')


class  sys_platform(models.Model):
    _name = 'sys.platform'
    _description = u'小组'
    name = fields.Char(u'名称')
    code = fields.Char(u'编号')



class  line_platform(models.Model):
    _name = 'line.platform'
    _description = u'小组'
    qty = fields.Integer(u'数量')
    qty_origin = fields.Integer(u'源数量')
    sys_platform_id = fields.Many2one('sys.platform', u'小组')
    purchase_order_line_id = fields.Many2one('purchase.order.line', u'采购订单行',ondelete='cascade')
    @api.model
    def create(self, vals):
        if vals and vals.get('qty'):
            vals.update({'qty_origin':vals['qty']})
        request = super(line_platform, self).create(vals)
        return request