# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
class carrier(models.Model):
    _name = 'oig.carrier'
    _description = u'承运人'
    name = fields.Char(u'承运人名称')
    code = fields.Char(u'承运人编码')
    # 下发状态
    issued_state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发'),
    ], string=u'下发状态', default='noIssued')

    # 承运人下发
    @api.multi
    def carrier_issued(self):
        dict = {}
        parameter = ['CustomerID', 'Customer_Type', 'Descr_C', 'Descr_E', 'Address1', 'Address2', 'Address2', 'Address3', 'Country', 'Province', 'City', 'Zip', 'Contact1', 'Contact1_Tel1', 'Contact1_Tel2', 'Contact1_Fax',
                     'Contact1_Title', 'Contact1_Email', 'Contact2', 'Contact2_Tel1', 'Contact2_Tel2', 'Contact2_Fax', 'Contact2_Title', 'Contact3', 'Contact3_Tel1', 'Contact3_Tel2', 'Contact3_Fax', 'Contact3_Title', 'Currency',
                     'Currency', 'Stop', 'R_Owner', 'UDF1', 'UDF2', 'UDF3', 'UDF4', 'UDF5', 'NOTES', 'BankAccount', 'easycode']
        for param in parameter:
            dict[param] = ''
        dict['CustomerID'] = self.code
        dict['Customer_Type'] = self.env['ir.config_parameter'].sudo().get_param('Customer_Type_CA')
        dict['Descr_C'] = self.name
        if dict:
            xmldata = {'header': dict}
            if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                result = self.env['send.controller'].dispatch('putCustData', xmldata)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    raise UserError('调用失败')
                else:
                    self.write({'issued_state': "hasIssued"})
            else:
                pass
        return True

        # 重写创建方法
    @api.model
    def create(self, vals):
        res = super(carrier, self).create(vals)
        if res.code or res.name:
            res.carrier_issued()
        return res

    # 重写修改方法
    @api.multi
    def write(self, vals):
        res = super(carrier, self).write(vals)
        if vals.get('code') or vals.get('name'):
            self.carrier_issued()
        return res

