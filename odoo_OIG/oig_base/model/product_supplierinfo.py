# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _


class  product_supplierinfo(models.Model):
    _inherit = 'product.supplierinfo'
    is_bz = fields.Boolean(u'是否包装', default=False)
    is_tb = fields.Boolean(u'是否贴标', default=False)
    _sql_constraints = [('name_product_tmpl_id_uniq', 'unique (name,product_tmpl_id)', "供应商必须唯一")]


