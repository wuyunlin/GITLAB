# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
class  mrp_bom(models.Model):
    _inherit = 'mrp.bom'
    state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发')
        ], string=u'下发状态', default="noIssued")

    # BOM信息下发接口
    @api.multi
    def mrp_issued(self):
        data = []
        dict = {}
        parameter = ['CustomerID', 'KitSKU', 'Descr_C', 'Descr_E', 'addtime', 'addwho', 'EditTime', 'Editwho',
                     'Enable_Flag', 'Kitting_Rate', 'WorkingProcedureDescr', 'PackMaterialFlag', 'VersionNo']
        for param in parameter:
            dict[param] = ''
        dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
        dict['KitSKU'] = self.product_tmpl_id.default_code
        dict['WorkingProcedureDescr '] = self.code
        dict['Descr_C'] = self.product_tmpl_id.name
        dict['Enable_Flag'] = 'Y'
        line_list = []
        for mrp in self.bom_line_ids:
            dictline = {}
            parameters = ['CustomerID', 'KitSKU', 'Descr_C', 'Descr_E', 'addtime', 'addwho', 'EditTime', 'Editwho',
                         'Enable_Flag', 'Kitting_Rate', 'WorkingProcedureDescr', 'PackMaterialFlag', 'VersionNo']
            for params in parameters:
                dictline[params] = ''
            dictline['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
            if not self.product_tmpl_id.default_code:
                raise UserError("产品"+self.product_tmpl_id.name+"对应的父件SKU代码不存在！")
            else:
                dictline['KitSKU'] = self.product_tmpl_id.default_code
            dictline['Sku'] = mrp.product_id.default_code
            dictline['Qty'] = mrp.product_qty
            dictline['Descr_C'] = mrp.product_id.name
            line_list.append(dictline)
        dict['detailsItem'] = line_list
        if dict:
            xmldata = {'header': [dict]}
            if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                result = self.env['send.controller'].dispatch('BomListData', xmldata)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    raise UserError('调用失败,错误信息是： '+result.get("Response").get("return").get("returnDesc"))
                else:
                    self.write({'state': "hasIssued"})
            else:
                pass
        return True

    # 重写创建方法
    @api.model
    def create(self, vals):
        res = super(mrp_bom, self).create(vals)
        if res.code or res.name:
            res.mrp_issued()
        return res

    # 重写修改方法
    @api.multi
    def write(self, vals):
        res = super(mrp_bom, self).write(vals)
        if vals.get('code') or vals.get('product_tmpl_id'):
            self.mrp_issued()
        return res





