# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
class  oig_receiver(models.Model):
    _name = 'oig.receiver'
    _description = u'收货人'
    name = fields.Char(u'收货人名称')
    code = fields.Char(u'收货人编码')
    stock_location_id = fields.Many2one('stock.location', u'收货人库位')
    stock_picking_type_id = fields.Many2one('stock.picking.type', u'收货类型')
    stock_picking_type_id2 = fields.Many2one('stock.picking.type', u'样品类型')
    issued_state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发'),
    ], string=u'下发状态', default='noIssued')
    is_compute = fields.Selection([
        ('N', u'非自动'),
        ('Y', u'自动'),
    ], string=u'是否自动计算体积', default='N')

    # 收货人下发
    @api.multi
    def issued_receiver(self):
        data = []
        dict = {}
        parameter = ['CustomerID', 'Customer_Type', 'Descr_C', 'Descr_E', 'Address1', 'Address2', 'Address2', 'Address3', 'Country', 'Province', 'City', 'Zip', 'Contact1', 'Contact1_Tel1', 'Contact1_Tel2', 'Contact1_Fax',
              'Contact1_Title', 'Contact1_Email', 'Contact2', 'Contact2_Tel1', 'Contact2_Tel2', 'Contact2_Fax', 'Contact2_Title', 'Contact3', 'Contact3_Tel1', 'Contact3_Tel2', 'Contact3_Fax', 'Contact3_Title', 'Currency',
              'Currency', 'Stop', 'R_Owner', 'UDF1', 'UDF2', 'UDF3', 'UDF4', 'UDF5', 'NOTES', 'BankAccount', 'easycode']
        for param in parameter:
            dict[param] = ''
        dict['CustomerID'] = self.code
        dict['Customer_Type'] = self.env['ir.config_parameter'].sudo().get_param('Customer_Type_CO')
        dict['Descr_C'] = self.name
        dict['Zip'] = self.stock_location_id.transfer_warehouse
        dict['UDF5'] = self.is_compute
        data.append(dict)
        if dict:
            xmldata = {'header': data}
            if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                result = self.env['send.controller'].dispatch('putCustData', xmldata)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    raise UserError('调用失败'+ result.get("Response").get("return").get("returnDesc"))
                else:
                    self.write({'issued_state': "hasIssued"})
            else:
                pass
        return True

    # 重写创建方法
    @api.model
    def create(self, vals):
        res = super(oig_receiver, self).create(vals)
        res.issued_receiver()
        return res

    # 重写修改方法
    @api.multi
    def write(self, vals):
        res = super(oig_receiver, self).write(vals)
        if vals.get('code') or vals.get('name') or vals.get('stock_location_id'):
            self.issued_receiver()
        return res
    @api.multi
    def backup_ar_data(self):
        sql="""delete from stock_jc_view where jc_date=current_date  ;
                insert into stock_jc_view(sku,warehouseid,qty,date,platforms,sites, subgroupwarehouseid,ftype,jc_date)
                select sku,warehouseid,qty,date,platforms,sites, subgroupwarehouseid,ftype ,current_date  from stock_current_view;
                delete from purchase_move_line_jc_view where jc_date=current_date  ;
                insert into purchase_move_line_jc_view(platforms,sites,subgroupwarehouseid,sku,billno,totalqty,
                date,stage,type,warehouseid,destinationwarehouseid,forearrivedate,jc_date)
                select *,current_date from purchase_move_line_view_bk;
                delete from product_first_date_jc where jc_date=current_date ; 
                insert into product_first_date_jc(sku,warehouseid,ftype,date,jc_date)
                select * ,current_date from product_first_date_bk; """
        self._cr.execute(sql)
        return True
    @api.multi
    def backup_stock_price(self):
        sql=""" select 1 from  stock_price_func();
                 delete from stock_jc_cost_view where jc_date=current_date;
                 insert into stock_jc_cost_view (fitemid,fnumber,whcode,qty,cost,prod_code,jc_date)
                 select *,current_date from stock_current_cost_view ;"""
        self._cr.execute(sql)
        return True

    @api.multi
    def data_fix(self):
        land_obj = self.env['stock.landed.cost']
        # 创建成本调整单
        ########### 备注：将小数准确性中Product Price 改为3，增加Account 为3
        picking_instance=self.env['stock.picking'].browse(8566)
        result = land_obj.auto_cost_adjust(picking_instance)
        if result['flag'] == 0:
            self._cr.rollback()
            record = {"returnFlag": "0", "result": {"message": result['message']}}
            return record
        # 通过成本调整单拣货字段查找对应的成本调整单并将名称修改为第三方入库单号
        stock_landed_cost = self.env['stock.landed.cost'].search([('picking_ids', '=', picking_instance.name)])
        stock_landed_cost.write({'name': picking_instance.name})