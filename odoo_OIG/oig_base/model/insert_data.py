# -*- coding: utf-8 -*-
from odoo import api, fields, models
import datetime
import json
class insert_data(models.Model):
    _name = 'insert.data'
    _description = u'插入数据'
    purchase_order = fields.Char(string=u'订单编号')
    product_id = fields.Char(string=u'产品')
    partner_id = fields.Char(string=u'供应商')
    oig_receiver_id = fields.Char(string=u'收货人')
    logistics_name = fields.Char(string=u"物流名称")
    logistics_number = fields.Char(string=u'物流单号')
    state = fields.Selection([
        ('0', u'失败'),
        ('1', u'成功'),
        ('2', u'待处理')
    ], string=u'状态', default='2')
    product_qty = fields.Integer(string=u'产品数量')
    packing_instruction = fields.Char(string=u'包装要求')
    quality_inspection = fields.Char(string=u'质检要求')
    description = fields.Char(string=u'采购说明')
    date_plan = fields.Date(string=u'预计交货日期')
    is_sample = fields.Selection([
        ('Y', u'是'),
        ('N', u'否'),
    ], string=u'是否提样', default='N')
    price = fields.Float(string=u'价格')

    def data_insert(self, *args):
        sql = """select id,product_id,oig_receiver_id,purchase_order,partner_id,logistics_name,logistics_number
                         from insert_data a where id=(select min(id) from insert_data where purchase_order=a.purchase_order) and state = '2' limit '%s'"""%(args[1])
        self._cr.execute(sql)
        fet_result = self._cr.fetchall()
        if fet_result:
            for param in fet_result:
                # 查询收货人是否存在
                oig_receiver = self.env['oig.receiver'].search([('name', '=', param[2])])
                if not oig_receiver:
                    self.do_exception(param[3], param[1], param[4], "收货人不存在！")
                    continue

                # 查询产品是否存在
                product_template = self.env['product.template'].search([('default_code', '=', param[1])])
                if not product_template:
                    self.do_exception(param[3], param[1], param[4], "产品信息不存在！")
                    continue

                # 查询供应商是否存在
                res_partner = self.env['res.partner'].search([('name', '=', param[4])])
                if not res_partner:
                    self.do_exception(param[3], param[1], param[4], "供应商不存在！")
                    continue

                now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
                dict = {}
                # 采购订单号
                dict['PONo'] = param[3]
                # 采购订单类型
                dict['POType'] = self.env['ir.config_parameter'].sudo().get_param('POType')
                # 货主ID
                dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
                # 创建时间
                dict['POCreationTime'] = now
                # 外部单号
                dict['POReference1'] = param[3]
                # 创建时间
                dict['ADDTIME'] = now
                # 创建人
                dict['ADDWHO'] = "admin"
                # 供应商代码
                dict['SupplierID'] = res_partner.ref
                # 仓库
                dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')
                # 供应商名称
                dict['Supplier_Name'] = res_partner.name
                # 退税单号(暂时写法)
                dict['POReference2'] = ""
                # 备注
                dict['UserDefine1'] = False
                # 物流名称
                dict['UserDefine2'] = param[5]
                # 物流单号
                dict['UserDefine3'] = param[6]
                # 外部单号
                dict['POReference1'] = False
                # 是否赊购
                if param[6]:
                    dict['UserDefine4'] = True
                else:
                    dict['UserDefine4'] = False
                line_list = []
                sql_line = """select id,product_id,oig_receiver_id,purchase_order,partner_id,
                                        logistics_name,logistics_number,product_qty,packing_instruction,quality_inspection,date_plan,is_sample from insert_data where state != '1' and purchase_order = '%s'"""%(param[3])
                self._cr.execute(sql_line)
                purchase_line_result = self._cr.fetchall()

                for line in purchase_line_result:
                    product_result = self.env['product.template'].search([('default_code', '=', line[1])])
                    line_dict = {}
                    # 采购订单号
                    line_dict['PONo'] = param[3]
                    # 采购订单行号
                    line_dict['POLineNo'] = line[0]
                    # 单位
                    line_dict['UOM'] = "Unit(s)"
                    # 质检要求
                    line_dict['UserDefine1'] = line[9]
                    # 包装要求
                    line_dict['UserDefine2'] = line[8]
                    # 是否提样
                    line_dict['UserDefine3'] = line[11]

                    if product_result:
                        # 产品
                        line_dict['SKU'] = product_result.default_code
                        # 产品中文描述
                        line_dict['SKUDescrC'] = product_result.name
                        # 订单数量
                        line_dict['OrderedQty'] = line[7]
                    else:
                        self.do_exception(param[3], line[1], param[4], "产品信息不存在！")
                        continue

                    # 收货时间
                    for product_supplier in product_template.seller_ids:
                        if product_supplier.display_name == param[4]:
                                line_dict['ReceivedTime'] = (datetime.date.today() + datetime.timedelta(days=product_supplier.delay)).strftime('%Y-%m-%d') + " 00:00:00"
                    oig_receiver = self.env['oig.receiver'].search([('name', '=', line[2])])
                    if oig_receiver:
                        # 收货人属性
                        line_dict['LotAtt04'] = oig_receiver.code
                    else:
                        self.do_exception(param[3], line[1], param[4], "收货人不存在！")
                        continue
                    line_list.append(line_dict)
                    dict['detailsItem'] = line_list

                if not dict.get('detailsItem'):
                    self.do_exception(param[3], param[1], param[4], param[3] + "采购订单对应的明细信息不全！")
                    continue

                if dict:
                    xmldata = {'header': [dict]}
                    if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                        result = self.env['send.controller'].dispatch('putPOData', xmldata)
                        if result.get('Response').get('return').get('returnDesc') == "ok":
                            insert_data_rsult = self.env['insert.data'].search([('purchase_order', '=', param[3])])
                            for param in insert_data_rsult:
                                param.write({'state': '1'})
                        else:
                            self.do_exception(param[3], param[1], param[4], result.get("Response").get("return").get("returnDesc"))
                            continue
        else:
            self.do_exception("", "", "", "未查到数据！")

    # 执行异常信息的记录
    def do_exception(self, purchase_order, product, res_partner, reason):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        exception_sql = """insert into exception_data(purchase_order,product,res_partner,exception_reason,create_date) 
                                    VALUES ('%s','%s','%s','%s','%s')"""%(purchase_order, product, res_partner, reason, now)
        self.env.cr.execute(exception_sql)
        purchase_order_result = self.env['insert.data'].search([('purchase_order', '=', purchase_order)])
        for insert_param in purchase_order_result:
            insert_param.write({'state': '0'})

class exception_data(models.Model):
    _name = 'exception.data'
    _description = u'插入异常数据'
    purchase_order = fields.Char(string=u'订单编号')
    product = fields.Char(string=u'产品名称')
    res_partner = fields.Char(string=u'供应商')
    exception_reason = fields.Text()