# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from datetime import datetime, timedelta
# from odoo.addons.siheng_interface.controllers.common import create_bill
# from odoo.addons.siheng_interface.controllers.common import create_line_bill
import json
import requests
import logging

_logger = logging.getLogger(__name__)

class purchase_stockin_log(models.Model):
    _name = 'purchase.stockin.log'
    _description = u'采购入库记录表'

    purchaser_no = fields.Char(u'采购订单号')
    origin = fields.Char(u'Wms入库单单号')
    remark = fields.Char(u'备注')
    default_code = fields.Char(u'产品编码')
    product_qty = fields.Integer(u'入库数量')
    refuse_qty = fields.Integer(u'拒收数量')
    sample_qty = fields.Integer(u'抽样数量')
    line_remark = fields.Char(u'明细备注')
    create_time = fields.Datetime(u'创建时间')
    state = fields.Char(u'状态') # 0:未处理  1:K3推送成功  2:K3推送失败 3:跑批生成采购入库单成功

    # 对采购入库记录表中的数据进行其他入库操作
    @api.multi
    def create_other_stockin(self):
        # 源位置为供应商Vendors
        src_warehouse_id = self.env['stock.location'].search([('name', '=', '供应商')]).id
        # 样品仓
        sample_warehouse_id = self.env['stock.location'].search([('name', '=', '样品仓')]).id
        # 分拣类型（收货类型）
        stock_picking_type = self.env['stock.picking.type'].search([('name', '=', '采购入库收货类型')])
        # 分拣类型（抽样类型）
        stock_picking_type2 = self.env['stock.picking.type'].search([('name', '=', '采购入库样品类型')])

        # 查询所有需要入库的采购订单
        sql = """select a.purchaser_no from purchase_stockin_log a
                    INNER JOIN insert_data b on b.purchase_order = a.purchaser_no and b.product_id = a.default_code
                    where a.state = '0' 
                    GROUP BY a.purchaser_no ORDER BY purchaser_no"""
        self.env.cr.execute(sql)
        purchase_nos = self.env.cr.fetchall()

        for purchase_no in purchase_nos:
            sql = """select a.*,b.oig_receiver_id,b.price from purchase_stockin_log a 
                        INNER JOIN insert_data b on b.purchase_order = a.purchaser_no and b.product_id = a.default_code
                        where a.state = '0' and a.purchaser_no = '%s'""" %(purchase_no)
            self.env.cr.execute(sql)
            data = self.env.cr.dictfetchall()
            # 生成采购入库单
            picking_instance = None
            for line in data:
                if not picking_instance:
                    stock_location_id = self.env['stock.location'].search([('name', '=', line['oig_receiver_id'])]).id
                    # 生成采购入库单主表
                    params1 = {'purchase_no': line.get('purchaser_no'), 'stock_picking_type': stock_picking_type,
                               'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': stock_location_id,
                               'remark': line.get('remark')}
                    picking_instance = self.create_bill(**params1)

                # 获取产品实例
                product_instance = self.env['product.product'].search([('default_code', '=', line['default_code'])])
                # 生成采购入库单明细
                params = {'purchase_no': line.get('purchaser_no'), 'picking_instance': picking_instance,
                          'product_instance': product_instance, 'product_qty': line['product_qty'],
                          'src_warehouse_id': src_warehouse_id, 'dest_warehouse_id': stock_location_id, 'price_unit':line['price'],
                          'remark': line.get('line_remark')}
                move_id = self.create_line_bill(**params)
            # 检查可用
            picking_instance.action_assign()
            for pack in picking_instance.pack_operation_product_ids:
                pack.qty_done = pack.product_qty
            picking_instance.do_transfer()

            # 生成抽样单
            num = 0  # 判断是否有样品需要入库 若大于0，则表示需要生成抽样单
            for line in data:
                if line['sample_qty'] > 0:
                    num += 1
            if num > 0:  # 如果有产品抽样数量大于0 的，则生产抽样单，否则不生成
                picking_sample = None
                for line in data:
                    if not picking_sample:
                        stock_location_id = self.env['stock.location'].search([('name', '=', line['oig_receiver_id'])]).id
                        # 生成抽样单主表
                        params1 = {'purchase_no': line['purchaser_no'] + '抽样单', 'stock_picking_type': stock_picking_type2,
                                   'src_warehouse_id': stock_location_id, 'dest_warehouse_id': sample_warehouse_id}
                        picking_sample = self.create_bill(**params1)
                    # 获取产品实例
                    product_instance = self.env['product.product'].search([('default_code', '=', line['default_code'])])
                    # 生成抽样单明细
                    if line['sample_qty'] > 0:
                        params = {'purchase_no': line['purchaser_no'], 'picking_instance': picking_sample,
                                  'product_instance': product_instance, 'product_qty': line['sample_qty'],
                                  'src_warehouse_id': stock_location_id, 'dest_warehouse_id': sample_warehouse_id}
                        move_id = self.create_line_bill(**params)
                # 检查可用
                picking_sample.action_assign()
                for pack in picking_sample.pack_operation_product_ids:
                    pack.qty_done = pack.product_qty
                picking_sample.do_transfer()

            # 将处理完成数据
            sql = """update  purchase_stockin_log set state = '3'  where purchaser_no = '%s'""" % (purchase_no)
            self.env.cr.execute(sql)
        return True

    # 调用K3接口
    @api.multi
    def send_k3(self):
        # 查询所有需要入库的采购订单
        sql = """select a.purchaser_no from purchase_stockin_log a
                            INNER JOIN insert_data b on b.purchase_order = a.purchaser_no and b.product_id = a.default_code
                            where a.state = '3' 
                            GROUP BY a.purchaser_no ORDER BY purchaser_no"""
        self.env.cr.execute(sql)
        purchase_nos = self.env.cr.fetchall()

        for purchase_no in purchase_nos:
            sql = """select purchaser_no as purchaseno,default_code as product,product_qty as receiptqty,sample_qty as samplingqty  
                      from purchase_stockin_log where state = '3' and purchaser_no = '%s'""" %(purchase_no)
            self.env.cr.execute(sql)
            data = self.env.cr.dictfetchall()
            post_data = json.dumps(data)

            url = self.env['ir.config_parameter'].sudo().get_param('send_k3_url')
            headers = {'Content-Type':'application/json; charset=utf-8'}
            _logger.info(u'send_k3调用K3接口，采购单号：%s：#########################：'%(purchase_no) + str(post_data))
            response = requests.post(url, data=post_data, headers=headers, verify=False)
            content = response.text  # 获取提交后返回的信息
            result = json.loads(content)
            if result.get('code') == '200':
                # 将处理成功数据
                sql = """update  purchase_stockin_log set state = '1'  where purchaser_no = '%s'""" % (purchase_no)
                self.env.cr.execute(sql)
                _logger.info(u'send_k3调用接口成功,采购单号：%s###################：'%(purchase_no) + str(result.get('msg')))
            else:
                # 将处理失败数据
                sql = """update  purchase_stockin_log set state = '2'  where purchaser_no = '%s'""" % (purchase_no)
                self.env.cr.execute(sql)
                _logger.info(u'send_k3调用接口失败，采购单号：%s####################：'%(purchase_no) + str(result.get('msg')))

        return True

    # 生成调拨单主表单
    @api.multi
    def create_bill(self, **kwargs):
        stock_picking = {'move_type': 'direct',
                         'picking_type_id': kwargs.get('stock_picking_type').id,
                         'origin': kwargs.get('purchase_no'),
                         'location_id': kwargs['src_warehouse_id'],
                         'location_dest_id': kwargs['dest_warehouse_id']}
        if kwargs.has_key('date'):
            stock_picking['expected_arrival_date'] = kwargs['date']
            stock_picking['theoretical_arrival_date'] = kwargs['date']
        if kwargs.has_key('remark'):
            stock_picking['remark'] = kwargs['remark']
        picking_instance = self.env['stock.picking'].create(stock_picking)
        return picking_instance

    # 生成调拨单明细
    @api.multi
    def create_line_bill(self, **kwargs):
        stock_move = {'name': kwargs.get('purchase_no'),
                      'picking_id': kwargs.get('picking_instance').id,
                      'product_id': kwargs.get('product_instance').id,
                      'product_uom_qty': kwargs.get('product_qty'),
                      'product_uom': kwargs.get('product_instance').uom_id.id,
                      'location_id': kwargs['src_warehouse_id'],
                      'location_dest_id': kwargs['dest_warehouse_id']}
        if kwargs.has_key('group_id'):
            stock_move['group_id'] = kwargs['group_id']
        if kwargs.has_key('remark'):
            stock_move['remark'] = kwargs['remark']
        if kwargs.has_key('price_unit'):
            stock_move['price_unit'] = kwargs['price_unit']
        move_id = self.env['stock.move'].create(stock_move)
        return move_id