# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.http import request
class retrun_form(models.Model):
    # 创建表的名称
    _name = 'return.form'
    # 创建表描述
    _description = u'退货单'
    # 字段名称
    name = fields.Char(u'单号', required=True, default='New')
    # 收货人
    stock_location_id = fields.Many2one('stock.location', u'收货人', required=True)
    # 日期
    date = fields.Date(u'退货单日期')
    # 源单号
    stock_picking_id = fields.Many2one('stock.picking', u'源单')
    # 订单明细
    return_form_line = fields.One2many('return.form.line', 'return_form_id', string='退货单明细')
    # 退货单状态
    state = fields.Selection([
        ('draft', u'草稿'),
        ('done', u'完成')
    ], string=u'退货单状态', default="draft")

    # 退单
    stock_location_return_id = fields.Many2one('stock.picking', u'退单')

    def button_confirm(self):
        try:
            picking = self.env['stock.picking'].browse(self.stock_picking_id[0].id)
            picking_type_id = picking.picking_type_id.return_picking_type_id.id or picking.picking_type_id.id
            new_picking = picking.copy({
                'move_lines': [],
                'picking_type_id': picking_type_id,
                'state': self.env['ir.config_parameter'].sudo().get_param('stock_state_draft'),
                'origin': picking.name,
                'location_id': picking.location_dest_id.id,
                'location_dest_id': self.stock_location_id[0].id})
            self.write({'stock_location_return_id': new_picking[0].id})

            for line in self.return_form_line:
                for product in self.stock_picking_id.pack_operation_product_ids:
                    if product.product_id[0].display_name == line.product[0].display_name:

                        # 获取反向调拨的数据
                        new_qty = line.number
                        if new_qty:
                            for move in self.stock_picking_id.move_lines:
                                if move.origin_returned_move_id.move_dest_id.id and move.origin_returned_move_id.move_dest_id.state != 'cancel':
                                    move_dest_id = line.move_id.origin_returned_move_id.move_dest_id.id
                                else:
                                    move_dest_id = False
                                if move.product_id.code == line.product.code:
                                    move.copy({
                                        'product_id': line.product.id,
                                        'product_uom_qty': new_qty,
                                        'picking_id': new_picking.id,
                                        'state': self.env['ir.config_parameter'].sudo().get_param('stock_state_draft'),
                                        # 退回仓库
                                        'location_id': self.stock_picking_id.location_dest_id.id,
                                        # 源仓库
                                        'location_dest_id': self.stock_picking_id.location_id.id,
                                        'picking_type_id': picking_type_id,
                                        'warehouse_id': picking.picking_type_id.warehouse_id.id,
                                        'origin_returned_move_id': move.id,
                                        'procure_method': self.env['ir.config_parameter'].sudo().get_param('stock_method'),
                                        'move_dest_id': move_dest_id,
                                    })
                            new_picking.action_confirm()
                            new_picking.action_assign()
                            new_picking.do_transfer()
                            self.write({'state': 'done'})
        except Exception as e:
            raise UserError(e)

        # 处理反向成本调整
        stock_picking = self.stock_picking_id
        query = """select slc.* from stock_landed_cost slc inner join stock_landed_cost_stock_picking_rel slcs on slc.id = slcs.stock_landed_cost_id inner join stock_picking sp on sp.id = slcs.stock_picking_id where sp.id = '%s'"""%(stock_picking.id)
        self.env.cr.execute(query, ())
        new_result = self._cr.fetchall()
        result = request.env['stock.landed.cost'].auto_cost_adjust(self.stock_location_return_id,new_result[0][0])
        return True

    # 自增退货单号
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('return.form') or '/'
        return super(retrun_form, self).create(vals)

class retrun_form_line(models.Model):
    _name = "return.form.line"
    _descrption = u"退货单明细";

    return_form_id = fields.Many2one('return.form', u'退货单')

    # 产品
    product = fields.Many2one('product.product', u'产品')

    # 数量
    number = fields.Integer(u'数量')

