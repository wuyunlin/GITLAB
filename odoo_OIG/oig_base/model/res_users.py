# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
import re
from odoo.exceptions import UserError
class  res_users(models.Model):
    _inherit = 'res.users'

    def check_password_ok(self,pwd):
        ########判断长度
        def checklen(pwd,length):
            if not  len(pwd) >=int( length):
                raise UserError('密码必须包含 大写 小写 数字 符号，且长度大于%s位;您当前密码不满足长度要求'%(length))
            else:
                return True
        #####判断大写
        def checkContainUpper(pwd):
            pattern = re.compile('[A-Z]+')
            match = pattern.findall(pwd)
            if match:
                return True
            else:
                raise UserError('密码必须包含 大写 小写 数字 符号，且长度大于%s位;您当前密码不含大写'%(length))
        #######判断数字
        def checkContainNum(pwd):
            pattern = re.compile('[0-9]+')
            match = pattern.findall(pwd)
            if match:
                return True
            else:
                raise UserError('密码必须包含 大写 小写 数字 符号，且长度大于%s位;您当前密码不含数字'%(length))
        #####判断小写
        def checkContainLower(pwd):
            pattern = re.compile('[a-z]+')
            match = pattern.findall(pwd)
            if match:
                return True
            else:
                raise UserError('密码必须包含 大写 小写 数字 符号，且长度大于%s位;您当前密码不含小写'%(length))
        ######判断符号
        def checkSymbol(pwd):
            pattern = re.compile('([^a-z0-9A-Z])+')
            match = pattern.findall(pwd)
            if match:
                return True
            else:
                raise UserError('密码必须包含 大写 小写 数字 符号，且长度大于%s位;您当前密码不含符号'%(length))
        # 判断密码长度是否合法
        length=self.env['ir.config_parameter'].sudo().get_param('pwd_length') or 8
        lenOK = checklen(pwd,length)
        # 判断是否包含大写字母
        upperOK = checkContainUpper(pwd)
        # 判断是否包含小写字母
        lowerOK = checkContainLower(pwd)
        # 判断是否包含数字
        numOK = checkContainNum(pwd)
        # 判断是否包含符号
        symbolOK = checkSymbol(pwd)
        return True

    @api.multi
    def write(self, values):
        if values.get('password'):
            self.check_password_ok(values.get('password'))
        return super(res_users, self).write(values)
