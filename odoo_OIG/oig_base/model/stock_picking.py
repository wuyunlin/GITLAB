# -*- coding: utf-8 -*-

from odoo import fields, models, api, tools
from odoo.exceptions import UserError
import xlrd
import base64
import datetime
import json
class stock_picking(models.Model):
    _inherit = 'stock.picking'

    expected_arrival_date = fields.Date(u'预计到货日期')

    theoretical_arrival_date = fields.Date(u'理论到货日期')

    asnno = fields.Char(u'ASN单号')

    remark = fields.Char(u'备注')

    # 导入文件字段
    file = fields.Binary(string=u'导入文件', filters='*.xls')

    @api.multi
    def write(self, vals):
        res = super(stock_picking, self).write(vals)
        for each in self:
            if each.theoretical_arrival_date > each.expected_arrival_date:
                raise UserError('预计到货日期只能大于理论到货日期！')
        return res

    @api.multi
    def action_confirm(self):
        for each in self:
            if not each.group_id:
                group_id = self.env['procurement.group'].create({'name': each.name, 'move_type': 'direct'}).id
            else:
                group_id=each.group_id.id
            sql="""update stock_move set group_id=%s where picking_id=%s and group_id is null;
                    update stock_picking set group_id=%s where id=%s"""%(group_id,each.id,group_id,each.id)
            self._cr.execute(sql)
        res=super(stock_picking, self).action_confirm()
        return res

    @api.multi
    def import_line(self):
        try:
            excel = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
        except:
            raise ValidationError('请选择导入文件。')
        sh = excel.sheet_by_index(0)
        # 接收xlsx导入的产品
        dict = {}
        for rx in range(sh.nrows):
            if rx == 0:
                continue
            data = [str(sh.cell(rx, i).value or '') for i in range(2)]
            dict[data[0]] = data[1]
        # 进行数量和sku校验,将不符合的产品提示给用户
        qty_msg = ''
        sku_msg = ''
        for sku,qty in dict.items():
            pack_operation_line = self.pack_operation_product_ids.filtered(lambda x: x.product_id.default_code == sku)
            if pack_operation_line:
                if pack_operation_line.product_qty < float(qty):
                   qty_msg += sku + '; '
            else:
                sku_msg += sku + '; '
        if qty_msg or sku_msg:
            raise UserError ('数量不符合的产品为：%s,\n sku不符合的产品为：%s' %(qty_msg,sku_msg))

        # 将数量写入对应产品的完成数量列中
        for sku, qty in dict.items():
            pack_operation_line = self.pack_operation_product_ids.filtered(lambda x: x.product_id.default_code == sku)
            pack_operation_line.write({'qty_done':qty})
        return True

    @api.multi
    def export_picking_data(self):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        try:
            excel = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
        except:
            raise UserError('请选择导入文件!')
        sh = excel.sheet_by_index(0)
        # 接收xlsx导入的产品
        dict = {}
        for rx in range(sh.nrows):
            if rx == 0:
                continue
            data = [str(sh.cell(rx, i).value or '') for i in range(4)]
            dict['product'] = data[0]
            dict['product_qty'] = data[1]
            dict['price'] = data[2]
            dict['remark'] = data[3]
            if dict['product']:
                product_product = self.env['product.product'].search([('default_code', '=', dict['product'])])
                if product_product:
                    stock_move_dict = {}
                    stock_move_dict['picking_id'] = self.id
                    stock_move_dict['product_id'] = product_product.id
                    stock_move_dict['location_id'] = self.location_id.id
                    stock_move_dict['name'] = product_product.name
                    stock_move_dict['location_dest_id'] = self.location_dest_id.id
                    stock_move_dict['product_uom_qty'] = dict['product_qty']
                    stock_move_dict['price_unit'] = dict['price']
                    stock_move_dict['remark'] = dict['remark']
                    stock_move_dict['product_uom'] = 1
                    self.env['stock.move'].create(stock_move_dict)
                else:
                    raise UserError(dict['product']+' ，该产品在Odoo中没有维护，请先维护该产品！')
        return True


    def export_stock_picking_data(self):
        sql = """SELECT
                    pp.default_code,
                    sm.product_uom_qty
                FROM
                    stock_picking sp
                INNER JOIN stock_move sm ON sp. ID = sm.picking_id
                INNER JOIN product_product pp ON pp. ID = sm.product_id
                WHERE
                    sp.ID = '%s'
                ORDER BY
                    sm.ID"""%(self.id)
        sql = sql.replace('=', '%3d').replace('+', '%2B')
        title = ['产品', '数量']
        result = {'titles': title, 'filename': '调拨数据' + '.xlsx', 'command': sql}
        jsonstr = json.dumps(result)
        return {'type': 'ir.actions.act_url',
                'url': '/web/excel/download?para=%s' % jsonstr,
                'target': 'new'}