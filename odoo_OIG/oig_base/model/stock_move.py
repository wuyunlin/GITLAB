# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

class  stock_move(models.Model):
    _inherit = 'stock.move'
    remark = fields.Char(u'备注')
    ###########################更新价格取数 如果没有价格取最近一次入库的价格 而不是取产品的标准价格
    def get_price_unit(self):
        """ Returns the unit price to store on the quant """
        res=super(stock_move, self).get_price_unit()
        if res:
            return res
        sql="""select cost from stock_quant where location_id=%s and product_id=%s order by in_date desc limit 1"""%(self.location_dest_id.id,self.product_id.id)
        self._cr.execute(sql)
        fet=self._cr.fetchone()
        price=fet and fet[0] or 0
        if price:
            return price
        sql="""select cost from stock_price where location_id=%s and sku=(select default_code from product_product where id=%s) limit 1"""%(self.location_dest_id.id,self.product_id.id)
        self._cr.execute(sql)
        fet=self._cr.fetchone()
        price_his = fet and fet[0] or 0
        if price_his:
            return price_his
        else:
            last_price=self.price_unit or price or price_his or self.product_id.standard_price
            if not last_price:
                raise UserError("产品:%s取不到价格,不允许操作"%(self.product_id.default_code))
            else:
                return last_price

