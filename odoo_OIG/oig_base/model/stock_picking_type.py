# -*- coding: utf-8 -*-

from odoo import fields, models

class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    name = fields.Char(translate=False)