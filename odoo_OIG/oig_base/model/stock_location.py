# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _

class  stock_location(models.Model):
    _inherit = 'stock.location'

    name = fields.Char(u'名称',translate=False)

    # 收货人类型
    transfer_warehouse = fields.Char(u'收货人类型【中转仓】')
    # 移动仓
    stock_location_id_move = fields.Many2one('stock.location', u'移动仓')
    # 发货仓
    stock_location_id_shipping = fields.Many2one('stock.location', u'发货仓')
    # 仓库类型
    ftype = fields.Selection([
        ('shipping_warehouse', u'发货仓'),
        ('transfer_warehouse', u'收货人'),
        ('move_warehouse', u'移动仓'),
    ], string=u'仓库类型')
    # 发货方式
    delivery = fields.Selection([
        ('sea', u'海运'),
        ('air', u'空运'),
    ], string=u'发货方式')
    # 仓库源ID
    origin_id = fields.Integer(u'仓库源ID')
    # 中转天数
    transfer_days = fields.Integer(u'中转天数')
    # 仓库英文名称
    en_name = fields.Char(u'仓库英文名称')

