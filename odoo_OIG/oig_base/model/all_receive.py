# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.addons.siheng_interface.controllers.send import sendController
import logging
from odoo.exceptions import UserError


class all_receive(models.Model):
    _name = 'all.receive'
    _description = u'所有库存数据'

    order_no = fields.Char(u'订单编号')

    qty = fields.Integer(u'数量')

    sku = fields.Char(u'产品')

    receiver = fields.Char(u'收货人')

    ware_house = fields.Char(u'中转仓')

    asn = fields.Char(u'ASN单号')

    box_heigth = fields.Integer(u'箱高')

    box_width = fields.Integer(u'箱宽')

    box_length = fields.Integer(u'箱长')

    box_no = fields.Char(u'箱号')

    supplier = fields.Char(u'供应商')

    full_care = fields.Char(u'满托')

    # 所有库存数据查询
    @api.multi
    def all_receiver_search(self):

        sql = """delete from all_receive"""
        self._cr.execute(sql)

        dict = {}
        # 货主ID
        dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
        # 仓库ID
        dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')

        xmldata = {'data': {'header': dict}}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            result = self.env['send.controller'].dispatch('queryINVData', xmldata)
            if result.get("Response").get("return").get("returnFlag") == "0":
                if result.get("Response").get("return").get("returnDesc") == "data+not+found!":
                    raise UserError('没有查到数据！')
                else:
                    raise UserError(result.get("Response").get("return").get("returnDesc"))
            else:
                try:
                    if len(result.get("Response").get("items")) > 0:
                        for parameter in result.get("Response").get("items").get("item"):
                            dictLine = {}
                            dictLine['box_length'] = float(parameter.get("CaseLen"))
                            dictLine['box_width'] = float(parameter.get("CaseWid"))
                            dictLine['box_heigth'] = float(parameter.get("CaseHei"))
                            dictLine['sku'] = parameter.get("SKU")
                            dictLine['qty'] = float(parameter.get("Qty"))
                            dictLine['box_no'] = parameter.get("WmsCaseno")
                            dictLine['asn'] = parameter.get("Lottatt07")
                            dictLine['supplier'] = parameter.get("SupplierID")
                            dictLine['ware_house'] = parameter.get("WarehouseID")
                            dictLine['receiver'] = parameter.get('LotAtt04')
                            self.create(dictLine)
                except Exception as e:
                    raise UserError(e)
        else:
            pass
        return True
