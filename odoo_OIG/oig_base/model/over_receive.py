# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
class  over_receive(models.Model):
    _name = 'over.receive'
    _description = u'多货库存数据'

    order_no = fields.Char(u'订单编号')

    qty = fields.Integer(u'数量')

    sku = fields.Char(u'产品')

    ware_house = fields.Char(u'中转仓')

    asn = fields.Char(u'ASN单号')

    box_heigth = fields.Integer(u'箱高')

    box_width = fields.Integer(u'箱宽')

    box_length = fields.Integer(u'箱长')

    box_no = fields.Char(u'箱号')

    supplier = fields.Char(u'供应商')

    full_care = fields.Char(u'满托')

    # 多货库存查询
    @api.multi
    def overweight_search(self):
        dict = {}
        # 货主ID
        dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
        # 仓库ID
        dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')

        xmldata = {'data': {'header': dict}}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            result = self.env['send.controller'].dispatch('queryINVData', xmldata)
            if result.get("Response").get("return").get("returnFlag") == "0":
                if result.get("Response").get("return").get("returnDesc") == "data+not+found!":
                    raise UserError('没有查到数据！')
                else:
                    raise UserError(result.get("Response").get("return").get("returnDesc"))
            else:
                if len(result.get("Response").get("items")) > 0:
                    for parameter in result.get("Response").get("items").get("item"):
                        over_receiver = self.env['over.receive'].search([('sku', '=', parameter.get("SKU"))])
                        if over_receiver:
                            over_receiver.unlink()

                        dictLine = {}
                        dictLine['box_length'] = float(parameter.get("CaseLen"))
                        dictLine['box_width'] = float(parameter.get("CaseWid"))
                        dictLine['box_heigth'] = float(parameter.get("CaseHei"))
                        # if parameter.get("SKU"):
                        #     product_template = self.env['product.template'].search([('default_code', '=', parameter.get("SKU"))])
                        dictLine['sku'] = parameter.get("SKU")
                        dictLine['qty'] = float(parameter.get("Qty"))
                        dictLine['box_no'] = parameter.get("WmsCaseno")
                        dictLine['asn'] = parameter.get("Lottatt07")
                        dictLine['supplier'] = parameter.get("SupplierID")
                        dictLine['ware_house'] = parameter.get("WarehouseID")
                        # self.create(dictLine)
                        self.env['over.receive'].create(dictLine)
        else:
            pass
        return True














