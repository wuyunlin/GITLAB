# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.addons.siheng_interface.controllers.send import sendController
import logging
from odoo.exceptions import UserError
class  product_template(models.Model):
    _inherit = 'product.template'
    name = fields.Char(translate=False)
    english_name = fields.Char(u'英文名称', size=200)
    mother_code = fields.Char(u'母代码',size=200, track_visibility='onchange')
    length = fields.Float(digits=(16, 2),string="长(cm)", track_visibility='onchange')
    width = fields.Float(digits=(16, 2), string="宽（cm）", track_visibility='onchange')
    height = fields.Float(digits=(16, 2), string="高（cm）", track_visibility='onchange')
    gross_weight = fields.Float(digits=(16, 2), string="毛重（g）", track_visibility='onchange')
    quality_inspection = fields.Text(u"质检要求", track_visibility='onchange')
    box_type = fields.Char(u'箱型', size=100)
    box_length = fields.Float(digits=(16, 2), string="箱长(cm)", track_visibility='onchange')
    box_width = fields.Float(digits=(16, 2), string="箱宽（cm）", track_visibility='onchange')
    box_height = fields.Float(digits=(16, 2), string="箱高（cm）", track_visibility='onchange')
    packing_bag = fields.Char(u'包装袋', size=100)
    receiverinfo_line = fields.One2many('product.receiverinfo', 'product_template_id', '收货人明细')
    is_bom = fields.Boolean(u'可以配置bom', default=False)
    packing_instruction = fields.Text(u"包装要求", track_visibility='onchange')
    box_qty = fields.Integer(u'箱规')
    trans_volumn = fields.Float(digits=(16, 2), string="发货体积(cm3)")
    state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发')
    ], string=u'下发状态', default="noIssued")
    # 产品源ID
    origin_id = fields.Integer(u'产品源ID')
    # 产品源编号
    origin_number = fields.Char(u'产品源编号')

    # 与收货人产品代码对应关系接口
    @api.multi
    def issued_product(self):
        # 与收货人产品资料代码对应关系接口
        data = []
        dictone = {}
        parameter = ['CustomerID', 'SKU', 'SupplierID', 'Active_Flag', 'UserDefine1', 'UserDefine2', 'UserDefine3', 'UserDefine4', 'UserDefine5',
                     'UserDefine6', 'UserDefine7', 'UserDefine8', 'Notes', 'AddTime', 'Addwho', 'PackID', 'UserDefine9', 'UserDefine10']
        for param in parameter:
            dictone[param] = ''
        for product in self.receiverinfo_line:
            dictnew = dictone.copy()
            dictnew['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
            dictnew['SKU'] = product.product_template_id.default_code
            dictnew['ConsigneeID'] = product.oig_receiver_id.code
            dictnew['UserDefine1'] = product.receive_product_code
            dictnew['UserDefine2'] = product.receive_product_code2
            data.append(dictnew)
        xmldata = {'header': data}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            if len(xmldata.get('header')):
                result = self.env['send.controller'].dispatch('putSkuCoData', xmldata)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    raise UserError('与收货人产品资料代码对应关系接口调用失败!' + result.get("Response").get("return").get("returnDesc"))
                elif result.get("Response").get("return").get("returnFlag") == "2":
                    raise UserError('与收货人产品资料代码对应关系接口调用部分成功!' + result.get("Response").get("return").get("returnDesc"))
                else:
                    self.write({'state': 'hasIssued'})
                    pass

        # 与供应商产品资料代码对应关系接口
        exceptionResult = ''
        productData = []
        productDict = {}
        parameters = ['CustomerID', 'SKU', 'SupplierID', 'Active_Flag', 'UserDefine1', 'UserDefine2', 'UserDefine3', 'UserDefine4', 'UserDefine5',
                      'UserDefine6', 'UserDefine7', 'UserDefine8', 'Notes', 'AddTime', 'Addwho', 'PackID', 'UserDefine9', 'UserDefine10']
        for para in parameters:
            productDict[para] = ''
        for productData_line in self.seller_ids:
            productDicts = productDict.copy()
            productDicts['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
            productDicts['SKU'] = productData_line.product_tmpl_id.default_code
            productDicts['SupplierID'] = str(productData_line.name.ref)
            productDicts['Active_Flag'] = self.env['ir.config_parameter'].sudo().get_param('active_flag_Y')
            if productData_line.product_code:
                productDicts['UserDefine1'] = productData_line.product_code
            else:
                exceptionResult += "产品："+productData_line.product_tmpl_id.name + "，没有收货人产品SKU"
            productData.append(productDicts)
        res_xmldata = {'header': productData}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            if len(res_xmldata.get('header')):
                res_result = self.env['send.controller'].dispatch('putSkuVeData', res_xmldata)
                if res_result.get("Response").get("return").get("returnFlag") == "0":
                   raise UserError('供应商产品资料代码对应关系接口调用失败' + res_result.get("Response").get("return").get("returnDesc")+exceptionResult)
                else:
                   self.write({'state': 'hasIssued'})
                   pass
        else:
            pass

        return True

    # 重写创建操作
    @api.model
    def create(self, vals):
        notnull_keys=['description_purchase','quality_inspection','packing_instruction']
        for key in notnull_keys:
            vals[key]=vals.get(key, '')
        res = super(product_template, self).create(vals)
        res.issued_product()
        return res

    # 重写修改操作
    @api.multi
    def write(self, vals):
        res = super(product_template, self).write(vals)
        if vals.get('seller_ids') or vals.get('receiverinfo_line'):
            self.issued_product()
        return res