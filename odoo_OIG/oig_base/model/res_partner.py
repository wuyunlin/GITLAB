# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
class res_partner(models.Model):
    _inherit = 'res.partner'
    pay_type = fields.Selection([
        ('public', u'公账'),
        ('private', u'私账'),
        ('net', u'网上拍'),
        ], string=u'付款类型', track_visibility='onchange')
    # 內部參照
    ref = fields.Char(u'内部参照', required=True, default='New', track_visibility='onchange')
    cn_province_id = fields.Many2one('cn.province', u'省')
    cn_city_id = fields.Many2one('cn.city', u'市')
    cn_area_id = fields.Many2one('cn.area', u'区')
    trans_day = fields.Integer(u'物流天数')
    pay_way = fields.Selection([
        ('all', u'预付全款'),
        ('pay_pay_delivery', u'预付30 % 尾款款到发货'),
        ('pay_delivery_pay', u'预付30 % 尾款货到付款'),
        ('delivery_then_pay', u'货到付款'),
        ('pay_to_delivery', u'款到发货'),
        ('period_pay', u'账期'),
        ('ali_period_pay', u'阿里账期'),
        ], string=u'付款方式', track_visibility='onchange')
    pay_account = fields.Char(u'付款账号')
    fnameen = fields.Char(u'英文名称')
    fadden = fields.Char(u'英文地址')
    business_licence = fields.Binary(u"营业执照")
    business_file = fields.Char(string="Business Name")
    idcard = fields.Binary(u"身份证")
    file_name = fields.Char(string="File Name")
    idcard_validate = fields.Binary(u"身份证验证结果")
    idcard_file = fields.Char(string="IDCard Name")
    seal = fields.Binary(u"账户盖章")
    seal_file = fields.Char(string="Seal Name")
    is_bz = fields.Boolean(u'是否包装', default=False)
    is_tz = fields.Boolean(u'是否贴标', default=False)
    is_srm = fields.Boolean(u'是否操作SRM', default=False)
    issued_state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发'),
    ], string=u'下发状态')
    account_period = fields.Integer(u'账期')
    type_flag = fields.Selection([
        ('supplier', u'供应商'),
        ('emp', u'职员'),
        ('department', u'部门'),
        ('company', u'关联公司'),
        ('express', u'物流商'),
        ('platform', u'平台'),
    ], string=u'类型标识')

    account_name = fields.Char(u'户名')

    bank_information = fields.Char(u'开户行信息')

    is_buy = fields.Boolean(u'是否预付', compute="_compute_buy", store=True)

    # 源供应商ID
    origin_id = fields.Integer(u'源供应商ID')

    name = fields.Char(u'供应商名称', required=True, track_visibility='onchange')

    @api.multi
    @api.depends('pay_type', 'pay_way')
    def _compute_buy(self):
        for supplier in self:
            if supplier.pay_type == "private" or supplier.pay_way in ("ali_period_pay", "period_pay", "delivery_then_pay", "pay_delivery_pay", "pay_to_delivery"):
                supplier.is_buy = False
            else:
                supplier.is_buy = True

    # 供应商下发
    @api.multi
    def res_partner_issued(self):
        data = []
        dict = {}
        parameter = ['CustomerID', 'Customer_Type', 'Descr_C', 'Descr_E', 'Address1', 'Address2', 'Address2', 'Address3', 'Country', 'Province', 'City', 'Zip', 'Contact1', 'Contact1_Tel1', 'Contact1_Tel2', 'Contact1_Fax',
                     'Contact1_Title', 'Contact1_Email', 'Contact2', 'Contact2_Tel1', 'Contact2_Tel2', 'Contact2_Fax', 'Contact2_Title', 'Contact3', 'Contact3_Tel1', 'Contact3_Tel2', 'Contact3_Fax', 'Contact3_Title', 'Currency',
                     'Currency', 'Stop', 'R_Owner', 'UDF1', 'UDF2', 'UDF3', 'UDF4', 'UDF5', 'NOTES', 'BankAccount', 'easycode']
        for param in parameter:
            
            dict[param] = ''
        dict['CustomerID'] = self.ref
        dict['Descr_C'] = self.name
        dict['Customer_Type'] = self.env['ir.config_parameter'].sudo().get_param('Customer_Type_VE')
        data.append(dict)
        if self.type_flag == 'supplier':
            if data:
                xmldata = {'header': data}
                if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                    result = self.env['send.controller'].dispatch('putCustData', xmldata)
                    if result.get("Response").get("return").get("returnFlag") == "0":
                        raise UserError('调用失败'+ result.get("Response").get("return").get("returnDesc"))
                    else:
                        self.write({'issued_state': "hasIssued"})
                else:
                    pass
        return True

    ###########获取供应商预付款金额【付款金额-到货金额】
    def _purchase_all_money(self):
        PurchaseOrder = self.env['purchase.order']
        orders = PurchaseOrder.search([('partner_id', 'child_of', self.id), ('state', 'in', ['purchase', 'done']), ('open_state', '!=', 'close')])
        allMoney = 0
        for order in orders:
            allMoney+=sum(line.qty_paid-(line.qty_received * line.price_subtotal/line.product_qty) for line in order.order_line)
        return allMoney

    #
    # 省改变的时候改变市的下拉列表
    # @api.onchange('cn_province_id')
    # def onchange_province_id(self):
    #     if not self.cn_province_id:
    #         self.cn_city_id = False
    #     else:
    #         bom = self.env['mrp.bom']._bom_find(product=self.product_id, picking_type=self.picking_type_id,
    #                                             company_id=self.company_id.id)
    #         if bom.type == 'normal':
    #             self.bom_id = bom.id
    #         else:
    #             self.bom_id = False
    #         self.product_uom_id = self.product_id.uom_id.id
    #         return {'domain': {'product_uom_id': [('category_id', '=', self.product_id.uom_id.category_id.id)]}}

    # 修改市同时改变物流天数
    @api.onchange('cn_city_id')
    def onchange_city_id(self):
        if not self.cn_city_id:
            self.trans_day = 0
        else:
            self.trans_day = self.cn_city_id.trans_day or 0

    # 修改省的时候市下面清空
    @api.onchange('cn_province_id')
    def onchange_province_id(self):
        self.cn_city_id = False

    @api.model
    def create(self, vals):
        if vals.get('ref', 'New') == 'New':
            vals['ref'] = self.env['ir.sequence'].next_by_code('res.partner') or '/'
        supplier = self.env['res.partner'].search([('name', '=', vals.get('name')), ('type_flag', '=', 'supplier')])
        if len(supplier) > 0:
            raise UserError("供应商已存在，不能再次创建！")
        res = super(res_partner, self).create(vals)
        if vals.get('ref') or vals.get('name'):
            res.res_partner_issued()
        return res

    @api.multi
    def write(self, vals):
        res = super(res_partner, self).write(vals)
        if vals.get('ref') or vals.get('name'):
            supplier = self.env['res.partner'].search([('name', '=', vals.get('name')), ('type_flag', '=', 'supplier')])
            if len(supplier) > 1:
                raise UserError("该供应商已存在，不能更改为这个供应商！")
            else:
                self.res_partner_issued()
        return res