# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
import xlrd
import base64
import xlwt
from datetime import datetime
from xlrd import xldate_as_tuple
import json
class change_order(models.Model):
    _name = 'change.order'
    _description = u'调整延期的单据日期'
    order_name = fields.Char(u'单号')
    date_time = fields.Char(u'预计到达日期')
    # 下发状态
    origin = fields.Char(u'源单号')
    state = fields.Selection([
        ('N', u'未操作'),
        ('Y', u'已操作'),
    ], string=u'操作状态')
    # 导入文件字段
    file = fields.Binary(string=u'导入文件', filters='*.xls')

    # 导入延期文件并处理
    @api.multi
    def export_order_data(self):
        try:
            excel = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
        except:
            raise UserError('请选择导入文件!')
        self.env['change.order'].search([('id', '!=', '0008')]).unlink()
        sh = excel.sheet_by_index(0)
        # 接收xlsx导入的产品
        uid = self.env.uid
        for rx in range(sh.nrows):
            if rx == 0:
                continue
            data = [str(sh.cell(rx, i).value or '') for i in range(3)]
            if data[0] and data[1]:
                stock_picking = self.env['stock.picking'].search([('name', '=', data[0])])
                if data[1]:
                    dateFormat = xlwt.XFStyle()
                    dateFormat.num_format_str = 'yyyy-mm-dd'
                    time = datetime(*xldate_as_tuple(int(data[1][:-2]), 0))
                    stock_picking.write({'expected_arrival_date': time})
                    sql = """insert into change_order(
                                 create_uid,write_uid,
                                 order_name,date_time,
                                 origin,state
                                 ) VALUES 
                                 ('%s','%s','%s','%s','%s','%s')""" \
                          %(uid, uid,
                            stock_picking.name, stock_picking.expected_arrival_date,
                            stock_picking.origin, 'Y')
                    self.env.cr.execute(sql)
                else:
                    raise UserError("模板中没有更新时间！")
            else:
                raise UserError("模板中没有单号名称！")
        return {
            'type': 'ir.actions.act_window',
            'name': u'导入调拨单数据',
            'view_type': 'form',
            'view_mode': 'tree,pivot',
            'res_model': 'change.order',
            'target': 'current',
            'view_id': False,
        }

    # 下载延期的调拨单数据
    @api.multi
    def download_order(self):
        sql = """SELECT
                    spk. NAME,
                    spk.expected_arrival_date,
                    spk.origin
                FROM
                    stock_picking spk
                WHERE
                    spk.expected_arrival_date < now()
                AND spk.state = 'assigned'
                AND spk.origin LIKE 'FY%'"""
        self._cr.execute(sql)
        result = self._cr.fetchall()
        if not result:
            raise UserError("没有延期的调拨单据！")
        # 设置表头
        title = ['调拨单号', '预计到达日期',  '单号']
        # 将数据封装并转为json
        result = {'titles': title, 'filename': '延期调拨单数据.xlsx', 'command': sql}
        jsonstr = json.dumps(result).replace('=', '%3d').replace('+', '%2B')
        return {'type': 'ir.actions.act_url',
                'url': '/web/excel/download?para=%s' % jsonstr,
                'target': 'new'}




