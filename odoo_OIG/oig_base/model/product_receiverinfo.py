# -*- coding: utf-8 -*-
from odoo import fields, models


class  product_receiverinfo(models.Model):
    _name = 'product.receiverinfo'
    receive_product_code = fields.Char(u'收货人编码')
    receive_product_code2 = fields.Char(u'收货人编码2')
    product_template_id = fields.Many2one('product.template', u'产品')
    oig_receiver_id = fields.Many2one('oig.receiver', u'收货人')
    issued_state = fields.Selection([
        ('noIssued', u'未下发'),
        ('hasIssued', u'已下发'),
    ], string=u'下发状态')