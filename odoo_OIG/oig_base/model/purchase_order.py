# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
from datetime import datetime
import odoo.addons.decimal_precision as dp
class  purchase_order(models.Model):
    _inherit = 'purchase.order'
    pay_type = fields.Selection([
        ('public', u'公账'),
        ('private', u'私账'),
        ('net', u'网上拍'),
        ], string=u'付款类型')
    date_order = fields.Date(u'单据日期', required=True, index=True, copy=False, default=fields.Date.today)
    pay_way = fields.Selection([
        ('all', u'预付全款'),
        ('pay_pay_delivery', u'预付30 % 尾款款到发货'),
        ('pay_delivery_pay', u'预付30 % 尾款货到付款'),
        ('delivery_then_pay', u'货到付款'),
        ('pay_to_delivery', u'款到发货'),
        ('period_pay', u'账期'),
        ('ali_period_pay', u'阿里账期'),
        ], string=u'付款方式', related='partner_id.pay_way')
    responser = fields.Char(u'负责人')
    create_name = fields.Char(u'制单人')
    oig_receiver_id = fields.Many2one('oig.receiver', u'收货人')
    tax_no = fields.Char(u'金融单号')
    contracts = fields.Binary(u"合同")
    contracts_file = fields.Char(string="Contracts File")
    open_state = fields.Selection([
        ('open', u'打开'),
        ('close', u'关闭'),
        ], string=u'开关状态', default='open', track_visibility='onchange')
    website = fields.Char(related='partner_id.website', string=u"账户网址")
    logistics_name = fields.Char(string=u"物流名称")
    logistics_number = fields.Char(string=u'物流单号')
    issued_state = fields.Selection([
        ('Y', u'已下发'),
        ('N', u'未下发'),
    ], string=u'采购订单下发状态', default='N', track_visibility='onchange')
    purchase_count = fields.Integer(compute="_compute_purchase_count")

    is_buy = fields.Boolean(related='partner_id.is_buy', string="是否预付")

    # 获取对应的采购付款申请单的数量
    def _compute_purchase_count(self):
        dict_date = []
        account_invoice = self.env['account.invoice'].search([('purchase_order_id', '=', self.id)])
        for account_invoice_param in account_invoice:
            result = self.env['purchase.pay'].search([('id', '=', account_invoice_param.purchase_pay_id.id)])
            if result.id:
                dict_date.append(result.id)
        list2 = []
        for data in dict_date:
            if not data in list2:
                list2.append(data)
        if len(list2) == 1 and list2[0]:
            self.purchase_count = len(list2)
        elif len(list2) == 1 and list2[0] == False:
            self.purchase_count = 0
        else:
            self.purchase_count = len(list2)
    # @api.multi
    def purchase_order_view(self):

        action = self.env.ref('purchase_pay.view_purchase_pay_tree')
        result = action.read()[0]

        dict_date = []
        account_invoice = self.env['account.invoice'].search([('purchase_order_id', '=', self.id)])
        for account_invoice_param in account_invoice:
            results = self.env['purchase.pay'].search([('id', '=', account_invoice_param.purchase_pay_id.id)])
            if results.id:
                dict_date.append(results.id)
        list2 = []
        for data in dict_date:
            if not data in list2:
                list2.append(data)

        pick_ids = list2
        result['context'] = {}
        if len(pick_ids) > 1:
            result['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
            return{
                'name': _('Purchase Pay'),
                'domain': "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]",
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'purchase.pay',
                'type': 'ir.actions.act_window',
                'view_id': False,
                'target': 'current',
                'context': self.env.context,
            }
        elif len(pick_ids) == 1 and pick_ids[0]:
            return{
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.pay',
                'type': 'ir.actions.act_window',
                'res_id': pick_ids[0],
                'context': self.env.context,
            }
        elif len(pick_ids) == 1 and pick_ids[0] == False:
            return True


    @api.multi
    def button_cancel(self):
        res = super(purchase_order, self).button_cancel()
        return True

class  purchase_order_line(models.Model):
    _inherit = 'purchase.order.line'

    @api.depends('invoice_lines.quantity')
    def _compute_qty_invoiced(self):
        for line in self:
            qty = 0.0
            for inv_line in line.invoice_lines:
                if inv_line.invoice_id.state not in ['cancel']:
                    qty += inv_line.uom_id._compute_quantity(inv_line.quantity, line.product_uom)
            line.qty_invoiced = qty

    @api.depends('invoice_lines.invoice_id.state')
    def _compute_qty_paid(self):
        for line in self:
            qty = 0.0
            for inv_line in line.invoice_lines:
                if inv_line.invoice_id.state in ['paid']:
                    if inv_line.invoice_id.type == 'in_invoice':
                        qty += inv_line.uom_id._compute_quantity(inv_line.quantity, line.product_uom)
                    else:
                        qty -= inv_line.uom_id._compute_quantity(inv_line.quantity, line.product_uom)
            line.qty_paid = qty

    @api.multi
    def show_details(self):
        # TDE FIXME: does not seem to be used
        view_id = self.env.ref('oig_base.view_purchase_order_line_form_add').id
        return {
            'name': ('小组库存'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.order.line',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'res_id': self.ids[0],
            'context': self.env.context}
    qty_paid = fields.Float(compute='_compute_qty_paid', digits=dp.get_precision('Payment Terms'), string="已付款金额", store=True)
    qty_invoiced = fields.Float(compute='_compute_qty_invoiced', string="Billed Qty", store=True, digits=dp.get_precision('Payment Terms'), track_visibility='onchange')
    qty_received = fields.Float(compute='_compute_qty_received', string="Received Qty", store=True, digits=dp.get_precision('Product Unit of Measure'), track_visibility='onchange')
    purchase_date_planned = fields.Date(u'采购预计到货日期')
    open_state = fields.Selection([
        ('open', u'打开'),
        ('close', u'关闭'),
        ], string=u'打开状态',default='open')
    platform_line = fields.One2many('line.platform', 'purchase_order_line_id', '小组明细')
    packing_instruction = fields.Text(u"包装要求", )
    quality_inspection = fields.Text(u"质检要求", )
    remark = fields.Text(u"备注",)
    purchase_interid = fields.Char(u'申请单号')
    date_planned = fields.Date(u'预定交货日期')
    date_order = fields.Date(related='order_id.date_order', string='单据日期', readonly=True)
    is_sample = fields.Selection([
        ('Y', u'是'),
        ('N', u'否'),
    ], string=u'是否提样')
    reason = fields.Char(u'修改到货日期原因')
    partner_id = fields.Many2one('res.partner', string='供应商')
    responser = fields.Char(u'负责人', related="order_id.responser")
    qty_invoiced = fields.Float(compute='_compute_qty_invoiced', string="Billed Qty", store=True)
    
    @api.multi
    def write(self, vals):
        res = super(purchase_order_line, self).write(vals)
        if len(self) > 0:
            for ea in self:
                if ea.purchase_date_planned < ea.date_planned:
                    raise UserError("采购预计到货日期不能大于预定交货日期！")
        return res


