# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime, timedelta
import logging
import json
_logger = logging.getLogger(__name__)

class delivery_tracking_report(models.TransientModel):
    _name = 'delivery.tracking.report'
    _description = u'发运单全程跟踪报表'

    delivery_no = fields.Char(string=u'发运单号')
    has_to_putaway_qty = fields.Boolean(string=u'是否有待上架数量', default=True)
    has_return_qty = fields.Boolean(string = u'是否有退回数量')

    @api.multi
    def report_print(self):
        # 获取发运单号
        delivery_no = self.delivery_no
        # 对发运单号进行处理
        str_delivery_nos = ''
        if delivery_no:
            if (',') in delivery_no:
                delivery_no_list = delivery_no.split(',')
                for str_delivery_no in delivery_no_list:
                    if not str_delivery_nos:
                        str_delivery_nos = """'%s'""" % str_delivery_no
                    else:
                        str_delivery_nos += """,'%s'""" % str_delivery_no

            else:
                str_delivery_nos = """'%s'""" % delivery_no
        else:
            str_delivery_nos = 'select name from stock_picking'

        # 判断是否查询待上架产品（待上架数量大于零）
        if self.has_to_putaway_qty:
            str_has_to_putaway_qty = 'and sum(d.product_uom_qty) - COALESCE(res . NUMBER,0) - COALESCE(res_a.done_qty,0)>0'
        else:
            str_has_to_putaway_qty = 'and 1=1'

        # 判断是否查询退回产品（退回数量大于零）
        if self.has_return_qty:
            str_has_return_qty = 'and COALESCE(res . NUMBER,0)>0'
        else:
            str_has_return_qty = 'and 1=1'


        try:
            # 清除暂存数据的表
            sql = 'delete from delivery_tracking_report_result where create_uid = ' + str(self.env.uid)
            self.env.cr.execute(sql)

            now_time = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
            uid = self.env.uid
            # 向暂存数据表中插入查询结果
            sql = """with res as (
                        select a.stock_picking_id,b.product,sum(b.number) as number from return_form a 
                        INNER JOIN return_form_line b on b.return_form_id = a.id
                        GROUP BY a.stock_picking_id,b.product
                        ),
                        res_a as (
                        select a.origin,c.product_id,sum(c.product_uom_qty) as done_qty from stock_picking a 
                        INNER JOIN stock_picking_type b on b.id = a.picking_type_id and b.name = '移仓入库内部调拨'
                        INNER JOIN stock_move c on c.picking_id = a.id
                        where a.state = 'done'
                        GROUP BY a.origin,c.product_id
                        )
                        INSERT INTO delivery_tracking_report_result (delivery_no,receiver,sku,order_qty,
                        return_qty,putaway_qty,to_putaway_qty,create_uid,write_uid,create_date,write_date) 
                        SELECT
                            A . NAME as 发运单号,
                            C . NAME as 收货人,
                            e.default_code as 产品,
                            sum(d.product_uom_qty) as 订单数量,
                            COALESCE(res . NUMBER,0) as 退回数量,
                            COALESCE(res_a.done_qty,0) as 目的仓已上架数量,
                            sum(d.product_uom_qty) - COALESCE(res . NUMBER,0) - COALESCE(res_a.done_qty,0) as 目的仓待上架数量,
                            '%s', '%s','%s','%s'
                        FROM
                            stock_picking A
                        INNER JOIN stock_picking_type b ON b. ID = A .picking_type_id
                        AND b. NAME = '发运内部调拨'
                        INNER JOIN oig_receiver C ON C .stock_location_id = A .location_id
                        INNER JOIN stock_move d ON d.picking_id = A . ID
                        INNER JOIN product_product e ON e. ID = d.product_id 
                        LEFT JOIN res on res.stock_picking_id = a.id and res.product = e.id
                        LEFT JOIN res_a on res_a.origin = a.name and res_a.product_id = e.id
                        WHERE a.name in (%s) GROUP BY A.NAME,C . NAME ,e.default_code,
                        res.number,res_a.done_qty HAVING 1=1 %s %s""" \
                        %(uid,uid,now_time,now_time,str_delivery_nos,str_has_to_putaway_qty,str_has_return_qty)
            self.env.cr.execute(sql)
        except Exception, ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql))
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': u'发运单全程跟踪报表',
                'view_type': 'form',
                'view_mode': 'tree,pivot',
                'res_model': 'delivery.tracking.report.result',
                'target': 'current',
                'view_id': False,
                'domain': [('create_uid', '=', self.env.uid)],
            }

        return False

    @api.multi
    def down_load(self):
        # 获取发运单号
        delivery_no = self.delivery_no
        # 对发运单号进行处理
        str_delivery_nos = ''
        if delivery_no:
            if (',') in delivery_no:
                delivery_no_list = delivery_no.split(',')
                for str_delivery_no in delivery_no_list:
                    if not str_delivery_nos:
                        str_delivery_nos = """'%s'""" % str_delivery_no
                    else:
                        str_delivery_nos += """,'%s'""" % str_delivery_no

            else:
                str_delivery_nos = """'%s'""" % delivery_no
        else:
            str_delivery_nos = 'select name from stock_picking'

        # 判断是否查询待上架产品（待上架数量大于零）
        if self.has_to_putaway_qty:
            str_has_to_putaway_qty = ' and sum(d.product_uom_qty) - COALESCE(res . NUMBER,0) - COALESCE(res_a.done_qty,0)>0'
        else:
            str_has_to_putaway_qty = 'and 1=1'

        # 判断是否查询退回产品（退回数量大于零）
        if self.has_return_qty:
            str_has_return_qty = 'and COALESCE(res . NUMBER,0)>0'
        else:
            str_has_return_qty = 'and 1=1'

        try:
            # 向暂存数据表中插入查询结果
            sql = """with res as (
                        select a.stock_picking_id,b.product,sum(b.number) as number from return_form a 
                        INNER JOIN return_form_line b on b.return_form_id = a.id
                        GROUP BY a.stock_picking_id,b.product
                        ),
                        res_a as (
                        select a.origin,c.product_id,sum(c.product_uom_qty) as done_qty from stock_picking a 
                        INNER JOIN stock_picking_type b on b.id = a.picking_type_id and b.name = '移仓入库内部调拨'
                        INNER JOIN stock_move c on c.picking_id = a.id
                        where a.state = 'done'
                        GROUP BY a.origin,c.product_id
                        )
                        SELECT
                            A . NAME as 发运单号,
                            C . NAME as 收货人,
                            e.default_code as 产品,
                            sum(d.product_uom_qty) as 订单数量,
                            COALESCE(res . NUMBER,0) as 退回数量,
                            COALESCE(res_a.done_qty,0) as 目的仓已上架数量,
                            sum(d.product_uom_qty) - COALESCE(res . NUMBER,0) - COALESCE(res_a.done_qty,0) as 目的仓待上架数量
                        FROM
                            stock_picking A
                        INNER JOIN stock_picking_type b ON b. ID = A .picking_type_id
                        AND b. NAME = '发运内部调拨'
                        INNER JOIN oig_receiver C ON C .stock_location_id = A .location_id
                        INNER JOIN stock_move d ON d.picking_id = A . ID
                        INNER JOIN product_product e ON e. ID = d.product_id 
                        LEFT JOIN res on res.stock_picking_id = a.id and res.product = e.id
                        LEFT JOIN res_a on res_a.origin = a.name and res_a.product_id = e.id
                        WHERE a.name in (%s) GROUP BY A.NAME,C . NAME ,e.default_code,
                        res.number,res_a.done_qty HAVING 1=1 %s %s""" %(str_delivery_nos,str_has_to_putaway_qty,str_has_return_qty)
            # 替换sql中的特殊字符
            sql = sql.replace('=', '%3d').replace('+', '%2B')
            # 设置表头
            title = ['发运单号', '收货人', '产品', '订单数量', '退回数量', '目的仓已上架数量', '目的仓待上架数量']
            # 将数据封装并转为json
            result = {'titles': title, 'filename': '发运单全程跟踪报表.xlsx', 'command': sql}
            jsonstr = json.dumps(result)
        except Exception, ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql))
        else:
            return {'type': 'ir.actions.act_url',
                    'url': '/web/excel/download?para=%s' % jsonstr,
                    'target': 'new'}