# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)

class delivery_tracking_report_result(models.Model):
    _name = 'delivery.tracking.report.result'
    _description = u'发运单全程跟踪报表结果'
    _order = 'delivery_no asc'

    delivery_no = fields.Char(u'发运单号')
    receiver = fields.Char(u'收货人')
    sku = fields.Char(u'产品编码')
    order_qty = fields.Integer(u'订单数量')
    return_qty = fields.Integer(u'退回数量')
    putaway_qty = fields.Integer(u'目的仓已上架数量')
    to_putaway_qty = fields.Integer(u'目的仓待上架数量')



class sales_storehouse_report_result(models.Model):
    _name = 'sales.storehouse.report.result'
    _description = u'销售出库报表结果'
    _order = 'sales_no asc'

    sales_no = fields.Char(u'出库单号')
    stock_location_name = fields.Char(u'仓库名称')
    sku = fields.Char(u'产品编码')
    product_qty = fields.Integer(u'产品数量')
    cost_price = fields.Float(u'成本价')
    amount = fields.Float(u'金额')
