# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import datetime
import logging
import json
_logger = logging.getLogger(__name__)
class subject_balance_report(models.TransientModel):
    _name = 'subject.balance.report'
    _description = u'科目余额报表'

    begin_time = fields.Date(string=u'开始时间')
    end_time = fields.Date(string=u'结束时间')
    code = fields.Char(string=u'科目代码')
    is_used = fields.Boolean(u'是否启用辅助科目', store=True)

    @api.multi
    def report_print(self):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        if self.begin_time and self.end_time:
            if self.is_used == True:
                sql = """SELECT
                             tttt. NAME,	tttt.code,
                             CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                             CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                             SUM (tttt.now_credit) AS now_credit,
                             SUM (tttt.now_debit) AS now_debit,
                             SUM (tttt.nian_debit) AS nian_debit,
                             SUM (tttt.nian_credit) AS nian_credit,
                             CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                             CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                             SUM (tttt.amount_currency) AS amount_currency,
                             SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency,
                             COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                             SUM (tttt.bq_amount_currency) AS bq_amount_currency,
                             tttt.partner_name
                            FROM
                                (
                                    WITH res AS (
                                        SELECT
                                            A . NAME,A .code,rp. NAME AS partner_name,
                                            SUM (C .amount_currency) AS amount_currency,
                                            rc. NAME AS currency_name,
                                            SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                            0 AS fs_debit,0 AS fs_credit,
                                            0 AS bn_debit,0 AS bn_credit,
                                            0 AS bn_amount_currency,0 AS bq_amount_currency
                                        FROM
                                            account_account A
                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                        INNER JOIN res_partner rp ON rp. ID = C .partner_id
                                        WHERE
                                            d. DATE < '%s'
                                        GROUP BY
                                            A .code,A . NAME,rc. NAME,rp. NAME
                                        UNION ALL
                                            SELECT
                                                A . NAME, A .code,rp.NAME AS partner_name,
                                                0 AS amount_currency,
                                                rc. NAME AS currency_name,
                                                0 AS qc_debit,0 AS qc_credit,
                                                SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                0 AS bn_debit,0 AS bn_credit,
                                                0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                            FROM
                                                account_account A
                                            LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                            LEFT JOIN account_move_line C ON C .account_id = b. ID
                                            LEFT JOIN account_move d ON d. ID = C .move_id
                                            LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                            INNER JOIN res_partner rp ON rp. ID = C .partner_id
                                            WHERE
                                                d. DATE <= '%s'
                                            AND d. DATE >= '%s'
                                            GROUP BY
                                                A .code,A . NAME,rc. NAME,rp. NAME
                                            UNION ALL
                                                SELECT
                                                    A . NAME,A .code,rp.NAME AS partner_name,
                                                    0 AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    0 AS qc_debit,0 AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                    SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                INNER JOIN res_partner rp ON rp. ID = C .partner_id
                                                WHERE
                                                    d. DATE <= '%s'
                                                AND d. DATE >= '%s'
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME,rp. NAME
                                    ) SELECT
                                        NAME,code,partner_name,
                                        currency_name,
                                        SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                        SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                        SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                        SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                        SUM (amount_currency) AS amount_currency,
                                        SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                    FROM
                                        res
                                    GROUP BY
                                        code,NAME,currency_name,partner_name
                                    ORDER BY
                                        code
                                ) AS tttt
                            GROUP BY
                                NAME,code,COALESCE (tttt.currency_name, 'CNY'),partner_name
                            ORDER BY
                                code"""% (
                                                self.begin_time+' 23:59:59',
                                                self.end_time+' 23:59:59',
                                                self.begin_time+' 00:00:00',
                                                self.end_time+' 23:59:59',
                                                datetime.datetime.strptime('2018-01-01', '%Y-%m-%d'))
            else:
                sql = """ SELECT
                             tttt. NAME,	tttt.code,
                             CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                             CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                             SUM (tttt.now_credit) AS now_credit,
                             SUM (tttt.now_debit) AS now_debit,
                             SUM (tttt.nian_debit) AS nian_debit,
                             SUM (tttt.nian_credit) AS nian_credit,
                             CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                             CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                             SUM (tttt.amount_currency) AS amount_currency,
                             SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency,
                             COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                             SUM (tttt.bq_amount_currency) AS bq_amount_currency
                            FROM
                                (
                                    WITH res AS (
                                        SELECT
                                            A . NAME,A .code,
                                            SUM (C .amount_currency) AS amount_currency,
                                            rc. NAME AS currency_name,
                                            SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                            0 AS fs_debit,0 AS fs_credit,
                                            0 AS bn_debit,0 AS bn_credit,
                                            0 AS bn_amount_currency,0 AS bq_amount_currency
                                        FROM
                                            account_account A
                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                        WHERE
                                            d. DATE < '%s'
                                        GROUP BY
                                            A .code,A . NAME,rc. NAME
                                        UNION ALL
                                            SELECT
                                                A . NAME, A .code,
                                                0 AS amount_currency,
                                                rc. NAME AS currency_name,
                                                0 AS qc_debit,0 AS qc_credit,
                                                SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                0 AS bn_debit,0 AS bn_credit,
                                                0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                            FROM
                                                account_account A
                                            LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                            LEFT JOIN account_move_line C ON C .account_id = b. ID
                                            LEFT JOIN account_move d ON d. ID = C .move_id
                                            LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                            WHERE
                                                d. DATE <= '%s'
                                            AND d. DATE >= '%s'
                                            GROUP BY
                                                A .code,A . NAME,rc. NAME
                                            UNION ALL
                                                SELECT
                                                    A . NAME,A .code,
                                                    0 AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    0 AS qc_debit,0 AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                    SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                WHERE
                                                    d. DATE <= '%s'
                                                AND d. DATE >= '%s'
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME
                                    ) SELECT
                                        NAME,code,
                                        currency_name,
                                        SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                        SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                        SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                        SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                        SUM (amount_currency) AS amount_currency,
                                        SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                    FROM
                                        res
                                    GROUP BY
                                        code,NAME,currency_name
                                    ORDER BY
                                        code
                                ) AS tttt
                            GROUP BY
                                NAME,code,COALESCE (tttt.currency_name, 'CNY')
                            ORDER BY
                                code
                            """ % (
                            self.begin_time+' 23:59:59',
                            self.end_time+' 23:59:59',
                            self.begin_time+' 00:00:00',
                            self.end_time+' 23:59:59',
                            datetime.datetime.strptime('2018-01-01', '%Y-%m-%d'))
        else:
            if self.is_used == True:
                sql = """SELECT
                             tttt. NAME,	tttt.code,
                             CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                             CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                             SUM (tttt.now_credit) AS now_credit,
                             SUM (tttt.now_debit) AS now_debit,
                             SUM (tttt.nian_debit) AS nian_debit,
                             SUM (tttt.nian_credit) AS nian_credit,
                             CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                             CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                             SUM (tttt.amount_currency) AS amount_currency,
                             SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency,
                             COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                             SUM (tttt.bq_amount_currency) AS bq_amount_currency,
                             tttt.partner_name
                            FROM
                                (
                                    WITH res AS (
                                        SELECT
                                            A . NAME,A .code,rp. NAME AS partner_name,
                                            SUM (C .amount_currency) AS amount_currency,
                                            rc. NAME AS currency_name,
                                            SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                            0 AS fs_debit,0 AS fs_credit,
                                            0 AS bn_debit,0 AS bn_credit,
                                            0 AS bn_amount_currency,0 AS bq_amount_currency
                                        FROM
                                            account_account A
                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                        INNER JOIN res_partner rp ON rp. ID = C .partner_id                                        
                                        GROUP BY
                                            A .code,A . NAME,rc. NAME,rp. NAME
                                        UNION ALL
                                            SELECT
                                                A . NAME, A .code,rp.NAME AS partner_name,
                                                0 AS amount_currency,
                                                rc. NAME AS currency_name,
                                                0 AS qc_debit,0 AS qc_credit,
                                                SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                0 AS bn_debit,0 AS bn_credit,
                                                0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                            FROM
                                                account_account A
                                            LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                            LEFT JOIN account_move_line C ON C .account_id = b. ID
                                            LEFT JOIN account_move d ON d. ID = C .move_id
                                            LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                            INNER JOIN res_partner rp ON rp. ID = C .partner_id                                           
                                            GROUP BY
                                                A .code,A . NAME,rc. NAME,rp. NAME
                                            UNION ALL
                                                SELECT
                                                    A . NAME,A .code,rp.NAME AS partner_name,
                                                    0 AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    0 AS qc_debit,0 AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                    SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                INNER JOIN res_partner rp ON rp. ID = C .partner_id                                                
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME,rp. NAME
                                    ) SELECT
                                        NAME,code,partner_name,
                                        currency_name,
                                        SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                        SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                        SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                        SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                        SUM (amount_currency) AS amount_currency,
                                        SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                    FROM
                                        res
                                    GROUP BY
                                        code,NAME,currency_name,partner_name
                                    ORDER BY
                                        code
                                ) AS tttt
                            GROUP BY
                                NAME,code,COALESCE (tttt.currency_name, 'CNY'),partner_name
                            ORDER BY
                                code"""
            else:
                sql = """ SELECT
                             tttt. NAME,	tttt.code,
                             CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                             CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                             SUM (tttt.now_credit) AS now_credit,
                             SUM (tttt.now_debit) AS now_debit,
                             SUM (tttt.nian_debit) AS nian_debit,
                             SUM (tttt.nian_credit) AS nian_credit,
                             CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                             CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                             SUM (tttt.amount_currency) AS amount_currency,
                             SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency,
                             COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                             SUM (tttt.bq_amount_currency) AS bq_amount_currency
                            FROM
                                (
                                    WITH res AS (
                                        SELECT
                                            A . NAME,A .code,
                                            SUM (C .amount_currency) AS amount_currency,
                                            rc. NAME AS currency_name,
                                            SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                            0 AS fs_debit,0 AS fs_credit,
                                            0 AS bn_debit,0 AS bn_credit,
                                            0 AS bn_amount_currency,0 AS bq_amount_currency
                                        FROM
                                            account_account A
                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id                                        
                                        GROUP BY
                                            A .code,A . NAME,rc. NAME
                                        UNION ALL
                                            SELECT
                                                A . NAME, A .code,
                                                0 AS amount_currency,
                                                rc. NAME AS currency_name,
                                                0 AS qc_debit,0 AS qc_credit,
                                                SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                0 AS bn_debit,0 AS bn_credit,
                                                0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                            FROM
                                                account_account A
                                            LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                            LEFT JOIN account_move_line C ON C .account_id = b. ID
                                            LEFT JOIN account_move d ON d. ID = C .move_id
                                            LEFT JOIN res_currency rc ON rc. ID = C .currency_id                                            
                                            GROUP BY
                                                A .code,A . NAME,rc. NAME
                                            UNION ALL
                                                SELECT
                                                    A . NAME,A .code,
                                                    0 AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    0 AS qc_debit,0 AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                    SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id                                                
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME
                                    ) SELECT
                                        NAME,code,
                                        currency_name,
                                        SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                        SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                        SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                        SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                        SUM (amount_currency) AS amount_currency,
                                        SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                    FROM
                                        res
                                    GROUP BY
                                        code,NAME,currency_name
                                    ORDER BY
                                        code
                                ) AS tttt
                            GROUP BY
                                NAME,code,COALESCE (tttt.currency_name, 'CNY')
                            ORDER BY
                                code
                            """
        self._cr.execute(sql)
        fet = self._cr.fetchall()
        uid = self.env.uid

        sql = """delete from subject_balance where create_uid = %s"""%(self.env.uid)
        self.env.cr.execute(sql)

        if fet:
            for res in fet:
                currency_name = ''
                if res[12] == None:
                    currency_name = ' '
                else:
                    currency_name = res[12]
                if self.is_used == True:
                    sql = """insert into subject_balance(
                             create_uid,write_uid,create_date,write_date,
                             code,name,
                             begin_credit,begin_debit,
                             now_credit,now_debit,
                             end_credit,end_debit,
                             bn_debit,bn_credit,
                             currency_name,amount_currency,
                             end_amount_currency,
                             bq_amount_currency,
                             partner_name
                             ) VALUES 
                             ('%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,'%s',%s,%s,%s,'%s')""" \
                          %(uid, uid, now, now,
                            res[1], res[0],
                            res[2], res[3],
                            res[4], res[5],
                            res[8], res[9],
                            res[6], res[7],
                            currency_name, res[10],
                            res[11], res[13],
                            res[14]
                            )
                else:
                    sql = """insert into subject_balance(
                             create_uid,write_uid,create_date,write_date,
                             code,name,
                             begin_credit,begin_debit,
                             now_credit,now_debit,
                             end_credit,end_debit,
                             bn_debit,bn_credit,
                             currency_name,amount_currency,
                             end_amount_currency,
                             bq_amount_currency
                             ) VALUES 
                             ('%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,'%s',%s,%s,%s)""" \
                          %(uid, uid, now, now,
                            res[1], res[0],
                            res[2], res[3],
                            res[4], res[5],
                            res[8], res[9],
                            res[6], res[7],
                            currency_name, res[10],
                            res[11], res[13]
                            )
                self.env.cr.execute(sql)
        return {
            'type': 'ir.actions.act_window',
            'name': u'科目余额报表',
            'view_type': 'form',
            'view_mode': 'tree,pivot',
            'res_model': 'subject.balance',
            'target': 'current',
            'view_id': False,
            'domain': [('create_uid', '=', self.env.uid)],
        }

    # 科目余额表下载功能
    @api.multi
    def down_load(self):
        ctx = dict(self._context or {})
        ctx['patch'] = False
        # 将查询的时间转换为ODOO默认的伦敦时间
        start_date = self.begin_time
        end_date = self.end_time
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        try:
            if self.begin_time and self.end_time:
                if self.is_used == True:
                    sql = """SELECT
                             tttt. NAME,tttt.partner_name,tttt.code,
                             CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                             CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                             SUM (tttt.now_debit) AS now_debit,
                             SUM (tttt.now_credit) AS now_credit,
                             SUM (tttt.nian_debit) AS nian_debit,
                             SUM (tttt.nian_credit) AS nian_credit,
                             CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                             CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                             COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                             SUM (tttt.amount_currency) AS amount_currency,
                             SUM (tttt.bq_amount_currency) AS bq_amount_currency,
                             SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency
                            FROM
                                (
                                    WITH res AS (
                                        SELECT
                                            A . NAME,A .code,rp. NAME AS partner_name,
                                            SUM (C .amount_currency) AS amount_currency,
                                            rc. NAME AS currency_name,
                                            SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                            0 AS fs_debit,0 AS fs_credit,
                                            0 AS bn_debit,0 AS bn_credit,
                                            0 AS bn_amount_currency,0 AS bq_amount_currency
                                        FROM
                                            account_account A
                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                        INNER JOIN res_partner rp ON rp. ID = C .partner_id
                                        WHERE
                                            d. DATE < '%s'
                                        GROUP BY
                                            A .code,A . NAME,rc. NAME,rp. NAME
                                        UNION ALL
                                            SELECT
                                                A . NAME, A .code,rp.NAME AS partner_name,
                                                0 AS amount_currency,
                                                rc. NAME AS currency_name,
                                                0 AS qc_debit,0 AS qc_credit,
                                                SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                0 AS bn_debit,0 AS bn_credit,
                                                0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                            FROM
                                                account_account A
                                            LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                            LEFT JOIN account_move_line C ON C .account_id = b. ID
                                            LEFT JOIN account_move d ON d. ID = C .move_id
                                            LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                            INNER JOIN res_partner rp ON rp. ID = C .partner_id
                                            WHERE
                                                d. DATE <= '%s'
                                            AND d. DATE >= '%s'
                                            GROUP BY
                                                A .code,A . NAME,rc. NAME,rp. NAME
                                            UNION ALL
                                                SELECT
                                                    A . NAME,A .code,rp.NAME AS partner_name,
                                                    0 AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    0 AS qc_debit,0 AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                    SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                INNER JOIN res_partner rp ON rp. ID = C .partner_id
                                                WHERE
                                                    d. DATE <= '%s'
                                                AND d. DATE >= '%s'
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME,rp. NAME
                                    ) SELECT
                                        NAME,code,partner_name,
                                        currency_name,
                                        SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                        SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                        SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                        SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                        SUM (amount_currency) AS amount_currency,
                                        SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                    FROM
                                        res
                                    GROUP BY
                                        code,NAME,currency_name,partner_name
                                    ORDER BY
                                        code
                                ) AS tttt
                            GROUP BY
                                NAME,code,COALESCE (tttt.currency_name, 'CNY'),partner_name
                            ORDER BY
                                code"""% (
                        self.begin_time+' 23:59:59',
                        self.end_time+' 23:59:59',
                        self.begin_time+' 00:00:00',
                        self.end_time+' 23:59:59',
                        datetime.datetime.strptime('2018-01-01', '%Y-%m-%d'))
                else:
                    sql = """ SELECT
                             tttt. NAME,	tttt.code,
                             CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                             CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                             SUM (tttt.now_debit) AS now_debit,
                             SUM (tttt.now_credit) AS now_credit,
                             SUM (tttt.nian_debit) AS nian_debit,
                             SUM (tttt.nian_credit) AS nian_credit,
                             CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                             CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                             COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                             SUM (tttt.amount_currency) AS amount_currency,
                             SUM (tttt.bq_amount_currency) AS bq_amount_currency,
                             SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency
                            FROM
                                (
                                    WITH res AS (
                                        SELECT
                                            A . NAME,A .code,
                                            SUM (C .amount_currency) AS amount_currency,
                                            rc. NAME AS currency_name,
                                            SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                            0 AS fs_debit,0 AS fs_credit,
                                            0 AS bn_debit,0 AS bn_credit,
                                            0 AS bn_amount_currency,0 AS bq_amount_currency
                                        FROM
                                            account_account A
                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                        WHERE
                                            d. DATE < '%s'
                                        GROUP BY
                                            A .code,A . NAME,rc. NAME
                                        UNION ALL
                                            SELECT
                                                A . NAME, A .code,
                                                0 AS amount_currency,
                                                rc. NAME AS currency_name,
                                                0 AS qc_debit,0 AS qc_credit,
                                                SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                0 AS bn_debit,0 AS bn_credit,
                                                0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                            FROM
                                                account_account A
                                            LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                            LEFT JOIN account_move_line C ON C .account_id = b. ID
                                            LEFT JOIN account_move d ON d. ID = C .move_id
                                            LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                            WHERE
                                                d. DATE <= '%s'
                                            AND d. DATE >= '%s'
                                            GROUP BY
                                                A .code,A . NAME,rc. NAME
                                            UNION ALL
                                                SELECT
                                                    A . NAME,A .code,
                                                    0 AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    0 AS qc_debit,0 AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                    SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                WHERE
                                                    d. DATE <= '%s'
                                                AND d. DATE >= '%s'
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME
                                    ) SELECT
                                        NAME,code,
                                        currency_name,
                                        SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                        SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                        SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                        SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                        SUM (amount_currency) AS amount_currency,
                                        SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                    FROM
                                        res
                                    GROUP BY
                                        code,NAME,currency_name
                                    ORDER BY
                                        code
                                ) AS tttt
                            GROUP BY
                                NAME,code,COALESCE (tttt.currency_name, 'CNY')
                            ORDER BY
                                code
                            """ % (
                        self.begin_time+' 23:59:59',
                        self.end_time+' 23:59:59',
                        self.begin_time+' 00:00:00',
                        self.end_time+' 23:59:59',
                        datetime.datetime.strptime('2018-01-01', '%Y-%m-%d'))
            else:
                if self.is_used == True:
                    sql = """SELECT
                                     tttt. NAME,tttt.partner_name,tttt.code,
                                     CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                                     CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                                     SUM (tttt.now_debit) AS now_debit,
                                     SUM (tttt.now_credit) AS now_credit,
                                     SUM (tttt.nian_debit) AS nian_debit,
                                     SUM (tttt.nian_credit) AS nian_credit,
                                     CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                                     CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                                     COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                                     SUM (tttt.amount_currency) AS amount_currency,
                                     SUM (tttt.bq_amount_currency) AS bq_amount_currency,
                                     SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency
                                    FROM
                                        (
                                            WITH res AS (
                                                SELECT
                                                    A . NAME,A .code,rp. NAME AS partner_name,
                                                    SUM (C .amount_currency) AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    0 AS bn_debit,0 AS bn_credit,
                                                    0 AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                INNER JOIN res_partner rp ON rp. ID = C .partner_id                                        
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME,rp. NAME
                                                UNION ALL
                                                    SELECT
                                                        A . NAME, A .code,rp.NAME AS partner_name,
                                                        0 AS amount_currency,
                                                        rc. NAME AS currency_name,
                                                        0 AS qc_debit,0 AS qc_credit,
                                                        SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                        0 AS bn_debit,0 AS bn_credit,
                                                        0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                                    FROM
                                                        account_account A
                                                    LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                    LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                    LEFT JOIN account_move d ON d. ID = C .move_id
                                                    LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                    INNER JOIN res_partner rp ON rp. ID = C .partner_id                                           
                                                    GROUP BY
                                                        A .code,A . NAME,rc. NAME,rp. NAME
                                                    UNION ALL
                                                        SELECT
                                                            A . NAME,A .code,rp.NAME AS partner_name,
                                                            0 AS amount_currency,
                                                            rc. NAME AS currency_name,
                                                            0 AS qc_debit,0 AS qc_credit,
                                                            0 AS fs_debit,0 AS fs_credit,
                                                            SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                            SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                        FROM
                                                            account_account A
                                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id
                                                        INNER JOIN res_partner rp ON rp. ID = C .partner_id                                                
                                                        GROUP BY
                                                            A .code,A . NAME,rc. NAME,rp. NAME
                                            ) SELECT
                                                NAME,code,partner_name,
                                                currency_name,
                                                SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                                SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                                SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                                SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                                SUM (amount_currency) AS amount_currency,
                                                SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                            FROM
                                                res
                                            GROUP BY
                                                code,NAME,currency_name,partner_name
                                            ORDER BY
                                                code
                                        ) AS tttt
                                    GROUP BY
                                        NAME,code,COALESCE (tttt.currency_name, 'CNY'),partner_name
                                    ORDER BY
                                        code"""
                else:
                    sql = """ SELECT
                                     tttt. NAME,tttt.code,
                                     CASE WHEN ((SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) > (0)) THEN (SUM (tttt.begin_debit) - SUM (tttt.begin_credit)) ELSE 0 END AS begin_debit,
                                     CASE WHEN (((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) > (0)) THEN((- SUM(tttt.begin_debit)) + SUM (tttt.begin_credit)) ELSE 0 END AS begin_credit,
                                     SUM (tttt.now_debit) AS now_debit,
                                     SUM (tttt.now_credit) AS now_credit,
                                     SUM (tttt.nian_debit) AS nian_debit,
                                     SUM (tttt.nian_credit) AS nian_credit,
                                     CASE WHEN ((SUM (tttt.end_debit) - SUM (tttt.end_credit)) > (0)) THEN (SUM (tttt.end_debit) - SUM (tttt.end_credit)) ELSE 0 END AS end_debit,
                                     CASE WHEN (((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) > (0)) THEN ((- SUM(tttt.end_debit)) + SUM (tttt.end_credit)) ELSE 0 END AS end_credit,
                                     COALESCE (tttt.currency_name, 'CNY') AS currency_name,
                                     SUM (tttt.amount_currency) AS amount_currency,
                                     SUM (tttt.bq_amount_currency) AS bq_amount_currency,
                                     SUM (tttt.amount_currency) + SUM (tttt.bq_amount_currency) AS bn_amount_currency
                                    FROM
                                        (
                                            WITH res AS (
                                                SELECT
                                                    A . NAME,A .code,
                                                    SUM (C .amount_currency) AS amount_currency,
                                                    rc. NAME AS currency_name,
                                                    SUM (debit) AS qc_debit,SUM (credit) AS qc_credit,
                                                    0 AS fs_debit,0 AS fs_credit,
                                                    0 AS bn_debit,0 AS bn_credit,
                                                    0 AS bn_amount_currency,0 AS bq_amount_currency
                                                FROM
                                                    account_account A
                                                LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                LEFT JOIN account_move d ON d. ID = C .move_id
                                                LEFT JOIN res_currency rc ON rc. ID = C .currency_id                                        
                                                GROUP BY
                                                    A .code,A . NAME,rc. NAME
                                                UNION ALL
                                                    SELECT
                                                        A . NAME, A .code,
                                                        0 AS amount_currency,
                                                        rc. NAME AS currency_name,
                                                        0 AS qc_debit,0 AS qc_credit,
                                                        SUM (debit) AS fs_debit,SUM (credit) AS fs_credit,
                                                        0 AS bn_debit,0 AS bn_credit,
                                                        0 AS bn_amount_currency,SUM (C .amount_currency) AS bq_amount_currency
                                                    FROM
                                                        account_account A
                                                    LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                    LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                    LEFT JOIN account_move d ON d. ID = C .move_id
                                                    LEFT JOIN res_currency rc ON rc. ID = C .currency_id                                            
                                                    GROUP BY
                                                        A .code,A . NAME,rc. NAME
                                                    UNION ALL
                                                        SELECT
                                                            A . NAME,A .code,
                                                            0 AS amount_currency,
                                                            rc. NAME AS currency_name,
                                                            0 AS qc_debit,0 AS qc_credit,
                                                            0 AS fs_debit,0 AS fs_credit,
                                                            SUM (debit) AS bn_debit,SUM (credit) AS bn_credit,
                                                            SUM (C .amount_currency) AS bn_amount_currency,0 AS bq_amount_currency
                                                        FROM
                                                            account_account A
                                                        LEFT JOIN account_account b ON b.code LIKE A .code || '%%'
                                                        LEFT JOIN account_move_line C ON C .account_id = b. ID
                                                        LEFT JOIN account_move d ON d. ID = C .move_id
                                                        LEFT JOIN res_currency rc ON rc. ID = C .currency_id                                                
                                                        GROUP BY
                                                            A .code,A . NAME,rc. NAME
                                            ) SELECT
                                                NAME,code,
                                                currency_name,
                                                SUM (qc_credit) begin_credit,SUM (qc_debit) begin_debit,
                                                SUM (fs_credit) now_credit,SUM (fs_debit) now_debit,
                                                SUM (qc_credit + fs_credit) AS end_credit,SUM (qc_debit + fs_debit) AS end_debit,
                                                SUM (bn_debit) AS nian_debit,SUM (bn_credit) AS nian_credit,
                                                SUM (amount_currency) AS amount_currency,
                                                SUM (bn_amount_currency) AS bn_amount_currency,SUM (bq_amount_currency) AS bq_amount_currency
                                            FROM
                                                res
                                            GROUP BY
                                                code,NAME,currency_name
                                            ORDER BY
                                                code
                                        ) AS tttt
                                    GROUP BY
                                        NAME,code,COALESCE (tttt.currency_name, 'CNY')
                                    ORDER BY
                                        code
                                    """
             # 替换sql中的特殊字符
            sql = sql.replace('=', '%3d').replace('+', '%2B')
            # 设置表头
            if self.is_used == True:
                title = ['科目名称', '辅助科目', '科目编码',  '期初借方', '期初贷方',  '本期借方', '本期贷方', '本年借方', '本年贷方', '期末借方', '期末贷方', '币种', '期初外币金额', '本期外币金额', '期末外币金额']
            else:
                title = ['科目名称', '科目编码',  '期初借方', '期初贷方',  '本期借方', '本期贷方', '本年借方', '本年贷方', '期末借方', '期末贷方', '币种', '期初外币金额', '本期外币金额', '期末外币金额']
            # 将数据封装并转为json
            result = {'titles': title, 'filename': '科目余额报表.xlsx', 'command': sql}
            jsonstr = json.dumps(result)
        except Exception, ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql_str))
        else:
            return {'type': 'ir.actions.act_url',
                    'url': '/web/excel/download?para=%s' % jsonstr,
                    'target': 'new'}