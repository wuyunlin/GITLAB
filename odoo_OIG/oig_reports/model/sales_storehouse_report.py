# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime, timedelta
import logging
import json
_logger = logging.getLogger(__name__)

class sales_storehouse_report(models.TransientModel):
    _name = 'sales.storehouse.report'
    _description = u'销售出库报表'

    sales_no = fields.Char(string=u'销售单号')

    @api.multi
    def report_print(self):
        # 获取销售单号
        sales_no = self.sales_no
        # 对销售单号进行处理
        str_sales_nos = ''
        if sales_no:
            if (',') in sales_no:
                sales_no_list = sales_no.split(',')
                for str_sales_no in sales_no_list:
                    if not str_sales_nos:
                        str_sales_nos = """'%s'""" % str_sales_no
                    else:
                        str_sales_nos += """,'%s'""" % str_sales_no

            else:
                str_sales_nos = """'%s'""" % sales_no
        else:
            str_sales_nos = 'select name from selling_out'

        try:
            # 清除暂存数据的表
            sql = 'delete from sales_storehouse_report_result where create_uid = ' + str(self.env.uid)
            self.env.cr.execute(sql)

            now_time = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
            uid = self.env.uid
            # 向暂存数据表中插入查询结果
            sql = """with res as (
                        select a.id,b.product_id,b.price_unit,b.product_qty from stock_picking a 
                        INNER JOIN stock_move b on b.picking_id = a.id
                        )
                        INSERT INTO sales_storehouse_report_result (sales_no,stock_location_name,sku,product_qty,
                        cost_price,amount,create_uid,write_uid,create_date,write_date) 
                        
                        select a.name AS 出库单号,c.name AS 仓库,d.default_code as 产品,b.qty as 数量,
                        res.price_unit as 成本价,(res.price_unit * res.product_qty) as 金额 ,
                        '%s', '%s','%s','%s'
                        from selling_out a
                        INNER JOIN selling_out_line2 b on b.selling_out_id = a.id
                        INNER JOIN stock_location c on a.target_storehouse = c.id
                        INNER JOIN product_product d on d.id = b.product
                        INNER JOIN res on res.id = a.sell_out_number and res.product_id = d.id
                        where a.state = 'done' and a.name in (%s)"""%(uid,uid,now_time,now_time,str_sales_nos)
            self.env.cr.execute(sql)
        except Exception, ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql))
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': u'销售出库报表',
                'view_type': 'form',
                'view_mode': 'tree,pivot',
                'res_model': 'sales.storehouse.report.result',
                'target': 'current',
                'view_id': False,
                'domain': [('create_uid', '=', self.env.uid)],
            }
        return False

    @api.multi
    def down_load(self):
        # 获取销售单号
        sales_no = self.sales_no
        # 对销售单号进行处理
        str_sales_nos = ''
        if sales_no:
            if (',') in sales_no:
                sales_no_list = sales_no.split(',')
                for str_sales_no in sales_no_list:
                    if not str_sales_nos:
                        str_sales_nos = """'%s'""" % str_sales_no
                    else:
                        str_sales_nos += """,'%s'""" % str_sales_no

            else:
                str_sales_nos = """'%s'""" % sales_no
        else:
            str_sales_nos = 'select name from selling_out'

        try:
            # 向暂存数据表中插入查询结果
            sql = """with res as (
                        select a.id,b.product_id,b.price_unit,b.product_qty from stock_picking a 
                        INNER JOIN stock_move b on b.picking_id = a.id
                        )
                        select a.name AS 出库单号,c.name AS 仓库,d.default_code as 产品,b.qty as 数量,
                        res.price_unit as 成本价,(res.price_unit * res.product_qty) as 金额 
                        from selling_out a
                        INNER JOIN selling_out_line2 b on b.selling_out_id = a.id
                        INNER JOIN stock_location c on a.target_storehouse = c.id
                        INNER JOIN product_product d on d.id = b.product
                        INNER JOIN res on res.id = a.sell_out_number and res.product_id = d.id
                        where a.state = 'done' and a.name in (%s)""" %(str_sales_nos)
            # 替换sql中的特殊字符
            sql = sql.replace('=', '%3d').replace('+', '%2B')
            # 设置表头
            title = ['出库单号', '仓库', '产品', '产品数量', '成本价', '金额']
            # 将数据封装并转为json
            result = {'titles': title, 'filename': '销售出库报表.xlsx', 'command': sql}
            jsonstr = json.dumps(result)
        except Exception, ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql))
        else:
            return {'type': 'ir.actions.act_url',
                    'url': '/web/excel/download?para=%s' % jsonstr,
                    'target': 'new'}