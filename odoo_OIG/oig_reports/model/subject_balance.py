# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)

class subject_balance(models.Model):
    _name = 'subject.balance'
    _description = u'科目余额表'

    code = fields.Char(u'科目代码')
    name = fields.Char(u'科目名称')

    begin_balance = fields.Float(u'期初金额')
    begin_credit = fields.Float(u'期初贷方')
    begin_debit = fields.Float(u'期初借方')

    now_balance = fields.Float(u'本期发生')
    now_credit = fields.Float(u'本期贷方')
    now_debit = fields.Float(u'本期借方')

    end_balance = fields.Float(u'期末金额')
    end_credit = fields.Float(u'期末贷方')
    end_debit = fields.Float(u'期末借方')

    bn_balance = fields.Float(u'本年累计')
    bn_credit = fields.Float(u'本年贷方')
    bn_debit = fields.Float(u'本年借方')

    currency_name = fields.Char(u'币种')
    amount_currency = fields.Float(u'期初外币金额')
    end_amount_currency = fields.Float(u'期末外币金额')
    bq_amount_currency = fields.Float(u'本期外币金额')
    partner_name = fields.Char(u'辅助科目')


