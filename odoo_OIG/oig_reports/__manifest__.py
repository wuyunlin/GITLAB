# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'oig_reports',
    'version': '1.2',
    'category': 'Purchases',
    'sequence': 60,
    'summary': 'oig report',
    'description': """
Manage goods requirement by Purchase Orders easily
==================================================
    """,
    'website': 'https://www.odoo.com/page/purchase',
    'data': [
        'views/delivery_tracking_report_view.xml',
        'views/sales_storehouse_report_view.xml',
        'views/report_result.xml',
        'security/ir.model.access.csv',
        'views/subject_balance_report_view.xml',
        'views/subject_balance.xml',
    ],
    'depends': ['oig_base'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
