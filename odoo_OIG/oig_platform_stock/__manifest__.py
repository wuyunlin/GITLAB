﻿{
    'name': '四衡小组库存改造',
    'version': '1.0',
    "category" : "Generic Modules/Test",
    'description': """
     """,
    'author': 'OIG',
    'depends': ['oig_base','stock_account','deltatech_stock_negative'],
    'init_xml': [ ],
     'qweb' : [  ],
    'data': [
        'security/ir.model.access.csv',
        'views/stock_quant_platform_views.xml',
        ],
    'installable': True,
    'active': False
}