# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _

class  stock_quant_platform(models.Model):
    _name = 'stock.quant.platform'
    _description = u'小组库存'
    #product_id = fields.Integer(u'产品ID')
    product_id = fields.Many2one('product.product', u'产品', related='stock_quant_id.product_id')
    #sys_platform_id = fields.Integer(u'小组ID')
    sys_platform_id = fields.Many2one('sys.platform', u'小组')
    #stock_location_id = fields.Integer(u'库位')
    stock_location_id = fields.Many2one('stock.location', u'库位', related='stock_quant_id.location_id')
    qty = fields.Integer(u'数量')
    old_qty = fields.Integer(u'源数量')
    origin_id = fields.Many2one('stock.quant', u'拆分出来的份')
    stock_quant_id = fields.Many2one('stock.quant', u'份')
