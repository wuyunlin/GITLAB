# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
import math
from odoo.exceptions import UserError
import logging, json, requests
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.addons import decimal_precision as dp
class  stock_quant(models.Model):
    _inherit = 'stock.quant'
    """方案1：调拨时候小组比例按照源仓库的比例调拨；这种情况下小组库存不能绑定在具体的quant上 因为可能会出现负数
            这种情况下小组库存只有总数概念
            这种情况下修改2个方法 1 _quant_create_from_move 
                                     创建quant的时候，获取比例，增加对应小组的数量
                                2 _quant_update_from_move quant
                                     发生调拨的时候，获取源仓库的比例 按照比例扣减源仓库的小组数量,增加目的仓库存"""

    """方案2：调拨时候小组比例按照源quant的比例调拨；这种情况下小组库存需要绑定在具体的quant上
             此时小组库存依附于quant;这就需要在quant拆分的时候 小组库存也做相应的拆分；
            这种情况下小组库存只有明细概念，总数用明细汇总来实现
            这种情况下修改2个方法 1 _quant_create_from_move 
                                     创建quant的时候，获取比例，增加对应小组的数量
                                     实现方式基本与方案1类似 只是此处需要创建小组明细库存而不是更新总数量
                                2  _quant_split
                                     发生拆分的时候，获取源quant的小组比例 按照比例扣减源quant的比例,给新生成的quant增加quant明细"""




    ########################方案2####################################
    ################更新与创建目的小组库存记录
    ##### 按照净量######销售出库时候 根据实际调拨的分配量与系统分配的量 算出差异 进行库存小组调拨 源与目的库位 均按如此进行调拨
    #######数量减少用负数表示
    def FA2_compute_platfrom_qty(self,qty_total, total_rateqty, cy, rate_list,purchase_flag,move=False):
        line_list=[]
        for each in rate_list:
            sys_platform_id,rate=each
            ####按比例计算出数量
            qty =   math.ceil(qty_total*float(rate)/total_rateqty)
            #######如果差异大于0 则每条明细向下调整1 直到差异为0
            if cy>0:
                qty=qty-1
                cy=cy-1
                if qty==0:
                    continue
            each_tuple=(sys_platform_id,qty)
            line_list.append(each_tuple)
            #####如果是采购入库 更新采购订单行上小组剩余数量
            if purchase_flag:
                platform_line=move.purchase_line_id.platform_line.filtered(lambda x:x.sys_platform_id.id==sys_platform_id)
                platform_line.qty=platform_line.qty-qty
        return line_list



    ##########方案2 新建newquant的小组明细
    def FA2_create_newquant_platform(self, line_list):
        line_create_list = []
        for line in line_list:
            data = line[:-1]
            if line[-1] < 0:
                raise UserError(('不能创建负数的小组库存记录'))
            line_create_list.append(tuple(list(line)+[ line[1],self.id]))
        if line_create_list:
            ########方案2 需要插入份 源数量
            sql = """insert into stock_quant_platform(sys_platform_id,qty,old_qty,stock_quant_id) values %s""" % (
                str(line_create_list)[1:-1])
            self._cr.execute(sql)
        return True
    #####################方案2 更新oldqunat的小组明细
    def FA2_oldquant_update_platform(self, line_list,newquant):
        sql = ''
        for line in line_list:
            sys_platform_id, qty = line
            sql += """update stock_quant_platform set qty=coalesce(qty,0)-%s,origin_id=%s
                      where stock_quant_id=%s and  sys_platform_id=%s;
                      delete from stock_quant_platform 
                      where stock_quant_id=%s and sys_platform_id=%s and qty=0;""" % (
                qty, newquant.id,self.id, sys_platform_id,self.id, sys_platform_id)
        if sql:
            self._cr.execute(sql)
        return True

    #############方案2的
    ######继承quant的生成方法，实现入库单据的小组库存 对应的业务单据有【采购入库】【盘盈入库】【其他入库】
    #######注意 销售退货入库单 是可以找到源quant的 找不到的按照其他入库处理"""
    @api.model
    def _quant_create_from_move(self, qty, move, lot_id=False, owner_id=False,
                                   src_package_id=False, dest_package_id=False,
                                   force_location_from=False, force_location_to=False):

        newquant = super(stock_quant, self)._quant_create_from_move(qty, move, lot_id=lot_id, owner_id=owner_id,
                                                                    src_package_id=src_package_id,
                                                                    dest_package_id=dest_package_id,
                                                                    force_location_from=force_location_from,
                                                                    force_location_to=force_location_to)

        #############根据move获取当前的单据类型 1 采购入库 2 其他类型入库
        #############获取各自的比例 1 采购订单来源于采购订单明细行  2其他类型入库 2.1 已有库存比例 2.2在售平台平均分配
        #######【生产入库】【盘盈入库】【其他入库】

        #######判断是否是采购入库
        def if_purchase():
            return move.location_id.usage == 'supplier'

        def get_rate_from_interface():
            if self.env['ir.config_parameter'].sudo().get_param('call_interface') == '1':
                import requests
                import json
                # 调用接口平台登录接口获取Authorization
                login_url = self.env['ir.config_parameter'].sudo().get_param('login_url')
                login_headers = {'Content-Type': 'application/json; charset=utf-8'}
                login_data = self.env['ir.config_parameter'].sudo().get_param('login_data')
                login_response = requests.post(login_url, data=login_data, headers=login_headers, verify=False)
                Authorization = login_response.headers._store['authorization'][1]
                url = self.env['ir.config_parameter'].sudo().get_param('group_url')
                headers = {}
                headers = {'Content-Type': 'application/json; charset=utf-8'}
                headers['Authorization'] = Authorization
                warehouse = ''
                if move.location_dest_id:
                    # 仓库类型为发货仓
                    if move.location_dest_id.ftype == 'shipping_warehouse':
                        if move.location_dest_id.origin_id != 0:
                            warehouse = move.location_dest_id.origin_id
                    # 仓库类型为收货人
                    elif move.location_dest_id.ftype == 'transfer_warehouse':
                        if move.location_dest_id.stock_location_id_shipping.origin_id != 0:
                            warehouse = move.location_dest_id.stock_location_id_shipping.origin_id
                    # 仓库类型为移动仓
                    elif move.location_dest_id.ftype == 'move_warehouse':
                        if move.location_dest_id.stock_location_id_shipping.origin_id != 0:
                            warehouse = move.location_dest_id.stock_location_id_shipping.origin_id
                    else:
                        warehouse
                # 暂时写法
                vals = {'sku': move.product_id.default_code, 'warehouseid': str(warehouse)}
                data = json.dumps(vals)
                response = requests.post(url, data=data, headers=headers, verify=False)
                result = []
                proportion = ''
                if requests.codes.ok == response.status_code or 201 == response.status_code:
                    content = json.loads(response.content)
                    if content['code'] == '200':
                        add_costs = content['data']
                        if len(add_costs) > 0:
                            for data in add_costs:
                                groups = data.get('platform') + "+" + data.get('site') + "+" + data.get('warehouse')
                                values = self.env['sys.platform'].search([('name', '=', groups)])
                                if vals:
                                    result.append((values.id, 1))
                                    proportion = result
                                else:
                                    raise UserError("没有找到对应的平台！")
                        else:
                            sql = """select id,1 from sys_platform"""
                            self._cr.execute(sql)
                            proportion = self._cr.fetchall()
                    else:
                        ########获取在售小组，比例1:1 先写死
                        sql = """select id,1 from sys_platform"""
                        self._cr.execute(sql)
                        proportion = self._cr.fetchall()
            else:
                sql = """select id,1 from sys_platform"""
                self._cr.execute(sql)
                proportion = self._cr.fetchall()
            return proportion

        def get_location_platform_stock(prod_location):
            product_id,location_id=prod_location
            ########y源库位比例
            sql = """select a.sys_platform_id,sum(a.qty) from stock_quant_platform a 
                     inner join stock_quant b on b.id=a.stock_quant_id
                     where b.product_id=%s and b.location_id=%s group by a.sys_platform_id"""%(product_id,location_id)
            self._cr.execute(sql)
            return self._cr.fetchall()

        #######获取比例
        def get_rate(flag):
            if flag == 'purchase':
                rate_list = [[line.sys_platform_id.id, line.qty] for line in move.purchase_line_id.platform_line]
            else:
                ###########生成quant 这里需要按照目的库位的小组比例
                rate_list = get_location_platform_stock([move.product_id.id, move.location_dest_id.id])
                if not rate_list:
                    #########获取在售小组比例均摊
                    rate_list = get_rate_from_interface()
            return rate_list

        #####强制可用功能禁止掉,本模块依赖于deltatech_stock_negative【禁止负库存模块】

        #######根据入库类型获取比例
        if if_purchase():
            if move.purchase_line_id:
                #########有对应的采购订单明细 按照其他入库处理
                rate_list = get_rate('purchase')
            else:
                #########没有对应的采购订单明细 按照其他入库处理
                rate_list = get_rate('other')
        else:
            rate_list = get_rate('other')

        ####按比例排序 处理尾差的时候用到【分摊尾差的时候 优先将扣减的数量分给数量多的小组 即 将数量少的小组优先结算完毕】
        rate_list = sorted(rate_list, key=lambda line: line[1], reverse=True)

        ########总数
        qty_total = int(newquant.qty)
        ####比例总数【不一定是数量】
        total_rateqty = sum(line[1] for line in rate_list)
        ####【预测数量】 按照比例向上取整后的总数量
        qty_forecast = sum(math.ceil(qty_total * float(line[1]) / total_rateqty) for line in rate_list)
        ####差异数量：【预测数量】与【总数】的差值
        cy = qty_forecast - qty_total
        purcharse_flag = False
        if if_purchase() and move.purchase_line_id:
            purcharse_flag = True
        #####获取目标库位应该分摊的小组数量 如果是采购的 更新采购订单小组明细
        line_list = self.FA2_compute_platfrom_qty(qty_total, total_rateqty, cy, rate_list, purcharse_flag,move)
        #########新建quant的时候 新建小组记录
        if line_list:
            newquant.FA2_create_newquant_platform(line_list)
        return newquant

    """继承源码的份拆分的方法 实现 内部调拨 出库作业 拆分份的时候的小组库存拆分
       对应的业务单据有【中转仓间的调拨】【发运出库】【销售出库】【盘亏出库】【其他出库】【采销出库】"""

    @api.multi
    def _quant_split(self, qty):
        res = super(stock_quant, self)._quant_split(qty)
        if not res:
            return res
        ############oldquant的数量是分隔出来的数量 newquant的数量是剩余数量
        oldquant,newquant=self,res
        #########调拨的时候 从源quant里获取比例
        def get_quant_platform(quant):
            product_id = quant.product_id.id
            sql = """select b.sys_platform_id,sum(b.qty) from  stock_quant_platform b 
                   where   stock_quant_id=%s group by b.sys_platform_id""" % (quant.id)
            self._cr.execute(sql)
            fet = self._cr.fetchall()
            if fet:
                rate_list = fet
            else:
                rate_list = False
            return rate_list
        rate_list =get_quant_platform(oldquant)
        if not rate_list:
            ####报错处理
            raise UserError(('源库位没有小组库存'))
        rate_list = sorted(rate_list, key=lambda line: line[1], reverse=False)
        ########总数
        qty_total = int(newquant.qty)
        ####比例总数【不一定是数量】
        total_rateqty = sum(line[1] for line in rate_list)
        ####【预测数量】 按照比例向上取整后的总数量
        qty_forecast = sum(math.ceil(qty_total * float(line[1]) / total_rateqty) for line in rate_list)
        ####差异数量：【预测数量】与【总数】的差值
        cy = qty_forecast - qty_total
        purcharse_flag = False
        #####获取目标库位应该分摊的小组数量
        line_list = self.FA2_compute_platfrom_qty(qty_total, total_rateqty, cy, rate_list, purcharse_flag,False)
        if line_list:
            #########更新oldquant的小组库存
            oldquant.FA2_oldquant_update_platform(line_list,newquant)
            #########更新newquant的小组库存
            newquant.FA2_create_newquant_platform(line_list)
        return res





