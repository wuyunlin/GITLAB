# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
import odoo
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons.account.tests.account_test_classes import AccountingTestCase

@odoo.tests.common.at_install(False)
@odoo.tests.common.post_install(True)
class Testplatformstock(odoo.tests.TransactionCase):


    def setUp(self):
        super(Testplatformstock, self).setUp()
    def my_setup(self):
        # 数据准备
        ######3个 订单行  产品1 数量100：【A小组 50 B 小组 50】             【先入库80再入库19个再入库1个】
        ##############    产品2 数量100：【A小组 99 B 小组 1 】             【先入库80再入库19个再入库1个】
        ##############    产品3 数量100：【A小组 1 B 小组 99 】             【先入库80再入库19个再入库1个】
        ##############    产品4 数量100：【A 小组 30 B小组 70】             【先入库80再入库10个再入库10个】
        ##############    产品5 数量100： 【A小组 33 B小组 33 C小组 34】    【先入库97再入库1个再入库1个再入库1个】
        ##############    产品6 数量100： 【A小组 10 B小组 45 C小组 45】    【先入库97再入库1个再入库1个再入库1个】
        ##############    产品7 数量100： 【A小组 97 B小组 2 C小组 1】      【先入库97再入库1个再入库1个再入库1个】
        ##############    产品8 数量100： 【A小组 49 B小组 50 C小组 1】     【先入库97再入库1个再入库1个再入库1个】
        ##############    产品9 数量4： 【A小组 1 B小组 1 C小组 1 D小组1】  【先入库3个再入库1个】
        ##############    产品10 数量4： 【A小组 1 B小组 1 C小组 1 D小组1】  【先入库1个再入库3个】
        ##############    产品11 数量4： 【A小组 1 B小组 1 C小组 1 D小组1】 【先入库2个再入库2个】
        ##### 测试前先创建一张订单利用复制功能 然后复制其明细 【因为后期可能会加必填字段】
        self.purchase_template=self.env['purchase.order'].search([('order_line','!=',False)], order="id desc", limit=1)
        self.product_template = self.env['product.product'].search([], order="id desc", limit=1)
        self.purchase_line_template=self.purchase_template.order_line[0]
        self.test_platform_A=self.env['sys.platform'].create({'name':'A','code':'A'})
        self.test_platform_B = self.env['sys.platform'].create({'name': 'B', 'code': 'B'})
        self.test_platform_C = self.env['sys.platform'].create({'name': 'C', 'code': 'C'})
        self.test_platform_D = self.env['sys.platform'].create({'name': 'D', 'code': 'D'})
        self.test_purchase=self.purchase_template.copy(default={'order_line':[]})
        self.test_product01=self.product_template.copy(default={'name':'pp_01','default_code':'pp_01'})
        self.test_product02=self.product_template.copy(default={'name':'pp_02','default_code':'pp_02'})
        self.test_product03=self.product_template.copy(default={'name':'pp_03','default_code':'pp_03'})
        self.test_product04=self.product_template.copy(default={'name':'pp_04','default_code':'pp_04'})
        self.test_product05=self.product_template.copy(default={'name':'pp_05','default_code':'pp_05'})
        self.test_product06=self.product_template.copy(default={'name':'pp_06','default_code':'pp_06'})
        self.test_product07=self.product_template.copy(default={'name':'pp_07','default_code':'pp_07'})
        self.test_product08=self.product_template.copy(default={'name':'pp_08','default_code':'pp_08'})
        self.test_product09 = self.product_template.copy(default={'name': 'pp_09', 'default_code': 'pp_09'})
        self.test_product10 = self.product_template.copy(default={'name': 'pp_10', 'default_code': 'pp_10'})
        self.test_product11 = self.product_template.copy(default={'name': 'pp_11', 'default_code': 'pp_11'})

        self.purchase_line_template_01=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product01.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':50}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':50})]
                                             })
        self.purchase_line_template_02=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product02.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':99}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':1})]
                                             })
        self.purchase_line_template_03=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product03.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':1}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':99})]
                                             })
        self.purchase_line_template_04=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product04.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':30}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':70})]
                                             })
        self.purchase_line_template_05=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product05.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':33}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':33}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 34})]
                                             })
        self.purchase_line_template_06=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product06.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':10}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':45}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 45})]
                                             })
        self.purchase_line_template_07=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product07.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':97}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':2}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 1})]
                                             })
        self.purchase_line_template_08=self.purchase_line_template.copy(default={'product_qty': 100, 'product_id': self.test_product08.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':49}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':50}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 1})]
                                             })
        self.purchase_line_template_09=self.purchase_line_template.copy(default={'product_qty': 4, 'product_id': self.test_product09.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':1}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':1}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 1}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_D.id, 'qty': 1})]
                                             })
        self.purchase_line_template_10=self.purchase_line_template.copy(default={'product_qty': 4, 'product_id': self.test_product10.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':1}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':1}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 1}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_D.id, 'qty': 1})]
                                             })
        purchase_line_template_11=self.purchase_line_template.copy(default={'product_qty': 4, 'product_id': self.test_product11.id,
                                             'order_id':self.test_purchase.id,
                                             'platform_line':[(0,0,{'sys_platform_id':self.test_platform_A.id,'qty':1}),
                                                              (0,0,{'sys_platform_id':self.test_platform_B.id,'qty':1}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_C.id, 'qty': 1}),
                                                              (0, 0, {'sys_platform_id': self.test_platform_D.id, 'qty': 1})]
                                             })
        self.test_purchase.button_confirm()

        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        location_sample=self.stock_picking.location_dest_id

        self.location_id=self.stock_picking.location_dest_id
        self.location_dest_id=self.location_id.copy(default={'name':'test_location','code':'test_location'})
        self.picking_type_id=self.stock_picking.picking_type_id

    def create_order(self):
        return self.env['stock.picking'].create({'move_type': 'direct',
                                                          'picking_type_id': self.picking_type_id.id,
                                                          'origin': 'TEST',
                                                          'location_id': self.location_id.id,
                                                          'location_dest_id': self.location_dest_id.id })

    def create_order_line(self,data):
        picking_id,product_id,product_uom_qty=data
        return   self.env['stock.move'].create({'name':'TEST',
                                                'picking_id': picking_id,
                                                'product_id': product_id,
                                                'product_uom_qty': product_uom_qty,
                                                'product_uom': 1,
                                                'location_dest_id':self.location_dest_id.id,
                                                'location_id': self.location_id.id})





    ######第一个明细测试  产品1 数量100：【A小组 50 B 小组 50】  【先入库80再入库19个再入库1个】
    def test_purchase_stock_001(self):
        self.my_setup()
        #############################第一次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品1 入库80  预计A小组40 B小组40
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product01).write({'qty_done': 80})
        ############主表操作
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search([('product_id','=',self.test_product01.id),
                                                       ('sys_platform_id','=',self.test_platform_A.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product01.id), ('sys_platform_id', '=', self.test_platform_B.id)])[-1].qty
        self.assertEquals(A_qty, 40, '【NO1 100:(50:50)】 第一次入库 2个小组平均分配的情况下【80】 A小组数量不是40')
        self.assertEquals(B_qty, 40, '【NO1 100:(50:50)】 第一次入库 2个小组平均分配的情况下【80】 B小组数量不是40')
        qty_A=self.test_purchase.order_line.filtered(lambda x: x.product_id ==self.test_product01).platform_line.filtered(lambda x: x.sys_platform_id ==self.test_platform_A).qty
        qty_B=self.test_purchase.order_line.filtered(lambda x: x.product_id ==self.test_product01).platform_line.filtered(lambda x: x.sys_platform_id ==self.test_platform_B).qty
        self.assertEquals(qty_A, 10, '【NO1 100:(50:50)】 第一次入库 2个小组平均分配的情况下 采购明细里 A小组数量剩余10个')
        self.assertEquals(qty_A, 10, '【NO1 100:(50:50)】 第一次入库 2个小组平均分配的情况下 采购明细里 B小组数量剩余10个')
        ####################第二次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品1 入库19  预计A小组40 B小组40
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product01).write({'qty_done': 19})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search([('product_id','=',self.test_product01.id),('sys_platform_id','=',self.test_platform_A.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product01.id), ('sys_platform_id', '=', self.test_platform_B.id)])[-1].qty
        self.assertEquals(A_qty, 49, '【NO1 100:(50:50)】 第二次入库 2个小组平均分配的情况下【19】 A小组数量不是49')
        self.assertEquals(B_qty, 50, '【NO1 100:(50:50)】2个小组平均分配的情况下【19】 B小组数量不是50')
        qty_A=self.test_purchase.order_line.filtered(lambda x: x.product_id ==self.test_product01).platform_line.filtered(lambda x: x.sys_platform_id ==self.test_platform_A).qty
        qty_B=self.test_purchase.order_line.filtered(lambda x: x.product_id ==self.test_product01).platform_line.filtered(lambda x: x.sys_platform_id ==self.test_platform_B).qty
        self.assertEquals(qty_A, 1, '【NO1 100:(50:50)】 2个小组平均分配的情况下 采购明细里 A小组数量不是1个')
        self.assertEquals(qty_B, 0, '【NO1 100:(50:50)】 2个小组平均分配的情况下 采购明细里 B小组数量不是0个')
        ####################第三次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品1 入库80  预计A小组40 B小组40
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product01).write({'qty_done': 1})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search(
            [('product_id','=',self.test_product01.id),('sys_platform_id','=',self.test_platform_A.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product01.id), ('sys_platform_id', '=', self.test_platform_B.id)])[-1].qty
        self.assertEquals(A_qty, 50, '【NO1 100:(50:50)】第三次入库 2个小组平均分配的情况下【1】 A小组数量不是50')
        self.assertEquals(B_qty, 50, '【NO1 100:(50:50)】第三次入库 2个小组平均分配的情况下【1】 B小组数量不是50')
        qty_A=self.test_purchase.order_line.filtered(lambda x: x.product_id ==self.test_product01).platform_line.filtered(lambda x: x.sys_platform_id ==self.test_platform_A).qty
        qty_B=self.test_purchase.order_line.filtered(lambda x: x.product_id ==self.test_product01).platform_line.filtered(lambda x: x.sys_platform_id ==self.test_platform_B).qty
        self.assertEquals(qty_A, 0, '【NO1 100:(50:50)】第三次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是0个')
        self.assertEquals(qty_B, 0, '【NO1 100:(50:50)】第三次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是0个')

        ##########第一次调拨 调拨50个       ######
        picking=self.create_order()
        product_id=self.test_product01.id
        product_qty=50
        data=[picking.id,product_id,product_qty]
        self.create_order_line(data)
        picking.action_confirm()
        picking.action_assign()
        for pack in picking.pack_operation_product_ids:
            pack.qty_done = pack.product_qty
        picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search(
            [('product_id','=',self.test_product01.id),('sys_platform_id','=',self.test_platform_A.id),('stock_location_id','=',picking.location_dest_id.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product01.id), ('sys_platform_id', '=', self.test_platform_B.id),
             ('stock_location_id', '=', picking.location_dest_id.id)])[-1].qty
        self.assertEquals(A_qty, 25, '【NO1 100:(50:50)】调拨50个 A小组调拨25个')
        self.assertEquals(B_qty, 25, '【NO1 100:(50:50)】调拨50个 A小组调拨25个')
        ############现在 源仓位小组库存【25:25】   给源仓位再加一批小组库存【125:25】 那么总库存为【150:50】
        sql = """select b.sys_platform_id,sum(b.qty) from  stock_quant_platform b 
               where b.product_id=%s and b.stock_location_id=%s group by b.sys_platform_id""" % (self.test_product01.id, picking.location_id.id)
        self.cr.execute(sql)
        fet = self.cr.fetchall()
        ###########然后调用40个 观察A 小组库存移动30 B小组移动10

        self.env['stock.quant.platform'].search([('product_id','=',self.test_product01.id),
                                                 ('stock_location_id','=',picking.location_id.id),
                                                 ('sys_platform_id', '=', self.test_platform_A.id),
                                                 ]).write({'qty':150})
        self.env['stock.quant.platform'].search([('product_id','=',self.test_product01.id),
                                                 ('stock_location_id','=',picking.location_id.id),
                                                 ('sys_platform_id', '=', self.test_platform_B.id),
                                                 ]).write({'qty':50})
        ##########第2次调拨 调拨40个 预计A 30个 B 10个      ######
        picking=self.create_order()
        product_id=self.test_product01.id
        product_qty=40
        data=[picking.id,product_id,product_qty]
        self.create_order_line(data)
        picking.action_confirm()
        picking.action_assign()
        for pack in picking.pack_operation_product_ids:
            pack.qty_done = pack.product_qty
        picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search(
            [('product_id','=',self.test_product01.id),('sys_platform_id','=',self.test_platform_A.id),('stock_location_id','=',picking.location_dest_id.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product01.id), ('sys_platform_id', '=', self.test_platform_B.id),
             ('stock_location_id', '=', picking.location_dest_id.id)])[-1].qty
        #100 50 50 25:25 150:50 30:10
        ###### 80+19+1  30+19+1 40 0+9+1  30 10
        ######A 40+9+1  25    15+9+1  150 22 128
        ######B 40+10   25    15+10   50  8  42
        self.assertEquals(A_qty, 55, '【NO1 100:(50:50)】调拨50个 A小组调拨30个 一共55')
        self.assertEquals(B_qty, 35, '【NO1 100:(50:50)】调拨50个 A小组调拨10个 一共35')



    ##############    产品2 数量100：【A小组 99 B 小组 1 】             【先入库80再入库19个再入库1个】
    def Atest_purchase_stock_002(self):
        self.my_setup()
        #############################第一次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品1 入库80  预计A小组40 B小组40
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product02).write({'qty_done': 80})
        ############主表操作
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product02.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product02.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        ####79.2:0.8  80:1  79:1
        self.assertEquals(A_qty, 79, '【NO2 100:(99:1)】 第一次入库 2个小组平均分配的情况下【80】 A小组数量不是79')
        self.assertEquals(B_qty, 1, ' 【NO2 100:(99:1)】 第一次入库 2个小组平均分配的情况下【80】 B小组数量不是1')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product02).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product02).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 20, '【NO2 100:(99:1)】 第一次入库 2个小组平均分配的情况下 采购明细里 A小组数量剩余20个')
        self.assertEquals(qty_B, 0, '【NO2 100:(99:1)】 第一次入库 2个小组平均分配的情况下 采购明细里 B小组数量剩余0个')
        ####################第二次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品2 入库19  预计A小组19 B小组0
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product02).write({'qty_done': 19})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product02.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product02.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 98, '【NO2 100:(99:1)】 第二次入库 2个小组平均分配的情况下【19】 A小组数量不是98')
        self.assertEquals(B_qty, 1, '【NO2 100:(99:1)】 第二次入库 2个小组平均分配的情况下【19】 B小组数量不是1')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product02).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product02).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 1, '【NO2 100:(99:1)】 第二次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是1个')
        self.assertEquals(qty_B, 0, '【NO2 100:(99:1)】 第二次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是0个')
        ####################第三次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品2 入库1  预计A小组1 B小组0
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product02).write({'qty_done': 1})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product02.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product02.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 99, '【NO2 100:(99:1)】 第三次入库 2个小组平均分配的情况下【1】 A小组数量不是99')
        self.assertEquals(B_qty, 1, '【NO2 100:(99:1)】 第三次入库 2个小组平均分配的情况下【1】 B小组数量不是1')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product02).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product02).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 0, '【NO2 100:(99:1)】 第三次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是0个')
        self.assertEquals(qty_B, 0, '【NO2 100:(99:1)】 第三次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是0个')


    ##############    产品3 数量100：【A小组 1 B 小组 99 】             【先入库80再入库19个再入库1个】
    def Atest_purchase_stock_003(self):
        self.my_setup()
        #############################第一次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品3 入库80  预计A小组0 B小组80
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product03).write({'qty_done': 80})
        ############主表操作
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product03.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product03.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        ####0.8:79.2  1:80  0:80
        self.assertEquals(A_qty, 1, '【NO3 100:(1:99)】 第一次入库 2个小组平均分配的情况下【80】 A小组数量不是1')
        self.assertEquals(B_qty, 79, ' 【NO3 100:(1:99)】 第一次入库 2个小组平均分配的情况下【80】 B小组数量不是79')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product03).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product03).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A,0, '【NO3 100:(1:99)】 第一次入库 2个小组平均分配的情况下 采购明细里 A小组数量剩余0个')
        self.assertEquals(qty_B, 20, '【NO3 100:(1:99)】 第一次入库 2个小组平均分配的情况下 采购明细里 B小组数量剩余20个')
        ####################第二次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品3 入库19  预计A小组0 B小组19
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product03).write({'qty_done': 19})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product03.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product03.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 1, '【NO3 100:(1:99)】 第二次入库 2个小组平均分配的情况下【19】 A小组数量不是1')
        self.assertEquals(B_qty, 98, '【NO3 100:(1:99)】 第二次入库 2个小组平均分配的情况下【19】 B小组数量不是98')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product03).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product03).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 0, '【NO3 100:(1:99)】 第二次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是0个')
        self.assertEquals(qty_B, 1, '【NO3 100:(1:99)】 第二次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是1个')
        ####################第三次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品3 入库1  预计A小组1 B小组0
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product03).write({'qty_done': 1})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product03.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product03.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 1, '【NO3 100:(1:99)】 第三次入库 2个小组平均分配的情况下【1】 A小组数量不是1')
        self.assertEquals(B_qty, 99, '【NO3 100:(1:99)】 第三次入库 2个小组平均分配的情况下【1】 B小组数量不是99')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product03).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product03).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 0, '【NO3 100:(1:99)】 第三次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是0个')
        self.assertEquals(qty_B, 0, '【NO3 100:(1:99)】 第三次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是0个')

    ##############    产品4 数量100：【A 小组 30 B小组 70】             【先入库80再入库10个再入库10个】
    def test_purchase_stock_004(self):
        self.my_setup()
        #############################第一次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品4 入库80  预计A小组0 B小组80
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product04).write({'qty_done': 80})
        ############主表操作
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        ####0.8:79.2  1:80  0:80
        self.assertEquals(A_qty, 24, '【NO4 100:(30:70)】 第一次入库 2个小组平均分配的情况下【80】 A小组数量不是24')
        self.assertEquals(B_qty, 56, ' 【NO4 100:(30:70)】 第一次入库 2个小组平均分配的情况下【80】 B小组数量不是56')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product04).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product04).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A,6, '【NO4 100:(30:70)】 第一次入库 2个小组平均分配的情况下 采购明细里 A小组数量剩余6个')
        self.assertEquals(qty_B, 14, '【NO4 100:(30:70)】 第一次入库 2个小组平均分配的情况下 采购明细里 B小组数量剩余14个')
        ####################第二次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品4 入库10  预计A小组3 B小组7
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product04).write({'qty_done': 10})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 27, '【NO4 100:(30:70)】 第二次入库 2个小组平均分配的情况下【10】 A小组数量不是27')
        self.assertEquals(B_qty, 63, '【NO4 100:(30:70)】 第二次入库 2个小组平均分配的情况下【10】 B小组数量不是63')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product04).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product04).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 3, '【NO4 100:(30:70)】 第二次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是3个')
        self.assertEquals(qty_B, 7, '【NO4 100:(30:70)】 第二次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是7个')
        ####################第三次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品4 入库10  预计A小组3 B小组7
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product04).write({'qty_done': 10})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 30, '【NO4 100:(30:70)】 第三次入库 2个小组平均分配的情况下【10】 A小组数量不是30')
        self.assertEquals(B_qty, 70, '【NO4 100:(30:70)】 第三次入库 2个小组平均分配的情况下【10】 B小组数量不是70')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product04).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product04).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 0, '【NO4 100:(30:70)】 第三次入库 2个小组平均分配的情况下 采购明细里 A小组数量不是0个')
        self.assertEquals(qty_B, 0, '【NO4 100:(30:70)】 第三次入库 2个小组平均分配的情况下 采购明细里 B小组数量不是0个')

        #30:70  预计15:35
        ##########第一次调拨 调拨50个       ######
        picking=self.create_order()
        product_id=self.test_product04.id
        product_qty=50
        data=[picking.id,product_id,product_qty]
        self.create_order_line(data)
        picking.action_confirm()
        picking.action_assign()
        for pack in picking.pack_operation_product_ids:
            pack.qty_done = pack.product_qty
        picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search(
            [('product_id','=',self.test_product04.id),('sys_platform_id','=',self.test_platform_A.id),('stock_location_id','=',picking.location_dest_id.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_B.id),
             ('stock_location_id', '=', picking.location_dest_id.id)])[-1].qty
        self.assertEquals(A_qty, 15, '【NO4 100:(30:70)】调拨50个 A小组调拨15个')
        self.assertEquals(B_qty, 35, '【NO4 100:(30:70)】调拨50个 A小组调拨35个')
        ###########然后调用5个 观察A 小组库存移动1.5 B小组移动3.5 2:4 --》2:3
        ##########第2次调拨 调拨5个 预计A 2个 B 3个      ######
        picking=self.create_order()
        product_id=self.test_product04.id
        product_qty=5
        data=[picking.id,product_id,product_qty]
        self.create_order_line(data)
        picking.action_confirm()
        picking.action_assign()
        for pack in picking.pack_operation_product_ids:
            pack.qty_done = pack.product_qty
        picking.do_transfer()
        A_qty=self.env['stock.quant.platform'].search(
            [('product_id','=',self.test_product04.id),('sys_platform_id','=',self.test_platform_A.id),('stock_location_id','=',picking.location_dest_id.id)])[-1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product04.id), ('sys_platform_id', '=', self.test_platform_B.id),
             ('stock_location_id', '=', picking.location_dest_id.id)])[-1].qty
        #100 50 50 25:25 150:50 30:10
        ###### 80+19+1  30+19+1 40 0+9+1  30 10
        ######A 40+9+1  25    15+9+1  150 22 128
        ######B 40+10   25    15+10   50  8  42
        self.assertEquals(A_qty, 17, '【NO4 100:(50:50)】调拨50个 A小组调拨30个 一共55')
        self.assertEquals(B_qty, 38, '【NO4 100:(50:50)】调拨50个 A小组调拨10个 一共35')







    ##############    产品5 数量100： 【A小组 33 B小组 33 C小组 34】    【先入库97再入库1个再入库2个】

    def Atest_purchase_stock_005(self):
        self.my_setup()
        #############################第一次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品5 入库80  预计A小组32.01 B小组32.01 c 小组 32.98     33:33:33-->33:32:32
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product05).write({'qty_done': 97})
        ############主表操作
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product05.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product05.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        C_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product05.id), ('sys_platform_id', '=', self.test_platform_C.id)])[
            -1].qty
        ####0.8:79.2  1:80  0:80
        self.assertEquals(A_qty, 32, '【NO5 100:(33:33:34)】 第一次入库 3个小组平均分配的情况下【97】 A小组数量不是32')
        self.assertEquals(B_qty, 33, ' 【NO5 100:(33:33:34)】 第一次入库 3个小组平均分配的情况下【97】 B小组数量不是33')
        self.assertEquals(C_qty, 32, ' 【NO5 100:(33:33:34)】 第一次入库 3个小组平均分配的情况下【97】 B小组数量不是32')

        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product05).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product05).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        qty_C = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product05).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_C).qty
        self.assertEquals(qty_A,1, '【NO5 100:(33:33:34)】 第一次入库 3个小组平均分配的情况下 采购明细里 A小组数量剩余1个')
        self.assertEquals(qty_B, 0, '【NO5 100:(33:33:34)】 第一次入库 3个小组平均分配的情况下 采购明细里 B小组数量剩余0个')
        self.assertEquals(qty_C, 2, '【NO5 100:(33:33:34)】 第一次入库 3个小组平均分配的情况下 采购明细里 C小组数量剩余2个')

        ####################第二次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品5 入库1  预出A：1
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product05).write({'qty_done': 1})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product05.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        self.assertEquals(A_qty, 33, '【NO5 100:(33:33:34)】 第二次入库 3个小组平均分配的情况下【1】 A小组数量不是33')

        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product05).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_B, 0, '【NO5 100:(33:33:34)】 第二次入库 3个小组平均分配的情况下 采购明细里 B小组数量不是0个')
        ####################第三次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品4 入库2  预计C：2
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product05).write({'qty_done': 2})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()

        C_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product05.id), ('sys_platform_id', '=', self.test_platform_C.id)])[
            -1].qty
        self.assertEquals(C_qty, 34, '【NO5 100:(33:33:34)】 第三次入库 3个小组平均分配的情况下【2】 C小组数量不是34')

        qty_C = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product05).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_C).qty
        self.assertEquals(qty_C, 0, '【NO5 100:(33:33:34)】 第三次入库 3个小组平均分配的情况下 采购明细里 C小组数量不是0个')


        ##############    产品11 数量4： 【A小组 1 B小组 1 C小组 1 D小组1】 【先入库2个再入库2个】

    def Atest_purchase_stock_0011(self):
        self.my_setup()
        #############################第一次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品11 入库2  预计A小组0 B小组0 c 小组 1 D小组1
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product11).write({'qty_done': 2})
        ############主表操作
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product11.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product11.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        C_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product11.id), ('sys_platform_id', '=', self.test_platform_C.id)])[
            -1].qty
        D_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product11.id), ('sys_platform_id', '=', self.test_platform_D.id)])[
            -1].qty
        ####0.8:79.2  1:80  0:80
        self.assertEquals(A_qty, 0, '【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下【2】 A小组数量不是0')
        self.assertEquals(B_qty, 0, ' 【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下【2】 B小组数量不是0')
        self.assertEquals(C_qty, 1, ' 【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下【2】 C小组数量不是1')
        self.assertEquals(D_qty, 1, ' 【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下【2】 C小组数量不是1')


        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product11).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product11).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        qty_C = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product11).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_C).qty
        qty_D = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product11).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_D).qty
        self.assertEquals(qty_A,1, '【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下 采购明细里 A小组数量剩余1个')
        self.assertEquals(qty_B, 1, '【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下 采购明细里 B小组数量剩余1个')
        self.assertEquals(qty_C, 0, '【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下 采购明细里 C小组数量剩余0个')
        self.assertEquals(qty_D, 0, '【NO11 4:(1:1:1:1)】 第一次入库 4个小组平均分配的情况下 采购明细里 C小组数量剩余0个')

        ####################第二次入库
        self.stock_picking = self.test_purchase.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
        ######产品11 入库2  预出C：1 D：1
        self.stock_picking.pack_operation_product_ids.filtered(
            lambda x: x.product_id == self.test_product11).write({'qty_done': 2})
        for pack in self.stock_picking.pack_operation_product_ids:
            if pack.qty_done > 0:
                pack.product_qty = pack.qty_done
            else:
                pack.unlink()
        self.stock_picking.do_transfer()
        A_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product11.id), ('sys_platform_id', '=', self.test_platform_A.id)])[
            -1].qty
        B_qty = self.env['stock.quant.platform'].search(
            [('product_id', '=', self.test_product11.id), ('sys_platform_id', '=', self.test_platform_B.id)])[
            -1].qty
        self.assertEquals(A_qty, 1, '【NO11 4:(1:1:1:1)】 第二次入库 4个小组平均分配的情况下【1】 A小组数量不是1')
        self.assertEquals(B_qty, 1, '【NO11 4:(1:1:1:1)】 第二次入库 4个小组平均分配的情况下【1】 B小组数量不是1')
        qty_A = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product11).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_A).qty
        qty_B = self.test_purchase.order_line.filtered(
            lambda x: x.product_id == self.test_product11).platform_line.filtered(
            lambda x: x.sys_platform_id == self.test_platform_B).qty
        self.assertEquals(qty_A, 0, '【NO11 4:(1:1:1:1)】 第二次入库 4个小组平均分配的情况下 采购明细里 A小组剩余数量不是0个')
        self.assertEquals(qty_B, 0, '【NO11 4:(1:1:1:1)】 第二次入库 4个小组平均分配的情况下 采购明细里 B小组剩余数量不是0个')



