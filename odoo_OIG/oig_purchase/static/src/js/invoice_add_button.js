
odoo.define('oig_purchase.ListView', function (require) {
"use strict";
var ControlPanelMixin = require('web.ControlPanelMixin');
var core = require('web.core');
var time = require('web.time');
var ListView = require('web.ListView');
var Model = require('web.DataModel');
var session = require('web.session');
var Widget = require('web.Widget');
var view = require('web.View');
var data = require('web.data');
var data_manager = require('web.data_manager');
var pyeval = require('web.pyeval');
var QWeb = core.qweb;
var _t = core._t;
var _lt = core._lt;
var StateMachine = window.StateMachine;

ListView.include({
    /**
     * Extend the render_buttons function of ListView by adding an event listener
     * on the import button.
     * @return {jQuery} the rendered buttons
     */
    render_buttons: function() {
        var self = this;
        var add_button = false;
        if (!this.$buttons) { // Ensures that this is only done once
            add_button = true;
        }
        this._super.apply(this, arguments); // Sets this.$buttons
        this.$buttons.on('click', '.o_list_button_action_A', oe_myservice_A.bind(this));
    },
});

//lynn  add tree button end
function oe_myservice_A() {
    var self = this;
    this.trigger_up('clear_uncommitted_changes', {
        callback: function() {
            self.rpc("/web/action/load", { action_id: "oig_purchase.action_update_date_wizard" }).done(function(result) {
                result.context = {'active_model':self.dataset.model,
                                   'default_res_users_id':session.uid,
                                   'active_ids':self.groups.get_selection().ids,
                                   'default_model_name':self.dataset.model};
                self.do_action(result);
            });
        },
    });
}




});












