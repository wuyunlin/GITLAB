# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
import odoo
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons.account.tests.account_test_classes import AccountingTestCase

@odoo.tests.common.at_install(False)
@odoo.tests.common.post_install(True)
class TestAccountInvoice(odoo.tests.TransactionCase):

    def setUp(self):
        super(TestAccountInvoice, self).setUp()

    # 采购审批测试
    def test_purchase_audit(self):
        # self.env['purchase.order']
        self.amount_total = self.env['account.invoice'].search()
        self.env['account.invoice'].purchase_audit()