# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
# 批量确认订单
class Batch_Confirm(models.TransientModel):
    _name = "batch.confirm"
    _description = "Batch Confirm"

    @api.multi
    def batch_confirm_order(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        result = ''
        for order in self.env['purchase.order'].browse(active_ids):
            if order.open_state == "open" and order.state == "draft":
                order.button_confirm()
        return {'type': 'ir.actions.act_window_close'}