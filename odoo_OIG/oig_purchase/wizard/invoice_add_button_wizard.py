# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID, tools, _
from odoo.exceptions import UserError
from odoo.addons.oig_purchase.models.account_invoice_support import SPECIAL_REASON
class update_date_wizard(models.TransientModel):
    _name = "update.date.wizard"
    _description = u"调整日期"
    date = fields.Date(string=u'调到的日期',required=True)
    @api.model
    def default_get(self, fields):
        rec = super(update_date_wizard, self).default_get(fields)
        context = dict(self._context or {})
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')
        # Checks on context parameters
        if not active_model or not active_ids:
            raise UserError(
                ("请至少选择一个付款明细"))
        # Checks on received invoice records
        invoice = self.env[active_model].browse(active_ids[0]).invoice_id
        if (invoice.state != 'draft' or invoice.audit_state not in ['first']):
            raise UserError(("当前付款申请单已经进入审批阶段，不能调整日期！"))
        return rec

    @api.multi
    def confirm(self):
        action = self.env.ref('purchase_pay.view_purchase_pay_tree')
        result = action.read()[0]
        ########查询满足条件的付款单
        def get_invoice(invoice_line):
            invoice=invoice_line.invoice_id
            find_invoice_ids=self.env['account.invoice'].search([('type','=',invoice.type),('state','=','draft'),('date_invoice','=',date),
                                                ('purchase_order_id','=',invoice.purchase_order_id.id)])

            return find_invoice_ids and find_invoice_ids[0].id or False

        def get_line(invoice_line,search_invoice):
            fit_line=invoice_line.search([('invoice_id','=',search_invoice),('purchase_line_id','=',invoice_line.purchase_line_id.id)])
            return fit_line and fit_line[0] or False


        ###取得勾选的付款明细ID集合
        active_ids = self._context.get('active_ids',[])
        if not active_ids:
            return True
        date=self.date
        #获取付款明细实例
        invoice_line_instance=self.env['account.invoice.line'].browse(active_ids)
        p_p_id = invoice_line_instance[0].purchase_pay_id[0].id
        one_invoice = invoice_line_instance[0].invoice_id
        dict = invoice_line_instance[0].purchase_id.action_view_invoice()
        purchase_pay_id = invoice_line_instance[0].purchase_pay_id.id
        ###判断日期大于源账单日期
        if one_invoice.date_invoice>self.date:
            raise UserError(('账单日期只可以向后延期不可以向前延！'))
        #######如果调整日期等于当前付款单的日期
        if date == one_invoice.date_invoice:
            return dict
        #######如果付款单的状态不处于草稿状态 不能进行调整
        if one_invoice.state not in ['draft'] or one_invoice.audit_state not in ['first']:
            raise UserError(('当前付款申请单已经进入审批阶段，不能调整日期！'))

        for invoice_line in invoice_line_instance:
            #####获取新的付款单
            search_invoice = get_invoice(invoice_line)
            remain_invoice = invoice_line.invoice_id
            ####没有新的付款单
            if not search_invoice:
                default_line = {'flag': 'delay', 'date_invoice': date, 'invoice_line_ids': False}
                now_invoice=invoice_line.invoice_id
                new_invoice = now_invoice.copy(default=default_line)
                invoice_line.write({'invoice_id': new_invoice.id})
            else:
                ######将付款明细移到找到的付款单下面
                ######判断是否当前产品已有付款明细行？ 1已有：合并数量并将当前付款明细删除 2 没有则将当前付款明细搬家
                fit_line=get_line(invoice_line,search_invoice)
                if fit_line:
                    quantity=invoice_line.quantity
                    invoice_line.unlink()
                    fit_line.write({'quantity':quantity+fit_line.quantity})
                else:

                    invoice_line.write({'invoice_id': search_invoice})
            if not remain_invoice.invoice_line_ids:
                remain_invoice.unlink()
        #dict = invoice_line_instance[0].purchase_id.action_view_invoice()

        # 延期时对关联采购订单的更新
        sql = """select string_agg(b.name,';') from account_invoice a
                        inner join purchase_order b on b.id = a.purchase_order_id where a.purchase_pay_id = %s"""%(p_p_id)
        self._cr.execute(sql)
        fet = self._cr.fetchone()
        self.env['purchase.pay'].browse(p_p_id).purchase_order = fet and fet[0] or ''

        # 处理跳转页面问题
        list2 = []
        if purchase_pay_id:
            list2.append(purchase_pay_id)
        if invoice_line_instance[0].purchase_pay_id:
            list2.append(invoice_line_instance[0].purchase_pay_id.id)
        result['context'] = {}
        if len(list2) > 1:
            result['domain'] = "[('id','in',[" + ','.join(map(str, list2)) + "])]"
            return{
                'name': _('Purchase Pay'),
                'domain': "[('id','in',[" + ','.join(map(str, list2)) + "])]",
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'purchase.pay',
                'type': 'ir.actions.act_window',
                'view_id': False,
                'target': 'current',
                'context': self.env.context,
            }
        elif len(list2) == 1 and list2[0]:
            return{
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.pay',
                'type': 'ir.actions.act_window',
                'res_id': list2[0],
                'context': self.env.context,
            }






