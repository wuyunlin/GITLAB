# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError

# 批量采购审批
class Batch_Approval_Confirm(models.TransientModel):

    _name = "batch.approval.confirm"
    _description = "Batch approval"

    @api.multi
    def batch_approval(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        result = ''
        for invoice in self.env['account.invoice'].browse(active_ids):
            try:
                invoice.purchase_audit()
            except Exception as err:
                result = result+err.name + "++"
        if result:
            raise UserError(result)
        else:
            return {'type': 'ir.actions.act_window_close'}

# 批量采购经理审批
class Special_Batch_Confirm(models.TransientModel):

    _name = "manager.approval.confirm"
    _description = "Manager approval"

    @api.multi
    def manager_approval(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        result = ''
        for invoice in self.env['account.invoice'].browse(active_ids):
            try:
                invoice.purchase_manager_audit()
            except Exception as e:
                result = result + e.name + "++"
        if result:
            raise UserError(result)
        else:
            return {'type': 'ir.actions.act_window_close'}


# 批量财务审批
class Special_Batch_Confirm(models.TransientModel):

    _name = "financial.approval.confirm"
    _description = "Financial approval"

    @api.multi
    def financial_approval(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        result = ''
        for invoice in self.env['account.invoice'].browse(active_ids):
            try:
                invoice.finance_audit()
            except Exception as e:
                result = result + e.name + "++"
        if result:
            raise UserError(result)
        else:
            return {'type': 'ir.actions.act_window_close'}

# 批量财务批量付款
class Financial_Pay(models.TransientModel):

    _name = "financial.pay"
    _description = "Financial Pay"

    @api.multi
    def financial_pay(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        result = ''
        for invoice in self.env['account.invoice'].browse(active_ids):
            try:
                invoice.verification()
            except Exception as e:
                result = result + e.name + "++"
        if result:
            raise UserError(result)
        else:
            return {'type': 'ir.actions.act_window_close'}

class Go_Back(models.TransientModel):

    _name = "go.back"
    _description = "Go Back"

    @api.multi
    def do_go_back(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for purchase_pay in self.env['purchase.pay'].browse(active_ids):
            purchase_pay.go_back()
        return {'type': 'ir.actions.act_window_close'}
