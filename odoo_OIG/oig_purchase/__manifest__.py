# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Oig Pruchase',
    'version': '1.2',
    'category': 'Purchases',
    'sequence': 60,
    'summary': 'Oig Pruchase',
    'description': """
Manage goods requirement by Purchase Orders easily
==================================================
    """,
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['oig_base', 'purchase_pay'],
    'data': [
        'views/account_invoice_support_views.xml',
        'views/timing_tasks.xml',
        'wizard/invoice_add_button_view.xml',
        'wizard/batch_add_button_view.xml',
        'views/close_order_timing_tasks.xml',
        'wizard/batch_confirm_view.xml',
        'views/over_receive_tasks.xml',
        'views/all_receive_tasks.xml',
        'views/purchase_requirement_views.xml',
        'security/ir.model.access.csv',
        'views/create_stock_move_view.xml',

    ],
   'qweb': ['static/src/xml/invoice_add_button.xml'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
