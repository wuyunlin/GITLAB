# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
import logging
import datetime
from odoo.tools.float_utils import float_compare
from odoo.exceptions import UserError
class purchase_order(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def create(self, vals):
        res = super(purchase_order, self).create(vals)
        return res

    # 确认订单
    def button_confirm(self):
        #####更新采购订单的收货类型为收货人的交货类型
        self.picking_type_id = self.oig_receiver_id.stock_picking_type_id
        ########更新付款方式为供应商的付款方式
        self.pay_way = self.partner_id.pay_way
        self.pay_type = self.partner_id.pay_type

        if self.pay_type == "private":
            if not self.partner_id.bank_information:
                raise UserError(self.name + "开户行信息不能为空，请在供应商中维护！")
            elif not self.partner_id.account_name:
                raise UserError(self.name + "户名不能为空，请在供应商中维护！")
            elif not self.partner_id.pay_account:
                raise UserError(self.name + "付款账户不能为空，请在供应商中维护！")
        elif self.pay_type == "net":
            if not self.website:
                raise UserError(self.name + "付款类型为网上拍，需要账户网址！")

        if not self.pay_way:
            raise UserError(self.name + "付款方式不能为空！")
        res = super(purchase_order, self).button_confirm()
        if self.pay_way in ['all', 'pay_pay_delivery', 'pay_delivery_pay']:
            self.create_pay_order('auto')
        self.get_purchase_order('auto')
        return res

    # 订单下发  其他入 其他出  成本调整单 销售出库明细 发运单明细
    def get_purchase_order(self, flag):
        dict = {}
        # 采购订单号
        dict['PONo'] = self.name
        # 采购订单类型
        dict['POType'] = self.env['ir.config_parameter'].sudo().get_param('POType')
        # 货主ID
        dict['CustomerID'] = self.env['ir.config_parameter'].sudo().get_param('CustomerID')
        # 创建时间
        dict['POCreationTime'] = self.create_date
        # 外部单号
        dict['POReference1'] = self.name
        # 创建时间
        dict['ADDTIME'] = self.date_order + " 00:00:00"
        # 创建人
        if self.env.user.id == 1:
            dict['ADDWHO'] = self.responser
        else:
            dict['ADDWHO'] = self.env.user.name
        # 供应商代码
        dict['SupplierID'] = self.partner_id.ref
        # 仓库
        dict['WarehouseID'] = self.env['ir.config_parameter'].sudo().get_param('WarehouseID')
        # 供应商名称
        dict['Supplier_Name'] = self.partner_id.name
        # 退税单号(暂时写法)
        dict['POReference2'] = self.tax_no
        # 备注
        dict['UserDefine1'] = self.notes
        # 物流名称
        dict['UserDefine2'] = self.logistics_name
        # 物流单号
        dict['UserDefine3'] = self.logistics_number
        # 外部单号
        dict['POReference1'] = self.partner_ref
        dict['UserDefine4'] = False
        line_list = []

        for line in self.order_line:
            line_dict = {}
            # 采购订单号
            line_dict['PONo'] = line.order_id.name
            # 采购订单行号
            line_dict['POLineNo'] = line.id
            # 产品
            line_dict['SKU'] = line.product_id.default_code
            # 产品中文描述
            line_dict['SKUDescrC'] = line.product_id.name
            # 订单数量
            line_dict['OrderedQty'] = line.product_qty
            # 收货时间
            line_dict['ReceivedTime'] = line.date_planned + " 00:00:00"
            # 单位 待确定
            line_dict['UOM'] = line.product_uom.name
            # 收货人属性
            line_dict['LotAtt04'] = line.order_id.oig_receiver_id.code
            # 质检要求
            line_dict['UserDefine1'] = line.quality_inspection
            # 包装要求
            line_dict['UserDefine2'] = line.packing_instruction
            # 是否提样
            line_dict['UserDefine3'] = line.is_sample
            # 备注
            line_dict['Notes'] = line.remark

            # 判断收货天数是否有维护，没有则提示错误
            temp = 0
            for seller in line.product_id.seller_ids:
                if self.partner_id.display_name == seller.name.display_name:
                    temp = 1
                    if not seller.delay:
                        raise UserError(line.name + "，这个产品没有维护交货天数！")

            if temp == 0:
                raise UserError(line.name + "，这个产品供应商中未维护此供应商")
            line_list.append(line_dict)

        dict['detailsItem'] = line_list

        if dict:
            xmldata = {'header': [dict]}
            if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                result = self.env['send.controller'].dispatch('putPOData', xmldata)
                now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
                log_sql = """insert into log_data(purchase_order,result,create_date) VALUES('%s','%s','%s')"""%(self.name, result.get('Response').get('return').get('returnDesc') + str(dict).replace('\'', '\"'), now)
                self.env.cr.execute(log_sql)
                if result.get("Response").get("return").get("returnFlag") == "0":
                    if ('不能重复审核，请先取消审核再操作') in (result.get("Response").get("return").get("returnDesc")):
                        self.write({'issued_state': 'Y'})
                    else:
                        raise UserError('调用失败：' + result.get("Response").get("return").get("returnDesc"))
                else:
                    self.write({'issued_state': 'Y'})
        return True

    # 采购状态明细查询
    def query_podata(self, flag, query_dict):
        if 0 and "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            result = self.env['send.controller'].dispatch('queryPOData', query_dict)
            if result.get("Response").get("return").get("returnFlag") == "0":
                raise UserError('订单查询调用失败' + result.get("Response").get("return").get("returnDesc"))
            else:
                ##########取消订单时候查询
                if flag == 'cancel':
                    for line in result.get("Response").get("ponos"):
                        if line['status'] not in ['00', '90']:
                            # 00订单创建 10订单确认  20部分ASN  30完全ASN  40完全收货  90订单取消  99订单关闭
                            raise UserError('SRM存在状态不是创建的订单行，不能取消订单')
                else:########关闭订单时候的查询
                    for line in result.get("Response").get("ponos"):
                        if line['status'] not in ['00', '40', '99']:
                            # 00订单创建 10订单确认  20部分ASN  30完全ASN  40完全收货 90订单取消 99订单关闭
                            raise UserError('SRM存在状态不是【创建 完全收货 订单关闭】的订单行，不能关闭订单')
                logging.info('订单查询调用成功')
        else:
            pass

    # 取消订单
    def button_cancel(self):
        data = {}
        state = self.state
        res = super(purchase_order, self).button_cancel()
        if state not in ['purchase']:
            return res
        ######订单明细查询
        self.query_podata('cancel', {'data': {'ordernos': {'PONO': self.name, 'POType': self.env['ir.config_parameter'].sudo().get_param('POType'), 'CustomerID': self.env['ir.config_parameter'].sudo().get_param('CustomerID')}}})
        ############取消订单接口
        dict = {}
        # 入库单号
        dict['PONO'] = self.name
        # 入库单类型
        dict['Reason'] = self.name
        xmldata = {'data': {'ordernos': dict}}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            result = self.env['send.controller'].dispatch('cancelPOData', xmldata)
            if result.get("Response").get("return").get("returnFlag") == "0":
                raise UserError('cancelPOData 取消订单调用失败' + result.get("Response").get("return").get("returnDesc"))
        return res

    # 调用SRM接口【采购订单关闭】
    def order_close(self):
        if self.open_state == 'close':
            return True
        self.write({'open_state': "close"})
        #####订单状态不是已经审核
        if self.state not in ['purchase', 'done']:
            return True
        ######订单明细查询
        self.query_podata('close', {'data': {'ordernos': {'PONO': self.name, 'POType': self.env['ir.config_parameter'].sudo().get_param('POType'), 'CustomerID': self.env['ir.config_parameter'].sudo().get_param('CustomerID')}}})
        ###########调用明细关闭
        # 设置变量初始值为0
        temp = 0
        for orderLine in self.order_line:
            if orderLine.open_state != "close":
                orderLine.button_close()

        for orderLine in self.order_line:
            if orderLine.open_state != 'close':
                # 如果明细行中有没有关闭的订单就赋值为1
                temp = 1

        # 如果上面循环完成，但是temp还是为0，则标识所有明细都已经关闭，那么直接关闭此订单
        if temp == 0:
            self.write({'open_state': 'close'})
            for picking in self.picking_ids:
                if picking.state != 'done':
                    picking.action_cancel()

        ########调用主表关闭接口
        dict = {}
        dict['PONO'] = self.name
        dict['Reason'] = self.name
        xmldata={'data': {'ordernos': dict}}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            result = self.env['send.controller'].dispatch('ClosePOData', xmldata)
            if result.get("Response").get("return").get("returnFlag") == "0":
                raise UserError('调用失败' + result.get("Response").get("return").get("returnDesc"))
        return True

    # 跑批自动生成付款申请单
    def auto_create_pay_order(self):
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        # 当天未进行一审的付款申请单，凌晨系统自动清理掉，重新生成当天需要付款的付款申请单
        def unlink_func():
            # 查询数据不是“延期”的，审核状态为“待采购审核”的
            result = self.env['account.invoice'].search([('flag', '!=', 'delay'), ('audit_state', '=', 'first'), ('type', '=', 'in_invoice')])
            # 循环所有的 状态不是“延期”的，审核状态为“待采购审核”的所有付款申请单
            for account_invoice in result:
                # 判断如果源付款申请单关联了大付款申请单
                if len(account_invoice.purchase_pay_id) > 0:
                    # 如果查到关联的付款申请单有且有一个，删除源付款申请单的同时，删除大付款申请单
                    if len(self.env['account.invoice'].search([('purchase_pay_id', '=', account_invoice.purchase_pay_id.id)])) == 1:
                        account_invoice.purchase_pay_id.unlink()
                    else:
                        account_invoice.unlink()
            return True
        unlink_func()
        #########删除没有审批的且是非延期的
        order = self.env['purchase.order'].search([('open_state', '=', 'open'), ('state', 'in', ('purchase', 'done'))])
        # 循环处理 付款申请单
        for orderData in order:
            try:
                orderData.create_pay_order('auto')
                logging.info("创建付款申请单成功,订单名称为："+ orderData.name)
                self.invalidate_cache()
                self._cr.commit()
            except Exception as e:
                self._cr.rollback()
                self.invalidate_cache()
                self._cr.commit()
                error = e.message or getattr(e, 'reason', '') and getattr(e.reason, 'strerror', '') or \
                        getattr(e, 'args', ['NOT KNOW ERROR'])[0]
                logging.info("创建付款申请单失败,订单名称为："+ orderData.name +"失败原因：" +error)
                sql = "insert into purchase_pay_log(purchase_order, log_info, create_date) VALUES ('%s', '%s', '%s')"%(orderData.name, "创建付款申请单失败,订单名称为："+ orderData.name +"失败原因：" +error, now)
                self.env.cr.execute(sql)

        sql = """select cost,purchase_order,cost_product_money,id from purchase_pay"""
        self._cr.execute(sql)
        fet = self._cr.fetchall()
        for re in fet:
            if not re[0] and not re[1] and not re[2]:
                self.env['purchase.pay'].search([('id', '=', re[3])]).unlink()
                insert_sql = "insert into purchase_pay_delete(purchase_order,create_date,log_info)VALUES ('%s','%s','%s')"%(re[1], now, '运费、采购订单和总金额为0！')
                self.env.cr.execute(insert_sql)
            purchae_pay = self.env['purchase.pay'].search([('id', '=', re[3])])
            if purchae_pay and re[1]:
                purchase_pay_result = self.env['account.invoice'].search([('purchase_pay_id', '=', purchae_pay.id)])
                if not purchase_pay_result:
                    purchae_pay.unlink()
                    insert_sql = "insert into purchase_pay_delete(purchase_order,create_date,log_info)VALUES ('%s','%s','%s')"%(re[1], now, '源付款申请单中不存在大付款申请单！')
                    self.env.cr.execute(insert_sql)
        return True

    # 将5天内合单的功能拆出来
    def fiveDays_join_order(self):
        #####  BEGIN
        sql = """with ok_partner as (
                    select a.partner_id from purchase_pay a
                    inner join res_partner b on b.id=a.partner_id and b.pay_way='pay_pay_delivery'
                    where
                    a.type='in_invoice'
                    and a.audit_state='first'
                    and a.date_due=(select current_date)
                    )
                    select  Row_Number() OVER (ORDER BY a.id),dense_rank() OVER (partition by b.id ORDER BY a.id ),
                    b.id as purchase_order_id, a.id,a.date_planned,b.id as purchase_id from purchase_order_line a
                    inner join purchase_order b on b.id=a.order_id and b.state in ('purchase','done')
                    inner join res_partner c on c.id=b.partner_id
                    where a.open_state='open'
                    and a.qty_invoiced<a.price_subtotal
                    and a.date_planned-coalesce(c.trans_day,0)   <=(select current_date+interval'5 days')                    
                    and b.partner_id in (select partner_id from ok_partner)
                    and (cast(b.partner_id as  varchar) || b.responser) in (select cast(partner_id as varchar)||responser from purchase_order)
                    order by Row_Number() OVER (ORDER BY a.id) desc
                    """
        self._cr.execute(sql)
        fet_result = self._cr.fetchall()
        if len(fet_result) > 0:
            pol_ids = []
            for each in fet_result:
                if each[1] != 1:
                    pol_ids.append(each[3])
                else:
                    pol_ids.append(each[3])
                    purchase_order_instance, purchase_order_line_ids = self.env['purchase.order'].browse(each[5]), \
                                                                       self.env['purchase.order.line'].browse(pol_ids)
                    try:
                        purchase_order_instance.create_pay_order('auto', {},purchase_order_line_ids, 5)
                        logging.info("5天自动创建付款申请单成功,订单名称为：" + purchase_order_instance.name)
                        self._cr.commit()
                    except Exception as e:
                        self._cr.rollback()
                        error = e.message or getattr(e, 'reason', '') and getattr(e.reason, 'strerror', '') or \
                                getattr(e, 'args', ['NOT KNOW ERROR'])[0]
                        logging.info("5天自动创建付款申请单失败,订单名称为：" + purchase_order_instance.name + "失败原因：" + error)
                        self._cr.commit()

                    pol_ids = []
        else:
            logging.info("5天自动创建付款申请单失败,没有查到数据！")
        return True

    # 关闭订单
    def close_order(self):
        #########删除没有审批的且是非延期的
        order = self.env['purchase.order'].search([('open_state', '=', 'open'), ('state', 'in', ('purchase', 'done'))])
        for orderData in order:
            temp = 0
            for orderline in orderData.order_line:
                try:
                    if (round(orderline.product_qty, 3) == round(orderline.qty_received, 3)) and (round(orderline.qty_paid, 3) == round((orderline.product_qty * orderline.price_unit), 3)):
                        dict = {}
                        # 采购订单单号
                        dict['PONO'] = orderData.order_line[0].order_id[0].name
                        # 采购订单行号
                        dict['POLineNo'] = orderline.id
                        dict['Reason'] = orderData.order_line[0].order_id[0].name
                        xmldata = {'data': {'ordernos': dict}}
                        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                            results = self.env['send.controller'].dispatch('closePOLine', xmldata)
                            if results.get("Response").get("return").get("returnFlag") == "0":
                                raise UserError('订单行关闭调用SRM接口失败')
                                temp = 1
                            else:
                                orderline.write({'open_state': 'close'})
                                self._cr.commit()
                    else:
                        temp = 1
                except Exception as e:
                    self._cr.rollback()
                    continue
            # 订单行都已经关闭，则关闭整张订单
            for order_line in orderData.order_line:
                if order_line.open_state == 'open':
                    temp == 1
            if temp == 0:
                dict = {}
                dict['PONO'] = orderData.order_line[0].order_id[0].name
                dict['Reason'] = orderData.order_line[0].order_id[0].name
                xmldata={'data': {'ordernos': dict}}
                if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
                    result = self.env['send.controller'].dispatch('ClosePOData', xmldata)
                    if result.get("Response").get("return").get("returnFlag") == "0":
                        raise UserError('调用失败' + result.get("Response").get("return").get("returnDesc"))
                    else:
                        orderData.write({'open_state': 'close'})
                        self._cr.commit()

    # 生成付款申请单
    def create_pay_order(self, flag, context={}, purchase_order_line_instance=[], delay=0):
        flag_result = ''
        if flag == "auto" or flag == "manual":
            flag_result = flag
        else:
            flag_result = flag.get('flag')
        dict = {}
        dict['partner_id'] = self.partner_id.id
        dict['type'] = 'in_invoice'
        # 付款时间
        dict['date_invoice'] = (datetime.datetime.now()+datetime.timedelta(hours=8)).strftime("%Y-%m-%d %H:%M:%S")
        # 创建方式（手动、自动）
        dict['flag'] = flag_result
        # 采购订单
        dict['purchase_order_id'] = self.id
        # 付款账号
        dict['pay_account'] = self.partner_id.pay_account
        # 户名
        dict['account_name'] = self.partner_id.account_name
        # 开户行信息
        dict['bank_information'] = self.partner_id.bank_information
        # 负责人
        dict['responser'] = self.responser
        if self.pay_type == "net":
            dict['website'] = self.website

        if self.pay_way in ['all', 'pay_pay_delivery', 'pay_delivery_pay'] and len(self.invoice_ids) == 0:
            dict['account_invoice_state'] = "prepayments"
        else:
            dict['account_invoice_state'] = "payment_requisition"
        dict['amount_total'] = 0
        result = self.env['account.invoice'].create(dict)
        new_lines = self.env['account.invoice.line']
        for line in (purchase_order_line_instance or self.order_line):
            # 如果订单明细行中有一个关闭的明细行时，怎不能生成付款申请单
            if line.open_state != 'close':
                data = self.prepare_invoice_line_from_po_line(line, delay)
                ########自动且金额为0 的情况下  不进入  【创建付款申请】
                if not (flag == 'auto' and data['quantity'] == 0):
                    data.update({'invoice_id': result.id})
                    new_line = new_lines.create(data)
                    result._onchange_invoice_line_ids()
        if (not result.amount_total) and flag == 'auto':
            result.purchase_pay_id.unlink()
            return True
        else:
            return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.pay',
                'type': 'ir.actions.act_window',
                'res_id': result.purchase_pay_id.id,
                'context': self.env.context,
            }

    # 处理付款申请单逻辑判断
    def prepare_invoice_line_from_po_line(self, line, delay=0):
        if self.pay_way:
            if self.pay_way == 'all':                                   # 预付全款
                qty = line.price_subtotal - line.qty_invoiced
            elif self.pay_way == 'pay_pay_delivery':                    # 预付30 % 尾款款到发货
                product_delay = 0.0
                percentage = 0.3
                ######加上日期判断 减去已开票
                # 采购日期
                purchase_order = self.date_order+" 00:00:00"
                # 交货天数
                for supplier in line.product_id.seller_ids:
                    if line.partner_id.name == supplier.name.name:
                        product_delay = supplier.delay
                if product_delay == 0.0:
                    product_delay = 0
                # 物流天数
                logistics_days = self.partner_id.trans_day
                # 获取生成真实付款单时间 （采购日期+交货天数-物流天数）
                realDate = (datetime.datetime.strptime(purchase_order, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(days=product_delay) - datetime.timedelta(days=logistics_days)).strftime('%Y-%m-%d')
                nowDate = (datetime.datetime.strptime(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), '%Y-%m-%d %H:%M:%S') + datetime.timedelta(days=delay)).strftime('%Y-%m-%d')
                if nowDate < realDate:
                    qty = line.price_subtotal*percentage - line.qty_invoiced
                elif nowDate >= realDate:
                    qty = line.price_subtotal - line.qty_invoiced
                else:
                    raise UserError('特殊情况，请检查代码！')
            elif self.pay_way == 'pay_delivery_pay':                            # 预付30 % 尾款货到付款
                percentage = 0.3
                qty = (line.price_subtotal * percentage) - line.qty_invoiced
                # 如果接收数量为0的话，支付30%
                if line.qty_received <= 0:
                    qty = qty
                else:
                    # 如果接收的数量大于0
                    qty += line.qty_received*(1-percentage) * line.price_unit
            elif self.pay_way == 'delivery_then_pay':                           # 货到付款
                if line.qty_received > 0:
                    qty = (line.qty_received * line.price_subtotal/line.product_qty) - line.qty_invoiced
                else:
                    qty = 0
            elif self.pay_way == 'period_pay' or self.pay_way == 'ali_period_pay':              # 账期
                ######获取最后到货日期
                last_day = '0'
                for move in line.move_ids:
                    last_day = max(move.write_date or last_day,last_day)
                if last_day == '0':
                    last_day = '2088-12-31 12:00:00'
                # 获取生成真实付款单时间 （采购日期+交货天数-物流天数）
                realPayDate = (datetime.datetime.strptime(last_day, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(days=self.partner_id.account_period)).strftime('%Y-%m-%d %H:%M:%S')
                # 当前时间
                nowDate = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
                # 当前时间大于应付时间，则全部支付
                if nowDate >= realPayDate and line.product_qty == line.qty_received:
                    qty = (line.product_qty * line.price_unit) - line.qty_invoiced
                else:
                    qty = 0
            elif self.pay_way == 'pay_to_delivery':                   # 款到发货
                purchase_order = self.date_order+" 00:00:00"
                # 交货天数
                for supplier in line.product_id.seller_ids:
                    if line.partner_id.name == supplier.name.name:
                        product_delay = supplier.delay
                if product_delay == 0.0:
                    product_delay = 0
                    # 物流天数
                logistics_days = self.partner_id.trans_day
                # 获取生成真实付款单时间 （采购日期+交货天数-物流天数）
                realDate = (datetime.datetime.strptime(purchase_order, '%Y-%m-%d %H:%M:%S') + datetime.timedelta(days=product_delay) - datetime.timedelta(days=logistics_days)).strftime('%Y-%m-%d')
                nowDate = (datetime.datetime.strptime(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), '%Y-%m-%d %H:%M:%S') + datetime.timedelta(days=delay)).strftime('%Y-%m-%d')
                if nowDate >= realDate:
                    qty = line.price_subtotal - line.qty_invoiced
                else:
                    qty = 0
            else:
                raise UserError(self.pay_way+'没有这种付款方式！')

            if float_compare(qty, 0.0, precision_rounding=line.product_uom.rounding) <= 0:
                qty = 0.0
            taxes = line.taxes_id
            invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
            invoice_line = self.env['account.invoice.line']
            many2many_tax_ids = [(4, x) for x in invoice_line_tax_ids.ids]
            data = {
                'purchase_line_id': line.id,
                'name': line.order_id.name+': '+line.name,
                'origin': line.order_id.origin,
                'uom_id': line.product_uom.id,
                'product_id': line.product_id.id,
                'price_unit': 1,
                'quantity': qty,
                'discount': 0.0,
                'account_analytic_id': line.account_analytic_id.id,
                'analytic_tag_ids': line.analytic_tag_ids.ids,
                'invoice_line_tax_ids': many2many_tax_ids

            }
            account = invoice_line.get_invoice_line_account('in_invoice', line.product_id, line.order_id.fiscal_position_id, self.env.user.company_id)
            if account:
                data['account_id'] = account.id
        else:
            raise UserError('没有付款方式，请检查付款方式！')
        return data

    # 定时任务每5分钟跑一次，主要处理采购付款申请单是待采购审核，
    # 但是对应的元付款申请单却有可能是待财务审核或者采购经理审核的问题
    def update_invoice_state(self):
        sql = """select ai.id from purchase_pay pp inner join account_invoice ai on ai.purchase_pay_id = pp.id where pp.audit_state = 'first' and ai.audit_state in('third','second','fourth')"""
        self._cr.execute(sql)
        fet = self._cr.fetchall()
        if fet:
            for param in fet:
                account_invoice_result = self.env['account.invoice'].search([('id', '=', param[0])]);
                if account_invoice_result:
                    account_invoice_result.write({'audit_state': 'first'})
        return True

class purchase_order_line(models.Model):
    _inherit = 'purchase.order.line'
    ##########订单明细批量关闭
    @api.multi
    def oe_myservice_1(self):
        for each in self:
            each.button_close()
        return True

    #  采购订单明细关闭 调用SRM接口【采购订单行关闭】
    def button_close(self):
        if self.open_state == 'close':
            raise UserError('采购订单【%s】产品【%s】已经关闭,不能关闭订单行'%(self.order_id.name,self.product_id.name))
        # 更新列表的状态为关闭
        self.write({'open_state': "close"})

        # 判断此订单行对应的状态，如果不是“取消或者已支付”状态的，都不允许关闭订单行
        for invoice in self.invoice_lines:
            if invoice.invoice_id.state in ('draft', 'open'):
                audit_state = ''
                if invoice.invoice_id.state == "draft":
                    raise UserError('有付款单没有支付或取消！')
                elif invoice.invoice_id.state == "open":
                    audit_state = "打开"
                raise UserError('此明细行是  '+ audit_state + '  状态,无法进行审核操作！')

        # 订单状态不是已经审核
        if self.order_id.state not in ['purchase', 'done']:
            raise UserError('采购订单【%s】没有确认订单，不能关闭'%(self.order_id.name))
        if round(self.qty_received * self.price_subtotal/self.product_qty, 3) != round(self.qty_paid, 3):
            raise UserError('采购订单【%s】产品【%s】的已付款金额【%s】与已入库金额(已接收数量*单价)【%s】不等,不能关闭订单行'%(self.order_id.name,self.product_id.name,self.qty_paid,self.qty_received*self.price_unit))
        dict = {}
        # 采购订单单号
        dict['PONO'] = self.order_id.name
        # 采购订单行号
        dict['POLineNo'] = self.id
        # 取消原因
        dict['Reason'] = self.order_id.name
        xmldata = {'data': {'ordernos': dict}}
        if "Y" == self.env['ir.config_parameter'].sudo().get_param('interface_process_Y'):
            result = self.env['send.controller'].dispatch('closePOLine', xmldata)
            if result.get("Response").get("return").get("returnFlag") == "0":
                raise UserError('订单行关闭调用SRM接口失败')
            else:
                self.write({'open_state': 'close'})
        return True

    @api.constrains('qty_invoiced')
    def _check_total(self):
        for each in self:
            # 检验付款申请总金额小于订单行的小计
            total = sum([res['price_subtotal'] if res.invoice_id.type == 'in_invoice' else -res['price_subtotal']
                         for res in each.invoice_lines if res.invoice_id.state not in ['cancel']])
            if round(total, 3) > round(each.price_subtotal, 3):
                raise UserError(("产品【%s】付款申请单总金额%s大于采购订单明细总计%s"%(each.product_id.default_code,total,each.price_subtotal)))
            if round(total, 3) < 0:
                raise UserError(("产品【%s】付款申请单总金额%s小于0" % (each.product_id.default_code,total, )))