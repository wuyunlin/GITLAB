# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models
from odoo.exceptions import UserError
import xlrd
import base64
import xlwt
import datetime
from xlrd import xldate_as_tuple
class purchase_requirement(models.Model):
    _name = 'purchase.requirement'
    _description = u'通过Excel导入生成采购订单'

    id = fields.Integer()
    import_date = fields.Date(u'导入日期')
    import_datetime = fields.Date(u'导入时间')
    warehouse = fields.Char(u'收货人')
    sku = fields.Char(u'产品SKU')
    qty = fields.Integer(u'产品数量')
    supplier = fields.Char(u'供应商')
    price = fields.Float(u'单价')
    responser = fields.Char(u'负责人')
    create_name = fields.Char(u'制单人')
    delay_days = fields.Integer(u'交货天数')
    tax_no = fields.Char(u'金融单号')
    state = fields.Selection([
        ('1.0', u'已导入数据'),
        ('2.0', u'修改基础信息成功'),
        ('2.1', u'修改基础信息失败'),
        ('3.0', u'分配预订单号成功'),
        ('3.1', u'分配预订单号失败'),
        ('4.0', u'数量合并成功'),
        ('4.1', u'数量合并失败'),
        ('5.0', u'生成询价单成功'),
        ('5.1', u'生成询价单失败'),
    ], string=u'状态')
    product_id = fields.Integer(u'产品ID')
    partner_id = fields.Integer(u'供应商ID')
    product_qty = fields.Integer(u'产品数量')
    oig_receiver_id = fields.Integer(u'收货人ID')
    name = fields.Char(u'采购申请单号')
    purchase_order_id = fields.Integer(u'采购订单ID')
    is_active = fields.Selection([('N', u'未被合并'), ('Y', u'已被合并')], string=u'是否被合并', default='N')
    error = fields.Text(u'错误信息')
    dict = fields.Text(u'创建采购订单的字典')
    yd_order = fields.Char(u'预定单号')
    file = fields.Binary(string=u'导入文件', filters='*.xls')

    # 第一步 Excel数据处理将收货人、供应商、产品转换成ID
    def do_data(self):
        sql = """select
                id,sku,warehouse,
                supplier,price,qty,
                responser,create_name,delay_days,
                tax_no,state,import_date,
                import_datetime,product_id,oig_receiver_id,
                partner_id,product_qty,name,
                purchase_order_id,is_active,error
                from purchase_requirement where state = '1.0'"""
        self._cr.execute(sql)
        fet = self._cr.dictfetchall()

        for params in fet:
            try:
                err_msg = ''
                res_partner = self.env['res.partner'].search([('name', '=', params['supplier'].strip()), ('type_flag', '=', 'supplier')])
                product_product = self.env['product.product'].search([('default_code', '=', params['sku'].strip())])
                oig_receiver = self.env['oig.receiver'].search([('name', '=', params['warehouse'].strip())])

                # 更新供应商ID
                if res_partner:
                    self.env['purchase.requirement'].browse(params['id']).write({'partner_id': res_partner[0].id})
                else:
                    err_msg += params['supplier']+"，此供应商没有查到！"

                # 更新产品
                if product_product:
                    self.env['purchase.requirement'].browse(params['id']).write({'product_id': product_product.id})
                    # 处理供应商的交货天数的问题
                    if res_partner:
                        product_supplierinfo = self.env['product.supplierinfo'].search([('name', '=', res_partner[0].id), ('product_tmpl_id', '=', product_product.product_tmpl_id.id)])
                        # 如果有供应商信息则更新交货天数
                        if product_supplierinfo:
                            if product_supplierinfo.delay != int(params['delay_days']):
                                product_supplierinfo.write({'delay': params['delay_days']})
                                # product_supplierinfo.product_tmpl_id.issued_product()
                            else:
                                pass
                        else:
                            product_template = product_product.product_tmpl_id
                            # 没有供应商信息则创建供应商信息
                            supplier_dict = {}
                            supplier_dict['delay'] = params['delay_days']
                            supplier_dict['name'] = res_partner[0].id
                            supplier_dict['product_name'] = product_template.name
                            supplier_dict['product_code'] = product_template.default_code
                            supplier_dict['product_tmpl_id'] = product_template.id
                            self.env['product.supplierinfo'].create(supplier_dict)
                    else:
                        pass
                else:
                    err_msg += params['sku']+"，此产品没有查到！"

                # 更新收货人
                if oig_receiver:
                    self.env['purchase.requirement'].browse(params['id']).write({'oig_receiver_id': oig_receiver.id})
                else:
                    err_msg += params['warehouse']+"，此收货人没有查到！"

                # err_msg为空，则标明更新产品、收货人、供应商ID没有错误信息,否则则记录错误信息
                purchase_requirement = self.env['purchase.requirement'].browse(params['id'])
                if err_msg == '':
                    purchase_requirement.write({'state': '2.0'})
                else:
                    purchase_requirement.write({'state': '2.1', 'error': err_msg})
                    continue
            except Exception as e:
                self.env['purchase.requirement'].browse(params['id']).write({'state': '2.1', 'error': "出现异常"+e.message})
        return True

    # 第二步 按照 供应商+收货人+负责人+制单人+金融单号 维度 进行合并订单,并写入预定单号
    def join_order(self):
        sql = """with res AS(
                    SELECT
                        warehouse,
                        supplier,
                        responser,
                        create_name,
                        tax_no,
                        string_agg(cast(id as varchar), '-') as pre_order,
                        CEIL (delay_days / 5.0) as delay_day
                    FROM
                        purchase_requirement
                    WHERE
                        STATE = '2.0'
                    GROUP BY
                        supplier,
                        warehouse,
                        responser,
                        create_name,
                        tax_no,
                        CEIL (delay_days / 5.0)
                    )
                    update purchase_requirement  SET yd_order = res.pre_order,state = '3.0' from res where 
                    res.warehouse = purchase_requirement.warehouse and 
                    res.supplier = purchase_requirement.supplier AND
                    res.responser = purchase_requirement.responser AND
                    res.create_name = purchase_requirement.create_name AND 
                    res.delay_day = CEIL(purchase_requirement.delay_days/5.0) AND 
                    res.tax_no = purchase_requirement.tax_no AND 
                    purchase_requirement."state" = '2.0'"""
        self._cr.execute(sql)
        return True

    # 第三步 处于一个预订单号内的SKU有多条，进行数量合并
    def join_qty(self):
        sql = """WITH res AS (
                SELECT
                    sku,yd_order,
                    SUM (cast(qty as INTEGER)) qty,
                    MAX (ID) ID,
                    tax_no
                FROM purchase_requirement where state = '3.0' GROUP BY sku,yd_order,tax_no
            ) UPDATE purchase_requirement
            SET product_qty = res.qty,STATE = '4.0',is_active='Y'
            FROM res WHERE res. ID = purchase_requirement. ID and purchase_requirement.state = '3.0'"""
        self._cr.execute(sql)
        return True

    # 第四步 组装字典创建询价单
    def create_purchase_order(self):
        now = datetime.datetime.now() .strftime("%Y-%m-%d")
        sql = """select partner_id,create_name,responser,oig_receiver_id,yd_order,tax_no
                  FROM purchase_requirement where state = '4.0' and product_qty is not null
                  GROUP BY partner_id,create_name,responser,oig_receiver_id,yd_order,tax_no"""
        self._cr.execute(sql)
        fet = self._cr.dictfetchall()
        # 循环创建采购订单主表
        for each in fet:
            try:
                res_partner = self.env['res.partner'].search([('id', '=', each['partner_id'])])
                purchase = {}
                # 供应商
                purchase['partner_id'] = each['partner_id']
                # 订单状态为询价单
                purchase['state'] = 'draft'
                purchase['date_planned'] = now
                # 制单人
                purchase['create_name'] = each['create_name']
                # 单据日期
                purchase['date_order'] = now
                purchase['open_state'] = 'open'
                # 负责人
                purchase['responser'] = each['responser']
                # 收货人
                purchase['oig_receiver_id'] = each['oig_receiver_id']
                # 付款类型
                purchase['pay_type'] = res_partner.pay_type
                # 采购订单下发状态
                purchase['issued_state'] = 'N'
                purchase['tax_no'] = each['tax_no']
                purchase_order_result = self.env['purchase.order'].create(purchase)
                # 查询明细
                line_sql = """select id,sku,warehouse,
                            supplier,price,qty,
                            responser,create_name,delay_days,
                            tax_no,state,import_date,
                            import_datetime,product_id,oig_receiver_id,
                            partner_id,product_qty,name,
                            purchase_order_id,is_active,error,yd_order            
                    from purchase_requirement where state = '4.0' and product_qty is not null and yd_order = '%s'"""%(each['yd_order'])
                self._cr.execute(line_sql)
                line_result = self._cr.dictfetchall()
                if len(line_result) > 0:
                    pass
                else:
                    purchase_order_result.unlink()
                # 循环生成采购订单明细
                for line in line_result:
                    purchase_line = {}
                    # 单位
                    purchase_line['product_uom'] = 1
                    # 产品单价
                    purchase_line['price_unit'] = line['price']
                    # 产品数量
                    purchase_line['product_qty'] = line['product_qty']
                    # 供应商
                    purchase_line['partner_id'] = line['partner_id']
                    # 已接收数量
                    purchase_line['qty_received'] = 0
                    # 状态
                    purchase_line['state'] = 'draft'
                    # 待定
                    purchase_line['order_id'] = purchase_order_result.id
                    # 产品Id
                    purchase_line['product_id'] = line['product_id']
                    # 申请单号
                    purchase_line['purchase_interid'] = "POREQ"+str(line['id'])
                    # 质检要求
                    purchase_line['quality_inspection'] = '无'
                    # 包装要求
                    purchase_line['packing_instruction'] = '无'
                    # 备注
                    purchase_line['remark'] = '无'
                    # 说明（产品中文名称）
                    product_product = self.env['product.product'].browse(line['product_id'])
                    if product_product:
                        product_template = product_product.product_tmpl_id
                        if product_template:
                            purchase_line['name'] = product_template.name
                            # 质检要求
                            purchase_line['quality_inspection'] = product_template.quality_inspection
                            # 包装要求
                            purchase_line['packing_instruction'] = product_template.packing_instruction
                            # 备注
                            purchase_line['remark'] = product_template.description_purchase
                        else:
                            purchase_line['name'] = ''
                    else:
                        purchase_line['name'] = ''
                    # 计算预定交货日期 （单据日期+交货天数）
                    date_time = datetime.datetime.strptime(line['import_date']+" 00:00:00", '%Y-%m-%d %H:%M:%S') + datetime.timedelta(days=line['delay_days'])
                    # 预定交货日期
                    purchase_line['date_planned'] = date_time
                    # 订单行打开状态
                    purchase_line['open_state'] = 'open'
                    # 采购预计到货日期
                    purchase_line['purchase_date_planned'] = date_time
                    # 是否提样
                    purchase_line['is_sample'] = 'N'
                    purchase_order_line = self.env['purchase.order.line'].create(purchase_line)

                    if purchase_order_line:
                        # 创建小组库存
                        line_platform = {}
                        line_platform['sys_platform_id'] = self.env['ir.config_parameter'].sudo().get_param('line_platform')
                        line_platform['qty'] = purchase_order_line.product_qty
                        line_platform['purchase_order_line_id'] = purchase_order_line.id
                        line_platform['qty_origin'] = purchase_order_line.product_qty
                        self.env['line.platform'].create(line_platform)
                        # 更新基础信息表中的状态
                        self.env['purchase.requirement'].browse(line['id']).write({'state': '5.0', 'purchase_order_id': purchase_order_line.order_id.id,
                                                                                   'name': purchase_order_line.order_id.name, 'dict': purchase_line})
                    else:
                        self.env['purchase.requirement'].browse(line['id']).write({'state': '5.0'})
                    self.invalidate_cache()
                    self._cr.commit()
            except Exception as e:
                if purchase_order_result:
                    purchase_order_result.write({'state': 'cancel'})
                    self.env['purchase.requirement'].browse(line['id']).write({'state': '5.1', 'error': "异常信息："+e.message})
                    purchase_order_result.unlink()
                    self._cr.commit()
                else:
                    self.invalidate_cache()
                    self._cr.commit()
                    self.env['purchase.requirement'].browse(line['id']).write({'state': '5.1', 'error': "异常信息："+e.message})
                    continue

    # 定时任务处理生成采购订单
    def auto_create_purchase_order(self):
        # Excel数据处理将收货人、供应商、产品转换成ID
        self.do_data()
        self._cr.commit()
        # 按照 供应商+收货人+负责人+制单人+金融单号 维度 进行合并订单,并写入预定单号
        self.join_order()
        self._cr.commit()
        # 处于一个预订单号内的SKU有多条，进行数量合并
        self.join_qty()
        self._cr.commit()
        # 组装字典创建采购订单
        self.create_purchase_order()
        self._cr.commit()

    # 导入Excel
    def excel_purchase_requirement(self):
        # 处理添加空数据的问题
        def del_data():
            sql="""select id from purchase_requirement where sku is null and supplier is null and warehouse is NULL and product_id is NULL"""
            self._cr.execute(sql)
            result = self._cr.dictfetchall()
            for each in result:
                del_sql = """delete from purchase_requirement where id = '%s'"""%(each['id'])
                self._cr.execute(del_sql)
            return True

        # 处理插入数据
        try:
            excel = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
        except:
            raise UserError('请选择导入文件!')
        del_data()
        sh = excel.sheet_by_index(0)
        dateFormat = xlwt.XFStyle()
        dateFormat.num_format_str = 'yyyy-mm-dd'
        # 接收xlsx导入的产品
        for rx in range(sh.nrows):
            try:
                if rx == 0:
                    continue
                else:
                    data = [str(sh.cell(rx, i).value or '') for i in range(10)]
                    purchase_requirement = {}
                    purchase_requirement['sku'] = data[0]
                    if data[1][len(data[1])-2:len(data[1])-1] == '.':
                        purchase_requirement['qty'] = int(data[1][0:-2])
                    else:
                        purchase_requirement['qty'] = data[1]
                    purchase_requirement['supplier'] = data[2]
                    purchase_requirement['price'] = float(data[3])
                    purchase_requirement['warehouse'] = data[4]
                    if data[5][len(data[5])-2:len(data[5])-1] == '.':
                        purchase_requirement['delay_days'] = int(data[5][0:-2])
                    else:
                        purchase_requirement['delay_days'] = data[5]
                    purchase_requirement['responser'] = data[6]
                    purchase_requirement['create_name'] = data[7]
                    purchase_requirement['import_date'] = datetime.datetime(*xldate_as_tuple(int(data[8][:-2]), 0))
                    purchase_requirement['tax_no'] = data[9]
                    purchase_requirement['state'] = '1.0'
                    self.env['purchase.requirement'].create(purchase_requirement)
            except:
                continue
        return {
            'type': 'ir.actions.act_window',
            'name': u'导入Excel数据',
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'purchase.requirement',
            'target': 'current',
            'view_id': False,
        }

    # 预览导入信息列表页
    def show_purchase_requirement(self):
        sql="""select id from purchase_requirement where sku is null and supplier is null and warehouse is NULL and product_id is NULL"""
        self._cr.execute(sql)
        result = self._cr.dictfetchall()
        for each in result:
            del_sql = """delete from purchase_requirement where id = '%s'"""%(each['id'])
            self._cr.execute(del_sql)
        return {
            'type': 'ir.actions.act_window',
            'name': u'导入Excel数据',
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'purchase.requirement',
            'target': 'current',
            'view_id': False,
        }

