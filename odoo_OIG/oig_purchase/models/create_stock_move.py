# -*- coding: utf-8 -*-
from odoo import api, fields, models
import datetime
class create_stock_move(models.Model):
    _name = 'create.stock.move'
    _description = u'创建月度出入库凭证'

    begin_time = fields.Date(u'开始时间')
    end_time = fields.Date(u'结束时间')
    supplier_ru = fields.Many2one('account.move', u'外购入库单凭证')
    other_ru = fields.Many2one('account.move', u'其他入库凭证')
    pk = fields.Many2one('account.move', u'盘亏')
    fh_cancel_ru = fields.Many2one('account.move', u'发货取消凭证')
    customer_ru = fields.Many2one('account.move', u'成本调整凭证')
    supplier_chu = fields.Many2one('account.move', u'销售出库凭证')
    other_chu_zi = fields.Many2one('account.move', u'其他仓出库凭证(中国自用库)')
    other_chu_other = fields.Many2one('account.move', u'其他仓出库凭证(除中国自用库外)')
    py = fields.Many2one('account.move', u'盘盈')
    cx_chu = fields.Many2one('account.move', u'采销凭证')
    state = fields.Selection([
        ('N', u'未执行'),
        ('Y', u'已执行'),
    ], readonly=True, string='状态', default='N')


    def create_pz(self):
        sql = """select * from report.monthly_crk_report_func('%s', '%s')"""%(self.begin_time, self.end_time)
        self._cr.execute(sql)
        fet = self._cr.fetchall()
        dict = {}
        # 库存商品科目(1405)
        stock_goods = self.env['ir.config_parameter'].sudo().get_param('stock_goods')
        # 应付账款科目(2202)
        bank_savings = self.env['ir.config_parameter'].sudo().get_param('bank_savings')
        # 盘亏科目贷方(6602.49)
        pk_account = self.env['ir.config_parameter'].sudo().get_param('pk_account')
        # 供应链中转(1406.07)
        supply_chain_transfer = self.env['ir.config_parameter'].sudo().get_param('supply_chain_transfer')
        # 销售商品成本(6401.01)
        difference = self.env['ir.config_parameter'].sudo().get_param('difference')
        # 采销科目(1221.01.14)
        sales_orders_account = self.env['ir.config_parameter'].sudo().get_param('sales_orders_account')
        # 暂估应付中转(2204.99)
        pay_transit = self.env['ir.config_parameter'].sudo().get_param('pay_transit')
        # 包装费科目(6601.17)
        packing_account = self.env['ir.config_parameter'].sudo().get_param('packing_account')
        for param in fet:
            if param[3]:
                # 外购入库单凭证 (借：库存商品(1405)  贷：应付账款(2202))
                if param[0] == 1 and param[1] == '供应商':
                    supplier_sql = """select 类型,sum(金额) from report.daily_crk_view where 类型 = '供应商' and 时间 >= '%s' and 时间 <= '%s' GROUP BY 类型"""%(self.begin_time, self.end_time)
                    self._cr.execute(supplier_sql)
                    fet = self._cr.fetchone()
                    self.create_account_move(dict, fet[1], param[1], 'supplier_ru', stock_goods, bank_savings)
                    continue
                # 其他入库凭证(借：库存商品(1405)  贷：供应链中转(1406.07))
                elif param[0] == 1 and param[1] == '其他仓':
                    self.create_account_move(dict, param[3], param[1], 'other_ru', stock_goods, supply_chain_transfer)
                    continue
                # 盘亏(借：管理费用-盘亏损失(6602.49) 贷：库存商品(1405))
                elif param[0] == 1 and param[1] == '盘点仓':
                    self.create_account_move(dict, param[3], param[1], 'pk', pk_account, stock_goods)
                    continue
                # 发货取消凭证（借：库存商品(1405)    贷：销售商品成本(6401.01)）
                elif param[0] == 1 and param[1] == '发货取消仓':
                    self.create_account_move(dict, param[3], param[1], 'fh_cancel_ru', stock_goods, difference)
                    continue
                # 成本调整单（借：库存商品(1405)   贷：暂估应付中转(2204.99)）
                elif param[0] == 1 and param[1] == '客户':
                    cost_sql = """select sum(运费)+sum(vat) as all_money from report.account_vat_view where 日期 >='%s' and 日期<='%s'"""%(self.begin_time, self.end_time)
                    self._cr.execute(cost_sql)
                    fet = self._cr.fetchone()
                    self.create_account_move(dict, fet[0], param[1], 'customer_ru', stock_goods, pay_transit)
                    continue
                # 销售出库凭证(借：销售商品成本(6401.01)    贷：库存商品(1405))
                elif param[0] == -1 and param[1] == '供应商':
                    self.create_account_move(dict, param[3], param[1], 'supplier_chu', difference, stock_goods)
                    continue
                # 其他出库凭证(1.中国自用仓生成：借：包装费(6601.17)     贷：库存商品(1405)
                #              2.其他仓库生成：  借: 供应链中转(1406.07) 贷：库存商品(1405))
                elif param[0] == -1 and param[1] == '其他仓':

                    # 中国自用库数据
                    other_sql = """select sum(金额) from report.daily_crk_view where 目标库位 = '中国自用仓库' and 时间 >='%s' and 时间 <= '%s'"""%(self.begin_time, self.end_time)
                    self._cr.execute(other_sql)
                    fet_zi = self._cr.fetchone()
                    self.create_account_move(dict, fet_zi[0], param[1], 'other_chu_zi', packing_account, stock_goods)


                    # 其他仓库数据
                    other_sql = """select sum(金额) from report.daily_crk_view where 目标库位 != '中国自用仓库' and 时间 >='%s' and 时间 <= '%s'"""%(self.begin_time, self.end_time)
                    self._cr.execute(other_sql)
                    fet_other = self._cr.fetchone()
                    self.create_account_move(dict, fet_other[0], param[1], 'other_chu_other', supply_chain_transfer, stock_goods)

                    continue
                # 盘盈(借：库存商品(1405)   贷：管理费用-盘亏损失(6602.49))
                elif param[0] == -1 and param[1] == '盘点仓':
                    self.create_account_move(dict, param[3], param[1], 'py', stock_goods, pk_account)
                    continue
                # 采销(借：采销(1221.01.14)  贷：库存商品(1405))
                elif param[0] == -1 and param[1] == '采销仓':
                    self.create_account_move(dict, param[3], param[1], 'cx_chu', sales_orders_account, stock_goods)
                    continue
                else:
                    pass
            else:
                pass
        dict['begin_time'] = self.begin_time
        dict['end_time'] = self.end_time
        dict['state'] = 'Y'
        result = self.env['create.stock.move'].create(dict)
        return {
            'type': 'ir.actions.act_window',
            'name': u'月度凭证',
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'create.stock.move',
            'target': 'current',
            'view_id': False,
        }

    # 创建凭证代码
    def create_account_move(self, dict, money, name, dict_name, account_debit, account_credit):
        account_move_lines = []
        nowDate = datetime.datetime.now() .strftime("%Y-%m-%d")

        # 借方科目
        account_account_debit = self.env['account.account'].search([('code', '=', account_debit)])

        # 贷方科目
        account_account_credit = self.env['account.account'].search([('code', '=', account_credit)])

        # 凭证借方
        account_invoice_line_debit = {'account_id': account_account_debit.id, 'debit': money, 'credit': 0, 'name': account_debit + name +"(借方)"}
        account_move_lines.append((0, 0, account_invoice_line_debit))

        # 凭证贷方
        account_invoice_credit = {'account_id': account_account_credit.id, 'debit': 0, 'credit': money, 'name': account_credit + name+"(贷方)"}
        account_move_lines.append((0, 0, account_invoice_credit))

        if account_move_lines:
            account_move_result = self.env['account.move'].create({'journal_id': int(self.env['ir.config_parameter'].sudo().get_param('journal_id')), 'date': nowDate, 'ref': money, 'line_ids': account_move_lines})
            if account_move_result:
                dict[dict_name] = account_move_result.id
            else:
                pass
        else:
            pass
        return True
