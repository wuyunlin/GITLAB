# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp
import datetime
SPECIAL_REASON=['改高金额特批； ', '手工建单特批； ', '付款金额大于5千且交货天数大于15天； ', '已经预付金额加上当前审批金额大于10万； ']
class account_invoice(models.Model):
    _inherit = 'account.invoice'
    # 创建方式（手动创建、自动创建）
    #####延期的账单 晚上跑批不删除
    flag = fields.Selection([
        ('manual', u'手动'),
        ('auto', u'自动'),
        ('delay', u'延期'),
    ], string=u'创建方式',track_visibility='onchange')
    purchase_order_id = fields.Many2one('purchase.order', u'采购单')
    audit_state = fields.Selection([
        ('first', u'待采购审批'),
        ('second', u'待采购经理审批'),
        ('third', u'待财务审批'),
        ('fourth', u'审核完成'),
    ], string=u'审核状态', default='first', track_visibility='onchange')
    pay_account = fields.Char(u'付款账户')
    account_name = fields.Char(u'户名')
    bank_information = fields.Char(u'开户行信息')
    special_flag = fields.Char(u'特批标识')
    special_reason = fields.Char(u'需要特批的缘由')
    account_invoice_state = fields.Selection([
        ('prepayments', u'预付款申请单'),
        ('payment_requisition', u'付款申请单'),
        ('refund_requisition', u'退款申请单'),
    ], string=u'付款申请单类型')
    supplier_no = fields.Char(u'付款申请单号')
    cost = fields.Float(u'运费', digit=(16,2))
    pay_type = fields.Char(u'付款方式')
    pay_number = fields.Char(u'付款流水号')
    cost_remark = fields.Char(u'运费备注')
    website = fields.Char(u'账户网址')
    # 外键 关联采购付款申请单
    purchase_pay_id = fields.Many2one("purchase.pay", u'采购付款申请单')
    responser = fields.Char(u'负责人')

    # 采购审批
    def purchase_audit(self):
        audit_state = 'third'
        # 增加原因判断，如果特批原因不为空，则设置为 “待采购经理审批”
        reason = self.special_reason
        if len(reason or ''):
            audit_state = 'second'

        # 任意一个供应商的预付款不得超过10万人民币（包含定金的预付：付款-到货金额）
        amount_paid = self.purchase_order_id.partner_id._purchase_all_money()
        approve_to_paid = self.amount_total or 0
        if amount_paid+approve_to_paid > float(self.env['ir.config_parameter'].sudo().get_param('Money_ten')):
            audit_state = 'second'
            if SPECIAL_REASON[3] not in (self.special_reason or ''):
                if self.special_reason:
                    self.write({'special_reason': self.special_reason + ' , ' + SPECIAL_REASON[3]})
                else:
                    self.write({'special_reason': SPECIAL_REASON[3]})

        # 采购审核状态为采购审批
        if self.audit_state == 'first':
            #####默认进入财务审批 如果是手工生成的进入采购经理审批
            if self.flag == 'manual':
                audit_state = 'second'
                if SPECIAL_REASON[1] not in (self.special_reason or ''):
                    if not self.special_reason:
                        reason = SPECIAL_REASON[1]
                    else:
                        reason = self.special_reason + SPECIAL_REASON[1]
                    self.write({'special_reason': reason})

            # 判断如果付款方式为公账，则要维护公账账户
            if self.purchase_order_id.pay_type == "public":
                if not self.partner_id.pay_account:
                    raise UserError(self.purchase_order_id[0].name + '付款方式为公账，但没有维护公账账户！')

            # 交货期交货金额必须小于5千，交货天数要小于15天
            if len(self.invoice_line_ids) > 0:
                if self.invoice_line_ids[0].purchase_id.pay_way == "all":
                    for invoice in self.invoice_line_ids:
                        for seller in invoice.product_id.seller_ids:
                            if self.partner_id.name == seller.name.name:
                                if seller.delay >= 15 and (self.amount_total > float(self.env['ir.config_parameter'].sudo().get_param('Money'))):
                                    audit_state = 'second'
                                    if SPECIAL_REASON[2] not in (self.special_reason or ''):
                                        if not self.special_reason:
                                            reason = SPECIAL_REASON[2]
                                        else:
                                            reason = self.special_reason + SPECIAL_REASON[2]
                                        self.write({'special_reason': reason})
            else:
                raise UserError('付款申请单没有明细行！')

            # 判断是否存在明细行
            if len(self.invoice_line_ids) > 0:
                # 在付款类型为私账的情况下，付款申请单金额超过一万，判断此采购订单必须有采购合同确认是否还有私账支付
                if self.invoice_line_ids[0].purchase_id.pay_type == "private":
                    # 如果金额大于1万，并且采购合同为空
                    if self.amount_total > float(self.env['ir.config_parameter'].sudo().get_param('Money')) and (not self.purchase_order_id[0].contracts):
                        raise UserError("采购付款申请单“"+ self.purchase_pay_id.supplier_no +"”中的采购订单“" + self.purchase_order_id.name + '”是私账付款：付款金额大于5千但是采购订单没有维护采购合同，请到采购订单中维护采购合同！')

                    # 判断私账情况下 付款账号、开户行信息、户名、营业执照、收款人身份证、账号盖章、身份证验证结果 不能为空
                    if not self.partner_id.pay_account:
                        raise UserError(self.purchase_order_id.name + '付款账号不能为空！')
                    elif not self.partner_id.bank_information:
                        raise UserError(self.purchase_order_id.name + '开户行信息不能为空！')
                    elif not self.partner_id.account_name:
                        raise UserError(self.purchase_order_id.name + '户名不能为空！')
                    elif not self.partner_id.business_licence:
                        raise UserError(self.purchase_order_id.name + '营业执照不能为空！')
                    elif not self.partner_id.idcard:
                        raise UserError(self.purchase_order_id.name + '收款人身份证不能为空！')
                    elif not self.partner_id.seal:
                        raise UserError(self.purchase_order_id.name + '账号盖章不能为空！')
                    elif not self.partner_id.idcard_validate:
                        raise UserError(self.purchase_order_id.name + '身份证验证结果不能为空！')

                    # 判断供应商账户中是否有值(判断是否还有私账支付	)
                    if not self.partner_id.pay_account:
                        raise UserError(self.purchase_order_id.name + '当前付款申请单为私账付款，供应商资料中没有维护私账账户！')
            else:
                raise UserError(self.purchase_order_id[0].name + '订单没有明细行')
            self.write({'audit_state': audit_state})
        else:
            raise UserError(self.purchase_order_id.name + '当前付款申请单不处于待采购审批阶段，不能进行采购审批！')

    # 采购经理进行审批
    def purchase_manager_audit(self):
        if self.audit_state == "second":
            self.write({'audit_state': "third"})
        else:
            raise UserError('当前账号不允许审核此单！')

    # 财务审批 （审批完成）
    def finance_audit(self):
        if self.audit_state == "third":
            self.write({'audit_state': "fourth"})
        else:
            raise UserError('当前账号不允许审核此单！')

    # 回退功能
    def go_back(self):
        if self.audit_state in ("second", "third", "fourth"):
            self.write({'audit_state': "first"})
            self.write({'special_reason': ""})
        else:
            raise UserError("当前状态不能进行回退操作！")

    @api.model
    def create(self, vals):
        if vals.get('supplier_no', 'New') == 'New':
            vals['supplier_no'] = self.env['ir.sequence'].next_by_code('account.invoice') or '/'
        res = super(account_invoice, self).create(vals)
        self.merge_pay_order(res)
        return res

    # 合并付款申请单
    def merge_pay_order(self, res):
        # 新增合并付款申请单(flag：自动或者手动、type：预付款单还是付款单、partner_id：供应商是否相同、date_due：付款日期是否相同，audit_state：审核状态是否相同、responser：负责人是否相同)
        resultAll = self.env['purchase.pay'].search([('flag', '=', res.flag), ('type', '=', res.type), ('partner_id', '=', res.partner_id.id), ('date_due', '=', res.date_invoice), ('audit_state', '=', res.audit_state), ('responser', '=', res.responser)])
        if resultAll:
            res.write({'purchase_pay_id': resultAll.id})
        else:
            dict = {}
            dict['flag'] = res.flag
            dict['purchase_order'] = res.purchase_order_id[0].name
            dict['pay_account'] = res.pay_account
            dict['account_name'] = res.account_name
            dict['bank_information'] = res.bank_information
            dict['special_flag'] = res.special_flag
            dict['special_reason'] = res.special_reason
            dict['account_invoice_state'] = res.account_invoice_state
            dict['supplier_no'] = self.env['ir.sequence'].next_by_code('purchase.pay') or '/'
            dict['pay_type'] = res.pay_type
            dict['pay_number'] = res.pay_number
            dict['website'] = res.website
            dict['type'] = res.type
            dict['date_due'] = res.date_invoice
            dict['partner_id'] = res.partner_id.id
            dict['responser'] = res.responser
            dict['pay_type'] = 'pbtccn'
            purchase_pay = self.env['purchase.pay'].create(dict)
            res.write({'purchase_pay_id': purchase_pay.id})

    @api.multi
    def unlink(self):
        for order in self:
            if order.audit_state != "first":
                raise UserError("当前状态不能进行删除操作，只有待采购审核才能删除！")
        return super(account_invoice, self).unlink()


    @api.multi
    def write(self, vals):
        res = super(account_invoice, self).write(vals)
        if vals.get('date_invoice'):
            self.merge_pay_order(self)
        return res


    # 验证付款申请单 并结合付款功能
    @api.one
    def verification(self):
        # 判断只有审核状态为审核完成，才可以进行付款操作
        purchase_pay_result = self.env['purchase.pay'].search([('id', '=', self.purchase_pay_id.id)])
        if self.audit_state == "fourth":
            if self.amount_total and not purchase_pay_result.cost:
                total = self.amount_total
            elif not self.amount_total and purchase_pay_result.cost:
                total = purchase_pay_result.cost
            elif self.amount_total and purchase_pay_result.cost:
                total = self.amount_total
            else:
                total = 0
            if total > 0:
                self.write({'state': 'paid'})
                pay_journal=self.env['account.journal'].search([('type', '=', 'bank')], limit=1)
                payment_type = self.type in ('out_invoice', 'in_refund') and 'inbound' or 'outbound'
                if payment_type == 'inbound':
                    payment_method = self.env.ref('account.account_payment_method_manual_in')
                    journal_payment_methods = pay_journal.inbound_payment_method_ids
                else:
                    payment_method = self.env.ref('account.account_payment_method_manual_out')
                    journal_payment_methods = pay_journal.outbound_payment_method_ids
                if payment_method not in journal_payment_methods:
                    raise UserError(('支付类型 %s 错误') % pay_journal.name)
            else:
                 raise UserError(self.purchase_pay_id.supplier_no+'当前付款申请单无需付款！')

        else:
            raise UserError(self.purchase_pay_id.supplier_no+'当前付款申请单未审核完成，无法进行付款！')
        return True

class account_invoice_line(models.Model):
    _inherit = 'account.invoice.line'

    quantity = fields.Float(string=u'金额', digits=dp.get_precision('Payment Terms'),copy=False,default=0)

    # 关联采购付款申请单主表    
    purchase_pay_id = fields.Many2one('purchase.pay', related="invoice_id.purchase_pay_id", store=True)

    # 外部订单号
    partner_ref = fields.Char(related='purchase_id.partner_ref', store=True, string='外部订单号')

    # 计划付款日期
    plan_pay_date = fields.Date(u'计划付款日期')

    @api.multi
    def write(self, vals):
        if len(self) > 0:
            for ea in self:
                if vals.get('quantity') > ea.purchase_line_id[0].price_subtotal:
                    raise UserError('调整金额%s不能超过总金额%s！'%(vals.get('quantity'),ea.purchase_line_id[0].price_subtotal))
                if vals.get('quantity') != None and vals.get('quantity') < 0:
                    raise UserError('调整金额不能小于0！')

        for each in self:
            if vals.get('quantity') > each.quantity:
                if SPECIAL_REASON[0] not in (each.invoice_id.special_reason or ''):
                    reason = each.invoice_id.special_reason and each.invoice_id.special_reason+';' or '' + SPECIAL_REASON[0]
                    each.invoice_id.write({'special_reason': reason})
        res = super(account_invoice_line, self).write(vals)
        return res