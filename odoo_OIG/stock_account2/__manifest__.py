# -*- coding: utf-8 -*-

{
    'name': '符合中国会计习惯的存货核算,四衡财务',
    'version': '1.0',
    'category': 'account',
    'description': """
符合中国会计习惯的存货核算
=========================================

存货核算中，系统存在的问题
--------------------------------------------
    * Odoo系统不支持全月一次加权平均成本法，在中国应用，需要增加此功能支持
    * Odoo系统的移动加权平均成本，是入库/出库时候实时自动计算。但，当会计人员怀疑某个成本不正确时候，系统缺乏成本重算功能，因而很难验证成本的正确性。
    * 系统存货核算的实现方法是入库、出库时候自动生成会计凭证。但，如果入出库单金额有误，需要删除凭证，将入出库单退回确认状态，纠正金额，重新验证，重新生成凭证。系统缺乏入出库单退回功能及重新生成凭证的功能。
    * 此外，每一条Stock Move生成一个凭证，自动生成的会计凭证太多，不适合会计人员进一步处理，同时也影响性能。
    * 缺乏成本价格调整功能：系统采购入库的Stock Move， Stock Quants上的成本价格取值PO价格，如果PO价格错了，或发票价格和PO价格，系统缺乏Stock Move和Stock Quants的成本价格调整功能。系统的到岸成本功能，目前只适用于实时会计凭证及先进先出成本法（实时价格）的情况下，需要改造，使其适用于各种成本法。
    * 系统缺乏中国会计广为应用的库存进销存报表
    * 系统缺乏期末调汇功能

功能改善
--------------------------------------------
    * 在入库单、出库单、盘点单上增加会计凭证字段，当该单生成了会计凭证时候（自动或手动），该字段标记会计凭证。
    * 增加全月一次加权平均成本计算功能。
    * 增加外币科目期末调汇计算功能。
    * 在入库单、出库单、盘点单上增加Wizard，由会计人员点击Wizard生成相应会计凭证（针对会计凭证字段为空的单据）。
    * 改造stock_landed_cost：使其可以用于标准成本法及移动加权平均成本法的情况下的入库成本分摊。
    * 增加库存进销存报表，以及入出库明细报表。

    """,
    'author': 'OSCG',
    'depends': ['mrp','stock', 'account', 'stock_account', 'stock_landed_costs'],
    'website': 'https://www.zhiyunerp.com/',
    'data': [
        'views/stock_account_view.xml',
        'wizard/stock_entry_view.xml',
        'month_cost/month_cost_view.xml',
        'month_cost/month_cost_computing_report.xml',
        'currency_adjust/currency_adjustment_view.xml',
        'currency_adjust/currency_adjustment_report.xml',
        'security/ir.model.access.csv',
        'reports/stock_move_report_view.xml',
        'reports/stock_cnreport_view.xml',
        'reports/total_in_stock_report_view.xml',
        'views/stock.xml',
        'oig_account_data.xml',
        'views/stock_landed_cost.xml',
    ],
}
