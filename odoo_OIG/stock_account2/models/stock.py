# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools import float_utils
import logging

_logger = logging.getLogger(__name__)

class Location(models.Model):
    _inherit = "stock.location"

    cost_adjust_location_ids = fields.Many2many('stock.location', 'cost_adjust_location_src_dest_rel',
            'dest_id', 'src_id', string=u'需成本调整的来源仓')