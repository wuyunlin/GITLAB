# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from odoo import api, fields, models, _,tools
import logging, json, requests
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import datetime
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp

_logger = logging.getLogger(__name__)

class LandedCost(models.Model):
    _inherit = 'stock.landed.cost'
    _description = 'Stock Landed Cost'

    amount_total = fields.Float(digits=dp.get_precision('Amount Total'))

    def get_valuation_lines(self):
        lines = []
        for move in self.mapped('picking_ids').mapped('move_lines'):
            # it doesn't make sense to make a landed cost for a product that isn't set as being valuated in real time at real cost
            # cost_method非real的情况，成本分摊到Stock Move的price_unit
            # valuation 非real_time的情况，只分摊，不产生分摊的会计凭证
            # 系统是按quants的cost金额分摊，对于采购入库单的分摊，不管哪种成本方法，这样分摊都是OK的。
            # 但调拨和出库的情况，real以外的成本方法，因为stock move的price_unit和quant cost差别较大，这样分摊是不适用的。
            #if move.product_id.valuation != 'real_time' or move.product_id.cost_method != 'real':
            #    continue
            vals = {
                'product_id': move.product_id.id,
                'move_id': move.id,
                'quantity': move.product_qty,
                'former_cost': sum(quant.cost * quant.qty for quant in move.quant_ids),
                'weight': move.product_id.weight * move.product_qty,
                'volume': move.product_id.volume * move.product_qty
            }
            lines.append(vals)

        if not lines and self.mapped('picking_ids'):
            raise UserError(_('The selected picking does not contain any move that would be impacted by landed costs. Landed costs are only possible for products configured in real time valuation with real price costing method. Please make sure it is the case, or you selected the correct picking'))
        return lines

    @api.multi
    def button_validate(self):
        if any(cost.state != 'draft' for cost in self):
            raise UserError(_('Only draft landed costs can be validated'))
        if any(not cost.valuation_adjustment_lines for cost in self):
            raise UserError(_('No valuation adjustments lines. You should maybe recompute the landed costs.'))
        if not self._check_sum():
            raise UserError(_('Cost and adjustments lines do not match. You should maybe recompute the landed costs.'))

        for cost in self:
            move = False
            cost.valuation_adjustment_lines.clear_caches()
            for line in cost.valuation_adjustment_lines.filtered(lambda line: line.move_id):
                if line.move_id.product_id.valuation == 'real_time':
                    if not move:
                        move = self.env['account.move'].create({
                            'journal_id': cost.account_journal_id.id,
                            'date': cost.date,
                            'ref': cost.name
                        })
                per_unit = line.final_cost / line.quantity
                diff = per_unit - line.former_cost_per_unit

                # If the precision required for the variable diff is larger than the accounting
                # precision, inconsistencies between the stock valuation and the accounting entries
                # may arise.
                # For example, a landed cost of 15 divided in 13 units. If the products leave the
                # stock one unit at a time, the amount related to the landed cost will correspond to
                # round(15/13, 2)*13 = 14.95. To avoid this case, we split the quant in 12 + 1, then
                # record the difference on the new quant.
                # We need to make sure to able to extract at least one unit of the product. There is
                # an arbitrary minimum quantity set to 2.0 from which we consider we can extract a
                # unit and adapt the cost.
                curr_rounding = line.move_id.company_id.currency_id.rounding
                diff_rounded = tools.float_round(diff, precision_rounding=curr_rounding)
                diff_correct = diff_rounded
                quants = line.move_id.quant_ids.sorted(key=lambda r: r.qty, reverse=True)
                quant_correct = False
                if quants\
                        and tools.float_compare(quants[0].product_id.uom_id.rounding, 1.0, precision_digits=1) == 0\
                        and tools.float_compare(line.quantity * diff, line.quantity * diff_rounded, precision_rounding=curr_rounding) != 0\
                        and tools.float_compare(quants[0].qty, 2.0, precision_rounding=quants[0].product_id.uom_id.rounding) >= 0:
                    # Search for existing quant of quantity = 1.0 to avoid creating a new one
                    quant_correct = quants.filtered(lambda r: tools.float_compare(r.qty, 1.0, precision_rounding=quants[0].product_id.uom_id.rounding) == 0)
                    if not quant_correct:
                        quant_correct = quants[0]._quant_split(quants[0].qty - 1.0)
                    else:
                        quant_correct = quant_correct[0]
                        quants = quants - quant_correct
                    diff_correct += (line.quantity * diff) - (line.quantity * diff_rounded)
                    diff = diff_rounded

                quant_dict = {}
                for quant in quants:
                    quant_dict[quant] = quant.cost + diff
                if quant_correct:
                    quant_dict[quant_correct] = quant_correct.cost + diff_correct
                for quant, value in quant_dict.items():
                    quant.sudo().write({'cost': value})
                #新加一行修改Stock Move价格
                line.move_id.write({'price_unit': line.move_id.price_unit + diff})
                qty_out = 0
                for quant in line.move_id.quant_ids:
                    if quant.location_id.usage != 'internal':
                        qty_out += quant.qty
                if move:
                    line._create_accounting_entries(move, qty_out)
            cost.write({'state': 'done', 'account_move_id': move and move.id})
            #move.post()
        return True

    # @api.model
    # def run_auto_cost_adjust(self):
    #     picking_obj = self.env['stock.picking']
    #
    #     now = datetime.datetime.now()
    #     today = datetime.datetime.today()
    #     start = (now - datetime.timedelta(hours=25)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
    #     end = now.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
    #
    #     pickings = picking_obj.search([
    #         ('date_done', '<=', end),
    #         ('date_done', '>', start)])
    #
    #     self.auto_cost_adjust(pickings)
    #     _logger.info('run_auto_cost_adjust:')
    #
    #     return True

    @api.model
    def auto_cost_adjust(self, pickings,origin_id=False):
        loc_obj = self.env['stock.location']
        land_obj = self.env['stock.landed.cost']
        prod_obj = self.env['product.product']
        # 查找成本自动分摊的对象
        cash_journal = self.env['ir.model.data'].xmlid_to_object('stock_account2.cost_adjust_journal')
        now = datetime.datetime.now()
        today = datetime.datetime.today()
        #get location config
        locations = loc_obj.search([])
        locs_dict = {}
        for l in locations:
            locs_dict[l.id] = l

        # 查找是到岸成本的产品
        products = prod_obj.search([('landed_cost_ok', '=', True)])
        #
        for picking in pickings:
            # if picking.location_id.id not in picking.location_dest_id.cost_adjust_location_ids.ids:
            # continue
            # 拼到岸成本基础数据
            land_vals = {'date': today.strftime(DEFAULT_SERVER_DATE_FORMAT), 'account_journal_id': cash_journal.id,
                    'picking_ids':  [[6, False, [picking.id]]], u'cost_lines': [],
                    u'valuation_adjustment_lines': []}
            for prod in products:
                # 拼成本行数据
                land_vals[u'cost_lines'].append([0, False, {u'price_unit': 0, u'split_method': u'equal',
                    u'product_id': prod.id, u'name': prod.name}]) #, u'account_id': cash_journal.id
            # 创建到岸成本单
            land_cost = land_obj.create(land_vals)
            # 通过成本行明细的product_id 查找其他字段并赋值
            for costline in land_cost.cost_lines:
                costline.onchange_product_id()
            # 原生方法生成计价调整明细并计算成本
            land_cost.compute_landed_cost()
            # 将接口请求过来的费用添加到成本调整单中
            if not origin_id:
                res = land_cost.get_auto_additional_landed_cost()
                if res['flag'] == 0:
                    self._cr.rollback()
                    return res
                #成本调整报错，手动操作
                land_cost.button_validate()
            else:
                update_res={'origin_id':origin_id,'now_id':land_cost.id}

                sql="""with origin_res as (
                        select b.product_id,c.product_id as fy_id, -b.additional_landed_cost/b.quantity as price_unit
                         from stock_landed_cost a 
                        inner join stock_valuation_adjustment_lines b on b.cost_id=a.id
                        inner join stock_landed_cost_lines c on c.id=b.cost_line_id
                        where a.id=%(origin_id)s 
                        group by b.product_id,c.product_id , -b.additional_landed_cost/b.quantity),
                        update_res as (
                        select b.id,b.product_id,c.product_id as fy_id,  d.price_unit*b.quantity as additional_landed_cost from stock_landed_cost a 
                        inner join stock_valuation_adjustment_lines b on b.cost_id=a.id
                        inner join stock_landed_cost_lines c on c.id=b.cost_line_id
                        inner join origin_res d on d.product_id=b.product_id and d.fy_id=c.product_id
                        where a.id=%(now_id)s
                        )
                        ---更新产品行
                        update stock_valuation_adjustment_lines 
                        set additional_landed_cost=update_res.additional_landed_cost
                        from update_res where update_res.id=stock_valuation_adjustment_lines.id;
                        ---更新费用行
                        with res as (
                        select b.cost_line_id, sum(b.additional_landed_cost) as price_unit from stock_landed_cost a 
                        inner join stock_valuation_adjustment_lines b on b.cost_id=a.id
                        where a.id=%(now_id)s group by b.cost_line_id)
                        update stock_landed_cost_lines  set price_unit=res.price_unit from res where res.cost_line_id=stock_landed_cost_lines.id;
                        ---删除调整为0的条目
                        delete from stock_landed_cost_lines where price_unit=0 and cost_id=%(now_id)s;
                        delete from stock_valuation_adjustment_lines where additional_landed_cost=0 and cost_id=%(now_id)s;
                        update stock_landed_cost set amount_total=(
                        select sum(b.price_unit) as price_unit from stock_landed_cost a
                       inner join stock_landed_cost_lines b on b.cost_id=a.id
                        where a.id=%(now_id)s ) where id=%(now_id)s
                        """%(update_res)
                self.env.cr.execute(sql)
                #self.env.cr.commit()
                #land_cost.button_validate()
            return {'flag': 1}


    @api.multi
    def get_auto_additional_landed_cost(self):
        cost_line_price_dict = {}
        for cost in self.filtered(lambda cost: cost.picking_ids): ### TODO
            # 空海运属性
            if cost.picking_ids.location_id.delivery == 'air':
                shipping_type_str = u'空运'
            else:
                shipping_type_str = u'海运'
            # 发货仓源ID
            warehousId = cost.picking_ids.location_id.stock_location_id_shipping.origin_id
            vals = {u'number': cost.name, u'warehouseType': shipping_type_str,
                    u'warehousName': warehousId, u'skus': []
                    }
            for valuation in cost.valuation_adjustment_lines:
                vals[u'skus'].append({u'uuid': valuation.id, u'sku': valuation.product_id.default_code,
                        u'cost': valuation.former_cost_per_unit, u'stage': False})

        # 调用接口平台的成本调整接口获取费用类型的费用
        add_costs_return = self.costEstimation(vals)
        add_costs_dict = {}
        if add_costs_return.get('flag') == 0:
            return add_costs_return
        add_costs=add_costs_return['add_costs']
        self.write({'origin_costupdate': json.dumps(add_costs)})

        # 新增
        for line_cost in add_costs['skus']:
            add_costs_dict[int(line_cost[u'uuid'])] = line_cost

        # feed addtional cost back
        # 将额外的到岸成本添加到计价调整明细中
        for valuation in cost.valuation_adjustment_lines:
            prod_add_cost = add_costs_dict[valuation.id]
            line_cost = prod_add_cost[valuation.cost_line_id.product_id.default_code] * valuation.quantity
            # if line_cost != 0:
            #     line_cost = round(line_cost + 0.005, 2)
            if line_cost:
                # 将额外的到岸成本写入计价调整明细
                valuation.write({'additional_landed_cost': line_cost})
                # 对同类型费用求和
                if cost_line_price_dict.has_key(valuation.cost_line_id.id):
                    cost_line_price_dict[valuation.cost_line_id.id] += line_cost
                else:
                    cost_line_price_dict[valuation.cost_line_id.id] = line_cost
            else:
                valuation.unlink()

        # 将对应类型费用总和写入成本行明细中
        for cost_line in cost.cost_lines:
            if cost_line_price_dict.get(cost_line.id,False):
                cost_line.write({'price_unit': cost_line_price_dict[cost_line.id]})
            else:
                cost_line.unlink()
        return {'flag': 1}







    def auto_get_valuation_lines(self):
        lines = []

        for move in self.mapped('picking_ids').mapped('move_lines'):
            # it doesn't make sense to make a landed cost for a product that isn't set as being valuated in real time at real cost
            if move.product_id.valuation != 'real_time' or move.product_id.cost_method != 'real':
                continue

            # in oig, cost adjusting depend on former_cost, so create adjusting by quants
            for quant in move.quant_ids:
                vals = {
                    'product_id': move.product_id.id,
                    'move_id': move.id,
                    'quantity': quant.qty,
                    'former_cost': quant.cost * quant.qty,
                    'weight': quant.product_id.weight * quant.qty,
                    'volume': quant.product_id.volume * quant.qty
                }
                lines.append(vals)

        if not lines and self.mapped('picking_ids'):
            raise UserError(_('The selected picking does not contain any move that would be impacted by landed costs. Landed costs are only possible for products configured in real time valuation with real price costing method. Please make sure it is the case, or you selected the correct picking'))
        return lines


    @api.multi
    def auto_compute_landed_cost(self):
        AdjustementLines = self.env['stock.valuation.adjustment.lines']
        # 添加数据前先删除计价调整明细单
        AdjustementLines.search([('cost_id', 'in', self.ids)]).unlink()

        digits = dp.get_precision('Product Price')(self._cr)
        towrite_dict = {}
        for cost in self.filtered(lambda cost: cost.picking_ids):
            total_qty = 0.0
            total_cost = 0.0
            total_weight = 0.0
            total_volume = 0.0
            total_line = 0.0
            # 获取计价调整明细数据
            all_val_line_values = cost.auto_get_valuation_lines()
            # 对计价调整明细行设值
            for val_line_values in all_val_line_values:
                for cost_line in cost.cost_lines:
                    val_line_values.update({'cost_id': cost.id, 'cost_line_id': cost_line.id})
                    # 创建计价调整明细单
                    self.env['stock.valuation.adjustment.lines'].create(val_line_values)
                total_qty += val_line_values.get('quantity', 0.0)
                total_cost += val_line_values.get('former_cost', 0.0)
                total_weight += val_line_values.get('weight', 0.0)
                total_volume += val_line_values.get('volume', 0.0)
                total_line += 1

            for line in cost.cost_lines:
                value_split = 0.0
                for valuation in cost.valuation_adjustment_lines:
                    value = 0.0
                    if valuation.cost_line_id and valuation.cost_line_id.id == line.id:
                        if line.split_method == 'by_quantity' and total_qty:
                            per_unit = (line.price_unit / total_qty)
                            value = valuation.quantity * per_unit
                        elif line.split_method == 'by_weight' and total_weight:
                            per_unit = (line.price_unit / total_weight)
                            value = valuation.weight * per_unit
                        elif line.split_method == 'by_volume' and total_volume:
                            per_unit = (line.price_unit / total_volume)
                            value = valuation.volume * per_unit
                        elif line.split_method == 'equal':
                            value = (line.price_unit / total_line)
                        elif line.split_method == 'by_current_cost_price' and total_cost:
                            per_unit = (line.price_unit / total_cost)
                            value = valuation.former_cost * per_unit
                        else:
                            value = (line.price_unit / total_line)

                        if digits:
                            value = tools.float_round(value, precision_digits=digits[1], rounding_method='UP')
                            fnc = min if line.price_unit > 0 else max
                            value = fnc(value, line.price_unit - value_split)
                            value_split += value

                        if valuation.id not in towrite_dict:
                            towrite_dict[valuation.id] = value
                        else:
                            towrite_dict[valuation.id] += value
        if towrite_dict:
            for key, value in towrite_dict.items():
                AdjustementLines.browse(key).write({'additional_landed_cost': value})
        return True

    # 调用接口平台的成本调整接口
    @api.multi
    def costEstimation(self,vals):
        import requests
        import json
        # 调用接口平台登录接口获取Authorization
        login_url = self.env['ir.config_parameter'].sudo().get_param('login_url')
        login_headers = {'Content-Type': 'application/json; charset=utf-8'}
        login_data = self.env['ir.config_parameter'].sudo().get_param('login_data')
        login_response = requests.post(login_url, data=login_data, headers=login_headers, verify=False)
        Authorization = login_response.headers._store['authorization'][1]

        # 调用成本调整接口
        # data = ' {"number":"CHG0128625","warehouseType":"空运","warehousName":"USWCWINIT","skus": [{"uuid":"100001","sku":"0T+50ZJC+DE","cost":10},{"uuid":"100002","sku":"0T+50ZJC+DE","cost":45},{"uuid":"100003","sku":"0T+50ZJC+DE","cost":11}] }'
        # url = 'http://localhost:8080/wms/costEstimation'
        url = self.env['ir.config_parameter'].sudo().get_param('costEstimation_url')
        headers = {}
        headers['Content-Type'] = 'application/json; charset=utf-8'
        headers['Authorization'] = Authorization
        data = json.dumps(vals)
        response = requests.post(url, data=data, headers=headers, verify=False)
        if requests.codes.ok == response.status_code or 201 == response.status_code:
            content = json.loads(response.content)
            if content['code'] == '200':
                add_costs = content['data']
                return {'flag': 1,'add_costs':add_costs}
            else:
                self._cr.rollback()
                return {'flag': 0, 'message': '调用成本测算接口失败：'+content['msg']}
        else:
            self._cr.rollback()
            return {'flag': 0, 'message': '调用成本测算接口失败'}

class AdjustmentLines(models.Model):
    _inherit = 'stock.valuation.adjustment.lines'
    former_cost_per_unit = fields.Float('Former Cost(Per Unit)', compute='_compute_former_cost_per_unit',
                                         store=True,digits=dp.get_precision('Product Price'))
