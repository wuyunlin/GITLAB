# -*- coding: utf-8 -*- #
from collections import defaultdict
from odoo import api, fields, models, _
from odoo.tools.misc import ustr
import logging
import time
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)
class StockPicking(models.Model):
    _inherit = 'stock.picking'

    account_move_id = fields.Many2one('account.move', u'会计凭证', copy=False)
    amount = fields.Float(u'总金额',digits=dp.get_precision('Product Price'))
    date_done = fields.Datetime(u'实际移动时间', help="Completion Date of Transfer", states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}, copy=False)

    @api.multi
    def _create_entry(self):
        """手工点击Wizard重建会计凭证
        """
        entry_ids = []
        for picking in self:
            if picking.state != 'done' or picking.account_move_id:
                continue
            for move in picking.move_lines:
                move._account_entry_move()
            if picking.browse(picking.id).account_move_id:
                entry_ids.append(picking.account_move_id.id)
        return entry_ids


class StockInventory(models.Model):
    _inherit = 'stock.inventory'

    account_move_id = fields.Many2one('account.move', u'会计凭证')
    date = fields.Datetime(u'时间盘点时间', required=True, states={'done': [('readonly', True)], 'cancel': [('readonly', True)]}, help="The date that will be used for the stock level check of the products and the validation of the stock move related to this inventory.")

    @api.multi
    def create_inventory_entry(self):
        """手工点击Wizard重建盘点会计凭证
        """
        entry_ids = []
        for inv in self:
            if inv.state != 'done' or inv.account_move_id:
                continue
            for move in inv.move_ids:
                move._account_entry_move()
            if inv.browse(inv.id).account_move_id:
                entry_ids.append(inv.account_move_id.id)
        return entry_ids



#########################################################
# 1）允许在StockPicking及Inventory录入实际的发生日期
# 2）系统基于Stock Move自动产生会计凭证时候，取Picking和Inventory上的实际日期，而不是系统当前日期
#
########################################################
class StockMove(models.Model):
    _inherit = "stock.move"

    # 计算小计金额
    @api.multi
    def _compute_line_amount(self):
        for line in self:
            line.line_amount = (line.price_unit or 0)*line.product_uom_qty
    line_amount = fields.Float(u'金额', compute='_compute_line_amount',digits=dp.get_precision('Product Price'))
    price_unit_origin = fields.Float(u'发出价格【未调整前】',digits=dp.get_precision('Product Price'))
    price_unit = fields.Float(u'价格',digits=dp.get_precision('Product Price'))

    @api.multi
    def action_done(self):
        """如果Picking上已经有实际入库/出库时间、或者盘点单上有实际盘点时间
            则将该时间写入Stock Move的date字段作为实际完成时间。
        """
        move_date = {}
        for move in self:
            if move.picking_id and move.picking_id.date_done:
                move_date[move.id] = move.picking_id.date_done
            elif move.inventory_id and move.inventory_id.date:
                move_date[move.id] = move.inventory_id.date
            else:
                move_date[move.id] = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        res = super(StockMove, self).action_done()
        for move in self:
            self.write({'date': move_date[move.id]})
        
        return res

    @api.one
    def _account_entry_move(self):
        """ 基于Picking或Inventory手动点击Wizard产生会计凭证时候，
             Picking调用本方法产生每个Stock Move的会计凭证。
         """
        if self.state != 'done' or self.product_id.type != 'product':
            # no stock valuation for consumable products
            return False
        account_move = False
        if self.picking_id:
            account_move = self.picking_id.account_move_id
        elif self.inventory_id:
            account_move = self.inventory_id.account_move_id
        if account_move and account_move.state != 'draft':
            return False

        location_from = self.location_id
        location_to = self.location_dest_id  # TDE FIXME: as the accounting is based on this value, should probably check all location_to to be the same
        company_from = location_from.usage == 'internal' and location_from.company_id or False
        company_to = location_to and (location_to.usage == 'internal') and location_to.company_id or False

        # Create Journal Entry for products arriving in the company; in case of routes making the link between several
        # warehouse of the same company, the transit location belongs to this company, so we don't need to create accounting entries
        if company_to and (self.location_id.usage not in ('internal', 'transit') and self.location_dest_id.usage == 'internal' or company_from != company_to):
            journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation()
            if location_from and location_from.usage == 'customer':  # goods returned from customer
                self.with_context(force_company=company_to.id)._create_account_move_line(acc_dest, acc_valuation, journal_id)
            else:
                self.with_context(force_company=company_to.id)._create_account_move_line(acc_src, acc_valuation, journal_id)

        # Create Journal Entry for products leaving the company
        if company_from and (self.location_id.usage == 'internal' and self.location_dest_id.usage not in ('internal', 'transit') or company_from != company_to):
            journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation()
            if location_to and location_to.usage == 'supplier':  # goods returned to supplier
                self.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_src, journal_id)
            else:
                self.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_dest, journal_id)

        if self.company_id.anglo_saxon_accounting and self.location_id.usage == 'supplier' and self.location_dest_id.usage == 'customer':
            # Creates an account entry from stock_input to stock_output on a dropship move. https://github.com/odoo/odoo/issues/12687
            journal_id, acc_src, acc_dest, acc_valuation = self._get_accounting_data_for_valuation()
            self.with_context(force_company=self.company_id.id)._create_account_move_line(acc_src, acc_dest, journal_id)

    @api.one
    def _create_account_move_line(self, credit_account_id, debit_account_id, journal_id):
        move_date = False
        account_move = False
        if self.picking_id:
            move_date = self.picking_id.date_done
            account_move = self.picking_id.account_move_id
        elif self.inventory_id:
            move_date = self.inventory_id.date
            account_move = self.inventory_id.account_move_id
        AccountMove = self.env['account.move']
        move_lines = self._prepare_account_move_line(self.product_uom_qty, self.price_unit, credit_account_id, debit_account_id)
        if move_lines:
            if not move_date:
                move_date = self._context.get('force_period_date', fields.Date.context_today(self))
            if account_move:
                account_move.write({'line_ids': move_lines})
                if self.picking_id:
                    amount = sum(abs(x.debit) for x in account_move.line_ids)
                    self.picking_id.write({ 'amount': amount})
            else:
                new_account_move = AccountMove.create({
                    'journal_id': journal_id,
                    'line_ids': move_lines,
                    'date': move_date,
                    'ref': self.picking_id.name})
                #new_account_move.post()
                account_move=new_account_move
                if self.picking_id:
                    amount=sum(abs(x.debit) for x in account_move.line_ids  )
                    self.picking_id.write({'account_move_id': new_account_move.id,'amount':amount })
                elif self.inventory_id:
                    self.inventory_id.write({'account_move_id': new_account_move.id })

    @api.one
    def _check_lock_date(self):
        lock_date = max(self.company_id.period_lock_date, self.company_id.fiscalyear_lock_date)
        if self.user_has_groups('account.group_account_manager'):
            lock_date = self.company_id.fiscalyear_lock_date
        if lock_date and self.date <= lock_date:
            return False
        return True
    #######覆盖写源码方法；此处无论是否是实体库，都需要将价格写入
    @api.multi
    def product_price_update_after_done(self):
        ''' Adapt standard price on outgoing moves if the product cost_method is 'real', so that a
        return or an inventory loss is made using the last value used for an outgoing valuation. '''
        #to_update_moves = self.filtered(lambda move: move.product_id.cost_method == 'real' and move.location_dest_id.usage != 'internal')
        to_update_moves=self
        to_update_moves._store_average_cost_price()
        for move in to_update_moves:
            move.write({'price_unit_origin': move.price_unit})


class StockQuant(models.Model):
    _inherit = "stock.quant"

    @api.model
    def _quant_create_from_move(self, qty, move, lot_id=False, owner_id=False,
                                src_package_id=False, dest_package_id=False,
                                force_location_from=False, force_location_to=False):
        """如果Picking或Inventory上有实际完成时间，以此时间作为入出库会计凭证的发生时间。
            而不是取系统时间。
        """
        dt = False
        if move.picking_id and move.picking_id.date_done:
            dt = move.picking_id.date_done
        elif move.inventory_id and move.inventory_id.date:
            dt = move.inventory_id.date
        ctx = self._context.copy()
        if dt: 
            dt2 = dt[:10]
            if not ctx.has_key('force_period_date'):
                ctx.update({'force_period_date': dt2})

        if self._context != ctx:
            self = self.with_context(ctx)
        quant = super(StockQuant, self)._quant_create_from_move(qty, move, lot_id=lot_id, owner_id=owner_id, src_package_id=src_package_id, dest_package_id=dest_package_id, force_location_from=force_location_from, force_location_to=force_location_to)
        if dt:
            quant.write({'in_date': dt})
        return quant

    @api.multi
    def _quant_update_from_move(self, move, location_dest_id, dest_package_id, lot_id=False, entire_pack=False):
        """如果Picking或Inventory上有实际完成时间，以此时间作为入出库会计凭证的发生时间。
            而不是取系统时间。
        """
        dt = False
        if move.picking_id and move.picking_id.date_done:
            dt = move.picking_id.date_done
        elif move.inventory_id and move.inventory_id.date:
            dt = move.inventory_id.date

        ctx = self._context.copy()
        if dt:
            dt2 = dt[:10]
            if not ctx.has_key('force_period_date'):
                ctx.update({'force_period_date': dt2})

        if self._context != ctx:
            self = self.with_context(ctx)
        res = super(StockQuant, self)._quant_update_from_move(move, location_dest_id, dest_package_id, lot_id=lot_id, entire_pack=entire_pack)
        return res


    def _create_account_move_line(self, move, credit_account_id, debit_account_id, journal_id):
        """改成一个Picking/Inventory一个会计凭证，而不是一个Stock Move一个会计凭证。
            实现方法是，Stock Move产生会计凭证时候，先查看Picking上是否已有凭证，有则
            将新产生的分录添加到该凭证，没有则先产生的凭证记录到Picking的account_move_id字段。
        """
        # group quants by cost
        quant_cost_qty = defaultdict(lambda: 0.0)
        for quant in self:
            quant_cost_qty[quant.cost] += quant.qty

        account_move = False
        ref = False
        if move.picking_id:
            account_move = move.picking_id.account_move_id
            ref = move.picking_id.name
        elif move.inventory_id:
            account_move = move.inventory_id.account_move_id
            ref = move.inventory_id.name
            
        AccountMove = self.env['account.move']
        for cost, qty in quant_cost_qty.iteritems():
            move_lines = move.with_context(self._context)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
            if move_lines:
                date = self._context.get('force_period_date', fields.Date.context_today(self))
                if account_move:
                    account_move.write({'line_ids': move_lines})
                    if move.picking_id:
                        amount = sum(abs(x.debit) for x in account_move.line_ids)
                        move.picking_id.write({'amount': amount})
                else:
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': date,
                        'ref': ref})
                    #new_account_move.post()
                    account_move=new_account_move
                    if move.picking_id:
                        amount = sum(abs(x.debit) for x in account_move.line_ids)
                        move.picking_id.write({'account_move_id': new_account_move.id ,'amount':amount})
                    elif move.inventory_id:
                        move.inventory_id.write({'account_move_id': new_account_move.id })

    @api.multi
    def _price_update(self, newprice):
        """ 覆盖Odoo原方法，原方法Bug：Stock Move缺乏方法_check_lock_date，
        其次，不应该在_account_entry_move之前调用super(StockQuant, self)._price_update(newprice)更新cost
        This function is called at the end of negative quant reconciliation
        and does the accounting entries adjustemnts and the update of the product
        cost price if needed """
        #super(StockQuant, self)._price_update(newprice)
        for quant in self:
            move = quant._get_latest_move()
            valuation_update = newprice - quant.cost
            # this is where we post accounting entries for adjustment, if needed
            # If neg quant period already closed (likely with manual valuation), skip update
            if not quant.company_id.currency_id.is_zero(valuation_update) and move._check_lock_date():
                quant.with_context(force_valuation_amount=valuation_update)._account_entry_move(move)

            # update the standard price of the product, only if we would have
            # done it if we'd have had enough stock at first, which means
            # 1) the product cost's method is 'real'
            # 2) we just fixed a negative quant caused by an outgoing shipment
            quant.sudo().write({'cost': newprice})
            if quant.product_id.cost_method == 'real' and quant.location_id.usage != 'internal':
                move._store_average_cost_price()


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.model
    def _anglo_saxon_sale_move_lines(self, i_line):
        """Return the additional move lines for sales invoices and refunds.
        i_line: An account.invoice.line object.
        res: The move line entries produced so far by the parent move_line_get.
        """
        inv = i_line.invoice_id
        company_currency = inv.company_id.currency_id

        # 非real_time的情况，手动基于Picking产生“发出商品”凭证，而后发票确认时候也需要冲发出商品分录
        #if i_line.product_id.type == 'product' and i_line.product_id.valuation == 'real_time':
        if i_line.product_id.type == 'product':
            fpos = i_line.invoice_id.fiscal_position_id
            accounts = i_line.product_id.product_tmpl_id.get_product_accounts(fiscal_pos=fpos)
            # debit account dacc will be the output account
            dacc = accounts['stock_output'].id
            # credit account cacc will be the expense account
            cacc = accounts['expense'].id
            if dacc and cacc:
                # to fix:
                # 当出库和发出商品间隔较长时间（跨月），此处简单取product standard_price有问题
                # 移动平均情况，如果出库和发票确认之间发生过入库，则出库时候的product standard_price，和发票确认时候的product standard_price可能有差异
                # FIFO的情况，如果出库和发票确认之间发生过出库，则出库时候的product standard_price，和发票确认时候的product standard_price可能有差异
                # 全月平均的情况，如果发票确认在出库的次次月发生，则出库时候的product standard_price，和发票确认时候的product standard_price可能有差异
                # 应改成以i_line.create_date时候的product.price.history的价格较为准确
                #price_unit = i_line._get_anglo_saxon_price_unit()
                price_unit = i_line.product_id.get_history_price(i_line.company_id.id, i_line.invoice_id.create_date)
                if inv.currency_id.id != company_currency:
                    currency_id = inv.currency_id.id
                    amount_currency = i_line._get_price(company_currency, price_unit)
                else:
                    currency_id = False
                    amount_currency = False
                return [
                    {
                        'type': 'src',
                        'name': i_line.name[:64],
                        'price_unit': price_unit,
                        'quantity': i_line.quantity,
                        'price': price_unit * i_line.quantity,
                        'currency_id': currency_id,
                        'amount_currency': amount_currency,
                        'account_id':dacc,
                        'product_id':i_line.product_id.id,
                        'uom_id':i_line.uom_id.id,
                        'account_analytic_id': i_line.account_analytic_id.id,
                        'analytic_tag_ids': i_line.analytic_tag_ids.ids and [(6, 0, i_line.analytic_tag_ids.ids)] or False,
                    },

                    {
                        'type': 'src',
                        'name': i_line.name[:64],
                        'price_unit': price_unit,
                        'quantity': i_line.quantity,
                        'price': -1 * price_unit * i_line.quantity,
                        'currency_id': currency_id,
                        'amount_currency': -1 * amount_currency,
                        'account_id':cacc,
                        'product_id':i_line.product_id.id,
                        'uom_id':i_line.uom_id.id,
                        'account_analytic_id': i_line.account_analytic_id.id,
                        'analytic_tag_ids': i_line.analytic_tag_ids.ids and [(6, 0, i_line.analytic_tag_ids.ids)] or False,
                    },
                ]
        return []