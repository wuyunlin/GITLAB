# -*- coding: utf-8 -*- #
from odoo import api, fields, models, _
from odoo.tools.misc import ustr
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)
class MonthCostReport(models.AbstractModel):
    _name = 'report.stock_account2.currency_adjustment'

    @api.model
    def render_html(self, docids, data=None):
        return self.env['report'].render('stock_account2.currency_adjustment', data)


class CurrencyAutoAdjustment(models.Model):
    _name = "currency.auto.adjustment"
    _description = u"期末调汇"

    @api.model
    def default_get(self, fields):
        account_obj = self.env['account.account']
        rec = super(CurrencyAutoAdjustment, self).default_get(fields)

        account_ids = []
        accounts = account_obj.search([('adjustment_fg', '=', True)])
        for account in accounts:
            account_ids.append(account.id)

        rec['account_ids'] = account_ids

        return rec

    name = fields.Char(u'说明', required=True, readonly=True, states={'draft': [('readonly', False)]} )
    adjust_date = fields.Date(u'调汇日期', copy=False, required=True, readonly=True, states={'draft': [('readonly', False)]})
    account_ids = fields.Many2many('account.account',  string=u'调汇科目', copy=True, required=True, readonly=True, states={'draft': [('readonly', False)]})
    journal_id = fields.Many2one('account.journal',  u'日记账', copy=True, required=True, readonly=True, states={'draft': [('readonly', False)]}, default=lambda self: self.env.user.company_id.currency_exchange_journal_id )
    state = fields.Selection([ ('draft', u'未计算'), ('done', u'已计算'),], u'状态', required=True, readony = True, default='draft')
    company_id = fields.Many2one('res.company', u'公司', required=True, readonly=True, states={'draft': [('readonly', False)]}, default=lambda self: self.env['res.company']._company_default_get('currency.auto.adjustment'))

    @api.multi
    def currency_adjust_old(self):
        self.ensure_one()
        adjust_date = self.adjust_date
        lock_date = max(self.company_id.period_lock_date, self.company_id.fiscalyear_lock_date)
        if self.user_has_groups('account.group_account_manager'):
            lock_date = self.company_id.fiscalyear_lock_date
        if lock_date and adjust_date <= lock_date:
            raise UserError(u"该日期【%s】已经财务关账，不可做调汇分录！" % (adjust_date, ) )
        log_report = []
        for account_id in self.account_ids:
            query = """SELECT sum(debit), sum(credit), sum(amount_currency) 
                        FROM account_move_line WHERE account_id = %s and company_id = %s""" % (account_id.id, self.company_id.id)
            self.env.cr.execute(query)
            row = self.env.cr.fetchone()
            if None == row[0] or None == row[1] or None == row[2]:
                 raise UserError(u"科目【%s %s】没有余额，不可调汇！" % (account_id.code, account_id.name) )
            local_balance = row[0] - row[1]
            currency_balance = row[2]
            local_currency = self.env.user.company_id.currency_id
            if account_id.currency_id.is_zero(currency_balance) and local_currency.is_zero(local_balance): continue
            local_amount = account_id.currency_id.with_context(date=adjust_date).compute(currency_balance, local_currency)
            if local_currency.is_zero(local_amount - local_balance): continue
            rate = local_currency.with_context(date=adjust_date)._get_conversion_rate(account_id.currency_id, local_currency)
            
            # 科目，科目代码，科目名称，币种，外币余额，最新汇率，本位币余额，调汇金额
            log = [account_id.id, account_id.code, account_id.name, account_id.currency_id.name, currency_balance, rate, local_balance, local_amount - local_balance]
            log_report.append(log)
            debit_value = credit_value = local_amount - local_balance
            debit_account = account_id.id
            if local_amount - local_balance > 0:
                credit_account = self.journal_id.default_credit_account_id.id
            else:
                credit_account = self.journal_id.default_debit_account_id.id

            debit_line_vals = {
                'name': 'CUR: %s' % self.name,
                'ref': self.name,
                'debit': debit_value > 0 and debit_value or 0,
                'credit': debit_value < 0 and -debit_value or 0,
                'account_id': debit_account,
                'currency_id': account_id.currency_id.id,
                'amount_currency': 0,
            }
            credit_line_vals = {
                'name': 'CUR: %s' % self.name,
                'ref': self.name,
                'debit': credit_value < 0 and -credit_value or 0,
                'credit': credit_value > 0 and credit_value or 0,
                'account_id': credit_account,
                'currency_id': account_id.currency_id.id,
                'amount_currency': 0,
            }
            res = [(0, 0, debit_line_vals), (0, 0, credit_line_vals)]
            account_move = self.env['account.move'].create({
                'journal_id': self.journal_id.id,
                'line_ids': res,
                'date': adjust_date,
                'ref': self.name})

        datas = {
            'ids': [],
            'model': 'currency.auto.adjustment',
            'logs': log_report
        }
        self.write({'state': 'done' })
        return self.env['report'].get_action([self.id], 'stock_account2.currency_adjustment', data=datas)

    @api.multi
    def currency_adjust(self):
        self.ensure_one()
        adjust_date = self.adjust_date
        lock_date = max(self.company_id.period_lock_date, self.company_id.fiscalyear_lock_date)
        if self.user_has_groups('account.group_account_manager'):
            lock_date = self.company_id.fiscalyear_lock_date
        if lock_date and adjust_date <= lock_date:
            raise UserError(u"该日期【%s】已经财务关账，不可做调汇分录！" % (adjust_date,))
        log_report = []
        need_account_ids=[x.id for x in  self.account_ids ]+[0,-1]
        query = """SELECT  sum(debit), sum(credit), sum(amount_currency),account_id,currency_id
                    FROM account_move_line  where account_id in %s and company_id = %s and COALESCE (currency_id ,8) <> 8
                    and date<='%s'
                    group by currency_id,account_id order by account_id,currency_id""" % (tuple(need_account_ids), self.company_id.id,adjust_date)
        self.env.cr.execute(query)
        fet = self.env.cr.fetchall() or []
        res=[]
        for row in fet:
            ########借方减去贷方
            local_balance = row[0] - row[1]
            ########外币余额
            currency_balance = row[2]
            #######本位币
            local_currency = self.env.user.company_id.currency_id
            #########外币
            foreign_currency =self.env['res.currency'].browse(row[4])
            #########科目
            account_instance=self.env['account.account'].browse(row[3])

            # 判断外币是否有维护汇率，没有则提示
            if foreign_currency.rate_ids:
                pass
            else:
                raise UserError(foreign_currency.name+"，此币种没有维护汇率！")

            if foreign_currency.is_zero(currency_balance) and local_currency.is_zero(local_balance):
                continue
            ##########计算外币余额在调汇日期对应的本位币金额
            local_amount = foreign_currency.with_context(date=adjust_date).compute(currency_balance,
                                                                                         local_currency)
            if local_currency.is_zero(local_amount - local_balance):
                continue
            ####找出换算比例
            rate = local_currency.with_context(date=adjust_date)._get_conversion_rate(foreign_currency,local_currency)

            # 科目，科目代码，科目名称，币种，外币余额，最新汇率，本位币余额，调汇金额
            log = [account_instance.id, account_instance.code, account_instance.name, foreign_currency.name, currency_balance, rate,
                   local_balance, local_amount - local_balance]
            log_report.append(log)
            debit_value = credit_value = local_amount - local_balance
            debit_account = account_instance.id
            credit_account = self.journal_id.default_credit_account_id.id
            debit_line_vals = {
                'name': 'CUR: %s' % self.name,
                'ref': self.name,
                'debit': -debit_value,
                'credit':  0,
                'account_id': credit_account,
                'currency_id': foreign_currency.id,
                'amount_currency': 0,
            }
            credit_line_vals = {
                'name': 'CUR: %s' % self.name,
                'ref': self.name,
                'debit': 0,
                'credit':  -credit_value,
                'account_id': debit_account,
                'currency_id': foreign_currency.id,
                'amount_currency': 0,
            }
            res.append((0,0,debit_line_vals))
            res.append((0, 0, credit_line_vals))
        account_move = self.env['account.move'].create({
            'journal_id': self.journal_id.id,
            'line_ids': res,
            'date': adjust_date,
            'ref': self.name})

        datas = {
            'ids': [],
            'model': 'currency.auto.adjustment',
            'logs': log_report
        }
        self.write({'state': 'done'})
        return self.env['report'].get_action([self.id], 'stock_account2.currency_adjustment', data=datas)


class AccountAccount(models.Model):
    _inherit = "account.account"
    _description = "Account"

    adjustment_fg = fields.Boolean(default=False, string='期末调汇')
