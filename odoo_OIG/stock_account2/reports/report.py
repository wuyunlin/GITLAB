# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

import logging
_logger = logging.getLogger(__name__)

class stock_move_report_result(models.Model):
    _name = 'stock.move.report.result'
    _description = u"收发明细报表"
    _order = 'op_date asc'

    order_name = fields.Char(u'单号')
    op_date = fields.Datetime(u'操作日期')
    qty = fields.Float(u'数量')
    inv = fields.Float(u'金额')
    price = fields.Float(u'价格')
    product = fields.Char(u'品名')
    uom = fields.Char(u'单位')
    product_id = fields.Many2one('product.product', u'产品id', required=True)
    default_code = fields.Char(u'产品编码')


class total_in_stock_report_result(models.Model):
    _name = 'total.in.stock.report.result'
    _description = u"进销存报表"

    product = fields.Char(u'产品名称')
    qc_qty = fields.Float(u'期初数量')
    qc_inventory = fields.Float(u'期初金额')
    qc_price = fields.Float(u'期初价格')
    qm_qty = fields.Float(u'期末数量')
    qm_inventory = fields.Float(u'期末金额')
    qm_price = fields.Float(u'期末价格')
    qz_in_qty = fields.Float(u'期间入库数量')
    qz_in_inventory = fields.Float(u'期间入库金额')
    qz_in_price = fields.Float(u'期间入库价格')
    qz_out_qty = fields.Float(u'期间出库数量')
    qz_out_inventory = fields.Float(u'期间出库金额')
    qz_out_price = fields.Float(u'期间出库价格')
    product_id = fields.Many2one('product.product', u'产品id', required=True)
    default_code = fields.Char(u'产品编码')
    adjust_inventory = fields.Float(u'调整金额')
    stock_location = fields.Char(u'仓库')
    stock_location_id = fields.Many2one('stock.location',u'仓库id',required=True)


