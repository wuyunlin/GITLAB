# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging
from datetime import datetime, timedelta

_logger = logging.getLogger(__name__)
class stock_move_report(models.TransientModel):
    _name = 'stock.move.report'
    _description = u"收发明细"

    date_start = fields.Date(string=u'期初', required = True)
    date_end = fields.Date(string=u'期末', required = True)
    stock_location = fields.Many2one("stock.location",  string= u'库位', required=True)

    @api.model
    def get_report_vals(self, data):
        his_obj=self.env['stock.history']
        stock_obj=self.env['stock.location']
        product_obj = self.env['product.product']
        date_start=data.get('date_start')
        date_end=data.get('date_end')
        date_start = datetime.strptime("%s 00:00:00" % date_start, "%Y-%m-%d %H:%M:%S") - timedelta(hours=8)
        date_end = datetime.strptime("%s 23:59:59" % date_end, "%Y-%m-%d %H:%M:%S") - timedelta(hours=8)
        stock_location=data.get('stock_location')
        res={}
        total_inv=0.0

        context = dict(self._context or {})
        context['history_date']=date_end

        stock_location_id = stock_obj.browse(stock_location)

        stock_location_ids = [stock_location]
        str_stock_location_ids = '%s' % stock_location
        for child in stock_location_id.child_ids:
            stock_location_ids.append(child.id)
            str_stock_location_ids += ',%s' % child.id
        params = (str_stock_location_ids, str_stock_location_ids, str_stock_location_ids, str_stock_location_ids,
                date_start, date_end)
        sql_str = """SELECT MV.product_id, PT.name, PU.name AS pu_name,
                  (CASE
                         WHEN MV.location_id in (%s)
                            THEN (- MV.product_qty)
                            ELSE MV.product_qty
                  END) AS quantity,
                  (CASE
                         WHEN MV.location_id in (%s)
                            THEN (MV.price_unit * (- MV.product_qty) )
                            ELSE (MV.price_unit * MV.product_qty)
                  END) AS inventory_value,
                  MV.origin AS source, MV.date AS date,pp.default_code 
                  FROM stock_move AS MV LEFT JOIN product_product AS PP ON (MV.product_id = PP.id)
                       LEFT JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                       LEFT JOIN product_uom AS PU  ON (MV.product_uom = PU.id)
                  WHERE (MV.location_id in (%s) or MV.location_dest_id in (%s)) AND MV.date >= '%s' AND MV.date < '%s'
                  AND MV.state = 'done'
                  """ % params
        self.env.cr.execute(sql_str)
        stock_id = self.env.cr.fetchall()

        if len(stock_id):
            for idx in stock_id:
                uom=idx[2]
                product=idx[1]
                if (not res.has_key(idx[0])):
                    res[idx[0]]=[]
                order=idx[5]
                date=idx[6]
                qty=idx[3]
                inv= idx[4]

                res[idx[0]].append({
                    'order': order,
                    'date': date,
                    'qty': qty,
                    'inv': inv,
                    'price': qty != 0 and inv / qty or 0.0,
                    'product':product,
                    'product_id':idx[0],
                    'default_code':idx[7],
                    'uom':uom,
                })

        return {
            'res':res,
            'total': total_inv,
        }

    @api.multi
    def report_print(self):
        data={}
        stock_report_res_obj = self.env['stock.move.report.result']

        date_start=self.date_start
        date_end=self.date_end
        stock_location=self.stock_location
        data.update({'date_start':date_start ,'date_end': date_end, 'stock_location':stock_location.id})
        key=self.get_report_vals(data)

        key['other']={
            'location': self.stock_location.name,
            'period':date_start+'--'+date_end,
        }

        try:
            #stock_report_res_obj
            sql_str = 'delete from stock_move_report_result where create_uid = ' + str(self.env.uid)
            self.env.cr.execute(sql_str)

            if len(key['res']):
                sql_str = 'insert into stock_move_report_result (order_name, op_date, qty, inv, price,product,uom,' \
                        'create_uid,write_uid,product_id,default_code,create_date,write_date) values '
                now_time = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
                for prod_id,pro_id_lines in key['res'].items():
                    for line in pro_id_lines:
                        sql_str += '(\'%s\',\'%s\',%s,%s,%s,\'%s\',\'%s\',%s,%s,%s,\'%s\',\'%s\',\'%s\'),' % (line['order'],line['date'],
                                line['qty'],line['inv'],line['price'],line['product'],line['uom'],self.env.uid,self.env.uid,
                                line['product_id'],line['default_code'],now_time,now_time)
                sql_str = sql_str[:-1]
                sql_str += ';'
                self.env.cr.execute(sql_str)
        except Exception,ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql_str))
            _logger.error('lines:' + repr(key['res']))
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': u'收发明细报表[%s：%s, %s]' % (date_start, date_end, self.stock_location.name),
                'view_type': 'form',
                'view_mode': 'pivot,tree,graph',
                'res_model': 'stock.move.report.result',
                'target': 'current',
                'view_id': False,
                'domain': [('create_uid', '=', self.env.uid)],
            }
        return False

