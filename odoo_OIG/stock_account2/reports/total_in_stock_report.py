# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging
from datetime import datetime, timedelta
import json

_logger = logging.getLogger(__name__)

class total_in_stock_report(models.TransientModel):
    _name = 'total.in.stock.report'
    _description = u" 仓库进销存汇总表"

    date_start = fields.Date(string=u'期初', required = True, default = datetime(datetime.today().year, datetime.today().month, 1))
    date_end = fields.Date(string=u'期末', required = True, default = datetime.today())
    stock_location = fields.Many2many("stock.location",  string= u'库位')

    @api.multi
    def report_print(self):
        ctx = dict(self._context or {})
        ctx['patch'] = False
        resp = self.with_context(ctx)

        # 将查询的时间转换为ODOO默认的伦敦时间
        date_start=date_start2=self.date_start
        date_end=date_end2=self.date_end
        date_start = datetime.strptime("%s 00:00:00" % date_start, "%Y-%m-%d %H:%M:%S") - timedelta(hours=8)
        date_end = datetime.strptime("%s 23:59:59" % date_end, "%Y-%m-%d %H:%M:%S") - timedelta(hours=8)

        # 拼接仓库id
        str_stock_location_ids = ''
        if len(self.stock_location):
            for stock_location in self.stock_location:
                if not str_stock_location_ids:
                    str_stock_location_ids = '%s' % stock_location.id
                else:
                    str_stock_location_ids += ',%s' % stock_location.id
        else:
            str_stock_location_ids = """select id from stock_location where usage = 'internal'"""

        try:
            # 清除暂存数据的表
            sql = 'delete from total_in_stock_report_result where create_uid = ' + str(self.env.uid)
            self.env.cr.execute(sql)

            now_time = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
            uid = self.env.uid
            # 向暂存数据表插入数据
            params = (str_stock_location_ids,date_start,str_stock_location_ids,date_start,str_stock_location_ids,date_start,date_end,
                      str_stock_location_ids, date_start, date_end,uid,uid,now_time,now_time)
            sql_str =  """ with res as(
                            ---期初+
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,
                                              SUM(MV.product_qty) AS quantity,
                                              SUM( MV.price_unit * MV.product_qty) AS inventory_value,
                                                                0 AS in_quantity,0 AS in_inventory_value,0 as adjust_in_inventory_value, 0 as out_quantity,0 AS out_inventory_value,0 as adjust_out_inventory_value
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_dest_id)       
                                              WHERE MV.location_dest_id in (%s) AND MV.date <= '%s' 
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            union all
                            ---期初-
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,
                                              SUM(-MV.product_qty) AS quantity,
                                              SUM( -MV.price_unit * MV.product_qty) AS inventory_value,
                                                                0 AS in_quantity,0 AS in_inventory_value,0 as adjust_in_inventory_value, 0 as out_quantity,0 AS out_inventory_value,0 as adjust_out_inventory_value
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_id)       
                                              WHERE MV.location_id in (%s) AND MV.date <= '%s' 
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            union all
                            
                            ---发生+
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,0,0,
                                              SUM(MV.product_qty) AS in_quantity,
                                              SUM(MV.price_unit * MV.product_qty) AS in_inventory_value,
                                                                SUM((MV.price_unit - MV.price_unit_origin) * MV.product_qty) AS asjust_in_inventory_value,0,0,0
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_dest_id)       
                                              WHERE MV.location_dest_id in (%s) AND MV.date >= '%s' AND MV.date <='%s'
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            union all
                            ---发生-
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,0,0,0,0,0,
                                              SUM(MV.product_qty) AS out_quantity,
                                              SUM(MV.price_unit * MV.product_qty) AS out_inventory_value,
                                                                SUM((MV.price_unit - MV.price_unit_origin) * MV.product_qty) AS asjust_out_inventory_value
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_id)       
                                              WHERE MV.location_id in (%s) AND MV.date >= '%s' AND MV.date <='%s'
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            )
                            INSERT INTO total_in_stock_report_result (product,product_id,default_code,stock_location,
                                    stock_location_id,qc_qty,qc_price,qc_inventory,qz_in_qty,qz_in_price,qz_in_inventory,
                                    qz_out_qty,qz_out_price,qz_out_inventory,adjust_inventory,qm_qty,qm_price,qm_inventory,
                                    create_uid,write_uid,create_date,write_date) 
                            SELECT
                                product_name,
                                product_id,
                                default_code,
                                stock_location_name,
                                stock_location_id,
                                --期初
                                SUM (quantity) AS quantity,
                                case when(SUM(quantity) = 0) then 0 ELSE(sum(inventory_value)/SUM (quantity)) END as price_unit,
                                sum(inventory_value) as inventory_value,
                                
                                --发生（收入） 
                                sum(in_quantity) as in_quantity,
                                case when(SUM(in_quantity) = 0) then 0 ELSE	(sum(in_inventory_value)/SUM (in_quantity)) END as in_price_unit,
                                sum(in_inventory_value) as in_inventory_value,
                            
                                --发生（发出）
                                sum(out_quantity) as out_quantity,
                                case when(SUM(out_quantity) = 0) then 0 ELSE (sum(out_inventory_value)/SUM (out_quantity)) END as out_price_unit,
                                sum(out_inventory_value) as out_inventory_value,
                                
                                -- 本期成本调整金额
                                case when (ftype = 'move_warehouse') then 0 ELSE (sum(adjust_in_inventory_value) + sum(adjust_out_inventory_value)) END as adjust_inventory_value,
                                -- 期末
                                (SUM (quantity) + sum(in_quantity) - sum(out_quantity)) as end_quantity,
                                case when (ftype = 'move_warehouse') then (case when((SUM (quantity) + sum(in_quantity) - sum(out_quantity)) = 0) then 0 ELSE	((sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value))/(SUM (quantity) + sum(in_quantity) - sum(out_quantity))) END )
                                ELSE (case when((SUM (quantity) + sum(in_quantity) - sum(out_quantity)) = 0) then 0 ELSE	((sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value) + sum(adjust_in_inventory_value) + sum(adjust_out_inventory_value))/(SUM (quantity) + sum(in_quantity) - sum(out_quantity))) END )END as end_price_unit,
                                case when (ftype = 'move_warehouse') then (sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value))
                                ELSE (sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value) + sum(adjust_in_inventory_value) + sum(adjust_out_inventory_value)) END as end_inventory_value,
                                '%s', '%s','%s','%s'
                            FROM
                                res
                            GROUP BY
                                product_name,
                                product_id,
                                default_code,
                                stock_location_name,
                                stock_location_id,
                                ftype
                            ORDER BY stock_location_id  """ % params
            self.env.cr.execute(sql_str)
        except Exception,ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql_str))
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': u'进销存报表[%s：%s]' % (date_start2, date_end2),
                'view_type': 'form',
                'view_mode': 'tree,pivot',
                'res_model': 'total.in.stock.report.result',
                'target': 'current',
                'view_id': False,
                'domain': [('create_uid', '=', self.env.uid)],
            }

        return False

    @api.multi
    def down_load(self):
        ctx = dict(self._context or {})
        ctx['patch'] = False
        resp = self.with_context(ctx)

        # 将查询的时间转换为ODOO默认的伦敦时间
        date_start = date_start2 = self.date_start
        date_end = date_end2 = self.date_end
        date_start = datetime.strptime("%s 00:00:00" % date_start, "%Y-%m-%d %H:%M:%S") - timedelta(hours=8)
        date_end = datetime.strptime("%s 23:59:59" % date_end, "%Y-%m-%d %H:%M:%S") - timedelta(hours=8)

        # 拼接仓库id
        str_stock_location_ids = ''
        if len(self.stock_location):
            for stock_location in self.stock_location:
                if not str_stock_location_ids:
                    str_stock_location_ids = '%s' % stock_location.id
                else:
                    str_stock_location_ids += ',%s' % stock_location.id
        else:
            str_stock_location_ids = """select id from stock_location where usage = 'internal'"""


        try:
            # 查询数据
            params = (str_stock_location_ids, date_start, str_stock_location_ids, date_start, str_stock_location_ids, date_start,
            date_end,str_stock_location_ids, date_start, date_end)
            sql_str = """ with res as(
                            ---期初+
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,
                                              SUM(MV.product_qty) AS quantity,
                                              SUM( MV.price_unit * MV.product_qty) AS inventory_value,
                                                                0 AS in_quantity,0 AS in_inventory_value,0 as adjust_in_inventory_value, 0 as out_quantity,0 AS out_inventory_value,0 as adjust_out_inventory_value
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_dest_id)       
                                              WHERE MV.location_dest_id in (%s) AND MV.date <= '%s' 
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            union all
                            ---期初-
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,
                                              SUM(-MV.product_qty) AS quantity,
                                              SUM( -MV.price_unit * MV.product_qty) AS inventory_value,
                                                                0 AS in_quantity,0 AS in_inventory_value,0 as adjust_in_inventory_value, 0 as out_quantity,0 AS out_inventory_value,0 as adjust_out_inventory_value
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_id)       
                                              WHERE MV.location_id in (%s) AND MV.date <= '%s' 
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            union all
    
                            ---发生+
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,0,0,
                                              SUM(MV.product_qty) AS in_quantity,
                                              SUM(MV.price_unit * MV.product_qty) AS in_inventory_value,
                                                                SUM((MV.price_unit - MV.price_unit_origin) * MV.product_qty) AS asjust_in_inventory_value,0,0,0
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_dest_id)       
                                              WHERE MV.location_dest_id in (%s) AND MV.date >= '%s' AND MV.date <='%s'
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            union all
                            ---发生-
                            SELECT MV.product_id, PT.name as product_name,pp.default_code,SL.id as stock_location_id,SL.name AS stock_location_name,SL.ftype,0,0,0,0,0,
                                              SUM(MV.product_qty) AS out_quantity,
                                              SUM(MV.price_unit * MV.product_qty) AS out_inventory_value,
                                                                SUM((MV.price_unit - MV.price_unit_origin) * MV.product_qty) AS asjust_out_inventory_value
                                              FROM stock_move AS MV 
                                              inner JOIN product_product AS PP ON (MV.product_id = PP.id)
                                              inner JOIN product_template AS PT ON (PP.product_tmpl_id = PT.id)
                                              inner join stock_location as SL on (SL.id = MV.location_id)       
                                              WHERE MV.location_id in (%s) AND MV.date >= '%s' AND MV.date <='%s'
                                              AND MV.state = 'done' GROUP BY  MV.product_id, PT.name,pp.default_code,SL.id
                            )
                        
                            SELECT
                                default_code,
                                product_name,
                                stock_location_name,
                                --期初
                                SUM (quantity) AS quantity,
                                case when(SUM(quantity) = 0) then 0 ELSE(sum(inventory_value)/SUM (quantity)) END as price_unit,
                                sum(inventory_value) as inventory_value,
    
                                --发生（收入） 
                                sum(in_quantity) as in_quantity,
                                case when(SUM(in_quantity) = 0) then 0 ELSE	(sum(in_inventory_value)/SUM (in_quantity)) END as in_price_unit,
                                sum(in_inventory_value) as in_inventory_value,
    
                                --发生（发出）
                                sum(out_quantity) as out_quantity,
                                case when(SUM(out_quantity) = 0) then 0 ELSE (sum(out_inventory_value)/SUM (out_quantity)) END as out_price_unit,
                                sum(out_inventory_value) as out_inventory_value,
    
                                -- 本期成本调整金额
                                case when (ftype = 'move_warehouse') then 0 ELSE (sum(adjust_in_inventory_value) + sum(adjust_out_inventory_value)) END as adjust_inventory_value,
                                -- 期末
                                (SUM (quantity) + sum(in_quantity) - sum(out_quantity)) as end_quantity,
                                case when (ftype = 'move_warehouse') then (case when((SUM (quantity) + sum(in_quantity) - sum(out_quantity)) = 0) then 0 ELSE	((sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value))/(SUM (quantity) + sum(in_quantity) - sum(out_quantity))) END )
                                ELSE (case when((SUM (quantity) + sum(in_quantity) - sum(out_quantity)) = 0) then 0 ELSE ((sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value) + sum(adjust_in_inventory_value) + sum(adjust_out_inventory_value))/(SUM (quantity) + sum(in_quantity) - sum(out_quantity))) END )END as end_price_unit,
                                case when (ftype = 'move_warehouse') then (sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value))
                                ELSE (sum(inventory_value) + sum(in_inventory_value) - sum(out_inventory_value) + sum(adjust_in_inventory_value) + sum(adjust_out_inventory_value)) END as end_inventory_value
                            FROM
                                res
                            GROUP BY
                                product_name,
                                product_id,
                                default_code,
                                stock_location_name,
                                stock_location_id,
                                ftype
                            ORDER BY stock_location_id  """ % params
            # 替换sql中的特殊字符
            sql_str = sql_str.replace('=','%3d').replace('+','%2B')
            # 设置表头
            title = ['产品编码','产品名称','仓库','期初数量','期初价格','期初金额','期间入库数量','期间入库价格','期间入库金额',
                     '期间出库数量','期间出库价格','期间出库金额','调整金额','期末数量','期末价格','期末金额']
            # 将数据封装并转为json
            result = {'titles': title, 'filename': '进销存报表.xlsx', 'command': sql_str}
            jsonstr = json.dumps(result)
        except Exception, ex:
            _logger.error('Exception:' + repr(ex))
            _logger.error('sql:' + repr(sql_str))
        else:
            return {'type': 'ir.actions.act_url',
                    'url': '/web/excel/download?para=%s' % jsonstr,
                    'target': 'new'}








