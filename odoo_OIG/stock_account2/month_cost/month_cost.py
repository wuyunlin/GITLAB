# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
import calendar
import datetime
from odoo.exceptions import UserError


class ProductProduct(models.Model):
    _inherit = "product.product"
    
    @api.multi
    def _set_standard_price(self, value):
        ''' Store the standard price change in order to be able to retrieve the cost of a product for a given date'''
        if not self._context.get('not_set_history_price', False):
            super(ProductProduct, self)._set_standard_price(value)


class product_price_history(models.Model):
    _inherit = 'product.price.history'

    stock_qty = fields.Float(string=u"库存")


class MonthCostReport(models.AbstractModel):
    _name = 'report.stock_account2.month_cost_computing_report'

    @api.model
    def render_html(self, docids, data=None):
        return self.env['report'].render('stock_account2.month_cost_computing_report', data)


class MonthCostComputer(models.Model):
    _name = 'account.month.cost.computer'
    _description = u'全月加权平均成本计算表'

    name = fields.Char(u'说明', required=True, readonly=True, states={'draft': [('readonly', False)]} )
    start_date = fields.Date(u'计算期初日期', required=True, readonly=True, states={'draft': [('readonly', False)]})
    end_date = fields.Date(u'计算期末日期', required=True, readonly=True, states={'draft': [('readonly', False)]})
    state = fields.Selection([ ('draft', u'未计算'), ('done', u'已计算'),], u'状态', required=True, readony = True, default='draft')
    company_id = fields.Many2one('res.company', u'公司', required=True, readonly=True, states={'draft': [('readonly', False)]}, default=lambda self: self.env['res.company']._company_default_get('account.month.cost.computer'))

    @api.multi
    def cost_calculation(self):
        self.ensure_one()
        start_date = self.start_date
        end_date = self.end_date
        log_report = self.month_cost_calculation(start_date, end_date, self.company_id.id)
        #log_report:  产品id, 上月数量, 上月金额, 期间入库数量, 期间入库金额, 期末数量, 期末成本价
        datas = {
            'ids': [],
            'model': 'account.month.cost.computer',
            'logs': log_report.values()
        }
        return self.env['report'].get_action([self.id], 'stock_account2.month_cost_computing_report', data=datas)

    @api.model
    def month_cost_calculation(self, start_date, end_date, company_id):
        """ 1) 根据当月入库的Stock Move，计算全月一次加权平均价格，计算结果设置到product standard_price，同时设置到product.price.history，日期为计算月1日
             2) 计算结果同时更新到当月出库的Stock Move的price_unit
             3) 计算原理是，汇总该月所有采购入库的Stock Move，取其数量和价格，加上上月的数量及价格，计算本月数量及价格
        """
        lock_date = max(self.company_id.period_lock_date, self.company_id.fiscalyear_lock_date)
        if self.user_has_groups('account.group_account_manager'):
            lock_date = self.company_id.fiscalyear_lock_date
        if lock_date and start_date <= lock_date:
            raise UserError(u"该日期【%s】已经财务关账，成本价格不可再计算！" % (start_date, ) )
        
        location_ids = self.env['stock.location'].search([('usage','=','supplier')])
        location_ids = [x.id for x in location_ids]
        out_location_ids = self.env['stock.location'].search([('usage','not in', ['internal', ] )])
        out_location_ids = [x.id for x in out_location_ids]
        # 求这个月stock_move的入库数量及金额
        date_end = '%s 23:59:59' % end_date
        start_date = "%s 00:00:00" % start_date

        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S") - datetime.timedelta(hours=8)
        date_end = datetime.datetime.strptime(date_end, "%Y-%m-%d %H:%M:%S") - datetime.timedelta(hours=8)
        # 排除已有会计凭证的Picking
        self.env.cr.execute("""select distinct p.name from stock_move m right join stock_picking p on (m.picking_id = p.id) 
        where  m.date >= %s and m.state = 'done' and m.company_id = %s and p.account_move_id is not null """,  [start_date, company_id])
        err = self.env.cr.fetchall()
        if err:
            err = [x[0] for x in err]
            raise UserError(u"下列Picking已有会计凭证，请先删除再行成本价格计算：%s" % (err, ) )
        
        # 产品id, 上月数量, 上月金额, 期间入库数量, 期间入库金额, 期末数量, 期末成本价
        computing_log = {}
        
        # 获取当期入库的Stock Move
        self.env.cr.execute("""select product_id, sum(product_uom_qty), sum(price_unit*product_uom_qty)  from stock_move
                      where  date >= %s and date <= %s and location_id in %s and state = 'done' and company_id = %s group by product_id
                      """,  [start_date, date_end, tuple(location_ids), company_id ])
        this_month_cost =  {}
        for r in self.env.cr.fetchall():
            this_month_cost[r[0]] = {'qty': r[1], 'val': r[2]}
            log = [0, 0, 0, 0, 0, 0, 0, '', '']
            log[0] = r[0]
            log[3] = r[1]
            log[4] = r[2]
            computing_log[r[0]] = log

        # 删除本月成本价格
        #计算本月成本价格
        for k in this_month_cost:
            old_val = 0
            old_qty = 0
            # 上月(最近)历史价格及库存
            self.env.cr.execute("""delete from product_price_history where product_id = %s and company_id = %s
                    and datetime >= %s """,  [k, company_id, start_date] )
            self.env.cr.execute("""select cost, stock_qty from product_price_history where product_id = %s
                    and company_id = %s order by datetime desc """,  [k, company_id ] )
            pre_month_cost = self.env.cr.fetchone()
            if pre_month_cost:
                old_val = pre_month_cost[0] * (pre_month_cost[1] or 0)
                old_qty = pre_month_cost[1] or 0
            this_month_cost[k]['cost'] = (this_month_cost[k]['val'] + old_val) / (this_month_cost[k]['qty']  + old_qty)
            log = computing_log[k]
            log[1] = old_qty
            log[2] = old_val
            log[6] = this_month_cost[k]['cost']
            computing_log[k] = log
        
        #计算本月库存
        prt_ids = this_month_cost.keys()
        ctx = dict(self._context or {})
        ctx.update({'to_date': str(date_end) })
        ctx.update({'force_company': company_id })
        products = self.env['product.product'].with_context(ctx).browse(prt_ids) 
        stock_dic = {}
        for prt in products:
            stock_dic[prt.id] = prt.qty_available
            log = computing_log[prt.id]
            log[5] = prt.qty_available
            log[7] = prt.default_code
            log[8] = prt.name

        #create price history
        history_obj = self.env['product.price.history']
        price_ids = []
        for k in this_month_cost:
            #本月末无库存
            if not stock_dic.has_key(k):
                this_month_cost[k]['qty'] = 0
            else:
                this_month_cost[k]['qty'] = stock_dic[k]
            cost = this_month_cost[k]['cost'] 
            price = history_obj.create({
                                            'product_id': k,
                                            'cost': cost,
                                            'stock_qty': this_month_cost[k]['qty'],
                                            'datetime': start_date,
                                            'company_id': company_id
                                            })
            price_ids.append(price.id)
            # 更新产品成本价格
            self.env['product.product'].browse(k).with_context(not_set_history_price=True).write({'standard_price': cost })
            # 更新出库成本价格
            self.env.cr.execute("""update stock_move set price_unit = %s 
            where  date >= %s and date <= %s and location_dest_id in %s and state = 'done' and product_id = %s and company_id = %s 
              """,  [cost, start_date, date_end, tuple(out_location_ids), k, company_id ])
        return computing_log
