# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging
from odoo.exceptions import ValidationError
from datetime import datetime


class stock_picking_entry(models.TransientModel):
    _name = "stock.picking.entry"
    _description = u"存货会计凭证"
    
    @api.multi
    def create_entry(self):
        picking_obj = self.env['stock.picking']
        picking_ids = self._context.get('active_ids', False)
        entry_ids = picking_obj.browse(picking_ids)._create_entry()
        action = self.env['ir.model.data'].xmlid_to_object('account.action_move_journal_line')
        res = { 'name': action.name,
            'type': action.type,
            'target': action.target,
            'view_mode': action.view_mode,
            'view_type': action.view_type,
            'res_model': action.res_model,
        }
        res['domain'] = "[('id','in', ["+','.join(map(str,entry_ids))+"])]"
        return res


class stock_inventory_entry(models.TransientModel):
    _name = "stock.inventory.entry"
    _description = u"盘点会计凭证"

    @api.multi
    def create_inventory_entry(self):
        inventory_obj = self.env['stock.inventory']
        entry_ids = inventory_obj.create_inventory_entry(inventory_ids)
        action = self.env['ir.model.data'].xmlid_to_object('account.action_move_journal_line')
        res = { 'name': action.name,
            'type': action.type,
            'target': action.target,
            'view_mode': action.view_mode,
            'view_type': action.view_type,
            'res_model': action.res_model,
        }
        res['domain'] = "[('id','in', ["+','.join(map(str,entry_ids))+"])]"
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
