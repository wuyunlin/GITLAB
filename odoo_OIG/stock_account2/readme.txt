Odoo成本价格计算剖析
    def action_done(self):
        self.product_price_update_before_done()
        res = super(StockMove, self).action_done()
        self.product_price_update_after_done()
        return res

0）Stock Picking Confirm时候，如果Stock Move的price_unit没有值，set_default_price_unit_from_product取得product_id.standard_price写入Stock Move的price_unit。（采购入库的情况，系统创建入库单时候，已经将去税的采购价格写入stock move的price_unit）
1）Stock Move的action_done
1.1）self.product_price_update_before_done先计算移动加权平均价格（如果采购入库，且是移动加权平均成本法，其他情况则不计算），写入产品的standard_price。
1.2）实际发生移动，并产生Quant
1.3）Quant产生的时候，创建会计凭证。
1.3.1）如果是移动平均成本法，采购入库金额取自Quant的cost（最初来自PO价格），其他情况（包括出库、采购退货、销售退货）金额取自产品成本价格self.product_id.standard_price
1.3.2）如果是先进先出成本法，金额取自Quant的cost（不管是入库，还是出库）
1.3.3）其他成本法（标准成本法），产品成本价格（不管是入库还是出库）取自self.product_id.standard_price
1.4）self.product_price_update_after_done()，如果是出库（目标库位非internal，包括出库到在途库位），且是先进先出成本法，计算该Stock Move的Quants的平均价格，该价格更新到产品的standard_price，以及Stock Move的price_unit

综上：
1）标准成本法
1.1）采购入库Stock Move价格取自PO单的去税价格，其他入库、出库、调拨、盘点的价格取自product_id.standard_price
1.2）存货会计凭证（不管入库、出库）的金额取自product_id.standard_price

2）移动加权平均成本法
2.1）采购入库的Stock Move价格取自PO单的去税价格
2.2）其他入库、出库、调拨、盘点的Stock Move价格confirm时候取自product_id.standard_price （stock move confirm时候的价格）
2.3）采购入库的存货会计凭证金额取自PO单的去税价格
2.4）其他入库、出库、调拨、盘点的存货会计凭证金额取自product_id.standard_price （stock move实际完成时候的价格）

3）先进先出成本法
3.1）采购入库的Stock Move价格取自PO单的去税价格，其他入库的价格取自product_id.standard_price
3.2）出库的Stock Move（包括盘亏）价格取自Quants的平均价格
3.3）（不管入库、出库、调拨）存货会计凭证金额取自Quants的cost。
3.4）盘盈的情况，盘盈入库的价格取自product_id.standard_price，而后产生Quant，此价格写入Quant的cost。入库完成后，又将Quant价格写入Stock Move及product_id.standard_price

4）强制出库产生的负数Quant的补平
4.1）如果强制出库，则产生负数Quant，其cost取自product_id.standard_price。
4.2）以后入库补平负数Quant时候，系统自动用补平Quant替换负数Quant，并产生差价的会计凭证。
4.3）如果是先进先出成本法，且是出库，系统用新cost更新锁定负数Quant的Stock Move的price_unit

5）冲销发出商品
5.1) 实时会计凭证，且Saxon的情况，客户发票确认时候，系统取得product_id.standard_price作为冲销发出商品的成本价格。
5.2) 移动平均价格，客户发票确认时候的product_id.standard_price，有可能异于出库时候的product_id.standard_price
5.3) FIFO情况，客户发票确认时候的product_id.standard_price，有可能异于出库时候写入的product_id.standard_price


