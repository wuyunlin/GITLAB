# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
import json
from datetime import datetime, date, timedelta
import pytz


class PreviewReport(models.AbstractModel):
    _name = 'report.oig_download.preview_setting'

    @api.model
    def render_html(self, docids, data=None):
        return self.env['report'].render('oig_download.preview_setting', data)

condition1_selection=[('>', '>'),
                             ('<', '<'),
                             ('=', '='),
                             ('>=', '>='),
                             ('<=', '<='),
                             ('like', '包含,前后用%分号'),
                             ('in', '包含[多个值,用逗号隔开]')]
class base_view(models.Model):
    _name = "base.view"
    self_dict={'para1':'key1','para2':'key2','para3':'key3'}
    _description = u'自定义导出数据'
    view_id = fields.Many2one('base.view.info', u'报表')
    name = fields.Text(related='view_id.view', readonly=True,string=u'对应的数据库视图')
    remark = fields.Char(related='view_id.remark', readonly=True,string=u'报表备注')
    para1 = fields.Char(related='view_id.para1', readonly=True,string=u'条件1')
    condition1 = fields.Selection(condition1_selection, string=u'关系1')
    value1 = fields.Char(u'值1')
    para2= fields.Char(related='view_id.para2', readonly=True,string=u'条件2')
    condition2 = fields.Selection(condition1_selection, string=u'关系2')
    value2 = fields.Char(u'值2')
    para3= fields.Char(related='view_id.para3', readonly=True,string=u'条件3')
    condition3 = fields.Selection(condition1_selection, string=u'关系3')
    value3 = fields.Char(u'值3')
    para4= fields.Char(related='view_id.para4', readonly=True,string=u'条件4')
    condition4 = fields.Selection(condition1_selection, string=u'关系4')
    value4 = fields.Char(u'值4')
    para5 = fields.Char(related='view_id.para5', readonly=True, string=u'条件5')
    condition5 = fields.Selection(condition1_selection, string=u'关系5')
    value5 = fields.Char(u'值5')
    para6 = fields.Char(related='view_id.para6', readonly=True, string=u'条件6')
    condition6 = fields.Selection(condition1_selection, string=u'关系6')
    value6 = fields.Char(u'值6')
    model_name = fields.Char(u'模型')
    is_func = fields.Boolean(related='view_id.is_func', readonly=True, string=u'是函数')

    @api.model
    def default_get(self, fields):
        res = super(base_view, self).default_get(fields)
        active_model = self.env.context.get('active_model')
        if active_model:
            sql="""select base_view_info_id from user_report_set where model='%s' order by seq limit 1"""%(active_model)
            self._cr.execute(sql)
            fet=self._cr.fetchone()
            if fet:
                res['view_id']=fet[0]
        return res
    ####获取查询语句
    def get_query(self):
        view_name = self.view_id.view
        name = self.view_id.name
        ###视图
        if not self.view_id.is_func:
            query = 'select * from %s where 1=1 ' % (view_name)
            if self.view_id.key1 and self.condition1 and self.value1:
                query += """ and %s %s '%s' """ % (self.view_id.key1, self.condition1, self.value1)
            if self.view_id.key2 and self.condition2 and self.value2:
                query += """ and %s %s '%s' """ % (self.view_id.key2, self.condition2, self.value2)
            if self.view_id.key3 and self.condition3 and self.value3:
                query += """ and %s %s '%s' """ % (self.view_id.key3, self.condition3, self.value3)
            if self.view_id.key4 and self.condition4 and self.value4:
                query += """ and %s %s '%s' """ % (self.view_id.key4, self.condition4, self.value4)
            if self.view_id.key5 and self.condition5 and self.value5:
                query += """ and %s %s '%s' """ % (self.view_id.key5, self.condition5, self.value5)
            if self.view_id.key6 and self.condition6 and self.value6:
                query += """ and %s %s '%s' """ % (self.view_id.key3, self.condition3, self.value6)
            return query
        ###函数
        else:
            para=[]
            if self.view_id.key1:
                para.append(self.value1 or '')
            if self.view_id.key2:
                para.append(self.value2 or '')
            if self.view_id.key3:
                para.append(self.value3 or '')
            if self.view_id.key4:
                para.append(self.value4 or '')
            if self.view_id.key5:
                para.append(self.value5 or '')
            if self.view_id.key6:
                para.append(self.value6 or '')
            func_query=view_name%tuple(para)
            query='select * from %s'%func_query
            return query
    def get_title(self):
        view_name = self.view_id.view
        ###视图
        if not self.view_id.is_func:
            if len(view_name.split('.'))==1:
                table_schema='public'
            else:
                table_schema = view_name.split('.')[0]
            query = """SELECT  column_name FROM information_schema.columns WHERE table_name = '%s' and table_schema='%s'""" % ( view_name.split('.')[-1],table_schema)
            self._cr.execute(query)
            result = self._cr.fetchall()
            title = [x[0] for x in result]
        else:
            title=self.view_id.title and self.view_id.title.split(',')
        return title
    # 执行下载操作
    def down_load(self):
        view_name = self.view_id.view
        name = self.view_id.name
        query=self.get_query()
        title=self.get_title()
        context_tz = pytz.timezone(u'Asia/Shanghai')
        current_now=context_tz.localize(datetime.now()).strftime('%Y-%m-%d %H:%M:%S')
        result = {'titles': title,'filename': name + current_now+'.xlsx', 'command': query}
        jsonstr = json.dumps(result)
        jsonstr=jsonstr.replace('%','%25').replace('=', '%3d').replace('+', '%2B')
        return {'type': 'ir.actions.act_url',
                'url': '/web/excel/download?para=%s' % jsonstr,
                'target': 'new'}

    # 执行预览操作
    def preview_information_setting(self):
        name = self.view_id.name
        view_name = self.view_id.view
        log_report = []
        view_title = []
        view_title.append(name)
        titles = self.get_title()
        log_report.append(titles)
        query=self.get_query()
        self._cr.execute(query)
        fet2 = self._cr.fetchall()
        if not fet2:
            raise UserError("没有查到数据！")
        for i in fet2:
            log = []
            for j in i:
                log.append(j)
            log_report.append(log)
        datas = {
            'ids': [],
            'model': 'base.view',
            'logs': log_report,
            'title': view_title,
            # 'header': view_header
        }
        return self.env['report'].get_action([self.id], 'oig_download.preview_setting', data=datas)
