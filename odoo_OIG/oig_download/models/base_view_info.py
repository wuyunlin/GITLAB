# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
class base_view_info(models.Model):
    _name = "base.view.info"
    _description = u'自定义导出数据'
    view = fields.Text(u'报表')
    name = fields.Char(u'下载文件名称')
    remark = fields.Char(u'报表备注')
    para1 = fields.Char(u'条件1')
    key1 = fields.Char(u'key1')
    para2 = fields.Char(u'条件2')
    key2 = fields.Char(u'key2')
    para3 = fields.Char(u'条件3')
    key3 = fields.Char(u'key3')
    para4 = fields.Char(u'条件4')
    key4 = fields.Char(u'key4')
    para5 = fields.Char(u'条件5')
    key5 = fields.Char(u'key5')
    para6 = fields.Char(u'条件6')
    key6 = fields.Char(u'key6')
    is_func = fields.Boolean(u'是函数')
    title = fields.Text(u'列名,以逗号隔开')


