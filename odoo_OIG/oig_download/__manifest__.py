﻿{
    'name': '视图下载',
    'version': '1.0',
    "category": "Generic Modules/Test",
    'description': """
     """,
    'author': 'OIG',
    'depends': ['oig_base'],
    'data': [
        'views/base_view_info.xml',
        'views/base_view.xml',
        'views/preview_information_report.xml',
        ],
    'installable': True,
    'auto_install': False,
    'application': True,
}