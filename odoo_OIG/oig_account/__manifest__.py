﻿{
    'name': '会计',
    'version': '1.0',
    "category": "Generic Modules/Test",
    'description': """
     """,
    'author': 'OIG',
    'depends': ['oig_base'],
    'data': [
        'views/carry_over.xml',
        'views/account_move_view.xml',
        'views/account_journal_view.xml',
        'views/account_account_view.xml',
        'wizard/batch_account_reverse_view.xml',
        'security/ir.model.access.csv',
        ],
    'installable': True,
    'auto_install': False,
    'application': True,
}