# coding=utf-8
_author_ = 'zhangqiang'

from odoo import api, fields, models, tools

class account_journal(models.Model):
    _inherit = 'account.journal'
    # 是否允许反过账
    update_posted = fields.Boolean(u'是否允许反过账', default=False)
