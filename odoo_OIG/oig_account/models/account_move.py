# -*- coding: utf-8 -*-
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError
import odoo.addons.decimal_precision as dp
import datetime
import xlrd
import base64
class account_move(models.Model):
    _inherit = 'account.move'

    # 制单人
    create_uid = fields.Many2one('res.users', string=u'制单人')
    # 修改人
    write_uid = fields.Many2one('res.users', string=u'修改人')

    file = fields.Binary(string=u'导入文件', filters='*.xls')

    # 导入凭证
    def create_account_move(self):
        pt_length = self.env['ir.config_parameter'].sudo().get_param('pt_length')
        amount_currency_length = self.env['ir.config_parameter'].sudo().get_param('pt_length')
        now = datetime.datetime.now() .strftime("%Y-%m-%d %H:%M:%S")
        try:
            excel = xlrd.open_workbook(file_contents=base64.decodestring(self.file))
        except:
            raise UserError('请选择导入文件!')
        sh = excel.sheet_by_index(0)
        # 接收xlsx导入的产品
        dict = {}
        account_move_lines = []
        amount_aa = 0
        amount_bb = 0
        wai_jie = 0
        wai_dai = 0
        for rx in range(sh.nrows):
            if rx == 0:
                continue
            data = [str(sh.cell(rx, i).value or '') for i in range(8)]
            if data[0]:
                if data[0][-2:] == '.0':
                    account_account = self.env['account.account'].search([('code', '=', data[0][:-2])])
                else:
                    account_account = self.env['account.account'].search([('code', '=', data[0])])
                # 科目
                dict['account_id'] = account_account.id
            else:
                raise UserError("模板中没有科目数据，请核对模板数据！")
            # 辅助科目
            if data[1]:
                res_partner = self.env['res.partner'].search([('name', '=', data[1].strip()), ('type_flag', '=', 'platform')])
                if res_partner:
                    dict['partner_id'] = res_partner.id
                else:
                    dict['partner_id'] = ''
            else:
                dict['partner_id'] = ''
            # 摘要
            dict['name'] = data[2]
            # # 外币金额
            # dict['account_currency'] = data[3]
            # 币种
            if data[3]:
                res_currency = self.env['res.currency'].search([('name', '=', data[3])])
                dict['currency_id'] = res_currency.id
            else:
                dict['currency_id'] = ''
            # 借方
            if data[4] == '':
                dict['debit'] = 0
            else:
                dict['debit'] = round(float(data[4]), int(pt_length))
                amount_aa += round(float(data[4]), int(pt_length))
            # 贷方
            if data[5] == '':
                dict['credit'] = 0
            else:
                dict['credit'] = round(float(data[5]), int(pt_length))
                amount_bb += round(float(data[5]), int(pt_length))
            # 外币借方
            if data[6] == '':
                dict['amount_currency_debit'] = 0
            else:
                dict['amount_currency_debit'] = round(float(data[6]), int(amount_currency_length))
                wai_jie += round(float(data[6]), int(amount_currency_length))

            # 外币贷方
            if data[7] == '':
                dict['amount_currency_credit'] = 0
            else:
                dict['amount_currency_credit'] = round(float(data[7]), int(amount_currency_length))
                wai_dai += round(float(data[7]), int(amount_currency_length))

            # 外币金额
            dict['amount_currency'] = dict['amount_currency_debit'] - dict['amount_currency_credit']

            if dict['account_id']:
                account_invoice_line = {'account_id': dict['account_id'], 'partner_id': dict['partner_id'], 'debit': dict['debit'], 'credit': dict['credit'], 'name': dict['name'], 'currency_id': dict['currency_id'], 'amount_currency': dict['amount_currency'], 'amount_currency_debit': dict['amount_currency_debit'], 'amount_currency_credit': dict['amount_currency_credit']}
                account_move_lines.append((0, 0, account_invoice_line))
            else:
                raise UserError(data[0]+"，这个科目有问题，请检查！")
        print "+++++++++借方总金额："+str(amount_aa)
        print "贷方总金额+++++："+str(amount_bb)
        print "外币借方总金额+++++："+str(wai_jie)
        print "外币贷方总金额+++++："+str(wai_dai)
        if account_move_lines:
            self.write({'line_ids': account_move_lines})
        return True

    # 重写创建方法，增加验证如果有子级科目则不允许创建凭证
    @api.model
    def create(self, vals):
        res = super(account_move, self).create(vals)
        res.update_name(1)
        return res
    @api.multi
    def write(self, vals):
        res = super(account_move, self).write(vals)
        if vals.get('name') or vals.get('date'):
            for each in self:
                each.update_name(0)
        return res
    @api.one
    def update_name(self, flag):
        def new_name():
            journal = self.journal_id
            if journal.sequence_id:
                sequence = journal.sequence_id
                new_name = sequence.with_context(ir_sequence_date=self.date).next_by_id()
                self._cr.execute("""update account_move set name='%s' where id=%s"""%(new_name,self.id)   )
        ######flag=0 表示更新默认不用更新号码；flag=1表示新建凭证时候；默认需要号码
        if flag==0:
            ######判断是否需要改号码
            sql_if="""select count(*) from account_move  where id=%s and  substring(name from 1 for 4)<> to_char(date,'YYmm')"""%self.id
            self._cr.execute(sql_if)
            fet_if=self._cr.fetchone()
            #########判断是否需要调整凭证号
            if fet_if[0]:
                flag=1
        #####flag=1 需要更新号码 那么需要先找断号，找不到则从号码序列获取
        if flag==1:
            #########是否有断号
            sql_dh="""with res as (
            select b.id,a.name,cast(row_number() over (order by a.name)+cast(to_char(b.date,'YYmm') as int)*1000000 as varchar) 
             as num from account_move a
             inner join account_move b on b.id=%s
             where substring(a.name from 1 for 4)= to_char(b.date,'YYmm') and substring(b.name from 1 for 4)<> to_char(b.date,'YYmm')
            order by a.name 
            )
            select id from res where res.name<>res.num limit 1 FOR UPDATE NOWAIT"""%(self.id)
            self._cr.execute(sql_dh)
            fet_dh = self._cr.fetchone()
            if fet_dh and fet_dh[0]:
                sql="""with res as (
                select b.id,a.name,cast(row_number() over (order by a.name)+cast(to_char(b.date,'YYmm') as int)*1000000 as varchar) 
                 as num from account_move a
                 inner join account_move b on b.id=%s
                 where substring(a.name from 1 for 4)= to_char(b.date,'YYmm') and substring(b.name from 1 for 4)<> to_char(b.date,'YYmm')
                order by a.name 
                ),
                res2 as (
                select id, num from res where res.name<>res.num limit 1 FOR UPDATE NOWAIT
                )
                update account_move set name=res2.num from res2 where res2.id=account_move.id;"""%(self.id)
                self._cr.execute(sql)
            else:
                new_name()
        else:
            return True
        return True




    @api.multi
    @api.depends('name', 'state')
    def name_get(self):
        result = []
        for move in self:
            if move.state == 'draft' and 0:
                name = '* ' + str(move.id)
            else:
                name = move.name
            result.append((move.id, name))
        return result




class account_move_line(models.Model):
    _inherit = 'account.move.line'

    # 余额
    debit = fields.Monetary(default=0.0, currency_field='company_currency_id', digits=dp.get_precision('Product Price'))
    credit = fields.Monetary(default=0.0, currency_field='company_currency_id', digits=dp.get_precision('Product Price'))
    balance = fields.Float(compute='_store_balance', store=True, currency_field='company_currency_id',
        help="Technical field holding the debit - credit in order to open meaningful graph views from reports",digits=dp.get_precision('Product Price'))
    # 是否需要辅助科目
    move_partner = fields.Boolean(u'是否需要辅助科目', related="account_id.has_partner")

    amount_currency_debit = fields.Float(u'外币借方', digits=dp.get_precision('Amount Currency'), default=0)

    amount_currency_credit = fields.Float(u'外币贷方', digits=dp.get_precision('Amount Currency'), default=0)

    _sql_constraints = [
        ('amount_currency_credit_debit', 'CHECK (amount_currency_debit*amount_currency_credit=0)', '外币借方和外币贷方乘积不能为0！'),
    ]

    @api.onchange('amount_currency_debit', 'amount_currency_credit')
    def _onchange_amount_currency(self):
        if self.amount_currency_debit and not self.amount_currency_credit:
            self.amount_currency = self.amount_currency_debit
            return
        elif not self.amount_currency_debit and self.amount_currency_credit:
            self.amount_currency = -self.amount_currency_credit
            return
        else:
            self.amount_currency = 0
            return

    @api.onchange('amount_currency', 'currency_id')
    def onchange_amount_currency(self):
        # 外币金额
        currency_balance = self.amount_currency
        # 外币币种
        foreign_currency = self.currency_id

        if currency_balance and foreign_currency:

            local_currency = self.env.user.company_id.currency_id
            # 本位币
            local_amount = foreign_currency.with_context().compute(currency_balance, local_currency)
            if currency_balance < 0:
                self.credit = abs(local_amount)
                self.debit = 0
                return
            else:
                self.debit = local_amount
                self.credit = 0
                return
        else:
            self.debit = 0
            self.credit = 0
            return

    @api.constrains('account_id')
    def _check_account(self):
        for stock_move in self:
            account_account = self.env['account.account'].search([('id', '=', stock_move.account_id.id)])
            if account_account.has_child == True:
                raise UserError("科目:"+account_account.code+" "+account_account.name+ "  不能创建凭证！")
            else:
                pass
class CurrencyRate(models.Model):
    _inherit = 'res.currency.rate'
    rate = fields.Float(digits=(12, 20), help='The rate of the currency to the currency of rate 1')

class Currency(models.Model):
    _inherit = "res.currency"
    rate = fields.Float(compute='_compute_current_rate', string='Current Rate', digits=(12, 20),
                        help='The rate of the currency to the currency of rate 1.')
