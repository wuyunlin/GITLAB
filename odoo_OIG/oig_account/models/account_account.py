# -*- coding: utf-8 -*-
from odoo import api, fields, models, SUPERUSER_ID


class account_account(models.Model):
    _inherit = 'account.account'

    # 余额方向
    balance_direction = fields.Char(u'余额方向')

    # 是否有子科目标识
    has_child = fields.Boolean(u'是否有子科目')

    # 是否需要辅助科目
    has_partner = fields.Boolean(u'是否需要辅助科目')



