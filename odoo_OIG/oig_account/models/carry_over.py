# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
import datetime
class carry_over(models.Model):
    _name = "carry.over"
    _description = u'费用结转'

    start_date = fields.Date(u'开始时间')

    end_date = fields.Date(u'结束时间')

    state = fields.Selection([
        ('new', u'新建'),
        ('post', u'过账')
    ], string=u'状态', default="new")

    account_move_id = fields.Many2one('account.move', u'结转凭证')
    # 结转科目(以"4"开头的科目)
    account_code = fields.Char(u'结转科目', compute='_compute_account_code')
    # 日记账
    journal_id = fields.Many2one('account.journal', string='日记账', required=True)

    @api.multi
    def _compute_account_code(self):
        for account_move in self:
            account_move.account_code = account_move.env['ir.config_parameter'].sudo().get_param('account_move_project')
        return True

    def account_move_do(self):
        if self.start_date and self.end_date:
            if self.start_date > self.end_date:
                raise UserError("开始时间不能大于结束时间！")
            now = datetime.datetime.now() .strftime("%Y-%m-%d")
            account_move_dict = {}
            account_move_dict['ref'] = "费用结转"
            account_move_dict['journal_id'] = self.journal_id.id
            account_move_dict['date'] = now
            adjust_amount = 0
            account_lines = []
            sql = """SELECT
                        aml.credit,
                        aml.debit,
                        aml.account_id,
                        aml.name,
                        aml.partner_id
                    FROM
                        account_move am
                    INNER JOIN account_move_line aml ON aml.move_id = am. ID
                    INNER JOIN account_account aa ON aa. ID = aml.account_id
                    WHERE
                        am. DATE >= '%s'
                    AND am. DATE <= '%s'
                    AND aa.code LIKE '6%%'
                    AND am. STATE = 'posted'
                    """%(self.start_date, self.end_date)
            self._cr.execute(sql)
            fet = self._cr.fetchall()
            for move in fet:
                adjust_amount += move[1] or -move[0]
                account_lines.append((0, 0, {
                      'account_id': move[2],
                      'partner_id': move[4],
                      'debit': move[0],
                      'credit': move[1],
                      'journal_id': self.journal_id.id,
                      'name': move[3]
                }))
            if adjust_amount:
                account_lines.append((0, 0, {'account_id': self.env['account.account'].search([('code', '=', self.env['ir.config_parameter'].sudo().get_param('account_move_project'))]).id,
                                             'debit': adjust_amount > 0 and adjust_amount or 0,
                                             'credit': adjust_amount < 0 and -adjust_amount or 0,
                                             'journal_id': self.journal_id.id,
                                             'name': '费用结转：【%s-%s】'%(self.start_date, self.end_date)}))
            if account_lines:
                account_move_dict['line_ids'] = account_lines
                accountresult = self.env['account.move'].create(account_move_dict)
                self.write({'account_move_id': accountresult.id})
                self.write({'state': 'post'})
            else:
                raise UserError("没有以6开头的科目无法生成凭证！")
