# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo
#    Copyright (C) 2013-2016 CodUP (<http://codup.com>).
#
##############################################################################
from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError
# 批量确认订单
class Batch_account_reverse(models.TransientModel):
    _name = "batch.account.reverse"
    _description = "Batch Account Reverse"

    @api.multi
    def button_cancel(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for move in self.env['account.move'].browse(active_ids):
            if move.state == "posted":
                move.button_cancel()
        return {'type': 'ir.actions.act_window_close'}
