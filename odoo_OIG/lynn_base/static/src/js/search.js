// odoo.define('lynn_base.search', function (require) {
//     "use strict";
//     var SearchView = require('web.SearchView');
//     SearchView.include({
//         init: function () {
//             var self = this;
//             self._super.apply(self, arguments);
//             self.visible_filters = (localStorage.visible_search_menu !== 'false');
//         }
//     });
// });

odoo.define('lynn_base.ListView', function (require) {
"use strict";
var ControlPanelMixin = require('web.ControlPanelMixin');
var core = require('web.core');
var time = require('web.time');
var ListView = require('web.ListView');
var Model = require('web.DataModel');
var session = require('web.session');
var Widget = require('web.Widget');
var view = require('web.View');
var data = require('web.data');
var data_manager = require('web.data_manager');
var pyeval = require('web.pyeval');
var QWeb = core.qweb;
var _t = core._t;
var _lt = core._lt;
var StateMachine = window.StateMachine;

ListView.List.include({
    /**
     * Extend the render_buttons function of ListView by adding an event listener
     * on the import button.
     * @return {jQuery} the rendered buttons
     */
    get_selection: function () {
        var result = {ids: [], records: []};
        // if (!this.options.selectable) {
        //     return result;
        // }
        var records = this.records;
        this.$current.find('td.o_list_record_selector input:checked')
                .closest('tr').each(function () {
            var record = records.get($(this).data('id'));
            result.ids.push(record.get('id'));
            result.records.push(record.attributes);
        });
        return result;
    },
});




ListView.include({
    /**
     * Extend the render_buttons function of ListView by adding an event listener
     * on the import button.
     * @return {jQuery} the rendered buttons
     */
    render_buttons: function() {
        var self = this;
        var add_button = false;
        if (!this.$buttons) { // Ensures that this is only done once
            add_button = true;
        }
        this._super.apply(this, arguments); // Sets this.$buttons
        this.$buttons.on('click', '.o_list_button_action_1', oe_myservice_1.bind(this));
        this.$buttons.on('click', '.o_list_button_action_2', oe_myservice_2.bind(this));
        this.$buttons.on('click', '.o_list_button_action_3', oe_myservice_3.bind(this));
    },
});





function oe_myservice_1() {
    var self = this;
    var call_kw = '/web/dataset/call_kw';
    var model = this.dataset.model;
    var method = 'oe_myservice_1';
    var args = [];
    args.push(self.groups.get_selection().ids)
    session.rpc(
        call_kw + '/' + model + '/' + method,
        {
            model: model,
            method: method,
            args: args,
            kwargs: {},
            context: this.getParent().action.context
        },
        undefined
    ).then(function(res) {
        self.reload();
    });
}


//lynn  add tree button end
function oe_myservice_3() {
    var self = this;
    this.trigger_up('clear_uncommitted_changes', {
        callback: function() {
            self.rpc("/web/action/load", { action_id: "oig_download.action_base_view" }).done(function(result) {
                result.context = {'active_model':self.dataset.model,
                                   'default_res_users_id':session.uid,
                                   'active_ids':self.groups.get_selection().ids,
                                   'default_model_name':self.dataset.model};
                self.do_action(result);
            });
        },
    });
}

function oe_myservice_2() {
    var self = this;
    this.trigger_up('clear_uncommitted_changes', {
        callback: function() {
            self.rpc("/web/action/load", { action_id: "lynn_base.action_users_tree_set_wizard" }).done(function(result) {
                result.context = {'active_model':self.dataset.model,
                                   'default_res_users_id':session.uid,
                                   'active_ids':self.groups.get_selection().ids,
                                   'default_model_name':self.dataset.model};
                self.do_action(result);
            });
        },
    });
}

// if true, the 'Import', 'Export', etc... buttons will be shown
//ListView.prototype.defaults.import_enabled = false;
// ListView.include({
//     /**
//      * Extend the render_buttons function of ListView by adding an event listener
//      * on the import button.
//      * @return {jQuery} the rendered buttons
//      */
//     render_buttons: function() {
//         var self = this;
//         var add_button = false;
//         if (!this.$buttons) { // Ensures that this is only done once
//             add_button = true;
//         }
//         this._super.apply(this, arguments); // Sets this.$buttons
//         this.$buttons.on('click', '.o_list_button_action_1', oe_myservice_1.bind(this));
//         this.$buttons.on('click', '.o_list_button_action_2', oe_myservice_2.bind(this));
//     },
// });


});






odoo.define('lynn_base.Widget.View', function (require) {
"use strict";
var ControlPanelMixin = require('web.ControlPanelMixin');
var core = require('web.core');
var time = require('web.time');
var ListView = require('web.ListView');
var Model = require('web.DataModel');
var session = require('web.session');
var Widget = require('web.Widget.');
var View = require('web.view');

var QWeb = core.qweb;
var _t = core._t;
var _lt = core._lt;
var StateMachine = window.StateMachine;

});






