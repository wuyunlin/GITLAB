# -*- coding: utf-8 -*-
from odoo import api, fields, models
import logging
import base64
import datetime
_logger = logging.getLogger(__name__)

import sys
reload(sys)
sys.setdefaultencoding('utf8')

class export_data_to_excel(models.TransientModel):
    _name = "export.data.to.excel"
    _description = u"excel导出数据参数配置"
    name = fields.Char(string='Action Usage')
    command = fields.Char(string='Action Usage')
    model = fields.Char(string='Action Usage')
    field = fields.Char(string='Action Usage')
    file_name = fields.Char(string='Action Usage')

class users_tree_set(models.Model):
    _name = "users.tree.set"
    _description = u"用户列表视图字段配置"
    name = fields.Char(string=u'字段名称')
    field_description = fields.Char(string=u'字段标签')
    ttype = fields.Char(string=u'字段类型')
    res_users_id = fields.Many2one('res.users', u'用户',ondelete='cascade')
    model_id = fields.Many2one('ir.model', u'模型',ondelete='cascade')
    model_name = fields.Char(string=u'模型名')
    sequence = fields.Integer(string=u'序号')



class users_tree_set_wizard(models.TransientModel):
    _name = "users.tree.set.wizard"
    _description = u"用户列表视图字段配置页面"
    res_users_id = fields.Many2one('res.users', u'用户', ondelete='cascade')
    model_id = fields.Many2one('ir.model', u'模型',ondelete='cascade')
    model_name = fields.Char(string=u'模型名')
    line = fields.One2many('users.tree.set.wizard.line', 'users_tree_set_wizard_id', string=u'配置明细')
    @api.one
    def confirm(self):
        model_name,res_users_id=self.model_name,self.res_users_id.id
        ############更新 用户对应模型展示字段信息表
        sql="""delete from users_tree_set where model_name='%s' and res_users_id=%s;
                insert into users_tree_set(name,model_name,sequence,res_users_id,
                ttype,field_description)
                select b.name,a.model_name,sequence,a.res_users_id,b.ttype,
                b.field_description from users_tree_set_wizard a
                left join users_tree_set_wizard_line b on b.users_tree_set_wizard_id=a.id
                where a.model_name='%s' and a.res_users_id=%s and b.is_visible=True
                and a.id=%s"""%(model_name,res_users_id,model_name,res_users_id,self.id)
        self.env.cr.execute(sql)
        return True
    @api.model
    def default_get(self, fields):
        settings = super(users_tree_set_wizard, self).default_get(fields)
        cr=self.env.cr
        context=self._context or {}
        model,res_users_id=context.get('active_model'),context.get('default_res_users_id')
        ########根据模型和用户 获取用户信息表&模型表里的内容 作为默认值展示
        if model and res_users_id:
            sql="""
                 select row_number() over() as sequence,a.name,a.field_description,a.ttype ,
                 case when b.id is not null then True else  False end as is_visible
                 from ir_model_fields a 
                 left join users_tree_set b on b.model_name=a.model and b.res_users_id=%s and b.name=a.name
                 where a.model='%s' order by  a.id"""%(res_users_id,model)
            cr.execute(sql)
            fet=cr.dictfetchall()
            if fet:
                line=[(0,0,one) for one in fet]
                settings.update({'line':line})
        return settings


class users_tree_set_wizard_line(models.TransientModel):
    _name = "users.tree.set.wizard.line"
    _description = u"用户列表视图字段配置明细"
    _order='is_visible desc,sequence'
    name = fields.Char(string=u'字段名称')
    field_description = fields.Char(string=u'字段标签')
    ttype = fields.Char(string=u'字段类型')
    is_visible = fields.Boolean(string=u'是否可见')
    sequence = fields.Integer('Sequence', default=0)
    users_tree_set_wizard_id = fields.Many2one('users.tree.set.wizard', u'用户列表视图字段配置页面', ondelete='cascade')








