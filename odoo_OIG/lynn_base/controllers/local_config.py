# -*- coding: utf-8 -*-
import odoo
import pytz
from datetime import datetime
from odoo.tools import ustr
from odoo.fields import Datetime
from odoo.models import BaseModel
import logging
import json
import itertools
from lxml import etree
from odoo.exceptions import AccessError, MissingError, ValidationError, UserError
from odoo import api
from odoo import http
from cStringIO import StringIO
from xlsxwriter.workbook import Workbook
from odoo.http import content_disposition, dispatch_rpc, request, \
                      serialize_exception as _serialize_exception
import re
#import datetime
import base64

_logger = logging.getLogger(__name__)

##########lynn 导出数据的时候 按照用户设置时区转换时间
def convert_to_export(self, value, record):
    """ convert `value` from the cache to a valid value for export. The
        parameter `env` is given for managing translations.
    """
    #将存储在数据库中的UTC时区的datetime字段转换成用户本机时区
    current_timezone = record.env.context.get('tz', False)
    if current_timezone == False:
        current_timezone = u'Asia/Shanghai'
    current_tz = pytz.timezone(current_timezone)
    utc_tz = pytz.timezone('UTC')
    if value:
        value_datetime = datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
        utc_tz_datetime = utc_tz.localize(value_datetime, is_dst=None)
        value = utc_tz_datetime.astimezone(current_tz)
        localized_time_str = value.strftime("%Y-%m-%d %H:%M:%S")
    else:
        localized_time_str=False
    if record.env.context.get('export_raw_data'):
        return localized_time_str
    return bool(localized_time_str) and ustr(localized_time_str)

Datetime.convert_to_export = convert_to_export


@api.model
def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
    """ fields_view_get([view_id | view_type='form'])

    Get the detailed composition of the requested view like fields, model, view architecture

    :param view_id: id of the view or None
    :param view_type: type of the view to return if view_id is None ('form', 'tree', ...)
    :param toolbar: true to include contextual actions
    :param submenu: deprecated
    :return: dictionary describing the composition of the requested view (including inherited views and extensions)
    :raise AttributeError:
                        * if the inherited view has unknown position to work with other than 'before', 'after', 'inside', 'replace'
                        * if some tag other than 'position' is found in parent view
    :raise Invalid ArchitectureError: if there is view type other than form, tree, calendar, search etc defined on the structure
    """
    View = self.env['ir.ui.view']

    result = {
        'model': self._name,
        'field_parent': False,
    }

    # try to find a view_id if none provided
    if not view_id:
        # <view_type>_view_ref in context can be used to overrride the default view
        view_ref_key = view_type + '_view_ref'
        view_ref = self._context.get(view_ref_key)
        if view_ref:
            if '.' in view_ref:
                module, view_ref = view_ref.split('.', 1)
                query = "SELECT res_id FROM ir_model_data WHERE model='ir.ui.view' AND module=%s AND name=%s"
                self._cr.execute(query, (module, view_ref))
                view_ref_res = self._cr.fetchone()
                if view_ref_res:
                    view_id = view_ref_res[0]
            else:
                _logger.warning('%r requires a fully-qualified external id (got: %r for model %s). '
                                'Please use the complete `module.view_id` form instead.', view_ref_key, view_ref,
                                self._name)

        if not view_id:
            # otherwise try to find the lowest priority matching ir.ui.view
            view_id = View.default_view(self._name, view_type)

    # context for post-processing might be overriden
    if view_id:
        # read the view with inherited views applied
        root_view = View.browse(view_id).read_combined(['id', 'name', 'field_parent', 'type', 'model', 'arch'])
        result['arch'] = root_view['arch']
        result['name'] = root_view['name']
        result['type'] = root_view['type']
        result['view_id'] = root_view['id']
        result['field_parent'] = root_view['field_parent']
        # override context from postprocessing
        if root_view['model'] != self._name:
            View = View.with_context(base_model_name=root_view['model'])
    else:
        # fallback on default views methods if no ir.ui.view could be found
        try:
            arch_etree = getattr(self, '_get_default_%s_view' % view_type)()
            result['arch'] = etree.tostring(arch_etree, encoding='utf-8')
            result['type'] = view_type
            result['name'] = 'default'
        except AttributeError:
            raise UserError(_("No default view of type '%s' could be found !") % view_type)

    # Apply post processing, groups and modifiers etc...
    xarch, xfields = View.postprocess_and_fields(self._name, etree.fromstring(result['arch']), view_id)
    result['arch'] = xarch
    result['fields'] = xfields
    # Add related action information if aksed
    if toolbar:
        toclean = ('report_sxw_content', 'report_rml_content', 'report_sxw', 'report_rml', 'report_sxw_content_data',
                   'report_rml_content_data')

        def clean(x):
            x = x[2]
            for key in toclean:
                x.pop(key, None)
            return x

        IrValues = self.env['ir.values']
        resprint = IrValues.get_actions('client_print_multi', self._name)
        resaction = IrValues.get_actions('client_action_multi', self._name)
        resrelate = IrValues.get_actions('client_action_relate', self._name)
        resprint = [clean(print_)
                    for print_ in resprint
                    if view_type == 'tree' or not print_[2].get('multi')]
        resaction = [clean(action)
                     for action in resaction
                     if view_type == 'tree' or not action[2].get('multi')]
        # When multi="True" set it will display only in More of the list view
        resrelate = [clean(action)
                     for action in resrelate
                     if (action[2].get('multi') and view_type == 'tree') or (
                     not action[2].get('multi') and view_type == 'form')]

        for x in itertools.chain(resprint, resaction, resrelate):
            x['string'] = x['name']

        result['toolbar'] = {
            'print': resprint,
            'action': resaction,
            'relate': resrelate,
        }
    #lynn 用户配置字段 及隐藏字段
    if view_type == 'tree':
        ###########系统开关 配置是否可以用户自定义tree字段
        cr=self.env.cr
        def get_flag():
            sql="""select count(*) from ir_config_parameter  where key='users_set' and value='True' """
            cr.execute(sql)
            return cr.fetchone()[0]
        flag=get_flag()
        if flag:
            ##########根据 uid model 获取所需要展示的字段
            def get_fields(uid,name):
                sql="""select name from users_tree_set where model_name='%s' and res_users_id=%s order by sequence"""%(name,uid)
                cr.execute(sql)
                fet=cr.fetchall()
                return fet and [x[0] for x in fet] or []
            re_arch = result['arch']
            re_arch_et = etree.fromstring(re_arch)
            uid_fields = get_fields(self._uid, self._name)
            for child in re_arch_et:
                if child.tag == 'field' and uid_fields:
                    ######没有设置 则设置隐藏属性
                    if  child.attrib.get('name') not in uid_fields:
                        modifiers = json.loads(child.attrib.get('modifiers') or '{}')
                        child.attrib['invisible'],modifiers['tree_invisible'] = '1',True
                        child.attrib['modifiers'] = json.dumps(modifiers)

            arch=etree.tostring(re_arch_et, encoding='utf-8')
            arch_tree = etree.fromstring(arch)
            arch_tree_copy = etree.fromstring(arch)
            uid_fields = get_fields(self._uid, self._name)
            ######所有用户设置的字段全部移除

            def if_match(node, kv_map):
                '''''判断某个节点是否包含所有传入参数属性
                   node: 节点
                   kv_map: 属性及属性值组成的map'''
                for key in kv_map:
                    if node.attrib.get(key) <> kv_map.get(key) or node.tag <> 'field':
                        return False
                return True

            def get_node_by_keyvalue(nodelist, kv_map):
                '''''根据属性及属性值定位符合的节点，返回节点
                   nodelist: 节点列表
                   kv_map: 匹配属性及属性值map'''
                result_nodes = []
                for node in nodelist:
                    if if_match(node, kv_map):
                        result_nodes.append(node)
                return result_nodes
            for child in arch_tree:
                if child.tag == 'field' and uid_fields and child.attrib.get('name')  in uid_fields:
                    arch_tree.remove(child)
            for field in  uid_fields:
                result_nodes=get_node_by_keyvalue(arch_tree_copy,{"name": field})
                if result_nodes:
                    for node in result_nodes:
                        arch_tree.append(node)
                else:
                    x=1
                    # demo = """<tree>
                    #                 <field name="partner_ref" modifiers="{}"/>
                    #             </tree>"""
                    # demo_arch = etree.fromstring(demo)
                    # demo_arch[0].attrib['name']=field
                    # arch_tree.append(demo_arch[0])
            result['arch'] = etree.tostring(arch_tree, encoding='utf-8')

    return result
BaseModel.fields_view_get = fields_view_get


@api.multi
def oe_myservice(self):
    return True
BaseModel.oe_myservice_1 = oe_myservice




###########################sql导出excel 以后移动到公共模块

class ExportData2Excel(http.Controller):
    _cp_path = "/ExportData2Excel"
    _rows_data = []
    _commands = {}
    _fields = {}
    _file_names = {}
    def get_data_from_sql(self,sql):
        #try:
        from odoo.service import security
        dbname = request.session.db
        #security.check(request.session.db, request.session.uid, request.session.password)
        registry = odoo.registry(dbname).check_signaling()
        registry.signal_caches_change()
        with registry.cursor() as cr:
            cr.execute(sql)
            self._rows_data = cr.fetchall()
        #except Exception:
            #self._rows_data = [(u'你未登录或登录超时!!',)]

    def make_data(self, titles, rows):
        fp = StringIO()
        workbook = Workbook(fp)
        worksheet = workbook.add_worksheet('Sheet 1')

        cell_format = workbook.add_format(
            {'bold': True, 'bg_color': 'silver', 'color': 'green', 'size': 16, 'text_h_align': 2, 'text_v_align': 2,
             'shrink': 1})
        for i, field_name in enumerate(titles):
            worksheet.write(0, i, field_name)
        worksheet.set_row(0, 30, cell_format, {'collapsed': True})

        cell_format = workbook.add_format({'size': 10, 'align': 'vcenter', 'shrink': 1})
        for row_index, row in enumerate(rows):
            for cell_index, cell_value in enumerate(row):
                cell_value = self.format_cell_value(cell_value)
                worksheet.write(row_index + 1, cell_index, cell_value)
                # worksheet.set_column(cell_index, cell_index, 15)
            worksheet.set_row(row_index + 1, 20, cell_format, {'collapsed': True})
        workbook.close()
        fp.seek(0)
        data = fp.read()
        fp.close()
        return data

    @http.route('/web/excel/download', type='http', auth='public')
    def download_document(self, ** kw):
        para=kw['para'] and json.loads(kw['para']) or {}
        command=para.get('command',"""select 'command not exsit'""")
        filename=para.get('filename','导出.xlsx')
        titles=para.get('titles',['导出'])
        self.get_data_from_sql(command)
        return request.make_response(
            self.make_data(titles, self._rows_data),
            headers=[
                ('Content-Disposition', 'attachment; filename="%s"' % filename.decode("latin-1")),
                # ('Content-Type', 'application/vnd.ms-excel')
                ('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            ]
        )

    def format_cell_value(self, cell_value):
        if isinstance(cell_value, basestring):
            cell_value = re.sub("\r", " ", cell_value)
        # cell_value = type(cell_value)(cell_value)
        if cell_value != 0 and not cell_value:
            cell_value = None
        return cell_value

ExportData2Excel()




###########################sql导出excel 以后移动到公共模块






















