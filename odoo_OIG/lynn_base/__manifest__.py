# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
#
# {
#     'name': 'LYNN basfix',
#     'version': '10.0.1.0',
#     'category': 'base',
#     'summary': 'base',
#     'description': """
#     """,
#     'depends': ['base'],
#     'data': ["views/local_data.xml"
#
#     ],
# #'qweb': ['static/src/xml/base_lynn.xml'],
#     'installable': True,
#     'auto_install': False,
#     'application': True,
# }
{
    "name": "LYNN basfix",
    "version": "10.0.1.0.0",
    "author": "lynn",
    "category": "base",
    "website": "http://www.serpentcs.com",
    "depends": ["base",'web', 'web_kanban'],
    'license': "AGPL-3",

    "data": [
        "views/local_data.xml",
        "security/ir.model.access.csv",
    ],
    'qweb': ['static/src/xml/base_lynn.xml'],

    'installable': True,
    'auto_install': False,
    'application': True,
}