# -*- coding: utf-8 -*-
import sys
import logging
from odoo import http
from odoo.http import request
from psycopg2 import extras
from psycopg2._psycopg import DatabaseError
from odoo.addons.china_area.controllers.china_area_data import china_area_date

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')

class init_city(http.Controller):
    def __init__(self):
        reload(sys)
        sys.setdefaultencoding('utf-8')
        
        #检查数据版本
        request.cr.execute(""" select  version from china_area""""")       
        databaseVersion =request.cr.dictfetchall()
        jsonDataVersion = china_area_date.json_date['version']
        versionValues=[]
        versionValues.append(jsonDataVersion)
        
        isExistsVersionDate = False
        
        for result in databaseVersion:
            if result['version'] == jsonDataVersion:
                    isExistsVersionDate = True
                    break
        #检查城市表是否有数据
#         request.cr.execute(""" select count(*) from res_country_state_city""")       
#         isExists =request.cr.dictfetchall()
        
#         if int(isExists[0]['count']):
        #通过version判断
        if isExistsVersionDate:
            _logger.info("china area data already exists")
        else:
            #新表，初始化数据
            _logger.info("not found city data, auto insert data ... ")
            
            jsonDate = china_area_date.json_date['address']
            state_args = []
            city_args = []
            district_args = []
            del_args = []
                
            #res_country -- find china
            request.cr.execute("""select id from res_country where name ='China'  """)       
            china_id=request.cr.dictfetchall()
                
            for province in jsonDate:
                #添加省份
                #id,code,name,country_id,type
                s_tuple = province['id'],'CNs',province['name'],china_id[0]['id'],province['type']
                state_args.append(s_tuple)
                
                delete_state = province['id'],
                del_args.append(delete_state)
                
                
                if 'city' in province:
                    for cityIndex,citys in enumerate(province['city']):
                        #id,name,parent_id,state_id,type
                        c_tuple = citys['id'],citys['name'],province['id'],citys['type'] 
                        city_args.append(c_tuple)
                                            
                        if 'district' in citys: 
                            for district in citys['district']:
                                #id,name,parent_id,state_id,type 
                                d_tuple = district['id'],district['name'],citys['id'],district['type']  
                                district_args.append(d_tuple)
    
            try:
                #添加版本信息
                request.cr.execute("insert into china_area(version) values(%s)" ,versionValues)
                #删除旧的省、城、区
                request.cr.execute("delete from res_country_state_city_county")
                request.cr.execute("delete from res_country_state_city")
                request.cr.executemany("delete from res_country_state where id  in (%s)" ,del_args)
                
                #保存省份
                request.cr.executemany("insert into res_country_state (id,code,name,country_id,type) values(%s,%s,%s,%s,%s)" , state_args)
                #保存城市
                request.cr.executemany("insert into res_country_state_city (id,name,parent_id,type ) values(%s,%s,%s,%s )" , city_args)
                #保存县区
                request.cr.executemany("insert into res_country_state_city_county (id,name,parent_id,type ) values(%s,%s,%s,%s )" , district_args)
                request.cr.commit()
                _logger.info("china area data save success")
            except DatabaseError,e:
                _logger.info("china area data save error, infomation:" +str(e))
                request.cr.rollback()
     
    @http.route(['/app_api/init_city'], type='json', auth="user")
    def init_city(self,**kwargs):
        pass
