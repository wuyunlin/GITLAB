# -*- coding: utf-8 -*-
import logging
import json
from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')

#定义APP应用
class china_area(models.Model):
    _name = "china.area"
    _description = 'china area' 
    
    name=fields.Char("name" )  
    parent_id = fields.Char("parent_id" )  
    type = fields.Char("type" )  
    zip = fields.Char("zip" )  
    version = fields.Char("version" )  

#country.state属性
class custom_country_state(models.Model):
    _inherit = 'res.country.state'
    type = fields.Char("type" ) 
    city_ids = fields.One2many('res.country.state.city', 'parent_id', string='Cities') 

    _sql_constraints = [
        ('name_code_uniq', 'unique(id)', 'The code of the state must be unique by country !')
    ]


#定义城市
class custom_country_state_city(models.Model):
    _name = "res.country.state.city"
    _description = u'城市'
    
    name=fields.Char("name" )  
    parent_id = fields.Many2one("res.country.state", 'State', ondelete='restrict')  
    type = fields.Char("type" )  
    zip = fields.Char("zip" )  
    county_ids = fields.One2many('res.country.state.city.county', 'parent_id', string='counties')

#定义县区
class custom_country_state_city_county(models.Model):
    _name = "res.country.state.city.county"
    _description = u'县区'
    
    name=fields.Char("name" )  
    parent_id = fields.Many2one("res.country.state.city", 'City', ondelete='restrict')  
    type = fields.Char("type" )  
    zip = fields.Char("zip" )  