# -*- coding: utf-8 -*-
{
    'name': "HollyWant china area manager",

    'summary': """
        模块加载自动进行省市区的初始化""",

    'description': """
    """,


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        #'china_area_view.xml',
        'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        
    ],
    "installable":True,
    'application': True,
    "auto_install":False,
}