{
    'name': 'stock config group',
    'description': '财务权限管理组',
    'author': 'holly-want',
	'category': 'HollyWant',
    'depends': ['base','stock'],
    'data':[
        'security/security_stock_group_config.xml',
    ],
    'installable': True,
}
# -*- coding: utf-8 -*-
