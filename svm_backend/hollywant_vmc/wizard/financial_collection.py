# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from odoo.http import request
from odoo.osv import osv
from odoo.exceptions import ValidationError
from odoo import fields, models, api
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)

# welcome email sent to portal users
# (note that calling '_' has no effect except exporting those strings for translation)


class financial_collection(models.TransientModel):
    """
        A wizard to manage the creation/removal of portal users.
    """
    _name = 'financial.collection'
    _description = u'财务收款'

    supply_id = fields.Many2one('res.users', required=True, string='补货员' )
    date = fields.Date(string='收款日期')
    amount = fields.Float(string='应收款总额', digit=(16,2))
    real_amount = fields.Float(string='实际收款总额', digit=(16, 2))
    rate = fields.Float(string='误差(率)', digit=(16, 3))
    description = fields.Char(string='误差(率)', )
    welcome_message = fields.Text(string='Invitation Message',)

    # @api.multi
    # def unlink(self):
    #     raise ValidationError('不能删除，因为有关联！')
    #     return super(financial_collection, self).unlink()

    @api.model
    def default_get(self, cr):
        result1 = []
        context = self.env.context
        if context is None:
            context = {}
        if context and context.get('active_ids', False):
            if len(context.get('active_ids')) < 1:
                raise osv.except_osv(_('Warning!'), _("请至少选择一个取款明细!"))
        sql="""
        select create_uid,sum(amount),current_date from vmc_money_record where id in %s and coalesce(state,'draft')='draft' and type in ('get')  group by create_uid"""%(tuple(context.get('active_ids')+[0]),)
        request.cr.execute(sql)
        fet=request.cr.fetchall()
        if not fet:
            raise osv.except_osv(_('Warning!'), _("请至少选择一条取款记录来做财务收款操作!"))
        if len(fet)>1:
            raise osv.except_osv(_('Warning!'), _("一次只可以为一个补货员进行财务收款操作!"))
        create_uid,amount,date=fet[0]
        res = super(financial_collection, self).default_get(cr)
        res.update({'amount':amount,'date':date,'supply_id':create_uid})
        return res

    @api.multi
    def onchange_amount(self, amount, real_amount,context=None):
        res={}
        context = self.env.context
        if round(amount, 2)*round(real_amount, 0) == 0:
            rate=0
            description = ''
        else:
            rate=abs(1-round(round(real_amount,2)/round(amount,2),3))
            if rate<0.003:
                description = '%s%%'%(rate*100)
            elif rate<0.05:
                description = '%s%%(请及时盘点)' % (rate*100)
            else:
                warning={'title': _('Warning'),'message': _('当前误差率(%s%%)>(5%%),不允许进行财务付款操作'%(rate*100))}
                return {'value':{'real_amount':0},'warning':warning}
        res['value']={'description':description,'rate':rate,'welcome_message':description}
        return res

    @api.one
    def action_apply(self):
        context = self.env.context
        #wizard = self.browse(ids[0], context)
        wizard = self
        vals={'company_id':wizard.supply_id.company_id.id,'real_amount':wizard.real_amount,'amount':wizard.amount,'supply_id':wizard.supply_id.id,'date':wizard.date,'rate':wizard.rate,'description':wizard.welcome_message}
        if wizard.rate>0.05:
            raise osv.except_osv(_('Warning!'), _('当前误差率(%s%%)>(5%%),不允许进行财务付款操作'%(wizard.rate*100)))
        if not wizard.real_amount:
            return True
        financial_id=self.env['vmc.financial.record'].create(vals)
        sql="""update vmc_money_record set state='done', vmc_financial_record_id =%s where id in %s and type='get' and create_uid=%s"""%(int(financial_id),tuple(context.get('active_ids')+[0]),wizard.supply_id.id)
        sql2="""with a as (select adjust_stock_id  from vmc_money_record where id in %s and create_uid=%s )
             update vmc_money_record  set state='confirm' from a where vmc_money_record.adjust_stock_id =a.adjust_stock_id and vmc_money_record.type='giveby_supply'"""%(tuple(context.get('active_ids')+[0]),wizard.supply_id.id)
        self.env.cr.execute(sql)
        self.env.cr.execute(sql2)
        mod_obj = self.env['ir.model.data']
        act_obj = self.env['ir.actions.act_window']
        view = mod_obj.get_object_reference( 'hollywant_vmc', 'hollywant_vmcmonery_financial_action')
        view_id = view and view[1] or False
        result = act_obj.browse(view_id).read([view_id])[0]
        result['domain'] = [('id', '=', int(financial_id.id))]
        return result
