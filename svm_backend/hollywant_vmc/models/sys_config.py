# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp import models, fields, api
import logging

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')

#定义APP应用
class sys_config(models.Model):
    _name = "sys.config"
    _order="group_flag asc"
    _description = 'system config,key value'   
    name=fields.Char("Key name",size=64,required=True)
    code=fields.Char("Key code",size=64,required=True)
    code_value=fields.Char("Key Value",size=128)
    group_flag=fields.Char("Group Flag",size=64,default="config") 
    comment=fields.Text("Comment")    
    _sql_constraints = [
        ('code', 'unique (code)', 'code must unique !')
    ]
    #根据键值得到value
    @api.model
    def get_value(self,codevalue):
        domain = [('code', '=', codevalue)]
        sys_config = self.browse(self.search(domain).ids)       
        if len(sys_config)>0:
            return sys_config[0]['code_value']
        else:
            return ''   

#     图片
    image=fields.Binary("Image")    