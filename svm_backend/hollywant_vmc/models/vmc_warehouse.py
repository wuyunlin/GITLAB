# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.http import request
import logging

import pytz

from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
from ..controllers.common import localizeStrTime

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')


def localizeStrTime(request, utcTimeStr, fromFormat, toFormat):
    """
    localize utc a time str gotten from database
    params:
    request : httprequest
    utcTimeStr:"2016-01-14 00:00:00"
    fromFormat:"%Y-%m-%d %H:%M:%S"
    toFormat:"%Y-%m-%d %H:%M:%S"
    """
    utc = pytz.timezone('UTC')
    # context_tz = pytz.timezone(request.context['tz'] or u'Asia/Shanghai')
    context_tz = pytz.timezone(u'Asia/Shanghai')
    utc_time = utc.localize(datetime.strptime(utcTimeStr, fromFormat), is_dst=False)  # UTC = no DST
    localized_time = utc_time.astimezone(context_tz)  # 将utc_time转为 context_tz时区的时间
    localized_time_str = localized_time.strftime(toFormat)
    return localized_time_str


class StockLocation(models.Model):
    _inherit = 'stock.location'

    qty_warning = fields.Integer(string='Qty Warning')


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'
    stock_type = fields.Selection(
        [('common', '一般仓库'), ('car', '车库'), ('vmc', '自动售货机')],
        string='仓库类型',
        default='common'
    )

    @api.onchange('stock_type')
    def on_change_stock_type(self, stock_type):
        return {'value': {'stock_type': stock_type}}

    user_id=fields.Many2one('res.users', string="Related Account")
    vmc_user_id = fields.Many2one('res.partner', string="Related Account")
    vmc_location_count = fields.Integer(
        string='Line Number',
        compute='_get_vmc_data',
        default=1
    )
    vmc_qty_warning = fields.Integer(
        string='Product Qty Warning',
        compute='_get_vmc_data'
    )
    cash_balance = fields.Char(string="现金余额")

    @api.onchange('vmc_location_count')
    def _check_vmc_location_count(self):
        if self.vmc_location_count <= 0:
            return {'warning': {'title': _('Warning!'), 'message': _(
                'The number of line must be greather than zero')}}

    @api.onchange('stock_type')
    def _change_stock_type(self):
        if self.stock_type == 'vmc':
            self.reception_steps = 'one_step'
            self.delivery_steps = 'ship_only'

    def _get_vmc_data(self):
        for warehouse in self:
            if warehouse.stock_type == 'vmc':
                warehouse.update({
                    'vmc_location_count': len(warehouse.lot_stock_id.child_ids),
                    'vmc_qty_warning': warehouse.lot_stock_id.qty_warning
                })

    # @api.model
    # def create(self, vals):
    #     warehouse = super(StockWarehouse, self).create(vals)
    #     if warehouse and warehouse.stock_type == 'vmc':
    #         warehouse.lot_stock_id.qty_warning = vals.get('vmc_qty_warning')
    #         for loc in range(1, vals.get('vmc_location_count') + 1):
    #             # 创建售货机货道
    #             self.env['stock.location'].create({
    #                 'name': _('Line') + str(loc),
    #                 'active': True,
    #                 'usage': 'internal',
    #                 'location_id': warehouse.lot_stock_id.id,
    #                 'posx': loc,
    #                 'posy': 0,
    #                 'posz': 0,
    #                 'company_id': warehouse.lot_stock_id.company_id.id,
    #                 'qty_warning': vals.get('vmc_qty_warning'),
    #                 'comment': 'vmc'
    #             })

    #         # 创建售货机账号
    #         vmc_name = self.env['ir.sequence'].next_by_code('res.users.vmc')
    #         vmc_user = self.env['res.users'].create({
    #             'login': vmc_name,
    #             'name': vmc_name,
    #             'customer_type': 2,
    #             'customer': True
    #         })
    #         warehouse.vmc_user_id = vmc_user.partner_id

    #     return warehouse

    # @api.multi
    # def write(self, vals):
    #     result = super(StockWarehouse, self).write(vals)
    #     if 'vmc_qty_warning' in vals:
    #         for warehouse in self.filtered(lambda r: r.stock_type == 'vmc'):
    #             warehouse.lot_stock_id.qty_warning = vals.get(
    #                 'vmc_qty_warning')
    #             warehouse.lot_stock_id.child_ids.write(
    #                 {'qty_warning': vals.get('vmc_qty_warning')}
    #             )

    #     return result


    @api.multi
    # 通过货道，商品，数量自动调拨到销售库存
    def synchronize_vmc_inventory_reduction(self, **kwargs):
        _logger.info(
            "synchronize_vmc_inventory_reduction: %s" %
            localizeStrTime(
                request,
                datetime.utcnow().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S"
            )
        )

        # 商品列表
        products_by_qty = kwargs['products_by_qty']

        for product_info in products_by_qty:
            # 出货货道id
            location_id = product_info['shelf_number']

            # 订单内部库存货道id
            request.cr.execute(
                """
                select id,location_id from stock_location
                where (name='库存' or name='Stock')
                and location_id = (
                    select location_id from StockLocation where id = %s
                    )
                and active = True
                """,
                (location_id,)
            )
            stock_location = request.cr.dictfetchall()
            if stock_location:
                location_dest_id = stock_location[0]['id']
            else:
                _logger.info(
                    """<error> cann't found the relevant stock location
                    which location_id :%s """ % (location_id))
                continue

            # 定位仓库
            request.cr.execute("""select id  from stock_warehouse where view_location_id = %s
                                 """,
                               (stock_location[0]['location_id'],))
            stock_warehouse = request.cr.dictfetchall()

            # 定位调拨类型id
            request.cr.execute(
                """
                    select id  from stock_picking_type where warehouse_id = %s
                    and ( name='内部调拨' or name ='Internal Transfers')
                    and active = True
                """,
                (stock_warehouse[0]['id'],)
            )
            stock_picking_type = request.cr.dictfetchall()

            picking_out = request.env['stock.picking'].create({
                'picking_type_id': stock_picking_type[0]['id'],
                'location_id': location_id,
                'location_dest_id': location_dest_id})

            _logger.info("create stock_picking :%s " % (picking_out.id))
            product_id = product_info['product_id']
            product_qty = product_info['product_qty']

            product_a = request.env['product.product'].browse(product_id)
            # 商品数量
            request.env['stock.move'].create({
                'name': product_a.name,
                'product_id': product_a.id,
                'product_uom_qty': product_qty,
                'product_uom': product_a.uom_id.id,
                'picking_id': picking_out.id,
                'location_id': location_id,
                'location_dest_id': location_dest_id})

            picking_out.action_confirm()
            # 转强制
            picking_out.force_assign()
            _logger.info("picking_out force_assign done")
            pack_opt = request.env['stock.pack.operation'].search(
                [('picking_id', '=', picking_out.id)], limit=1
            )
            # 检查是否存在产品序列号

            request.cr.execute(
                """
                    select id,name from stock_production_lot
                    where product_id = %s  order by create_date desc limit 1
                """,
                (product_a.id,))
            stock_production_lot = request.cr.dictfetchall()
            if stock_production_lot:
                lot_id = stock_production_lot[0]['id']
                lot_name = stock_production_lot[0]['name']
            else:
                lot1 = request.env['stock.production.lot'].create(
                    {'product_id': product_a.id}
                )
                lot_id = lot1.id
                lot_name = lot1.name
            request.env['stock.pack.operation.lot'].create(
                {
                    'operation_id': pack_opt.id,
                    'lot_id': lot_id,
                    'qty': product_qty
                }
            )
            _logger.info(
                "product %s used Lot/Serial number:%s " %
                (product_a.name, lot_name)
            )

            pack_opt.qty_done = product_qty
            picking_out.do_new_transfer()
            _logger.info("synchronize_vmc_inventory_reduction success")
