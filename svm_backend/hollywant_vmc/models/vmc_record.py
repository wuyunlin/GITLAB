# -*- coding: utf-8 -*-
from odoo import api, fields, models

import logging
_logger = logging.getLogger(__name__)

from odoo.http import request
from ..controllers.common import createstock
from odoo import SUPERUSER_ID

#缺少个关联到补货单的字段，连调时候加上
class vmc_delivery_record(models.Model):
    _name = 'vmc.delivery.record'
    _order='id desc'
    _description=u"提货记录"
    name = fields.Char(u'单号')
    stock_warehouse_id = fields.Many2one(
        'stock.warehouse',
        string='提货源仓库( 默认主仓库)',
    )
    company_id = fields.Many2one(
        'res.company',
        string='所属公司',
    )
    stock_warehouse_idcar = fields.Many2one(
        'stock.warehouse',
        string='提货目标仓库(默认车仓库)',
    )

    stock_picking_id = fields.Many2one(
        'stock.picking',
        string='调拨单',
    )
    create_uid = fields.Many2one(
        'res.users',
        string='操作员',
    )
    state= fields.Selection(
        string='状态',
        selection='_get_state',
        default='draft',
    )

    create_date = fields.Datetime(
        string="后台创建时间",
        readonly=True
    )
    order_time = fields.Datetime(
        string="订单时间",
        readonly=True
    )
    order_lines = fields.One2many(
        'vmc.delivery.record.line',
        'vmc_delivery_record_id',
        string='提货明细',
    )

    def createstock(request, self, vals_list):
        move_obj = request.env['stock.move']
        move_ids = []
        for move_vals in vals_list:
            move_ids += [move_obj.create(move_vals).id]
        self.action_confirm(request.cr, SUPERUSER_ID, move_ids)
        return move_ids

    @api.model
    def _get_state(self):
        return [
            ('draft', '草稿'),
            ('confirm', '确认'),
            ('cancel', '取消'),
        ]

    @api.model
    def create(self, vals):
        name = self.env['ir.sequence'].next_by_code('vmc.delivery.record')
        vals.update({'name':name})
        res=super(vmc_delivery_record, self.sudo()).create(vals)
        return res

    @api.multi
    def write(self, vals):

        if vals.get('state')=='confirm':
            sql = """select b.name,b.name as origin,now()-interval'8 hours' as date,now()-interval'8 hours' as date_expected,product_id,'make_to_order' as procure_method,
                   product_qty as product_uom_qty,product_uom,b.company_id,coalesce(c.lot_stock_id,0) as location_id,coalesce(d.lot_stock_id,0) as location_dest_id
                   from vmc_delivery_record_line a
                   left join vmc_delivery_record b on b.id=a.vmc_delivery_record_id
                   left join stock_warehouse c on c.id=b.stock_warehouse_id
                   left join stock_warehouse d on d.id=b.stock_warehouse_idcar
                    where vmc_delivery_record_id=%s""" % (self.id or 0)
            request.cr.execute(sql)
            fet = request.cr.dictfetchall() or []
            move_vals = {

                'picking_type_id': 217,

            }
            group_id=False
            vals_list = []
            for line in fet:
                if not group_id:
                    group_id=self.env["procurement.group"].create({'name':line['name']}).id
                move_vals['group_id']=group_id
                line.update(move_vals)
                vals_list.append(line)
            if vals_list:
                move_ids=createstock(request, self.pool.get('stock.move'), vals_list)
                sql="""select picking_id as stock_picking_id  from stock_move where id in %s limit 1""" %( tuple(move_ids+[0,-1]), )
                request.cr.execute(sql)
                fet = request.cr.dictfetchone() or {}
                if fet:
                    vals.update(fet)
        res = super(vmc_delivery_record, self).write(vals)
        return res
    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.write({'state':'confirm'})
        return True

class vmc_delivery_record_line(models.Model):
    _name = 'vmc.delivery.record.line'
    product_id = fields.Many2one(
        'product.product',
        string='商品',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )
    product_uom = fields.Many2one(
        'product.uom',
        string='单位'

    )
    vmc_delivery_record_id = fields.Many2one(
        'vmc.delivery.record',
        string='提货记录'
    )
    fore_qty = fields.Float('预计数量')
    product_qty = fields.Float('实际数量')
    description = fields.Char('描述')
vmc_delivery_record_line()

class vmc_procurement_record(models.Model):
    _name = 'vmc.procurement.record'
    _order='id desc'
    _description=u"补货记录"
    name = fields.Char(u'单号')
    stock_warehouse_id = fields.Many2one(
        'stock.warehouse',
        string='提货仓（默认车仓）',
    )
    create_uid = fields.Many2one(
        'res.users',
        string='操作员',
    )
    stock_picking_id = fields.Many2one(
        'stock.picking',
        string='调拨单',
    )
    vmc_adjust_stock_id = fields.Many2one(
        'vmc.adjust.stock',
        string='补货单',
    )
    vmc_machines_id = fields.Many2one(
        'vmc.machines',
        string='售货机',
    )

    create_date = fields.Datetime(
        string="后台创建时间",
        readonly=True
    )
    order_time = fields.Datetime(
        string="订单时间",
        readonly=True
    )
    order_lines = fields.One2many(
        'vmc.procurement.record.line',
        'vmc_procurement_record_id',
        string='补货明细',
    )


    @api.model
    def create(self, vals):
        name = self.env['ir.sequence'].next_by_code('vmc.procurement.record')
        vals.update({'name': name})
        res = super(vmc_procurement_record, self.sudo()).create(vals)
        return res

class vmc_procurement_record_line(models.Model):
    _name = 'vmc.procurement.record.line'
    product_id = fields.Many2one(
        'product.product',
        string='商品',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )
    product_uom = fields.Many2one(
        'product.uom',
        string='单位'

    )
    vmc_procurement_record_id = fields.Many2one(
        'vmc.procurement.record',
        string='补货记录',
	    ondelete='cascade'
    )
    product_qty = fields.Float('数量')
    channel_no = fields.Char('货道号')
    stock_location_id = fields.Many2one(
        'stock.location',
        string='货道库位'
    )
    description = fields.Char('描述')

#清空记录
class vmc_clear_record(models.Model):
    _name = 'vmc.clear.record'
    _order='id desc'
    _description=u"清空记录"
    name = fields.Char(u'单号')
    stock_picking_id = fields.Many2one(
        'stock.picking',
        string='清空单',
    )
    create_uid = fields.Many2one(
        'res.users',
        string='操作员',
    )
    vmc_adjust_stock_id = fields.Many2one(
        'vmc.adjust.stock',
        string='补货单',
    )
    vmc_machines_id = fields.Many2one(
        'vmc.machines',
        string='售货机',
    )
    stock_warehouse_id = fields.Many2one(
        'stock.warehouse',
        string='清空的仓库( 默认车仓库)',
    )

    create_date = fields.Datetime(
        string="后台创建时间",
        readonly=True
    )
    order_time = fields.Datetime(
        string="订单时间",
        readonly=True
    )
    order_lines = fields.One2many(
        'vmc.clear.record.line',
        'vmc_clear_record_id',
        string='清空明细',
    )

vmc_clear_record()

class vmc_clear_record_line(models.Model):
    _name = 'vmc.clear.record.line'
    product_id = fields.Many2one(
        'product.product',
        string='商品',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )
    product_uom = fields.Many2one(
        'product.uom',
        string='单位'

    )
    vmc_clear_record_id = fields.Many2one(
        'vmc.clear.record',
        string='补货记录',
	    ondelete='cascade'
    )
    product_qty = fields.Float('数量')
    diff_qty = fields.Float('差异数量')
    channel_no = fields.Char('货道号')
    stock_location_id = fields.Many2one(
        'stock.location',
        string='货道库位'
    )
    description = fields.Char('描述')

#还货记录
class vmc_back_record(models.Model):
    _name = 'vmc.back.record'
    _order='id desc'
    _description=u"还货记录"
    name = fields.Char(u'单号')
    stock_picking_id = fields.Many2one(
        'stock.picking',
        string='还货单',
    )

    stock_warehouse_id = fields.Many2one(
        'stock.warehouse',
        string='还货目标仓库(默认主仓库)',
    )
    create_uid = fields.Many2one(
        'res.users',
        string='补货员',
    )
    stock_warehouse_idcar = fields.Many2one(
        'stock.warehouse',
        string='还货源仓库(默认车仓库)',
    )

    create_date = fields.Datetime(
        string="后台创建时间",
        readonly=True
    )
    order_time = fields.Datetime(
        string="订单时间",
        readonly=True
    )
    order_lines = fields.One2many(
        'vmc.clear.record.line',
        'vmc_clear_record_id',
        string='还货明细',
    )

class vmc_back_record_line(models.Model):
    _name = 'vmc.back.record.line'
    product_id = fields.Many2one(
        'product.product',
        string='商品',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )
    product_uom = fields.Many2one(
        'product.uom',
        string='单位'

    )
    vmc_back_record_id = fields.Many2one(
        'vmc.back.record',
        string='补货记录',
	    ondelete='cascade'
    )
    product_qty = fields.Float('数量')
    description = fields.Char('描述')

class vmc_money_record(models.Model):
    _name = 'vmc.money.record'
    _order = 'id desc'
    vmc_machines_id = fields.Many2one(
        'vmc.machines',
        string='售货机名称',
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        related='vmc_machines_id.company_id',
        readonly=True
    )

    create_uid = fields.Many2one(
        'res.users',
        string='补货员',
    )
    create_date = fields.Datetime(
        string="后台创建时间",
        readonly=True
    )
    order_time = fields.Datetime(
        string="时间",
        readonly=True
    )
    amount = fields.Float('金额')

    mention_amount = fields.Float('应提金额')

    machine_coin_50cent = fields.Integer('硬币盒剩余5角个数')

    machine_coin_100cent = fields.Integer('硬币盒剩余1元个数')

    portable_coin_50cent = fields.Integer('补5角个数')

    portable_coin_100cent = fields.Integer('补1元个数')

    type= fields.Selection(
        string='类型',
        selection='_get_type',
    )
    state= fields.Selection(
        string=' 状态',
        selection='_get_state',
    )
    vmc_financial_record_id = fields.Many2one(
        'vmc.financial.record',
        string='收款单',
    )
    area = fields.Char('区县')
    address = fields.Char('详细地址')
    location =fields.Char('摆放位置')
    description = fields.Char('描述')
    adjust_stock_id = fields.Char('补货单id')

    @api.model
    def _get_type(self):
        return [
            ('get', '取款'),
            ('giveby_supply', '补货员补款'),
	        ('giveby_machine', '机器补款'),
        ]
    @api.model
    def _get_state(self):
        return [
            ('draft', '新建'),
            ('confirm','已确认'),
            ('done', '已收款'),
        ]

#财务收款记录
class vmc_financial_record(models.Model):
    _name = 'vmc.financial.record'
    _order='id desc'
    _description=u"财务收款记录"
    name = fields.Char(u'单号')
    create_uid = fields.Many2one(
        'res.users',
        string='制单人',
    )

    supply_id = fields.Many2one(
        'res.users',
        string='补货员',
    )

    vmc_machines_id = fields.Many2one(
        'vmc.machines',
        string='售货机',
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        readonly=True
    )

    create_date = fields.Datetime(
        string="后台创建时间",
        readonly=True
    )
    date = fields.Date(
        string="收款日期"
    )
    amount = fields.Float(
		string="应收款总额",
		readonly=True
	)

    real_amount = fields.Float(
		string="实收款总额",
		readonly=True
	)
    rate = fields.Float(
	    string="误差(率)",
	    readonly=True
    )
    description = fields.Char(
	    string="备注",
	    readonly=True
    )


    @api.model
    def create(self, vals):
		name = self.env['ir.sequence'].next_by_code('vmc.financial.record')
		vals.update({'name': name})
		res = super(vmc_financial_record, self).create(vals)
		return res


class stock_move(models.Model):
    _inherit = "stock.move"
    svm_type = fields.Char(
        string='记录类型',size=100
    )
    supply_id = fields.Many2one(
        'res.users',
        string='补货员',
    )