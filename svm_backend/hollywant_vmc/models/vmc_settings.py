# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.http import request
from ..controllers import vmc
from odoo.exceptions import ValidationError
from odoo.tools import float_compare
from odoo.exceptions import UserError

import logging
_logger = logging.getLogger(__name__)


def get_latitude_and_longitude():  # 获得原经纬度，及实际经纬度 原异常信息
    request.cr.execute(
        """
            select id,coordinate,location,error_info from vmc_machines
        """
    )
    list = request.cr.dictfetchall()
    if list:
        return list
    else:
        return False


def check_distance_abnormal():  # 根据两地经纬度算距离
    distance_list = []
    record_list = get_latitude_and_longitude()
    if record_list:
        for d in record_list:
            if d['coordinate'] and d['location']:
                lon1 = float(d['coordinate'].split(',')[0])  # 原经度
                lat1 = float(d['coordinate'].split(',')[1])  # 原纬度
                lon2 = float(d['location'].split(',')[0])  # 现经度
                lat2 = float(d['location'].split(',')[1])  # 现纬度
                distance = vmc.get_distance(lon1, lat1, lon2, lat2)
                result_dict = {}
                result_dict['id'] = d['id']
                result_dict['distance'] = distance
                result_dict['error_info'] = d['error_info']
                distance_list.append(result_dict)  # 把id和距离的字典 加入到distance_list
            else:
                pass

    return distance_list


class VmcSettings(models.Model):
    _name = 'vmc.settings'
    # _inherits = {'res.partner': 'partner_id'}
    # partner_id = fields.Many2one(
    #     'res.partner', ondelete='restrict', required=True
    # )
    name = fields.Char("名称", default="设置")
    vmc_machine = fields.Many2many('vmc.machines','vmc_settings_machines_rel','setting_id','machine_id','售货机')

    net_checktime = fields.Integer("网络检测时间", default=10)
    check_distance = fields.Float("警报距离", digits=(10, 6), default=100)
    company_id = fields.Many2one('res.company', string='所属公司')
    vmc_machine_status = fields.Boolean(
        string='售货机可选状态',
        default=False
    )

    @api.model
    def _get_company_id(self, *args, **kwargs):
        return self.env.user.company_id.id

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcSettings, self).unlink()

    company_id = fields.Many2one('res.company', string=u'所属公司',default=_get_company_id)

    @api.model
    def create(self, vals):
        sql = """
                        select id from vmc_settings where vmc_machine_status in (False, True)
                    """
        cr = self.env.cr
        cr.execute(sql)
        result_status = cr.dictfetchall()

        machines = vals.get("vmc_machine", [])
        if machines and len(machines[0]) > 1:
            machines_ids = machines[0][2]
            if machines_ids:
                machines_id0 = machines_ids[0]
                if len(machines_ids) > 1:
                    sql = """
                                    select * from vmc_settings_machines_rel where machine_id in %s
                                """ % (str(tuple(machines_ids)))
                elif len(machines_ids) == 1:
                    sql = """
                                   select * from vmc_settings_machines_rel where machine_id = %d
                               """ % (machines_id0)
                cr = self.env.cr
                cr.execute(sql)
                result = cr.dictfetchall()
                if result:
                    machines_name = []
                    for dict_machine in result:
                        sql = """
                                        select name from res_partner where id = (select partner_id from vmc_machines where id = %s)
                                    """ % (dict_machine.get('machine_id', 0))
                        cr.execute(sql)
                        machine_name_result = cr.dictfetchall()
                        if machine_name_result:
                            machines_name.append(machine_name_result[0].get('name', ''))
                    machine_name_str = ','.join(machines_name)
                    raise UserError('机器“{}”已经被配置！'.format(machine_name_str))
            else:
                sql = """
                                    select vmc_machine_status from vmc_settings where vmc_machine_status = False and id != %d
                                """ % (self.id)
                cr = self.env.cr
                cr.execute(sql)
                result_status = cr.dictfetchall()
                if result_status:
                    raise UserError('已经存在通用配置记录!')

            request.cr.execute('''
                               update vmc_settings set vmc_machine_status=False
                               where id not in (select setting_id from vmc_settings_machines_rel)
                               ''')
            request.cr.execute('''
                               update vmc_settings set vmc_machine_status=True
                               where id in (select setting_id from vmc_settings_machines_rel)
                               ''')

        elif len(machines) == 0:
            sql = """
                                select vmc_machine_status from vmc_settings where vmc_machine_status = False and id != %d
                            """ % (self.id)
            cr = self.env.cr
            cr.execute(sql)
            result_status = cr.dictfetchall()
            if result_status:
                raise UserError('已经存在通用配置记录!')

                # 第一条记录是通用配置
        if len(result_status) == 0 and len(machines) == 0:
            vals.update({
                "vmc_machine_status": False
            })
        # 第一条记录是普通配置
        elif len(result_status) == 0 and len(machines) != 0:
            vals.update({
                "vmc_machine_status": True
            })

        else:
            vals.update({
                "vmc_machine_status": True
            })

            request.cr.execute('''
                                          update vmc_settings set vmc_machine_status=False
                                          where id not in (select setting_id from vmc_settings_machines_rel)
                                          ''')
            request.cr.execute('''
                                          update vmc_settings set vmc_machine_status=True
                                          where id in (select setting_id from vmc_settings_machines_rel)
                                          ''')

        settings = super(VmcSettings, self).create(vals)

        return settings

    @api.one
    def write(self, vals):
        sql = """
                          select vmc_machine_status from vmc_settings where vmc_machine_status = False and id = %d
                       """ % (self.id)
        cr = self.env.cr
        cr.execute(sql)
        status_common = cr.dictfetchall()

        sql_1 = """
                          select machine_id from vmc_settings_machines_rel where setting_id = %d
                       """ % (self.id)
        cr.execute(sql_1)
        status_machines = cr.dictfetchall()

        machines = vals.get("vmc_machine", [])
        # 普通配置
        if status_machines:
            # 1、普通配置到通用配置
            if len(machines) == 1 and len(machines[0][2]) == 0:
                raise UserError('普通配置不允许更改为通用配置！')

            else:
                if machines and len(machines[0]) > 1:
                    machines_ids = machines[0][2]
                    if machines_ids:
                        machines_id0 = machines_ids[0]
                        if len(machines_ids) > 1:
                            sql = """
                                              select * from vmc_settings_machines_rel where machine_id in %s and setting_id != %d
                                          """ % (str(tuple(machines_ids)), self.id)
                        elif len(machines_ids) == 1:
                            sql = """
                                               select * from vmc_settings_machines_rel where machine_id = %d and setting_id != %d
                                         """ % (machines_id0, self.id)
                        cr = self.env.cr
                        cr.execute(sql)
                        result = cr.dictfetchall()
                        if result:
                            machines_name = []
                            for dict_machine in result:
                                sql = """
                                                  select name from res_partner where id = (select partner_id from vmc_machines where id = %s)
                                             """ % (dict_machine.get('machine_id', 0))
                                cr.execute(sql)
                                machine_name_result = cr.dictfetchall()
                                if machine_name_result:
                                    machines_name.append(machine_name_result[0].get('name', ''))
                            machine_name_str = ','.join(machines_name)

                            raise UserError('机器“{}”已经被配置！'.format(machine_name_str))

                    else:
                        sql = """
                                          select vmc_machine_status from vmc_settings where vmc_machine_status = False and id != %d
                                       """ % (self.id)
                        cr = self.env.cr
                        cr.execute(sql)
                        result_status = cr.dictfetchall()
                        if result_status:
                            raise UserError('已经存在通用配置记录!')

                    request.cr.execute('''
                                              update vmc_settings set vmc_machine_status=False
                                              where id not in (select setting_id from vmc_settings_machines_rel)
                                              ''')
                    request.cr.execute('''
                                              update vmc_settings set vmc_machine_status=True
                                              where id in (select setting_id from vmc_settings_machines_rel)
                                              ''')

                # 通用配置改变，更改名称或者其他的参数
                elif status_common and not status_machines and len(machines) == 0:
                    sql = """
                                            select vmc_machine_status from vmc_settings where vmc_machine_status = False and id != %d
                                       """ % (self.id)
                    cr = self.env.cr
                    cr.execute(sql)
                    result_status = cr.dictfetchall()
                    if result_status:
                        raise UserError('已经存在通用配置记录!')

        # 通用配置设置成普通配置
        if not status_machines and status_common and len(machines) != 0:
            raise UserError('通用配置不允许更改为普通配置！')

        settings = self
        result = super(VmcSettings, self).write(vals)
        request.cr.execute(
            """
                Update ir_cron set interval_number = %s
            """, (settings.net_checktime,))
        # distance_list = check_distance_abnormal()
        # if distance_list:
        #     for d in distance_list:
        #         if d['error_info'] != '网络异常':
        #             if float_compare(
        #                     d['distance'],
        #                     vals['check_distance'],
        #                     4) > 0:  # 超过警戒距离
        #                 request.cr.execute('''
        #                     update vmc_machines set error_info='%s',
        #                      error_info_color = '%s'
        #                     where id = %s
        #                 ''' % ('距离异常', 'red', str(d['id'])))
        #
        #             else:
        #                 request.cr.execute('''
        #                     update vmc_machines set error_info='%s',
        #                      error_info_color = '%s'
        #                     where id = %s
        #                 ''' % ('设备正常', 'green', str(d['id'])))

        return result


class VmcCountDownTimeSettings(models.Model):
    _name = 'vmc.countdown.time.settings'
    _description = u'售货机倒计时间设置'

    name = fields.Char(string='名称', default='公用倒计时间设置')
    vmc_machine = fields.Many2many('vmc.machines', 'vmc_countdown_settings_machines_rel', 'setting_id',
                                   'machine_id', '售货机')
    general_page_countdown = fields.Integer(string='一般页面倒计时间', default=60, required=True)
    purchase_page_countdown = fields.Integer(string='商品购买页面倒计时间', default=90, required=True)
    message_countdown_long = fields.Integer(string='提示信息倒计时间(长)', default=10, required=True)
    message_countdown_short = fields.Integer(string='提示信息倒计时间(短)', default=5, required=True)
    help_page_countdown = fields.Integer(string='帮助页面倒计时间', default=30, required=True)
    advertising_countdown = fields.Integer(string='广告倒计时间', default=10, required=True)
    vmc_machine_status = fields.Boolean(
        string='售货机可选状态',
        default=False
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcCountDownTimeSettings, self).unlink()

    @api.model
    def create(self, vals):
        sql = """
                    select id from vmc_countdown_time_settings where vmc_machine_status in (False, True)
                """
        cr = self.env.cr
        cr.execute(sql)
        result_status = cr.dictfetchall()

        machines = vals.get("vmc_machine", [])
        if machines and len(machines[0]) > 1:
            machines_ids = machines[0][2]
            if machines_ids:
                machines_id0 = machines_ids[0]
                if len(machines_ids) > 1:
                    sql = """
                                select * from vmc_countdown_settings_machines_rel where machine_id in %s
                            """ % (str(tuple(machines_ids)))
                elif len(machines_ids) == 1:
                    sql = """
                               select * from vmc_countdown_settings_machines_rel where machine_id = %d
                           """ % (machines_id0)
                cr = self.env.cr
                cr.execute(sql)
                result = cr.dictfetchall()
                if result:
                    machines_name = []
                    for dict_machine in result:
                        sql = """
                                    select name from res_partner where id = (select partner_id from vmc_machines where id = %s)
                                """ % (dict_machine.get('machine_id', 0))
                        cr.execute(sql)
                        machine_name_result = cr.dictfetchall()
                        if machine_name_result:
                            machines_name.append(machine_name_result[0].get('name', ''))
                    machine_name_str = ','.join(machines_name)
                    raise UserError('机器“{}”已经被配置！'.format(machine_name_str))
            else:
                sql = """
                                select vmc_machine_status from vmc_countdown_time_settings where vmc_machine_status = False and id != %d
                            """ % (self.id)
                cr = self.env.cr
                cr.execute(sql)
                result_status = cr.dictfetchall()
                if result_status:
                    raise UserError('已经存在通用配置记录!')

            request.cr.execute('''
                           update vmc_countdown_time_settings set vmc_machine_status=False
                           where id not in (select setting_id from vmc_countdown_settings_machines_rel)
                           ''')
            request.cr.execute('''
                           update vmc_countdown_time_settings set vmc_machine_status=True
                           where id in (select setting_id from vmc_countdown_settings_machines_rel)
                           ''')

        elif len(machines) == 0:
            sql = """
                            select vmc_machine_status from vmc_countdown_time_settings where vmc_machine_status = False and id != %d
                        """ % (self.id)
            cr = self.env.cr
            cr.execute(sql)
            result_status = cr.dictfetchall()
            if result_status:
                raise UserError('已经存在通用配置记录!')

                # 第一条记录是通用配置
        if len(result_status) == 0 and len(machines) == 0:
            vals.update({
                "vmc_machine_status": False
            })
        # 第一条记录是普通配置
        elif len(result_status) == 0 and len(machines) != 0:
            vals.update({
                "vmc_machine_status": True
            })

        else:
            vals.update({
                "vmc_machine_status": True
            })

            request.cr.execute('''
                                      update vmc_countdown_time_settings set vmc_machine_status=False
                                      where id not in (select setting_id from vmc_countdown_settings_machines_rel)
                                      ''')
            request.cr.execute('''
                                      update vmc_countdown_time_settings set vmc_machine_status=True
                                      where id in (select setting_id from vmc_countdown_settings_machines_rel)
                                      ''')

        settings = super(VmcCountDownTimeSettings, self).create(vals)

        return settings

        @api.one
        def write(self, vals):
            sql = """
                        select vmc_machine_status from vmc_countdown_time_settings where vmc_machine_status = False and id = %d
                     """ % (self.id)
            cr = self.env.cr
            cr.execute(sql)
            status_common = cr.dictfetchall()

            sql_1 = """
                        select machine_id from vmc_countdown_settings_machines_rel where setting_id = %d
                     """ % (self.id)
            cr.execute(sql_1)
            status_machines = cr.dictfetchall()

            machines = vals.get("vmc_machine", [])
            # 普通配置
            if status_machines:
                # 1、普通配置到通用配置
                if len(machines) == 1 and len(machines[0][2]) == 0:
                    raise UserError('普通配置不允许更改为通用配置！')

                else:
                    if machines and len(machines[0]) > 1:
                        machines_ids = machines[0][2]
                        if machines_ids:
                            machines_id0 = machines_ids[0]
                            if len(machines_ids) > 1:
                                sql = """
                                            select * from vmc_countdown_settings_machines_rel where machine_id in %s and setting_id != %d
                                        """ % (str(tuple(machines_ids)), self.id)
                            elif len(machines_ids) == 1:
                                sql = """
                                             select * from vmc_countdown_settings_machines_rel where machine_id = %d and setting_id != %d
                                       """ % (machines_id0, self.id)
                            cr = self.env.cr
                            cr.execute(sql)
                            result = cr.dictfetchall()
                            if result:
                                machines_name = []
                                for dict_machine in result:
                                    sql = """
                                                select name from res_partner where id = (select partner_id from vmc_machines where id = %s)
                                           """ % (dict_machine.get('machine_id', 0))
                                    cr.execute(sql)
                                    machine_name_result = cr.dictfetchall()
                                    if machine_name_result:
                                        machines_name.append(machine_name_result[0].get('name', ''))
                                machine_name_str = ','.join(machines_name)

                                raise UserError('机器“{}”已经被配置！'.format(machine_name_str))

                        else:
                            sql = """
                                        select vmc_machine_status from vmc_countdown_time_settings where vmc_machine_status = False and id != %d
                                     """ % (self.id)
                            cr = self.env.cr
                            cr.execute(sql)
                            result_status = cr.dictfetchall()
                            if result_status:
                                raise UserError('已经存在通用配置记录!')

                        request.cr.execute('''
                                            update vmc_countdown_time_settings set vmc_machine_status=False
                                            where id not in (select setting_id from vmc_countdown_settings_machines_rel)
                                            ''')
                        request.cr.execute('''
                                            update vmc_countdown_time_settings set vmc_machine_status=True
                                            where id in (select setting_id from vmc_countdown_settings_machines_rel)
                                            ''')

                    # 通用配置改变，更改名称或者其他的参数
                    elif status_common and not status_machines and len(machines) == 0:
                        sql = """
                                          select vmc_machine_status from vmc_countdown_time_settings where vmc_machine_status = False and id != %d
                                     """ % (self.id)
                        cr = self.env.cr
                        cr.execute(sql)
                        result_status = cr.dictfetchall()
                        if result_status:
                            raise UserError('已经存在通用配置记录!')

            # 通用配置设置成普通配置
            if not status_machines and status_common and len(machines) != 0:
                raise UserError('通用配置不允许更改为普通配置！')

            result = super(VmcCountDownTimeSettings, self).write(vals)

            return result


class VmcResetTimeSettings(models.Model):
    _name = 'vmc.reset.time.settings'
    _description = u'售货机重启时间设置'

    name = fields.Char(string='名称', default='公用重启时间设置')
    vmc_machine = fields.Many2many('vmc.machines', 'vmc_reset_settings_machines_rel', 'setting_id', 'machine_id',
                                   '售货机')
    time = fields.Char(string='重启时间', default='03:00:00', required=True)
    reset_time = fields.Char(string='重启周期', default='每天', required=True)
    vmc_machine_status = fields.Boolean(
        string='售货机可选状态',
        default=False
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcResetTimeSettings, self).unlink()

    @api.model
    def create(self, vals):
        sql = """
                        select id from vmc_reset_time_settings where vmc_machine_status in (False, True)
                    """
        cr = self.env.cr
        cr.execute(sql)
        result_status = cr.dictfetchall()

        machines = vals.get("vmc_machine", [])
        if machines and len(machines[0]) > 1:
            machines_ids = machines[0][2]
            if machines_ids:
                machines_id0 = machines_ids[0]
                if len(machines_ids) > 1:
                    sql = """
                                    select * from vmc_reset_settings_machines_rel where machine_id in %s
                                """ % (str(tuple(machines_ids)))
                elif len(machines_ids) == 1:
                    sql = """
                                   select * from vmc_reset_settings_machines_rel where machine_id = %d
                               """ % (machines_id0)
                cr = self.env.cr
                cr.execute(sql)
                result = cr.dictfetchall()
                if result:
                    machines_name = []
                    for dict_machine in result:
                        sql = """
                                        select name from res_partner where id = (select partner_id from vmc_machines where id = %s)
                                    """ % (dict_machine.get('machine_id', 0))
                        cr.execute(sql)
                        machine_name_result = cr.dictfetchall()
                        if machine_name_result:
                            machines_name.append(machine_name_result[0].get('name', ''))
                    machine_name_str = ','.join(machines_name)
                    raise UserError('机器“{}”已经被配置！'.format(machine_name_str))
            else:
                sql = """
                                    select vmc_machine_status from vmc_reset_time_settings where vmc_machine_status = False and id != %d
                                """ % (self.id)
                cr = self.env.cr
                cr.execute(sql)
                result_status = cr.dictfetchall()
                if result_status:
                    raise UserError('已经存在通用配置记录!')

            request.cr.execute('''
                               update vmc_reset_time_settings set vmc_machine_status=False
                               where id not in (select setting_id from vmc_reset_settings_machines_rel)
                               ''')
            request.cr.execute('''
                               update vmc_reset_time_settings set vmc_machine_status=True
                               where id in (select setting_id from vmc_reset_settings_machines_rel)
                               ''')

        elif len(machines) == 0:
            sql = """
                                select vmc_machine_status from vmc_reset_time_settings where vmc_machine_status = False and id != %d
                            """ % (self.id)
            cr = self.env.cr
            cr.execute(sql)
            result_status = cr.dictfetchall()
            if result_status:
                raise UserError('已经存在通用配置记录!')

                # 第一条记录是通用配置
        if len(result_status) == 0 and len(machines) == 0:
            vals.update({
                "vmc_machine_status": False
            })
        # 第一条记录是普通配置
        elif len(result_status) == 0 and len(machines) != 0:
            vals.update({
                "vmc_machine_status": True
            })

        else:
            vals.update({
                "vmc_machine_status": True
            })

            request.cr.execute('''
                                          update vmc_reset_time_settings set vmc_machine_status=False
                                          where id not in (select setting_id from vmc_reset_settings_machines_rel)
                                          ''')
            request.cr.execute('''
                                          update vmc_reset_time_settings set vmc_machine_status=True
                                          where id in (select setting_id from vmc_reset_settings_machines_rel)
                                          ''')

        settings = super(VmcResetTimeSettings, self).create(vals)

        return settings

    @api.one
    def write(self, vals):
        sql = """
                        select vmc_machine_status from vmc_reset_time_settings where vmc_machine_status = False and id = %d
                     """ % (self.id)
        cr = self.env.cr
        cr.execute(sql)
        status_common = cr.dictfetchall()

        sql_1 = """
                        select machine_id from vmc_reset_settings_machines_rel where setting_id = %d
                     """ % (self.id)
        cr.execute(sql_1)
        status_machines = cr.dictfetchall()

        machines = vals.get("vmc_machine", [])
        # 普通配置
        if status_machines:
            # 1、普通配置到通用配置
            if len(machines) == 1 and len(machines[0][2]) == 0:
                raise UserError('普通配置不允许更改为通用配置！')

            else:
                if machines and len(machines[0]) > 1:
                    machines_ids = machines[0][2]
                    if machines_ids:
                        machines_id0 = machines_ids[0]
                        if len(machines_ids) > 1:
                            sql = """
                                            select * from vmc_reset_settings_machines_rel where machine_id in %s and setting_id != %d
                                        """ % (str(tuple(machines_ids)), self.id)
                        elif len(machines_ids) == 1:
                            sql = """
                                             select * from vmc_reset_settings_machines_rel where machine_id = %d and setting_id != %d
                                       """ % (machines_id0, self.id)
                        cr = self.env.cr
                        cr.execute(sql)
                        result = cr.dictfetchall()
                        if result:
                            machines_name = []
                            for dict_machine in result:
                                sql = """
                                                select name from res_partner where id = (select partner_id from vmc_machines where id = %s)
                                           """ % (dict_machine.get('machine_id', 0))
                                cr.execute(sql)
                                machine_name_result = cr.dictfetchall()
                                if machine_name_result:
                                    machines_name.append(machine_name_result[0].get('name', ''))
                            machine_name_str = ','.join(machines_name)

                            raise UserError('机器“{}”已经被配置！'.format(machine_name_str))

                    else:
                        sql = """
                                        select vmc_machine_status from vmc_reset_time_settings where vmc_machine_status = False and id != %d
                                     """ % (self.id)
                        cr = self.env.cr
                        cr.execute(sql)
                        result_status = cr.dictfetchall()
                        if result_status:
                            raise UserError('已经存在通用配置记录!')

                    request.cr.execute('''
                                            update vmc_reset_time_settings set vmc_machine_status=False
                                            where id not in (select setting_id from vmc_reset_settings_machines_rel)
                                            ''')
                    request.cr.execute('''
                                            update vmc_reset_time_settings set vmc_machine_status=True
                                            where id in (select setting_id from vmc_reset_settings_machines_rel)
                                            ''')

                # 通用配置改变，更改名称或者其他的参数
                elif status_common and not status_machines and len(machines) == 0:
                    sql = """
                                          select vmc_machine_status from vmc_reset_time_settings where vmc_machine_status = False and id != %d
                                     """ % (self.id)
                    cr = self.env.cr
                    cr.execute(sql)
                    result_status = cr.dictfetchall()
                    if result_status:
                        raise UserError('已经存在通用配置记录!')

        # 通用配置设置成普通配置
        if not status_machines and status_common and len(machines) != 0:
            raise UserError('通用配置不允许更改为普通配置！')

        result = super(VmcResetTimeSettings, self).write(vals)

        return result


class ResCompany(models.Model):
    _inherit = 'res.company'

    customer_phone = fields.Char(
        string='客服电话'
    )
