# -*- coding: utf-8 -*-
from odoo import fields, models


class HollywantPartner(models.Model):
    _inherit = "res.partner"

    customer_type = fields.Selection(
        [(1, '零售终端'), (2, '自动售货机'), (3, '普通消费者')],
        '客户类别', default=1, required=True
    )

    county = fields.Many2one(
        "res.country.state.city.county", 'County', ondelete='restrict')
    county_name = fields.Char(string='county_name')
    state_name = fields.Char(string='state_name')
    city_id = fields.Many2one(
        "res.country.state.city", 'City', ondelete='restrict')
