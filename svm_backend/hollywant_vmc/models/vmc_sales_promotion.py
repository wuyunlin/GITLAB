# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError
import time
import urllib2
from datetime import datetime, timedelta, tzinfo


ISOTIMEFORMAT = '%Y-%m-%d %H:%m'


class VmcProductPriceSort(models.Model):
    _inherit = 'product.product'

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self.env.context
        if context.get('promotion_type'):
            order = 'list_price desc'

        return super(VmcProductPriceSort, self).search(args, offset, limit, order, count)


class VmcSalesPromotion(models.Model):
    _name = 'vmc.promotion'

    name = fields.Char(
        string="促销名称",
    )

    start_using = fields.Boolean(
        string='启用',
    )

    close_using_time = fields.Datetime(
        string="规则停用时间",
    )

    start_using_time = fields.Datetime(
        string="规则启用时间",
    )

    promotion_type = fields.Selection(
        string='促销规则',
        selection='_get_promotion_type',
        default='discount',
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='产品',
    )

    @api.model
    def _get_promotion_time_type(self):
        return [
            ('1', '按每天的时间段'),
            ('2', '按时间段'),
        ]

    promotion_time_type = fields.Selection(string="促销时间类型", selection='_get_promotion_time_type', default='1')

    period_of_start_date=fields.Datetime(string="时间段开始时间")
    period_of_end_date = fields.Datetime(string="时间段结束时间")

    start_date = fields.Date(
        # string="开始日期",
        # required=True
    )

    end_date = fields.Date(
        # string="结束日期",
        # required=True
    )

    start_time = fields.Char(
        # string='开始时间',
        # required=True
    )

    end_time = fields.Char(
        # string='结束时间',
        # required=True
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcSalesPromotion, self).unlink()

    @api.constrains('buy_gifts', 'promotion_lines','promotion_type')
    def _constraint_buy_gifts_promotion_limit(self):
        if self.promotion_type=='one_more' and self.buy_gifts == False:
            product_list = [x for x in self.promotion_lines]
            if len(product_list) > 1:
                raise ValidationError("非随机赠时，赠送产品不能选多个！！")
            if len(product_list) == 0:
                raise ValidationError("请选择赠送产品！")
        else:
            pass

    @api.constrains('promotional_image_links')
    def _constraint_url_image_limit(self):
        def url_limit1(url1):
            try:
                res = urllib2.urlopen(urllib2.Request(url1))
                code = res.getcode()
                res.close()
                return code
            except Exception, e:
                return 44

        if self.is_promotional_image_links == "是":
            if self.promotional_image_links:
                url_link = self.promotional_image_links
                res = url_limit1(url_link)
                if res == 200 :
                    first_link_type = urllib2.urlopen(url_link)
                    if first_link_type.headers['Content-Type'].find('image') != -1:
                        pass
                    else:
                        raise ValidationError('促销图片链接不是图片类型')
                else:
                    raise ValidationError('链接无效')
            else:
                raise ValidationError('图片链接不能为空')
        else:
            pass

    @api.constrains('promotion_time_type', 'period_of_start_date', 'period_of_end_date')
    def _constraints_on_period_time(self):
        for line in self:
            if line.promotion_time_type == "2":
                if line.period_of_start_date > line.period_of_end_date:
                    raise ValidationError("""开始时间'%s'不能大于结束时间%'s'"""%(line.period_of_start_date , line.period_of_end_date))
                if line.period_of_end_date <= time.strftime(ISOTIMEFORMAT, time.localtime(time.time())):
                    raise ValidationError('促销结束时间不能小于当前时间')

    @api.constrains('promotion_time_type', 'start_time', 'end_time', 'start_date', 'end_date')
    def _constraint_on_time(self):
        def is_valid_date(str):
            try:
                time.strptime(str, "%H:%M:%S")
                return True
            except:
                return False

        for line in self:
            if line.promotion_time_type == "1":
                if line.start_date > line.end_date:
                    raise ValidationError('开始日期不能大于结束日期')
                if line.start_time > line.end_time:
                    raise ValidationError('开始时间不能大于结束时间')
                if line.promotion_time_type == '1':
                    if is_valid_date(line.start_time) and is_valid_date(line.end_time):
                        pass
                    else:
                        raise ValidationError('时间格式不正确，应该类似XX:XX:XX')
                    if time.strftime(ISOTIMEFORMAT, time.localtime(time.time())) > line.end_date+" "+line.end_time:
                        raise ValidationError('促销结束时间不能小于当前时间')
                else:
                    if time.strftime(ISOTIMEFORMAT, time.localtime(time.time())) > line.period_of_end_date:
                        raise ValidationError('促销结束时间不能小于当前时间')

    payment_use = fields.Char(string='支付状态')
    payment_alipay = fields.Boolean('支付宝')
    payment_weixin = fields.Boolean('微信')
    payment_wangbi = fields.Boolean('旺币')
    payment_cash = fields.Boolean('现金支付')
    payment_other = fields.Boolean('其他')

    promotional_image_links = fields.Char('促销图片链接')
    is_promotional_image_links = fields.Selection(
        string='是否需要促销图片链接',
        selection='_get_is_promotional_image_links_type',
        default='否')

    @api.model
    def _get_is_promotional_image_links_type(self):
        return [
            (u'是', u'是'),
            (u'否', u'否'),
        ]

    last_update_time = fields.Datetime(
        string="上次同步时间",
    )

    discount_price = fields.Float(
        string="优惠价",
    )

    discount_rate = fields.Float('折扣比例')
    unchange_price = fields.Float('固定价格')
    # buy_gifts = fields.Selection(
    #     selection='_get_buy_type',
    #     default='all_gifts',
    # )

    buy_gifts = fields.Boolean('随机赠')

    promotion_lines = fields.One2many(
        comodel_name="vmc.promotion.lines",
        inverse_name='promotion_id',
        string='赠送详情',
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        readonly=True,
        default=lambda self: self.env.user.company_id,
    )

    @api.model
    def _get_promotion_type(self):
        return [
            ('one_more', '买赠'),
            ('discount', '折扣'),
            ('unchange_count', '立减'),
        ]

    vmc_sales_id_list = fields.Many2many('vmc.machines', 'vmc_sales_machines_list', 'vmc_sales_id', 'machine_id',
                                             '选择售货机')
    vmc_sales_select_product_id = fields.Many2many('product.product', 'vmc_product_promotion_rel', 'vmc_select_id'
                                                   , 'product_id', '选择商品')

    @api.constrains('payment_cash', 'unchange_price', 'vmc_sales_select_product_id')
    def _constraint_promotion_type_payment_cash_limit(self):
        if self.promotion_type == 'unchange_count':
            product_ids = [x.id for x in self.vmc_sales_select_product_id]
            cr = self.env.cr
            price_list = []
            cr.execute("""
                        select pt.list_price
                        from product_product as pp
                        left join product_template as pt on pp.product_tmpl_id=pt.id
                        where pp.id in  %s and round(pt.list_price,2)<= %s""",
                       (tuple(product_ids + [0, -1]), round(self.unchange_price,2)))
            result = cr.fetchall()
            if result:
                raise ValidationError('立减价格应小于商品的价格')
            else:
                pass

        if self.payment_cash and self.unchange_price % 0.5 != 0:
            raise UserError('勾选现金支付后，促销立减的金额应为0.5的倍数')

    @api.one
    def action_is_close_using(self):
        if not self.start_using:
            return True
        close_using_time = str(datetime.utcnow())
        self.env.cr.execute("""update vmc_promotion set start_using_time=null,close_using_time='%s',start_using=False where id=%s"""%( close_using_time,self.id))
        return  True

    @api.one
    def action_is_start_using(self):
        if self.start_using:
            return True
        start_using_time=str(datetime.utcnow())
        self.env.cr.execute("""update vmc_promotion set close_using_time=null,start_using_time='%s',start_using=True where id=%s"""%(start_using_time, self.id))
        return  True

    @api.model
    def create(self, vals):
        vmc_id_list = vals.get('vmc_sales_id_list', [])
        vmc_product_id_list = vals.get('vmc_sales_select_product_id', [])

        # 不能将售货机或者商品置为空
        if not vmc_id_list and not vmc_product_id_list:
            raise UserError('不允许将售货机或者商品置为空！！！')

        # 当前记录中的售货机和商品列表
        vmc_list = vals.get('vmc_sales_id_list')[0][2] if vmc_id_list else []
        product_list = vals.get('vmc_sales_select_product_id')[0][2] if vmc_product_id_list else []

        # 循环对售货机进行判断
        for vmc in range(len(vmc_list)):
            sql = """
                    select a.vmc_sales_id, a.machine_id from vmc_sales_machines_list a
                    left join vmc_promotion b on b.id=a.vmc_sales_id
                    where a.machine_id = %d and b.start_using ='t'
                """ % (vmc_list[vmc])

            cr = self.env.cr
            cr.execute(sql)
            result_vmc = cr.dictfetchall()

            # 售货机的情况下：判断商品是否存在
            for i in range(len(result_vmc)):
                sql = """
                        select vmc_select_id, product_id from vmc_product_promotion_rel where vmc_select_id = %d
                    """ % (result_vmc[i]['vmc_sales_id'])

                cr.execute(sql)
                result_product = cr.dictfetchall()

                product_id_list = []
                # 查询的产品id列表
                for j in range(len(result_product)):
                    product_id_list.append(result_product[j]['product_id'])

                # 数据库中新增售货机对应的产品与当前记录中的产品是否有重复，有则进行下一步判断：日期
                product_merge_id_set = set(product_list) & set(product_id_list)

                # 获取数据库中与当前记录中的售货机配置疑似重复的售货机对应记录的相关信息
                sql = """
                        select start_date, end_date, start_time, end_time, period_of_start_date, period_of_end_date, promotion_time_type from vmc_promotion where id = %d
                    """ % (result_vmc[i]['vmc_sales_id'])

                cr.execute(sql)
                result = cr.dictfetchall()

                # 售货机和商品都存在，判断是否在同一时间段内
                if list(product_merge_id_set):
                    # 1：按每天的时间段 2：按时间段
                    if vals.get('promotion_time_type') == '1' and result[0]['promotion_time_type'] == '1':
                        # 日期在不允许范围内，在日期范围再进行判断是否在时间点范围内
                        if ((vals.get('start_date') >= result[0]['start_date'] and vals.get('start_date') <= result[0][
                            'end_date']) and vals.get('end_date') >= result[0]['end_date']) or \
                                ((vals.get('end_date') >= result[0]['start_date'] and vals.get('end_date') <= result[0][
                                    'end_date']) and vals.get('start_date') <= result[0]['start_date']) or \
                                (vals.get('start_date') <= result[0]['start_date'] and vals.get('end_date') >=
                                    result[0]['end_date']) or \
                                (vals.get('start_date') >= result[0]['start_date'] and vals.get('end_date') <=
                                    result[0]['end_date']):
                            if ((vals.get('start_time') >= result[0]['start_time'] and vals.get('start_time') <=
                                result[0]['end_time']) and vals.get('end_time') >= result[0]['end_time']) or \
                                    ((vals.get('end_time') >= result[0]['start_time'] and vals.get('end_time') <=
                                        result[0]['end_time']) and vals.get('start_time') <= result[0]['start_time']) or \
                                    (vals.get('start_time') <= result[0]['start_time'] and vals.get('end_time') >=
                                        result[0]['end_time']) or \
                                    (vals.get('start_time') >= result[0]['start_time'] and vals.get('end_time') <=
                                        result[0]['end_time']):

                                # 提取报错相关信息
                                if len(list(product_merge_id_set)) > 1:
                                    sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id in %s
                                        """ % (str(tuple(list(product_merge_id_set))))
                                else:
                                    sql = """
                                                select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                    where a.id = %d
                                            """ % (list(product_merge_id_set)[0])

                                cr.execute(sql)
                                error_result_product = cr.dictfetchall()
                                error_list_product = []

                                for error in range(len(error_result_product)):
                                    error_list_product.append(error_result_product[error].get('name', ''))

                                error_str_product = ','.join(error_list_product)

                                sql = """
                                        select vmc.id,partner.name from vmc_machines as vmc
                                        left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                        left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                        left join res_partner as partner on vmc.partner_id=partner.id
                                        where machine_id = %d and promotion.start_using='t'
                                    """ % (result_vmc[i]['machine_id'])
                                cr.execute(sql)
                                error_result_vmc = cr.dictfetchall()
                                error_str_vmc = error_result_vmc[0]['name']

                                raise UserError(
                                    '商品"{0}"已经有促销信息！！！在售货机"{1}"已经配置！！！'.format(error_str_product, error_str_vmc))

                    # 当前记录为第一种时间类型，查询到的为第二种时间类型，将第二种转化为第一种，比较日期是否有重叠：年月日
                    elif vals.get('promotion_time_type') == '1' and result[0]['promotion_time_type'] == '2':
                        period_of_start_date_list = result[0]['period_of_start_date'].split(' ')
                        period_of_end_date_list = result[0]['period_of_end_date'].split(' ')

                        start_date = period_of_start_date_list[0]
                        end_date = period_of_end_date_list[0]

                        # 日期在不允许范围内，在日期范围再进行判断是否在时间点范围内
                        if ((vals.get('start_date') >= start_date and vals.get('start_date') <= end_date) and vals.get(
                                'end_date') >= end_date) or \
                                ((vals.get('end_date') >= start_date and vals.get('end_date') <= end_date) and vals.get(
                                    'start_date') <= start_date) or \
                                (vals.get('start_date') <= start_date and vals.get('end_date') >= end_date) or \
                                (vals.get('start_date') >= start_date and vals.get('end_date') <= end_date):

                            # 提取报错相关信息
                            if len(list(product_merge_id_set)) > 1:
                                sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id in %s
                                        """ % (str(tuple(list(product_merge_id_set))))
                            else:
                                sql = """
                                                select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                    where a.id = %d
                                            """ % (list(product_merge_id_set)[0])

                            cr.execute(sql)
                            error_result_product = cr.dictfetchall()
                            error_list_product = []

                            for error in range(len(error_result_product)):
                                error_list_product.append(error_result_product[error].get('name', ''))

                            error_str_product = ','.join(error_list_product)

                            sql = """
                                        select vmc.id,partner.name from vmc_machines as vmc
                                        left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                        left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                        left join res_partner as partner  on vmc.partner_id=partner.id
                                        where machine_id = %d and promotion.start_using ='t'
                                    """ % (result_vmc[i]['machine_id'])
                            cr.execute(sql)
                            error_result_vmc = cr.dictfetchall()
                            error_str_vmc = error_result_vmc[0]['name']

                            raise UserError('商品"{0}"在售货机"{1}"中可能会有相同配置！！！已经存在第二种时间类型的促销记录！！！'.format(error_str_product,
                                                                                                     error_str_vmc))

                    # 按时间段
                    elif vals.get('promotion_time_type') == '2' and result[0]['promotion_time_type'] == '2':
                        if ((vals.get('period_of_start_date') >= result[0]['period_of_start_date'] and vals.get(
                                'period_of_start_date') <= result[0]['period_of_end_date']) and vals.get(
                                'period_of_end_date') >= result[0]['period_of_end_date']) or \
                                ((vals.get('period_of_end_date') >= result[0]['period_of_start_date'] and vals.get(
                                    'period_of_end_date') <= result[0]['period_of_end_date']) and vals.get(
                                    'period_of_start_date') <= result[0]['period_of_start_date']) or \
                                (vals.get('period_of_start_date') <= result[0]['period_of_start_date'] and vals.get(
                                    'period_of_end_date') >= result[0]['period_of_end_date']) or \
                                (vals.get('period_of_start_date') >= result[0]['period_of_start_date'] and vals.get(
                                    'period_of_end_date') <= result[0]['period_of_end_date']):
                            # 提取报错相关信息
                            if len(list(product_merge_id_set)) > 1:
                                sql = """
                                        select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                            where a.id in %s
                                    """ % (str(tuple(list(product_merge_id_set))))
                            else:
                                sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id = %d
                                        """ % (list(product_merge_id_set)[0])

                            cr.execute(sql)
                            error_result_product = cr.dictfetchall()
                            error_list_product = []

                            for error in range(len(error_result_product)):
                                error_list_product.append(error_result_product[error].get('name', ''))

                            error_str_product = ','.join(error_list_product)

                            sql = """
                                    select vmc.id,partner.name from vmc_machines as vmc
                                    left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                    left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                    left join res_partner as partner on vmc.partner_id=partner.id
                                    where machine_id = %d and promotion.start_using='t'
                                """ % (result_vmc[i]['machine_id'])
                            cr.execute(sql)
                            error_result_vmc = cr.dictfetchall()
                            error_str_vmc = error_result_vmc[0]['name']

                            raise UserError(
                                '商品"{0}"已经有促销信息！！！在售货机"{1}"已经配置！！！'.format(error_str_product, error_str_vmc))

                    # 当前记录为第二种时间类型，查询到的为第一种时间类型，将第二种转化为第一种，比较日期是否有重叠：年月日
                    elif vals.get('promotion_time_type') == '2' and result[0]['promotion_time_type'] == '1':
                        start_date = vals.get('period_of_start_date').split(' ')[0]
                        end_date = vals.get('period_of_end_date').split(' ')[0]

                        if ((start_date >= result[0]['start_date'] and start_date <= result[0][
                            'end_date']) and end_date >= result[0]['end_date']) or \
                                ((end_date >= result[0]['start_date'] and end_date <= result[0][
                                    'end_date']) and start_date <= result[0]['start_date']) or \
                                (start_date <= result[0]['start_date'] and end_date >= result[0]['end_date']) or \
                                (start_date >= result[0]['start_date'] and end_date <= result[0]['end_date']):
                            # 提取报错相关信息
                            if len(list(product_merge_id_set)) > 1:
                                sql = """
                                        select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                            where a.id in %s
                                    """ % (str(tuple(list(product_merge_id_set))))
                            else:
                                sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id = %d
                                        """ % (list(product_merge_id_set)[0])

                            cr.execute(sql)
                            error_result_product = cr.dictfetchall()
                            error_list_product = []

                            for error in range(len(error_result_product)):
                                error_list_product.append(error_result_product[error].get('name', ''))

                            error_str_product = ','.join(error_list_product)

                            sql = """
                                    select vmc.id,partner.name from vmc_machines as vmc
                                    left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                    left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                    left join res_partner as partner on vmc.partner_id=partner.id
                                    where machine_id = %d and promotion.start_using ='t'
                                """ % (result_vmc[i]['machine_id'])
                            cr.execute(sql)
                            error_result_vmc = cr.dictfetchall()
                            error_str_vmc = error_result_vmc[0]['name']

                            raise UserError('商品"{0}"在售货机"{1}"中可能会有相同配置！！！已经存在第一种时间类型的促销记录！！！'.format(error_str_product,
                                                                                                     error_str_vmc))

        settings = super(VmcSalesPromotion, self).create(vals)
        return settings

    @api.one
    def write(self, vals):
        # 获取原记录中的售货机和商品
        if self.start_using:
            raise UserError('该促销规则已经启用，不允许修改,只允许使用停用按钮来停用该促销！！！')
        sql_1 = """
                select vmc.id,a.machine_id,partner.name,promotion.start_date,promotion.end_date,promotion.start_time,
                promotion.end_time,promotion.period_of_start_date,promotion.period_of_end_date,
                promotion.promotion_time_type from vmc_machines as vmc
                left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                left join res_partner as partner on vmc.partner_id=partner.id
                where promotion.id= %d
            """ % (self.id)

        cr = self.env.cr
        cr.execute(sql_1)

        original_vmc_dict = cr.dictfetchall()

        sql_2 = """
                select vmc_select_id, product_id from vmc_product_promotion_rel where vmc_select_id = %d
            """ % (self.id)
        cr.execute(sql_2)
        original_product_dict = cr.dictfetchall()

        original_machine_list = []
        original_product_list = []

        for m in range(len(original_vmc_dict)):
            original_machine_list.append(original_vmc_dict[m].get('machine_id', ''))

        for n in range(len(original_product_dict)):
            original_product_list.append(original_product_dict[n].get('product_id', ''))

        # 修改记录：售货机和商品是否被修改
        vmc_id_list = vals.get('vmc_sales_id_list', [])
        vmc_product_id_list = vals.get('vmc_sales_select_product_id', [])

        # 售货机和商品被修改：获取最新的售货机和商品列表并且找出原记录未有的售货机进行重新判断
        vmc_list = vals.get('vmc_sales_id_list')[0][2] if vmc_id_list else original_machine_list
        product_list = vals.get('vmc_sales_select_product_id')[0][2] if vmc_product_id_list else original_product_list

        # 新增的售货机需要重新判断
        compare_original_vmc = list(set(vmc_list) - set(original_machine_list))
        if not compare_original_vmc:
            compare_original_vmc = original_machine_list

        # 被修改的记录售货机和商品不为空，所以修改后也不为空
        if not vmc_list and not product_list:
            raise UserError('不允许将售货机或者商品置为空！！！')

        # 循环对售货机进行判断(排除当前被修改的原记录)
        # for vmc in range(len(compare_original_vmc)):
        for vmc in range(len(vmc_list)):
            sql = """
                    select vmc_sales_id, machine_id from vmc_sales_machines_list where machine_id = %d and vmc_sales_id != %d
                """ % (vmc_list[vmc], self.id)

            cr.execute(sql)
            result_vmc = cr.dictfetchall()

            # 售货机存在的情况下：判断商品是否存在
            for i in range(len(result_vmc)):
                sql = """
                        select vmc_select_id, product_id from vmc_product_promotion_rel where vmc_select_id = %d
                    """ % (result_vmc[i]['vmc_sales_id'])

                cr.execute(sql)
                result_product = cr.dictfetchall()

                product_id_list = []
                # 获取数据库中新增售货机对应的产品列表
                for j in range(len(result_product)):
                    product_id_list.append(result_product[j]['product_id'])

                # 数据库中新增售货机对应的产品与当前被修改的记录中的产品是否有重复，有则进行下一步判断：日期
                product_merge_id_set = list(set(product_id_list) & set(product_list))

                # 获取数据库中新增售货机对应记录的日期信息，作为与当前记录日期作判断
                sql = """
                        select start_date, end_date, start_time, end_time,period_of_start_date,period_of_end_date,promotion_time_type from vmc_promotion where id = %d
                    """ % (result_vmc[i]['vmc_sales_id'])

                cr.execute(sql)
                result = cr.dictfetchall()

                # 若当前记录行中日期以及时间类型未被修改则默认从原表取数据
                start_date = (
                original_vmc_dict[0]['start_date'] if not vals.get('start_date', []) else vals.get('start_date'))
                end_date = (original_vmc_dict[0]['end_date'] if not vals.get('end_date', []) else vals.get('end_date'))
                start_time = (
                original_vmc_dict[0]['start_time'] if not vals.get('start_time', []) else vals.get('start_time'))
                end_time = (original_vmc_dict[0]['end_time'] if not vals.get('end_time', []) else vals.get('end_time'))
                period_of_start_date = (
                original_vmc_dict[0]['period_of_start_date'] if not vals.get('period_of_start_date', []) else vals.get(
                    'period_of_start_date'))
                period_of_end_date = (
                original_vmc_dict[0]['period_of_end_date'] if not vals.get('period_of_end_date', []) else vals.get(
                    'period_of_end_date'))
                promotion_time_type = (
                original_vmc_dict[0]['promotion_time_type'] if not vals.get('promotion_time_type', []) else vals.get(
                    'promotion_time_type'))

                # 售货机和商品都存在，判断是否在同一时间段内
                if product_merge_id_set:
                    # 原始记录是第一种类型，当前记录为第二种类型，转化为第一种类型
                    if (original_vmc_dict[0]['promotion_time_type'] == '1' and promotion_time_type == '2') or (
                            original_vmc_dict[0]['promotion_time_type'] == '2' and promotion_time_type == '2'):
                        if result[0]['promotion_time_type'] == '1':

                            start_date = period_of_start_date.split(' ')[0]
                            end_date = period_of_end_date.split(' ')[0]

                            # 日期在不允许范围内，在日期范围再进行判断是否在时间点范围内
                            if ((start_date >= result[0]['start_date'] and start_date <= result[0][
                                'end_date']) and end_date >= result[0]['end_date']) or \
                                    ((end_date >= result[0]['start_date'] and end_date <= result[0][
                                        'end_date']) and start_date <= result[0]['start_date']) or \
                                    (start_date <= result[0]['start_date'] and end_date >= result[0]['end_date']) or \
                                    (start_date >= result[0]['start_date'] and end_date <= result[0]['end_date']):

                                # 提取报错相关信息
                                if len(product_merge_id_set) > 1:
                                    sql = """
                                               select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                   where a.id in %s
                                           """ % (str(tuple(product_merge_id_set)))
                                else:
                                    sql = """
                                               select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                   where a.id = %d
                                           """ % (product_merge_id_set[0])

                                cr = self.env.cr
                                cr.execute(sql)
                                error_result_product = cr.dictfetchall()
                                error_list_product = []

                                for error in range(len(error_result_product)):
                                    error_list_product.append(error_result_product[error].get('name', ''))

                                error_str_product = ','.join(error_list_product)

                                sql = """
                                          select vmc.id,partner.name from vmc_machines as vmc
                                          left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                          left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                          left join res_partner as partner on vmc.partner_id=partner.id
                                          where machine_id = %d
                                      """ % (result_vmc[i]['machine_id'])
                                cr.execute(sql)
                                error_result_vmc = cr.dictfetchall()
                                error_str_vmc = error_result_vmc[0]['name']

                                if original_vmc_dict[0]['promotion_time_type'] == '2' and promotion_time_type == '2':
                                    raise UserError(
                                        '商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第二种类型，当前记录修改为第二种类型！！！已经存在第一种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))

                                elif original_vmc_dict[0]['promotion_time_type'] == '1' and promotion_time_type == '2':
                                    raise UserError(
                                        '商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第一种类型，当前记录修改为第二种类型！！！已经存在第一种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))

                        elif result[0]['promotion_time_type'] == '2':
                            # 日期在不允许范围内，在日期范围再进行判断是否在时间点范围内
                            if ((period_of_start_date >= result[0]['period_of_start_date'] and period_of_start_date <=
                                result[0]['period_of_end_date']) and period_of_end_date >= result[0][
                                'period_of_end_date']) or \
                                    ((period_of_end_date >= result[0]['period_of_start_date'] and end_date <= result[0][
                                        'period_of_end_date']) and period_of_start_date <= result[0][
                                        'period_of_start_date']) or \
                                    (start_date <= result[0]['period_of_start_date'] and end_date >= result[0][
                                        'period_of_end_date']) or \
                                    (start_date >= result[0]['period_of_start_date'] and end_date <= result[0][
                                        'period_of_end_date']):

                                # 提取报错相关信息
                                if len(product_merge_id_set) > 1:
                                    sql = """
                                               select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                   where a.id in %s
                                           """ % (str(tuple(product_merge_id_set)))
                                else:
                                    sql = """
                                               select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                   where a.id = %d
                                           """ % (product_merge_id_set[0])

                                cr = self.env.cr
                                cr.execute(sql)
                                error_result_product = cr.dictfetchall()
                                error_list_product = []

                                for error in range(len(error_result_product)):
                                    error_list_product.append(error_result_product[error].get('name', ''))

                                error_str_product = ','.join(error_list_product)

                                sql = """
                                          select vmc.id,partner.name from vmc_machines as vmc
                                          left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                          left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                          left join res_partner as partner on vmc.partner_id=partner.id
                                          where machine_id = %d
                                      """ % (result_vmc[i]['machine_id'])
                                cr.execute(sql)
                                error_result_vmc = cr.dictfetchall()
                                error_str_vmc = error_result_vmc[0]['name']

                                if original_vmc_dict[0]['promotion_time_type'] == '2' and promotion_time_type == '2':
                                    raise UserError(
                                        '33商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第二种类型，当前记录修改为第二种类型！！！已经存在第一种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))

                                elif original_vmc_dict[0]['promotion_time_type'] == '1' and promotion_time_type == '2':
                                    raise UserError(
                                        '商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第一种类型，当前记录修改为第二种类型！！！已经存在第二种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))



                    # 原始记录是第二种类型，当前记录为第一种类型，转化为第二种类型
                    elif (original_vmc_dict[0]['promotion_time_type'] == '2' and promotion_time_type == '1') or (
                            original_vmc_dict[0]['promotion_time_type'] == '1' and promotion_time_type == '1'):
                        if result[0]['promotion_time_type'] == '1':
                            # 取原始记录当中的日期分割成年月日和十分秒形式
                            if ((start_date >= result[0]['start_date'] and start_date <= result[0][
                                'end_date']) and end_date >= result[0]['end_date']) or \
                                    ((end_date >= result[0]['start_date'] and end_date <= result[0][
                                        'end_date']) and start_date <= result[0]['start_date']) or \
                                    (start_date <= result[0]['start_date'] and end_date >= result[0]['end_date']) or \
                                    (start_date >= result[0]['start_date'] and end_date <= result[0]['end_date']):
                                # 提取报错相关信息
                                if len(product_merge_id_set) > 1:
                                    sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id in %s
                                        """ % (str(tuple(product_merge_id_set)))
                                else:
                                    sql = """
                                                select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                    where a.id = %d
                                            """ % (product_merge_id_set[0])

                                cr = self.env.cr
                                cr.execute(sql)
                                error_result_product = cr.dictfetchall()
                                error_list_product = []

                                for error in range(len(error_result_product)):
                                    error_list_product.append(error_result_product[error].get('name', ''))

                                error_str_product = ','.join(error_list_product)

                                sql = """
                                       select vmc.id,partner.name from vmc_machines as vmc
                                       left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                       left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                       left join res_partner as partner on vmc.partner_id=partner.id
                                       where machine_id = %d
                                   """ % (result_vmc[i]['machine_id'])
                                cr.execute(sql)
                                error_result_vmc = cr.dictfetchall()
                                error_str_vmc = error_result_vmc[0]['name']

                                if original_vmc_dict[0]['promotion_time_type'] == '1' and promotion_time_type == '1':
                                    raise UserError(
                                        '商品"{0}"已经有促销信息！！！在售货机"{1}"已经配置！！！'.format(error_str_product, error_str_vmc))

                                elif original_vmc_dict[0]['promotion_time_type'] == '2' and promotion_time_type == '1':
                                    raise UserError(
                                        '商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第二种类型，当前记录为第一种类型！！！已经存在第一种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))

                        elif result[0]['promotion_time_type'] == '2':
                            # 取原始记录当中的日期分割成年月日和十分秒形式
                            start_date_original_list = result[0]['period_of_start_date'].split(' ')
                            end_date_original_list = result[0]['period_of_start_date'].split(' ')

                            start_date_original = start_date_original_list[0]
                            end_date_original = end_date_original_list[0]

                            if ((
                                        start_date >= start_date_original and start_date <= end_date_original) and end_date >= end_date_original) or \
                                    ((
                                             end_date >= start_date_original and end_date <= end_date_original) and start_date <= start_date_original) or \
                                    (start_date <= start_date_original and end_date >= end_date_original) or \
                                    (start_date >= start_date_original and end_date <= end_date_original):
                                # 提取报错相关信息
                                if len(product_merge_id_set) > 1:
                                    sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id in %s
                                        """ % (str(tuple(product_merge_id_set)))
                                else:
                                    sql = """
                                            select b.name,b.id,b.name from product_product as a left join product_template as b on a.product_tmpl_id=b.id
                                                where a.id = %d
                                        """ % (product_merge_id_set[0])

                                cr = self.env.cr
                                cr.execute(sql)
                                error_result_product = cr.dictfetchall()
                                error_list_product = []

                                for error in range(len(error_result_product)):
                                    error_list_product.append(error_result_product[error].get('name', ''))

                                error_str_product = ','.join(error_list_product)

                                sql = """
                                       select vmc.id,partner.name from vmc_machines as vmc
                                       left join vmc_sales_machines_list as a on vmc.id=a.machine_id
                                       left join vmc_promotion as promotion on a.vmc_sales_id=promotion.id
                                       left join res_partner as partner on vmc.partner_id=partner.id
                                       where machine_id = %d
                                   """ % (result_vmc[i]['machine_id'])
                                cr.execute(sql)
                                error_result_vmc = cr.dictfetchall()
                                error_str_vmc = error_result_vmc[0]['name']

                                if original_vmc_dict[0]['promotion_time_type'] == '1' and promotion_time_type == '1':
                                    raise UserError(
                                        '商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第一种类型，当前记录为第一种类型！！！已经存在第二种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))

                                elif original_vmc_dict[0]['promotion_time_type'] == '2' and promotion_time_type == '1':
                                    raise UserError(
                                        '88商品"{0}"在售货机"{1}"中可能会有相同配置！！！原始记录是第二种类型，当前记录为第一种类型！！！已经存在第二种时间类型的促销记录！！'.format(
                                            error_str_product, error_str_vmc))

        # 商品"{0}"在售货机"{1}"中可能会有相同配置！！！已经存在第一种时间类型的促销记录！！！
        settings = super(VmcSalesPromotion, self).write(vals)
        return settings


class VmcSalesPromotionLines(models.Model):
    _name = 'vmc.promotion.lines'

    name = fields.Char(
        string="名称",
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='产品',
    )

    quantity = fields.Integer(
        string='数量',
        default=1
    )

    description = fields.Char(
        string='备注',
    )

    promotion_id = fields.Many2one(
        comodel_name='vmc.promotion',
        string='促销名称',
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='Company',
        readonly=True,
        default=lambda self: self.env.user.company_id,
    )