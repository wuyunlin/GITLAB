# -*- coding: utf-8 -*-
#  author = scofield.yu



from odoo import api, fields, models,_
from odoo.http import request
from odoo.exceptions import UserError


class money(models.Model):
    _name='vmc.money'
    _rec_name = 'name_id'


    name_id=fields.Char(
        string='编号'
    )

    supply_operator = fields.Many2one(
        'res.users',
        string="补货员"
    )
    create_uid = fields.Many2one(
        'res.users',
        string = '制单人'
    )
    write_uid = fields.Many2one(
        'res.users',
        string = '修改人'
    )
    write_data=fields.Datetime(
        string='修改日期'
     )
    pre_money=fields.Float(
        digits=(6, 2),
        string='预支硬币'
    )

    should_money=fields.Float(
        digits=(6, 2),
        string='应归还硬币',
        compute='s_money'
    )

    real_money=fields.Float(
        digits=(6, 2),
        string='实际归还硬币',

     )

    operator=fields.Char(
        string='操作员'
    )

    advance_time = fields.Date(string='预支日期')
    note = fields.Text(string="备注")

    state = fields.Selection(
        string='预支硬币状态',
        selection='_get_state',
        default='draft'
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        related='supply_operator.company_id',
        readonly=True,
    )

    #新增预支记录

    @api.model
    def _get_state(self):
        return [
            ('draft', '草稿'),
            ('confirm1', '已取硬币'),
            ('confirm2', '已还硬币'),
        ]

    @api.multi
    def action_confirm(self):
        if self.pre_money==0:
            raise UserError(_('预支硬币不能为0'))
        self.write({'state': 'confirm1'})

    @api.multi
    def action_confirm2(self):
        self.write({'state': 'confirm2'})
        for r in self:
            if not r.supply_operator:
                pass
            else:
                request.cr.execute("""update vmc_money_record set state='done' where create_uid=%s and state='confirm' """%(r.supply_operator.id,))

    @api.model
    def create(self, vals):
        name = self.env['ir.sequence'].next_by_code('vmc.money')
        vals.update({'name_id': name})
        res = super(money, self).create(vals)
        return res

    @api.depends('pre_money','supply_operator')
    def s_money(self):
        for r in self:
            if not r.supply_operator:
                r.should_money =0
            else:
                request.cr.execute("""
                   select sum(amount) from vmc_money_record
                    where type = 'giveby_supply'
                    and create_uid=%s and state in ('confirm','draft')
              """%(r.supply_operator.id,))
                fet = request.cr.fetchone()
                res=fet and fet[0] or 0
                r.should_money= r.pre_money-round(res,2)


