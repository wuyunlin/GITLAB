# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
from odoo import fields, models, api
from odoo.exceptions import ValidationError


class VmcAdvertisementList(models.Model):

    _name = 'vmc.advertisement.list'

    name = fields.Char(
        string="名称",
        required=True
    )

    vmc_advertisement_list_line_ids = fields.One2many(
        comodel_name="vmc.advertisement.list.line",
        inverse_name='vmc_advertisement_list_id',
        string='广告',
    )

    vmc_advertisement_list_line_copy_ids = fields.One2many(
        comodel_name="vmc.advertisement.list.line.copy",
        inverse_name='vmc_advertisement_list_copy_id',
        string='广告',
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        readonly=True,
        default=lambda self: self.env.user.company_id,
    )

    advertisement_id_list = fields.Many2many('vmc.machines', 'vmc_machines_advertisement_list', 'url_id', 'machine','售货机')

    # @api.multi
    # def unlink(self):
    #     raise ValidationError('不能删除，因为有关联！')
    #     return super(VmcAdvertisementList, self).unlink()

class VmcAdvertisementListLine(models.Model):

    _name = 'vmc.advertisement.list.line'

    vmc_advertisement_id = fields.Many2one(
        comodel_name="vmc.advertisement",
        string='广告',
    )

    vmc_advertisement_list_id = fields.Many2one(
        comodel_name='vmc.advertisement.list',
        string='广告列表',
    )

    # todo: shoulde check the unique, the play_sequence cann't be duplicated
    play_sequence = fields.Integer(
        string='播放顺序'
    )

    description = fields.Char(
        string='备注',
    )

    type = fields.Char(
        string='类型',
    )

    # @api.one
    @api.onchange('vmc_advertisement_id')
    def onchange_vmc_advertisement_id(self):
        if self.vmc_advertisement_id.type == 'IMAGE':
            self.type='图片'
        if self.vmc_advertisement_id.type == 'VIDEO':
            self.type='视频'


class VmcAdvertisementListLineCopy(models.Model):

    _name = 'vmc.advertisement.list.line.copy'

    vmc_advertisement_id = fields.Many2one(
        comodel_name="vmc.advertisement",
        string='广告',
    )

    vmc_advertisement_list_copy_id = fields.Many2one(
        comodel_name='vmc.advertisement.list',
        string='广告列表',
    )

    # todo: shoulde check the unique, the play_sequence cann't be duplicated
    play_sequence = fields.Integer(
        string='播放顺序'
    )

    description = fields.Char(
        string='备注',
    )

    type = fields.Char(
        string='类型',
    )

    # @api.one
    @api.onchange('vmc_advertisement_id')
    def onchange_vmc_advertisement_id(self):
        if self.vmc_advertisement_id.type == 'IMAGE':
            self.type='图片'
        if self.vmc_advertisement_id.type == 'VIDEO':
            self.type='视频'