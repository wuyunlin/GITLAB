# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models, _
from odoo.http import request
from datetime import datetime
from odoo.exceptions import UserError, ValidationError
import json
import logging
from odoo.addons.hollywant_base.tools.jpush_message import jpush_message
import odoo.tools.config as config
import traceback

_logger = logging.getLogger(__name__)
_test_logger = logging.getLogger('补货日志')


class VmcAdjustStock(models.Model):
    _name = 'vmc.adjust.stock'
    _order = 'create_date desc, id desc'
    _rec_name = 'adjust_stock_code'

    # @api.multi
    @api.depends('vmc_machine_id.state_id','vmc_machine_id.city_id','vmc_machine_id.county')
    def _compute_country(self):
        for line in self:
            if not line.vmc_machine_id:
                self.country = ''
            else:
                country = '%s%s%s' % (line.vmc_machine_id.state_id and line.vmc_machine_id.state_id.name or '',
                                      line.vmc_machine_id.city_id and line.vmc_machine_id.city_id.name or '',
                                      line.vmc_machine_id.county and line.vmc_machine_id.county.name or '')
                line.country = country
   # @api.multi
    @api.depends('vmc_machine_id')
    def _compute(self):

        for line in self:
            if not line.vmc_machine_id:
                self.vmc_machine_id = False
            else:
                sql = """select id from vmc_swap_order where vmc_machine_id=%s and state='confirm' """ % (line.vmc_machine_id.id)
                request.cr.execute(sql)
                fet = request.cr.fetchall()
                if fet:
                    if len(fet) > 1:
                        country=False
                    vmc_swap_order = fet[0][0]
                else:
                    vmc_swap_order=False
                line.vmc_swap_order_id = vmc_swap_order


    country=fields.Char(string='区县',compute='_compute_country',store=True)
    vmc_swap_order_id=fields.Many2one('vmc.swap.order',compute='_compute',string='换货单',store=True)

    name = fields.Char(
        string=''
    )

    adjust_stock_code = fields.Char(
        string="补货单编号"
    )

    storehouse = fields.Many2one(
        'stock.warehouse',
        string="提货仓库"
    )

    supply_operator = fields.Many2one(
        'res.users',
        string="补货员"
    )

    supply_date = fields.Datetime(
        string='补货时间'
    )
    vmc_machine_id = fields.Many2one(
        'vmc.machines',
        string='售货机名称',
        required=True
    )

    picking_ids = fields.Many2many('stock.move', 'adjust_move_rel', 'vmc_adjust_stock_id', 'stock_move_id', string='调拨记录',
       readonly=True)

    warehouse_id = fields.Many2one(
        comodel_name='stock.warehouse',
        string='关联仓库',
        related='vmc_machine_id.warehouse_id',
        readonly=True
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        related='vmc_machine_id.company_id',
        readonly=True
    )


    detailed_address = fields.Char(
        related='vmc_machine_id.street2',
        string='详细地址',
    )

    placing_position = fields.Char(
        related='vmc_machine_id.street',
        string='摆放位置',
    )

    vmc_code = fields.Char(
        string='简称',
        related='vmc_machine_id.vmc_code',
        readonly=True)

    vmc_brand = fields.Char(
        string='品牌',
        related='vmc_machine_id.vmc_brand',
        readonly=True
    )

    stack_qty = fields.Integer(
        string='料道数',
        related='vmc_machine_id.stack_qty',
        readonly=True
    )

    factory_code = fields.Char(
        string='出厂编码',
        related='vmc_machine_id.factory_code',
        readonly=True
    )

    machine_sync = fields.Char(
        string='售货机同步状态'
    )

    payment_alipay = fields.Boolean(
        string='支付宝',
        related='vmc_machine_id.payment_alipay',
        readonly=True
    )

    payment_weixin = fields.Boolean(
        string='微信',
        related='vmc_machine_id.payment_weixin',
        readonly=True
    )

    payment_wangbi = fields.Boolean(
        string='旺币',
        related='vmc_machine_id.payment_wangbi',
        readonly=True
    )

    payment_cash = fields.Boolean(
        string='现金',
        related='vmc_machine_id.payment_cash',
        readonly=True
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='所属公司 ',
        related='vmc_machine_id.company_id',
        readonly=True,
    )

    property_adjust_stock_lines = fields.One2many(
        comodel_name='vmc.adjust.stock.line',
        inverse_name='vmc_adjust_stock_id',
        string='售货机货道',
    )

    state = fields.Selection(
        string='状态',
        selection='_get_state',
       default='draft'
    )
    is_withdrawn = fields.Boolean(string='是否已取款', default=False)

    parameter = fields.Text(string='报文参数')

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcAdjustStock, self).unlink()

    @api.model
    def _get_state(self):
        return [
            ('draft', '草稿'),
            ('confirm', '已确认'),
            ('on_taking_delivery', '提货中'),
            ('after_taked_delivery', '已提货'),
            ('reslenishment', '补货中'),
            ('finished', '已补货'),
            ('overtime', '超时'),
            ('cancel', '已取消')
        ]

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})
     
    @api.multi
    def action_reset(self):
        self.write({'state': 'draft'})

    @api.multi
    def action_confirm(self):
        if not (self.supply_operator and self.storehouse):
            raise UserError(_('请先填写好补货员和提货仓库'))
        # sql = """
        # select id from vmc_adjust_stock where vmc_machine_id=%s and state='finished' and  is_withdrawn=False
        # """ % (self.vmc_machine_id.id,)
        # request.cr.execute(sql)
        # fet = request.cr.fetchall()
        # if len(fet) >= 1:
        #     id_list = [x[0] for x in fet]
        #     adjust_stock_code_list = []
        #     for i in id_list:
        #         request.cr.execute("""select adjust_stock_code from vmc_adjust_stock where id=%s""" % (i,))
        #         result = request.cr.fetchall()
        #         adjust_stock_code_list.append(result[0][0])

        #     adjust_stock_code_str = ','.join(adjust_stock_code_list)

        #     self.write({'instructions_state': 'draft'})
        #     raise UserError('该售货机已有处于已补货但未取款的补货单（%s）！'% (adjust_stock_code_str,))
        # else:
        self.write({'state': 'confirm'})
        if not self.vmc_machine_id.payment_cash:
            self.write({'is_withdrawn':'True'})
        self.procure_order_action_send_jpushmessage()
        _test_logger.debug("补货已经确认，开始调用补货方法!")

    @api.one
    def procure_order_action_send_jpushmessage(self):
        content = "收到新的售货机补货需求，前往查看！"
        title = "补货提醒"

        try:
            #修改成用config文件
            hollywant_jpush_key=config.get('hollywant_stock_jpush_key',None)
            hollywant_jpush_url =config.get('hollywant_jpush_url',  'https://api.jpush.cn/v3/push')
            # flag：False连ios测试环境，True 连ios正式环境，默认是正式环境
            flag = config.get('hollywant_stock_jpush_env_flag',  True)

            _test_logger.debug("配置参数获取：hollywant_jpush_key = %s\thollywant_jpush_url = %s"
                               "\thollywant_jpush_env_flag = %s" % (
                               hollywant_jpush_key or '', hollywant_jpush_url, flag))
            jpush_message1 = jpush_message(hollywant_jpush_url, hollywant_jpush_key)
            tags = [ str(self.supply_operator.id) ]
            _test_logger.debug("补货消息推送完成 : tags = %s" %tags)
            return jpush_message1.send_jpush_mesage(content, title, tags, flag, extras={"procure_order_id": self.id  })
        except:
            _logger.info(traceback.format_exc())
            _logger.error(u'确认补货单消息推送异常')
    # @api.multi
    # def action_save_draft(self):
    #     self.write({'state': 'draft'})

    # @api.one
    @api.onchange('vmc_machine_id')
    def onchange_vmc_machine_id(self):
        self.property_adjust_stock_lines = False
        vmc_swap_order=0
        if self.vmc_machine_id:
            self.country='%s%s%s'%(self.vmc_machine_id.state_id and self.vmc_machine_id.state_id.name or '',
                                   self.vmc_machine_id.city_id and self.vmc_machine_id.city_id.name or '',
                                   self.vmc_machine_id.county and self.vmc_machine_id.county.name  or '')
            #self.detailed_address=self.vmc_machine_id.street2 or ''
            #self.placing_position=self.vmc_machine_id.street or ''
            #self.factory_code = self.vmc_machine_id.factory_code or ''
            sql="""select id from vmc_swap_order where vmc_machine_id=%s and state='confirm' """%(self.vmc_machine_id.id)
            request.cr.execute(sql)
            fet = request.cr.fetchall()
            if fet:
                if len(fet)>1:
                    raise UserError(_('该售货机处于确认状态的换货单有%s张！'%len(fet)))
                vmc_swap_order=fet[0][0]
            #####更新仓库库存到stock
            machine_line_obj=self.env['vmc.machine.stacks']
            for line in self.vmc_machine_id.machine_stack:
                line.write({'stock':line.stock_qty})
            sql="""
            ---换货单
            with a as (
            select a.id, c.product_id,c.box_number, c.stack_number, b.swap_product_id as product_new, b.type,
            coalesce(c.stack_volume, 0) as qty from vmc_swap_order a
            left join vmc_swap_order_line b on b.vmc_swap_id=a.id
            inner join vmc_machine_stacks c on c.id=b.vmc_machine_stack_id and (coalesce(c.product_id,0) != coalesce(b.swap_product_id,0) or b.type='remove')
            where a.id=%s  ),
            ---售货机
            b as (
            select
            b.id as vmc_machine_stack_id, b.product_id,
            ------库存 < 0 取容量 容量大于库存取容量－库存 容量小于库存取0
            case when coalesce(b.stock, 0) < 0
            then coalesce(b.stack_volume, 0)
            when coalesce(b.stack_volume, 0) - coalesce(b.stock, 0) > 0
            then coalesce(b.stack_volume, 0) - coalesce(b.stock, 0)
            else 0
            end as qty, b.box_number,b.stack_number
            from vmc_machines a
            left join vmc_machine_stacks b on b.vmc_machine_id = a.id
            where  a.id = %s)
            ---------料道号                                                                                                  补货需求
            select b.vmc_machine_stack_id, b.product_id as origin_product_id,
            --补货商品
            case when a.type='remove' then null  else
            coalesce(a.product_new, b.product_id) end as adjust_product_id,

             case when a.type='remove' then 0
             when a.type in ('add','change') then coalesce(a.qty, 0)
            else coalesce(b.qty, 0) end as replenish_demand
            from b left  join  a   on a.stack_number = b.stack_number and a.box_number=b.box_number
            where coalesce(a.product_new, b.product_id) is not null
            order by b.stack_number"""%(vmc_swap_order,self.vmc_machine_id.id)
            request.cr.execute(sql)
            fet=request.cr.dictfetchall()
            self.property_adjust_stock_lines = fet or []
            self.vmc_swap_order_id=vmc_swap_order

    @api.model
    def create(self, vals):
        name = self.env['ir.sequence'].next_by_code('vmc.adjust.stock')
        vals.update({'adjust_stock_code': name})
        res = super(VmcAdjustStock, self).create(vals)
        return res

    @api.constrains('state')
    def _check_unique(self):
        state =self.state
        if state in ('confirm','on_taking_delivery','after_taked_delivery','reslenishment') and self.vmc_machine_id:
            self._cr.execute("""select count(*) from vmc_adjust_stock  where state in ('confirm','on_taking_delivery','after_taked_delivery','reslenishment')
                                 and vmc_machine_id=%s and id <>%s """,(self.vmc_machine_id.id,self.id or 0))
            count = self._cr.fetchone()
            if count[0] > 0:
              raise ValidationError(_('该售货机处于已确认，提货中，已提货,补货中状态的单据已存在！'))
        return True

    #todo
    @api.multi
    def _check_adjust_state(self, keep_hour):
        _logger.debug("Begin _check_adjust_state ...")
        cr=self.env.cr
        cr.execute(
					"""update vmc_adjust_stock set state='overtime' where state in ('draft','confirm','on_taking_delivery','after_taked_delivery','reslenishment')
			           and write_date<now()-interval'8 hours'-interval'%s hours'
					"""%keep_hour)
        _logger.debug("END _check_adjust_state ...")
        return True

    @api.multi
    def _check_adjust_reslenishment(self):
        _logger.debug("开始调用_check_adjust_reslenishment接口")
        sql="""select parameter from vmc_adjust_stock where state='reslenishment'  """
        self.env.cr.execute(sql)
        fet=self.env.cr.fetchall()
        if not fet:
            return True
        for line in fet:
            args=()
            kwargs=json.loads(line[0])
            if kwargs:
                self._finish_reslenishment(*args, **kwargs)
            else:
                raise  ValidationError('')
        _logger.debug("完成调用_check_adjust_reslenishment接口")

    @api.multi
    def _finish_reslenishment(self,*args,**kwargs):
        _logger.debug("Begin _finish_reslenishment ...")
        try:
            _logger.debug("补货记录参数 _finish_reslenishment"+str(kwargs))
            # 1 创建补货记录
            move_obj = self.env['stock.move'].sudo()
            move_ids = []
            vmc_clear_record_obj = self.env['vmc.clear.record']
            # 售货机ID
            machine_id = kwargs['machine_id']
            uid = kwargs['uid']
            vmc_adjust_stock_id = kwargs['vmc_adjust_stock_id']
            group_id1 = group_id2 = 0
            # 创建补货记录
            vmc_procurement_move_vals = {'vmc_adjust_stock_id': vmc_adjust_stock_id, 'vmc_machines_id': machine_id}
            sql_updatestacks = sql_updatesupplier = sql_updatechange = ''
            for line in kwargs['record']:
                if not group_id1:
                    group_id1 = self.env['procurement.group'].create({'name': kwargs['code'] + u'补货'}).id
                product_id = int(line['supply_product']['id'])
                product_id2 = product_id
                # 取得产品单位
                sql = """select uom_id from product_product a
                   left join product_template b on b.id=a.product_tmpl_id where a.id=%s""" % (product_id)
                self.env.cr.execute(sql)
                uom_id = self.env.cr.fetchone()[0]
                # 取得产品对应的货道的库位
                sql = """select b.stack_location_id from vmc_machines a
                   left join vmc_machine_stacks b on b.vmc_machine_id=a.id where b.stack_number='%s' and b.box_number ='%s' and b.vmc_machine_id=%s """ % (
                line['stack']['stack_no'], line['stack']['box_no'], machine_id)
                self.env.cr.execute(sql)
                fet = self.env.cr.fetchone()
                if not fet:
                    _logger.error(u"补货记录：没有找到货到对应的库位")
                    return {'msg': u'没有找到货道对应的库位:%s' % (line['stack']['stack_no'])}
                stock_location_id = fet[0]
                # move 字典
                move_vals = {
                    'name': kwargs['code'] + u':补货',
                    'company_id': kwargs['company_id'],
                    'product_id': product_id,
                    'product_uom': uom_id,
                    'product_uom_qty': line['supply_product']['actual_count'],
                    'location_id': kwargs['car_stock_loctaion_id'],
                    'location_dest_id': stock_location_id,
                    'procure_method': 'make_to_stock',
                    'origin': kwargs['code'] + u':补货',
                    'picking_type_id': kwargs['int_type_id'],
                    'date': kwargs['create_date'],
                    'date_expected': kwargs['create_date'],
                    'group_id': group_id1,
                    'warehouse_id': kwargs['common_stock_warehouse_id'],
                    'supply_id': uid,
                    'svm_type': u'补货'
                }

                move_ids += [move_obj.create(move_vals).id]
                if not sql_updatechange:
                    sql_updatechange = """update vmc_swap_order set state='swaped' where
                                       id =(select coalesce(vmc_swap_order_id,0) from vmc_adjust_stock where id=%s);""" % (
                    vmc_adjust_stock_id)
                # 更新售货机货道信息；产品＋数量 注意库存数量不能大于货道容量
                if not line['product_change']:
                    # sql = """select sum(stock),sum(stack_volume) from vmc_machine_stacks where coalesce(stock,0)+%s>stack_volume
                    #         and vmc_machine_id=%s and stack_number='%s' and box_number ='%s';""" % (
                    # line['supply_product']['actual_count'], machine_id, line['stack']['stack_no'],
                    # line['stack']['box_no'])
                    sql = """select sum(stock),sum(stack_volume) from vmc_machine_stacks where %s>stack_volume
                            and vmc_machine_id=%s and stack_number='%s' and box_number ='%s';""" % (
                        line['supply_product']['actual_count'], machine_id, line['stack']['stack_no'],
                        line['stack']['box_no'])
                    self.env.cr.execute(sql)
                    fet = self.env.cr.fetchone()
                    if fet[1]:
                        self.env.cr.rollback()
                        return {'msg': u'不换货:货道(%s)对应的补货数量(%s)超过货道容量(%s)' % (
                        line['stack']['stack_no'], line['supply_product']['actual_count'], fet[1])}

                if line['product_change']:
                    sql = """select sum(stock),sum(stack_volume) from vmc_machine_stacks where %s>stack_volume
                             and vmc_machine_id=%s and stack_number='%s' and box_number ='%s' """ % (
                    line['supply_product']['actual_count'], machine_id, line['stack']['stack_no'],
                    line['stack']['box_no'])
                    self.env.cr.execute(sql)
                    fet = self.env.cr.fetchone()
                    if fet[1]:
                        self.env.cr.rollback()
                        return {'msg': u'货道(%s)对应的换货数量(%s)超过货道容量(%s)' % (
                            line['stack']['stack_no'], fet[0], fet[1])}

                sql1 = """update vmc_machine_stacks set stock=coalesce(stock,0)+%s where vmc_machine_id=%s and
                          stack_number='%s' and box_number ='%s' ; """ % (
                line['supply_product']['actual_count'], machine_id, line['stack']['stack_no'],
                line['stack']['box_no'])

                # 更新实际补货数量
                sql_updatesupplier += """update vmc_adjust_stock_line set replenish_count_actual=%s where vmc_adjust_stock_id=%s and
                                       vmc_machine_stack_id in (select id from vmc_machine_stacks where stack_number='%s'
                                       and vmc_machine_id=%s and box_number ='%s'); """ % (
                line['supply_product']['actual_count'], vmc_adjust_stock_id, line['stack']['stack_no'], machine_id,
                line['stack']['box_no'])
                # 有换货的情况下，创建清空记录 售货机-->车库位
                if line['product_change'] and  int(line['current_product']['id']):
                    if not group_id2:
                        group_id2 = self.env['procurement.group'].create({
                            'name': kwargs['code'] + u':换货'}).id
                    product_id = int(line['current_product']['id'])
                    sql = """select uom_id from product_product a
                       left join product_template b on b.id=a.product_tmpl_id where a.id=%s""" % (product_id)
                    self.env.cr.execute(sql)
                    uom_id = self.env.cr.fetchone()[0]
                    if line['current_product']['stock']:
                        move_vals = {
                            'name': kwargs['code'] + u':清空',
                            'company_id': kwargs['company_id'],
                            'product_id': product_id,
                            'product_uom': uom_id,
                            'product_uom_qty': abs(line['current_product']['stock']),
                            'location_dest_id': line['current_product']['stock']>0 and kwargs['car_stock_loctaion_id'] or stock_location_id,
                            'location_id': line['current_product']['stock']>0 and stock_location_id or 5,
                            'procure_method': 'make_to_stock',
                            'origin': kwargs['code'] + u':清空',
                            'picking_type_id': kwargs['int_type_id'],
                            'date': kwargs['create_date'],
                            'date_expected': kwargs['create_date'],
                            'group_id': group_id2,
                            'supply_id': uid,
                            'svm_type': u'清空'
                        }
                        move_ids += [move_obj.create(move_vals).id]
                        _logger.debug("500==move_ids"+str(move_ids))
                    # 更新售货机货道信息；产品＋数量 注意库存数量不能大于货道容量

                    # sql1 = """update vmc_machine_stacks set product_id=%s,stock=%s where vmc_machine_id=%s and
                    #           stack_number='%s' and box_number ='%s'; """ % (
                    # product_id2, line['supply_product']['actual_count'], machine_id, line['stack']['stack_no'],
                    # line['stack']['box_no'])
                # sql_updatesupplier +=sql1
                # self.env.cr.execute(sql1)
            if move_ids:
                _logger.error("move_ids="+str(move_ids))
                move_obj.browse(move_ids).action_confirm()
                move_obj.browse(move_ids).force_assign()
                move_obj.browse(move_ids).action_done()
                sql = """update stock_pack_operation set qty_done=product_qty where picking_id in
                         ( select distinct picking_id  as picking_id  from stock_move where id in %s);
                         ----picking与补货单建立联系
                         insert into adjust_move_rel (vmc_adjust_stock_id,stock_move_id)
                         select %s,id  as move_id  from stock_move where id in %s""" % (
                tuple(move_ids + [0, -1]), vmc_adjust_stock_id, tuple(move_ids + [0, -1]),)
                self.env.cr.execute(sql)
            # 4 更新售货机货道信息；产品＋数量
            if sql_updatestacks:
                self.env.cr.execute(sql_updatestacks)
                _logger.debug(u"补货记录：更新售货机货到信息")
            # 5 补货单更新为补货完成，更新实际补货数量
            supply_date = str(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
            sql_updatesupplier += """update vmc_adjust_stock set state='finished', supply_date = '%s' where id=%s;
            """ % (supply_date, vmc_adjust_stock_id,)
            # 6 更新换货单状态为已经换货
            if sql_updatechange:
                sql_updatesupplier += sql_updatechange
            self.env.cr.execute(sql_updatesupplier)
        except Exception, e:
            self.env.cr.rollback()
            _logger.error("except _finish_reslenishment ..."+str(e))
            return {'msg': u'task1:补货完成接口出错(%s)' % str(e)}
        _logger.debug("END _finish_reslenishment ...")
        return True



class VmcAdjustStockLine(models.Model):
    _name = 'vmc.adjust.stock.line'

    name = fields.Char(
        string="名称",
    )

    vmc_adjust_stock_id = fields.Many2one(
        comodel_name='vmc.adjust.stock',
        string='补货单',
    )

    vmc_machine_stack_id = fields.Many2one(
        comodel_name='vmc.machine.stacks',
        string='料道号',
    )

    box_number = fields.Selection(
        string = '货柜',
        related = 'vmc_machine_stack_id.box_number',
        readonly = True
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='商品',
        related='vmc_machine_stack_id.product_id',
        readonly=True,
    )
    
    origin_product_id = fields.Many2one(
        comodel_name='product.product',
        string='原商品',
    )
    adjust_product_id = fields.Many2one(
        comodel_name='product.product',
        string='补货商品',
    )

    adjust_sequence_no = fields.Char(
        string='补货商品序列号',
    )

    stock = fields.Integer(
        string='库存',
        related='vmc_machine_stack_id.stock',
        readonly=True
    )

    sequence_no = fields.Char(
        srting='产品序列号',
        related='vmc_machine_stack_id.product_sequence_no',
        readonly=True
    )

    stack_volume = fields.Integer(
        string='容量',
        related='vmc_machine_stack_id.stack_volume',
        readonly=True
    )

    stack_location_id = fields.Many2one(
        comodel_name="stock.location",
        string="仓库位置",
        related='vmc_machine_stack_id.stack_location_id',
        readonly=True
    )

    replenish_demand = fields.Integer(
        string='补货需求'
    )

    replenish_count_actual = fields.Integer(
        string='实际补货数量'
    )
    @api.multi
    def onchange_replenish_demand(self, replenish_demand,stack_volume):
        result={}
        if  replenish_demand > stack_volume:
            result = {'value': {'replenish_demand': stack_volume >0 and stack_volume or 0}}
            result['warning'] = {'title': _('Warning'),
			                     'message': _(
				                     '您选择的补货需求(%s)>容量(%s),不满足要求，现已填写补货数量更新为%s'%(replenish_demand,stack_volume,stack_volume>0 and stack_volume or 0))}
        return result