# -*- coding: utf-8 -*-
from odoo import fields, models
import logging
_logger = logging.getLogger(__name__)


class stock_picking(models.Model):
    _inherit = "stock.picking"

    supply_id = fields.Many2one(
        'res.users',
        string='补货员',
    )

    svm_type = fields.Char(
        string='订单类型'
    )


class VmcAdjustStock(models.Model):
    _inherit = 'vmc.adjust.stock'

    def get_combine_order_lines(self, vmc_adjust_stocks):
        res = {}

        for vmc_adjust_stock in vmc_adjust_stocks:
            for line in vmc_adjust_stock.property_adjust_stock_lines:
                product = line.product_id
                product_name = product.name_get()[0][1]
                replenish_demand = line.replenish_demand
                if not res.get(product.id, False):
                    res[product.id] = {
                        'product_name': product_name,
                        'replenish_demand': replenish_demand
                    }
                else:
                    res[product.id]['replenish_demand'] += replenish_demand

        combine = []

        for rec in res:
            combine.append(
                [
                    res[rec]['product_name'],
                    res[rec]['replenish_demand']
                ]
            )
        return combine
