# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class product_templ_svm(models.Model):
    _inherit = 'product.template'

    net_weight = fields.Char(
        string='净含量',
        default=""
    )
    product_describe = fields.Html()
