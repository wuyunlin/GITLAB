# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.http import request
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class VmcMachine(models.Model):
    _name = 'vmc.machines'
    _inherits = {'res.partner': 'partner_id'}

    # vmc2water=fields.Many2one('water.god.repair')

    @api.multi
    def _check_filter_day(self):
        pass
    partner_id = fields.Many2one(
        'res.partner', ondelete='restrict',required=True
    )
    # vmc_name = fields.Char('名称',required = True) 直接使用partner Name
    vmc_code = fields.Char('资产编号', required=True)
    box_qty = fields.Char('货柜数量')
    stack_qty = fields.Integer('料道数量')
    vmc_brand = fields.Char('售货机品牌')
    factory_code = fields.Char('出厂编码')
    coordinate = fields.Char('坐标')
    location = fields.Char('售货机位置', readonly=True)
    payment_alipay = fields.Boolean('支付宝')
    payment_alipay_account = fields.Char('支付宝支付账号') 
    payment_weixin = fields.Boolean('微信')
    payment_weixin_account = fields.Char('微信支付账号') 
    payment_wangbi = fields.Boolean('旺币')
    payment_wangbi_account = fields.Char('旺币支付账号') 
    payment_cash = fields.Boolean('现金')
    payment_other = fields.Boolean('其他')
    company_id = fields.Many2one(comodel_name='res.company',
        string='公司',
        readonly=True,
        default=lambda self: self.env.user.company_id,)

    is_disable = fields.Selection(string="是否停用", selection='_get_is_disable_type', default='false')
    location_name = fields.Many2one('vmc.location.manage', '点位名称')

    county = fields.Many2one("res.country.state.city.county", 'County', ondelete='restrict')
    county_name = fields.Char(string='county_name')
    state_name = fields.Char(string='state_name')
    city_id = fields.Many2one("res.country.state.city", 'City', ondelete='restrict')

    #下面是水神加的字段
    electrolytic_status = fields.Boolean('电解液是否充足', readonly="true")
    filter_elem_day = fields.Integer('滤芯剩余天数', compute=_check_filter_day, default=30)
    filter_elem_date = fields.Date('滤芯更换日期')

    ph_value = fields.Char('PH值')
    acc_value = fields.Char('ACC值')
    water_degree = fields.Char('水表度数')
    # water_press = fields.Char('水压')
    # net_status = fields.Char('网络状况')

    # type=fields.Char('机器类型')
    @api.model
    def _get_is_disable_type(self):
        return [
            ('true', '停用'),
            ('false', '未停用'),
        ]

    machine_stack = fields.One2many(
        'vmc.machine.stacks',
        'vmc_machine_id',
        string='自动售货机货道',
        copy=True
    )
    machine_monitoring = fields.One2many(
        'vmc.machine.monitoring',
        'vmc_machine_id',
        string='售货机故障信息',
        copy=True
    )
    replenishment_record = fields.One2many(
        'vmc.adjust.stock',
        'vmc_machine_id',
        string='补货记录',
    )
    error_info = fields.Char(
        string="售货机状态",
        default="正常"
    )
    error_info_color = fields.Char(
        string="故障信息颜色标签",
        default='green',
    )
    net_status = fields.Char('网络状态')
    error_status = fields.Char('故障状态')
    door_open_status = fields.Boolean('开关门状态')
    app_version = fields.Char('客户端版本')
    total_crash = fields.Float(string="现金余额")
    cash_capacity = fields.Float(string='硬币器容量')
    warehouse_id = fields.Many2one(
        "stock.warehouse",
        ondelete='restrict'
    )
    warn_qty = fields.Integer("警戒数量", default=5)
    stock_state = fields.Char(
        string="库存状态",
        default='正常',
    )
    stock_state_color = fields.Char(
        string="库存状态颜色标签",
        default='green',
    )
    last_update_time = fields.Datetime(
        string="上次同步时间",
        readonly=True
    )

    vmc_machine_settings_id = fields.Many2one(
        comodel_name='vmc.settings',
        string='售货机参数配置'
    )

    vmc_settings_status = fields.Boolean(
        string='售货机设置状态标志',
        default=False
    )




    _sql_constraints = [
        ('factory_code_uniq', 'unique (factory_code)', '售货机设备出厂编码必须唯一 !')
    ]

    @api.constrains('coordinate')
    def _constrains_coordinate(self):
        find_index=self.coordinate.find(',')
        if find_index == -1:
                raise ValidationError('请以逗号分隔经纬度')
        else:
            pass 

    @api.onchange('location_name')
    def onchange_location(self):
        if self.location_name:
            sql="""select county,location_address,state_id,city_id,state_name,county_name from vmc_location_manage where id=%s"""%self.location_name.id
            request.cr.execute(sql)
            fet = request.cr.dictfetchone()
            self.county = fet['county']
            self.city_id = fet['city_id']
            self.state_id = fet['state_id']
            self.street2=fet['location_address']


    @api.one
    def action_is_disable(self):
        if self.is_disable == 'true':
            self.write({'is_disable': 'false'})
        else:
            self.write({'is_disable': 'true'})

    # test
    @api.model
    def cg_test(self):
        _logger.error("cg test:")
        return {'msg':'you are good'}

    # 修改创建过程和仓库和location关联起来
    @api.model
    def create(self, vals):
        machine = super(VmcMachine, self).create(vals)
        self.link_stack_to_location(machine)
        request.cr.execute("""
        insert into vmc_machine_monitoring
        (vmc_machine_id, is_location_abnormal, is_dooropen,
        is_load_soldout, is_papermoney_stop, is_leakchange_5jiao, is_leakchange_1yuan, is_vmc_disconnected)
        values
        (%s, '0', 'false', 'false', 'false', 'false', 'false', 'false')
        """, (machine.id,))
        return machine

    @api.one
    def write(self, vals):
        result = super(VmcMachine, self).write(vals)
        machine = self
        self.link_stack_to_location(machine)

        request.cr.execute("""select stack_number,id from vmc_machine_stacks where vmc_machine_id=%s""", (self.id,))
        stack_number_result = request.cr.fetchall()

        stack_number_list = []
        id_list = []
        for i in range(len(stack_number_result)):
            stack_number_list.append(stack_number_result[i][0])
            id_list.append(stack_number_result[i][1])

        stack_number_id_list = []
        for x in stack_number_list:
            stack_number_id_list.append(int(x))

        for j in range(len(stack_number_list)):
            request.cr.execute("""
                update vmc_machine_stacks set stack_number_id=%s
                where vmc_machine_id=%s
                and id=%s""", (stack_number_id_list[j], self.id, id_list[j]))

        return result
    # def link_to_replenishment_record(self, machine):
    #     request.cr.execute("""select name,vmc_machine_id,vmc_code,company_id,vmc_brand,warehouse_id
    #         from vmc_machine_stacks
    #         where vmc_machine_id = %s""", (machine.id,))

    def link_stack_to_location(self, machine):
        warehouse = machine.warehouse_id
        # vmcMachine_stack = machine.machine_stack
        # stack_ids = vmcMachine_stack.ids
        request.cr.execute("""select id,stack_number,stack_location_id,
        case when box_number in ('1','9') then
             '食品柜'
        else '饮料柜' end as box
            from vmc_machine_stacks
            where vmc_machine_id = %s""", (machine.id,))
        stacks = request.cr.dictfetchall()
        warehouse_lot_stock_id = warehouse.lot_stock_id.id
        company_id = machine.company_id.id
        machine_name = machine.name

        for stack in stacks:
            if not stack['stack_location_id']:
                location = self.env['stock.location'].create({
                    'name': machine_name + "_"+stack['box'] + u"料道" + stack['stack_number'],
                    'active': True,
                    'usage': 'internal',
                    'location_id': warehouse_lot_stock_id,
                    'posx': stack['stack_number'],
                    'posy': 0,
                    'posz': 0,
                    'company_id': company_id,
                    'comment': 'vmc'})
                request.cr.execute("""update vmc_machine_stacks
                    set stack_location_id = %s
                    where id = %s """, (location.id, stack['id'],))
            else:
                request.cr.execute("""update stock_location set name = %s where id = %s """,
                                   (machine_name + "_"+ stack['box']+ u"料道" + stack['stack_number'], stack['stack_location_id'],))

        return True

    @api.multi
    def action_create_adjust_stock_order(self):
        self.ensure_one()
        return {
            'name': '库存补货记录',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'vmc.adjust.stock',
            'context':   {
                'default_vmc_machine_id': self.id
            }
        }

    @api.multi
    def action_machine_monitoring_info(self):
        self.ensure_one()
        return {
                'name': '售货机故障信息',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_id': self.machine_monitoring.id,
                'res_model': 'vmc.machine.monitoring',
                'context': {
                    'default_vmc_machine_id': self.id
                },
            }

    @api.onchange('machine_stack', 'warn_qty')
    def onchange_stock_status(self):
        self.ensure_one()

        total_stock = 0

        if not self.machine_stack:
            self.stock_state = '需补货'
            self.stock_state_color = 'red'
            return

        if self.machine_stack:
            for stack in self.machine_stack:
                if not stack.stock:
                    self.stock_state = '需补货'
                    self.stock_state_color = 'red'
                    return
                total_stock += stack.stock

        if total_stock <= 0:
            self.stock_state = '需补货'
            self.stock_state_color = 'red'
            return

        if total_stock < self.warn_qty:
            self.stock_state = '库存低'
            self.stock_state_color = 'orange'
            return

        self.stock_state = '正常'
        self.stock_state_color = 'green'

#todo
    # @api.cr_uid_context
    # def _check_machine_state(self, cr, uid, context=None):
    @api.multi
    def _check_machine_state(self,context=None):
        _logger.debug("Begin _check_machine_state ...")

        cr=self.env.cr
        cr.execute(
            """
                select id,last_update_time
                    from vmc_machines
            """
        )

        vmc_machines = cr.dictfetchall()

        _logger.debug("Error machine list is empty")

        error_machine_ids = [0]

        for machine in vmc_machines:
            _logger.debug("Check machine %s...", machine['id'])
            if not machine['last_update_time']:
                _logger.debug(
                    """The last update time of the machine %s is Null.
                    Add the machine into error machine list...""",
                    machine['id']
                )
                error_machine_ids.append(machine['id'])
            else:
                current_time = datetime.utcnow()
                _logger.debug("Current time is %s...", current_time)
                last_update_time = datetime.strptime(
                    machine['last_update_time'], '%Y-%m-%d %H:%M:%S')
                _logger.debug("Last_update_time is %s...", last_update_time)
                diff = current_time - last_update_time
                _logger.debug(
                    """The difference seconds between
                    current time and last update time is %s...""",
                    diff
                )
                if diff.total_seconds() > 120:
                    _logger.debug(
                        """The difference is more than 120s.
                        Add the machine into error machine list ..."""
                    )
                    error_machine_ids.append(machine['id'])

        _logger.debug("Error machine list is %s", tuple(error_machine_ids))

        if error_machine_ids:
            _logger.debug("Update the machine state to 网络异常")
            status = '网络异常'
            cr.execute(
                """
                    update vmc_machines set
                        net_status = %s,
                        error_info_color = 'red'
                    where id in %s
                """,
                (status, tuple(error_machine_ids),)
            )

        _logger.debug("End _check_machine_state ...")
        return

    def _send_one(self, channel, message):
        _logger.info('\nSend long polling')
        _logger.info(channel)
        _logger.info(message)
        _logger.info('\n')
        self.env['bus.bus'].sendone(channel, message)

    @api.model
    def _refresh_dashboard(self,*args):
        if args and len(args)==2:
            self._send_one(args[0],args[1])

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcMachine, self).unlink()


    type=fields.Selection([
        ('vmc', '售货机'),
        ('water_god', '水神')
    ], string='机器类型')




class VmcMachinePriceList(models.Model):
    _name = 'vmc.machine.price.list'

    name = fields.Char(
        string="价格表名称",
    )

    # vmc_machine_id = fields.Many2one(
    #     'vmc.machines',
    #     string='自动售货机',
    #     required=True,
    #     ondelete='cascade',
    #     index=True,
    #     copy=False
    # )


    vmc_sales_id_list = fields.Many2many('vmc.machines', 'vmc_sales_machines_rel', 'vmc_sales_id', 'machine_id',
                                             '选择售货机')

    product_price_list = fields.One2many(
        'vmc.product.price.list',
        'vmc_machine_price_list_id',
        string='商品',
        copy=True
    )
    # vmc_product_price_list = fields.Many2many(
    #     'vmc.product.price.list', 'vmc_product_price_list_link', 'vmc_price_list_id', 'product_id',
    #     '选择商品')

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        readonly=True,
        default=lambda self: self.env.user.company_id,
    )


class VmcProductPriceList(models.Model):
    _name = 'vmc.product.price.list'

    specify_price = fields.Float(string='指定价格')

    input_price = fields.Float(related='product_id.list_price', string='产品价格')

    vmc_machine_price_list_id = fields.Many2one('vmc.machine.price.list', string='商品列表')

    product_id = fields.Many2one(
        'product.product',
        string='商品名称',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )

    # product_price = fields.Many2one('product.template', related='product_id.list_price')


class vmcMachine_stack(models.Model):
    _name = 'vmc.machine.stacks'

    @api.multi
    def _compute_stock(self):
        for line in self:
            if not (line.stack_location_id and line.product_id):
                line.stock_qty = 0
            else:
                sql="""with RECURSIVE cte as (
                       select id from stock_location a where a.id= %s
                       union all
                       select b.id from stock_location b inner join cte c on c.id=b.location_id
                       )
                       select sum(qty) from stock_quant a
                       where 1=1
                       ---and a.qty>0
                       and a.product_id= %s
                       ---and a.reservation_id is null
                       and a.company_id = %s
                       and a.location_id =
                       any(select id from cte)"""%(int(line.stack_location_id.id),int(line.product_id.id),int(line.vmc_machine_id.company_id.id))
                self.env.cr.execute(sql)
                fet=self.env.cr.fetchone()
                line.stock_qty = fet and fet[0] or 0
    box_number = fields.Selection(string='货柜', selection='_get_box_number_type',)
    stack_number = fields.Char('料道号', required=True)
    stack_number_id = fields.Integer('料道号序号')

    box_type = fields.Selection(
        string='货柜类型',
        selection='_get_box_type',
    )

    @api.model
    def _get_box_number_type(self):
        return [
            ('9', '食品柜'),
            ('11', '饮料柜'),
        ]

    @api.model
    def _get_box_type(self):
        return [
            ('goods', '商品柜'),
            ('drinks', '饮料柜'),
            ('foods', '食品柜'),
        ]

    stack_type = fields.Selection(
        string='料道类型',
        selection='_get_stack_type',
    )

    @api.model
    def _get_stack_type(self):
        return [
            ('snake_type', '蛇形料道'),
            ('spring_type', '弹簧料道'),
            ('convenient_type', '便利柜'),
        ]

    product_id = fields.Many2one(
        'product.product',
        string='商品名称',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )

    # @api.one
    @api.onchange('product_id')
    def onchange_product_id(self):
        self.product_price=self.product_id.list_price

    product_price = fields.Float('商品单价')
    vmc_machine_id = fields.Many2one(
        'vmc.machines',
        string='自动售货机',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )
    stock = fields.Integer('料道存量')
    stock_qty = fields.Integer(string='料道库存(仓库)', compute='_compute_stock')
    product_sequence_no = fields.Char('产品序列号',required=True)
    stack_volume = fields.Integer('料道容量')
    stack_location_id = fields.Many2one("stock.location", string="仓库位置")
    company_id = fields.Many2one('res.company', string="所属公司")
    name = fields.Char(
        related='stack_number'
    )

    @api.constrains('product_sequence_no')
    def _constrains_product_sequence_no(self):
        ox_list=['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','a','b','c','d','e','f']
        if len(self.product_sequence_no)!=2:
            raise ValidationError('产品序列号应为16进制的两位数')
        for x in  self.product_sequence_no:
            if x.isspace():
                raise ValidationError('产品序列号不能包含空格')
            elif x not in ox_list:
                raise ValidationError('产品序列号应该为16进制')
            else:
                pass

    # @api.constrains('stock', 'stack_volume')
    # def _constraint_on_stack(self):
    #     for line in self:
    #         if line.stock > line.stack_volume:
    #             raise ValidationError('料道存量不能大于料道容量')
    #         if line.stock < 0 or line.stack_volume < 0:
    #             raise ValidationError('料道存量,料道容量不能小于0')

    _sql_constraints = [
        (
            'machine_stack_box_number_uniq',
            'unique (vmc_machine_id,stack_number,box_number)',
            '单一售货机设备的货道编号必须唯一!'
        )
    ]

    @api.model
    def create(self, vals):
        if vals.get('stack_number',0):
            vals.update({'stack_number_id':vals.get('stack_number',0).isdigit()  and int(vals.get('stack_number',0) ) or 0})
        machine_stack = super(vmcMachine_stack, self).create(vals)
        return machine_stack

    @api.one
    def write(self, vals):
        if vals.get('stack_number',0):
            vals.update({'stack_number_id':vals.get('stack_number',0).isdigit()  and int(vals.get('stack_number',0) ) or 0})
        result = super(vmcMachine_stack, self).write(vals)

        return result

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        order = 'stack_number_id'
        return super(vmcMachine_stack, self).search(args, offset, limit, order, count=count)


class VmcMachine_monitoring(models.Model):
    _name = 'vmc.machine.monitoring'
    _rec_name = 'vmc_machine_id'

    monitoring_time = fields.Datetime(string="监控时间", readonly='True')
    is_location_abnormal = fields.Selection(string="地理位置", selection='_get_location_abnormal')
    is_dooropen = fields.Selection(string="开关门", selection='_get_dooropen_type')
    is_load_soldout = fields.Selection(string="料道售空", selection='_get_soldout_type')
    is_papermoney_stop = fields.Selection(string="纸币停用", selection='_get_papermoney_type')
    is_leakchange_5jiao = fields.Selection(string="五角找零不足", selection='_get_leakchange_5jiao_type')
    is_leakchange_1yuan = fields.Selection(string="一元找零不足", selection='_get_leakchange_1yuan_type')
    is_vmc_disconnected = fields.Selection(string="串口通讯异常", selection='_get_vmc_disconnected_type')

    net_status = fields.Char(related='vmc_machine_id.net_status', string='网络状态')

    @api.model
    def _get_location_abnormal(self):
        return [
            ('0', '正常'),
            ('1', '地理位置异常'),
            ('2', '无法获取地理位置')
        ]

    @api.model
    def _get_dooropen_type(self):
        return [
            ('false', '正常'),
            ('true', '门被打开'),
        ]

    @api.model
    def _get_soldout_type(self):
        return [
            ('false',  '正常'),
            ('true', '料道已经售空'),
        ]

    @api.model
    def _get_papermoney_type(self):
        return [
            ('false', '正常'),
            ('true', '纸币已经停用'),
        ]

    @api.model
    def _get_leakchange_5jiao_type(self):
        return [
            ('false', '正常'),
            ('true', '五角零钱不足'),
        ]

    @api.model
    def _get_leakchange_1yuan_type(self):
        return [
            ('false', '正常'),
            ('true', '一元零钱不足'),
        ]

    @api.model
    def _get_vmc_disconnected_type(self):
        return [
            ('false', '正常'),
            ('true', '串口通讯异常'),
        ]

    vmc_machine_id = fields.Many2one(
        'vmc.machines',
        string='自动售货机',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcMachine_monitoring, self).unlink()


class VmcInstructionsIssued(models.Model):
    _name = 'vmc.instructions.issued'
    _rec_name = "show_name"

    show_name = fields.Char(string="指令详情", default='指令详情')
    industrial_type = fields.Selection(string="指令类型", selection='_get_industrial_type', default='1')
    important_level = fields.Selection(string="重要性等级", selection='_get_important_level_type', default='1')
    issued_time = fields.Datetime(string="指令创建时间")
    run_time = fields.Datetime(string="指令执行时间", required=True)
    special_data = fields.Text(string="特殊数据")
    instructions_state = fields.Selection(
        string='指令状态',
        selection='_get_instructions_state',
        default='confirm'
    )

    @api.model
    def _get_industrial_type(self):
        return [
            ('1', '系统重启'),
            ('2', 'App重启'),
            ('3', '版本更新'),
        ]

    @api.model
    def _get_important_level_type(self):
        return [
            ('1', '一般'),
            ('2', '重要'),
            ('3', '非常重要'),
        ]

    @api.model
    def _get_instructions_state(self):
        return [
            ('confirm', '下发'),
            ('get_successful', '已获取'),
            ('run', '执行中'),
            ('finished', '完成'),
        ]

    run_machine = fields.Many2many(
        'vmc.machines', 'vmc_instruction_machines_relation', 'industrial_pc_restart', 'machines_id',
        string='执行的售货机', required=True
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcInstructionsIssued, self).unlink()

    # @api.multi
    # def action_push(self):
        # sql = """
        # select id from vmc_instructions_issued where vmc_machine_id=%s and instructions_state='confirm'"""%(self.vmc_machine_id.id)
        # sql = """
        # select vmc_machine_id from vmc_instruction_machines_relation
        # where industrial_pc_restart=%s"""%(self.id)
        #
        # request.cr.execute(sql)
        # fet = request.cr.fetchall()
        # if len(fet) >= 1:
        #     self.write({'instructions_state': 'draft'})
        #     raise UserError('该售货机已有处于确认状态的指令！')
        # else:
        # self.write({'instructions_state': 'confirm'})

    # @api.multi
    # def action_push_again(self):
    #     sql = """select id from vmc_instructions_issued where vmc_machine_id=%s and instructions_state='confirm'"""%(self.vmc_machine_id.id)
    #     request.cr.execute(sql)
    #     fet = request.cr.fetchall()
    #     if len(fet) >= 1:
    #         self.write({'instructions_state': 'issued_failure'})
    #         raise UserError('该售货机已有处于确认状态的指令！')
    #     else:
    #         self.write({'instructions_state': 'confirm'})
