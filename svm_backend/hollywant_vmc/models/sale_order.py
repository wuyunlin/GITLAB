# -*- coding: utf-8 -*-
import logging
from odoo import fields, models

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"
    shelf_number = fields.Char(
        string='Shelf Number', help="Shelf Number for Vending Order", size=5
    )


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    pay_method = fields.Selection([
        ('cash', 'Cash'),
        ('bank', 'Bank'),
        ('alipay', '支付宝'),
        ('wechat', '微信支付')
    ], string='Pay Method', store=True, readonly=True)
