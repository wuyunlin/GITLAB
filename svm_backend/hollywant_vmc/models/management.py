# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

import logging

_logger = logging.getLogger(__name__)


class vmc_area_manage(models.Model):
    _name = 'vmc.area.manage'
    _description = "vmc area manage"
    _rec_name = "area_name"
    # 区域表
    @api.model
    def _get_company_id(self):
        return self.env.user.company_id.id

    area_name = fields.Char('区域名称', required=True)
    # location_name = fields.One2many('vmc.location.manage', 'vmc_location_ids', string='点位ids', copy=True)
    route_name = fields.One2many('vmc.route.manage', 'area_name', string='线路名称')
    manager = fields.Char('区域负责人', required=True)
    mobile = fields.Char('手机号', required=True)
    todo = fields.Char('备注')
    company_id = fields.Many2one('res.company', '登录用户所属公司',default=_get_company_id)

    _sql_constraints = [
        ('area_name_uniq', 'unique (area_name,company_id)', '区域名称必须唯一！')
    ]

    # @api.multi
    # def unlink(self):
    #     if any(area.route_name for area in self):
    #         raise ValidationError('你不能删除，因为它关联了线路！')
    #     return super(vmc_area_manage, self).unlink()
    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(vmc_area_manage, self).unlink()



class vmc_route_manage(models.Model):
    _name = 'vmc.route.manage'
    _description = "vmc route manage"
    _rec_name = "route_name"
    # 线路表

    route_name = fields.Char('线路名称', required=True)
    area_name = fields.Many2one('vmc.area.manage', '区域名称', required=True)
    manager = fields.Char('线路负责人', required=True)
    mobile = fields.Char('手机号', required=True)
    todo = fields.Char('备注')

    _sql_constraints = [
        ('route_name_uniq', 'unique (route_name,company_id)', '线路名称必须唯一！')
    ]

    @api.model
    def _get_company_id(self):
        return self.env.user.company_id.id

    company_id = fields.Many2one('res.company', '登录用户所属公司',default=_get_company_id)

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(vmc_route_manage, self).unlink()


class vmc_location_manage(models.Model):
    _name = 'vmc.location.manage'
    _description = "vmc location manage"
    _rec_name = "name"

    street = fields.Char(
        string='Street',
    )

    street2 = fields.Char(
        string='Street2',
    )
    zip = fields.Char(
        string='zip',
    )
    city = fields.Char(
        string='City',
    )
    state_name = fields.Char(string='state_name')
    county = fields.Many2one("res.country.state.city.county", 'County', ondelete='restrict')
    state_id = fields.Many2one("res.country.state", 'State', ondelete='restrict')
    country_id =  fields.Many2one('res.country', 'Country', ondelete='restrict', default=49)
    city_id = fields.Many2one("res.country.state.city", 'City', ondelete='restrict')
    county_name = fields.Char(string='county_name', default="中国")

    name = fields.Char(string='点位名称', required=True)
    area_name = fields.Many2one('vmc.area.manage', '区域名称', required=True)

    route_name = fields.Many2one('vmc.route.manage', '线路名称', required=True)

    location_rental = fields.Integer('点位租金')
    location_rental_begin = fields.Date('租约开始日期')
    location_rental_end = fields.Date('租约结束日期')
    location_type = fields.Selection(
        [('商场', '商场'), ('写字楼/办公室', '写字楼/办公室'), ('校园', '校园'), ('医院', '医院'), ('工厂', '工厂'), ('其它', '其它')], '点位类型')
    location_address = fields.Char('点位具体地址')
    manager = fields.Many2one('res.users', '点位负责人', required=True)
    mobile = fields.Char('手机号', required=True)
    todo = fields.Char('备注')

    @api.model
    def _get_company_id(self):
        return self.env.user.company_id.id

    company_id = fields.Many2one('res.company', '登录用户所属公司',default=_get_company_id)

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(vmc_location_manage, self).unlink()

    _sql_constraints = [
        ('name_uniq', 'unique (name,company_id)', '点位名称必须唯一！')
    ]

    @api.onchange('state_id')
    def onchange_state_id(self):
        if self.state_id:
            self.city_id = self.env['res.country.state.city'].search([]).filtered(lambda r:r.parent_id==self.state_id)[0].id
        else:
            self.city_id = False

    @api.onchange('city_id')
    def onchange_city_id(self):
        if self.city_id:

            self.county = self.env['res.country.state.city.county'].search([]).filtered(lambda r:r.parent_id==self.city_id)[0].id
        else:
            self.county = ''

    @api.onchange('area_name')
    def onchange_area_name(self):
        if self.area_name and self.area_name.route_name:
            self.route_name = self.area_name.route_name[0]
        else:
            self.route_name = ''
    @api.multi
    def onchange_location_rental_end(self,  location_rental_begin, location_rental_end):
        if location_rental_begin:
            if location_rental_end < location_rental_begin:
                result = {'value': {'location_rental_end': location_rental_begin}}
                result['warning'] = {'title': _('Warning'), 'message': _('点位租约结束日期应该大于租约开始日期')}
                return result
    @api.multi
    def onchange_location_rental_begin(self, location_rental_begin, location_rental_end):
        if location_rental_end:
            if location_rental_end < location_rental_begin:
                result = {'value': {'location_rental_begin': location_rental_end}}
                result['warning'] = {'title': _('Warning'), 'message': _('点位租约开始日期不应大于租约结束日期')}
                return result


class vmc_machines_manage(models.Model):
    _name = 'vmc.machines.manage'
    _description = "vmc machines manage"
    _rec_name = "machine_name"

    #@api.one
    @api.model
    def _get_machine_balance(self):
        self.machine_balance = self.one_coin_brain + self.helf_coin_brain

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(vmc_machines_manage, self).unlink()

    machine_balance = fields.Integer(string='现金余额', compute=_get_machine_balance)
    machine_name = fields.Char('售货机名称', required=True)
    machine_code = fields.Char('售货机编号')

    coordinate = fields.Char('坐标')
    location = fields.Char('售货机摆放位置')
    payment_alipay = fields.Boolean('支付宝')
    payment_alipay_account = fields.Char('支付宝支付账号')
    payment_weixin = fields.Boolean('微信')
    payment_weixin_account = fields.Char('微信支付账号')
    payment_wangbi = fields.Boolean('旺币')
    payment_wangbi_account = fields.Char('旺币支付账号')
    payment_cash = fields.Boolean('现金')
    payment_other = fields.Boolean('其他')
    sim_account = fields.Char('sim卡号')
    active = fields.Boolean('售货机状态')

    location_name = fields.Many2one('vmc.location.manage', '点位名称')

    # machine_address = fields.Char(
    #     string='售货机地址'
    #  )

    @api.multi
    def _compute(self, fields, arg, context=None):
        for line in self:
            country = ''
            if line.location_name:
                country = '%s%s%s%s' % (line.location_name.state_id and line.location_name.state_id.name or '',
                                        line.location_name.city_id and line.location_name.city_id.name or '',
                                        line.location_name.county and line.location_name.county.name or '',
                                        line.location_name.location_address or '')
            self.country = country

    # _columns = {
    #     'machine_address': fields2.function(_compute, string='区县', type='char', store = True),
    # }
    machine_address=fields.Char(compute='_compute',string='区县', type='char', store = True)
    # 领用资产时写入
    machine_id = fields.Many2one(
        comodel_name='salemachine.assert.management',
        string='售货机',
    )

    assert_num = fields.Char(
        string='资产编号',
        related='machine_id.assert_num',
        readonly=True
    )

    company_id = fields.Many2one(
        string='所属公司 ',
        related='machine_id.company_id',
        readonly=True,
    )

    sale_machine_brand = fields.Char(
        string='售货机品牌',
        related='machine_id.sale_machine_brand',
        readonly=True
    )

    contain_num = fields.Integer(
        string='货柜数量',
        related='machine_id.contain_num',
        readonly=True,
    )

    one_coin_brain = fields.Float(
        string='一元容量',
        related='machine_id.one_coin_brain',
        readonly=True
    )

    helf_coin_brain = fields.Float(
        string='5角容量',
        related='machine_id.helf_coin_brain',
        readonly=True
    )

    #@api.one
    @api.onchange('location_name')
    def onchange_location_name(self):
        if self.location_name:
            self.machine_address = '%s%s%s%s' % (
                self.location_name.state_id and self.location_name.state_id.name or '',
                self.location_name.city_id and self.location_name.city_id.name or '',
                self.location_name.county and self.location_name.county.name or '',
                self.location_name.location_address or '',)

    @api.model
    def _get_is_contain1_show_default(self):
        if self.contain_num >= 1:
            self.is_contain1_show = True

    @api.model
    def _get_is_contain2_show_default(self):
        if self.contain_num >= 2:
            self.is_contain2_show = True

    @api.model
    def _get_is_contain3_show_default(self):
        if self.contain_num >= 3:
            self.is_contain3_show = True

    @api.model
    def _get_is_contain4_show_default(self):
        if self.contain_num >= 4:
            self.is_contain4_show = True

    @api.model
    def _get_is_contain5_show_default(self):
        if self.contain_num >= 5:
            self.is_contain5_show = True

    machine_contain1 = fields.One2many(
        'vmc.machines.contain.manage',
        'vmc_machine_manage1_id',
        string='自动售货机货柜',
        copy=True
    )
    is_contain1_show = fields.Boolean(compute=_get_is_contain1_show_default)

    machine_contain2 = fields.One2many(
        'vmc.machines.contain.manage',
        'vmc_machine_manage2_id',
        string='自动售货机货柜',
        copy=True
    )
    is_contain2_show = fields.Boolean(compute=_get_is_contain2_show_default)

    machine_contain3 = fields.One2many(
        'vmc.machines.contain.manage',
        'vmc_machine_manage3_id',
        string='自动售货机货柜',
        copy=True
    )
    is_contain3_show = fields.Boolean(compute=_get_is_contain3_show_default)

    machine_contain4 = fields.One2many(
        'vmc.machines.contain.manage',
        'vmc_machine_manage4_id',
        string='自动售货机货柜',
        copy=True
    )
    is_contain4_show = fields.Boolean(compute=_get_is_contain4_show_default)

    machine_contain5 = fields.One2many(
        'vmc.machines.contain.manage',
        'vmc_machine_manage5_id',
        string='自动售货机货柜',
        copy=True
    )
    is_contain5_show = fields.Boolean(compute=_get_is_contain5_show_default)

    _sql_constraints = [
        ('machine_code', 'unique (machine_code)', '售货机编号必须唯一 !')
    ]


class vmc_machines_contain_manage(models.Model):  # 货柜配置
    _name = 'vmc.machines.contain.manage'
    _description = "vmc machines contain manage"
    _rec_name = "channel_num"

    vmc_machine_manage1_id = fields.Many2one(
        'vmc.machines.manage',
        string='自动售货机配置',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )

    vmc_machine_manage2_id = fields.Many2one(
        'vmc.machines.manage',
        string='自动售货机配置',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )
    vmc_machine_manage3_id = fields.Many2one(
        'vmc.machines.manage',
        string='自动售货机配置',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )
    vmc_machine_manage4_id = fields.Many2one(
        'vmc.machines.manage',
        string='自动售货机配置',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )
    vmc_machine_manage5_id = fields.Many2one(
        'vmc.machines.manage',
        string='自动售货机配置',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )
    product_price = fields.Float(
        string='商品单价',
        related='product_id.lst_price',
        readonly=True
    )

    product_id = fields.Many2one(
        'product.product',
        string='商品名称',
        domain=[('sale_ok', '=', True)],
        change_default=True,
        ondelete='restrict'
    )
    contain_num = fields.Char('货柜号')
    channel_num = fields.Integer('料道号', required=True)
    channel_size = fields.Integer('料道容量')
    channel_stock = fields.Integer('料道存量')
    company_id = fields.Many2one('res.company', string="所属公司")

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(vmc_machines_contain_manage, self).unlink()
