# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from random import Random
import time
import logging
_logger = logging.getLogger(__name__)


ISOTIMEFORMAT = '%Y-%m-%d %X'


class VmcPickgoodsCode(models.Model):
    _name = 'vmc.pickgoods.code'
    _rec_name = 'pickgoods_code'

    pickgoods_code = fields.Char('提货码')
    pickgoods_code_num = fields.Integer('提货码数量')
    pickgoods_code_order = fields.Many2one('vmc.order', string='提货码订单')

    product_id = fields.Many2one(
        'product.product',
        string='产品',
    )

    user_machine=fields.Many2many(
        'vmc.machines','vmc_code_machines_relation','pickgoods_code','machines_id',
        string='可用售货机',
    )

    @api.model
    def _get_company_id(self, *args, **kwargs):
        return self.env.user.company_id.id

    company_id = fields.Many2one('res.company',string= u'所属公司',default=_get_company_id)

    is_valid = fields.Boolean('是否有效')

    start_time = fields.Datetime(
        string="起始时间",
    )
    end_time = fields.Datetime(
        string="截止时间",
    )

    order = fields.Many2one('vmc.order', string='订单')

    @api.multi
    def _check_picking_code_state(self):
        _logger.debug("Begin _check_picking_code_state ...")
        cr=self.env.cr
        cr.execute(
                    """update vmc_pickgoods_code set is_valid='False' where end_time<now()-interval'8 hours'
                    """)
        _logger.debug("END _check_picking_code_state ...")
        return True

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcPickgoodsCode, self).unlink()

class VmcPickgoodsCodeCreate(models.TransientModel):
    _name = 'vmc.pickgoods.code.create'
    _rec_name = 'rec_name'

    rec_name = fields.Char(default='提货码生成')
    pickgoods_code = fields.Char('提货码')
    pickgoods_code_num = fields.Integer('提货码数量')
    product_id = fields.Many2one(
        'product.product',
        string='产品',
        required=True
    )

    user_machine = fields.Many2many(
        'vmc.machines', 'vmc_code_machines_relation1', 'pickgoods_code', 'machines_id',
        string='可用售货机',
        required=True
    )

    is_valid = fields.Boolean('是否有效', default=True )

    start_time = fields.Datetime(
        string="起始时间",
    )
    end_time = fields.Datetime(
        string="截止时间",
    )

    order = fields.Many2one(
        'vmc.order',
        string='订单')

    @api.model
    def _get_company_id(self, *args, **kwargs):
        return self.env.user.company_id.id

    company_id = fields.Many2one('res.company', string=u'所属公司',default=_get_company_id)

    @api.constrains('pickgoods_code_num')
    def _constraints_on_pickgoods_code_num(self):
        for line in self:
            if line.pickgoods_code_num <= 0:
                raise ValidationError('提货码数量不能小于1')

    @api.constrains('start_time', 'end_time')
    def _constraints_on_time(self):
        for line in self:
            if line.start_time > line.end_time:
                raise ValidationError('起始时间不能大于截止时间')
            if line.end_time <= time.strftime(ISOTIMEFORMAT, time.localtime(time.time())):
                raise ValidationError('截止时间不能小于当前时间')

    @api.multi
    def create_pickgoods_code(self):
        self.ensure_one()
        act_obj = self.env['ir.actions.act_window']
        mod_obj = self.env['ir.model.data']

        def random_str():
            str1 = ''
            chars = '0123456789'
            length = len(chars) - 1
            random = Random()
            for i in range(7):
                str1 += chars[random.randint(0, length)]

            sum = 0
            for i in range(len(str1)):
                sum += int(str1[i]) * pow(2, i)

            eighth_digits = sum % 2
            str1 += str(eighth_digits)
            return str1

        pickgoods_code_list = []
        pickgoods_code_num = self.pickgoods_code_num or 0

        # while pickgoods_code_num:
        #     pickgoods_code_list.append(random_str())
        #     pickgoods_code_num -= 1

        def generateRand(counter):
            random_num = random_str()
            if (counter < pickgoods_code_num):
                if (random_num not in pickgoods_code_list):
                    pickgoods_code_list.append(random_num)
                    counter += 1
                generateRand(counter)

        generateRand(0)

        user_machine = []
        for i in self.user_machine:
            user_machine.append((4, i.id))
        code_ids = []
        vals = {'product_id': self.product_id and self.product_id.id or False,
                'is_valid': self.is_valid or False,
                'pickgoods_code_num': self.pickgoods_code_num or False,
                'start_time': self.start_time or False,
                'end_time': self.end_time or False,
                'user_machine': user_machine or False,
                'order': self.order and self.order.id or False}

        for code in pickgoods_code_list:
            vals['pickgoods_code'] = code
            code_ids.append(int(self.env['vmc.pickgoods.code'].create(vals)))

        view = mod_obj.get_object_reference('hollywant_vmc', 'action_vmc_pickgoods_code')
        view_id = view and view[1] or False
        result = act_obj.browse(view_id)[0].read([])[0]
        # result['domain'] = [('id', 'in', code_ids)]
        return result