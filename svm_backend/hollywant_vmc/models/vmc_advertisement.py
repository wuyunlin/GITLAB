# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
from odoo import api, fields, models
from odoo.exceptions import ValidationError
import urllib2


class VmcAdvertisement(models.Model):
    _name = 'vmc.advertisement'

    name = fields.Char(
        string="名称",
        required=True
    )

    type = fields.Selection(
        string='类型',
        selection='_get_type',
        default='IMAGE',
    )

    url = fields.Char(
        string='下载链接',
    )

    url_head = fields.Char(
        string='首页图片下载链接',
    )

    url_detail = fields.Char(
        string='详情页图片下载链接',
    )

    # company_id = fields.Many2one(
    #     comodel_name='res.company',
    #     string='公司',
    #     readonly=True
    # )

    priority = fields.Integer(
        string='优先级',
    )

    start_date = fields.Date(
        string="开始日期",
    )

    end_date = fields.Date(
        string="结束日期",
    )

    start_time = fields.Char(
        string="开始时间",
    )

    end_time = fields.Char(
        string="结束时间",
    )

    repeat_time = fields.Integer(
        string="轮播次数",
        default=0
    )

    last_update_time = fields.Datetime(
        string="上次同步时间",
        readonly=True
    )

    repeat_crycle = fields.Selection(
        string='重复周期',
        selection='_get_repeat_crycle',
        default='DAY',
    )

    per_day = fields.Boolean(
        string='每天'
    )

    per_monday = fields.Boolean(
        string='每周一'
    )

    per_tuesday = fields.Boolean(
        string='每周二'
    )

    per_wednesday = fields.Boolean(
        string='每周三'
    )

    per_thursday = fields.Boolean(
        string='每周四'
    )

    per_friday = fields.Boolean(
        string='每周五'
    )

    per_saturday = fields.Boolean(
        string='每周六'
    )

    per_sunday = fields.Boolean(
        string='每周日'
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='关联产品',
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        readonly=True,
        default=lambda self: self.env.user.company_id,
    )


    @api.model
    def _get_type(self):
        return [
            ('IMAGE', '图片'),
            ('VIDEO', '视频'),
        ]

    @api.model
    def _get_repeat_crycle(self):
        return [
            ('DAY', '按天'),
            ('WEEK', '按周'),
        ]

    @api.constrains('url')
    def _constraint_url_video_limit(self):
        def url_limit(url):
            try:
                res = urllib2.urlopen(urllib2.Request(url))
                code = res.getcode()
                res.close()
                return code
            except Exception, e:
                return 44

        if self.type == 'VIDEO':
            if self.url:
                a = self.url
                res = url_limit(a)
                if res == 200:
                    link_type = urllib2.urlopen(a)
                    if link_type.headers['Content-Type'].find('video') != -1:
                        pass
                    else:
                        raise ValidationError('链接不是视频类型')
                else:
                    raise ValidationError('链接无效')
            else:
                raise ValidationError('视频链接不能为空')
        else:
            pass


    @api.constrains('url_head', 'url_detail')
    def _constraint_url_image_limit(self):
        def url_limit1(url1):
            try:
                res = urllib2.urlopen(urllib2.Request(url1))
                code = res.getcode()
                res.close()
                return code
            except Exception, e:
                return 44

        if self.type == "IMAGE":
            if self.url_head and self.url_detail:
                url_head_first = self.url_head
                url_head_context = self.url_detail
                res = url_limit1(url_head_first)
                res1 = url_limit1(url_head_context)
                if res == 200 and res1 == 200:
                    first_link_type = urllib2.urlopen(url_head_first)
                    context_link_type = urllib2.urlopen(url_head_context)
                    if first_link_type.headers['Content-Type'].find('image') != -1 and context_link_type.headers[
                        'Content-Type'].find('image') != -1:
                        pass
                    else:
                        raise ValidationError('首页链接或者详情页链接不是图片类型')
                else:
                    raise ValidationError('链接无效')
            else:
                raise ValidationError('图片链接不能为空')
        else:
            pass
    #
    # @api.multi
    # def unlink(self):
    #     raise ValidationError('不能删除，因为有关联！')
    #     return super(VmcAdvertisement, self).unlink()