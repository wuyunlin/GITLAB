# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import UserError, ValidationError


class VmcOrder(models.Model):
    _name = 'vmc.order'
    _order = 'create_date desc, id desc'
    name = fields.Char(
        string='订单号'
    )

    amount_total = fields.Float(
        string='订单金额'
    )

    machine_id = fields.Many2one(
        comodel_name='vmc.machines',
        string='所属售货机',
        required=True
    )

    payment_method = fields.Char(
        string='支付方式'
    )

    payment_status = fields.Char(
        string='支付状态'
    )

    order_status = fields.Char(
        '订单状态'
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='商品',
    )

    donation_product_id = fields.Many2one(
        comodel_name='product.product',
        string='赠品',
    )

    product_name = fields.Char(
        string='商品名称'
    )

    temperature = fields.Char(
        string='温度'
    )

    weather = fields.Char(
        string='天气'
    )
    stack_number = fields.Char(
        string='料道号'
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        related='machine_id.company_id',
        readonly=True,
    )
    promotion_policy=fields.Many2one(
        comodel_name='vmc.promotion',
        string='促销政策',
    )
    promotion_rules=fields.Char(
        string='促销规则'
    )
    discounted_prices=fields.Float(
        string='优惠价格'
    )
    shipping_status=fields.Selection(
        string='出货状态',
        selection='_get_shipping_type'
    )

    @api.model
    def _get_shipping_type(self):
        return [
            ('true','出货成功'),
            ('false','出货失败')
        ]

    create_time = fields.Datetime(
        string="订单创建时间(售货机时间)",
        readonly=True
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcOrder, self).unlink()
