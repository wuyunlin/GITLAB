# -*- coding: utf-8 -*-
#  author = scofield.yu

import time
from odoo import api, fields, models,_
from odoo.http import request
from odoo.exceptions import UserError, ValidationError


class VmcSwapOrder(models.Model):
    _name = 'vmc.swap.order'
    _order = 'create_date desc, id desc'
    _rec_name = 'swap_order_id'


    country=fields.Char(compute='_compute_country',string='区县')

#todo
    #@api.multi
    @api.depends('vmc_machine_id.state_id', 'vmc_machine_id.city_id','vmc_machine_id.county')
    def _compute_country(self):
        for line in self:
            if not line.vmc_machine_id:
                self.country = ''
            else:
                country = '%s%s%s' % (line.vmc_machine_id.state_id and line.vmc_machine_id.state_id.name or '',
                                      line.vmc_machine_id.city_id and line.vmc_machine_id.city_id.name or '',
                                      line.vmc_machine_id.county and line.vmc_machine_id.county.name or '')
                line.country = country

    # @api.multi
    # def _compute(self):
    #     res = {}
    #     if not isinstance(self._columns, list):
    #         fields = [self._columns]
    #     for line in self:
    #         for field in fields:
    #             if field=='country':
    #                 if not line.vmc_machine_id:
    #                     res[line.id] =''
    #                 else:
    #                     country = '%s%s%s' % (line.vmc_machine_id.state_id and line.vmc_machine_id.state_id.name or '',
    #                                           line.vmc_machine_id.city_id and line.vmc_machine_id.city_id.name or '',
    #                                           line.vmc_machine_id.county and line.vmc_machine_id.county.name or '')
    #                 line.country = country
    #             elif field == 'vmc_swap_order_id':
    #                 if not line.vmc_machine_id:
    #                     vmc_machine_id = False
    #                 else:
    #                     sql = """select id from vmc_swap_order where vmc_machine_id=%s and state='confirm' """ % (line.vmc_machine_id.id)
    #                     request.cr.execute(sql)
    #                     fet = request.cr.fetchall()
    #                     if fet:
    #                         if len(fet) > 1:
    #                             country=False
    #                         vmc_swap_order = fet[0][0]
    #                     else:
    #                         vmc_swap_order=False
    #                 line.vmc_swap_order = vmc_swap_order


    #country=fields.Char(compute='_compute',string='区县')
    #country=fields.Char(string='区县')
    name = fields.Char(
        string=''
    )

    swap_order_id = fields.Char(
        string='换货单编号',
    )

    vmc_machine_id = fields.Many2one(
        'vmc.machines',
        string='售货机名称',
        required=True
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='公司',
        related='vmc_machine_id.company_id',
        readonly=True
    )



    detailed_address = fields.Char(
        related='vmc_machine_id.street2',
        string='详细地址',
    )

    placing_position = fields.Char(
        related='vmc_machine_id.street',
        string='摆放位置',
    )

    container = fields.One2many(
        comodel_name='vmc.swap.order.line',
        inverse_name='vmc_swap_id',
        string='售货机货道'
    )

    state = fields.Selection(
        string='状态',
        selection='_get_state',
        default='draft'
    )

    machine_sync = fields.Char(
        string='售货机同步状态'
    )

    @api.multi
    def unlink(self):
        raise ValidationError('不能删除，因为有关联！')
        return super(VmcSwapOrder, self).unlink()

    @api.model
    def _get_state(self):
        return [
            ('draft', '草稿'),
            ('confirm', '已确认'),
            ('swaped', '已换货'),
            ('cancel', '已取消'),
        ]

    @api.model
    def _default_get(self):
        swap_order_id=str(int(time.time()*1000))
        self.write({'swap_order_id':swap_order_id})

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})

    @api.multi
    def action_reset(self):
        self.write({'state': 'draft'})

    @api.multi
    def action_confirm(self):
        self.write({'state': 'confirm'})

    @api.model
    def create(self, vals):
        name = self.env['ir.sequence'].next_by_code('vmc.swap.order')
        vals.update({'swap_order_id': name})
        res = super(VmcSwapOrder, self).create(vals)
        return res

    @api.constrains('state')
    def _check_unique(self):
        state = self.state
        if state == 'confirm' and self.vmc_machine_id:
            self._cr.execute("""select count(*) from vmc_swap_order where state ='confirm'
                                 and vmc_machine_id=%s and id <>%s """, (self.vmc_machine_id.id,self.id or 0))
            count = self._cr.fetchone()
            if count[0] > 0:
                raise UserError(_('每台售货机的已确认状态的换货单只能存在一个！'))
        return True

#todo
   # @api.one
    @api.onchange('vmc_machine_id')
    def onchange_vmc_machine_id(self):
        self.container = False
        if self.vmc_machine_id:
            self.country = '%s%s%s' % (self.vmc_machine_id.state_id and self.vmc_machine_id.state_id.name or '',
                                   self.vmc_machine_id.city_id and self.vmc_machine_id.city_id.name or '',
                                   self.vmc_machine_id.county and self.vmc_machine_id.county.name  or '')
            #####更新仓库库存到stock
            machine_line_obj=self.env['vmc.machine.stacks']
            for line in self.vmc_machine_id.machine_stack:
                line.write({'stock':line.stock_qty})

            if self.vmc_machine_id.machine_stack:
                lines = []
                for stack in self.vmc_machine_id.machine_stack:
                    lines.append(
                        {
                           'vmc_machine_stack_id': stack.id,
                           'origin_product_id': stack.product_id.id,
                           'type': stack.product_id and 'change' or 'add'
                        }
                    )
                self.container = lines


class VmcSwapOrderLine(models.Model):
    _name = 'vmc.swap.order.line'

    @api.model
    def _get_type(self):
        return [
            ('add', '新增'),
            ('change', '换货'),
            ('remove', '移除'),
        ]
    
    vmc_swap_id = fields.Many2one(
        comodel_name='vmc.swap.order',
        string='换货单',
    )

    vmc_machine_stack_id = fields.Many2one(
        comodel_name='vmc.machine.stacks',
        string='料道号',
    )

    box_number = fields.Selection(
        string = '货柜',
        related = 'vmc_machine_stack_id.box_number',
        readonly = True
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='商品',
        related='vmc_machine_stack_id.product_id',
        readonly=True,
    )
    
    origin_product_id = fields.Many2one(
        comodel_name='product.product',
        string='原商品',
    )

    swap_product_id = fields.Many2one(
        comodel_name='product.product',
        string='换货商品',
    )

    stock = fields.Integer(
        string='库存',
        related='vmc_machine_stack_id.stock',
        readonly=True
    )
    type = fields.Selection(
        string='动作',
        selection='_get_type',
        default='change'
    )
