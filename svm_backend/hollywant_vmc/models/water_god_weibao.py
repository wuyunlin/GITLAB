# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.http import request
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class VmcMachine(models.Model):
    _name = 'water.god.repair'

    # vmc_code = fields.Char('资产编号', required=True)
    # box_qty = fields.Char('货柜数量')
    # stack_qty = fields.Integer('料道数量')
    # vmc_brand = fields.Char('售货机品牌')
    # factory_code = fields.Char('出厂编码')
    # coordinate = fields.Char('坐标')

    vmc_brand = fields.Char('售货机品牌')
    factory_code = fields.Char('出厂编码')

    electrolytic_status=fields.Char('电解液是否充足')
    electrolytic_surplus=fields.Char('电解液剩余')
    filter_elem_status=fields.Char('滤芯是否更换')

    ph_value=fields.Char('PH值')
    acc_value=fields.Char('ACC值')
    water_degree=fields.Char('水表度数')
    water_press=fields.Char('水压')
    net_status=fields.Char('网络状况')


class VmcMachineminotor(models.Model):
    _name = 'water.god.minotor'

    factory_code = fields.Char('设备编码')
    water_god_name=fields.Char('设备名称')
    water_god_number=fields.Char('机器号')
    water_god_company=fields.Char('运营商')
    net_status = fields.Char('网络状况')
    door_status=fields.Char('开关门')
    electrolytic_status = fields.Char('电解液')
    filter_number = fields.Char('滤芯')
    water_press = fields.Char('水压')


    ph_value = fields.Char('PH值')
    acc_value = fields.Char('ACC值')
    water_degree = fields.Char('水表读数')
    fix_version = fields.Char('固件版本')
    app_version = fields.Char('app版本')

class Vmc_repair_sign(models.Model):
    _name='water.god.repair.sign'

    sign_time=fields.Datetime('上报时间')
    sign_person=fields.Char('维护人')
    machines_name=fields.Char('设备名称')
    machines_number=fields.Char('机器号')
    repair_content=fields.Char('维护内容')
    repair_time=fields.Datetime('维护时间')




