# -*- coding: utf-8 -*-
from odoo import fields, models


class HollywantCompany(models.Model):
    _inherit = "res.company"

    payment_id = fields.Char('支付id')
    payment_secret = fields.Char('支付密钥')
    paysuccess_link = fields.Html()