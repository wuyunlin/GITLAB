#coding=utf-8
import logging
from odoo import models, api, fields
_logger = logging.getLogger(__name__)

class hollywant_res_users(models.Model):
    _inherit = "res.users"

    is_warehouse = fields.Boolean(u'仓管',compute = '_get_user_role', readonly=False)
    is_exchange = fields.Boolean(u'换货员',compute = '_get_user_role', readonly=False)
    is_account = fields.Boolean(u'财务',compute = '_get_user_role', readonly=False)
    is_purchase = fields.Boolean(u'采购',compute = '_get_user_role', readonly=False)
    is_admin = fields.Boolean(u'管理员',compute = '_get_user_role', readonly=False)

    @api.model
    def create(self, vals):
        if self.env.context and self.env.context.get('only_new_accounting'):
            # save groups
            group_vals = self.get_group_type(vals)
            if vals.get('mobile'):
                vals_mobile = {
                    "mobile": vals.get('mobile'),
                    "phone": vals.get('mobile')
                }
                vals.update(vals_mobile)

            vals.update({
                'password':vals.get('password') if vals.get('password') else '123456',
            })
            vals.update(group_vals)
        user_id = super(hollywant_res_users, self).create(vals)
        return user_id

    @api.one
    def write(self,vals , *args, **kwargs):
        if self.env.context and self.env.context.get('only_new_accounting') :
            # save groups
            group_vals = self.get_group_type(vals)
            vals.update(group_vals)
        user = super(hollywant_res_users, self).write(vals)

    @api.model
    def get_group_type(self,vals):
        """
        获取组的类型
        :param vals:
        :return:
        """
        return_vals = {}
        warehouse_id = self.get_warehouse_id()
        account_id = self.get_account_id()
        admin_id = self.get_admin_id()
        purchase_id = self.get_purchase_id()
        exchange_id = self.get_exchange_id()
        exchange_str = 'in_group_'+str(exchange_id)
        warehouse_str = 'in_group_'+str(warehouse_id)
        account_str = 'in_group_'+str(account_id)
        purchase_str = 'in_group_'+str(purchase_id)
        admin_str = 'in_group_'+str(admin_id)
        if vals.get('is_warehouse') == True:
            return_vals[warehouse_str] = True
        elif vals.get('is_warehouse') == False:
            return_vals[warehouse_str] = False

        if vals.get('is_account') == True:
            return_vals[account_str] = True
        elif vals.get('is_account') == False:
            return_vals[account_str] = False

        if vals.get('is_purchase') == True:
            return_vals[purchase_str] = True
        elif vals.get('is_purchase') == False:
            return_vals[purchase_str] = False

        if vals.get('is_admin') == True:
            return_vals[admin_str] = True
        elif vals.get('is_admin') == False:
            return_vals[admin_str] = False

        if vals.get('is_exchange') == True:
            return_vals[exchange_str] = True
        elif vals.get('is_exchange') == False:
            return_vals[exchange_str] = False


        return return_vals

    @api.model
    def get_warehouse_id(self):
        cr = self.env.cr
        cr.execute("""select id from res_groups where name='vending_warehouse' and category_id in(
                                    select id from ir_module_category where name ='vending') """)
        groups = cr.dictfetchall()
        saler_id = 0
        for result in groups:
            saler_id = result['id']
        return saler_id

    @api.model
    def get_exchange_id(self):
        cr = self.env.cr
        cr.execute("""select id from res_groups where name='vending_exchange' and category_id in(
                                    select id from ir_module_category where name ='vending') """)
        groups = cr.dictfetchall()
        saler_id = 0
        for result in groups:
            saler_id = result['id']
        return saler_id

    @api.model
    def get_account_id(self):
        cr = self.env.cr
        cr.execute("""select id from res_groups where name='vending_account' and category_id in(
                                        select id from ir_module_category where name ='vending') """)
        groups = cr.dictfetchall()
        saler_id = 0
        for result in groups:
            saler_id = result['id']
        return saler_id

    @api.model
    def get_purchase_id(self):
        cr = self.env.cr
        cr.execute("""select id from res_groups where name='vending_purchase' and category_id in(
                                        select id from ir_module_category where name ='vending') """)
        groups = cr.dictfetchall()
        saler_id = 0
        for result in groups:
            saler_id = result['id']
        return saler_id

    @api.model
    def get_admin_id(self):
        cr = self.env.cr
        cr.execute("""select id from res_groups where name='vending_admin' and category_id in(
                                        select id from ir_module_category where name ='vending') """)
        groups = cr.dictfetchall()
        saler_id = 0
        for result in groups:
            saler_id = result['id']
        return saler_id

    @api.one
    def _get_user_role(self, *args, **kwargs):
        """
        获取组类型
        @param args:
        @param kwargs:
        @return:
        """
        if self.id:
            groups_ids = self.groups_id.ids
            warehouse_id = self.get_warehouse_id()
            account_id = self.get_account_id()
            admin_id = self.get_admin_id()
            purchase_id = self.get_purchase_id()
            exchange_id = self.get_exchange_id()
            if warehouse_id in groups_ids:
                self.is_warehouse = True
            if account_id in groups_ids:
                self.is_account = True
            if purchase_id in groups_ids:
                self.is_purchase = True
            if admin_id in groups_ids:
                self.is_admin = True
            if exchange_id in groups_ids:
                self.is_exchange = True














