# -*- coding: utf-8 -*-
from odoo import api, fields, models,_

import time
import logging
_logger = logging.getLogger(__name__)


class machine_assert_management(models.Model):
    _name = "salemachine.assert.management"
    _rec_name = "assert_num"

    assert_num = fields.Char('资产编号', required=True,default='' )
    assert_manager = fields.Many2one('res.users', string='资产管理员', required=True)
    sale_machine_brand = fields.Char('售货机品牌')
    sale_machine_model = fields.Char('售货机型号')
    sale_machine_style = fields.Selection(string="售货机类型", selection="_machine_type", required=True)

    @api.model
    def _machine_type(self):
        return [
            ('combina_machine', '组合机'),
            ('drink_machine', '饮料机'),
            ('food_machine', '食品机'),
            ('convenient_cabinet', '便利柜')
        ]

    purchase_date=fields.Date('采购日期')
    warranty_date = fields.Date('质保日期')
    pay_way = fields.Selection(string="缴款方式", selection="_paypal_way")

    @api.model
    def _paypal_way(self):
        return [
            ('instal_credit', '分期'),
            ('rent', '租借'),
            ('single', '趸交')
        ]

    use_person= fields.Many2one('res.users', string="领用人")
    use_status = fields.Boolean('领用状态')
    key_user = fields.Many2one('res.users', string="钥匙")
    backup_key = fields.Many2one('res.users', string='备用钥匙')
    key_code = fields.Char('钥匙编号')
    company_id = fields.Many2one('res.company', string='所属公司')
    machine_waight = fields.Float('机器净重')
    machine_num = fields.Char('机器编号', copy=False,default='')
    firmware = fields.Char('固件版本')
    normal_atm_electric = fields.Float('常温耗电量')
    single_cool_power = fields.Float('单侧制冷耗电量')
    bilateral_side_cool = fields.Float('双侧制冷耗电量')
    cool_hot_power = fields.Float('制冷+加热耗电量')
    IPC_brand = fields.Char('工控机品牌')
    IPC_serial_num = fields.Char('工控机序列号')
    paper_currency_brand = fields.Char('纸币器品牌')
    paper_currency_num = fields.Char('纸币器序列号')
    coin_currency_brand = fields.Char('硬币器品牌')
    coin_currency_num = fields.Char('硬币器序列号')
    one_coin_brain = fields.Float('一元容量')
    helf_coin_brain = fields.Float('五角容量')
    screen_brand = fields.Char('显示器品牌')
    screen_measurement = fields.Float('显示器尺寸')
    screen_PN = fields.Char('屏P/N')
    screen_serial_num = fields.Char('显示器序列号')
    contain_num = fields.Integer('货柜数量')
    contain_style = fields.Selection(string='货柜类型', selection="_get_box_type")

    @api.model
    def _get_box_type(self):
        return [
            ('goods', '商品柜'),
            ('drinks', '饮料柜'),
            ('foods', '食品柜'),
        ]

    @api.onchange('use_person')
    def _onchange_use_person(self):
        if self.use_person:
            self.use_status = True
        else:
            self.use_status = False

    _sql_constraints = [
        ('assert_num_uniq', 'unique (assert_num)', '资产编号必须唯一！'),
        ('assert_num_uniq', 'unique (assert_num)', '资产编号必须唯一！'),
    ]
    # _defaults = {
    #     'assert_num': '',
    #     'machine_num': ''
    # }

    @api.model
    def create(self,vals,context = {}, *args, **kwargs):
        cr = self.env.cr
        vals['company_id'] = self.env.user.company_id.id
        machine_assert = super(machine_assert_management, self).create(vals)
        machine_capacity=machine_assert.one_coin_brain+machine_assert.helf_coin_brain
        if vals['use_status'] == True:
            #插入记录
            # sql = """insert into vmc_machines (vmc_code,cash_capacity,partner_id) VALUES (%s,%s,%s)"""%(machine_assert.assert_num,machine_capacity,machine_assert.partner_id)
            # cr.execute(sql)
            machine_vals={
                "name":machine_assert.assert_num,
                "vmc_code":machine_assert.assert_num,
                "cash_capacity":self.one_coin_brain+self.helf_coin_brain
                }
            vmc_machine=self.env['vmc.machines'].create(machine_vals)
        return machine_assert

    @api.multi
    def write(self, vals):
        cr = self.env.cr
        p = super(machine_assert_management, self).write(vals)

        try:
            use_status = vals['use_status']
        except:
            use_status = False
        if use_status == True:
            #先到数据库里判断有没有对应的资产编号
            # sql = """ select * from vmc_machines where vmc_code= %s""" %self.assert_num
            cr.execute(""" select * from vmc_machines where vmc_code= %s""",(self.assert_num,))
            vmc_machines_dict = cr.dictfetchall()
            if not vmc_machines_dict:
                # sql = """insert into vmc_machines (vmc_code,cash_capacity,partner_id) VALUES (%s,%s,%s)""" % (self.assert_num,self.one_coin_brain+self.helf_coin_brain,self.partner_id)
                # cr.execute(sql)
                machine_vals={
                "name":self.assert_num,
                "vmc_code":self.assert_num,
                "cash_capacity":self.one_coin_brain+self.helf_coin_brain
                }
                vmc_machine=self.env['vmc.machines'].create(machine_vals)
        return p
    @api.multi
    def onchange_purchase_date(self, purchase_date, warranty_date):
        if warranty_date:
            if warranty_date < purchase_date:
                result = {'value': {'purchase_date': warranty_date}}
                result['warning'] = {'title': _('Warning'), 'message': _('采购日期不应大于质保日期')}
                return result

        if purchase_date > time.strftime("%Y-%m-%d"):
            result = {'value': {'purchase_date': time.strftime("%Y-%m-%d")}}
            result['warning'] = {'title': _('Warning'),'message': _('采购日期应不晚于今天')}
            return result
    @api.multi
    def onchange_warranty_date(self, warranty_date,purchase_date):
        if purchase_date:
            if warranty_date < purchase_date:
                result = {'value': {'warranty_date': purchase_date}}
                result['warning'] = {'title': _('Warning'), 'message': _('质保日期应该大于采购日期')}
                return result
