# -*- coding: utf-8 -*-
from odoo import http
import time
from odoo.http import request
from product import AppProductApi


class VmcWarehouse(http.Controller):

    @http.route(
        ['/vmc/warehouse/getWarehouseProductsInfo'], type='json', auth="user"
    )
    def get_warehouse_products_info(self, offset=0, limit=False, sort=None):
        company_id = request.env['res.users'].browse(request.uid).company_id.id
        request.cr.execute(
            """
                select id,code,lot_stock_id,name from stock_warehouse
                where  stock_type = 'vmc' and company_id =%s
            """, (company_id,))
        records = []
        warehouse_result = request.cr.dictfetchall()
        class_AppProductApi = AppProductApi()
#         class_search_read_product = search_read_product()
        for warehouse in warehouse_result:
            record = {}
            record['stock_warehouse_id'] = warehouse['id']
            record['stock_warehouse_name'] = warehouse['name']
            request.cr.execute(
                """
                    select id,name,posx from stock_location
                    where usage='internal' and active=True
                    and return_location=False
                    and complete_name ilike %s
                """,
                ('物理位置 / ' + warehouse['code'] + '%',)
            )
            location_result = request.cr.dictfetchall()
            stock_locations = []
            for location in location_result:
                location_info = {}
                location_info['stock_location_id'] = location['id']
                location_info['stock_location_name'] = location['name']
                location_info['stock_location_posx'] = location['posx']
                request.cr.execute(
                    '''
                       SELECT product_id,sum(qty) as product_qty
                       FROM stock_quant
                       WHERE location_id = %s group by product_id
                    ''', (location['id'],))
                product_result = request.cr.dictfetchall()

                products = {}
                for product in product_result:
                    if product['product_qty'] <= 0:
                        continue
                    if 'product_qty' not in products:
                        products['product_qty'] = 0
                    uom = class_AppProductApi.get_product_saleuom_minuom_byid(
                        product['product_id'])
                    if product['product_qty'] * \
                            uom['factor'] >= products['product_qty']:
                        products['product_id'] = product['product_id']
                        product_product = \
                            request.env['product.product'].browse(
                                product['product_id'])
                        list_price = product_product.product_tmpl_id.list_price
                        products['product_price'] = '%0.2f' % (
                            list_price / uom['factor'])
                        products['product_qty'] = product[
                            'product_qty'] * uom['factor']
                        products[
                            'product_name'] = product_product.product_tmpl_id.name
                        if product_product.product_tmpl_id.image_small:
                            time_array = time.strptime(
                                product_product.product_tmpl_id.write_date,
                                "%Y-%m-%d %H:%M:%S"
                            )
                            updatetime = int(time.mktime(time_array))
                            products[
                                'image_small'] = request.httprequest.host_url
                            + "web/binary/image?model=product.template&id="
                            + str(product_product.product_tmpl_id.id)
                            + "&field=image_medium&updatetime="
                            + str(updatetime)
                        else:
                            products['image_small'] = ""
                location_info['products'] = products
                stock_locations.append(location_info)
            record['stock_locations'] = stock_locations
            records.append(record)
        return records
