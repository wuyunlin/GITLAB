# -*- coding: utf-8 -*-
from ..controllers.vmc import vmcController, predispatch
from odoo.http import request
import logging

_logger = logging.getLogger(__name__)


@predispatch
class VmcAdvertisement(vmcController):
    def vmc_ad_list(self, *args, **kwargs):
        _logger.debug("Begin vmc_ad_list ...")
        machine_id = kwargs['machine_id']
        records = []
        # 售货机对应的广告列表
        request.cr.execute(
            '''
                select url_id as list_id
                from vmc_machines_advertisement_list
                where machine = %s
            ''', (machine_id,)
        )
        r = request.cr.dictfetchone()
        if r is not None:
            list_id = r['list_id']
            request.cr.execute(
                '''
                    select ad.url as url, ad.url_detail, ad.type as type, line.play_sequence as play_sequence
                    from vmc_advertisement_list_line as line
                    left join vmc_advertisement as ad
                     on (ad.id = line.vmc_advertisement_id)
                    where line.vmc_advertisement_list_id = %s
                    order by line.play_sequence
                ''', (list_id,)
            )
            advertisements = request.cr.dictfetchall()
            for ad in advertisements:
                records.append(
                    {
                        'ad_url': ad['url'],
                        'ad_type': ad['type'],
                        'ad_order': ad['play_sequence'],
                        'ad_detail': ad['url_detail'] or ''
                    }
                )
            request.cr.execute(
                '''
                    select ad.url_head as url, ad.url_detail, ad.type as type, line.play_sequence as play_sequence
                    from vmc_advertisement_list_line_copy as line
                    left join vmc_advertisement as ad
                     on (ad.id = line.vmc_advertisement_id)
                    where line.vmc_advertisement_list_copy_id = %s
                    order by line.play_sequence
                ''', (list_id,)
            )
            advertisements_copy = request.cr.dictfetchall()
            for ad in advertisements_copy:
                records.append(
                    {
                        'ad_url': ad['url'],
                        'ad_type': ad['type'],
                        'ad_order': ad['play_sequence'],
                        'ad_detail': ad['url_detail'] or ''
                    }
                )

        _logger.debug("End vmc_ad_list ...\n")
        return {
            'records': records,
            'total': len(records),
        }
