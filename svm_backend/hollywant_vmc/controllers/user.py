# -*- coding: utf-8 -*-
import copy
from datetime import datetime, timedelta, tzinfo
import random
import time
import json
import logging
import os
import traceback

from odoo import http,SUPERUSER_ID
import odoo.tools.config as config


from odoo.addons.web.controllers.main import Session
from odoo.addons.web.controllers.main import DataSet
from odoo.http import request
from odoo.tools.translate import _
from common import localizeStrTime,image_url
from passlib.context import CryptContext
from common import is_landline,is_mobile
# from odoo.addons.app_api.helper.distance import getDistance
# from odoo.addons.app_api.helper.distance import getLocation
# from odoo.addons.app_api.helper.distance import Point
_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')

class User(Session):
    @http.route(['/app_api/session/authenticate'], type='json', auth="none")
    def api_authenticate(self, *args, **kwargs):
        # dbs = openerp.service.db.list_dbs(False)
        # if db not in dbs:
        #     return {
        #         "title": "用户初始化",
        #         "error": "数据库不存在！"
        #     }
        login = kwargs['login']
        password = kwargs['password']
        db = config["db_name"]
        request.session.authenticate(db, login, password)
        # if self.session_info().get("uid") is False:
        if request.uid is False:
            return {
                "title": "用户初始化",
                "error": "用户名或密码错误，请重新登录！"
            }
        # session_info = self.session_info()
        session_info = request.session
        # current_user = request.env['res.users'].browse(self.session_info().get("uid"))
        current_user = request.env['res.users'].browse(request.uid)
        session_info['user_name'] = current_user.partner_id.name
        session_info['tags']=[str(request.session.uid),"p"+str(current_user.partner_id.id)]
        session_info['session_id'] = request.session.sid
        request.cr.execute("""SELECT rg.name FROM res_groups rg LEFT JOIN ir_module_category imc ON imc.id = rg.category_id
                                                WHERE imc.name =%s AND rg.id IN  ( 
                                                SELECT gid FROM res_groups_users_rel WHERE uid =%s )
                                                order by case when rg.name like 'Admin' then 0 when rg.name like 'Dealer' then 1 else 2 end limit 1 """ ,
                                                ('Dealer',str(current_user.id)))
        groups =request.cr.dictfetchall()
        role =None
        for result in groups:
            role = result['name']
        session_info['role']=role
        return session_info

    #用户登录状态下修改密码
    @http.route('/app_api/session/change_password', type='json', auth="user")
    def change_password(self, *args, **kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        old_password = kwargs['old_password']
        new_password = kwargs['new_password']
        confirm_password= new_password   
        if not uid or request.session['login']=='public':
            return {
                "title": "用户没有登录",
                "error": "请先登录后再查看购物车"
            }   
        if not (old_password.strip() and new_password.strip() and confirm_password.strip()):
            return {'error':_('密码不能为空.'),'title': _('更改密码')}
        if new_password != confirm_password:
            return {'error': _('两次输入的密码不一致.'),'title': _('更改密码')}
        try:
            if request.env['res.users'].change_password(
                old_password, new_password):
                return {'msg': _('密码更改成功'),'title': _('更改密码')}
        except Exception:
            return {'error': _('原密码错误更改失败.'), 'title': _('更改密码')}
        return {'error': _('密码更改失败 !'), 'title': _('更改密码')}
    #用户找回密码
    @http.route('/app_api/session/find_password', type='json', auth="none")
    def find_password(self,login,newpassword,code):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        if not (login.strip() and newpassword.strip()):
            return {'error':_('账号密码不能为空.'),'title': _('找回密码')}
        verifyCode = request.session.VerifyCode    
        verifyMobile = request.session.VerifyMobile     
        if verifyCode == None or verifyCode !=code:
            return {'error':_('验证码不正确.'),'title': _('找回密码')}
        if verifyMobile == None or verifyMobile !=login:
            return {'error':_('验证码和手机不正确.'),'title': _('找回密码')}
        request.session['VerifyTime']=None#清空时间和code
        request.session['VerifyCode']=None
        try:
            #通过手机号login，找到对应的客户            
            userlist =pool['res.users'].search(cr,SUPERUSER_ID,[('login', '=', login)])            
            if userlist == None or len(userlist)!=1:
                return {'error':_('该账号不存在.'),'title': _('找回密码')}
            user_obj=request.env['res.users'].browse(userlist[0])
            crypt_password=CryptContext(['pbkdf2_sha512']).encrypt(newpassword)
            #user_obj.sudo().write({'password': newpassword})
            rest=request.cr.execute("""
            UPDATE res_users SET password_crypt=%s WHERE id=%s;
            """, (crypt_password,user_obj.id,))
            return {'msg':_('修改密码成功，请重新登录'),'title': _('找回密码')}
        except:
            logging.info(traceback.format_exc())
            return {'error': _('密码更改失败.'), 'title': _('找回密码')}        
    @http.route('/app_api/session/destroy', type='json', auth="user")
    def destroy(self):
        request.session.logout()
        return {'msg': _('注销成功 !'), 'title': _('注销成功')}
     #获取手机上面banner的管理,可以根据code获取配置列表  position：类型，banner:banner、促销:promotion、banner和促销:share、公告：notice     
    @http.route(['/app_api/user/banner'], type='json', auth="none")
    def user_banner_list(self,code,position="banner",offset=0,limit=0):
        if not isinstance(offset,int):
            offset =0
        if not isinstance(limit,int):
            limit =0
        if limit <0:
            limit = 0
        if offset <0:
            offset =0 
        if position == None:
            position='banner'    
        if position =='information' :#如果是消息列表，那么information代表notice和promotion 并且是可见的
            if limit ==0:
                request.cr.execute("""
                select a.id app_id,a.name as app_name,b.*  from mobile_app a,mobile_banner b where b.app_id=a.id and b.active=TRUE and a.active=TRUE and a.code=%s and b.display_position in ('promotion','notice') and b.isvisible=True order by b.isvisible desc,b.banner_seq asc;
                """, (code,))
            else:   
                 request.cr.execute("""
                select a.id app_id,a.name as app_name,b.*  from mobile_app a,mobile_banner b where b.app_id=a.id and b.active=TRUE and a.active=TRUE and a.code=%s and b.display_position in ('promotion','notice') and b.isvisible=True order by b.isvisible desc,b.banner_seq asc  limit %s offset  %s;
                """, (code,limit,offset,))   
        else:
            if limit ==0:
                request.cr.execute("""
                select a.id app_id,a.name as app_name,b.*  from mobile_app a,mobile_banner b where b.app_id=a.id and b.active=TRUE and a.active=TRUE and a.code=%s and b.display_position in(%s,'share') order by b.banner_seq asc;
                """, (code,position)) 
            else:
                request.cr.execute("""
                select a.id app_id,a.name as app_name,b.*  from mobile_app a,mobile_banner b where b.app_id=a.id and b.active=TRUE and a.active=TRUE and a.code=%s and b.display_position in(%s,'share') order by b.banner_seq asc  limit %s offset  %s;
                """, (code,position,limit,offset)) 
        result=request.cr.dictfetchall()
        res = {
            "msg": position+" list",
        }     
        banner_list=[]
        for banner in result or []:
            updatetime=time.mktime(datetime.strptime(banner.get('write_date'),"%Y-%m-%d %H:%M:%S.%f").timetuple())
            create_date=localizeStrTime(request,banner.get('create_date'),"%Y-%m-%d %H:%M:%S.%f","%Y-%m-%d %H:%M:%S")
            image_small=""     
            if banner["image"]:
                image_small=request.httprequest.host_url + "web/binary/image?model=mobile.banner&id=" + str(banner.get('id')) +"&field=image&updatetime="+str(updatetime)
             
            banner_detail={
                            'name':banner.get('name'),
                            'create_date':create_date,
                            'action_type':banner.get('action_type'),
                            'action_value':banner.get('action_value') or "", 
                            'image_small':image_small,
                            'content':banner.get('comment') or "",
                            'type':banner.get('display_position')
                             }
            banner_list.append(banner_detail)
        res['records']=banner_list
        return res
    #获取品牌列表       
    @http.route(['/app_api/user/brand'], type='json', auth="user")
    def user_brand_list(self):     
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        current_user = request.env['res.users'].browse(request.uid)  
        request.cr.execute("""
        select *  from brand_brand where active=True and id in (select distinct(product_brand_id) from product_template where product_brand_id is not null and company_id=%s) order by brand_seq asc;
        """, (current_user.company_id.id,))    
        result=request.cr.dictfetchall()
        res = {
            "msg": "brand list",
        }     
        if len(result) < 4:
            request.cr.execute("""
                select *  from brand_brand where active=True and fixed =True order by brand_seq asc;
            """)       
            result_list=request.cr.dictfetchall()
            is_exist = False
            for brand_fix in result_list or []:
                is_exist = False
                for result_temp in result:
                    if result_temp.get('id') ==brand_fix.get('id'):
                        is_exist=True
                        break
                if is_exist == False:
                    result.append(brand_fix)
                if len(result) == 4:#补全到4个
                    break
                
        brand_list=[]
        for brand in result or []:           
            updatetime=time.mktime(datetime.strptime(brand.get('write_date'),"%Y-%m-%d %H:%M:%S.%f").timetuple()) 
            image_small=""     
            if brand["image"]:
                image_small=request.httprequest.host_url + "web/binary/image?model=brand.brand&id=" + str(brand.get('id')) +"&field=image&updatetime="+str(updatetime)     
            brand_detail={
                            'brand_id':brand.get('id'),
                            #'updatetime':updatetime,
                            'name':brand.get('name') or "",
                            'comment':brand.get('comment') or "",                            
                            'image_small':image_small 
                             }
            brand_list.append(brand_detail)
        res['records']=brand_list
        return res
    #获取品牌列表
    @http.route(['/app_api/user/feedback'], type='json', auth="user")
    def user_feedback_create(self,**kwargs):
        feedback_env = request.env['feedback.feedback']
        current_user = request.env['res.users'].browse(request.uid)
        args = {}
        args['name'] = current_user.partner_id.name
        if kwargs.has_key('phone'):
            args['phone'] = kwargs['phone'] or ""
        if kwargs.has_key('content'):
            args['content'] = kwargs['content'] or ""
        if kwargs.has_key('image1'):
            args['image1'] = kwargs['image1']
        if kwargs.has_key('image2'):
            args['image2'] = kwargs['image2']
        if kwargs.has_key('image3'):
            args['image3'] = kwargs['image3']
        feedback = feedback_env.sudo().create(args)

        return {

                "msg": "感谢您的反馈意见!",

            }

    
class User_Manager(DataSet):
    #业代新增终端客户，包括对应的地址信息       
    @http.route(['/app_api/user/create'], type='json', auth="user")
    def create_teminal_user(self,**kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        cus_id=kwargs["cus_id"]
        cus_phone=kwargs["cus_phone"]
        cus_telphone=None
        if is_mobile(cus_phone) ==False:
            return {'error':_('请输入正确的手机号.')}
        if kwargs.has_key("cus_telphone"):
            cus_telphone=kwargs["cus_telphone"]
        if cus_telphone !=None and cus_telphone !="":
            if len(cus_telphone) <=0 or len(cus_telphone)>=12:
                return {'error':_('请输入正确的固话号.')}
        
        current_user = request.env['res.users'].browse(request.uid)
        #客户新增流程,判断依据是 cus_id
        if cus_id == 0 or cus_id is None:
            userlist =pool['res.users'].search(cr,SUPERUSER_ID,[('login', '=', cus_phone)]) 
            #dealer_id = pool['res.groups'].search(cr,uid,[['name','=','Dealer']],0)
            terminal_group_id = pool['res.groups'].search(cr,uid,[['name','=','Terminal']],0)
            #request.cr.execute("""select id from res_groups where name='User' and category_id in(
            #                        select id from ir_module_category where name ='HollyWant Dealer') """ )
            #groups =request.cr.dictfetchall()
            #role = 0
            #for result in groups:
             #   role = result['id']
            if userlist != None and len(userlist)>=1:
                return {'error':_('该手机号已存在')}
            args={
                  "tz": "Asia/Shanghai",
                   "alias_contact": "everyone",
                  "notify_email": "none",
                  "password":"123456",
                  "user_id":request.uid,
                  "customer":True,
                  "active": True,
                  "login":cus_phone,
                  "name":kwargs["cus_name"],
                  "mobile":cus_phone,
                  "phone":cus_telphone,
                  "company_id": current_user.company_id.id or 1,
                  "company_ids": [[6,False,[current_user.company_id.id or 1]]],
                  #"sel_groups_"+str(role):role,
                  "sel_groups_"+str(terminal_group_id[0]):terminal_group_id[0],
                  }
            address_list =kwargs["address_list"]
            if address_list != None and len(address_list) >=1:
                address_temp=address_list[0]
                args["country_id"]=address_temp["country_id"]
                args["state_id"]=address_temp["state_id"]
                args["state_name"]=address_temp["state_name"]
                args["city_id"]=address_temp["city_id"]
                args["city"]=address_temp["city"]
                args["county"]=address_temp["county"]
                args["county_name"]=address_temp["county_name"]
                args["street"]=address_temp["address"]
                location=getLocation(address_temp["state_name"]+address_temp["city"]+address_temp["county_name"]+address_temp["address"])
                if location != None:
                    args["location"]=location
                    
               
            user_env = request.env['res.users']
            create_user=user_env.create(args)
            address_ids=[]
            address_ids.append(create_user.partner_id.id)
            res={
                 "uid":create_user.id
                 }
            #如果创建成功，还需要继续处理 大于一个地址的情况
            if address_list != None and len(address_list) >1:
                index =0;
                for address in address_list:
                    if index == 0:
                        index=index+1
                        continue 
                    addrss_args={
                                  "tz": "Asia/Shanghai",
                                  "type":"delivery",
                                  "is_default":False,
                                  "use_parent_address":False,
                                  "parent_id":create_user.partner_id.id,
                                  "street":address["address"],  
                                  "notify_email": "none",
                                  #"user_id":request.uid,
                                  "customer":True,
                                  "active": True,
                                  "name":kwargs["cus_name"],
                                  "mobile":cus_phone,
                                  "phone":cus_telphone,
                                  "company_id": current_user.company_id.id or 1,
                                  "country_id":address["country_id"],
                                  "state_id":address["state_id"],
                                  "state_name":address["state_name"],                                  
                                  "city_id":address["city_id"],
                                  "city":address["city"],
                                  "county":address["county"],
                                  "county_name":address["county_name"],
                                  "street":address["address"],                                  
                                 }
                    location=getLocation(address["state_name"]+address["city"]+address["county_name"]+address["address"])
                    if location != None:
                        addrss_args["location"]=location
                    crate_partner=request.env['res.partner'].create(addrss_args)
                    address_ids.append(crate_partner.id)         
            res["address_ids"]=address_ids 
            return res
        #客户修改流程
        else:
            return {'error':_('请调用更新接口.')}
    #业代修改终端客户，包括对应的地址信息       
    @http.route(['/app_api/user/update'], type='json', auth="user")
    def update_teminal_user(self,**kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        current_user = request.env['res.users'].browse(request.uid)
        cus_id=kwargs["cus_id"]
        cus_phone=kwargs["cus_phone"]
        cus_telphone=None
        if is_mobile(cus_phone) ==False:
            return {'error':_('请输入正确的手机号.')}
        if kwargs.has_key("cus_telphone"):
            cus_telphone=kwargs["cus_telphone"]
        if cus_telphone !=None and cus_telphone !="":
            if len(cus_telphone) <=0 or len(cus_telphone)>=12:
                return {'error':_('请输入正确的固话号.')}
        #客户新增流程,判断依据是 cus_id
        if cus_id == 0 or cus_id is None:
            return {'error':_('请指定终端的ID接口.')}
        #客户修改流程
        else:
            userlist =pool['res.users'].search(cr,SUPERUSER_ID,[('login', '=', cus_phone),('id', '!=', cus_id)]) 
            if userlist != None and len(userlist)>=1:
                return {'error':_('该手机号客户已经被注册.')}
            update_teminal_user = request.env['res.users'].browse(cus_id)
            if update_teminal_user==None :
                return {'error':_('该终端用户不存在.')}
            #if request.uid !=1 and update_teminal_user.partner_id.user_id.id !=request.uid:
                #return {'error':_('该终端用户不属于当前业代.')}
            address_ids=[]
            res={
                 "uid":cus_id
                 }
            current_user = request.env['res.users'].browse(request.uid)
            update_user_args={
                            "name": kwargs["cus_name"],
                            #"phone":cus_phone,
                            "mobile":cus_phone,
                            "login":cus_phone,
                        }
                        
            update_teminal_user.write(update_user_args)
            
            address_list =kwargs["address_list"]
            if address_list != None and len(address_list) >=1:
                for address in address_list:
                    #如果id存在则更新，否则就新增
                    if address['address_id'] !=None and address['address_id'] != 0:
                        update_teminal_partner=request.env['res.partner'].browse(address['address_id'])
                        #if request.uid !=1 and update_teminal_partner.user_id.id !=request.uid:
                           # return {'error':_('该地址id'+str(address['address_id'])+'不属于当前业代.')}
                        address_ids.append(address['address_id'])
                        if update_teminal_partner.state_id ==address["state_id"] and update_teminal_partner.city_id==address["city_id"] and update_teminal_partner.county==address["county"] and update_teminal_partner.street==address["address"] and update_teminal_partner.phone==address["phone"]:
                            continue                        
                        update_partner_args={
                                                "street":address["address"],
                                                #"country_id":address["country_id"],
                                                #"country_name":address["country_name"],
                                                "state_id":address["state_id"],
                                                "state_name":address["state_name"],
                                                "city_id":address["city_id"],
                                                "city":address["city"],
                                                "county":address["county"],
                                                "county_name":address["county_name"],
                                                "street":address["address"],
                                                "active": address["active"],
                                                "phone":cus_telphone,
                                            }
                        location=getLocation(address["state_name"]+address["city"]+address["county_name"]+address["address"])
                        if location != None:
                            update_partner_args["location"]=location                    
                        update_teminal_partner.write(update_partner_args)
                        
                    else:
                        addrss_args={
                                  "tz": "Asia/Shanghai",
                                  "type":"delivery",
                                  "is_default":False,
                                  "use_parent_address":False,
                                  "parent_id":update_teminal_user.partner_id.id,
                                  "street":address["address"],  
                                  "notify_email": "none",
                                  #"user_id":request.uid,
                                  "customer":True,
                                  "active": address["active"],
                                  "name":kwargs["cus_name"],
                                  "mobile":cus_phone,
                                  "phone":cus_telphone,
                                  "company_id": current_user.company_id.id or 1,
                                  "country_id":address["country_id"],
                                  "state_id":address["state_id"],
                                  "state_name":address["state_name"],
                                  "city_id":address["city_id"],
                                  "city":address["city"],
                                  "county":address["county"],
                                  "county_name":address["county_name"],
                                  "street":address["address"],
                                 }
                        location=getLocation(address["state_name"]+address["city"]+address["county_name"]+address["address"])
                        if location != None:
                            addrss_args["location"]=location
                        crate_partner=request.env['res.partner'].create(addrss_args)
                        address_ids.append(crate_partner.id)
           
            res["address_ids"]=address_ids 
            return res
        
    #业代的送货单和收款单以及退货单       
    @http.route(['/app_api/user/mycount'], type='json', auth="user")    
    def dealer_count(self,**kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        delivery_count = 0 #配送单
        payment_count = 0  #支付单
        return_order_count = 0 #退货单
        isSaler = request.env['res.users'].isSaler(request)
        current_user = request.env['res.users'].browse(request.uid)
        if isSaler == True:
            request.cr.execute("""select count(sp.*) as num from stock_picking sp
                                     inner join sale_order so on sp.origin = so.name
                                      where  sp.name ilike (%s)   
                                        and so.create_uid =%s and so.company_id =%s
                                            and so.state in ('sale') """,
                ('%OUT%',uid,current_user.company_id.id or 1,))
            
#             request.cr.execute("""select count(*) as num from sale_order s left join stock_picking p on s.name=p.origin  where p.state in('done')   and p.name ilike (%s)   
#                 and s.user_id =%s
#                 and s.company_id =%s
#                 """,
#                 ('%OUT%',uid,current_user.company_id.id or 1,))
            delivery_counts =request.cr.dictfetchall()
            for num in delivery_counts:
                delivery_count=num['num']
            request.cr.execute("""select sum(invoice_open_count) as num from sale_order where state in ('sale', 'done')
                and user_id = %s
                and company_id = %s""",
                (uid, current_user.company_id.id or 1))
            payment_counts =request.cr.dictfetchall()
            for num in payment_counts:
                payment_count=num['num']
            request.cr.execute("""select count(*) as num from return_order where state in('sent') 
                and create_uid = %s
                and company_id = %s""",
                (uid, current_user.company_id.id or 1))
            return_order_counts =request.cr.dictfetchall()
            for num in return_order_counts:
                return_order_count=num['num']
        else:
            request.cr.execute("""select count(sp.*) as num from stock_picking sp
                                     inner join sale_order so on sp.origin = so.name
                                      where  sp.name ilike (%s)   
                                        and so.create_uid =%s 
                                            and so.state in ('sale')   
                """,
                ('%OUT%',uid,))
            
#             request.cr.execute("""select count(*) as num from stock_picking  where state in('done')    and name ilike (%s) 
#                 and partner_id =%s""",
#                 ('%OUT%',current_user.partner_id.id,))
            delivery_counts =request.cr.dictfetchall()
            for num in delivery_counts:
                delivery_count=num['num']
            request.cr.execute("""select sum(invoice_open_count) as num from sale_order where state in ('sale', 'done') 
                and partner_id = %s""",
                (current_user.partner_id.id,))
            payment_counts =request.cr.dictfetchall()
            for num in payment_counts:
                payment_count=num['num']
            request.cr.execute("""select count(*) as num from return_order where state in('sent') 
                and partner_id =%s""",
                (current_user.partner_id.id,))
            return_order_counts =request.cr.dictfetchall()
            for num in return_order_counts:
                return_order_count=num['num']
        res={
            "delivery_count":delivery_count or 0,
            "payment_count":payment_count or 0,
            "return_order_count":return_order_count or 0
             }
        return res
    #业代或者终端获取附近的商店      
    @http.route(['/app_api/user/location_stores'], type='json', auth="user")    
    def location_stores(self,**kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry   
        #values = request.env['res.partner'].search([('user_id','=',request.uid),('active','=',True),('location','!=',None)]).read(["id","name","location"])
            
        app_location=kwargs["app_location"]
        app_locations=app_location.split(',')
        stores_list=[]
        if len(app_locations) !=2:
            return {"error":"位置信息错误"}
        current_user = request.env['res.users'].browse(request.uid)
        request.cr.execute("""select p.id,p.name,p.location,p.mobile,p.state_name,p.city,p.county_name,p.street,u."id" as uid,p.parent_id  from res_partner p left join res_users u on p.id=u.partner_id where p.active=True and p.customer=True and p.location is not null 
                                and  p.company_id =%s """,
                (current_user.company_id.id or 1,))
        values =request.cr.dictfetchall()        
        for value in values:
            location=value["location"]
            locations=location.split(',')
            if len(locations) !=2:
                continue
            else:
                p1=Point()
                p1.lng = float(locations[0] ) #经度
                p1.lat = float(locations[1] )
                p2=Point()
                p2.lng = float(app_locations[0])  #经度
                p2.lat = float(app_locations[1] )
                distance =getDistance(p1,p2)
                if distance<2000:#目前定义1000米内叫附近
                    store={
                           "id":value["id"],#用户地址id
                           "name":value["name"],
                           "distance":distance,#按照这个字段进行排序
                           "state_name":value["state_name"],
                           "city":value["city"],
                           "county_name":value["county_name"],
                           "street":value["street"],
                           "mobile":value["mobile"],
                           "uid":value["uid"] #用户的id
                           }
                    if value["uid"] ==None and value["parent_id"] !=None:
                        user_id=-1      
                        request.cr.execute("""select *  from res_users u where partner_id=%s """,
                                (value["parent_id"],))
                        user_values =request.cr.dictfetchall()  
                        for user_value in user_values:
                            user_id=user_value["id"]
                        if user_id == -1:
                            continue
                        store["uid"]=user_id
                    elif value["uid"] ==None and value["parent_id"] ==None:
                        continue
                    stores_list.append(store)
        return {
                "stores_list":sorted(stores_list, key=lambda store: store["distance"])}

    #根据名字或者电话搜索当前业代的终端列表以及未付款总额
    @http.route(['/app_api/user/terminalunpaidsearch'], type='json', auth="user")
    def search_saler_terminal(self,**kwargs):
        #saler_user_id = request.uid
        current_user = request.env['res.users'].browse(request.uid)
        company_id = current_user.company_id.id
        if kwargs.has_key('limit'):
            limit = kwargs['limit']
        else:
            limit = 10
        if kwargs.has_key('offset'):
            offset = kwargs['offset']
        else:
            offset = 0
        if kwargs.has_key('keyword'):
            request.cr.execute("""select ru.id,rp.id as partner_id,name,phone,mobile from res_partner as rp,res_users as ru where rp.customer = TRUE and ru.active=True and rp.id = ru.partner_id and rp.company_id=%s and (rp.name like %s or rp.mobile like %s) order by name asc  limit %s offset  %s""",
            (company_id,'%'+kwargs['keyword'].strip()+'%','%'+kwargs['keyword'].strip()+'%',limit,offset))
        else:
            request.cr.execute("""select ru.id,rp.id as partner_id,name,phone,mobile from res_partner as rp,res_users as ru where rp.customer = TRUE and ru.active=True and rp.id = ru.partner_id and rp.company_id=%s  order by name asc  limit %s offset  %s""",
            (company_id,limit,offset))
        terminals =request.cr.dictfetchall()

        stores_list = []
        for store in terminals:
            store={
                           "user_id":store["id"],#用户id
                           "partner_id":store["partner_id"],#用户id
                           "name":store["name"],
                           "payment_unpay_count":self.get_user_payment_orders(store["partner_id"]),#未支付金额

                           }
            stores_list.append(store)
        return {"records":stores_list}
    #业代或者终端获取附近的商店,同时获取该店的付款情况      
    @http.route(['/app_api/user/location_stores_payment'], type='json', auth="user")    
    def location_stores_payment(self,**kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry   
        #values = request.env['res.partner'].search([('user_id','=',request.uid),('active','=',True),('location','!=',None)]).read(["id","name","location"])
            
        app_location=kwargs["app_location"]
        app_locations=app_location.split(',')
        stores_list=[]
        if len(app_locations) !=2:
            return {"error":"位置信息错误"}
        current_user = request.env['res.users'].browse(request.uid)
        request.cr.execute("""select p.id,p.name,p.location,p.mobile,p.state_name,p.city,p.county_name,p.street,u."id" as uid,p.parent_id  from res_partner p , res_users u where u.active and p.id=u.partner_id and p.location is not null 
                                and p.customer = TRUE and  p.company_id =%s """,
                (current_user.company_id.id or 1,))
        values =request.cr.dictfetchall()        
        for value in values:
            location=value["location"]
            locations=location.split(',')
            if len(locations) !=2:
                continue
            else:
                p1=Point()
                p1.lng = float(locations[0] ) #经度
                p1.lat = float(locations[1] )
                p2=Point()
                p2.lng = float(app_locations[0])  #经度
                p2.lat = float(app_locations[1] )
                distance =getDistance(p1,p2)
                if distance<5000:#目前定义5000附近的店铺
                    store={
                           "user_id":value["uid"],#用户的id
                           "partner_id":value["id"],#用户地址id
                           "name":value["name"],
                           "distance":distance,#按照这个字段进行排序
                           #"state_name":value["state_name"],
                           #"city":value["city"],
                           #"county_name":value["county_name"],
                           #"street":value["street"],
                           #"mobile":value["mobile"],
                           "payment_unpay_count":'0',#未支付金额
                           
                           }                    
                    payment_unpay_count=self.get_user_payment_orders(value["id"])
                    store['payment_unpay_count']=payment_unpay_count
                    stores_list.append(store)
        return {
                "records":sorted(stores_list, key=lambda store: store["distance"])}
    def get_user_payment_orders(self,partner_id):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        return_value=0.0
        request.cr.execute("""select sum(amount_total_company_signed) as amount from account_invoice where state='open' and partner_id=%s """ ,
            (partner_id,))
        values =request.cr.dictfetchall() 
        for v in values:
            if v['amount'] !=None:
                return_value= "%.2f" %v['amount']
        return str(return_value)
    def get_user_payment_orders1(self,partner_id):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        order_ids=request.env['sale.order'].search([('partner_id','=',partner_id),('invoice_status','=','invoiced'),('invoice_open_count','>',0),('state','in',('sale','done'))]).read(['id'])
        return_value='0'
        if order_ids ==None or len(order_ids)== 0:
            return return_value
        #context['active_model'] = 'account.invoice'
        #context['active_ids'] = []
        orderids=[]
        for order_id in order_ids:
            orderids.append(order_id['id'])        
        #for order in request.env['sale.order'].browse(orderids):
           #invoice_list=request.env['account.invoice'].search([('state','=','open'),('id','in',order.invoice_ids.ids)])
             #for invoice_obj in invoice_list:                
          #      context['active_ids'].append(invoice_obj['id'])
        #fields = ["amount"]
        #payment_info = request.env['account.register.payments'].with_context(**context).default_get(fields)
        invoice_ids=[]
        for order in request.env['sale.order'].browse(orderids):
            invoice_ids.extend(order.invoice_ids.ids)
        invoice_str_ids=[]
        for invoice_strid in invoice_ids:
            invoice_str_ids.append(str(invoice_strid))
        str_invoice=','.join(invoice_str_ids)
        request.cr.execute("""select sum(amount_total_company_signed) as amount from account_invoice where state='open' and id in ("""+str_invoice+')' ,
            ())
        values =request.cr.dictfetchall() 
        for v in values:
            return_value =v['amount']
        return str(return_value)
class User_Adress(http.Controller):
    def get_standart_addr(self, partner):
        addr = {
            "id": partner.id or 0,
            "name": partner.name or "",
            "phone": partner.phone or "",
            "mobile": partner.mobile or "",
            "country_id": partner.country_id.id or 0,
            "country_name": partner.country_id.name or "",
            "state_id": partner.state_id.id or 0,
            "state_name": partner.state_id.name or "",
            "city": partner.city_id.id or 0,
            "city_name": partner.city or "",
            "county": partner.county.id or 0,
            "county_name": partner.county.name or "",
            "street": partner.street or "",
            "type": partner.type or "",
            "zip": partner.zip or "",
            "display_name": partner.display_name or "",
            "active": partner.active or False,
            "is_default": partner.is_default or False,
        }
        return addr
    
    @http.route(['/app_api/user/list_address'], type='json', auth="user")
    def api_list_address(self,is_default=None):
        addr_list = []
        current_user = request.env['res.users'].browse(request.uid)
        partner = current_user.partner_id
        isSaler = request.env['res.users'].isSaler(request)
        if isSaler:
            addr_list.append(self.get_standart_addr(partner))
        elif is_default:
            partner_list = []
            partner_list += partner.child_ids.ids
            partner_list.append(partner.id)
            for _id in partner_list:
                _part = request.env['res.partner'].browse(_id)
                if _part.is_default:
                    addr_list.append(self.get_standart_addr(_part))
                    break
        else:
            addr_list.append(self.get_standart_addr(partner))
            child_ids = request.env['res.partner'].search([('parent_id', '=', partner.id)])
            for _part in child_ids:
                addr_list.append(self.get_standart_addr(_part))
        # if is_default:
        #     values = request.env['res.partner'].search(['|',('id', '=', partner.id),('parent_id', '=', partner.id),('is_default','=',True),('active','=',True),('address_flag','=',True)]).read(['id','active','type','name','phone','mobile','country_id','state_id','city_id','street','zip','email','is_default','county'])
        # else:
        #     values = request.env['res.partner'].search(['|',('id', '=', partner.id),('parent_id', '=', partner.id),('active','=',True),('address_flag','=',True)],0,100,"is_default desc").read(['id','active','type','name','phone','mobile','country_id','state_id','city_id','street','zip','email','is_default','county'])
        return {
                "records":addr_list
                }

    #防止app端不按int类型传id
    def check_addr_ids(self, addr):
        id_list = ["id", "company_id", "parent_id", "country_id", "state_id", "city", "county"]
        for _id in id_list:
            if addr.get(_id):
                addr[_id] = int(addr[_id])
        return addr

    @http.route(['/app_api/user/update_address'], type='json', auth="user")
    def api_update_address(self, args, method=None):
        partner_env = request.env['res.partner']
        current_user = request.env['res.users'].browse(request.uid)
        partner = current_user.partner_id
        record = []
        for fields in args:
            fields = self.check_addr_ids(fields)
            addr_id = fields.get("id")
            method = "update" if partner_env.browse(addr_id) else "add"
            if method:
                #保证国家省市区名称与id统一
                if fields.get('country_id'):
                    fields['country_name'] = request.env['res.country'].browse(fields['country_id']).name
                if fields.get('state_id'):
                    fields['state_name'] = request.env['res.country.state'].browse(fields['state_id']).name
                if fields.get('city'):
                    fields['city_id'] = fields['city']
                    city = request.env['res.country.state.city'].browse(fields['city_id'])
                    fields['city'] = city['name']
                if fields.get('county'):
                    fields['county_name'] = request.env['res.country.state.city.county'].browse(fields['county']).name

                if method == "add":
                    del fields['id']
                    fields['type'] = 'delivery'
                    fields['address_flag'] = True
                    fields['company_id'] = current_user.company_id.id
                    fields['company_type'] = "person"
                    fields['parent_id'] = current_user.partner_id.id
                    fields['supplier'] = False
                    fields['is_company'] = False
                    fields['customer'] = False
                    fields['employee'] = False
                    fields['write_uid'] = request.uid
                    fields['active'] = True
                    # if fields.get('city'):
                    #     fields['city_id'] = fields['city']
                    #     city = request.env['res.country.state.city'].browse(fields['city_id'])
                    #     fields['city'] = city['name']
                    if fields.get('is_default'):
                        values = partner_env.search(['|',('id', '=', partner.id),('parent_id', '=', partner.id),('is_default','=',True),('active','=',True),('address_flag','=',True)]).read(['id','is_default'])
                        for value in values:
                            value['is_default'] = False
                            partner_env.browse(value['id']).write(value)
                    #获取地址坐标
                    location=getLocation(fields["state_name"]+fields["city"]+fields["county_name"]+fields["street"])
                    if location != None:
                        fields["location"]=location
                    address = partner_env.create(fields)
                if method == "update":
                    if not fields.get('active'):
                        curr_partner = partner_env.browse(fields['id'])
                        if curr_partner.is_default:
                            values = partner_env.search(['|',('id', '=', partner.id),('parent_id', '=', partner.id),('is_default','=',False),('active','=',True),('address_flag','=',True)]).read(['id','is_default'])
                            if len(values) > 0:
                                values[0]['is_default'] = True
                                partner_env.browse(values[0]['id']).write(values[0])
                            fields['is_default'] = False
                            location=getLocation(fields["state_name"]+fields["city"]+fields["county_name"]+fields["street"])
                            if location != None:
                                fields["location"]=location
                            address = partner_env.browse(fields['id']).write(fields)
                            break
                            # return {
                            #         'msg': 'success'
                            #         }
                    if fields.get('is_default') and fields['is_default']:
                        values = partner_env.search(['|',('id', '=', partner.id),('parent_id', '=', partner.id),('is_default','=',True),('active','=',True),('address_flag','=',True)]).read(['id','is_default'])
                        for value in values:
                            value['is_default'] = False
                            partner_env.browse(value['id']).write(value)
                    # if fields.get('city'):
                    #     fields['city_id'] = fields['city']
                    #     city = request.env['res.country.state.city'].browse(fields['city_id'])
                    #     fields['city'] = city['name']

                    address = partner_env.browse(fields['id']).write(fields)
        return {
            'msg': 'success',
            }

    @http.route(['/app_api/user/get_address_version'], type='json', auth="user")
    def api_address_json_version(self):
        sys_config_env = request.env['sys.config']
        sys_configs = sys_config_env.search([['group_flag','=','file_json'],['code','=','address_list_json']]).read(['id','code_value','comment'])
        result = {}
        if len(sys_configs) > 0:
            return {'version':sys_configs[0]['code_value'],'url':sys_configs[0]['comment'] or ''}
        else:
            return {
                "title": "获取地址json文件",
                "error": "地址文件没有维护"
            }    
        return {'version':'1.0'}
#     @http.route(['/app_api/user/delete_address'], type='json', auth="user")
    def api_delete_address(self,id):
        partner = request.env['res.users'].browse(request.uid).partner_id
        to_del_partner = request.env['res.partner'].browse(id)
        if id != partner.id:
            to_del_partner.unlink()
            return {
                'msg': 'success'
                }
        return {
                "title": "删除失败",
                "error": "删除地址失败！"
            }
    """
    #根据策划需求，该版本只有一个经销商，直接取该用户的公司信息
    """
    @http.route(['/app_api/user/supplier'], type='json', auth="user")
    def get_supplier_info(self):
        current_user = request.env['res.users'].browse(request.uid)
        supplier = current_user.company_id
        supplier_info = {}
        supplier_info['supplier_name'] = supplier.display_name or ""
        supplier_info['phone'] = supplier.phone or ""
        supplier_info['zip'] = supplier.zip or ""
        if supplier.state_id:
            supplier_info['state'] = supplier.state_id.display_name or ""
        supplier_info['city'] = supplier.city or ""
        supplier_info['street'] = (supplier.street if supplier.street else "") + (supplier.street2 if supplier.street2 else "")
        supplier_info['logo'] = request.httprequest.host_url + image_url(supplier,'logo_web', '300x300')
        return supplier_info

    #获取当前业代的终端列表
    @http.route(['/app_api/user/terminal'], type='json', auth="user")
    def get_saler_terminal(self):
        #saler_user_id = request.uid
        current_user = request.env['res.users'].browse(request.uid)
        company_id = current_user.company_id.id
        request.cr.execute("""select ru.id,rp.id as partner_id,name,phone,mobile from res_partner as rp,res_users as ru where ru.active=True and rp.id = ru.partner_id and rp.customer = TRUE and rp.company_id=%s order by rp.create_date desc""",
            (company_id,))
        terminals =request.cr.dictfetchall()
        for customer in terminals:
            customer['phone'] = customer['phone'] or ""
            customer['mobile'] = customer['mobile'] or ""
            customer['customer_name'] = customer['name']
            customer.pop('name')
            request.cr.execute("""select id as partner_id,street,active,state_name,city,county_name,county,city_id,state_id,zip,email,price_float,is_default,type,mobile,phone,name from res_partner where  id = %s or parent_id = %s order by parent_id asc""",
            (customer['partner_id'],customer['partner_id'],))
            customer_addresses =request.cr.dictfetchall()
            customer['addresses'] = []
            for address in customer_addresses:
                address['price_float'] = address['price_float']
                address['phone'] = address['phone'] or ""
                address['mobile'] = address['mobile'] or ""
                address['street'] = address['street'] or ""
                address['state_name'] = address['state_name'] or ""
                address['city_name'] = address['city'] or ""
                address['city'] =address['city_id'] or 0
                address['county_name'] = address['county_name'] or ""
                address['id'] = address['partner_id']
                address['country_id'] = 49
                address['country_name'] = "中国"
                address['state_id'] = address['state_id'] or 0
                address['county'] = address['county'] or 0
                address['email'] = address['email'] or ""
                address['type'] = address['type'] or ""
                address['zip'] = address['zip'] or ""
                address.pop('city_id')
                address.pop('partner_id')

                customer['addresses'].append(address)
            customer.pop('partner_id')
        response = {}
        response['records'] = terminals
        return response

    #根据名字或者电话搜索当前业代的终端列表
    @http.route(['/app_api/user/terminalsearch'], type='json', auth="user")
    def search_saler_terminal(self,**kwargs):
        #saler_user_id = request.uid
        current_user = request.env['res.users'].browse(request.uid)
        company_id = current_user.company_id.id
        open = 0
        if kwargs.has_key('active'):
            open = int(kwargs['active']) #if open = 1 只显示营业中的partner
        if open == 1:
            request.cr.execute("""select ru.id,rp.id as partner_id,name,phone,mobile from res_partner as rp,res_users as ru where rp.customer = TRUE and ru.active=True and rp.active = True and rp.id = ru.partner_id and rp.company_id=%s and (rp.name like %s or rp.mobile like %s)""",
                (company_id,'%'+kwargs['keyword'].strip()+'%','%'+kwargs['keyword'].strip()+'%',))
        else:
             request.cr.execute("""select ru.id,rp.id as partner_id,name,phone,mobile from res_partner as rp,res_users as ru where rp.customer = TRUE and ru.active=True and rp.id = ru.partner_id and rp.company_id=%s and (rp.name like %s or rp.mobile like %s)""",
                (company_id,'%'+kwargs['keyword'].strip()+'%','%'+kwargs['keyword'].strip()+'%',))
        terminals =request.cr.dictfetchall()



        for customer in terminals:
            customer['phone'] = customer['phone'] or ""
            customer['mobile'] = customer['mobile'] or ""
            customer['customer_name'] = customer['name']
            customer.pop('name')
            request.cr.execute("""select id as partner_id,street,active,state_name,city,county_name,county,city_id,state_id,zip,email,price_float,is_default,type,mobile,phone,name from res_partner where  id = %s or parent_id = %s  order by parent_id asc""",
                (customer['partner_id'],customer['partner_id'],))
            customer_addresses =request.cr.dictfetchall()
            customer['addresses'] = []
            for address in customer_addresses:
                address['price_float'] = address['price_float']
                address['phone'] = address['phone'] or ""
                address['mobile'] = address['mobile'] or ""
                address['street'] = address['street'] or ""
                address['state_name'] = address['state_name'] or ""
                address['city_name'] = address['city'] or ""
                address['city'] =address['city_id'] or 0
                address['county_name'] = address['county_name'] or ""
                address['id'] = address['partner_id']
                address['country_id'] = 49
                address['country_name'] = "中国"
                address['state_id'] = address['state_id'] or 0
                address['county'] = address['county'] or 0
                address['email'] = address['email'] or ""
                address['type'] = address['type'] or ""
                address['zip'] = address['zip'] or ""
                address.pop('partner_id')
                address.pop('city_id')
                customer['addresses'].append(address)
            customer.pop('partner_id')
        response = {}
        response['records'] = terminals
        return response


    #获取业代客户的详情
    @http.route(['/app_api/user/terminaldetail'], type='json', auth="user")
    def get_terminal_detail(self,**kwargs):
        saler_user_id = request.uid
        current_user = request.env['res.users'].browse(request.uid)
        current_customer =  request.env['res.users'].browse(int(kwargs['id'])) #根据user_id 查询当前ID
        partner_ids = current_user.getSalerTerminal(request)
        if current_customer.partner_id.id not in  partner_ids:
            return {
                "error": "您无权查看此客户信息!"
            }

        request.cr.execute("""select id,street,active,state_name,city,county_name,county,city_id,state_id,zip,email,is_default,type,mobile,phone,name,parent_id from res_partner where  id = %s or parent_id = %s order by parent_id asc""",
            (current_customer.partner_id.id,current_customer.partner_id.id,))
        terminals =request.cr.dictfetchall()
        response = {}
        for address in terminals:
            address['phone'] = address['phone'] or ""
            address['mobile'] = address['mobile'] or ""
            address['street'] = address['street'] or ""
            address['state_name'] = address['state_name'] or ""
            address['city_name'] = address['city'] or ""
            address['city'] =address['city_id'] or 0
            address['county_name'] = address['county_name'] or ""
            #address['id'] = address['partner_id']
            address['country_id'] = 49
            address['country_name'] = "中国"
            address['state_id'] = address['state_id'] or 0
            address['county'] = address['county'] or 0
            address['email'] = address['email'] or ""
            address['type'] = address['type'] or ""
            address['zip'] = address['zip'] or ""

            if not address['parent_id']: #主partner
                response['customer_name'] = address['name']
                response['mobile'] = address['mobile']
            address.pop('parent_id')
            address.pop('city_id')

        response['records'] = terminals
        return response


    #返回帐号密码信息
    @http.route(['/app_api/user/login_message'], type='json', auth="public")
    def get_login_message(self, **kwargs):
        #current_user = request.env['res.users'].browse(request.uid)
        #current_customer = request.env['res.users'].browse(int(kwargs['login']))  # 根据user_id 查询当前ID
        request.cr.execute(
            """select login from res_users where login like '18888888%'""",)
        login_messages = request.cr.dictfetchall()
        records = []
        for message in login_messages:
            login_detail = {}
            login_detail['login_id'] = message['login']
            login_detail['login_password'] = '123456'
            records.append(login_detail)
        if len(records) > 1:
            records = [random.choice(records)]
        else:
            records = records
        return {"records":records}



    # 购物车客户
    @http.route(['/app_api/user/shopchart_message'], type='json', auth="user")
    def search_shopchart_message(self, **kwargs):
        current_user = request.env['res.users'].browse(request.uid)
        partner = current_user.partner_id
        order = partner.last_website_so_id if partner.last_website_so_id and partner.last_website_so_id.state == 'draft' else False
        response = {}
        if order:
            if order.partner_id.id == partner.id:
                return response

            request.cr.execute(
                    """select ru.id,rp.id as partner_id,name,phone,mobile from res_partner as rp,res_users as ru where  rp.id=ru.partner_id and rp.id=%s""",
                    (order.partner_id.id,))

            terminals = request.cr.dictfetchall()

            for customer in terminals:
                customer['phone'] = customer['phone'] or ""
                customer['mobile'] = customer['mobile'] or ""
                customer['customer_name'] = customer['name']
                customer.pop('name')
                request.cr.execute(
                    """select id as partner_id,street,active,state_name,city,county_name,county,city_id,state_id,zip,email,price_float,is_default,type,mobile,phone,name from res_partner where  id = %s or parent_id = %s  order by parent_id asc""",
                    (customer['partner_id'], customer['partner_id'],))
                customer_addresses = request.cr.dictfetchall()
                customer['addresses'] = []
                for address in customer_addresses:
                    address['price_float'] = address['price_float']
                    address['phone'] = address['phone'] or ""
                    address['mobile'] = address['mobile'] or ""
                    address['street'] = address['street'] or ""
                    address['state_name'] = address['state_name'] or ""
                    address['city_name'] = address['city'] or ""
                    address['city'] = address['city_id'] or 0
                    address['county_name'] = address['county_name'] or ""
                    address['id'] = address['partner_id']
                    address['country_id'] = 49
                    address['country_name'] = "中国"
                    address['state_id'] = address['state_id'] or 0
                    address['county'] = address['county'] or 0
                    address['email'] = address['email'] or ""
                    address['type'] = address['type'] or ""
                    address['zip'] = address['zip'] or ""
                    address.pop('partner_id')
                    address.pop('city_id')
                    customer['addresses'].append(address)
                customer.pop('partner_id')
            response['records'] = terminals
        return response

    # 版本更新 by hyf
    @http.route(['/app_api/update_version_api'], type='json', auth='none')
    def update_version(self, **kwargs):

        try:
            data = {}
            error_info = {}
            if kwargs:
                channel = kwargs['channel']
                model = kwargs['model']
                system = kwargs['system']
                version = kwargs['version']
                appkey = kwargs['appkey']
                error_info['error'] = []
                if not channel:
                    error_info['error'].append('channel不能为空')
                if not system:
                    error_info['error'].append('system不能为空')
                if not appkey:
                    error_info['error'].append('appkey不能为空')
                if error_info['error']:
                    return{
                        "title": "app版本更新",
                        "error": error_info['error'],
                    }

            if system == "iOS":  # iOS通道
                request.cr.execute('''select update_url,version,update_size,update_title,update_note
                                   ,update_interval,update_limittimes,power_update
                                   from api_update_ios WHERE system_type='%s' and appkey='%s' and channel_number='%s' ORDER BY write_date DESC LIMIT 1'''% (system, appkey, channel))
                rows_dict = request.cr.dictfetchall()
                if rows_dict:
                    data['url'] = rows_dict[0]['update_url']
                    data['version'] = rows_dict[0]['version']
                    data['size'] = rows_dict[0]['update_size']
                    data['title'] = rows_dict[0]['update_title']
                    data['changes'] = rows_dict[0]['update_note']
                    if rows_dict[0]['power_update'] == "1":  # 强制更新
                        data['upgrade'] = 801
                    else:
                        if version != data['version']:
                            data['upgrade'] = 800
                        else:
                            data['upgrade'] = 0
                    data['interval'] = rows_dict[0]['update_interval']
                    data['limitTimes'] = rows_dict[0]['update_limittimes']
                else:
                    error_info['msg'] = '服务器无可更新版本'
            elif system == "Android":  # Android通道
                request.cr.execute('''select update_url,version,update_size,update_title,update_note
                                   ,update_interval,update_limittimes,power_update
                                   from api_update_android WHERE system_type='%s' and appkey='%s' and channel_number='%s' ORDER BY write_date DESC LIMIT 1'''%(system, appkey, channel))
                rows_dict = request.cr.dictfetchall()
                if rows_dict:
                    data['url'] = rows_dict[0]['update_url']
                    data['version'] = rows_dict[0]['version']
                    data['size'] = rows_dict[0]['update_size']
                    data['title'] = rows_dict[0]['update_title']
                    data['changes'] = rows_dict[0]['update_note']
                    if rows_dict[0]['power_update'] == "1":  # 强制更新
                        data['upgrade'] = 801
                    else:
                        if version != data['version']:
                            data['upgrade'] = 800
                        else:
                            data['upgrade'] = 0
                    data['interval'] = rows_dict[0]['update_interval']
                    data['limitTimes'] = rows_dict[0]['update_limittimes']
                else:
                    error_info['msg'] = '服务器无可更新版本'
            else:
                error_info['msg'] = '传入数据出错'
            result = {
                "url": data['url'] or '',
                "version": data['version'] or '',
                "size": data['size'] or '',
                "title": data['title'] or 'app更新版本',
                "changes": data['changes'] or '',
                "upgrade": data['upgrade'] or 1,
                "interval": data['interval'] or 0,
                "limitTimes": data['limitTimes'] or 0
            }
            return result
        except Exception , e:
            if error_info:
                error = error_info['msg']
            else:
                error = ''
            return{
                "title": "app版本更新",
                "error": error,
            }