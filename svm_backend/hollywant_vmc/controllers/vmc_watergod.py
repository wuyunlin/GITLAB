# -*- coding: utf-8 -*-
from odoo import http,SUPERUSER_ID
import odoo
from datetime import datetime, timedelta
from odoo.http import request
from odoo.addons.web.controllers.main import DataSet
from common import *
import odoo.tools.config as config
from odoo.tools import float_compare
import logging
import json
import time
import requests
from odoo.addons.hollywant_base.tools.version_utils import versionCompare
from base_controller import baseController

class vmcWaterGodController(baseController):
	# 获取售货机地址　新增　
	def vmc_machines_info(self, *args, **kwargs):
		factory_code = kwargs['factory_code']
		sql = """select COALESCE(rcscc.name,'') as qu_name ,COALESCE(rcsc.name,'') as city_name,
				 vm.vmc_code,COALESCE(vlm.location_address,' ') as location_address ,vm.* from vmc_machines vm

	            left join res_country_state_city_county rcscc on rcscc.id=vm.county

	            left join res_country_state_city rcsc on rcsc.id=vm.city_id

	            left join vmc_location_manage vlm on vlm.id=vm.location_name

	            where vm.factory_code='%s' """ % (factory_code)

		request.cr.execute(sql)
		result_list = request.cr.dictfetchall()
		if result_list and len(result_list)>0:
			result = result_list[0]
		else:
			return {
				"title": "获取机器信息",
				"error": "机器号不存在！"
			}
		address = '%s%s%s' % (
		result.get('city_name' or ''), result.get('qu_name' or ''), result.get('location_address' or '')
		)
		machine_code = result.get('vmc_code', '')
		current_time = datetime.utcnow()
		current_time_str = datetime.strftime(current_time, "%Y-%m-%d")
		current_today = localizeTime(request, current_time_str, "%Y-%m-%d", "%Y-%m-%d")
		filter_elem_date_str =result.get('filter_elem_date', current_time_str)
		if not filter_elem_date_str:
			filter_elem_date_str = current_time_str
		filter_elem_date = localizeTime(request,filter_elem_date_str,"%Y-%m-%d", "%Y-%m-%d")
		filter_use_days = (current_today-filter_elem_date).days
		filter_elem_day=100-filter_use_days
		records = {'address': address,
					'machine_code': machine_code,
					'filter_elem_day':filter_elem_day

				   }
		return records

	#维修记录上报接口
	def repair_up_report(self, *args, **kwargs):
		factory_code = kwargs['factory_code']
		report_type = kwargs['report_type']
		report_value = kwargs['report_value']
		report_content = kwargs['report_content']
		machines_name = kwargs.get('machines_name','')
		records = {}
		result_list = getMachinesByFactoryCode(request,factory_code)
		if result_list and len(result_list) > 0:
			machine = result_list[0]
		else:
			return {
				"title": "获取机器信息",
				"error": "机器号不存在！"
			}
		repaire_uid = 1
		if request.uid:
			repaire_uid = request.uid
		current_time_now = datetime.utcnow()
		current_time = datetime.strftime(current_time_now, "%Y-%m-%d %H:%M:%S")
		machines_number = machine['vmc_code']
		if report_type == 'electrolytic':#如果是电解液
			electrolytic_status = False
			if report_value == "1":
				electrolytic_status= True
			sql = """update vmc_machines set electrolytic_status =%s where factory_code='%s'""" % (electrolytic_status,factory_code)
			request.cr.execute(sql)
			records.update({'message': '电解液补满成功'})
		elif report_type == 'filter':
			sql = """update vmc_machines set filter_elem_date ='%s' where factory_code='%s'""" % (current_time, factory_code)
			request.cr.execute(sql)
			records.update({'message': '更换滤芯成功'})
		elif report_type == 'phvalue':
			sql = """update vmc_machines set ph_value =%s where factory_code='%s'""" % (
				report_value, factory_code)
			request.cr.execute(sql)
			records.update({'message': '更新ph值成功'})
		elif report_type == 'accvalue':
			sql = """update vmc_machines set acc_value =%s where factory_code='%s'""" % (
				report_value, factory_code)
			request.cr.execute(sql)
			records.update({'message': '更新ACC值成功'})
		elif report_type == 'waterdegree':
			sql = """update vmc_machines set water_degree =%s where factory_code='%s'""" % (
				report_value, factory_code)
			request.cr.execute(sql)
			records.update({'message': '更新水温成功'})
		else:
			return  {
				"title": "获取机器信息",
				"error": "汇报类型不支持！"
			}
		sql = """insert into water_god_repair_sign(sign_time,create_uid,write_date,sign_person,machines_number,write_uid,machines_name,repair_time,create_date,repair_content)
							  values('%s',%s,'%s',%s,'%s',%s,'%s','%s','%s','%s')""" % (	current_time, repaire_uid, current_time, repaire_uid, machines_number, repaire_uid, machines_name, current_time,current_time, report_content)
		print sql
		request.cr.execute(sql)
		return records

