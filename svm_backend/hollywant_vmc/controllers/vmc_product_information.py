# -*- coding: utf-8 -*-
from ..controllers.vmc import vmcController, predispatch
from odoo.http import request
from common import image_url, localizeStrTime
from datetime import datetime, timedelta
import logging

_logger = logging.getLogger(__name__)


@predispatch
class VmcProductInfo(vmcController):
    # 售货机商品基本信息接口
    @predispatch
    def vmc_product_list(self, *args, **kwargs):
        _logger.debug("Begin vmc_machine_product_list ...")
        stacks_products = []
        if 'machine_id' in kwargs:
            machine_id = kwargs['machine_id']
            machine = request.env['vmc.machines'].sudo().browse(
                int(machine_id)
            )
            stacks = machine.machine_stack
            stacks_ids = stacks.ids
            stacks_products = self._get_product_info(machine_id, stacks_ids)
        _logger.debug("End vmc_machine_product_list\n")
        return {"total": len(stacks_products), "records": stacks_products}

    # 获取每个料道的商品基本信息
    def _get_product_info(self, machine_id, stacks_ids):
        model = "vmc.machine.stacks"
        fields = [
            "product_id",
            "stack_number",
            "box_number",
            "stock",
            "product_sequence_no",
        ]
        domain = [('id', 'in', stacks_ids)]
        offset = 0
        limit = False
        sort = "product_id desc"
        stacks = self.search_read(model, fields, offset, limit, domain, sort)
        stacks_products = []
        for stack in stacks['records']:
            if stack['product_id']:
                record = {}
                stack_id = stack['id'] or 0
                product = request.env['product.product'].sudo().browse(
                    stack['product_id'][0])
                record['id'] = stack['product_id'][0] or ""
                record['name'] = stack['product_id'][1] or ""
                record['stack_no'] = stack['stack_number'] or ""
                record['box_no'] = stack['box_number'] or ""
                host_url_replaced = request.httprequest.host_url.replace('http:', 'https:')
                host_url_replaced = host_url_replaced.replace(':88', '')
                record['image_url'] = host_url_replaced + image_url(
                    product.product_tmpl_id, 'image', '300x300')
                # 商品原价
                request.cr.execute("""
                                         select vs.product_price
                                         from vmc_machine_stacks as vs
                                         where vs.stack_number = %s and vs.box_number= %s and vs.vmc_machine_id = %s
                                         """, (stack['stack_number'], stack['box_number'], machine_id)
                                   )
                s = request.cr.dictfetchone()
                product_price = s['product_price'] or 0
                product_price = int(product_price * 100)
                record['price'] = product_price
                record['seq_no'] = stack['product_sequence_no'] or ""
                # 商品类别,饮品还是食品
                record['product_type'] = product.product_tmpl_id.categ_id.name or ""
                # 净含量
                record['net_weight'] = product.product_tmpl_id.net_weight or ""
                # 数据库得到的字段, 包含 商品详情图片url 的html字符串
                product_details_html_info = product.product_tmpl_id.product_describe
                # 对字符串切片, 拿到图片的url
                if product_details_html_info:
                    index_start = product_details_html_info.find('/') + 1  # 从斜杠之后开始切片
                    index_end = product_details_html_info.find('.jpg')
                    if index_end <> -1:
                        product_details_image_url = product_details_html_info[index_start:index_end + offset]
                    else:
                        # product_details_image_url = product_details_html_info[index_start:]
                        image_list = product_details_html_info.split('"')
                        if image_list and len(image_list) > 1 and image_list[1] != None and len(image_list[1]) > 1:
                            product_details_image_url = image_list[1][1:]
                        else:
                            record['product_details_image_url'] = ''
                            product_details_image_url = ''
                    host_url_replaced = request.httprequest.host_url.replace('http:', 'https:')
                    host_url_replaced = host_url_replaced.replace(':88', '')
                    record['product_details_image_url'] = (
                        host_url_replaced + product_details_image_url)
                else:
                    record['product_details_image_url'] = ''
                stacks_products.append(record)
        return stacks_products

    # 售货机商品促销信息接口
    @predispatch
    def vmc_promotion_list(self, *args, **kwargs):
        _logger.debug("Begin vmc_machine_product_list ...")
        stacks_products = []
        if 'machine_id' in kwargs:
            machine_id = kwargs['machine_id']
            machine = request.env['vmc.machines'].sudo().browse(
                int(machine_id)
            )
            stacks = machine.machine_stack
            stacks_ids = stacks.ids
            stacks_products = self._get_product_sales_promotion_info(machine_id, stacks_ids)

        _logger.debug("End vmc_machine_product_list\n")
        return {"records": stacks_products}

    # 获取每个料道商品促销信息
    def _get_product_sales_promotion_info(self, machine_id, stacks_ids):
        model = "vmc.machine.stacks"
        fields = [
            "product_id",
            "stack_number",
            "box_number",
            "stock",
        ]
        domain = [('id', 'in', stacks_ids)]
        offset = 0
        limit = False
        sort = "product_id desc"
        stacks = self.search_read(model, fields, offset, limit, domain, sort)
        stacks_products = []
        for stack in stacks['records']:
            if stack['product_id']:
                record = {}
                stack_id = stack['id'] or 0
                product = request.env['product.product'].sudo().browse(
                    stack['product_id'][0])
                record['id'] = stack['product_id'][0] or ""
                record['product_id'] = stack['product_id'][0] or ""
                record['name'] = stack['product_id'][1] or ""
                # 促销详情
                product_id = stack['product_id'][0]
                request.cr.execute(
                    """
                        select vp.id as promotion_id, vp.promotion_type,vp.is_promotional_image_links,vp.promotional_image_links, vp.name, vp.discount_rate,
                               vp.promotion_time_type, vp.start_time, vp.end_time, vp.start_date, vp.period_of_start_date, vp.period_of_end_date, vp.end_date,
                               vp.payment_cash, vp.payment_weixin, vp.payment_alipay, vp.payment_wangbi, vp.unchange_price
                        from vmc_promotion as vp, product_product as pp, product_template as pt,
                             vmc_product_promotion_rel as pl, vmc_machines as vm,vmc_sales_machines_list as vl
                        where pp.id = %s and vp.start_using = True and pl.product_id = pp.id and pp.product_tmpl_id = pt.id
                          and pl.vmc_select_id = vp.id and vl.machine_id = vm.id and vl.vmc_sales_id = vp.id and vm.id = %s
                    """,
                    (product_id, machine_id)
                )
                all_promotion_record = request.cr.dictfetchall()
                current_time = datetime.utcnow() - timedelta(hours=-8)
                current_time_str = datetime.strftime(current_time, "%H:%M:%S")
                current_date_str = datetime.strftime(current_time, "%Y-%m-%d")
                utc_current_time = datetime.utcnow()
                utc_current_timedate_str = datetime.strftime(utc_current_time, "%Y-%m-%d %H:%M:%S")
                # 遍历所有的促销,找出还未过期的促销
                active_promotion_record = []
                for single_promotion_record in all_promotion_record:
                    if single_promotion_record['end_date'] is not None:
                        if current_date_str > single_promotion_record['end_date'] \
                                or (current_date_str == single_promotion_record['end_date']
                                    and current_time_str >= single_promotion_record['end_time']):
                            pass
                        else:
                            active_promotion_record.append(single_promotion_record)
                    elif single_promotion_record['period_of_end_date'] is not None:
                        if utc_current_timedate_str >= single_promotion_record['period_of_end_date']:
                            pass
                        else:
                            active_promotion_record.append(single_promotion_record)
                # 对促销按时间早晚排序
                for single_record in active_promotion_record:
                    if single_record['start_time'] and single_record['start_date'] is not None:
                        single_record['key_for_sort'] = single_record['start_date'] + ' ' + single_record['start_time']
                    elif single_record['period_of_start_date'] is not None:
                        single_record['key_for_sort'] = single_record['period_of_start_date']
                active_promotion_record.sort(key=lambda t: t['key_for_sort'], reverse=False)
                #没有促销规则 则跳过
                if not active_promotion_record:
                    continue
                # 促销详情字段
                record['promotion_details'] = []
                for r in active_promotion_record:
                    single_promotion = {}
                    # 转换成utc时间
                    time_format = "%Y-%m-%d %H:%M:%S"
                    time_period_start = r['period_of_start_date']
                    time_period_end = r['period_of_end_date']
                    if time_period_start is not None:
                        time_period_start = localizeStrTime(request, time_period_start, time_format, time_format)
                    else:
                        time_period_start = ""
                    if time_period_end is not None:
                        time_period_end = localizeStrTime(request, time_period_end, time_format, time_format)
                    else:
                        time_period_end = ""
                    # 促销弹窗图片链接
                    promotional_image_links = r['promotional_image_links'] if r['is_promotional_image_links'] == '是' else ''
                    # 商品原价
                    request.cr.execute("""
                                         select vs.product_price
                                         from vmc_machine_stacks as vs
                                         where vs.stack_number = %s and vs.box_number= %s and vs.vmc_machine_id = %s
                                         """, (stack['stack_number'], stack['box_number'], machine_id)
                                       )
                    s = request.cr.dictfetchone()
                    product_price = s['product_price'] or 0
                    product_price = int(product_price * 100)
                    # 折扣
                    if r['promotion_type'] == "discount":
                        discount_rate = float(r['discount_rate']) / 100
                        promotion_price = product_price * discount_rate
                    # 立减
                    elif r['promotion_type'] == 'unchange_count':
                        price_variance = int(r['unchange_price'] * 100)  # 立减的价格数值
                        promotion_price = product_price - price_variance
                    # 买赠
                    else:
                        promotion_price = 0
                    # 可选支付方式
                    payment_option = []
                    if r['payment_alipay']:
                        payment_option.append('ALIPAY')
                    if r['payment_weixin']:
                        payment_option.append('WECHATPAY')
                    if r['payment_cash']:
                        payment_option.append('RMB')
                    if r['payment_wangbi']:
                        payment_option.append('WANGBI')
                    # 可选支付方式 过渡方案
                    alipay = '1' if r['payment_alipay'] else '0'
                    weixin = '1' if r['payment_weixin'] else '0'
                    cash = '1' if r['payment_cash'] else '0'
                    wangbi = '1' if r['payment_wangbi'] else '0'
                    payment_way = alipay + weixin + cash + wangbi
                    promotion_time_type = 'every_day' if r['promotion_time_type'] == '1' else 'cross_day'  # 促销时间类型
                    single_promotion = {
                        "promotion_type": r["promotion_type"],
                        "name": r["name"],
                        "promotion_id": r['promotion_id'] or 0,
                        "promotion_time_type": promotion_time_type,
                        "start_date": r["start_date"] or "",
                        "end_date": r["end_date"] or "",
                        "start_time": r["start_time"] or "",
                        "end_time": r["end_time"] or "",
                        "time_period_start": time_period_start,
                        "time_period_end": time_period_end,
                        "promotion_price": promotion_price,
                        "payment_way": payment_way,
                        "payment_option": payment_option,
                        "promotional_image_links": promotional_image_links,
                    }
                    # 买赠商品详情
                    freebie = []
                    promotion_id = r['promotion_id']
                    request.cr.execute(
                        """
                        select pt.name, pt.net_weight, vms.product_id as id, vms.product_sequence_no as seq_no, vms.stock,
                               vms.stack_number as stack_no, vms.box_number as box_no
                        from vmc_promotion as vp, vmc_promotion_lines as vpl, product_product as pp,
                             vmc_machine_stacks as vms, vmc_machines as vm, product_template as pt,
                             vmc_product_promotion_rel as pl, vmc_sales_machines_list as vl
                        where pl.product_id = %s and pl.vmc_select_id = vp.id and vpl.promotion_id = vp.id
                          and vpl.product_id = vms.product_id and vms.vmc_machine_id = vm.id and vpl.product_id = pp.id and pp.product_tmpl_id = pt.id
                          and vl.machine_id = vm.id and vl.vmc_sales_id = vp.id
                          and vm.id = %s and vp.start_using = True and vp.id = %s""",
                        (product_id, machine_id, promotion_id)
                    )
                    s = request.cr.dictfetchall()
                    if s:
                        freebie = s
                    single_promotion.update({
                        "freebie": freebie
                    })
                    #record['promotion_details'].append(single_promotion)
                    record['promotion_details']=single_promotion
                    stacks_products.append(record.copy())
        return stacks_products

    # 售货机商品库存信息接口
    @predispatch
    def vmc_stock_list(self, *args, **kwargs):
        _logger.debug("Begin vmc_machine_product_list ...")
        stacks_products = []
        if 'machine_id' in kwargs:
            machine_id = kwargs['machine_id']
            machine = request.env['vmc.machines'].sudo().browse(
                int(machine_id)
            )
            stacks = machine.machine_stack
            stacks_ids = stacks.ids
            stacks_products = self._get_product_stock_info(machine_id, stacks_ids)

        _logger.debug("End vmc_machine_product_list\n")
        return {"records": stacks_products}

    # 获取料道商品库存信息
    def _get_product_stock_info(self, machine_id, stacks_ids):
        model = "vmc.machine.stacks"
        fields = [
            "product_id",
            "stack_number",
            "box_number",
            "stock",
            "stock_qty",
        ]
        domain = [('id', 'in', stacks_ids)]
        offset = 0
        limit = False
        sort = "product_id desc"
        stacks = self.search_read(model, fields, offset, limit, domain, sort)
        stacks_products = []
        for stack in stacks['records']:
            if stack['product_id']:
                record = {}
                record['id'] = stack['product_id'][0] or ""
                record['name'] = stack['product_id'][1] or ""
                record['stack_no'] = stack['stack_number'] or ""
                record['box_no'] = stack['box_number'] or ""
                record['stock'] = stack['stock_qty'] or 0
                stacks_products.append(record)
        return stacks_products
