# -*- coding: utf-8 -*-
import logging

from odoo import http,SUPERUSER_ID
from odoo.http import request

from math import floor

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')


class AppProductApi(http.Controller):
    #得到一个产品的最小单位，以及当前销售单位转换成最小单位的系数，以及最小单位对应的价格
    @http.route(['/app_api/product/product_minuom_factor'], type='json', auth="public", website=True)
    def get_product_saleuom_minuom_byid(self,product_id):
        product_product = request.env['product.product'].browse(product_id)
        return self.get_product_saleuom_minuom(product_product)
    def get_product_saleuom_minuom(self,product_product):
        #获取产品对应的销售单位ID
        product_uom =product_product.product_tmpl_id.uom_id
        product_uom_id =product_uom.id
        product_uom_categ_id =product_uom.category_id.id
        request.cr.execute("""
                select * from product_uom where active = True and category_id=%s order by uom_type,factor
            """, (product_uom_categ_id,))  
        uom_obj = {}
        uomResult=request.cr.dictfetchall()
        sale_uom=1;
        min_uom=1;
        min_uom_id=product_uom_id
        min_uom_name=''
        for uom in uomResult:            
            if uom['id'] == product_uom_id:  #销售单位      
                sale_uom=uom['factor']            
            min_uom= uom['factor'] #循环完是最小单位
            min_uom_id=uom['id']
            min_uom_name=uom['name']
        factor=floor(round(min_uom/sale_uom))
        uom_obj['factor']=factor
        uom_obj['min_uom_id']=min_uom_id
        uom_obj['min_uom_name']=min_uom_name
        return uom_obj
