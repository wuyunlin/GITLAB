# -*- coding: utf-8 -*-
{
    'name': "HollyWant VMC",

    'summary': """
        """,

    'description': """
        如旺自动售货机模块
    """,

    'author': "holly-want",
    'website': "http://http://www.want-want.com/",

    # Categories can be used to filter modules in modules listing
    # for the full list
    'category': 'HollyWant',
    'version': '0.1',
    'sequence': 1000,
    # any module necessary for this one to work correctly
    'depends': [
        'base', 'account', 'crm', 'purchase', 'sale',
        'stock', 'website_quote', 'delivery', 'hollywant_base','account_group',
        'stock_group','website'
    ],

    # always loaded
    'data': [
        'security/svm_group_security.xml',
        'views/vmc_sequence.xml',
        'views/partner.xml',
        # 'views/sale_order.xml',
        'views/warehouse.xml',
        'views/index.xml',
        'views/vmc_machine.xml',
        'views/vmc_machine_stacks_view.xml',
        'views/vmc_advertisement_view.xml',
        'views/vmc_advertisement_list_view.xml',
        'views/vmc_order.xml',
        'views/vmc_sales_promotion_view.xml',
        'views/vmc_order_sequence.xml',
        'views/vmc_adjust_stock_view.xml',
        'views/vmc_record.xml',
        'views/vmc_settings.xml',
        'views/vmc_swap_order.xml',
        'wizard/financial_collection_view.xml',
        'views/vmc_stock_move.xml', #  删掉了stock.move_scrap()的Button
        'security/vmc_security.xml',
        'security/ir.model.access.csv',
        'views/vmc_money.xml',
        'views/machine.xml',
        'views/management.xml',
        'wizard_machine_manage/vmc_machine_manage.xml',
        'views/vmc_machine_monitoring_info.xml',
        'views/vmc_users_list.xml',
        'report/vmc_order_report_view.xml',
        'views/vmc_pickgoods_code.xml',
        'views/vmc_create_pickgoods_code.xml',
        'views/vmc_product.xml',
        'views/company.xml',
        'views/inherit_res_company.xml',
        'views/sys_config_view.xml',
        'views/group_menu_view.xml',
        'views/hollywant_stock_adjust_view.xml',
        'views/hollywant_stock_delivery.xml',
        'views/hollywant_stock_return.xml',
        'views/hollywant_stock_view.xml',
        'views/report_stock_add.xml',
        'views/report_stock_adjust.xml',
        'views/report_stock_delivery.xml',
        'views/report_stock_return.xml',
        'views/water_god_repair.xml',
        'views/water_god_repair_sign.xml',
        'views/water_god_minotor.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'qweb': [
        'static/xml/qweb.xml',
    ]
}
