# -*- coding: utf-8 -*-

from odoo import fields, models



class SaleReport(models.Model):
    _inherit = "sale.report"

    pay_method = fields.Selection(
        [
            ('cash', 'Cash'),
            ('bank', 'Bank'),
            ('alipay', '支付宝'),
            ('wechat', '微信支付')
        ],
        string='Pay Method',
        store=True,
        readonly=True
    )

    def _select(self):
        return super(SaleReport, self)._select() + ", s.pay_method as pay_method"

    def _group_by(self):
        return super(SaleReport, self)._group_by() + ", s.pay_method"
