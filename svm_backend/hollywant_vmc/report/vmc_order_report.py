# -*- coding: utf-8 -*-

from odoo import models, fields,api
from odoo import tools


class VmcOrderReport(models.Model):
    _name = "vmc.order.report"
    _auto = False

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='产品',
        readonly='1'
    )

    machine_id = fields.Many2one(
        comodel_name='vmc.machines',
        string='售货机',
        readonly='1'
    )

    create_time = fields.Datetime(
        string="订单日期",
        readonly='1'
    )

    payment_method = fields.Char(
        string='支付方式',
        readonly='1'
    )

    amount_total = fields.Float(
        string='总价',
        readonly='1'
    )

    avg_price = fields.Float(
        string='平均价格',
        readonly='1',
        group_operator="avg"
    )
    counts = fields.Integer(
        string='数量',
        readonly='1'
    )


    # def init(self, cr):
    #     tools.drop_view_if_exists(cr, 'vmc_order_report')
    #     cr.execute("""
    #         CREATE OR REPLACE VIEW vmc_order_report AS (
    #             select min(id) as id,
    #                 product_id,
    #                 machine_id,
    #                 create_time,
    #                 payment_method,
    #                 sum(amount_total) as amount_total,
    #                 round(cast (avg(amount_total) as numeric),2) as avg_price,count(*) as counts
    #
    #             from vmc_order where payment_status='已支付'
    #          group by product_id,
    #                 machine_id,
    #                 create_time,
    #                 payment_method
    #
    #     )""")

    @api.model_cr
    def init(self):
        cr = self._cr
        tools.drop_view_if_exists(cr,'vmc_order_report')
        cr.execute("""
            CREATE OR REPLACE VIEW vmc_order_report AS (
                select min(id) as id,
                    product_id,
                    machine_id,
                    create_time,
                    payment_method,
                    sum(amount_total) as amount_total,
                    round(cast (avg(amount_total) as numeric),2) as avg_price,count(*) as counts

                from vmc_order where payment_status='已支付'
             group by product_id,
                    machine_id,
                    create_time,
                    payment_method

        )""")
