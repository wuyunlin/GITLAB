# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Dashboard',
    'category': 'Sales',
    'summary': 'Hollywant Dashboard',
    'version': '9.0.0.0.1',
    'website': 'b2b.hollywant.com',
    'author': "Hollywant",
    'depends': [
        'web',
    ],
    'qweb': [
        'static/src/xml/dashboard.xml',
    ],
    'css' : [
        'static/src/css/*.css',
    ],
    'js' : [
        'static/src/js/*.js',
    ],
    'data': [
        'views/dashboard_data.xml',
    ],
    'images': [
        'static/src/images/*.png',
    ],
    'license': 'AGPL-3',
    'installable': True,
}
