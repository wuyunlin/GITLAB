# -*- coding: utf-8 -*-
from vmc_app import VmcDashboardAppController, data_filter
from odoo import http
from odoo.http import request
import time
from time import gmtime, strftime, localtime, mktime
from datetime import datetime, timedelta, date
from common import utcStrTime, utctime_start_lastdays, utctime_start_thisday, \
    utctime_start_thisweek, utctime_start_thismonth, vmc_lastdays_ranking_list, utc_strftime_zeropoint, \
    cn_strftime_zeropoint, cn_current_date, sales_amount_and_count, utctime_start_lasthours, cn_time_start_lasthours, \
    single_day_sales_details, several_days_sales_details

import logging

_logger = logging.getLogger(__name__)


class AppV2(VmcDashboardAppController):
    @data_filter
    # 商品销量排行接口
    def product_ranking(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        # puq desc,puq asc,ps desc,ps asc,puq 销量 ps 销售额
        order = 'ps desc'
        days_backwards = 30
        time_start_last_month = utctime_start_lastdays(days_backwards)
        request.cr.execute(
            """
                select product_id, product_name, count(id) as puq, round(cast(sum(amount_total) as numeric),2) as ps
                from """ + vmc_order + """
                where payment_status = '已支付' and create_time >= %s
                group by product_id, product_name
                order by """ + order + """
                """, (time_start_last_month,))
        result = request.cr.dictfetchall()
        sales_ranking_list = []
        for product in result:
            product_id = product.get('product_id')
            product_detail = {
                'product_id': product.get('product_id'),
                'product_name': product.get('product_name'),
                'last_month_sales_amount': product.get('ps'),
                'last_month_sales_count': product.get('puq'),
            }
            request.cr.execute(
                """
                    select count(id) as total_sales_count, round(cast(sum(amount_total) as numeric),2) as total_sales_amount
                    from """ + vmc_order + """
                    where payment_status = '已支付' and product_id = %s
                    """, (product_id,))
            result = request.cr.dictfetchone()
            product_detail.update(result)
            sales_ranking_list.append(product_detail)
        return {"records": sales_ranking_list}

    @data_filter
    # 售货机排行接口
    def vmc_sales_ranking(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        # puq desc,puq asc,ps desc,ps asc,puq 销量 ps 销售额
        order = 'ps desc'
        days_backwards = 30
        offset = 0
        limit = 9999  # 表示取所有数据
        vmc_ranking_list_last_month = vmc_lastdays_ranking_list(days_backwards, vmc_order, offset, limit, order)
        vmc_sales_ranking_list = []
        for vmc in vmc_ranking_list_last_month:
            vmc_id = vmc.get('vmc_id')
            vmc_detail = {
                'vmc_id': vmc.get('vmc_id'),
                'vmc_name': vmc.get('vmc_name'),
                'vmc_brand': vmc.get('vmc_brand'),
                'last_month_sales_amount': vmc.get('ps'),
                'last_month_sales_count': vmc.get('puq'),
            }
            request.cr.execute(
                """
                    select count(id) as total_sales_count, round(cast(sum(amount_total) as numeric),2) as total_sales_amount
                    from """ + vmc_order + """
                    where payment_status = '已支付' and machine_id = %s
                    """, (vmc_id,))
            result = request.cr.dictfetchone()
            vmc_detail.update(result)
            vmc_sales_ranking_list.append(vmc_detail)
        return {'records': vmc_sales_ranking_list}

    # 售货机状态
    @data_filter
    def vmc_status(self, *args, **kwargs):
        vmc_machines = kwargs['vmc_machines']
        limit = 10
        offset = int(kwargs['page']) - 1
        offset = offset * limit
        sql = """select vm.id as vmc_id, vm.payment_cash, rp.name as vmc_name,rp.street2, rp.street as vmc_location,
                      k.name as city, m.name as state, l.name as county,
                      vm.net_status, mo.is_location_abnormal, mo.is_dooropen, mo.is_load_soldout,
                      mo.is_vmc_disconnected, mo.is_leakchange_5jiao, mo.is_leakchange_1yuan, mo.is_vmc_disconnected,
                      vam.area_name, vam.manager as area_admin, vam.mobile as area_admin_tel,
                      vrm.route_name, vrm.manager as route_admin, vrm.mobile as route_admin_tel,
                      vlm.name as location_name, rpa.name as location_admin, vlm.mobile as location_admin_tel, rc.customer_phone as service_tel
               from """ + vmc_machines + """
                left outer join res_partner as rp on (vm.partner_id = rp.id)
                left outer join res_company as rc on (vm.company_id = rc.id)
                left outer join vmc_location_manage as vlm on (vm.location_name = vlm.id)
                left outer join res_users as ru on (vlm.manager = ru.id)
                left outer join res_partner as rpa on (ru.partner_id = rpa.id)
                left outer join vmc_area_manage as vam on (vlm.area_name = vam.id)
                left outer join vmc_route_manage as vrm on (vlm.route_name = vrm.id)
                left outer join vmc_machine_monitoring as mo on (vm.id = mo.vmc_machine_id)
                left outer join vmc_machines_manage as mm on (vm.id = mm.machine_id)
                left outer join res_country_state as m on (m.id = rp.state_id)
                left outer join res_country_state_city as k on (k.id = vm.city_id)
                left outer join res_country_state_city_county as l on (l.id = vm.county)
                where rp.name not like '%废弃%'
                order by vm.net_status asc nulls last, mo.is_dooropen desc nulls last, mo.is_location_abnormal desc nulls last, mo.is_vmc_disconnected desc nulls last,
                         mo.is_leakchange_5jiao desc nulls last, mo.is_leakchange_1yuan desc nulls last, mo.is_load_soldout desc nulls last
               offset {}
               limit {}
            """.format(offset, limit)
        request.cr.execute(sql)
        vmc_list = request.cr.dictfetchall()
        for vmc in vmc_list:
            vmc['vmc_status'] = []
            if vmc['net_status'] == '网络异常':
                vmc['vmc_status'].append('网络异常')
            if vmc['is_dooropen'] == 'true' and vmc['net_status'] == '网络正常':
                vmc['vmc_status'].append('机门打开')
            if vmc['is_location_abnormal'] in ('1', '2') and vmc['net_status'] == '网络正常':
                vmc['vmc_status'].append('位置异常')
            if vmc['is_vmc_disconnected'] == 'true' and vmc['net_status'] == '网络正常':
                vmc['vmc_status'].append('串口异常')
            if vmc['is_leakchange_5jiao'] == 'true' and vmc['net_status'] == '网络正常':
                vmc['vmc_status'].append('五角不足')
            if vmc['is_leakchange_1yuan'] == 'true' and vmc['net_status'] == '网络正常':
                vmc['vmc_status'].append('一元不足')
            if vmc['is_load_soldout'] == 'true' and vmc['net_status'] == '网络正常':
                vmc['vmc_status'].append('料道售空')
            for key, value in vmc.iteritems():
                if value is None:
                    vmc[key] = ''
            state = vmc['state']
            city = vmc['city']
            county = vmc["county"]
            street = vmc["street2"]
            if state != city:
                vmc["vmc_address"] = state + city + county + street
            else:
                vmc["vmc_address"] = city + county + street
            [vmc.pop(key, None) for key in ['score', "net_status", "is_location_abnormal", "is_dooropen",
                                            "is_load_soldout", "payment_cash", "is_vmc_disconnected",
                                            "is_leakchange_1yuan", "is_leakchange_5jiao", "state", "city", "county",
                                            "street2",
                                            ]
             ]
        return {"records": vmc_list}

    @data_filter
    def last_days_sales_trends(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        days_backwards = 30
        records_last_month = sales_amount_and_count(days_backwards, vmc_order)
        vmc_id = 0
        records_last_day = self._sales_trends_perhour(vmc_id, vmc_order)
        last_month_sales_amount = 0
        for e in records_last_month:
            last_month_sales_amount += e['sales_amount']
        last_month_average_sales_amount = round(float(last_month_sales_amount) / 30, 2)
        last_day_sales_amount = 0
        for e in records_last_day:
            last_day_sales_amount += e['sales_amount']
        last_day_average_sales_amount = round(float(last_month_average_sales_amount) / 24, 2)
        r = {
            'last_month_sales_amount': last_month_sales_amount,
            'last_month_average_sales_amount': last_month_average_sales_amount,
            'last_day_sales_amount': last_day_sales_amount,
            'last_day_average_sales_amount': last_day_average_sales_amount,
            'records_last_month': records_last_month,
            'records_last_day': records_last_day
        }
        return r

    # 过去24小时每小时订单分布
    def _sales_trends_perhour(self, vmc_id, vmc_order):
        records = []
        # 当前北京时间
        current_time = datetime.utcnow() - timedelta(hours=-8)
        current_hour = current_time.hour
        # 时间戳间隔为1个小时
        timestamp_interval = 3600
        time_format = "%Y-%m-%d %H:%M:%S"
        # 北京时间与utc时间时间差
        timezone_interval = 8 * 3600
        hours_backwards = 24
        time_start = utctime_start_lasthours(hours_backwards)
        initial_time_start_struct_time = time.strptime(time_start, time_format)
        # utc时间过去24小时前 时间戳
        initial_time_start_timestampp = time.mktime(initial_time_start_struct_time)
        # 北京时间过去24小时前 时间戳
        cn_initial_time_start_timestampp = time.mktime(initial_time_start_struct_time) + timezone_interval
        # 遍历当天每个小时
        for i in range(0, hours_backwards):
            # 查询开始时间戳
            time_start_timestamp = initial_time_start_timestampp + timestamp_interval * i
            time_start_struct_time = localtime(time_start_timestamp)
            time_start = strftime(time_format, time_start_struct_time)
            if i == hours_backwards - 1:  # 当前时刻所在的小时
                # 查询终止时间 utc 时间
                time_end = datetime.utcnow().strftime(time_format)
                # 图表横轴数据 北京时间
                time_period_start = (datetime.utcnow() - timedelta(hours=-8)).strftime('%H')
                time_period_start = time_period_start + ':' + '00'
                time_period = (datetime.utcnow() - timedelta(hours=-8)).strftime('%H:%M')
            else:
                # 图表横轴数据 北京时间
                time_start_timestamp = cn_initial_time_start_timestampp + timestamp_interval * i
                time_period_start = strftime(r'%H:%M', localtime(time_start_timestamp))
                time_end_timestamp = cn_initial_time_start_timestampp + timestamp_interval * (
                    i + 1)
                time_period = strftime(r'%H:%M', localtime(time_end_timestamp))
                # 查询终止时间 utc 时间
                time_end_timestamp = initial_time_start_timestampp + timestamp_interval * (i + 1) - 1
                time_end_struct_time = localtime(time_end_timestamp)
                time_end = time.strftime(time_format, time_end_struct_time)
            time_period_interval = '60'
            record = self._sales_data(vmc_id, time_start, time_end, time_period_start, time_period,
                                      time_period_interval, vmc_order)
            records.append(record)
        return records

    # 更多数据
    @data_filter
    def single_vmc_or_product_sales_details(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        time_start = kwargs['time_start']
        time_end = kwargs['time_end']
        vmc_id = kwargs['vmc_id']
        product_id = kwargs['product_id']
        sql_extra = ""
        if product_id != 0:
            sql_extra += " and product_id = {}".format(product_id)
        else:
            pass
        cn_datetime_start = time_start + ' ' + "00:00:00"
        time_format = "%Y-%m-%d %H:%M:%S"
        datetime_start = utcStrTime(request, cn_datetime_start, time_format, time_format)
        datetime_end = time_end + ' ' + '15:59:59'
        if time_start == time_end:
            hours_backwards = 24  # 查询某天每小时订单情况
            record = single_day_sales_details(datetime_start, hours_backwards, vmc_order, vmc_id, sql_extra)
        else:  # 查询这一段时间每一天的订单情况
            time_interval = ''
            record = several_days_sales_details(datetime_start, datetime_end, vmc_id, time_interval, vmc_order, sql_extra)
        for r in record:
            r.pop('time_period_interval')
        records = {
            'records': record
        }
        return records