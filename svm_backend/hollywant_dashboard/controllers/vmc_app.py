# -*- coding: utf-8 -*-

import logging
from time import gmtime, strftime, localtime, mktime

from common import *
from common import utcStrTime, utctime_start_lastdays, utctime_start_thisday, \
    utctime_start_thisweek, utctime_start_thismonth, vmc_lastdays_ranking_list, utc_strftime_zeropoint, \
    cn_strftime_zeropoint, cn_current_date
from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')


# 过滤出用户有权限查看的数据, 过滤停用的售货机
def data_filter(func):
    def _data_filter(*args, **kwargs):
        _logger.debug("Begin data_filter ...")
        uid = request.session.uid
        if uid is None or uid is False:
            return {
                "title": "您还未登录",
                "error": "未登录"
            }
        sql = """
                select *
                from res_groups_users_rel
                where uid = %s and gid = (select id from res_groups where name = 'Svm_show_all_company')
              """ % (uid,)
        request.cr.execute(sql)
        r = request.cr.dictfetchone()
        if r is not None:  # 用户在能查看所有公司数据的权限组内
            sql = """
                    select id
                    from res_company
                  """
            request.cr.execute(sql)
            s = request.cr.fetchall()
            # 公司列表
            company = []
            for c in s:
                c = c[0]
                company.append(c)
            company_id_tuple = tuple(company)
            vmc_order = "(select vor.*" \
                        " from vmc_order as vor " \
                        " inner join vmc_machines as vma on (vor.machine_id = vma.id)" \
                        " where vma.is_disable = 'false') " \
                        "as vo"
            vmc_machines = "(select * from vmc_machines where is_disable = 'false') as vm"
            product_template = "(select * from product_template ) as pt"

        else:
            sql = """
                    select rc.id
                    from res_users as ru
                    inner join res_company as rc on (ru.company_id = rc.id)
                    where ru.id = %s
                  """ % (uid,)
            request.cr.execute(sql)
            s = request.cr.fetchone()
            company_id_tuple = s + (-1,)  # 防止数据库查询报错
            vmc_order = "(select vor.* " \
                        " from vmc_order as vor " \
                        " inner join vmc_machines as vma on (vor.machine_id = vma.id) " \
                        " inner join res_company as rc on (vma.company_id = rc.id) " \
                        " where rc.id in {} and vma.is_disable = 'false')" \
                        "as vo".format(company_id_tuple)
            vmc_machines = "(select vma.* " \
                           " from vmc_machines as vma " \
                           " inner join res_company as rc on (vma.company_id = rc.id) " \
                           " where rc.id in {} and vma.is_disable = 'false')" \
                           "as vm".format(company_id_tuple)
            product_template = "(select * " \
                               "from product_template as ptm " \
                               "where ptm.company_id in {} ) " \
                               "as pt".format(company_id_tuple)
        kwargs_extra = {
            'vmc_order': vmc_order,
            'uid': uid,
            'company_id_tuple': company_id_tuple,
            'vmc_machines': vmc_machines,
            'product_template': product_template,
        }
        kwargs.update(kwargs_extra)
        _logger.debug("End data_filter ...\n")
        return func(*args, **kwargs)

    return _data_filter


class VmcDashboardAppController(http.Controller):
    @http.route(['/dashboard/vmc/app'], type='json', auth="none")
    def vmc_call_kw(self, *args, **kwargs):
        result = eval("self." + kwargs['method'])(*args, **kwargs)
        return result

    @data_filter
    # 售货机列表
    def vmc_list(self, *args, **kwargs):
        uid = kwargs['uid']
        company_id_tuple = kwargs['company_id_tuple']
        vmc_machines = kwargs['vmc_machines']
        page = int(kwargs['page']) - 1
        limit = 10
        offset = limit * page
        request.cr.execute(
            """select vm.vmc_brand, vm.id as vmc_id, rp.name
               from """ + vmc_machines + """, res_partner as rp
               where vm.partner_id = rp.id
               order by rp.name
               offset %s
               limit %s
            """, (offset, limit))
        r = request.cr.dictfetchall()
        return {"records": r}

    @data_filter
    # 商品列表
    def product_list(self, *args, **kwargs):
        product_template = kwargs['product_template']
        page = int(kwargs['page']) - 1
        limit = 10
        offset = limit * page
        request.cr.execute(
            """select pp.id as product_id, pt.name as product_name
               from """ + product_template + """, product_product as pp
                       where pt.id = pp.product_tmpl_id
                       order by pt.name
                       offset %s
                       limit %s
                        """, (offset, limit))
        r = request.cr.dictfetchall()
        return {"records": r}

    def _product_list(self, product_template):
        request.cr.execute(
            """select pp.id as product_id, pt.name as product_name
               from """ + product_template + """, product_product as pp
                   where pt.id = pp.product_tmpl_id
                   order by pt.name
                """
        )
        product_list = request.cr.dictfetchall()
        return product_list

    @data_filter
    # 所有的售货机和商品
    def all_vmc_and_products(self, *args, **kwargs):
        vmc_machines = kwargs['vmc_machines']
        product_template = kwargs['product_template']
        request.cr.execute(
            """select vm.vmc_brand, vm.id as vmc_id, rp.name
               from """ + vmc_machines + """, res_partner as rp
                   where vm.partner_id = rp.id
                   order by rp.name
                """
        )
        vmc_list = request.cr.dictfetchall()
        product_list = self._product_list(product_template)
        r = {
            'vmc_list': vmc_list,
            'product_list': product_list,
        }
        return {"records": r}

    @data_filter
    # 筛选器内搜索售货机
    def vmc_searched(self, *args, **kwargs):
        name = kwargs['name']
        name = '%' + name.strip() + '%'
        vmc_machines = kwargs['vmc_machines']
        request.cr.execute(
            """select vm.vmc_brand, vm.id as vmc_id, rp.name
               from """ + vmc_machines + """, res_partner as rp
               where vm.partner_id = rp.id and rp.name ilike %s
               order by rp.name
            """, (name,))
        r = request.cr.dictfetchall()
        return {"records": r}

    @data_filter
    # 首页历史订单成交总额,历史订单成交总数,今日新增订单成交总额,今日新增订单成交总数
    # 售货机总数,本周新增售货机数,过去30天新增售货机数,过去30天新增商品数,线上支付占比,线上支付总额
    def sales_statistics(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        vmc_machines = kwargs['vmc_machines']
        product_template = kwargs['product_template']
        records = []
        record = {}
        request.cr.execute(
            """select count(id) as sales_count, round(cast(sum(amount_total) as numeric),2) as sales_amount
               from """ + vmc_order + """
               where payment_status = '已支付'
            """
        )
        result = request.cr.dictfetchone()
        # 历史订单成交总数
        total_sales_count = result['sales_count'] or 0
        record['total_sales_count'] = total_sales_count
        # 历史订单成交总额
        total_sales_amount = result['sales_amount'] or 0.00
        record['total_sales_amount'] = total_sales_amount
        time_start = utc_strftime_zeropoint()
        request.cr.execute(
            """select count(id) as sales_count, round(cast(sum(amount_total) as numeric),2) as sales_amount
               from """ + vmc_order + """
               where payment_status = '已支付' and create_date >= %s
            """, (time_start,)
        )
        result = request.cr.dictfetchone()
        # 今日新增订单成交总数
        today_sales_count = result['sales_count']
        record['today_sales_count'] = today_sales_count or 0
        # 今日新增订单成交总额
        today_sales_amount = result['sales_amount']
        record['today_sales_amount'] = today_sales_amount or 0.00
        # 线上支付总额,线上支付占比
        request.cr.execute(
            """select count(id) as sales_count, round(cast(sum(amount_total) as numeric),2) as sales_amount
               from """ + vmc_order + """
               where payment_status = '已支付' and payment_method in ('支付宝', '微信', '旺币')"""
        )
        result = request.cr.dictfetchone()
        online_pay_amount = result['sales_amount'] or 0
        print ('online_pay_amount', online_pay_amount)
        if total_sales_amount != 0:
            online_pay_percentage = round(float(online_pay_amount) / total_sales_amount, 2) * 100
        else:
            online_pay_percentage = 0
        record['online_pay_percentage'] = str(int(online_pay_percentage)) + '%'
        record['online_pay_percentage_new'] = str(int(online_pay_percentage))
        # 线上支付总额
        online_pay_amount = result['sales_amount']
        record['online_pay_amount'] = online_pay_amount or 0.00
        # 所有售货机数量
        request.cr.execute(
            """select count(id) as total_vmc_count
               from """ + vmc_machines + """
            """)
        result = request.cr.dictfetchone()
        total_vmc_count = result['total_vmc_count']
        record['total_vmc_count'] = total_vmc_count
        # 本周新增售货机数量
        # 本周周一utc0点
        time_start_week = utctime_start_thisweek()
        request.cr.execute(
            """select count(id) as today_vmc_count
               from """ + vmc_machines + """
               where create_date >= %s
            """, (time_start_week,))
        result = request.cr.dictfetchone()
        today_vmc_count = result['today_vmc_count']
        record['today_vmc_count'] = today_vmc_count
        # 过去30天新增售货机数量
        days_backwards = 30
        time_start_last_month = utctime_start_lastdays(days_backwards)
        request.cr.execute(
            """select count(id) as last_month_vmc_count
               from """ + vmc_machines + """
                       where create_date >= %s
                    """, (time_start_last_month,))
        result = request.cr.dictfetchone()
        last_month_vmc_count = result['last_month_vmc_count']
        record['last_month_vmc_count'] = last_month_vmc_count
        # 过去30天新增商品数
        last_month_product_count = self._last_month_product_count(product_template)
        record['last_month_product_count'] = last_month_product_count
        records.append(record)
        return {"records": records}

    # 过去30天新增商品数
    def _last_month_product_count(self, product_template):
        days_backwards = 30
        time_start_last_month = utctime_start_lastdays(days_backwards)
        request.cr.execute(
            """select count(id) as last_month_product_count
               from """ + product_template + """
               where create_date >= %s
            """, (time_start_last_month,))
        result = request.cr.dictfetchone()
        last_month_product_count = result['last_month_product_count']
        return last_month_product_count

    @data_filter
    # 销量趋势
    def sales_trends(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        vmc_id = kwargs['vmc_id']
        time_interval = kwargs['time_interval']
        # 今日销量趋势
        if time_interval == 'day':
            records = self._sales_trends_perhour(vmc_id, vmc_order)
            # 时间段间隔,分为单位
            time_period_interval = records[0]['time_period_interval']
        # 本周/本月销量趋势
        else:
            records = self._sales_trends_perday(vmc_id, time_interval, vmc_order)
        # 删除不必要的字段
        for r in records:
            r.pop('time_period_interval')
        # 今日/本周/本月 总成交订单笔数
        total_sales_count = 0
        for e in records:
            total_sales_count += e['sales_count']
        if time_interval == 'day':
            return {"records": records, 'total_sales_count': total_sales_count,
                    'time_period_interval': time_period_interval}
        else:
            return {"records": records, 'total_sales_count': total_sales_count}

    @data_filter
    # 售货机排行页面,售货机过去7天每日平均订单成交额排行榜
    def lastweek_vmc_sales_ranking(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        records = []
        # 查询过去7天数据
        days_backwards = 7
        # 榜单前十
        limit = 10
        offset = int(kwargs['page']) - 1
        offset = offset * limit
        order = kwargs.get('order')
        if order == "des" or order is None:
            # 售货机过去7天每日平均订单成交额排行榜前十
            order = 'ps desc'
            vmc_list = vmc_lastdays_ranking_list(days_backwards, vmc_order, offset, limit, order)
        else:
            # 售货机过去7天每日平均订单成交额排行榜倒数前十
            order = 'ps asc'
            vmc_list = vmc_lastdays_ranking_list(days_backwards, vmc_order, offset, limit, order)

        # 每台售货机过去3天的订单成交金额和成交笔数
        for i in range(3):
            record = {}
            # 时间间隔为1天
            time_interval = 3600 * 24
            # 北京时间和 utc 时间的时差
            timezone_interval = 8 * 3600
            # 订单查询起始日期时间
            timestamp_start = time.localtime(time.time() - time_interval * i + timezone_interval)
            # 年-月-日
            date_start = time.strftime(r'%Y-%m-%d', timestamp_start)
            date = time.strftime('%m/%d', timestamp_start)
            time_start = date_start + ' ' + '00:00:00'
            # 转换成utc时间
            time_start = utcStrTime(request, time_start, "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            # 当前服务器时间
            t = time.time()
            # 查询终止时间
            timestamp_end = time.localtime(t - time_interval * i + timezone_interval)
            # 格式化时间
            date_end = time.strftime(r'%Y-%m-%d', timestamp_end)
            time_end = date_end + ' ' + '23:59:59'
            # 转换成utc时间
            time_end = utcStrTime(request, time_end, "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            daily_sales_amount = []
            for vmc in vmc_list:
                vmc_id = vmc['vmc_id']
                vmc_name = vmc['vmc_name']
                vmc_brand = vmc['vmc_brand']
                request.cr.execute(
                    """select round(cast(sum(amount_total) as numeric),2) as sales_amount, count(id) as sales_count
                       from vmc_order
                       where machine_id = %s and payment_status = '已支付' and create_time >= %s and create_time <= %s
                    """,
                    (vmc_id, time_start, time_end,))
                request_result = request.cr.dictfetchone()
                sales_amount = request_result['sales_amount'] or 0.00
                sales_count = request_result['sales_count']
                daily_sales_amount.append(
                    {
                        "vmc_name": vmc_name,
                        "vmc_brand": vmc_brand,
                        "sales_amount": sales_amount,
                        "sales_count": sales_count
                    }
                )
            record['date'] = date
            record['daily_sales_amount'] = daily_sales_amount
            records.insert(0, record)
        average_vmc_sales_ranking = self._average_vmc_sales_ranking(vmc_list)
        return {"records": records, "average_vmc_sales_ranking": average_vmc_sales_ranking}

    @data_filter
    # 商品销量排行接口
    def product_sales_ranking(self, *args, **kwargs):
        vmc_order = kwargs['vmc_order']
        limit = 10
        offset = int(kwargs['page']) - 1
        offset = offset * limit
        vmc_id = kwargs['vmc_id']
        time_interval = kwargs['time_interval']
        # puq desc,puq asc,ps desc,ps asc,puq 销量 ps 销售额
        order = 'ps desc'
        if time_interval == 'day':
            time_start = utctime_start_thisday()
        elif time_interval == 'week':
            time_start = utctime_start_thisweek()
        else:
            time_start = utctime_start_thismonth()
        if vmc_id == 0:
            request.cr.execute(
                """
                    select product_id, product_name, count(id) as puq, round(cast(sum(amount_total) as numeric),2) as ps
                    from """ + vmc_order + """
                    where payment_status = '已支付' and create_time >= %s
                    group by product_id, product_name
                    order by """ + order + """
                    offset %s
                    limit %s;
                """, (time_start, offset, limit))
            result = request.cr.dictfetchall()
            sales_ranking_list = []
            for product in result:
                product_detail = {
                    'product_id': product.get('product_id'),
                    'product_name': product.get('product_name'),
                    'sales_amount': product.get('ps'),
                    'sales_count': product.get('puq'),
                }
                sales_ranking_list.append(product_detail)
        else:
            request.cr.execute(
                """
                    select product_id, product_name, count(id) as puq, round(cast(sum(amount_total) as numeric),2) as ps
                    from """ + vmc_order + """
                    where payment_status = '已支付' and machine_id = %s and create_time >= %s
                    group by product_id, product_name
                    order by """ + order + """
                    offset %s
                    limit %s;
                """, (vmc_id, time_start, offset, limit))
            result = request.cr.dictfetchall()
            sales_ranking_list = []
            for product in result:
                product_detail = {
                    'product_id': product.get('product_id'),
                    'product_name': product.get('product_name'),
                    'sales_amount': product.get('ps'),
                    'sales_count': product.get('puq'),
                }
                sales_ranking_list.append(product_detail)
        return {"records": sales_ranking_list}

    # 当天每小时订单分布
    def _sales_trends_perhour(self, vmc_id, vmc_order):
        records = []
        # 当前北京时间
        current_time = datetime.utcnow() - timedelta(hours=-8)
        current_hour = current_time.hour
        # 时间戳间隔为1个小时
        timestamp_interval = 3600
        time_format = "%Y-%m-%d %H:%M:%S"
        # 北京时间与utc时间时间差
        timezone_interval = 8 * 3600
        # 北京时间当天0点
        time_start = cn_strftime_zeropoint()
        initial_time_start_struct_time = time.strptime(time_start, time_format)
        # 初始时间戳 北京时间0点 utc时间昨天下午4点
        initial_time_start_timestampp = time.mktime(initial_time_start_struct_time) - timezone_interval
        # 遍历当天每个小时
        for i in range(current_hour + 1):
            # 查询开始时间戳
            time_start_timestamp = initial_time_start_timestampp + timestamp_interval * i
            time_start_struct_time = localtime(time_start_timestamp)
            time_start = strftime(time_format, time_start_struct_time)
            if i == current_hour:  # 当前时刻所在的小时
                # 查询终止时间 utc 时间
                time_end = datetime.utcnow().strftime(time_format)
                # 图表横轴数据 北京时间
                time_period_start = (datetime.utcnow() - timedelta(hours=-8)).strftime('%H')
                time_period_start = time_period_start + ':' + '00'
                time_period = (datetime.utcnow() - timedelta(hours=-8)).strftime('%H:%M')
            else:
                # 图表横轴数据 北京时间
                time_start_timestamp = initial_time_start_timestampp + timestamp_interval * i + timezone_interval
                time_period_start = strftime(r'%H:%M', localtime(time_start_timestamp))
                time_end_timestamp = initial_time_start_timestampp + timestamp_interval * (i + 1) + timezone_interval
                time_period = strftime(r'%H:%M', localtime(time_end_timestamp))
                # 查询终止时间 utc 时间
                time_end_timestamp = initial_time_start_timestampp + timestamp_interval * (i + 1) - 1
                time_end_struct_time = localtime(time_end_timestamp)
                time_end = time.strftime(time_format, time_end_struct_time)
            time_period_interval = '60'
            record = self._sales_data(vmc_id, time_start, time_end, time_period_start, time_period,
                                      time_period_interval, vmc_order)
            records.append(record)
        return records

    # 本周或本月每天订单分布
    def _sales_trends_perday(self, vmc_id, time_interval, vmc_order):
        records = []
        # 时间戳间隔为1天
        timestamp_interval = 3600 * 24
        time_format = "%Y-%m-%d %H:%M:%S"
        # 北京时间和 utc 时间的时差
        timezone_interval = 8 * 3600
        if time_interval == 'week':
            # 本周周一utc0点
            time_start = utctime_start_thisweek()
        elif time_interval == 'month':
            time_start = utctime_start_thismonth()
        # 转换成utc时间字符串形式
        initial_time_start_struct_time = time.strptime(time_start, time_format)
        # 初始时间戳 北京时间0点 utc昨天下午4点
        initial_time_start_timestampp = time.mktime(initial_time_start_struct_time)
        current_time = datetime.utcnow() - timedelta(hours=-8)
        if time_interval == 'week':
            current_day = current_time.isoweekday()  # 当前为周几 int类型
        elif time_interval == 'month':
            current_day = current_time.day  # 当前为本月几号 int类型
        for i in range(current_day):
            # 查询开始时间戳
            time_start_timestamp = initial_time_start_timestampp + timestamp_interval * i
            time_start_struct_time = localtime(time_start_timestamp)
            time_start = strftime(time_format, time_start_struct_time)
            # 图表横轴数据格式
            time_period_format = '%m/%d'
            if i == current_day - 1:  # 当天
                # 图表横轴数据 北京时间
                if time_interval == 'month':
                    # 月-日
                    time_period = (datetime.utcnow() - timedelta(hours=-8)).strftime(time_period_format)
                else:
                    # 周几
                    time_period = (datetime.utcnow() - timedelta(hours=-8)).strftime('%w')
                    time_period = self._weekday_cn(time_period)
            else:
                # 图表横轴数据 北京时间
                time_period_timestamp = initial_time_start_timestampp + timestamp_interval * i + timezone_interval
                if time_interval == 'month':
                    # 月-日
                    time_period = strftime(time_period_format, localtime(time_period_timestamp))
                else:
                    # 周几
                    time_period = strftime('%w', localtime(time_period_timestamp))
                    time_period = self._weekday_cn(time_period)
            # 查询终止时间 utc 时间
            time_end_timestamp = initial_time_start_timestampp + timestamp_interval * (i + 1) - 1
            time_end_struct_time = localtime(time_end_timestamp)
            time_end = time.strftime(time_format, time_end_struct_time)
            time_period_start = ''
            time_period_interval = ''
            record = self._sales_data(vmc_id, time_start, time_end, time_period_start, time_period,
                                      time_period_interval, vmc_order)
            records.append(record)
        return records

    def _sales_data(self, vmc_id, time_start, time_end, time_period_start, time_period, time_period_interval,
                    vmc_order):
        record = {}
        # 查询每个售货机的订单情况
        if vmc_id != 0:
            request.cr.execute(
                """select round(cast(sum(amount_total) as numeric),2) as sales_amount, count(id) as sales_count
                   from vmc_order
                   where machine_id = %s and payment_status = '已支付' and create_time >= %s and create_time <= %s""",
                (vmc_id, time_start, time_end,))
        # 查询所有售货机的订单情况
        else:
            request.cr.execute(
                """select round(cast(sum(amount_total) as numeric),2) as sales_amount, count(id) as sales_count
                   from """ + vmc_order + """
                   where payment_status = '已支付' and create_time >= %s and create_time <= %s""",
                (time_start, time_end,))
        result = request.cr.dictfetchone()
        sales_amount = result['sales_amount'] or 0.00
        sales_count = result['sales_count'] or 0
        if time_period_start != '':
            record['time_period_start'] = time_period_start
        record['time_period'] = time_period
        record['sales_count'] = sales_count
        record['sales_amount'] = sales_amount
        record['time_period_interval'] = time_period_interval
        if time_period_interval != '':
            record['time_period_interval'] = time_period_interval
        return record

    # 周几, 中文形式
    def _weekday_cn(self, date):
        weekday_dict = {
            "1": "周一",
            "2": "周二",
            "3": "周三",
            "4": "周四",
            "5": "周五",
            "6": "周六",
            "0": "周日"
        }
        return weekday_dict[date]

    # 售货机过去7天每日平均订单成交额
    def _average_vmc_sales_ranking(self, vmc_list):
        records = []
        for vmc in vmc_list:
            record = {}
            vmc_id = vmc['vmc_id']
            vmc_name = vmc['vmc_name']
            # 开始查询时间，从几天前零时开始进行订单查询
            days_backwards = 7
            utctime_start = utctime_start_lastdays(days_backwards)
            request.cr.execute(
                """select round(cast(sum(amount_total) as numeric),2) as sales_amount_perweek, count(id) as sales_count_perweek
                   from vmc_order
                   where machine_id = %s and payment_status = '已支付' and create_date >= %s
                """,
                (vmc_id, utctime_start))
            r = request.cr.dictfetchone()
            sales_amount_perweek = r['sales_amount_perweek'] or 0.00
            sales_count_perweek = r['sales_count_perweek']
            average_sales_amount = round(sales_amount_perweek / 7, 2)
            average_sales_count = round(float(sales_count_perweek) / 7, 2)
            record.update(
                {
                    "vmc_name": vmc_name,
                    "average_sales_amount": average_sales_amount,
                    "average_sales_count": average_sales_count
                }
            )
            records.append(record)
        return records
