# -*- coding: utf-8 -*-

import logging
import json
from common import *
from common import localizeStrTime, image_url, utcStrTime, utctime_start_lastdays, \
    vmc_lastdays_ranking_list, utctime_start_thisweek, utc_strftime_zeropoint


from odoo import http

from odoo.http import request

# from odoo.addons.web.controllers.main import ensure_db, db_info, Home
from odoo.addons.web.controllers.main import ensure_db, Home

_logger = logging.getLogger(__name__)
_schema = logging.getLogger(__name__ + '.schema')


class HollywantDashboard(http.Controller):
    @http.route([
        '/dashboard/vmc/tv',
    ], type='http', auth="public", website=True)
    def dashboard(self, **post):
        ensure_db()
        request.params['login_success'] = False
        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except odoo.exceptions.AccessDenied:
            values['databases'] = None
        old_uid = request.uid
        uid = request.session.authenticate(request.session.db, "dashboard", "123456")
        if uid is not False:
            request.params['login_success'] = True
        request.uid = old_uid
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }
        return request.render("hollywant_dashboard.vmc_dashboard_tv",qcontext=context)


class HollywantDashboard_warning(http.Controller):
    @http.route([
        '/dashboard/vmc_warning/tv',
    ], type='http', auth="public", website=True)
    def dashboard(self, **post):
        ensure_db()
        request.params['login_success'] = False
        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except odoo.exceptions.AccessDenied:
            values['databases'] = None
        old_uid = request.uid
        uid = request.session.authenticate(request.session.db, "dashboard", "123456")
        print uid
        if uid is not False:
            request.params['login_success'] = True
        request.uid = old_uid
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }
        return request.render("hollywant_dashboard.vmc_dashboard_warning",qcontext=context)

class HollywantDashboard_map(http.Controller):
    @http.route([
        '/dashboard/vmc_warning/map',
    ], type='http', auth="public", website=True)
    def dashboard(self, **post):
        ensure_db()
        request.params['login_success'] = False
        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except odoo.exceptions.AccessDenied:
            values['databases'] = None
        old_uid = request.uid
        uid = request.session.authenticate(request.session.db, "dashboard", "123456")
        print uid
        if uid is not False:
            request.params['login_success'] = True
        request.uid = old_uid
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }
        return request.render("hollywant_dashboard.vmc_dashboard_map",qcontext=context)

class vmc_tv_api(http.Controller):
    # 首页商品销量排行接口
    @http.route(['/dashboard/vmc_tv/product_sales_ranking'], type='json', auth="public", website=True)
    def vmc_product_sales_ranking(self, *args, **kwargs):
        # puq desc,puq asc,ps desc,ps asc,puq 销量 ps 销售额
        order = 'ps desc'
        vmc_order = "(select vor.*" \
                    " from vmc_order as vor " \
                    " inner join vmc_machines as vma on (vor.machine_id = vma.id)" \
                    " where vma.is_disable = 'false' )" \
                    "as vo"
        request.cr.execute(
            """
                select product_name, count(id) as puq, round(cast(sum(amount_total) as numeric),2) as ps
                from """ + vmc_order + """
                where payment_status = '已支付'
                group by product_name
                order by """ + order + """
                limit 6;
            """)
        result = request.cr.dictfetchall()
        sales_ranking_list = []
        for product in result:
            product_detail = {
                'product_name': product.get('product_name'),
                'sales_amount': "%0.2f" % product.get('ps'),
                'sales_count': "%0.2f" % product.get('puq'),
            }
            sales_ranking_list.append(product_detail)
        return sales_ranking_list

    # 首页历史订单成交总额、历史订单成交总数、20天订单成交总额、今日新增订单成交总额、今日新增订单成交总数
    @http.route(['/dashboard/vmc_tv/order_statistics'], type='json', auth='public')
    def order_statistics(self):
        records = []
        record = {}
        vmc_order = "(select vor.*" \
                    " from vmc_order as vor " \
                    " inner join vmc_machines as vma on (vor.machine_id = vma.id)" \
                    " where vma.is_disable = 'false' )" \
                    "as vo"
        request.cr.execute(
            """select count(id) as sales_count, round(cast(sum(amount_total) as numeric),2) as sales_amount
               from """ + vmc_order + """
               where payment_status = '已支付'
            """
        )
        result = request.cr.dictfetchall()
        # 历史订单成交总数
        record['total_sales_count'] = result[0]['sales_count']
        # 历史订单成交总额
        record['total_sales_amount'] = '%.2f' % result[0]['sales_amount'] or 0.00
        time_start = utc_strftime_zeropoint()
        request.cr.execute(
            """select count(id) as sales_count, round(cast(sum(amount_total) as numeric),2) as sales_amount
               from vmc_order
               where payment_status = '已支付' and create_time >= %s

            """,
            (time_start,))
        result = request.cr.dictfetchall()
        # 今日新增订单成交总数
        record['today_sales_count'] = result[0]['sales_count'] or 0
        # 今日新增订单成交总额
        today_sales_amount = result[0]['sales_amount']
        if today_sales_amount is not None:
            record['today_sales_amount'] = '%.2f' % today_sales_amount
        else:
            record['today_sales_amount'] = 0.00
        days_backwards = 20
        # 从20天前零时开始进行订单查询
        time_lastmonth = utctime_start_lastdays(days_backwards)
        request.cr.execute(
            """select round(cast(sum(amount_total) as numeric),2) as sales_amount
               from vmc_order
               where payment_status = '已支付' and create_time >= %s

            """,
            (time_lastmonth,))
        result = request.cr.dictfetchall()
        # 过去20天订单成交总额
        lastmonth__sales_amount = result[0]['sales_amount']
        if lastmonth__sales_amount is not None:
            record['lastmonth_sales_amount'] = '%.2f' % lastmonth__sales_amount
        else:
            record['lastmonth_sales_amount'] = 0.00
        records.append(record)
        return {"records": records}

    # 所有的售货机数量，本周新增售货机数量, 过去20天订单成交总额前四名的售货机的名称和机型
    @http.route(['/dashboard/vmc_tv/vmc_lastmonth_sales_ranking'], type='json', auth="public", website=True)
    def vmc_lastmonth_sales_ranking(self):
        days_backwards = 20
        offset = 0
        limit = 4
        order = 'ps desc'
        vmc_order = 'vmc_order'
        list = vmc_lastdays_ranking_list(days_backwards, vmc_order, offset, limit, order)
        vmc_machines = "(select * from vmc_machines where is_disable = 'false' ) as vm"
        # 所有售货机数量
        request.cr.execute(
            """select count(id) as total_vmc_number
               from """ + vmc_machines + """
            """)
        utctimestartthisweek = utctime_start_thisweek()
        result = request.cr.dictfetchone()
        # 本周新增售货机数量
        request.cr.execute(
            """select count(id) as thisweek_vmc_number
               from vmc_machines
               where create_date >= %s
            """, (utctimestartthisweek,))
        r = request.cr.dictfetchone()
        list.append(result)
        list.append(r)
        return list

    # 首页图表 过去7日每日订单额
    @http.route(['/dashboard/vmc_tv/daily_sales_amount'], type='json', auth='public')
    def daily_sales_amount(self):
        records = []
        # 查询过去7天每天的订单额
        for i in range(7):
            record = {}
            # 时间间隔为1天
            time_interval = 3600 * 24
            # 北京时区与 utc 时区 时间差
            timezone_interval = 3600 * 8
            # 订单查询起始时间
            timestamp_start = time.localtime(time.time() - time_interval * i + timezone_interval)
            # 日期格式为年-月-日
            date_start = time.strftime(r'%Y-%m-%d', timestamp_start)
            # 日期格式为月-日
            date = time.strftime(r'%m/%d', timestamp_start)
            time_start = date_start + ' ' + '00:00:00'
            # 转换成utc时间
            time_start = utcStrTime(request, time_start, "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            # 当前服务器日期时间
            t = time.time()
            # 订单查询终止时间
            timestamp_end = time.localtime(t - time_interval * i + timezone_interval)
            date_end = time.strftime(r'%Y-%m-%d', timestamp_end)
            time_end = date_end + ' ' + '23:59:59'
            # 转换成utc时间
            time_end = utcStrTime(request, time_end, "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            request.cr.execute(
                """select round(cast(sum(amount_total) as numeric),2) as sales_amount
                   from vmc_order
                   where payment_status = '已支付' and create_time >= %s and create_time <= %s

                """,
                (time_start, time_end,))
            result = request.cr.dictfetchall()
            daily_sales_amount = result[0]['sales_amount']
            if daily_sales_amount is not None:
                record['daily_sales_amount'] = '%.2f' % daily_sales_amount
            else:
                record['daily_sales_amount'] = 0.00
            record['date'] = date
            records.insert(0, record)
        return {"records": records}

    # 过去15天订单成交总额前四名的售货机每个售货机每天的成交金额
    @http.route(['/dashboard/vmc_tv/vmc_daily_sales_amount'], type='json', auth="public", website=True)
    def vmc_daily_sales_amount(self):
        records = []
        offset = 0
        limit = 4
        days_backwards = 20
        order = 'ps desc'
        vmc_order = 'vmc_order'
        vmc_list = vmc_lastdays_ranking_list(days_backwards, vmc_order, offset, limit, order)
        # 过去15天订单成交总额前四名的每家售货机每天的成交金额
        for i in range(15):
            record = {}
            # 时间间隔为1天
            time_interval = 3600 * 24
            # 北京时区与 utc 时区 时间差
            timezone_interval = 3600 * 8
            # 订单查询起始日期时间
            current_time = time.localtime(time.time() - time_interval * i + timezone_interval)
            # 格式化时间 年-月-日
            date_start = time.strftime(r'%Y-%m-%d', current_time)
            # 格式化时间 月-日
            date = time.strftime(r'%m/%d', current_time)
            time_start = date_start + ' ' + '00:00:00'
            # 转换成utc时间
            time_start = utcStrTime(request, time_start, "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            # 格式化时间
            date_end = time.strftime(r'%Y-%m-%d', current_time)
            time_end = date_end + ' ' + '23:59:59'
            # 转换成utc时间
            time_end = utcStrTime(request, time_end, "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M:%S")
            daily_sales_amount = []
            for vmc in vmc_list:
                vmc_id = vmc['vmc_id']
                vmc_name = vmc['vmc_name']
                request.cr.execute(
                    """select round(cast(sum(amount_total) as numeric),2) as sales_amount
                       from vmc_order
                       where machine_id = %s and payment_status = '已支付' and create_time >= %s and create_time <= %s
                    """,
                    (vmc_id, time_start, time_end,))
                request_result = request.cr.dictfetchall()
                vmc_daily_sale_amount = request_result[0]['sales_amount']
                if vmc_daily_sale_amount is not None:
                    vmc_daily_sale_amount = '%.2f' % vmc_daily_sale_amount
                else:
                    vmc_daily_sale_amount = 0.00
                daily_sales_amount.append(
                    {
                        "vmc_name": vmc_name,
                        "sales_amount": vmc_daily_sale_amount
                    }
                )
            record['date'] = date
            record['daily_sales_amount'] = daily_sales_amount
            records.insert(0, record)
        return {"records": records}

    # 售货机监控
    @http.route(['/dashboard/vmc_tv/vmc_monitor'], type='json', auth="public", website=True)
    def vmc_monitor(self):
        vmc_machines = "(select * from vmc_machines where is_disable = 'false') as vm"
        request.cr.execute(
            """select vm.id, vm.payment_cash, rp.name as vmc_name,rp.street2, rp.street as vmc_location,
                      m.name as state, k.name as city, l.name as county,
                      vm.net_status, mo.is_location_abnormal, mo.is_dooropen, mo.is_load_soldout,
                      mo.is_vmc_disconnected, mo.is_leakchange_5jiao, mo.is_leakchange_1yuan
               from """ + vmc_machines + """
                left outer join res_partner as rp on (vm.partner_id = rp.id)
                left outer join vmc_machine_monitoring as mo on (vm.id = mo.vmc_machine_id)
                left outer join vmc_machines_manage as mm on (vm.id = mm.machine_id)
                left outer join res_country_state as m on (m.id = rp.state_id)
                left outer join res_country_state_city as k on k.id = vm.city_id
                left outer join res_country_state_city_county as l on l.id = vm.county
                where rp.name not like '%废弃%'
            """
        )
        r = request.cr.dictfetchall()
        for vmc in r:
            vmc['is_location_abnormal'] = True if vmc['is_location_abnormal'] in ('1', '2') else False
            vmc['net_status'] = True if vmc['net_status'] == '网络异常' else False
            for key, value in vmc.iteritems():
                if value is None:
                    vmc[key] = ''
                elif value == 'false':
                    vmc[key] = False
                elif value == 'true':
                    vmc[key] = True
            state = vmc['state']
            city = vmc['city']
            county = vmc["county"]
            street = vmc["street2"]
            if state != city:
                vmc["vmc_address"] = state + city + county + street
            else:
                vmc["vmc_address"] = city + county + street
            vmc.pop("city", None)
            vmc.pop("county", None)
            vmc.pop("street2", None)
        return {"records": r}
