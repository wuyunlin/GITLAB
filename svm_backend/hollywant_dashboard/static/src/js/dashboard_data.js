/**
 * Created by zhouh on 2016/10/29.
 */
$(function () {
//    $(document).ready(function(){
//        $.post('/dashboard/b2b/tv',function(data){
//
//             console.log(data);
//    });
//});
    hour_refresh();
    minute_refresh();
    timer = $.timer(1000*60*60, function(){
      hour_refresh();
     });
    timer = $.timer(1000*60, function(){
        minute_refresh();
     });
    timer = $.timer(1000, function(){
         var mydate = new Date();
                            var year= mydate.getFullYear();
                            var month= mydate.getMonth() + 1;
                            var day= mydate.getDate();
                            var date=year+'/'+month+'/'+day;
                            var hour=mydate.getHours();
                            var minute=mydate.getMinutes();
                            var hour_head='';
                            var minute_head='';
                            if(hour<10)
                            {
                                hour_head='0';
                            }
                             if(minute<10)
                             {
                                 minute_head='0';
                             }
                            var nowtime=hour_head+hour+':'+minute_head+minute;
                            $("#time").text(date);
                            $("#endtime").text(nowtime);
     })

    function hour_refresh()
    {
                     $.ajax({
                    url: "/dashboard/b2b_tv/daily_sales_amount",
                    type: 'POST',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                            "jsonrpc": "2.0",
                            "params": {},
                            "method": "call",

                        }
                    ),
                    success: function (rm) {
                        var product;
                        console.log(rm);
                             var unit;
                             var divd;
                            var max=parseInt(rm.result.records[0]['daily_sales_amount']);
                            for (var i=0;i<rm.result.records.length;i++)
                            {
                                 if(max<parseInt(rm.result.records[i]['daily_sales_amount']))
                                 {
                                     max=parseInt(rm.result.records[i]['daily_sales_amount']);
                                 }
                            }
                            if(parseInt(max/100000000)!=0)
                            {
                                unit='亿';
                                divd=100000000;
                            }else if(parseInt(max/10000)!=0)
                            {
                                unit='万';
                                divd=10000;
                            }else if(parseInt(max/1000)!=0)
                            {
                                unit='千';
                                divd=1000;
                            }else {
                                unit='元';
                                divd=1;
                            }
                            console.log(unit);
                            console.log(divd);
                            var myChart = echarts.init(document.getElementById('main'));
                            var option = {
                                title: {
                                    text: ''
                                },
                                tooltip: {},
                                legend: {
                                    data:[]
                                },
                                xAxis: {
                                    data: [rm.result.records[0]['date'],
                                            rm.result.records[1]['date'],
                                            rm.result.records[2]['date'],
                                            rm.result.records[3]['date'],
                                            rm.result.records[4]['date'],
                                            rm.result.records[5]['date'],
                                            rm.result.records[6]['date']],
                                     axisLine:{
                                                    lineStyle:{
                                                        color:['#ffffff'],

                                                    }
                                                },
                                     axisLabel : {
                                                     textStyle:{
                                                        fontWeight: 'bold',
                                                    }

                                                },
                                    axisTick:{
                                         show:false,
                                    },

                                },
                                yAxis: [
                                            {
                                                type : 'value',
                                                axisLabel : {
                                                    formatter: '{value} '+unit,
                                                    textStyle:{
                                                        fontWeight: 'bold',

                                                    }

                                                },
                                                axisLine:{
                                                    show: false,
                                                    lineStyle:{
                                                        color:['#ffffff'],
                                                    }
                                                },
                                                splitNumber:4,

                                            }
                                        ],
                                series: [{
                                    type: 'bar',
									barWidth : 50,
                                    data: [parseInt(rm.result.records[0]['daily_sales_amount'])/divd,
                                            parseInt(rm.result.records[1]['daily_sales_amount'])/divd,
                                            parseInt(rm.result.records[2]['daily_sales_amount'])/divd,
                                            parseInt(rm.result.records[3]['daily_sales_amount'])/divd,
                                            parseInt( rm.result.records[4]['daily_sales_amount'])/divd,
                                            parseInt(rm.result.records[5]['daily_sales_amount'])/divd,
                                            parseInt(rm.result.records[6]['daily_sales_amount'])/divd]

                                }],
                                  itemStyle: {
                                      normal: {
                                          color: function (params) {
                                              // build a color map as your need.
                                              var colorList = [
                                               '#ff8888','#ff8888','#ff8888','#ff8888','#ff8888','#ff8888','#ffffff'
                                              ]
                                              ;
                                              return colorList[params.dataIndex]
                                          }
                                      }
                                  }

                            };
                            myChart.setOption(option);
                    }

                });
                    $.ajax({
                    url: "/dashboard/b2b_tv/company_lastmonth_sales_ranking",
                    type: 'POST',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                            "jsonrpc": "2.0",
                            "params": {},
                            "method": "call",

                        }
                    ),
                    success: function (rm) {
                        var product;
                        console.log(rm);
                         for(var i=0;i<rm.result.length-1;i++)
                         {
                             $("#company_ranking #c"+(i+1)+" #common_jxs_name").text(rm.result[i]['company_name']);
                             $("#company_ranking  #c"+(i+1)+" #common_jxs_number").text(rm.result[i]['retailer_num']);
                             $("#company_ranking  #c"+(i+1)+" #common_jxs_money").text(rm.result[i]['ps']);
                         }
                     if(parseInt(rm.result[rm.result.length-1]['total_company_number'])>9999)
                     {
                         $("#company_ranking #company-num").text(Number(parseInt(rm.result[rm.result.length-1]['total_company_number'])/10000).toFixed(2)+'万')
                     }
                        else
                     {
                         $("#company_ranking #company-num").text(rm.result[rm.result.length - 1]['total_company_number']);
                     }
                          if(parseInt(rm.result[rm.result.length-1]['total_retailer_number'])>9999)
                     {
                        $("#company_ranking #retailer_num").text(Number(parseInt(rm.result[rm.result.length-1]['total_retailer_number'])/10000).toFixed(2)+'万');
                     }
                        else
                     {
                         $("#company_ranking #retailer_num").text(rm.result[rm.result.length-1]['total_retailer_number']);
                     }


                    }

                });
                  $.ajax({
                    url: "/dashboard/b2b_tv/company_daily_sales_amount",
                    type: 'POST',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                            "jsonrpc": "2.0",
                            "params": {},
                            "method": "call",

                        }
                    ),
                    success: function (rm) {
                        var product;
                        console.log(rm);
                        var color_array = ['#DD514D','#FFC000','#4DABCD'];
                        var series=[];
                        for (var i=0;i<rm.result.records[0]['daily_sales_amount'].length;i++)
                        {
                            var line={
                                 name: rm.result.records[0]['daily_sales_amount'][i]['company'],
                                 type: 'line',
                                 data: [rm.result.records[0]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[1]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[2]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[3]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[4]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[5]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[6]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[7]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[8]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[9]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[10]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[11]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[12]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[13]['daily_sales_amount'][i]['sales_amount'],
                                        rm.result.records[14]['daily_sales_amount'][i]['sales_amount'],
                                        ],
                                symbolSize:10,
                                itemStyle : {
                                 normal : {
                                     color:color_array[i],
                                    lineStyle:{
                                        color:color_array[i],
                                        width:5
                                    }

                                }
                            },

                            };

                            series.push(line);
                        }
                        var myChart = echarts.init(document.getElementById('main2'));

                        // 指定图表的配置项和数据
                        var option = {
                            title: {
                                text: ''
                            },
                            tooltip: {},
                            legend: {
                                data:['']
                            },
                            xAxis: {

                                    axisLine:{
                                                    show:false
                                                },
                                     axisLabel : {
                                                     textStyle:{
                                                        fontWeight: 'bold',

                                                    }

                                                },
                                    axisTick: {
                                        show: false,
                                    },


                                data: [rm.result.records[0]['date'],
                                    rm.result.records[1]['date'],
                                    rm.result.records[2]['date'],
                                    rm.result.records[3]['date'],
                                    rm.result.records[4]['date'],
                                    rm.result.records[5]['date'],
                                    rm.result.records[6]['date'],
                                    rm.result.records[7]['date'],
                                    rm.result.records[8]['date'],
                                    rm.result.records[9]['date'],
                                    rm.result.records[10]['date'],
                                    rm.result.records[11]['date'],
                                    rm.result.records[12]['date'],
                                    rm.result.records[13]['date'],
                                    rm.result.records[14]['date']]
                            },
                            yAxis: {
                                show:false
                            },
                            series:series,
                        };

                        // 使用刚指定的配置项和数据显示图表。
                        myChart.setOption(option);

                    }

                });

              $.ajax({
                    url: "/dashboard/b2b_tv/product_sales_ranking",
                    type: 'POST',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                            "jsonrpc": "2.0",
                            "params": {},
                            "method": "call",

                        }
                    ),
                    success: function (rm) {
                        var product;
                        console.log(rm);

                        $("#top-selling-products #t1 #sale_name").text(rm.result[0]['product_name']);
                        $("#top-selling-products #t1 #sale_money").text(rm.result[0]['sales_amount']);
                        $("#top-selling-products #t2 #sale_name").text(rm.result[1]['product_name']);
                        $("#top-selling-products #t2 #sale_money").text(rm.result[1]['sales_amount']);
                        $("#top-selling-products #t3 #sale_name").text(rm.result[2]['product_name']);
                        $("#top-selling-products #t3 #sale_money").text(rm.result[2]['sales_amount']);
                        $("#top-selling-products #t4 #sale_name").text(rm.result[3]['product_name']);
                        $("#top-selling-products #t4 #sale_money").text(rm.result[3]['sales_amount']);
                        $("#top-selling-products #t5 #sale_name").text(rm.result[4]['product_name']);
                        $("#top-selling-products #t5 #sale_money").text(rm.result[4]['sales_amount']);
                        $("#top-selling-products #t6 #sale_name").text(rm.result[5]['product_name']);
                        $("#top-selling-products #t6 #sale_money").text(rm.result[5]['sales_amount']);


                    }

                });
    }
    function minute_refresh(){

                    $.ajax({
                        url: "/dashboard/b2b_tv/order_statistics",
                        type: 'POST',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                                "jsonrpc": "2.0",
                                "params": {},
                                "method": "call",

                            }
                        ),
                        success: function (rm) {
                            var product;
                            var totamount=rm.result.records[0]['total_sales_amount'];
                            var tdamount=rm.result.records[0]['today_sales_amount'];
                            var totorder=rm.result.records[0]['total_sales_count'];
                            var tdorder=rm.result.records[0]['today_sales_count'];
                            var lastm=rm.result.records[0]['lastmonth_sales_amount'];
                            var stm=$("#sold-amount #total-amount").text();
                            var sdm=$("#sold-amount #today-amount").text();
                            var std=$("#sold-order #total-order").text();

                            console.log(rm);
                            if(parseInt(totamount)!=parseInt(stm)) {
                                if (stm.charAt(stm.length - 1) == '亿') {

                                    if ((Number(totamount) / 100000000).toFixed(2).toString() != stm.substr(0, stm.length - 1)) {

                                        scrollnum_totleamount("#sold-amount #total-amount", totamount);
                                    }
                                    else{
                                        console.log();
                                    }

                                }
                                else {
                                    scrollnum_totleamount("#sold-amount #total-amount", totamount);
                                }
                            }


                             if(parseInt(tdamount)!=parseInt(sdm))
                            {
                                scrollnum_todayamount("#sold-amount #today-amount", tdamount);
                            }

                              if(parseInt(totorder)!=parseInt(std)) {
                                if (std.charAt(std.length - 1) == '万') {

                                    if ((Number(totorder) / 10000).toFixed(2).toString() != std.substr(0, std.length - 1)) {

                                        scrollnum_totalorder("#sold-order #total-order",totorder);
                                    }
                                    else{
                                        console.log();
                                    }

                                }
                                else {
                                    scrollnum_totalorder("#sold-order #total-order", totorder);
                                }
                            }
                            scrollnum_todayorder("#sold-order #today-order", tdorder);

                            if(parseFloat(lastm)>9999)
                            {
                            $("#company_ranking #lastmouth_amount").text(Number(parseFloat(lastm)/10000).toFixed(2)+'万');
                            }
                            else{
                                 $("#company_ranking #lastmouth_amount").text(parseFloat(lastm));
                            }
                        }

                    });
    }


     function scrollnum_totleamount(name,num){

          var decimal_places = 2;
          var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;
          var unit='';
         var tage=num;
         if (parseInt(num)>100000000)
         {
              tage=100000000;
         }
          $(name)
              .prop('number',parseFloat($(name).text()) )
              .animateNumber(
            {
                number: parseFloat(tage)*decimal_factor,
                numberStep: function(now, tween) {
                var floored_number = Math.floor(now) / decimal_factor,
                    target = $(tween.elem);
                if (decimal_places > 0) {
                  // force decimal places even if they are 0
                  floored_number = floored_number.toFixed(decimal_places);

                }
                    if(floored_number>99999999)
                    {
                        $(name)
                              .prop('number',1)
                              .animateNumber(
                            {
                                number: parseFloat((Number(num/100000000)).toFixed(2))*decimal_factor,
                                numberStep: function(now, tween) {
                                var floored_number = Math.floor(now) / decimal_factor,
                                    target = $(tween.elem);
                                if (decimal_places > 0) {
                                  // force decimal places even if they are 0
                                  floored_number = floored_number.toFixed(decimal_places);

                                }

                                target.text(floored_number+'亿');
                                }
                              },
                              3600
                          )

                    }
                target.text(floored_number);
                }
              },
              3600
          )


 }
    function scrollnum_todayamount(name,num){
          var decimal_places = 2;
          var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;
          $(name)
              .prop('number',parseFloat($(name).text()) )
              .animateNumber(
            {
                number: parseFloat(num)*decimal_factor,
                numberStep: function(now, tween) {
                var floored_number = Math.floor(now) / decimal_factor,
                    target = $(tween.elem);
                if (decimal_places > 0) {
                  // force decimal places even if they are 0
                  floored_number = floored_number.toFixed(decimal_places);

                }
                target.text(floored_number);
                }
              },
              3600
          )

 }
    function scrollnum_totalorder(name,num){
         var decimal_places = 2;
          var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;
          var unit='';
         var tage=num;
         if (parseInt(num)>10000)
         {
              tage=10000;
         }
          $(name)
              .prop('number',parseFloat($(name).text()) )
              .animateNumber(
            {
                number: parseFloat(tage),
                numberStep: function(now, tween) {
                var floored_number = Math.floor(now) ,
                    target = $(tween.elem);
                    if(floored_number>9999)
                    {
                        $(name)
                              .prop('number',1)
                              .animateNumber(
                            {
                                number: parseFloat((Number(num/10000)).toFixed(2))*decimal_factor,
                                numberStep: function(now, tween) {
                                var floored_number = Math.floor(now)/ decimal_factor ,
                                    target = $(tween.elem);
                                     if (decimal_places > 0)
                                     {
                                          // force decimal places even if they are 0
                                          floored_number = floored_number.toFixed(decimal_places);

                                        }
                                target.text(floored_number+'万');
                                }
                              },
                              3600
                          )

                    }
                target.text(floored_number);
                }
              },
              3600
          )

          //$(name)
          //    .prop('number',parseFloat($(name).text()) )
          //    .animateNumber(
          //  {
          //      number: parseInt(num),
          //      numberStep: function(now, tween) {
          //      var floored_number = Math.floor(now)
          //          target = $(tween.elem);
          //
          //      target.text(floored_number);
          //      }
          //    },
          //    3600
          //)

 }
 function scrollnum_todayorder(name,num){
          $(name)
              .prop('number',parseFloat($(name).text()) )
              .animateNumber(
            {
                number: parseInt(num),
                numberStep: function(now, tween) {
                var floored_number = Math.floor(now)
                    target = $(tween.elem);

                target.text(floored_number);


                }
              },
              3600
          )

 }
    function scrollnum_lastmouthamount(name,num){
          var decimal_places = 2;
          var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;
          $(name)
              .prop('number',parseFloat($(name).text()) )
              .animateNumber(
            {
                number: parseFloat(num)*decimal_factor,
                numberStep: function(now, tween) {
                var floored_number = Math.floor(now) / decimal_factor,
                    target = $(tween.elem);
                if (decimal_places > 0) {
                  // force decimal places even if they are 0
                  floored_number = floored_number.toFixed(decimal_places);

                }
                target.text(floored_number);
                }
              },
              3600
          )

 }



});
