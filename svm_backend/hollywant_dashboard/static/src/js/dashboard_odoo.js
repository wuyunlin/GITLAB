/**
 * Created by zhouh on 2016/11/5.
 */
odoo.define('hollywant_dashboard.hollywant_vmc_tv', function (require) {
    "use strict";
    var core = require('web.core');
    var Widget = require('web.Widget');
    var ajax = require('web.ajax');
    var QWeb = core.qweb;
    var bus = require('bus.bus').bus;
    var HomePageTemplate = Widget.extend(
        {
            template: "tv",
            init: function (parent) {
                var self = this;
                this._super(parent);
                var timer;
                 window.onresize = function () {
                     self.$el.css("height",$(window).height()-80);
                     self.$el.css("width","100%");
                 }

                timer = window.setInterval(function(){
                     var self=this;
                            var mydate = new Date();
                            var year= mydate.getFullYear();
                            var month= mydate.getMonth() + 1;
                            var day= mydate.getDate();
                            var date=year+'/'+month+'/'+day;
                            var hour=mydate.getHours();
                            var minute=mydate.getMinutes();
                            var hour_head='';
                            var minute_head='';
                            if(hour<10)
                            {
                                hour_head='0';
                            }
                             if(minute<10)
                             {
                                 minute_head='0';
                             }
                            var nowtime=hour_head+hour+':'+minute_head+minute;
                            self.$("#time").text(date);
                            self.$("#endtime").text(nowtime);
                    }, 1000);

                bus.add_channel('auto_refresh_vmc_dashboard');
                bus.start_polling();
                console.log("Start polling\n\n");
                self.daily_sale();
                self.lastmonth();
                self.com_daily_sale();
                self.product_rank();
                self.order();


            },
            start: function () {
                var self = this;
                self.$el.css("height",$(window).height()-70);
                self.$el.css("width","100%");
                var margin_top = $(window).width()/100;
                var left_reference = $(window).height()*0.64-30;
                console.log(self.$('#company_ranking').css({
                    'margin-top':margin_top,
                }));
                console.log(self.$('#center_down').css({
                     'margin-top':margin_top,
                }));
                console.log(self.$('#top-selling-products').css({
                     'margin-top':margin_top,
                }));

                bus.on('notification', this, function (notification) {
                    var self = this;
                    console.log('\n\nGot notification');
                    console.log(notification[0]);
                    var message = notification[0][0];
                    var model = notification[0][1];
                    console.log(model);
                    //console.log(message);
                    if (model == 'vmc.order') {
                        console.log("notification from vmc order");
                        if (message == 'auto_refresh_vmc_dashboard'){
                            console.log("message: auto_refresh_vmc_dashboard");
                            self.daily_sale();
                            self.lastmonth();
                            self.com_daily_sale();
                            self.product_rank();
                            self.order();
                            console.log("finished load sale report");
                        }
                        console.log("\n\n");
                    }
            });

            },
            daily_sale :function(){
                var width=$(window).width();
               var self = this;
                 self.rpc("/dashboard/vmc_tv/daily_sales_amount",{}).then(function (rm) {
                   var product;
                     console.log(rm);
                             var unit;
                             var divd;
                            var max=parseInt(rm.records[0]['daily_sales_amount']);
                            for (var i=0;i<rm.records.length;i++)
                            {
                                 if(max<parseInt(rm.records[i]['daily_sales_amount']))
                                 {
                                     max=parseInt(rm.records[i]['daily_sales_amount']);
                                 }
                            }

                            if(parseInt(Number(max/100000000).toFixed(2))!=0)
                            {
                                unit='亿';
                                divd=100000000;
                            }else if(parseInt(Number(max/10000).toFixed(2))!=0)
                            {
                                unit='万';
                                divd=10000;
                            }else if(parseInt(Number(max/1000).toFixed(2))!=0)
                            {
                                unit='千';
                                divd=1000;
                            }else {
                                unit='';
                                divd=1;
                            }
                            console.log(unit);
                            console.log(divd);
                            var myChart = echarts.init(document.getElementById('main'));
                            var option = {
                                title: {
                                    text: ''
                                },
                                grid:{
                                    top:40,
                                    bottom:40,
                                    left:110,
                                    right:110
                                },
                                tooltip : {         // Option config. Can be overwrited by series or data

                                            trigger: 'axis',
                                            //show: true,   //default true
                                            showDelay: 0,
                                            hideDelay: 50,
                                            transitionDuration:0,
                                            borderRadius : 8,
                                            borderWidth: 2,
                                            padding: 10,    // [5, 10, 15, 20]
                                            position : function(p) {
                                                // 位置回调
                                                // console.log && console.log(p);
                                                return [p[0] + 10, p[1] - 10];
                                            },
                                            textStyle : {
                                                decoration: 'none',
                                                fontSize: 1,
                                            },
                                            formatter: function (params,ticket,callback) {
                                                var res='';
                                                for (var i = 0, l = params.length; i < l; i++) {
                                                    res +=params[i].seriesName + ' : ' + params[i].value+unit;
                                                }
                                               return res;
                                            }

                                        },
                                legend: {
                                    data:[]
                                },
                                xAxis: {
                                    data: [rm.records[0]['date'],
                                            rm.records[1]['date'],
                                            rm.records[2]['date'],
                                            rm.records[3]['date'],
                                            rm.records[4]['date'],
                                            rm.records[5]['date'],
                                            rm.records[6]['date']],
                                     axisLine:{
                                                    lineStyle:{
                                                        color:['#ffffff'],
                                                    }
                                                },
                                     axisLabel : {
                                                     textStyle:{
                                                        fontWeight: 'bold',
                                                         fontSize:14-parseInt((1920-parseInt(width))/38),

                                                    }

                                                },
                                    axisTick:{
                                         show:false,
                                    },

                                },
                                yAxis: [
                                            {
                                                type : 'value',
                                                axisLabel : {
                                                    formatter: '{value} '+unit,
                                                    textStyle:{
                                                        fontWeight: 'bold',
                                                        fontSize: 18-parseInt((1920-parseInt(width))/30),

                                                    }

                                                },
                                                axisLine:{
                                                    show: false,
                                                    lineStyle:{
                                                        color:['#ffffff'],
                                                    }
                                                },
                                                splitNumber:4,

                                            }
                                        ],
                                series: [{
                                    name:'当日销售',
                                    type: 'bar',
									barWidth : 50,
                                    data: [parseInt(rm['records'][0]['daily_sales_amount'])/divd,
                                            parseInt(rm.records[1]['daily_sales_amount'])/divd,
                                            parseInt(rm.records[2]['daily_sales_amount'])/divd,
                                            parseInt(rm.records[3]['daily_sales_amount'])/divd,
                                            parseInt( rm.records[4]['daily_sales_amount'])/divd,
                                            parseInt(rm.records[5]['daily_sales_amount'])/divd,
                                            parseInt(rm.records[6]['daily_sales_amount'])/divd],
                                    label:{
                                        normal:{
                                            formatter:function(params){
                                                var res=params[0].name;
                                                for(var i= 0,l=params.length;i<l;i++)
                                                {
                                                    res+=params[i].seriesName+':'+params[i].value+unit;
                                                }
                                                return res;
                                            }
                                        }
                                    }

                                }],
                                  itemStyle: {
                                      normal: {
                                          color: function (params) {
                                              // build a color map as your need.
                                              var colorList = [
                                               '#ff8888','#ff8888','#ff8888','#ff8888','#ff8888','#ff8888','#ffffff'
                                              ];
                                              return colorList[params.dataIndex]
                                          }
                                      },
                                      emphasis:{
                                          color:'#ffffff'
                                      }

                                  }

                            };
                            myChart.setOption(option);
                    });
            },
             lastmonth:function()
             {
                 var self = this;

                 self.rpc("/dashboard/vmc_tv/vmc_lastmonth_sales_ranking", {}).then(function (rm) {
                     console.log(rm);
                         for(var i=0;i<rm.length-2;i++) {
                             self.$el.find("#company_ranking #c" + (i + 1) + " #common_jxs_name").text(rm[i]['vmc_name']);
                             self.$el.find("#company_ranking  #c" + (i + 1) + " #common_jxs_number").text(rm[i]['vmc_name']);
                             if (parseFloat(rm[i]['ps']) > 99999999) {
                                 self.$el.find("#company_ranking  #c" + (i + 1) + " #common_jxs_money").text(Number(parseInt(rm[i]['ps']) / 100000000).toFixed(2) + '亿');
                             }
                             else if (parseFloat(rm[i]['ps']) > 9999)
                             {
                                  self.$el.find("#company_ranking  #c" + (i + 1) + " #common_jxs_money").text(Number(parseInt(rm[i]['ps']) / 10000).toFixed(2) + '万');
                             }
                             else {
                                 self.$el.find("#company_ranking  #c" + (i + 1) + " #common_jxs_money").text(Number(parseInt(rm[i]['ps'])).toFixed(2));
                             }
                             }

                          if(parseInt(rm[rm.length-2]['total_retailer_number'])>9999)
                     {
                        self.$el.find("#retailer_num").text(Number(parseInt(rm[rm.length-2]['total_vmc_number'])/10000).toFixed(2)+'万');
                     }
                        else
                     {
                         self.$el.find("#retailer_num").text(rm[rm.length-2]['total_vmc_number']);
                     }

                       if(parseInt(rm[rm.length-1]['thisweek_vmc_number'])>9999)
                     {
                        self.$el.find("#company-num").text(Number(parseInt(rm[rm.length-1]['thisweek_vmc_number'])/10000).toFixed(2)+'万');
                     }
                        else
                     {
                         self.$el.find("#company-num").text(rm[rm.length-1]['thisweek_vmc_number']);
                     }


                    });
             },
            com_daily_sale:function()
            {
                var self = this;
                var unit,divd;
                 var width=$(window).width();

                 self.rpc("/dashboard/vmc_tv/vmc_daily_sales_amount",  {}).then(function (rm) {
                     console.log(rm);
                        var product;
                        var max=0.0;
                        if (rm.records[0]['daily_sales_amount'].length>0){
                            max=rm.records[0]['daily_sales_amount'][0]['sales_amount'];
                            for (var i=0;i<rm.records.length;i++)
                            {
                                for(var j=0;j< rm.records[1]['daily_sales_amount'].length;j++)
                                {
                                    if (max < parseInt(rm.records[i]['daily_sales_amount'][j]['sales_amount'])) {
                                        max = parseInt(rm.records[i]['daily_sales_amount'][j]['sales_amount']);
                                    }
                                }
                            }
                        }
                         if(parseInt(Number(max/100000000).toFixed(2))!=0)
                            {
                                unit='亿';
                                divd=100000000;
                            }else if(parseInt(Number(max/10000).toFixed(2))!=0)
                            {
                                unit='万';
                                divd=10000;
                            }else if(parseInt(Number(max/1000).toFixed(2))!=0)
                            {
                                unit='千';
                                divd=1000;
                            }else {
                                unit='';
                                divd=1;
                            }
                        var color_array = ['#DD514D','#FFC000','#4DABCD','#00B642'];
                        var series=[];
                        for (var i=0;i<rm.records[0]['daily_sales_amount'].length;i++)
                        {
                            var line={
                                 name: rm.records[0]['daily_sales_amount'][i]['vmc_name'],
                                type: 'line',
                                data: [parseInt(rm.records[0]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[1]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[2]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[3]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[4]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[5]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[6]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[7]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[8]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[9]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[10]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[11]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[12]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[13]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        parseInt(rm.records[14]['daily_sales_amount'][i]['sales_amount'])/divd,
                                        ],
								symbolSize:10,
                                itemStyle : {
                                normal : {
                                    color:color_array[i],
                                    lineStyle:{
                                    color:color_array[i],
                                    width:5
                                    }

                                }
                            },
                            };
                            series.push(line);
                        }
                        var myChart = echarts.init(document.getElementById('main2'));
                        var option = {
                            animation:false,
                            title: {
                                text: ''
                            },
                            tooltip: {},
                            legend: {
                                data:[''],

                            },
                            grid:{
                                top:70,
                              bottom:40,
                                left:110,
                                right:110,
                            },
                            xAxis: {

                                    axisLine:{
                                                    show:false
                                                },
                                     axisLabel : {
                                                     textStyle:{
                                                        fontWeight: 'bold',
                                                        fontSize:parseInt(14-parseInt((1920-parseInt(width))/38)),
                                                         color:'#777777',

                                                    }

                                                },
                                    axisTick: {
                                        show: false,
                                    },


                                data: [rm.records[0]['date'],
                                    rm.records[1]['date'],
                                    rm.records[2]['date'],
                                    rm.records[3]['date'],
                                    rm.records[4]['date'],
                                    rm.records[5]['date'],
                                    rm.records[6]['date'],
                                    rm.records[7]['date'],
                                    rm.records[8]['date'],
                                    rm.records[9]['date'],
                                    rm.records[10]['date'],
                                    rm.records[11]['date'],
                                    rm.records[12]['date'],
                                    rm.records[13]['date'],
                                    rm.records[14]['date']]
                            },
                           yAxis: [
                                            {
                                                type : 'value',
                                                axisLabel : {
                                                    formatter: '{value} '+unit,
                                                    textStyle:{
                                                        fontWeight: 'bold',
                                                        fontSize: 18,
                                                         color:'#777777',

                                                    }

                                                },
                                                axisLine:{
                                                    show: false,
                                                    lineStyle:{
                                                        color:['#000000'],
                                                    }
                                                },
                                                splitNumber:4,

                                            }
                                        ],
                            series:series,
                        };
                        myChart.setOption(option);

                    });
                console.log(width);
                console.log(parseInt(14-parseInt((1920-parseInt(width))/38)));
            },
            product_rank:function()
            {
                var self = this;



                 self.rpc("/dashboard/vmc_tv/product_sales_ranking", {}).then(function (rm) {
                     var product;
                     console.log(rm);
                     for (var i = 1; i <rm.length+1; i++) {

                             $("#top-selling-products #t"+i+" #sale_name").text(rm[i-1]['product_name']);
                             if (parseFloat(rm[i-1]['sales_amount']) > 99999999) {
                                 $("#top-selling-products #t"+i+" #sale_money").text(Number(parseFloat(rm[i-1]['sales_amount']) / 100000000).toFixed(2) + '亿');
                             }
                             else if(parseFloat(rm[i-1]['sales_amount']) > 9999)
                             {
                                  $("#top-selling-products #t"+i+" #sale_money").text(Number(parseFloat(rm[i-1]['sales_amount']) / 10000).toFixed(2) + '万');
                             }
                             else {
                                 $("#top-selling-products #t"+i+" #sale_money").text(rm[i-1]['sales_amount']);
                             }
                     }
                         $("#top-selling-products #t2 #sale_name").text(rm[1]['product_name']);
                        $("#top-selling-products #t2 #sale_money").text(rm[1]['sales_amount']);
                        $("#top-selling-products #t3 #sale_name").text(rm[2]['product_name']);
                        $("#top-selling-products #t3 #sale_money").text(rm[2]['sales_amount']);
                        $("#top-selling-products #t4 #sale_name").text(rm[3]['product_name']);
                        $("#top-selling-products #t4 #sale_money").text(rm[3]['sales_amount']);
                        $("#top-selling-products #t5 #sale_name").text(rm[4]['product_name']);
                        $("#top-selling-products #t5 #sale_money").text(rm[4]['sales_amount']);
                        $("#top-selling-products #t6 #sale_name").text(rm[5]['product_name']);
                        $("#top-selling-products #t6 #sale_money").text(rm[5]['sales_amount']);
                    });
            },

             order: function() {
                 var self = this;




                 self.rpc("/dashboard/vmc_tv/order_statistics", {}).then(function (rm) {
                     console.log(rm);
                            var product;
                            var totamount=rm["records"][0]['total_sales_amount'];
                            var tdamount=rm["records"][0]['today_sales_amount'];
                            var totorder=rm["records"][0]['total_sales_count'];
                            var tdorder=rm["records"][0]['today_sales_count'];
                            var lastm=rm["records"][0]['lastmonth_sales_amount'];
                            var stm=self.$el.find("#sold-amount #total-amount").text();
                            var sdm=self.$el.find("#sold-amount #today-amount").text();
                            var std=self.$el.find("#sold-order #total-order").text();

                            console.log(rm["records"]);
                             if(parseFloat(totamount)>99999999)
                                    {
                                    self.$el.find("#sold-amount #total-amount").text(Number(parseFloat(totamount)/100000000).toFixed(2)+'亿');
                                    }
                                    else{
                                           if(parseFloat(totamount)>9999) {
                                                self.$el.find("#sold-amount #total-amount").text(Number(parseFloat(totamount)/10000).toFixed(2)+'万');
                                           }
                                            else
                                           {
                                                self.$el.find("#sold-amount #total-amount").text(parseFloat(totamount));
                                           }
                                    }


                             self.$el.find("#sold-amount #today-amount").text(tdamount);
                              if(parseInt(totorder)>9999)
                                    {
                                    self.$el.find("#sold-order #total-order").text(Number(parseInt(totorder)/10000).toFixed(2)+'万');
                                    }
                                    else{
                                         self.$el.find("#sold-order #total-order").text(parseInt(totorder));
                                    }
                            self.$el.find("#sold-order #today-order").text(tdorder);
                            if(parseFloat(lastm)>99999999)
                            {
                            self.$el.find("#company_ranking #lastmouth_amount").text(Number(parseFloat(lastm)/100000000).toFixed(2)+'亿');
                            }
                            else if(parseFloat(lastm)>9999)
                            {
                                self.$el.find("#company_ranking #lastmouth_amount").text(Number(parseFloat(lastm)/10000).toFixed(2)+'万');
                            }
                            else{
                                 self.$el.find("#company_ranking #lastmouth_amount").text(parseFloat(lastm));
                            }
                    });


             },
        }
    );

    core.action_registry.add('hollywant_dashboard.hollywant_vmc_tv', HomePageTemplate);

    HomePageTemplate.is_ready = init();
    return HomePageTemplate;
});