/**
 * Created by zhouhao on 16-12-1.
 */
odoo.define('hollywant_dashboard.hollywant_vmc_warning', function (require) {
    "use strict";
    var core = require('web.core');
    var Widget = require('web.Widget');
    var ajax = require('web.ajax');
    var bus = require('bus.bus').bus;
    var HomePageTemplate = Widget.extend(
        {
            template: "warning",
            init: function (parent) {
                var self = this;
                this._super(parent);
                var timer;
                 window.onresize = function () {
                     self.$el.css("height",$(window).height()-80);
                     self.$el.css("width","100%");
                 }

                timer = window.setInterval(function(){

                     var self=this;
                            var mydate = new Date();
                            var year= mydate.getFullYear();
                            var month= mydate.getMonth() + 1;
                            var day= mydate.getDate();
                            var date=year+'/'+month+'/'+day;
                            var hour=mydate.getHours();
                            var minute=mydate.getMinutes();
                            var hour_head='';
                            var minute_head='';
                            if(hour<10)
                            {
                                hour_head='0';
                            }
                             if(minute<10)
                             {
                                 minute_head='0';
                             }
                            var nowtime=hour_head+hour+':'+minute_head+minute;
                            self.$("#time").text(date);
                            self.$("#endtime").text(nowtime);
                    }, 1000);

                bus.add_channel('auto_refresh_vmc_warning');
                bus.start_polling();
                console.log("Start polling\n\n");


            },
            start: function () {
                var self = this;
                self.$el.css("height",$(window).height()-70);
                self.$el.css("width","100%");

                var margin_top = $(window).width()/100;
                var left_reference = $(window).height()*0.64-30;
                console.log(self.$('#company_ranking').css({
                    'margin-top':margin_top,
                }));
                console.log(self.$('#center_down').css({
                     'margin-top':margin_top,
                }));
                console.log(self.$('#top-selling-products').css({
                     'margin-top':margin_top,
                }));

                bus.on('notification', this, function (notification) {
                    var self = this;
                    console.log('\n\nGot notification');
                    console.log(notification);
                    var message = notification[0];
                    var model = notification[1];
                    console.log(model);
                    //console.log(message);
                    if (model == 'vmc.warning') {
                        console.log("notification from vmc warning");
                        if (message == 'auto_refresh_vmc_warning'){
                            console.log("message: auto_refresh_vmc_warning");
                            self.tableRefresh();
                            console.log("finished load sale report");
                        }
                        console.log("\n\n");
                    }
            });
                self.tableRefresh();
                self.$('table tr td').css('width',$(window).width()/10);
            },

            tableRefresh: function () {
                var self =this;
                self.rpc("/dashboard/vmc_tv/vmc_monitor", {}).then(function (rm) {
                    var mid="";
                    console.log(rm);
                    for(var i=0;i<rm.records.length;i++)

                     {

                         var net;
                         var lab;
                         var door;
                         var soldout;
                         var papersold;
                         var leak5;
                         var leak1;

                         if(rm.records[i].net_status.toString().toLocaleLowerCase()=="true")
                         {
                             net="<td style='color:#A757A8'>异常</td>";
                         }
                         else if(rm.records[i].net_status.toString().toLocaleLowerCase()=="false") {
                             net="<td>正常</td>";
                         }
                         else{
                             net="<td>数据为空</td>";
                         }
                          if(rm.records[i].is_location_abnormal.toString().toLocaleLowerCase()=="true")
                         {
                             lab="<td style='color:red'>异常</td>";
                         }
                         else if(rm.records[i].is_location_abnormal.toString().toLocaleLowerCase()=="false"){
                             lab="<td>正常</td>";
                         }
                         else {
                              lab="<td>数据为空</td>";
                          }
                          if(rm.records[i].is_dooropen.toString().toLocaleLowerCase()=="true")
                         {
                             door="<td style='color:#E9967A'>开</td>";
                         }
                         else if(rm.records[i].is_dooropen.toString().toLocaleLowerCase()=="false"){
                              console.log(rm.records[i].is_dooropen);
                             door="<td>关</td>";
                         }
                         else{
                               door="<td>数据为空</td>";
                          }
                         if(rm.records[i].is_load_soldout.toString().toLocaleLowerCase()=="true")
                         {
                            soldout="<td style='color:#E9967A'>是</td>";
                         }
                         else if(rm.records[i].is_load_soldout.toString().toLocaleLowerCase()=="false"){
                             soldout="<td>否</td>";
                         }
                         else{
                             soldout="<td>数据为空</td>";
                         }
                          if(rm.records[i].is_vmc_disconnected.toString().toLocaleLowerCase()=="true")
                         {
                            papersold="<td style='color:#E9967A'>是</td>";
                         }
                         else if(rm.records[i].is_vmc_disconnected.toString().toLocaleLowerCase()=="false"){
                             papersold="<td>否</td>";
                         }
                         else{
                              papersold="<td>数据为空</td>";
                          }
                          if (rm.records[i].payment_cash.toString().toLocaleLowerCase()=="false")
                         {
                             leak5="<td>-</td>";
                             leak1="<td>-</td>";
                         }
                         else
                             {
                              if (rm.records[i].is_leakchange_5jiao.toString().toLocaleLowerCase() == "true") {
                                  leak5 = "<td style='color:#E9967A'>是</td>";
                              }
                              else if (rm.records[i].is_leakchange_5jiao.toString().toLocaleLowerCase() == "false") {
                                  leak5 = "<td>否</td>";
                              }
                              else {
                                  leak5 = "<td>数据为空</td>";
                              }
                              if (rm.records[i].is_leakchange_1yuan.toString().toLocaleLowerCase() == "true") {
                                  leak1 = "<td style='color:#E9967A'>是</td>";
                              }
                              else if (rm.records[i].is_leakchange_1yuan.toString().toLocaleLowerCase() == "false") {
                                  leak1 = "<td>否</td>";
                              }
                              else {
                                  leak1 = "<td>数据为空</td>";
                              }
                          }
                        mid=mid+"<tr style='border: 1px solid red'> " +
                            "<td>"+rm.records[i].vmc_name+"</td> "+
                            "<td>"+rm.records[i].vmc_address+"</td> " +
                            "<td>"+rm.records[i].vmc_location+"</td> " +
                             net+
                             lab+
                            door+
                            soldout +
                            papersold+
                            leak5+
                            leak1+
                            "</tr>";
                    }
                    $("#tab #tb").html(mid);

                    });
            },




        }
    );

    core.action_registry.add('hollywant_dashboard.hollywant_vmc_warning', HomePageTemplate);

    HomePageTemplate.is_ready = init();
    return HomePageTemplate;
});