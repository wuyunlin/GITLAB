{
    'name': 'account config group',
    'description': '财务权限管理组',
    'author': 'holly-want',
	'category': 'HollyWant',
    'depends': ['base','account'],
    'data':[
        'security/security_account_group_config.xml',
    ],
    'installable': True,
}
# -*- coding: utf-8 -*-
