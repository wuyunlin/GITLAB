from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Person(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()

class Person22(models.Model):
    name2 = models.CharField(max_length=30)
    age = models.IntegerField()

class Person122(models.Model):
    name2 = models.CharField(max_length=30)
    age = models.IntegerField()